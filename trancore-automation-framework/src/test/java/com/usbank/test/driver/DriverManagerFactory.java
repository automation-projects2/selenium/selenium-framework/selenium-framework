package com.usbank.test.driver;

import com.usbank.test.enums.BrowserType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.TimeUnit;

public class DriverManagerFactory {
    private static final Logger logger = LogManager.getLogger(DriverManagerFactory.class);


    public static DriverManager initializeDriver(String runModeString, String browserString) {
        RunMode runMode = RunMode.valueOf(runModeString);
        BrowserType browserType = BrowserType.valueOf(browserString);
        return initializeDriver(runMode, browserType);
    }

    public static DriverManager initializeDriver(RunMode runMode, BrowserType browserType) {
        DriverManager driverManager = null;

        String errorMismatchMessage = "Browser Type: " + browserType.toString() + " is not supported for Run Mode: " + runMode.toString();

        if (runMode == null) {
            logAndThrowError("Run Mode is null!");
        }

        switch (runMode) {
            case LOCAL: {
                switch (browserType) {
                    case FIREFOX:
                        driverManager = new FirefoxDriverManager();
                        break;
                    case IE:
                        driverManager = new IEDriverManager();
                        break;
                    case EDGE:
                        driverManager = new EdgeDriverManager();
                        break;
                    case CHROME:
                        driverManager = new ChromeDriverManager();
                        break;
                    case HEADLESS:
                        driverManager = new HeadlessDriverManager();
                        break;
                    default:
                        logAndThrowError("Not Valid Browser Type for local run mode!");
                }
                break;
            }
            case REMOTE_CBT:
                driverManager = new RemoteCBT();
                break;
            case GRID:
                driverManager = new RemoteSeleniumGrid();
                break;
            case REMOTE_SEETESTBROWSER: {
                if (isNativeBrowserType(browserType)) {
                    logAndThrowError(errorMismatchMessage);
                } else {
                    driverManager = new RemoteSeetest();
                }
                break;
            }
            case REMOTE_SEETESTNATIVE: {
                if (isNativeBrowserType(browserType)) {
                    driverManager = new RemoteSeetest();
                } else {
                    logAndThrowError(errorMismatchMessage);
                }
                break;
            }
            default:
                logAndThrowError("Run Mode: " + runMode.toString() + "is not currently supported!");
        }

        if (driverManager == null) {
            logAndThrowError("Driver Manager is Null!");
        }

        driverManager.setBrowserType(browserType);
        driverManager.setRunMode(runMode);
        driverManager.createDriver();
        driverManager.getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return driverManager;

    }

    public static boolean isNativeBrowserType(BrowserType browserType) {
        if (browserType.equals(BrowserType.ANDROID_NATIVE_APPLICATION) || browserType.equals(BrowserType.IOS_NATIVE_APPLICATION)) {
            return true;
        }
        return false;
    }

    public static void logAndThrowError(String errorMessage) {
        logger.error(errorMessage);
        throw new RuntimeException(errorMessage);
    }
}
