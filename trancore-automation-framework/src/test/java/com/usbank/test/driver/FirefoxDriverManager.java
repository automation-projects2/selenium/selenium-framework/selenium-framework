package com.usbank.test.driver;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class FirefoxDriverManager extends DriverManager {
    @Override
    public void createDriver() {
        System.setProperty("webdriver.gecko.driver",
                loadProperty("/config.properties", "automation.driver.firefox.location"));
        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("--start-maximized");
        driver = new FirefoxDriver(options);
    }
}
