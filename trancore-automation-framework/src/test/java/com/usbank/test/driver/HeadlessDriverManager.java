package com.usbank.test.driver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class HeadlessDriverManager extends DriverManager {
    @Override
    public void createDriver() {
        System.setProperty("webdriver.chrome.driver",
                loadProperty("/config.properties", "automation.driver.chrome.location"));
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-extensions");
        options.addArguments("--ignore-certificate-errors");
        options.addArguments("--disable-gpu");
        options.addArguments("--no-sandbox");
        options.addArguments("--headless");
        driver = new ChromeDriver(options);
    }
}
