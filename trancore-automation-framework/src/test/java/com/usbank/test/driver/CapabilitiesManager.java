package com.usbank.test.driver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Loads the appium capabilities
 */

public final class CapabilitiesManager {
    private static DesiredCapabilities capabilities;
    private static final String ACCESS_KEY = "accessKey";

    private static final Logger log = LogManager.getLogger(CapabilitiesManager.class);
    /** Reads the appium capabilities from json file * */
//    public static DesiredCapabilities getCapabilities() {
//        log.info("Getting desired capabilities");
//        switch (AppCache.platform) {
//            case ANDROID:
//                setCapabilities(JsonFile.ANDROID_CONFIG);
//                break;
//            case IOS:
//                setCapabilities(JsonFile.IOS_CONFIG);
//                break;
//            case WEB:
//                setCapabilities(JsonFile.WEB_CONFIG);
//                break;
//            default:
//                log.error("No Matching Platform name");
//                break;
//        }
//        return capabilities;
//    }
//
//    /**
//     * Set the Desired Capabilities for initiating the Driver. The System properties passed
//     * overrides the Configuration provided in the Config files.
//     */
//    private static void setCapabilities(final JsonFile jsonFile) {
//        log.info("Setting capabilities from {}", jsonFile);
//        Map<String, Object> configCapabilities =
//                ObjectMapperUtil.toMap(JsonConfigService.get(jsonFile));
//        ObjectMapperUtil.overrideMapWithSysProperties(configCapabilities);
//
//        Map<String, String> additionalProperties =
//                (Map<String, String>) configCapabilities.remove("additional");
//        if (Objects.nonNull(additionalProperties)) {
//            configCapabilities.putAll(additionalProperties);
//        }
//
//        capabilities = new DesiredCapabilities(configCapabilities);
//
//        if (CloudType.SEETEST.equals(AppCache.baseConfig.getCloudType())) {
//            final String sysAccessKeyValue = System.getProperty(ACCESS_KEY);
//            String accessKeyValue =
//                    Strings.isNullOrEmpty(sysAccessKeyValue)
//                            ? AppCache.cloudConfig.getSeetest().getAccessKey()
//                            : sysAccessKeyValue;
//            capabilities.setCapability(ACCESS_KEY, accessKeyValue);
//        }
//    }
//
//    /**
//     * Add Device's UDID to capabilities
//     *
//     * @param udid Device udid
//     * @param overrideWithEnvSettings Overrides the capabilities with env settings app details
//     */
//    public static DesiredCapabilities getCapabilities(
//            final String udid, final boolean overrideWithEnvSettings) {
//        capabilities = getCapabilities();
//        capabilities.setCapability(MobileCapabilityType.UDID, udid);
//
//        if (overrideWithEnvSettings) {
//            capabilities = capabilities.merge(new DesiredCapabilities(environmentProperties()));
//        }
//
//        return capabilities;
//    }
//
//    private static Map<String, Object> environmentProperties() {
//        Map<String, Object> envProperties;
//        if (AppCache.platform.equals(Platform.IOS)) {
//            envProperties = ObjectMapperUtil.toMap(AppCache.appEnvironmentSettings.getIos());
//        } else {
//            envProperties = ObjectMapperUtil.toMap(AppCache.appEnvironmentSettings.getAndroid());
//        }
//        return envProperties;
//    }
}
