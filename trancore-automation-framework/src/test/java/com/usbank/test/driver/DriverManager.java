package com.usbank.test.driver;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.usbank.test.enums.BrowserType;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public abstract class DriverManager<T extends WebDriver> {

    protected static final Logger logger = LogManager.getLogger(DriverManager.class);

    protected T driver;
    protected BrowserType browserType;
    protected RunMode runMode;
    protected Map<String, Map<String, String>> remoteDesiredCapabilities = new HashMap<>();

    public final String proxyAPP = "web-proxyapp.us.bank-dns.com";
    public final String proxyMain = "web-proxymain.us.bank-dns.com";

    public abstract void createDriver();

    public DriverManager() {
        loadDesiredCapabilities();
    }

    public void quitDriver() {
        if (null != driver) {
            driver.quit();
            driver = null;
        }
    }

    public T getDriver() {
        if (null == driver) {
            logger.error("Driver is Null, call create()!!");
            throw new RuntimeException("Driver is null!, call create()!");
        }
        return driver;
    }

    protected String loadProperty(String propertiesFilename, String propertyPath) {
        Properties prop = new Properties();
        String value = "";

        try (InputStream stream = this.getClass().getResourceAsStream(propertiesFilename)) {
            if (stream == null) {
                throw new FileNotFoundException("FileName:Property --->" + propertiesFilename + ":" + propertyPath);
            }
            prop.load(stream);
            value = prop.getProperty(propertyPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (StringUtils.isEmpty(value)) {
            String msg = "Property is empty: + " + propertyPath;
            logger.warn(msg);
        }
        return value;
    }


    protected void loadDesiredCapabilities() {
        //Load Remote Desired Capabilities
        String locationDesiredCaps = "src/test/resources/remoteDesiredCapabilities.json";
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
            File data = new File(locationDesiredCaps);
            TypeReference<HashMap<String, HashMap<String, String>>> typeReference = new TypeReference<HashMap<String, HashMap<String, String>>>() {
            };

            remoteDesiredCapabilities = mapper.readValue(data, typeReference);
        } catch (IOException e) {
            logger.error("Error loading Desired Capabilities File: " + e.getMessage() + locationDesiredCaps, e);
            throw new RuntimeException("Failed to Load Desired Capabilities Data!");
        }

        logger.info("Loaded: " + remoteDesiredCapabilities.size() + " desired caps records from " + locationDesiredCaps);
    }

    public DesiredCapabilities getDesiredCapabilitiesForBrowser(BrowserType browserType) {
        //https://www.browserstack.com/automate/capabilities
        Map<String, String> desiredCapsString = remoteDesiredCapabilities.get(browserType.toString());
        if (MapUtils.isNotEmpty(desiredCapsString)) {
            DesiredCapabilities capabilities = new DesiredCapabilities();
            desiredCapsString.forEach((k, v) -> {
                capabilities.setCapability(k, v);
                logger.info(k + ":" + v);
            });
            return capabilities;
        } else {
            throw new RuntimeException("No matching remote capabilities for:" + browserType);
        }
    }

    public BrowserType getBrowserType() {
        return browserType;
    }

    public void setBrowserType(BrowserType browserType) {
        this.browserType = browserType;
    }

    public Map<String, Map<String, String>> getRemoteDesiredCapabilities() {
        return remoteDesiredCapabilities;
    }

    public RunMode getRunMode() {
        return runMode;
    }

    public void setRunMode(RunMode runMode) {
        this.runMode = runMode;
    }

}
