package com.usbank.test.driver;

import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;

public class IEDriverManager extends DriverManager {
    @Override
    public void createDriver() {
        System.setProperty("webdriver.ie.driver",
                loadProperty("/config.properties", "automation.driver.ie.location"));
        InternetExplorerOptions options = new InternetExplorerOptions();
        options.addCommandSwitches("--start-maximized");
        driver = new InternetExplorerDriver(options);
    }
}
