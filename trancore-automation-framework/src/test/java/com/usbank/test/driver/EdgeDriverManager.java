package com.usbank.test.driver;

import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;

public class EdgeDriverManager extends DriverManager {
    @Override
    public void createDriver() {
        System.setProperty("webdriver.edge.driver",
                loadProperty("/config.properties", "automation.driver.edge.location"));
        EdgeOptions options = new EdgeOptions();

        driver = new EdgeDriver(options);
    }
}
