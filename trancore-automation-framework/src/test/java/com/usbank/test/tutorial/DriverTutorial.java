package com.usbank.test.tutorial;

import com.usbank.test.automationFramework.trancore.pageObjects.PageObjectManager;
import com.usbank.test.component.SpringConfiguration;
import com.usbank.test.component.TestDataManager;
import com.usbank.test.enums.BrowserType;
import com.usbank.test.objects.EnvironmentData;
import io.appium.java_client.ios.IOSDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfiguration.class)
public class DriverTutorial {

    private static final Logger logger = LogManager.getLogger(FrameworkTests.class);

    @Autowired
    private PageObjectManager pageObjectManager;

    @Autowired
    private TestDataManager testDataManager;

    private RemoteSeetest remoteSeetest;

    @Test
    public void testContextLoading() {
        assertNotNull(pageObjectManager);
        assertNotNull(testDataManager);
    }


    @Test
    public void loginWebSeeTestRemoteDriver() {
        remoteSeetest = createSeeTestDriver(BrowserType.CHROME, RunMode.REMOTE_SEETESTBROWSER);

        if (remoteSeetest.getDriver() instanceof RemoteWebDriver) {
            RemoteWebDriver webDriver = (RemoteWebDriver) remoteSeetest.getDriver();
            logger.info(webDriver.getCapabilities().toString());
            Optional<EnvironmentData> trancoreURL = testDataManager.getEnvironmentsByEnvironmentSystem("UAT1", "Elan");
            webDriver.get(trancoreURL.get().getValue());
            new WebDriverWait(webDriver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText("Privacy Policy")));
            webDriver.findElement(By.id("userId")).sendKeys("usb9486");


        } else {
            Assert.fail("RemoteSeeTest not instance of Remote Web Driver!");
        }

        remoteSeetest.getDriver().quit();
    }

    @Test
    public void loginWebSeeTestRemoteDriverPageObject() {

        remoteSeetest = createSeeTestDriver(BrowserType.CHROME, RunMode.REMOTE_SEETESTBROWSER);

        LoginPage loginPage = new LoginPage(remoteSeetest.getDriver());

        if (remoteSeetest.getDriver() instanceof RemoteWebDriver) {
            RemoteWebDriver webDriver = (RemoteWebDriver) remoteSeetest.getDriver();
            logger.info(webDriver.getCapabilities().toString());
            Optional<EnvironmentData> trancoreURL = testDataManager.getEnvironmentsByEnvironmentSystem("UAT1", "Elan");
            webDriver.get(trancoreURL.get().getValue());
            new WebDriverWait(webDriver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText("Privacy Policy")));
            loginPage.loginToLandingPage("usb9486", "testing123");


        } else {
            Assert.fail("RemoteSeeTest not instance of Remote Web Driver!");
        }

        remoteSeetest.getDriver().quit();
    }

    @Test
    public void loginNativeSeeTestIOSDriver() {
        remoteSeetest = createSeeTestDriver(BrowserType.IOS_NATIVE_APPLICATION, RunMode.REMOTE_SEETESTNATIVE);

        if (remoteSeetest.getDriver() instanceof IOSDriver) {
            IOSDriver driver = (IOSDriver) remoteSeetest.getDriver();
            logger.info(driver.getCapabilities().toString());
            driver.rotate(ScreenOrientation.LANDSCAPE);
            driver.findElement(By.partialLinkText("Settings")).click();
            driver.findElement(By.partialLinkText("Cancel")).click();
            new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText("Personal ID")));
            driver.context("WEB");
            driver.findElement(By.id("userId")).sendKeys("usb9486");
            if (!driver.isKeyboardShown()) {
                driver.getKeyboard();
            }


        } else {
            Assert.fail("RemoteSeeTest not instance of IOSDriver!");
        }

        remoteSeetest.getDriver().quit();
    }

    @Test
    public void loginNativeSeeTestRemoteDriverPageObject() {

        remoteSeetest = createSeeTestDriver(BrowserType.IOS_NATIVE_APPLICATION, RunMode.REMOTE_SEETESTNATIVE);

        LoginPage loginPage = new LoginPage(remoteSeetest.getDriver());

        if (remoteSeetest.getDriver() instanceof RemoteWebDriver) {
            IOSDriver webDriver = (IOSDriver) remoteSeetest.getDriver();
            webDriver.findElement(By.id("Settings")).click();
            webDriver.findElement(By.id("UAT 3")).click();
            webDriver.context("WEB");
            logger.info(webDriver.getCapabilities().toString());
            loginPage.loginToLandingPage("losusbuat0952", "testing123");


        } else {
            Assert.fail("RemoteSeeTest not instance of Remote Web Driver!");
        }

        remoteSeetest.getDriver().quit();
    }

    public RemoteSeetest createSeeTestDriver(BrowserType browserType, RunMode runMode) {
        RemoteSeetest r = new RemoteSeetest();
        r.setBrowserType(browserType);
        r.setRunMode(runMode);
        r.createDriver();
        r.getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return r;
    }
}
