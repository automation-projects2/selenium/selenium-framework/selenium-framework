package com.usbank.test.automationFramework.trancore.pageObjects.services;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.usbank.test.driver.DriverManager;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class LostOrStolenPage<T extends WebDriver> {

    //This is to click on SERVICES Button for ABC Partner
    //@FindBy(how = How.XPATH, using = "//span[contains(text(),'Services')]")

    //This is to click on SERVICES Button for Fidelity
    //@FindBy(how = How.LINK_TEXT, using = "Services")

    //This is to click on SERVICES Button for Elan, EJ, & ACG
    private static final long EXPLICIT_WAIT_TIME_SECONDS = 30;
    @FindBy(how = How.LINK_TEXT, using = "SERVICES")
    private WebElement lnkServicesPage;

    @FindBy(how = How.ID, using = "otpAndContinue")
    private WebElement otpButtonContinue;
    @FindBy(how = How.ID, using = "radioButton0")
    private WebElement radiobuttonYes;
    @FindBy(how= How.NAME,using = "cardDeliveryRadio")
    private WebElement radioButtonStandardDelivery;
    @FindBy(how = How.ID, using = "sendOTP")
    private WebElement linkSendOTP;
    @FindBy(how = How.ID, using = "otp-value")
    private WebElement textfieldEnterOTP;
    @FindBy(how = How.LINK_TEXT, using = "Report card lost or stolen")
    private WebElement lnkReportCardLostOrStolen;

    @FindBy(how = How.ID, using = "reviewRecentContinue")
    private WebElement reviewRecentContinue;

    //@FindBy(how = How.XPATH, using = "//button[@name='Continue']")
    //@FindBy(how = How.XPATH, using = "//button[@name='Continue' or @name='continue']")
    private WebElement btnContinue;

    @FindBy(how = How.XPATH, using = "//button[@id='sendOTP']")
    private WebElement btnSendOtpCode;

    @FindBy(how = How.XPATH, using = "//button[@id='otpAndContinue']")
    private WebElement btnOtppasscodeContinue;

    @FindBy(how = How.XPATH, using = "//input[@data-testid='OTPInput']")
    private WebElement txtOtppasscode;

    //@FindBy(how = How.ID, using = "checkbox0")
    @FindBy(how = How.XPATH, using = "//input[@id='radioButton0']")
    private WebElement rbYesIRecognizeAllThesePurchases;

    @FindBy(how = How.CSS, using = "#reviewRecentContinue")
    private WebElement btnContinueOnReviewRecentTransactions;

    @FindBy(how = How.ID, using = "missingDate")
    private WebElement txtMissingDate;

    //@FindBy(how = How.CSS, using = "div.elan-app div.middleWrapper:nth-child(3) div.layoutPageContent2:nth-child(2) div.layoutPageContent3 div.tc_page_shadow div.lostStolenAdditionalDetails div.singleBlock div:nth-child(5) div:nth-child(1) div.buttonnav.marginButtonNav:nth-child(4) > button.tranCoreButton.buttonpad.omv_button--full.omv_fullbutton.new_omvButton:nth-child(1)")
    //private WebElement btnContinueOnAdditionalDetails;

    @FindBy(how = How.XPATH, using = " //button[@id='quickQuestionContinue']")
    private WebElement btnContinueOnAdditionalDetails;

    @FindBy(how = How.ID, using = "0")
    private WebElement rbStandardDelivery;
    @FindBy(how=How.ID, using = "cardDeliveryContinue")
    private WebElement cardDeliveryContinue;
    @FindBy(how = How.XPATH, using = "//button[@name='Continue']")
    private WebElement btnContinueOnCardDeliveryPage;
    @FindBy(how = How.NAME,using = "continue")
    private WebElement reviewAndSubmitContinue;
    @FindBy(how = How.XPATH, using = "//button[@name='Continue']")
    private WebElement btnReportCardLostOrStolen;

    @FindBy(how = How.XPATH, using = "//button[@name='Back']")
    private WebElement btnReturnToServices;

    @FindBy(how = How.ID, using = "quickQuestionContinue")
    private WebElement btnContinueQuickQuestionPage;
    @FindBy(xpath = "//a[text()='Card activation']")
    private WebElement linkCardActivation;

    @FindBy(id= "last4SSN-tel-input")
    private WebElement txtLast4digitSSN;

    @FindBy(id = "securityCode-tel-input")
    private WebElement txtSecurityCode;

    @FindBy (xpath = "//button[text()='Submit']")
    private WebElement btnSubmit;
    @FindBy(how = How.ID, using = "tc_HeaderLogout")
    private WebElement linkLogout;
    @FindBy(xpath="//h2[@class='ca-contentAreaSubHeader']")
    private WebElement cardSuccessmsg;

    @FindBy(how = How.XPATH, using = "//button[text()='Close']")
    private WebElement btnClose;
    @FindBy(how = How.ID, using = "lostStolenContinue")
    private WebElement linkContinue;

    @FindBy(how = How.CLASS_NAME, using = "errortext")
    private WebElement errorTextQuickQtn;

    Alert alert;
    private T driver;

    public LostOrStolenPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(inDriver, this);
    }

    public void clickOnServicesPage() {
        lnkServicesPage.click();
    }

    public void clickOnReportCardLostOrStolen() {
        lnkReportCardLostOrStolen.click();
    }

    public void clickOnContinue() {
        btnContinue.click();
    }

    public void selectYesIRecognizeAllThesePurchases() {
        rbYesIRecognizeAllThesePurchases.click();
    }


    public void clickOnContinueButtonReviewTransactionsPage() {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        btnContinueOnReviewRecentTransactions.click();
    }

    public void enterMissingDate(String missingDate) {
        txtMissingDate.sendKeys(missingDate);
    }

    //public void clickContinueQuickQuestionPage(){
      //  btnContinueQuickQuestionPage.click();
    //}

    public void clickOnContinueButtonAdditionalDetails() {
        btnContinueOnAdditionalDetails.click();
    }
    public void clickOnStandDelivery() {
        radioButtonStandardDelivery.click();
    }

    public void captureReviewAndSubmitRequestDetails() {
        String heading = driver.findElement(By.xpath("//span[contains(text(),'Review and submit your request')]")).getText();
        System.out.println(heading);

        String info = driver.findElement(By.xpath("//span[contains(text(),'Please make sure this information is correct befor')]")).getText();
        System.out.println(info);

        String deliveryOpt = driver.findElement(By.xpath("//b[contains(text(),'Delivery option')]")).getText();
        System.out.println(deliveryOpt);

        String expectationDelivery = driver.findElement(By.xpath("//div[contains(text(),'You can expect your card(s) within 10 days')]")).getText();
        System.out.println(expectationDelivery);

        String newCardInfo = driver.findElement(By.xpath("//b[contains(text(),'The new card(s) will be sent to:')]")).getText();
        System.out.println(newCardInfo);

        String cardAddress = driver.findElement(By.xpath("//div[@class='tieringElement']//div[1]")).getText();
        System.out.println(cardAddress);

        String lastInfo = driver.findElement(By.xpath("//div[7]//span[1]")).getText();
        System.out.println(lastInfo);
    }
    public void clickOnCardActivationLink() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(linkCardActivation));
        linkCardActivation.click();
    }
    public void enrollmentPageofCardActivation(String ssn, String cvv){
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(txtLast4digitSSN));
        txtLast4digitSSN.sendKeys(ssn);
        txtSecurityCode.sendKeys(cvv);
        btnSubmit.click();
    }
    public void verifiesCardSuccessMsg(String successmessage){
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(cardSuccessmsg));
        String getCardSuccessMessage = cardSuccessmsg.getText();
        System.out.println("The success message is: "+getCardSuccessMessage);
        Assert.assertEquals(successmessage,getCardSuccessMessage);

    }
    public void verifyCardActivationStatus(){
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnClose));
        btnClose.click();
    }
    public void clickOnReportCardLostOrStolenButton() {
        btnReportCardLostOrStolen.click();
    }
    public void clickContinueQuickQuestionPage(){btnContinueQuickQuestionPage.click();}

    public void clickOnReturnToServicesButton() {
        btnReturnToServices.click();
    }
    public void reportLostAndStolenCard(String Value, String passcode, String missingDate) throws InterruptedException {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkReportCardLostOrStolen));
        lnkReportCardLostOrStolen.click();
        linkContinue.click();

            if (!Value.equals("SingleContact")) {
            String type;
            if (Value.matches("[0-9]+")) {
                type = "OnlyNumbers";
            } else {
                type = "emailonly";
            }
            System.out.println("Type is : "+type);
            List<WebElement> selectRadioButton = driver.findElements(By.className("label__container"));
            switch (type) {
                case ("OnlyNumbers"): {
                    for (int i = 0; i < selectRadioButton.size(); i++) {
                        if (selectRadioButton.get(i).getText().equals("Text: ***-***-" + Value)) {
                            selectRadioButton.get(i).click();
                            break;
                        }
                    }
                    break;
                }
                case ("emailonly"): {
                        for (int i = 0; i < selectRadioButton.size(); i++) {
                        System.out.println("Email is first: "+selectRadioButton.get(i).getText());
                        Thread.sleep(15000);
                        if (selectRadioButton.get(i).getText().equals("Email: " + Value)) {
                            System.out.println("Email is: "+selectRadioButton.get(i).getText());
                            selectRadioButton.get(i).click();
                            break;
                        }
                    }
                    break;
                }

            }

        }
        linkSendOTP.click();
        textfieldEnterOTP.clear();
        textfieldEnterOTP.sendKeys(passcode);
        otpButtonContinue.click();
        reviewRecentContinue.click();
        radiobuttonYes.click();
        reviewRecentContinue.click();
        txtMissingDate.sendKeys(missingDate);
        btnContinueQuickQuestionPage.click();
        radioButtonStandardDelivery.click();
        cardDeliveryContinue.click();
        reviewAndSubmitContinue.click();
        linkLogout.click();
    }

    public void verifyErrortextQuickQuestion() {
        //assertEquals("It looks like you didn't give us a date. Please enter a date before continuing.", errorTextQuickQtn.getText());
    }

    public void clickSendOtpPasscodeButton() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(btnSendOtpCode));
        btnSendOtpCode.click();
    }

    public void clickOnContinueOtpPasscodeButton() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(txtOtppasscode));
        txtOtppasscode.sendKeys("111111");
        btnOtppasscodeContinue.click();
    }
}
