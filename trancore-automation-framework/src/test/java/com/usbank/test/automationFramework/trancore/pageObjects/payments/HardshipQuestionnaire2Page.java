package com.usbank.test.automationFramework.trancore.pageObjects.payments;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HardshipQuestionnaire2Page<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(HardshipQuestionnaire2Page.class);
    private T driver;
    private WebDriverWait wait;

    @FindBy(xpath = "//h1[@data-test='page-header']")
    private WebElement questionnaire2PageHeader;

    @FindBy(id = "recommendPlanParagraph")
    private WebElement toRecommendAPlanParagraph;

    @FindBy(name = "currentMonthlyIncome")
    private WebElement currentMonthlyIncome;

    @FindBy(id = "disclosureParagraph")
    private WebElement disclosureForMonthlyIncome;

    @FindBy(name = "currentMonthlyExpenses")
    private WebElement currentMonthlyExpenses;

    @FindBy(className = "helperText")
    private WebElement helperTextForMonthlyExpenses;

    @FindBy(className = "questionnaire-select-class")
    private WebElement reasonForHardship;

    // Continue and Cancel page objects will be re-used from Questionnaire1 page

    public HardshipQuestionnaire2Page(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 30);
    }

    public boolean verifyQuestionnaire2PageTitle() {
        wait.until(ExpectedConditions.visibilityOf(questionnaire2PageHeader));
        logger.info("Questionnaire2 page title is as expected: " + driver.getTitle());
        return !driver.getTitle().equals("Credit Card Account Access: Questions");
    }

    public boolean verifyQuestionnaire2PageHeaderDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(questionnaire2PageHeader));
        logger.info("Questionnaire2 page header displayed: " + questionnaire2PageHeader.getText());
        return questionnaire2PageHeader.getText().equals("Tell us more about your hardship.");
    }

    public boolean currentMonthlyIncomeFieldISDisplayed() {
        logger.info("Current Monthly Income field displayed: " + currentMonthlyIncome.getText());
        return currentMonthlyIncome.getText().equals("Current monthly income");
    }

    public boolean currentMonthlyExpensesFieldISDisplayed() {
        logger.info("Current Monthly Income field displayed: " + currentMonthlyExpenses.getText());
        return currentMonthlyExpenses.getText().equals("Current monthly expenses");
    }
    public WebElement inputCurrentMonthlyIncome(){    return currentMonthlyIncome;    }
    public WebElement inputCurrentMonthlyExpenses() {
        return currentMonthlyExpenses;
    }

    public Select selectHardshipReason() {
        return new Select(reasonForHardship);
    }
    public boolean isRecommendPlanParagraphDisplayed(){

        wait.until(ExpectedConditions.visibilityOf(toRecommendAPlanParagraph));
        return toRecommendAPlanParagraph.isDisplayed();
    }
}
