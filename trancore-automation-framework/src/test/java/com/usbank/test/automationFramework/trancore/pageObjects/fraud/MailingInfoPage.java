package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MailingInfoPage<T extends WebDriver> {

    protected static final Logger logger = LogManager.getLogger(MailingInfoPage.class);

    protected T driver;


    public MailingInfoPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement btnContinue;

    @FindBy(xpath = "//*[text()='Where would you like us to mail updates?']")
    private WebElement lblMailingInfo;

    @FindBy(id = "phoneNumber")
    private WebElement txtDefaultPhoneNbr;

    @FindBy(id = "radioAltAddress")
    private WebElement rdbAlternateAddress;

    @FindBy(id = "addresseeName")
    private WebElement sddAddresseeName;

    @FindBy(id = "mailingAddressLine1")
    private WebElement txtMailingAddress;

    @FindBy(id = "mailingCity")
    private WebElement txtMailingCity;

    @FindBy(id = "mailingState")
    private WebElement sddMailingState;

    @FindBy(id = "mailingZipFive")
    private WebElement txtMailingZipFive;

    @FindBy(id = "altPhoneNumber")
    private WebElement txtPhoneNumber;

    @FindBy(id = "validUntil")
    private WebElement txtValidUntil;

    public WebElement getLblMailingInfo() {
        return lblMailingInfo;
    }

    public void waitForMailingInfoPageToLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(lblMailingInfo));
    }

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        ele.click();
    }

    public void clickContinue() {
        clickElement(driver, btnContinue);
    }

    public WebElement getRdbAlternateAddress() {
        return rdbAlternateAddress;
    }

    public WebElement getSddAddresseeName() {
        return sddAddresseeName;
    }

    public WebElement getTxtMailingAddress() {
        return txtMailingAddress;
    }

    public WebElement getTxtMailingCity() {
        return txtMailingCity;
    }

    public WebElement getSddMailingState() {
        return sddMailingState;
    }

    public WebElement getTxtMailingZipFive() {
        return txtMailingZipFive;
    }

    public WebElement getTxtPhoneNumber() {
        return txtPhoneNumber;
    }

    public WebElement getTxtValidUntil() {
        return txtValidUntil;
    }

    public WebElement getTxtDefaultPhoneNbr() {
        return txtDefaultPhoneNbr;
    }
}
