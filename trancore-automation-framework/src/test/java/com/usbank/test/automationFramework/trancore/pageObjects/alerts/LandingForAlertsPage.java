package com.usbank.test.automationFramework.trancore.pageObjects.alerts;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class LandingForAlertsPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(LandingForAlertsPage.class);

    private T driver;
    private int implicitTimeout = 00;

    @FindBy(how = How.LINK_TEXT, using = "ALERTS")
    private WebElement linkAlertsPage;

    @FindBy(how = How.XPATH, using = "//a[@title='Sign up now']")
    private WebElement signUpNowLinkAlertPage;

    @FindBy(how=How.XPATH,using = "//a[text()='Alerts History' or text()='ALERTS HISTORY']")
    private WebElement alertHistoryLinkAlertPage;

    @FindBy(how=How.XPATH,using="//a[@title='Manage Alerts  ']")
    private WebElement manageAlertlink;

    @FindBy(how=How.NAME,using="group1")
    private WebElement statusAlertPage;

    @FindBy(how = How.XPATH, using = "//a[@title='See Agreement' or @title='View Alert Service Agreement']")
    private WebElement seeAgreementLinkAlertPage;

    @FindBy(how = How.XPATH, using = "//button[@name='Cancel']")
    private WebElement cancelButtonSignUpNowLinkAlertPage;

    @FindBy(how = How.XPATH, using = "//button[@name='Back']")
    private WebElement backButtonAlertHistoryLinkAlertPage;

    @FindBy(how = How.XPATH, using = "//button[@value='Close Window']")
    private WebElement closeWindowSeeAgreementLinkAlertPage;

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Account Alerts')]")
    private WebElement accountAlertsTabAlertPage;

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Security Alerts')]")
    private WebElement securityAlertsTabAlertPage;

    @FindBy(how = How.XPATH, using = "//tr[@data-testid='groupHeaderBalanceAlerts']")
    private WebElement balanceAlerts;

    @FindBy(how = How.XPATH, using = "//tr[@data-testid='groupHeaderTransactionAlerts']")
    private WebElement transcationAlerts;

    @FindBy(how = How.XPATH, using = "//tr[@data-testid='groupHeaderStatementandPaymentAlerts']")
    private WebElement statementPaymentAlerts;

    @FindBy(how = How.XPATH, using = "//tr[@id='alert_15']/td/a/div")
    private WebElement availblecreditLinkAlert;

    @FindBy(how = How.XPATH, using = "//tr[@id='alert_1']/td/a/div")
    private WebElement balanceexceedsLinkAlert;

    @FindBy(how = How.XPATH, using = "//tbody/tr[2]//td[3]//a")
    private WebElement setUpLink;

    @FindBy(how = How.XPATH, using = "//input[@id='acr_15']")
    private WebElement alertAmount;

    @FindBy(how = How.XPATH, using = "//ol[@class='alert_content']//li[1]//span[1]//select[1]")
    private WebElement selectAlertEmailId;

    @FindBy(how = How.ID, using = "setup_addr_email_1_15")
    private WebElement selectAlertEmailValue;

    @FindBy(how = How.XPATH, using = "//input[@type='text']")
    private WebElement enterAlertEmailValue;

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Save')]")
    private WebElement saveButton;

    @FindBy(how = How.XPATH, using = "//tr[@id='alert_15']/td[3]/div/a[contains(text(),'Edit')]")
    private WebElement editLinkAvailableCredit;
    // alert setup success message
    @FindBy(how = How.XPATH, using = " //div[@id='layoutContentBody']//div[contains(text(),'set up')]")
    private WebElement setupSuccessActualText;
    //alert edit success message
    @FindBy(how = How.XPATH, using = "//div[@id='layoutContentBody']//div[contains(text(),'updated')]")
    private WebElement editSuccessActualText;
    //Security alert success message
    @FindBy(how = How.XPATH, using = "//div[@id='layoutContentBody']//div[contains(text(),'successfully')]")
    private WebElement SecuritySuccessActualText;
    //alert delete success message
   /* @FindBy(how = How.XPATH, using = "//div[@id='layoutContentBody']//div[contains(text(),'deleted')]")
    private WebElement deleteSuccessActualText;*/
    @FindBy(how=How.XPATH,using="//div[@aria-live='polite']//div[contains(text(),'deleted')] ")
    private WebElement deleteSuccessActualText;

    @FindBy(how = How.ID, using = "security_save")
    private WebElement securitySaveButton;
    @FindBy(how = How.ID, using = "setup_addr_type_1_16")
    private WebElement securityContactSelectdropdown;
    @FindBy(how = How.ID, using = "setup_addr_email_1_16")
    private WebElement SelectSecurityContactEmailvalue;
    @FindBy(how = How.ID, using = "addr_value_1_16")
    private WebElement SecurityContactEmailTextbox;

    @FindBy(how = How.XPATH, using = "//tr[@data-testid='groupHeaderStatementandPaymentAlerts']/td")
    private WebElement statementAndPaymentLink;

    //AutoPay Scheduled and AutoPay Processed
    @FindBy(how = How.XPATH, using = "//*[@id='href_alert_5' or @id='alert_5']")
    private WebElement AutoPayScheduledandAutoPayProcessedlink;
    @FindBy(how = How.ID, using = "href_setup_5")
    private WebElement setUpLinkForAutoPayScheduledandAutoPayProcessed;
    @FindBy(how=How.ID, using="setup_addr_type_1_5")
    private WebElement AutoPayScheduledandAutoPayProcessedSelectdropdown;
    @FindBy(how = How.ID, using = "setup_addr_email_1_5")
    private WebElement AutoPayScheduledandAutoPayProcessedContactEmailvalue;
    @FindBy(how = How.ID, using = "addr_value_1_5")
    private WebElement AutoPayScheduledandAutoPayProcessedContactEmailTextbox;
    @FindBy(how=How.ID,using="edit_save_5")
    private WebElement AutoPayScheduledandAutoPayProcessedSaveButton;

    //Online statements available
    @FindBy(how=How.XPATH,using = "//*[@id='href_alert_0' or @id='alert_0']")
    private WebElement onlineStatmentAvailablelink;
    @FindBy(how = How.ID, using = "href_setup_0")
    private WebElement setUpLinkForOnlineStatmentAvailable;
    @FindBy(how=How.ID, using="setup_addr_type_1_0")
    private WebElement onlineStatmentAvailableSelectdropdown;
    @FindBy(how = How.ID, using = "setup_addr_email_1_0")
    private WebElement onlineStatmentAvailableContactEmailvalue;
    @FindBy(how = How.ID, using = "setup_addr_email_1_0")
    private WebElement onlineStatmentAvailableContactEmailTextbox;
    @FindBy(how=How.ID,using="edit_save_0")
    private WebElement onlineStatmentAvailableSaveButton;

    //Payment Due
    @FindBy(how = How.XPATH, using = "//*[@id='href_alert_2' or @id='alert_2']")
    private WebElement paymentDuelink;
    @FindBy(how = How.XPATH, using = "href_setup_2")
    private WebElement setUpLinkForpaymentDue;
    @FindBy(how=How.ID,using = "due_2")
    private WebElement numberOfDaysPriorToDueDateSelectdropdown;
    @FindBy(how=How.ID, using="setup_addr_type_1_2")
    private WebElement paymentDueSelectdropdown;
    @FindBy(how = How.ID, using = "setup_addr_email_1_2")
    private WebElement paymentDueContactEmailvalue;
    @FindBy(how = How.ID, using = "addr_value_1_2")
    private WebElement paymentDueContactEmailTextbox;
    @FindBy(how=How.ID,using="edit_save_2")
    private WebElement paymentDueSaveButton;

    //Payment Overdue
    @FindBy(how = How.XPATH, using = "//*[@id='href_alert_4' or @id='alert_4']")
    private WebElement paymentOverduelink;
    @FindBy(how = How.ID, using = "href_setup_4")
    private WebElement setUpLinkForpaymentOverdue;
    @FindBy(how=How.ID, using="setup_addr_type_1_4")
    private WebElement paymentOverdueSelectdropdown;
    @FindBy(how = How.ID, using = "setup_addr_email_1_4")
    private WebElement paymentOverdueContactEmailvalue;
    @FindBy(how = How.ID, using = "addr_value_1_4")
    private WebElement paymentOverdueContactEmailTextbox;
    @FindBy(how=How.ID,using="edit_save_4")
    private WebElement paymentOverdueSaveButton;

    //Payment Posted
    @FindBy(how = How.XPATH, using = "//*[@id='href_alert_3' or @id='alert_3']")
    private WebElement paymentPostedlink;
    @FindBy(how = How.ID, using = "href_setup_3")
    private WebElement setUpLinkForpaymentPosted;
    @FindBy(how=How.ID, using="setup_addr_type_1_3")
    private WebElement paymentPostedSelectdropdown;
    @FindBy(how = How.ID, using = "setup_addr_email_1_3")
    private WebElement paymentPostedContactEmailvalue;
    @FindBy(how = How.ID, using = "addr_value_1_3")
    private WebElement paymentPostedContactEmailTextbox;
    @FindBy(how=How.ID,using="edit_save_3")
    private WebElement paymentPostedSaveButton;

    //Payment schedule
    @FindBy(how = How.XPATH, using = "//*[@id='href_alert_6' or @id='alert_6']")
    private WebElement paymentSchedulelink;
    @FindBy(how = How.ID, using = "href_setup_6")
    private WebElement setUpLinkForpaymentSchedule;
    @FindBy(how=How.ID, using="setup_addr_type_1_6")
    private WebElement paymentScheduleSelectdropdown;
    @FindBy(how = How.ID, using = "setup_addr_email_1_6")
    private WebElement paymentScheduleContactEmailvalue;
    @FindBy(how = How.ID, using = "addr_value_1_6")
    private WebElement paymentScheduleContactEmailTextbox;
    @FindBy(how=How.ID,using="edit_save_6")
    private WebElement paymentScheduleSaveButton;

    //Edit link for AutoPay Scheduled and AutoPay Processed
    @FindBy(how = How.ID, using = "href_edit_5")
    private WebElement editLinkForAutoPayScheduledandAutoPayProcessed;
    //Edit link for Online statements available
    @FindBy(how = How.ID, using = "href_edit_0")
    private WebElement editLinkForOnlineStatmentAvailable;
    //Edit link for Payment Due
    @FindBy(how = How.ID, using = "href_edit_2")
    private WebElement editLinkForpaymentDue;
    //Edit link for Payment Overdue
    @FindBy(how = How.ID, using = "href_edit_4")
    private WebElement editLinkForpaymentOverdue;
    //Edit link for payment Posted
    @FindBy(how = How.ID, using = "href_edit_3")
    private WebElement editUpLinkForpaymentPosted;
    //Edit link for Payment schedule
    @FindBy(how = How.ID, using = "href_edit_6")
    private WebElement editLinkForpaymentSchedule;

    // links for Subalerts
    @FindBy(how = How.XPATH, using = "//tbody/tr[@id='alert_11']/td/div/a")
    private WebElement atmTranscationAlertSetupLink;
    @FindBy(how = How.XPATH, using = " //tbody/tr[@id='alert_10']/td/div/a")
    private WebElement cardNotPresentAlertSetupLink;
    @FindBy(how = How.XPATH, using = "//tbody/tr[@id='alert_7']/td/div/a")
    private WebElement creditPostedAlertSetupLink;
    @FindBy(how = How.XPATH, using = "//tbody/tr[@id='alert_8']/td/div/a")
    private WebElement deditPostedAlertSetupLink;
    @FindBy(how = How.XPATH, using = "//tbody/tr[@id='alert_12']/td/div/a")
    private WebElement declineTranscationAlertSetupLink;
    @FindBy(how = How.XPATH, using = "//tbody/tr[@id='alert_13']/td/div/a")
    private WebElement gasStationPurchaseAlertSetupLink;
    @FindBy(how = How.XPATH, using = "//tbody/tr[@id='alert_14']/td/div/a")
    private WebElement internationaTranscationAlertSetupLink;
    @FindBy(how = How.XPATH, using = "//tbody/tr[@id='alert_9']/td/div/a")
    private WebElement transcationNotificationAlertSetupLink;

    //Available credit
    @FindBy(how = How.ID ,using = "href_setup_15")
    private WebElement  availbleCreditAlertSetUpLink;
    @FindBy(how = How.ID, using = "acr_15")
    private WebElement  availbleCreditAlertAmount;
    @FindBy(how = How.XPATH, using = "//ol[@class='alert_content']//li[1]//span[1]//select[1]")
    private WebElement  availbleCreditEmailDropdown;
    @FindBy(how = How.ID, using = "setup_addr_email_1_15")
    private WebElement  availbleCreditAlertEmailValue;
    @FindBy(how = How.XPATH, using = "//input[@type='text']")
    private WebElement  availbleCreditEmailTextBox;
    @FindBy(how = How.ID, using = "edit_save_15")
    private WebElement availbleCreditsaveButton;
    @FindBy(how = How.ID, using = "href_edit_15")
    private WebElement availableCreditEditLink;

    //Balance Exceeds
    @FindBy(how = How.ID, using = "bal_1")
    private WebElement balanceExceedsamount;
    @FindBy(how = How.ID, using = "href_setup_1")
    private WebElement balanceExceedsSetupLink;
    @FindBy(how = How.ID, using = "setup_addr_type_1_1")
    private WebElement balanceExceedsContactSelectdropdown;
    @FindBy(how = How.ID, using = "setup_addr_email_1_1")
    private WebElement selectBalanceExceedsEmailvalue;
    @FindBy(how = How.ID, using = "addr_value_1_1")
    private WebElement balanceExceedsContactEmailTextbox;
    @FindBy(how = How.ID, using = "edit_save_1")
    private WebElement balanceExceedsSaveButton;
    @FindBy(how = How.ID, using = "href_edit_1")
    private WebElement balanceExceedsEditLink;

    //ATM transcation
    @FindBy(how = How.ID, using = "setup_addr_type_1_11")
    private WebElement atmTranscationContactSelectdropdown;
    @FindBy(how = How.ID, using = "setup_addr_email_1_11")
    private WebElement selectAtmTranscationEmailvalue;
    @FindBy(how = How.ID, using = "addr_value_1_11")
    private WebElement atmTranscationContactEmailTextbox;
    @FindBy(how = How.ID, using = "edit_save_11")
    private WebElement atmTranscationSaveButton;
    @FindBy(how = How.ID, using = "href_edit_11")
    private WebElement atmTranscationEditLink;

    //CardNotPresent
    @FindBy(how = How.ID, using = "setup_addr_type_1_10")
    private WebElement cardNotPresentContactSelectdropdown;
    @FindBy(how = How.ID, using = "setup_addr_email_1_10")
    private WebElement selectCardNotPresentEmailvalue;
    @FindBy(how = How.ID, using = "addr_value_1_10")
    private WebElement cardNotPresentContactEmailTextbox;
    @FindBy(how = How.ID, using = "edit_save_10")
    private WebElement cardNotPresentSaveButton;
    @FindBy(how = How.ID, using = "href_edit_10")
    private WebElement cardNotPresentEditLink;

    //Credit Posted
    @FindBy(how = How.ID, using = "setup_addr_type_1_7")
    private WebElement creditPostedContactSelectdropdown;
    @FindBy(how = How.ID, using = "setup_addr_email_1_7")
    private WebElement selectCreditPostedEmailvalue;
    @FindBy(how = How.ID, using = "addr_value_1_7")
    private WebElement creditPostedContactEmailTextbox;
    @FindBy(how = How.ID, using = "edit_save_7")
    private WebElement creditPostedSaveButton;
    @FindBy(how = How.ID, using = "href_edit_7")
    private WebElement creditPostedEditLink;

    //Debit Posted
    @FindBy(how = How.ID, using = "setup_addr_type_1_8")
    private WebElement debitPostedContactSelectdropdown;
    @FindBy(how = How.ID, using = "setup_addr_email_1_8")
    private WebElement selectDebitPostedEmailvalue;
    @FindBy(how = How.ID, using = "addr_value_1_8")
    private WebElement debitPostedContactEmailTextbox;
    @FindBy(how = How.ID, using = "edit_save_8")
    private WebElement debitPostedSaveButton;
    @FindBy(how = How.ID, using = "href_edit_9")
    private WebElement debitPostedEditLink;
    @FindBy(how = How.XPATH, using = "//input[@type='radio']")
    private WebElement selectDebitPostedAmount;
    @FindBy(how = How.XPATH, using = " //input[@id='debitAmount']")
    private WebElement addDebitPostedAmount;

    //Decline Transcation
    @FindBy(how = How.ID, using = "setup_addr_type_1_12")
    private WebElement declineTrascationContactSelectdropdown;
    @FindBy(how = How.ID, using = "setup_addr_email_1_12")
    private WebElement selectDeclineTrascationEmailvalue;
    @FindBy(how = How.ID, using = "addr_value_1_12")
    private WebElement declineTrascationContactEmailTextbox;
    @FindBy(how = How.ID, using = "edit_save_12")
    private WebElement declineTrascationSaveButton;
    @FindBy(how = How.ID, using = "href_edit_12")
    private WebElement declineTrascationEditLink;

    //Gas Station Purchase
    @FindBy(how = How.ID, using = "setup_addr_type_1_13")
    private WebElement gasStationPurchaseContactSelectdropdown;
    @FindBy(how = How.ID, using = "setup_addr_email_1_13")
    private WebElement selectGasStationPurchaseEmailvalue;
    @FindBy(how = How.ID, using = "addr_value_1_13")
    private WebElement gasStationPurchaseContactEmailTextbox;
    @FindBy(how = How.ID, using = "edit_save_13")
    private WebElement gasStationPurchaseSaveButton;
    @FindBy(how = How.ID, using = "href_edit_13")
    private WebElement gasStationPurchaseEditLink;

    //International Transcation
    @FindBy(how = How.ID, using = "setup_addr_type_1_14")
    private WebElement internationalTranscationalContactSelectdropdown;
    @FindBy(how = How.ID, using = "setup_addr_email_1_14")
    private WebElement selectInternationalTranscationalEmailvalue;
    @FindBy(how = How.ID, using = "addr_value_1_14")
    private WebElement internationalTranscationaleContactEmailTextbox;
    @FindBy(how = How.ID, using = "edit_save_14")
    private WebElement internationalTranscationalSaveButton;
    @FindBy(how = How.ID, using = "href_edit_14")
    private WebElement internationalTranscationalEditLink;

    //Notification Alert
    @FindBy(how = How.ID, using = "setup_addr_type_1_9")
    private WebElement notificationContactSelectdropdown;
    @FindBy(how = How.ID, using = "setup_addr_email_1_9")
    private WebElement selectNotificationEmailvalue;
    @FindBy(how = How.ID, using = "addr_value_1_9")
    private WebElement notificationContactEmailTextbox;
    @FindBy(how = How.ID, using = "edit_save_9")
    private WebElement notificationSaveButton;
    @FindBy(how = How.ID, using = "dlr_9")
    private WebElement notificationAmount;
    @FindBy(how = How.ID, using = "href_edit_9")
    private WebElement notificationEditLink;

    //delete for AutoPayScheduled and AutoPayProcessed
    @FindBy(how = How.ID, using = "href_delete_5")
    private WebElement deleteLinkForAutoPayScheduledandAutoPayProcessed;
    @FindBy(how=How.XPATH,using="//*[@id='alert-5']//button[contains(text(),'Continue')]")
    private WebElement deleteContinuebuttonForAutoPayScheduledandAutoPayProcessed;
    //delete for OnlineStatment Available
    @FindBy(how = How.ID, using = "href_delete_0")
    private WebElement deleteLinkForOnlineStatementAvailable;
    @FindBy(how=How.XPATH,using="//*[@id='alert-0']//button[contains(text(),'Continue')]")
    private WebElement deleteContinuebuttonForOnlineStatementAvailable;
    //delete for Payment Due
    @FindBy(how = How.ID, using = "href_delete_2")
    private WebElement deleteLinkPaymentDue;
    @FindBy(how=How.XPATH,using="//*[@id='alert-2']//button[contains(text(),'Continue')]")
    private WebElement deleteContinuebuttonForPaymentDue;
    //delete for Payment overdue
    @FindBy(how = How.ID, using = "href_delete_4")
    private WebElement deleteLinkForPaymentOverdue;
    @FindBy(how=How.XPATH,using="//*[@id='alert-4']//button[contains(text(),'Continue')]")
    private WebElement deleteContinuebuttonForPaymentOverdue;
    //delete for Payment Posted
    @FindBy(how = How.ID, using = "href_delete_3")
    private WebElement deleteLinkForPaymentPosted;
    @FindBy(how=How.XPATH,using="//*[@id='alert-3']//button[contains(text(),'Continue')]")
    private WebElement deleteContinuebuttonForPaymentPosted;
    //Payment schedule
    @FindBy(how = How.ID, using = "href_delete_6")
    private WebElement deleteLinkForPaymentSchedule;
    @FindBy(how=How.XPATH,using="//*[@id='alert-6']//button[contains(text(),'Continue')]")
    private WebElement deleteContinuebuttonForPaymentSchedule;

    //For retriving the card status
    @FindBy(how=How.XPATH,using="//*[@id='alert_5']//span")
    private WebElement getStatusAutopaySheduledAndProcessed;
    @FindBy(how=How.XPATH,using="//*[@id='alert_0']//span")
    private WebElement getStatusOnlineStatementAvailable;
    @FindBy(how=How.XPATH,using="//*[@id='alert_2']//span")
    private WebElement getStatusPaymentDue;
    @FindBy(how=How.XPATH,using="//*[@id='alert_4']//span")
    private WebElement getStatusPaymentOverdue;
    @FindBy(how=How.XPATH,using="//*[@id='alert_3']//span")
    private WebElement getStatusPaymentPosted;
    @FindBy(how=How.XPATH,using="//*[@id='alert_6']//span")
    private WebElement getStatusPaymentSchedule;
    //Fraud Alert
    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Fraud Alerts')]")
    private WebElement fraudAlertsTabAlertPage;
    @FindBy(how = How.XPATH, using = "//input[@title='Update Contact Information']")
    private WebElement updateContactInfoFraudAlert;
    @FindBy(how = How.XPATH, using = "//button[@id='returnToServices']")
    private WebElement cancelButtonFraudAlert;
    @FindBy(how = How.XPATH, using = "//section[@id='layoutContentPageSubHeader']//span[contains(text(),'One-time passcode')]")
    private WebElement actualFraudAlertPageTitle;

    //For retriving the card status
    @FindBy(how = How.XPATH, using = "//tr[@id='alert_15']/td[@class='vcp_alert_status']//span")
    private WebElement getStatusAvailableCredit;
    @FindBy(how = How.XPATH, using = "//tr[@id='alert_1']/td[@class='vcp_alert_status']//span")
    private WebElement getStatusBalanceExceedds;
    @FindBy(how = How.XPATH, using = "//*[@id='alert_11']//span")
    private WebElement getStatusAtmTranscation;
    @FindBy(how = How.XPATH, using = "//*[@id='alert_10']//span")
    private WebElement getStatusCardNotFound;
    @FindBy(how = How.XPATH, using = "//*[@id='alert_7']//span")
    private WebElement getStatusCrdeitPosted;
    @FindBy(how = How.XPATH, using = "//tr[@id='alert_9']/td[@class='vcp_alert_status']//span")
    private WebElement getStatusDebitPosted;
    @FindBy(how = How.XPATH, using = "//*[@id='alert_12']//span")
    private WebElement getStatusDeclineTranscation;
    @FindBy(how = How.XPATH, using = "//*[@id='alert_13']//span")
    private WebElement getStatusGasTranscation;
    @FindBy(how = How.XPATH, using = "//*[@id='alert_14']//span")
    private WebElement getStatusinternationalTranscation;
    @FindBy(how = How.XPATH, using = "//tr[@id='alert_9']/td[@class='vcp_alert_status']//span")
    private WebElement getStatusnotification;

    //Delete links
    @FindBy(how = How.ID, using = "href_delete_15")
    private WebElement availbleCreditAlertDeleteLink;
    @FindBy(how = How.XPATH, using = "//tr[@id='alert-15']//button[contains(text(),'Continue')]")
    private WebElement availbleCreditAlertContinueButton;
    @FindBy(how = How.ID, using = "href_delete_1")
    private WebElement balanceExceedsAlertDeleteLink;
    @FindBy(how = How.XPATH, using = "//tr[@id='alert-1']//button[contains(text(),'Continue')]")
    private WebElement balanceExceedsAlertContinueButton;
    @FindBy(how = How.ID, using = "href_delete_11")
    private WebElement atmTranscationAlertDeleteLink;
    @FindBy(how = How.XPATH, using = "//tr[@id='alert-11']//button[contains(text(),'Continue')]")
    private WebElement atmTranscationAlertContinueButton;
    @FindBy(how = How.ID, using = "href_delete_10")
    private WebElement cardNotPresentAlertDeleteLink;
    @FindBy(how = How.XPATH, using = "//tr[@id='alert-10']//button[contains(text(),'Continue')]")
    private WebElement cardNotPresentAlertContinueButton;
    @FindBy(how = How.ID, using = "href_delete_7")
    private WebElement creditPostedAlertDeleteLink;
    @FindBy(how = How.XPATH, using = "//tr[@id='alert-7']//button[contains(text(),'Continue')]")
    private WebElement creditPostedAlertContinueButton;
    @FindBy(how = How.ID, using = "href_delete_9")
    private WebElement debitPostedAlertDeleteLink;
    @FindBy(how = How.XPATH, using = "//tr[@id='alert-9']//button[contains(text(),'Continue')]")
    private WebElement debitPostedAlertContinueButton;
    @FindBy(how = How.ID, using = "href_delete_12")
    private WebElement declineTranscationAlertDeleteLink;
    @FindBy(how = How.XPATH, using = "//tr[@id='alert-12']//button[contains(text(),'Continue')]")
    private WebElement declineTranscationAlertContinueButton;
    @FindBy(how = How.ID, using = "href_delete_13")
    private WebElement gasStationPurchaseAlertDeleteLink;
    @FindBy(how = How.XPATH, using = "//tr[@id='alert-13']//button[contains(text(),'Continue')]")
    private WebElement gasStationPurchaseAlertContinueButton;
    @FindBy(how = How.ID, using = "href_delete_14")
    private WebElement internationalTranscationalAlertDeleteLink;
    @FindBy(how = How.XPATH, using = "//tr[@id='alert-14']//button[contains(text(),'Continue')]")
    private WebElement internationalTranscationalAlertContinueButton;
    @FindBy(how = How.ID, using = "href_delete_9")
    private WebElement notificationAlertDeleteLink;
    @FindBy(how = How.XPATH, using = "//tr[@id='alert-9']//button[contains(text(),'Continue')]")
    private WebElement notificationAlertContinueButton;

    //Balance Aler error messgae
    @FindBy(how = How.XPATH, using = "//*[contains(text(),'Available Credit amount field.')]")
    private WebElement actualErrorMessageForAmountFieldAvailabeCreditAlert;
    @FindBy(how = How.XPATH, using = "//*[contains(text(),'Account Balance Exceeds field.')]")
    private WebElement actualErrorMessageForAmountFieldBalanceExceedsAlert;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'An Address is required.')]")
    private WebElement actualErrorMessageForEmailFieldBalanceAlert;
    //Modified
    @FindBy(how= How.XPATH,using="//*[contains(text(),'numeric ')]")
    private WebElement actualErrorMessageForInformatAmountFieldAvailableCreditAlert;
    @FindBy(how = How.XPATH, using = "//*[contains(text(),'Balance Exceeds must be a numeric value.')]")
    private WebElement actualErrorMessageForInformatAmountFieldBalanceExceedsAlert;

    //Debit Posted alert error messgae
    @FindBy(how = How.XPATH, using = "//*[contains(text(),'greater')]")
    private WebElement actualErrorMessageForRadiobuttonDebitPostedAlert;
    @FindBy(how = How.XPATH, using = "//*[contains(text(),'Address is required')]")
    private WebElement actualErrorMessageForEmailIDDebitPostedAlert;
    @FindBy(how = How.XPATH, using = "//*[contains(text(),'would like')]")
    private WebElement actualErrorMessageForAmountFieldDebitPostedAlert;
    @FindBy(how = How.XPATH, using = "//*[contains(text(),'debit amount must be a numeric')]")
    private WebElement actualErrorMessageForInformatAmountFieldDebitPostedAlert;

    //Travel Notification Alert error message
    @FindBy(how = How.XPATH, using = "//*[contains(text(),'Authorization Amount')]")
    private WebElement actualErrorMessageForTravelNotificationAlert;

    //Payment Due Alert error message
    @FindBy(how = How.XPATH, using = " //*[contains(text(),'number of days')]")
    private WebElement actualErrorMessageForPaymentDueAlert;

    //Comman Error message for balance alert
    @FindBy(how = How.XPATH, using = "//span[contains(text(),'manage')]")
    private WebElement actualCommanErrorMessageForBalnaceAlert;
    @FindBy(how = How.XPATH, using = "//input[@title='OK']")
    private WebElement OkButtonForAlert;
    @FindBy(how = How.ID, using = "dialog1Title")
    private WebElement dialog1Title;
    @FindBy(how = How.ID, using = "dialog1Desc")
    private WebElement dialog1Desc;
    @FindBy(how = How.XPATH, using = "//*[@type='button' and @name='OK']")
    private WebElement okbtnInDialog;

    @FindBy(how=How.XPATH,using="//*[@id='layoutContentErrorMessages']//td[contains(text(),'Address is required')]")
    private WebElement emailNonSelectedErrorMessage;
    @FindBy(how=How.XPATH,using="//*[@id='layoutContentErrorMessages']//td[contains(text(),'review and re-enter')]")
    private  WebElement incorrectFormatEmailErrorMessage;
    @FindBy(how=How.XPATH,using = "//*[@id='layoutContentBody']//div[contains(text(),'No updates')]")
    private WebElement existingemailwhieEditErrorMessage;

    //Phonenumber for Availablecredit alert
    @FindBy(how = How.ID, using = "addr_value_1_15")
    private WebElement addAvailableCreditMobileNovalue;
    @FindBy(how = How.ID, using = "setup_addr_text_1_15")
    private WebElement  availbleCreditAlertTextValue;

    //Phonenumber for Balance exceeds alert
    @FindBy(how = How.ID, using = "addr_value_1_1")
    private WebElement addBalanceExceedsMobileNovalue;
    @FindBy(how = How.ID, using = "setup_addr_text_1_1")
    private WebElement  BalanceExceedsAlertTextValue;

    //phonenumber for ATM transcation
    @FindBy(how = How.ID, using = "addr_value_1_11")
    private WebElement addAtmTranscationMobileNovalue;
    @FindBy(how = How.ID, using = "setup_addr_text_1_11")
    private WebElement  AtmTranscationAlertTextValue;

    //phonenumber for Card Not Present
    @FindBy(how = How.ID, using = "addr_value_1_10")
    private WebElement addCardNotPresentMobileNovalue;
    @FindBy(how = How.ID, using = "setup_addr_text_1_10")
    private WebElement  CradNotPresentAlertTextValue;

    //phonenumber for Credit Posted
    @FindBy(how = How.ID, using = "addr_value_1_7")
    private WebElement addCreditPostedMobileNovalue;
    @FindBy(how = How.ID, using = "setup_addr_text_1_7")
    private WebElement  CreditPostedAlertTextValue;

    //phonenumber for debit Posted
    @FindBy(how = How.ID, using = "addr_value_1_8")
    private WebElement addDebitPostedMobileNovalue;
    @FindBy(how = How.ID, using = "setup_addr_text_1_8")
    private WebElement  DebitPostedAlertTextValue;

    //phonenumber for decline Transcation
    @FindBy(how = How.ID, using = "addr_value_1_12")
    private WebElement addDeclineTranscationMobileNovalue;
    @FindBy(how = How.ID, using = "setup_addr_text_1_12")
    private WebElement  DeclineTranscationAlertTextValue;

    //phonenumber for Gas station purchase
    @FindBy(how = How.ID, using = "addr_value_1_13")
    private WebElement addGasStationMobileNovalue;
    @FindBy(how = How.ID, using = "setup_addr_text_1_13")
    private WebElement  GasStationAlertTextValue;

    //phonenumber for International
    @FindBy(how = How.ID, using = "addr_value_1_14")
    private WebElement addInternationalMobileNovalue;
    @FindBy(how = How.ID, using = "setup_addr_text_1_14")
    private WebElement  InternationalAlertTextValue;

    //phonenumber for Transcation notification
    @FindBy(how = How.ID, using = "addr_value_1_9")
    private WebElement addNotificationMobileNovalue;
    @FindBy(how = How.ID, using = "setup_addr_text_1_9")
    private WebElement  notificationAlertTextValue;

    //phonenumber for Autopay scheduled and Autopay processed
    @FindBy(how = How.ID, using = "addr_value_1_5")
    private WebElement addAutopayScheduledMobileNovalue;
    @FindBy(how = How.ID, using = "setup_addr_text_1_5")
    private WebElement  AutopayScheduledAlertTextValue;

    //For retriving the card status
    @FindBy(how = How.XPATH, using = "//td[@id='alert_15']//div[@class='switch__touch']")
    private WebElement getStatusAvailableCreditomvbutton;
    @FindBy(how = How.XPATH, using = "//td[@id='alert_15']//ons-col//span")
    private WebElement getStatusAvailableCreditomv;
    @FindBy(how = How.XPATH, using = "//td[@id='alert_1']//div[@class='switch__touch']")
    private WebElement getStatusBalanceExceedsomvbutton;
    @FindBy(how = How.XPATH, using = "//td[@id='alert_1']//ons-col//span")
    private WebElement getStatusBalanceExceedsomv;

    //Balance alert Radio button for Email ID
    @FindBy(how = How.ID ,using = "setup_addr_type_1_15_0")
    private WebElement  availbleCreditAlertEmailRadiobuttonselection;
    @FindBy(how = How.ID ,using = "setup_addr_type_1_1_0")
    private WebElement  balanceExceedsAlertEmailRadiobuttonselection;

    //Transcation alert Radio button for Email ID
    @FindBy(how = How.ID ,using = "setup_addr_type_1_11_0")
    private WebElement  atmTranscationAlertEmailRadiobuttonselection;
    @FindBy(how = How.ID ,using = "setup_addr_type_1_10_0")
    private WebElement  cardNotPresentAlertEmailRadiobuttonselection;
    @FindBy(how = How.ID ,using = "setup_addr_type_1_7_0")
    private WebElement  creditPostedAlertEmailRadiobuttonselection;
    @FindBy(how = How.ID ,using = "setup_addr_type_1_8_0")
    private WebElement  debitPostedAlertEmailRadiobuttonselection;
    @FindBy(how = How.ID ,using = "setup_addr_type_1_12_0")
    private WebElement  declineTranscationAlertEmailRadiobuttonselection;
    @FindBy(how = How.ID ,using = "setup_addr_type_1_13_0")
    private WebElement  gasStationPurchaseAlertEmailRadiobuttonselection;
    @FindBy(how = How.ID ,using = "setup_addr_type_1_14_0")
    private WebElement  internationalTranscationAlertEmailRadiobuttonselection;
    @FindBy(how = How.ID ,using = "setup_addr_type_1_9_0")
    private WebElement transcationNotificationAlertEmailRadiobuttonselection;

    //Retriving the cardstatus for Transcation alert OMV
    @FindBy(how = How.XPATH, using = "//td[@id='alert_11']//span[@class='omv-alert-status-indicator']")
    private WebElement getStatusAtmTranscationomvbutton;
    @FindBy(how = How.XPATH, using = "//td[@id='alert_11']//div[@class='switch__touch']")
    private WebElement getStatusAtmTranscationomv;
    @FindBy(how = How.XPATH, using = "//td[@id='alert_10']//span[@class='omv-alert-status-indicator']")
    private WebElement getStatusCardNotPresentomvbutton;
    @FindBy(how = How.XPATH, using = "//td[@id='alert_10']//div[@class='switch__touch']")
    private WebElement getStatusCardNotPresentomv;
    @FindBy(how = How.XPATH, using = "//td[@id='alert_7']//span[@class='omv-alert-status-indicator']")
    private WebElement getStatusCreditPostedomvbutton;
    @FindBy(how = How.XPATH, using = "//td[@id='alert_7']//div[@class='switch__touch']")
    private WebElement getStatusCreditPostedomv;
    @FindBy(how = How.XPATH, using = "//td[@id='alert_8']//span[@class='omv-alert-status-indicator']")
    private WebElement getStatusDebitPostedomvbutton;
    @FindBy(how = How.XPATH, using = "//td[@id='alert_8']//div[@class='switch__touch']")
    private WebElement getStatusDebitPostedomv;
    @FindBy(how = How.XPATH, using = "//td[@id='alert_12']//span[@class='omv-alert-status-indicator']")
    private WebElement getStatusDeclineTranscationomvbutton;
    @FindBy(how = How.XPATH, using = "//td[@id='alert_12']//div[@class='switch__touch']")
    private WebElement getStatusDeclineTranscationomv;
    @FindBy(how = How.XPATH, using = "//td[@id='alert_13']//span[@class='omv-alert-status-indicator']")
    private WebElement getStatusGasstationPurchaseomvbutton;
    @FindBy(how = How.XPATH, using = "//td[@id='alert_13']//div[@class='switch__touch']")
    private WebElement getStatusGasstationPurchaseomv;
    @FindBy(how = How.XPATH, using = "//td[@id='alert_14']//span[@class='omv-alert-status-indicator']")
    private WebElement getStatusInternationalTranscationomvbutton;
    @FindBy(how = How.XPATH, using = "//td[@id='alert_14']//div[@class='switch__touch']")
    private WebElement getStatusInternationalTranscationomv;
    @FindBy(how = How.XPATH, using = "//td[@id='alert_9']//span[@class='omv-alert-status-indicator']")
    private WebElement getStatusTranscationNotificationomvbutton;
    @FindBy(how = How.XPATH, using = "//td[@id='alert_9']//div[@class='switch__touch']")
    private WebElement getStatusTranscationNotificationomv;

    @FindBy(how=How.ID,using="setup_addr_type_1_0_0")WebElement onlinestatementAvailableEmailRadiobuttonselection;
    @FindBy(how=How.ID,using="setup_addr_type_1_5_0") WebElement AutoPayScheduledandAutoPayProcessedEmailRadiobuttonselection;
    @FindBy(how=How.ID,using = "setup_addr_type_1_2_0") WebElement paymentDueEmailRadiobuttonselection;
    @FindBy(how=How.ID,using = "setup_addr_type_1_4_0") WebElement paymentOverdueEmailRadiobuttonselection;
    @FindBy(how=How.ID,using = "setup_addr_type_1_3_0") WebElement paymentPostedEmailRadiobuttonselection;
    @FindBy(how=How.ID,using = "setup_addr_type_1_6_0") WebElement paymentScheduleEmailRadiobuttonselection;
    @FindBy(how=How.ID,using = "setup_addr_type_1_16_0") WebElement securityEmailRadiobuttonselection;

    @FindBy(how=How.XPATH,using = "//*[@id='security_delete' or @id='href_delete_16']")WebElement deleteSecutityAlertlink;
    @FindBy(how = How.XPATH,using = "//*[@title='Continue']") WebElement continuebuttonForSecurity;
    @FindBy(how = How.XPATH,using = "//*[@class='closeOmvModal o-close-1 button']") WebElement closeSeeAgreementpage;


    public LandingForAlertsPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
        driver.manage().timeouts().implicitlyWait(implicitTimeout, TimeUnit.SECONDS);
    }

    public void clickAlertsLink() {
        linkAlertsPage.click();
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
    }
    public void clickManageAlertLink(){
        System.out.println(driver.getTitle());
        Assert.assertTrue("Failed:Mismatch in page title of alert homepage",driver.getTitle().endsWith("Alert History"));
        manageAlertlink.click();
    }
    public void verifyAlertHomePageTitle(){
        Assert.assertTrue("Failed:Mismatch in page title of the alert homepage",driver.getTitle().endsWith(" Alerts Overview"));
        logger.info("Passed:Manage alert link is taking the user to alert home page successfully after click");
    }

    public void clickSignupNowLink()
    {
        signUpNowLinkAlertPage.click();
    }
    public void closeSignupPage()
    {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(cancelButtonSignUpNowLinkAlertPage));
        cancelButtonSignUpNowLinkAlertPage.click();
    }
    public void clickAlertHistoryLink()
    {
        alertHistoryLinkAlertPage.click();
    }
    public void closeAlertHistoryPage()
    {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        System.out.println("page title"+driver.getTitle());
        Assert.assertTrue("Mismatch in page title of alert history",driver.getTitle().contains("Alert History"));
        wait.until(ExpectedConditions.elementToBeClickable(backButtonAlertHistoryLinkAlertPage));
        backButtonAlertHistoryLinkAlertPage.click();
    }

    public void clickSeeAgreementLink()
    {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(seeAgreementLinkAlertPage));
        seeAgreementLinkAlertPage.click();
    }
    public void closeSeeAgreementLink()
    {
        Set<String> allWindowHandles = driver.getWindowHandles();
        System.out.println("Number of Windows" + allWindowHandles.size());
        Iterator<String> it = allWindowHandles.iterator();
        String parentWindow = it.next();
        System.out.println("Window ID" + parentWindow);
        String popUp = it.next();
        // **** Switching to Child Window *******
        driver.switchTo().window(popUp);
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        System.out.println(closeWindowSeeAgreementLinkAlertPage.getText());
        WebDriverWait wait = new WebDriverWait(driver, implicitTimeout);
        wait.until(ExpectedConditions.elementToBeClickable(closeWindowSeeAgreementLinkAlertPage));
        closeWindowSeeAgreementLinkAlertPage.click();
        driver.switchTo().window(parentWindow);
    }

    public void setUpAlertForStatementAndPaymentSubTypes(String alertSubType,String emailId) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if (alertSubType.equals("Online Statement Available")) {
            onlineStatmentAvailablelink.click();
            setUpLinkForOnlineStatmentAvailable.click();
            logger.info("Online Statement Available alert is selected");
            setonlineStatmentAvailableAlerts(emailId);
        }
        else if (alertSubType.equals("AutoPay Scheduled and AutoPay Processed")) {
            AutoPayScheduledandAutoPayProcessedlink.click();
            setUpLinkForAutoPayScheduledandAutoPayProcessed.click();
            logger.info("AutoPay Scheduled and AutoPay Processed alert is selected");
            setAutoPayScheduledandAutoPayAlerts(emailId);
        }
        else if (alertSubType.equals("Payment Due")) {
            paymentDuelink.click();
            setUpLinkForpaymentDue.click();
            logger.info("Payment Due alert is selected");
            setPaymentDueAlerts(emailId);
        }
        else if (alertSubType.equals("Payment Overdue")) {
            paymentOverduelink.click();
            setUpLinkForpaymentOverdue.click();
            logger.info("Payment Overdue alert is selected");
            setPaymentOverdueAlerts(emailId);
        }
        else if (alertSubType.equals("Payment Posted")) {
            paymentPostedlink.click();
            setUpLinkForpaymentPosted.click();
            logger.info("Payment Overdue alert is selected");
            setPaymentPostedAlerts(emailId);
        }
        else if (alertSubType.equals("Payment Scheduled")) {
            paymentSchedulelink.click();
            setUpLinkForpaymentSchedule.click();
            logger.info("Payment Overdue alert is selected");
            setPaymentScheduleAlerts(emailId);
        }
        else
            logger.info("Please verify the subtype name of alert");
    }
    public void editAlertForStatementAndPaymentSubTypes(String alertSubType,String emailId) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if (alertSubType.equals("Online Statement Available")) {
            onlineStatmentAvailablelink.click();
            editLinkForOnlineStatmentAvailable.click();
            logger.info("Online Statement Available alert is selected");
            editonlineStatmentAvailableAlerts(emailId);
        }
        else if (alertSubType.equals("AutoPay Scheduled and AutoPay Processed")) {
            AutoPayScheduledandAutoPayProcessedlink.click();
            editLinkForAutoPayScheduledandAutoPayProcessed.click();
            logger.info("AutoPay Scheduled and AutoPay Processed alert is selected");
            editAutoPayScheduledandAutoPayAlerts(emailId);
        }
        else if (alertSubType.equals("Payment Due")) {
            paymentDuelink.click();
            editLinkForpaymentDue.click();
            logger.info("Payment Due alert is selected");
            editPaymentDueAlerts(emailId);
        }
        else if (alertSubType.equals("Payment Overdue")) {
            paymentOverduelink.click();
            editLinkForpaymentOverdue.click();
            logger.info("Payment Overdue alert is selected");
            editPaymentOverdueAlerts(emailId);
        }
        else if (alertSubType.equals("Payment Posted")) {
            paymentPostedlink.click();
            editUpLinkForpaymentPosted.click();
            logger.info("Payment Overdue alert is selected");
            editPaymentPostedAlerts(emailId);
        }
        else if (alertSubType.equals("Payment Scheduled")) {
            paymentSchedulelink.click();
            editLinkForpaymentSchedule.click();
            logger.info("Payment Overdue alert is selected");
            editPaymentScheduleAlerts(emailId);
        }
        else
            logger.info("Please verify the subtype name of alert");
    }

    public void deleteAlertForStatementAndPaymentSubTypes(String alertSubType) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if (alertSubType.equals("Online Statement Available")) {
            onlineStatmentAvailablelink.click();
            deleteLinkForOnlineStatementAvailable.click();
            deleteContinuebuttonForOnlineStatementAvailable.click();

        }
        else if (alertSubType.equals("AutoPay Scheduled and AutoPay Processed")) {
            AutoPayScheduledandAutoPayProcessedlink.click();
            deleteLinkForAutoPayScheduledandAutoPayProcessed.click();
            deleteContinuebuttonForAutoPayScheduledandAutoPayProcessed.click();
        }
        else if (alertSubType.equals("Payment Due")) {
            paymentDuelink.click();
            deleteLinkPaymentDue.click();
            deleteContinuebuttonForPaymentDue.click();

        }
        else if (alertSubType.equals("Payment Overdue")) {
            paymentOverduelink.click();
            deleteLinkForPaymentOverdue.click();
            deleteContinuebuttonForPaymentOverdue.click();
        }
        else if (alertSubType.equals("Payment Posted")) {
            paymentPostedlink.click();
            deleteLinkForPaymentPosted.click();
            deleteContinuebuttonForPaymentPosted.click();

        }
        else if (alertSubType.equals("Payment Scheduled")) {
            paymentSchedulelink.click();
            deleteLinkForPaymentSchedule.click();
            deleteContinuebuttonForPaymentSchedule.click();

        }
        else
            logger.info("Please verify the subtype name of alert");
    }

    public void verifyAccountSetUpAlertSuccessMessage(){
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        Assert.assertEquals("Thank you.  Your alert has been set up successfully.",setupSuccessActualText.getText());
        logger.info("Passed:Your alert has been set up successfully");
    }
    public void verifyAccountEditAlertSuccessMessage(){
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        Assert.assertEquals("Thank you.  Your alert has been updated successfully.",editSuccessActualText.getText());
        logger.info("Passed:Your alert has been updated successfully");
    }


    public void clickSecurityAlertLink()
    {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(securityAlertsTabAlertPage));
        securityAlertsTabAlertPage.click();
    }

    public void setSecurityAlerts(String status,String emailId){
        new Select(statusAlertPage).selectByVisibleText(status);
        new Select(securityContactSelectdropdown).selectByIndex(0);
        new Select(SelectSecurityContactEmailvalue).selectByValue("Add New Email");
        SecurityContactEmailTextbox.sendKeys(emailId);
        securitySaveButton.click();
    }
    public void editSecurityAlerts(String emailId){

        new Select(securityContactSelectdropdown).selectByIndex(0);
        new Select(SelectSecurityContactEmailvalue).selectByValue("Add New Email");
        SecurityContactEmailTextbox.sendKeys(emailId);
        securitySaveButton.click();
    }
    public void verifySecurityAlertSuccessMessage(){
        Assert.assertEquals("Thank you.  Your alert has been updated successfully.",setupSuccessActualText.getText());
        System.out.println("Passed:Your security alert has been set up successfully");
    }

    public void clickstatementAndPaymentLink(){
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(statementAndPaymentLink));
        statementAndPaymentLink.click();
    }
    //will pass data to set the alert that are under statement and payments alerts
    public void setAutoPayScheduledandAutoPayAlerts(String emailId){
        new Select(AutoPayScheduledandAutoPayProcessedSelectdropdown).selectByIndex(0);
        new Select(AutoPayScheduledandAutoPayProcessedContactEmailvalue).selectByValue("Add New Email");
        AutoPayScheduledandAutoPayProcessedContactEmailTextbox.sendKeys(emailId);
        AutoPayScheduledandAutoPayProcessedSaveButton.click();
    }
    public void setAutoPayScheduledandAutoPayProcessedAlerts(String status,String emailId){
        new Select(statusAlertPage).selectByVisibleText(status);
        new Select(paymentScheduleSelectdropdown).selectByIndex(0);
        new Select(paymentScheduleContactEmailvalue).selectByValue("Add New Email");
        paymentScheduleContactEmailTextbox.sendKeys(emailId);
        paymentScheduleSaveButton.click();
    }
    public void setonlineStatmentAvailableAlerts(String emailId){
        new Select(onlineStatmentAvailableSelectdropdown).selectByIndex(0);
        new Select(onlineStatmentAvailableContactEmailvalue).selectByValue("Add New Email");
        onlineStatmentAvailableContactEmailTextbox.sendKeys(emailId);
        onlineStatmentAvailableSaveButton.click();
    }
    public void setPaymentDueAlerts(String emailId){
        new Select(numberOfDaysPriorToDueDateSelectdropdown).selectByIndex(2);
        new Select(paymentDueSelectdropdown).selectByIndex(0);
        new Select(paymentDueContactEmailvalue).selectByValue("Add New Email");
        paymentDueContactEmailTextbox.sendKeys(emailId);
        paymentDueSaveButton.click();
    }
    public void setPaymentOverdueAlerts(String emailId){
        new Select(paymentOverdueSelectdropdown).selectByIndex(0);
        new Select(paymentOverdueContactEmailvalue).selectByValue("Add New Email");
        paymentOverdueContactEmailTextbox.sendKeys(emailId);
        paymentOverdueSaveButton.click();
    }
    public void setPaymentPostedAlerts(String emailId){
        new Select(paymentPostedSelectdropdown).selectByIndex(0);
        new Select(paymentPostedContactEmailvalue).selectByValue("Add New Email");
        paymentPostedContactEmailTextbox.sendKeys(emailId);
        paymentPostedSaveButton.click();
    }
    public void setPaymentScheduleAlerts(String emailId){
        new Select(paymentScheduleSelectdropdown).selectByIndex(0);
        new Select(paymentScheduleContactEmailvalue).selectByValue("Add New Email");
        paymentScheduleContactEmailTextbox.sendKeys(emailId);
        paymentScheduleSaveButton.click();
    }

    //will pass data to edit the alert that are under statement and payments alerts
    public void editAutoPayScheduledandAutoPayAlerts(String emailId){
        new Select(AutoPayScheduledandAutoPayProcessedSelectdropdown).selectByIndex(0);
        new Select(AutoPayScheduledandAutoPayProcessedContactEmailvalue).selectByValue("Add New Email");
        AutoPayScheduledandAutoPayProcessedContactEmailTextbox.sendKeys(emailId);
        AutoPayScheduledandAutoPayProcessedSaveButton.click();
    }
    public void editAutoPayScheduledandAutoPayProcessedAlerts(String emailId){
        new Select(paymentScheduleSelectdropdown).selectByIndex(0);
        new Select(paymentScheduleContactEmailvalue).selectByValue("Add New Email");
        paymentScheduleContactEmailTextbox.sendKeys(emailId);
        paymentScheduleSaveButton.click();
    }
    public void editonlineStatmentAvailableAlerts(String emailId){
        new Select(onlineStatmentAvailableSelectdropdown).selectByIndex(0);
        new Select(onlineStatmentAvailableContactEmailvalue).selectByValue("Add New Email");
        onlineStatmentAvailableContactEmailTextbox.sendKeys(emailId);
        onlineStatmentAvailableSaveButton.click();
    }
    public void editPaymentDueAlerts(String emailId){

        new Select(numberOfDaysPriorToDueDateSelectdropdown).selectByIndex(2);
        new Select(paymentDueSelectdropdown).selectByIndex(0);
        new Select(paymentDueContactEmailvalue).selectByValue("Add New Email");
        paymentDueContactEmailTextbox.sendKeys(emailId);
        paymentDueSaveButton.click();
    }
    public void editPaymentOverdueAlerts(String emailId){
        new Select(paymentOverdueSelectdropdown).selectByIndex(0);
        new Select(paymentOverdueContactEmailvalue).selectByValue("Add New Email");
        paymentOverdueContactEmailTextbox.sendKeys(emailId);
        paymentOverdueSaveButton.click();
    }
    public void editPaymentPostedAlerts(String emailId){
        new Select(paymentPostedSelectdropdown).selectByIndex(0);
        new Select(paymentPostedContactEmailvalue).selectByValue("Add New Email");
        paymentPostedContactEmailTextbox.sendKeys(emailId);
        paymentPostedSaveButton.click();
    }
    public void editPaymentScheduleAlerts(String emailId){
        new Select(paymentScheduleSelectdropdown).selectByIndex(0);
        new Select(paymentScheduleContactEmailvalue).selectByValue("Add New Email");
        paymentScheduleContactEmailTextbox.sendKeys(emailId);
        paymentScheduleSaveButton.click();
    }


    public void clickBalanceTranfer()
    {
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        balanceAlerts.click();
    }

    public void setUpAlertForBalanceTranferSubTypes (String alertSubType,String Amount,String emailId)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType.equals("Available Credit"))
        {
            availblecreditLinkAlert.click();
            availbleCreditAlertSetUpLink.click();
            setUpAvailableAlertFunctionality(Amount,emailId);
            logger.info("Passed:Available credit alert is selected");
        }
        else if(alertSubType.equals("Balance Exceeds"))
        {
            balanceexceedsLinkAlert.click();
            balanceExceedsSetupLink.click();
            setUpBalanceExceedsAlertFunctionality(Amount,emailId);
            logger.info("Passed:Balance exceeds alert is selected");
        }
        else
        {
            System.out.println("alert is not selected");
        }
    }
    public void setUpAvailableAlertFunctionality(String Amount,String emailId)
    {
        availbleCreditAlertAmount.sendKeys(Amount);
        new Select(availbleCreditEmailDropdown).selectByIndex(0);
        new Select(availbleCreditAlertEmailValue).selectByValue("Add New Email");
        availbleCreditEmailTextBox.sendKeys(emailId);
        availbleCreditsaveButton.click();

    }
    public void setUpBalanceExceedsAlertFunctionality(String Amount,String emailId)
    {
        balanceExceedsamount.sendKeys(Amount);
        new Select(balanceExceedsContactSelectdropdown).selectByIndex(0);
        new Select(selectBalanceExceedsEmailvalue).selectByValue("Add New Email");
        balanceExceedsContactEmailTextbox.sendKeys(emailId);
        balanceExceedsSaveButton.click();
    }
    public void clickTranscationAlerts()
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        jse.executeScript("arguments[0].click();", transcationAlerts);
        System.out.println("Transcation alert is selected");
    }

    public void setUpAlertForTranscationSubTypes(String alertSubType1,String emailid1)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType1.equals("ATM Transaction"))
        {
            atmTranscationAlertSetupLink.click();
            System.out.println("ATMTranscation alert is selected");
            setAtmTranscationAlert(emailid1);
        }
        else if(alertSubType1.equals("Card Not Present"))
        {
            cardNotPresentAlertSetupLink.click();
            System.out.println("cardNotPresent alert is selected");
            setCardNotPresentAlert(emailid1);

        }
        else if(alertSubType1.equals("Credit Posted"))
        {
            creditPostedAlertSetupLink.click();
            System.out.println("Credit Posted alert is selected");
            setCreditPostedAlert(emailid1);

        }
        else if(alertSubType1.equals("Debit Posted"))
        {
            deditPostedAlertSetupLink.click();
            System.out.println("Debit Posted alert is selected");
            setDebitPostedAlert(emailid1);
        }
        else if(alertSubType1.equals("Decline Transaction"))
        {
            declineTranscationAlertSetupLink.click();
            System.out.println("Decline Transaction alert is selected");
            setDeclineTranscationAlert(emailid1);
        }
        else if(alertSubType1.equals("Gas Station Purchase"))
        {
            gasStationPurchaseAlertSetupLink.click();
            System.out.println("Gas Station Purchase alert is selected");
            setGasStationPurchaseAlert(emailid1);
        }
        else if(alertSubType1.equals("International Transaction"))
        {
            internationaTranscationAlertSetupLink.click();
            System.out.println("International Transaction alert is selected");
            setInternationaTranscationAlert(emailid1);
        }
        else if(alertSubType1.equals("Transaction Notification"))
        {
            transcationNotificationAlertSetupLink.click();
            System.out.println("Transaction Notification alert is selected");
            setTranscationNotificationAlert(emailid1);
        }
        else
        {
            System.out.println("Alert is not selected");
        }
    }
    public void setAtmTranscationAlert(String emailid1)
    {
        new Select(atmTranscationContactSelectdropdown).selectByIndex(0);
        new Select(selectAtmTranscationEmailvalue).selectByValue("Add New Email");
        atmTranscationContactEmailTextbox.sendKeys(emailid1);
        atmTranscationSaveButton.click();
    }
    public void setCardNotPresentAlert(String emailid1)
    {
        new Select(cardNotPresentContactSelectdropdown).selectByIndex(0);
        new Select(selectCardNotPresentEmailvalue).selectByValue("Add New Email");
        cardNotPresentContactEmailTextbox.sendKeys(emailid1);
        cardNotPresentSaveButton.click();
    }
    public void setCreditPostedAlert(String emailid1)
    {
        new Select(creditPostedContactSelectdropdown).selectByIndex(0);
        new Select(selectCreditPostedEmailvalue).selectByValue("Add New Email");
        creditPostedContactEmailTextbox.sendKeys(emailid1);
        creditPostedSaveButton.click();
    }
    public void setDebitPostedAlert(String emailid1)
    {
        selectDebitPostedAmount.click();
        addDebitPostedAmount.sendKeys("1");
        new Select(debitPostedContactSelectdropdown).selectByIndex(0);
        new Select(selectDebitPostedEmailvalue).selectByValue("Add New Email");
        debitPostedContactEmailTextbox.sendKeys(emailid1);
        debitPostedSaveButton.click();
    }

    public void setDeclineTranscationAlert(String emailid1)
    {
        new Select(declineTrascationContactSelectdropdown).selectByIndex(0);
        new Select(selectDeclineTrascationEmailvalue).selectByValue("Add New Email");
        declineTrascationContactEmailTextbox.sendKeys(emailid1);
        declineTrascationSaveButton.click();
    }
    public void setGasStationPurchaseAlert(String emailid1)
    {
        new Select(gasStationPurchaseContactSelectdropdown).selectByIndex(0);
        new Select(selectGasStationPurchaseEmailvalue).selectByValue("Add New Email");
        gasStationPurchaseContactEmailTextbox.sendKeys(emailid1);
        gasStationPurchaseSaveButton.click();
    }
    public void setInternationaTranscationAlert(String emailid1)
    {
        new Select(internationalTranscationalContactSelectdropdown).selectByIndex(0);
        new Select(selectInternationalTranscationalEmailvalue).selectByValue("Add New Email");
        internationalTranscationaleContactEmailTextbox.sendKeys(emailid1);
        internationalTranscationalSaveButton.click();

    }
    public void setTranscationNotificationAlert(String emailid1)
    {
        notificationAmount.sendKeys("1");
        new Select(notificationContactSelectdropdown).selectByIndex(0);
        new Select(selectNotificationEmailvalue).selectByValue("Add New Email");
        notificationContactEmailTextbox.sendKeys(emailid1);
        notificationSaveButton.click();
    }

    public void editAlertForBalanceTranferSubTypes (String alertSubType,String EditAmount)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType.equals("Available Credit"))
        {
            availblecreditLinkAlert.click();
            logger.info("Available credit alert is selected");
            availableCreditEditLink.click();
            editAvailableAlertFunctionality(EditAmount);

        }
        else if(alertSubType.equals("Balance Exceeds"))
        {
            balanceexceedsLinkAlert.click();
            System.out.println("Balance exceeds alert is selected");
            balanceExceedsEditLink.click();
            editBalanceExceedsAlertFunctionality(EditAmount);
        }
        else
        {
            logger.info("alert is not selected");
        }
    }
    public void editAvailableAlertFunctionality(String EditAmount)
    {
        availbleCreditAlertAmount.clear();
        availbleCreditAlertAmount.sendKeys(EditAmount);
        availbleCreditsaveButton.click();

    }
    public void editBalanceExceedsAlertFunctionality(String EditAmount)
    {
        balanceExceedsamount.clear();
        balanceExceedsamount.sendKeys(EditAmount);
        balanceExceedsSaveButton.click();

    }
    public void editAlertForTranscationSubTypes(String alertSubType1,String Editemailid1)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType1.equals("ATM Transaction"))
        {
            atmTranscationEditLink.click();
            logger.info("ATMTranscation alert is selected");
            editAtmTranscationAlert(Editemailid1);
        }
        else if(alertSubType1.equals("Card Not Present"))
        {
            cardNotPresentEditLink.click();
            logger.info("cardNotPresent alert is selected");
            editCardNotPresentAlert(Editemailid1);

        }
        else if(alertSubType1.equals("Credit Posted"))
        {
            creditPostedEditLink.click();
            logger.info("Credit Posted alert is selected");
            editCreditPostedAlert(Editemailid1);

        }
        else if(alertSubType1.equals("Debit Posted"))
        {
            deditPostedAlertSetupLink.click();
            logger.info("dedit Posted alert is selected");
            editDebitPostedAlert(Editemailid1);
        }
        else if(alertSubType1.equals("Decline Transaction"))
        {
            declineTrascationEditLink.click();
            logger.info("Decline Transaction alert is selected");
            editDeclineTranscationAlert(Editemailid1);
        }
        else if(alertSubType1.equals("Gas Station Purchase"))
        {
            gasStationPurchaseEditLink.click();
            logger.info("Gas Station Purchase alert is selected");
            editGasStationPurchaseAlert(Editemailid1);
        }
        else if(alertSubType1.equals("International Transaction"))
        {
            internationalTranscationalEditLink.click();
            logger.info("International Transaction alert is selected");
            editInternationaTranscationAlert(Editemailid1);
        }
        else if(alertSubType1.equals("Transaction Notification"))
        {
            notificationEditLink.click();
            logger.info("Transaction Notification alert is selected");
            editTranscationNotificationAlert(Editemailid1);
        }
        else
        {
            logger.info("Alert is not selected");
        }
    }
    public void editAtmTranscationAlert(String Editemailid1)
    {
        new Select(atmTranscationContactSelectdropdown).selectByIndex(0);
        new Select(selectAtmTranscationEmailvalue).selectByValue("Add New Email");
        atmTranscationContactEmailTextbox.sendKeys(Editemailid1);
        atmTranscationSaveButton.click();
    }
    public void editCardNotPresentAlert(String Editemailid1)
    {
        new Select(cardNotPresentContactSelectdropdown).selectByIndex(0);
        new Select(selectCardNotPresentEmailvalue).selectByValue("Add New Email");
        cardNotPresentContactEmailTextbox.sendKeys(Editemailid1);
        cardNotPresentSaveButton.click();
    }
    public void editCreditPostedAlert(String Editemailid1)
    {
        new Select(creditPostedContactSelectdropdown).selectByIndex(0);
        new Select(selectCreditPostedEmailvalue).selectByValue("Add New Email");
        creditPostedContactEmailTextbox.sendKeys(Editemailid1);
        creditPostedSaveButton.click();
    }
    public void editDebitPostedAlert(String Editemailid1)
    {
        selectDebitPostedAmount.click();
        addDebitPostedAmount.sendKeys("1");
        new Select(debitPostedContactSelectdropdown).selectByIndex(0);
        new Select(selectDebitPostedEmailvalue).selectByValue("Add New Email");
        debitPostedContactEmailTextbox.sendKeys(Editemailid1);
        debitPostedSaveButton.click();
    }

    public void editDeclineTranscationAlert(String Editemailid1)
    {
        new Select(declineTrascationContactSelectdropdown).selectByIndex(0);
        new Select(selectDeclineTrascationEmailvalue).selectByValue("Add New Email");
        declineTrascationContactEmailTextbox.sendKeys(Editemailid1);
        declineTrascationSaveButton.click();
    }
    public void editGasStationPurchaseAlert(String Editemailid1)
    {
        new Select(gasStationPurchaseContactSelectdropdown).selectByIndex(0);
        new Select(selectGasStationPurchaseEmailvalue).selectByValue("Add New Email");
        gasStationPurchaseContactEmailTextbox.sendKeys(Editemailid1);
        gasStationPurchaseSaveButton.click();
    }
    public void editInternationaTranscationAlert(String Editemailid1)
    {
        new Select(internationalTranscationalContactSelectdropdown).selectByIndex(0);
        new Select(selectInternationalTranscationalEmailvalue).selectByValue("Add New Email");
        internationalTranscationaleContactEmailTextbox.sendKeys(Editemailid1);
        internationalTranscationalSaveButton.click();

    }
    public void editTranscationNotificationAlert(String Editemailid1)
    {
        notificationAmount.sendKeys("1");
        new Select(notificationContactSelectdropdown).selectByIndex(0);
        new Select(selectNotificationEmailvalue).selectByValue("Add New Email");
        notificationContactEmailTextbox.sendKeys(Editemailid1);
        notificationSaveButton.click();
    }

    //For selecting the alert Subtype and clicking on edit link
    public void editAlertByChangingStatusOfSubTypeAlertsUnderStatementAndPayment(String alertSubType) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if (alertSubType.equals("Online Statement Available")) {
            onlineStatmentAvailablelink.click();
            logger.info("Card status before editing for "+alertSubType+" subType:"+getStatusOnlineStatementAvailable.getText());
            String CardStatus=getStatusOnlineStatementAvailable.getText();
            editLinkForOnlineStatmentAvailable.click();
            if(CardStatus.equalsIgnoreCase("Active")){
                new Select(statusAlertPage).selectByVisibleText("Inactive");
            }else
            {
                new Select(statusAlertPage).selectByVisibleText("Active");
            }
            onlineStatmentAvailableSaveButton.click();
            onlineStatmentAvailablelink.click();
            logger.info("Card status After editing for "+alertSubType+" subType:"+getStatusOnlineStatementAvailable.getText());
        }
        else if (alertSubType.equals("AutoPay Scheduled and AutoPay Processed")) {
            AutoPayScheduledandAutoPayProcessedlink.click();
            String CardStatus=getStatusAutopaySheduledAndProcessed.getText();
            logger.info("Card status before editing for "+alertSubType+" subType:"+getStatusAutopaySheduledAndProcessed.getText());
            editLinkForAutoPayScheduledandAutoPayProcessed.click();
            if(CardStatus.equalsIgnoreCase("Active")){

                new Select(statusAlertPage).selectByVisibleText("Inactive");
            }else
            {
                new Select(statusAlertPage).selectByVisibleText("Active");
            }
            AutoPayScheduledandAutoPayProcessedSaveButton.click();
            AutoPayScheduledandAutoPayProcessedlink.click();
            logger.info("Card status After editing for "+alertSubType+" subType:"+getStatusAutopaySheduledAndProcessed.getText());
        }
        else if (alertSubType.equals("Payment Due")) {
            paymentDuelink.click();
            logger.info("Card status before editing for "+alertSubType+" subType:"+getStatusPaymentDue.getText());
            String CardStatus=getStatusPaymentDue.getText();
            editLinkForpaymentDue.click();
            if(CardStatus.equalsIgnoreCase("Active")){

                new Select(statusAlertPage).selectByVisibleText("Inactive");
            }else
            {
                new Select(statusAlertPage).selectByVisibleText("Active");
            }
            paymentDueSaveButton.click();
            paymentDuelink.click();
            logger.info("Card status After editing for "+alertSubType+" subType:"+getStatusPaymentDue.getText());

        }
        else if (alertSubType.equals("Payment Overdue")) {
            paymentOverduelink.click();
            logger.info("Card status before editing for "+alertSubType+" subType:"+getStatusPaymentOverdue.getText());
            String CardStatus=getStatusPaymentOverdue.getText();
            editLinkForpaymentOverdue.click();
            if(CardStatus.equalsIgnoreCase("Active")){

                new Select(statusAlertPage).selectByVisibleText("Inactive");
            }else
            {
                new Select(statusAlertPage).selectByVisibleText("Active");
            }
            paymentOverdueSaveButton.click();
            paymentOverduelink.click();
            logger.info("Card status After editing for "+alertSubType+" subType:"+getStatusPaymentOverdue.getText());

        }
        else if (alertSubType.equals("Payment Posted")) {
            paymentPostedlink.click();
            logger.info("Card status before editing for "+alertSubType+" subType:"+getStatusPaymentPosted.getText());
            String CardStatus=getStatusPaymentPosted.getText();
            editUpLinkForpaymentPosted.click();
            if(CardStatus.equalsIgnoreCase("Active")){

                new Select(statusAlertPage).selectByVisibleText("Inactive");
            }else
            {
                new Select(statusAlertPage).selectByVisibleText("Active");
            }
            paymentPostedSaveButton.click();
            paymentPostedlink.click();
            logger.info("Card status After editing for "+alertSubType+" subType:"+getStatusPaymentPosted.getText());

        }
        else if (alertSubType.equals("Payment Scheduled")) {
            paymentSchedulelink.click();
            logger.info("Card status before editing for "+alertSubType+" subType:"+getStatusPaymentSchedule.getText());
            String CardStatus=getStatusPaymentSchedule.getText();
            editLinkForpaymentSchedule.click();
            if(CardStatus.equalsIgnoreCase("Active")){

                new Select(statusAlertPage).selectByVisibleText("Inactive");
            }else
            {
                new Select(statusAlertPage).selectByVisibleText("Active");
            }
            paymentScheduleSaveButton.click();
            paymentSchedulelink.click();
            logger.info("Card status After editing for "+alertSubType+" subType:"+getStatusPaymentSchedule.getText());


        }
        else
            logger.info("Please verify the subtype name of alert");
    }
    public void editSecurityAlertByChangingStatus(){
        String CardStatus=new Select(statusAlertPage).getFirstSelectedOption().getText();
        logger.info("Security alert card status before changing the status: "+CardStatus);
        if(CardStatus.equalsIgnoreCase("Active")){

            new Select(statusAlertPage).selectByVisibleText("Inactive");
        }else
        {
            new Select(statusAlertPage).selectByVisibleText("Active");
        }
        securitySaveButton.click();
        String CardStatusAfterEdit=new Select(statusAlertPage).getFirstSelectedOption().getText();
        logger.info("Security alert card status after changing the status: "+CardStatusAfterEdit);

    }
    //Edit card status functionality for card status

    public void editAlertByChangingStatusOfSubtypesForBalanceTranferSubTypes (String alertSubType)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType.equals("Available Credit"))
        {
            availblecreditLinkAlert.click();
            logger.info("Available credit alert is selected");
            System.out.println("Card status before editing:"+getStatusAvailableCredit.getText());
            String CardStatus=getStatusAvailableCredit.getText();
            availableCreditEditLink.click();
            if(CardStatus.equalsIgnoreCase("Active"))
            {
                new Select(statusAlertPage).selectByVisibleText("Inactive");
            }
            else
            {
                new Select(statusAlertPage).selectByVisibleText("Active");
            }
            availbleCreditsaveButton.click();
            System.out.println("Card status after editing:"+getStatusAvailableCredit.getText());
        }
        else if(alertSubType.equals("Balance Exceeds"))
        {
            balanceexceedsLinkAlert.click();
            logger.info("Balance exceeds alert is selected");
            System.out.println("Card status before editing:"+getStatusBalanceExceedds.getText());
            String CardStatus=getStatusBalanceExceedds.getText();
            balanceExceedsEditLink.click();
            if(CardStatus.equalsIgnoreCase("Active"))
            {
                new Select(statusAlertPage).selectByVisibleText("Inactive");
            }
            else
            {
                new Select(statusAlertPage).selectByVisibleText("Active");
            }
            balanceExceedsSaveButton.click();
            System.out.println("Card status after editing:"+getStatusBalanceExceedds.getText());
        }
        else
        {
            logger.info("alert is not selected");
        }
    }


    //Edit card Status function for transcation alert
    public void editAlertByChangingStatusOfSubtypesForTranscationSubTypes(String alertSubType1)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType1.equals("ATM Transaction"))
        {
            logger.info("ATM credit alert is selected");
            System.out.println("Card status before editing:"+getStatusAtmTranscation.getText());
            String CardStatus=getStatusAtmTranscation.getText();
            atmTranscationEditLink.click();
            if(CardStatus.equalsIgnoreCase("Active"))
            {
                new Select(statusAlertPage).selectByVisibleText("Inactive");
            }
            else
            {
                new Select(statusAlertPage).selectByVisibleText("Active");
            }
            atmTranscationSaveButton.click();
            System.out.println("Card status after editing:"+getStatusAtmTranscation.getText());
        }
        else if(alertSubType1.equals("Card Not Present"))
        {
            logger.info("card Not found  alert is selected");
            System.out.println("Card status before editing:"+getStatusCardNotFound.getText());
            String CardStatus=getStatusCardNotFound.getText();
            cardNotPresentEditLink.click();
            if(CardStatus.equalsIgnoreCase("Active"))
            {
                new Select(statusAlertPage).selectByVisibleText("Inactive");
            }
            else
            {
                new Select(statusAlertPage).selectByVisibleText("Active");
            }
            cardNotPresentSaveButton.click();
            System.out.println("Card status after editing:"+getStatusCardNotFound.getText());
        }
        else if(alertSubType1.equals("Credit Posted"))
        {
            logger.info("credit posted alert is selected");
            System.out.println("Card status before editing:"+getStatusCrdeitPosted.getText());
            String CardStatus=getStatusCrdeitPosted.getText();
            creditPostedEditLink.click();
            if(CardStatus.equalsIgnoreCase("Active"))
            {
                new Select(statusAlertPage).selectByVisibleText("Inactive");
            }
            else
            {
                new Select(statusAlertPage).selectByVisibleText("Active");
            }
            creditPostedSaveButton.click();
            System.out.println("Card status after editing:"+getStatusCrdeitPosted.getText());

        }
        else if(alertSubType1.equals("Debit Posted"))
        {
            logger.info("debit posted alert is selected");
            System.out.println("Card status before editing:"+getStatusDebitPosted.getText());
            String CardStatus=getStatusDebitPosted.getText();
            atmTranscationEditLink.click();
            if(CardStatus.equalsIgnoreCase("Active"))
            {
                new Select(statusAlertPage).selectByVisibleText("Inactive");
            }
            else
            {
                new Select(statusAlertPage).selectByVisibleText("Active");
            }
            debitPostedSaveButton.click();
            System.out.println("Card status after editing:"+getStatusDebitPosted.getText());
        }
        else if(alertSubType1.equals("Decline Transaction"))
        {
            logger.info("Decline Transcation alert is selected");
            System.out.println("Card status before editing:"+getStatusDeclineTranscation.getText());
            String CardStatus=getStatusDeclineTranscation.getText();
            declineTrascationEditLink.click();
            if(CardStatus.equalsIgnoreCase("Active"))
            {
                new Select(statusAlertPage).selectByVisibleText("Inactive");
            }
            else
            {
                new Select(statusAlertPage).selectByVisibleText("Active");
            }
            declineTrascationSaveButton.click();
            System.out.println("Card status after editing:"+getStatusDeclineTranscation.getText());
        }
        else if(alertSubType1.equals("Gas Station Purchase"))
        {
            logger.info("Gas Station alert is selected");
            System.out.println("Card status before editing:"+getStatusGasTranscation.getText());
            String CardStatus=getStatusGasTranscation.getText();
            gasStationPurchaseEditLink.click();
            if(CardStatus.equalsIgnoreCase("Active"))
            {
                new Select(statusAlertPage).selectByVisibleText("Inactive");
            }
            else
            {
                new Select(statusAlertPage).selectByVisibleText("Active");
            }
            gasStationPurchaseSaveButton.click();
            System.out.println("Card status after editing:"+getStatusGasTranscation.getText());
        }
        else if(alertSubType1.equals("International Transaction"))
        {
            logger.info("International Transcation  alert is selected");
            System.out.println("Card status before editing:"+getStatusinternationalTranscation.getText());
            String CardStatus=getStatusinternationalTranscation.getText();
            internationalTranscationalEditLink.click();
            if(CardStatus.equalsIgnoreCase("Active"))
            {
                new Select(statusAlertPage).selectByVisibleText("Inactive");
            }
            else
            {
                new Select(statusAlertPage).selectByVisibleText("Active");
            }
            internationalTranscationalSaveButton.click();
            System.out.println("Card status after editing:"+getStatusinternationalTranscation.getText());
        }
        else if(alertSubType1.equals("Transaction Notification"))
        {
            logger.info(" Transaction Notification alert is selected");
            System.out.println("Card status before editing:"+getStatusnotification.getText());
            String CardStatus=getStatusnotification.getText();
            notificationEditLink.click();
            if(CardStatus.equalsIgnoreCase("Active"))
            {
                new Select(statusAlertPage).selectByVisibleText("Inactive");
            }
            else
            {
                new Select(statusAlertPage).selectByVisibleText("Active");
            }
            notificationSaveButton.click();
            System.out.println("Card status after editing:"+getStatusnotification.getText());
        }
        else
        {
            logger.info("Alert is not selected");
        }
    }
    //Delete functionality for alert
    public void deleteAlertForBalanceTranferSubTypes (String alertSubType)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType.equals("Available Credit"))
        {
            availblecreditLinkAlert.click();
            logger.info("Available credit alert is selected");
            deleteAvailableAlertFunctionality();

        }
        else if(alertSubType.equals("Balance Exceeds"))
        {
            balanceexceedsLinkAlert.click();
            logger.info("Balance exceeds alert is selected");
            deleteBalanceExceedsAlertFunctionality();
        }
        else
        {
            logger.info("alert is not selected");
        }
    }
    public void deleteAvailableAlertFunctionality()
    {
        availbleCreditAlertDeleteLink.click();
        availbleCreditAlertContinueButton.click();
        logger.info("Available credit Alert is deleted successful ");
    }
    public void deleteBalanceExceedsAlertFunctionality()
    {
        balanceExceedsAlertDeleteLink.click();
        balanceExceedsAlertContinueButton.click();
        logger.info("Balance Exceeds Alert is deleted successful");
    }


    public void verifyAccountdeleteAlertSuccessMessage(String subtype )
    {
        Assert.assertEquals("Thank you.  Your alert has been deleted successfully.",deleteSuccessActualText.getText());
        logger.info("Passed:Your "+ subtype + "alert has been deleted successfully");
    }


    //Delete function for transcation alert
    public void deleteAlertForTranscationSubTypes(String alertSubType1)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType1.equals("ATM Transaction"))
        {
            deleteAtmTranscationAlert();
            logger.info("ATMTranscation alert is deleted");
        }
        else if(alertSubType1.equals("Card Not Present"))
        {
            deleteCardNotPresentAlert();
            logger.info("cardNotPresent alert is deleted");
        }
        else if(alertSubType1.equals("Credit Posted"))
        {
            deleteCreditPostedAlert();
            logger.info("Credit Posted alert is deleted");

        }
        else if(alertSubType1.equals("Debit Posted"))
        {
            deleteDebitPostedAlert();
            logger.info("dedit Posted alert is deleted");
        }
        else if(alertSubType1.equals("Decline Transaction"))
        {
            deleteDeclineTranscationAlert();
            logger.info("Decline Transaction alert is deleted");
        }
        else if(alertSubType1.equals("Gas Station Purchase"))
        {
            deleteGasStationPurchaseAlert();
            logger.info("Gas Station Purchase alert is deleted");
        }
        else if(alertSubType1.equals("International Transaction"))
        {
            deleteInternationaTranscationAlert();
            logger.info("International Transaction alert is deleted");
        }
        else if(alertSubType1.equals("Transaction Notification"))
        {
            deleteTranscationNotificationAlert();
            logger.info("Transaction Notification alert is deleted");
        }
        else
        {
            logger.info("Alert is not selected");
        }
    }
    public void deleteAtmTranscationAlert()
    {
        atmTranscationAlertDeleteLink.click();
        atmTranscationAlertContinueButton.click();
    }
    public void deleteCardNotPresentAlert()
    {
        cardNotPresentAlertDeleteLink.click();
        cardNotPresentAlertContinueButton.click();
    }
    public void deleteCreditPostedAlert()
    {
        creditPostedAlertDeleteLink.click();
        creditPostedAlertContinueButton.click();

    }
    public void deleteDebitPostedAlert()
    {
        debitPostedAlertDeleteLink.click();
        debitPostedAlertContinueButton.click();
    }

    public void deleteDeclineTranscationAlert()
    {
        declineTranscationAlertDeleteLink.click();
        declineTranscationAlertContinueButton.click();
    }
    public void deleteGasStationPurchaseAlert()
    {
        gasStationPurchaseAlertDeleteLink.click();
        gasStationPurchaseAlertContinueButton.click();
    }
    public void deleteInternationaTranscationAlert()
    {
        internationalTranscationalAlertDeleteLink.click();
        internationalTranscationalAlertContinueButton.click();

    }
    public void deleteTranscationNotificationAlert()
    {
        notificationAlertDeleteLink.click();
        notificationAlertContinueButton.click();
    }

    //Farud Alert functionality
    public void clickFraudertsLink() {
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        fraudAlertsTabAlertPage.click();
    }

    public void clickUpdateContactInfo()
    {
        updateContactInfoFraudAlert.click();
    }

    public void verifyPageTitleFraudAlertPage()
    {
        System.out.println("Actualtext :" +actualFraudAlertPageTitle.getText());
        Assert.assertEquals("One-time passcode",actualFraudAlertPageTitle.getText());
        logger.info("Passed:One-time passcode title is dispalyed successfully");
        cancelButtonFraudAlert.click();
    }

    //Verify the functionality of Set up Balance alert when amount field is empty
    public void setUpAlertEmptyAmountfieldForBalanceTranferSubTypes (String alertSubType,String emailId )
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType.equals("Available Credit"))
        {
            availblecreditLinkAlert.click();
            availbleCreditAlertSetUpLink.click();
            setUpAmountfeildEmptyAvailableAlertFunctionality(emailId);
            logger.info("Passed:Available credit alert is selected");
        }
        else if(alertSubType.equals("Balance Exceeds"))
        {
            balanceexceedsLinkAlert.click();
            balanceExceedsSetupLink.click();
            setUpAmountfeildEmptyBalanceExceedsAlertFunctionality(emailId);
            logger.info("Passed:Balance exceeds alert is selected");
        }
        else
        {
            System.out.println("alert is not selected");
        }
    }
    public void setUpAmountfeildEmptyAvailableAlertFunctionality(String emailId)
    {
        availbleCreditAlertAmount.clear();
        new Select(availbleCreditEmailDropdown).selectByIndex(0);
        new Select(availbleCreditAlertEmailValue).selectByValue("Add New Email");
        availbleCreditEmailTextBox.sendKeys(emailId);
        availbleCreditsaveButton.click();

    }
    public void setUpAmountfeildEmptyBalanceExceedsAlertFunctionality(String emailId)
    {
        balanceExceedsamount.clear();
        new Select(balanceExceedsContactSelectdropdown).selectByIndex(0);
        new Select(selectBalanceExceedsEmailvalue).selectByValue("Add New Email");
        balanceExceedsContactEmailTextbox.sendKeys(emailId);
        balanceExceedsSaveButton.click();
    }
    public void verifyAmountFieldEmptyAlertErroressage( String alertSubType){
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType.equals("Available Credit"))
        {
            Assert.assertEquals("Please enter a valid dollar amount for the Available Credit amount field.", actualErrorMessageForAmountFieldAvailabeCreditAlert.getText());
            logger.info("Passed:Please enter a valid dollar amount for the Available Credit amount field message is displayed successfully");
        }
        else if(alertSubType.equals("Balance Exceeds"))
        {
            Assert.assertEquals("Please enter a valid dollar amount for the Account Balance Exceeds field.", actualErrorMessageForAmountFieldBalanceExceedsAlert.getText());
            logger.info("Passed:Please enter a valid dollar amount for the Account Balance Exceeds field message is displayed successfully");
        }
    }

    //Functionality of balnce alert when all the fields are empty
    public void setUpAlertEmptyAllfieldForBalanceTranferSubTypes (String alertSubType)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType.equals("Available Credit"))
        {
            //availblecreditLinkAlert.click();
            availbleCreditAlertSetUpLink.click();
            logger.info("Passed:Available credit alert is selected");
            availbleCreditsaveButton.click();
        }
        else if(alertSubType.equals("Balance Exceeds"))
        {
            //balanceexceedsLinkAlert.click();
            balanceExceedsSetupLink.click();
            logger.info("Passed:Balance exceeds alert is selected");
            balanceExceedsSaveButton.click();
        }
        else
        {
            System.out.println("alert is not selected");
        }
    }

    public void verifyFieldEmptyAlertErroressage( String alertSubType){
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType.equals("Available Credit"))
        {
            System.out.println((actualErrorMessageForEmailFieldBalanceAlert.getText()));
            Assert.assertEquals("An Address is required. Please enter a valid address.", actualErrorMessageForEmailFieldBalanceAlert.getText());
            Assert.assertEquals("Please enter a valid dollar amount for the Available Credit amount field.", actualErrorMessageForAmountFieldAvailabeCreditAlert.getText());
            logger.info("Passed:Error message displayed for Amount and Email Field ");
        }
        else if(alertSubType.equals("Balance Exceeds"))
        {
            Assert.assertEquals("An Address is required. Please enter a valid address.", actualErrorMessageForEmailFieldBalanceAlert.getText());
            Assert.assertEquals("Please enter a valid dollar amount for the Account Balance Exceeds field.", actualErrorMessageForAmountFieldBalanceExceedsAlert.getText());
            logger.info("Passed:Error message displayed for Email and Amount Field ");
        }
    }

    //Incorrect fromat of Amount Field
    public void verifyIncorrectFormatAmountFieldEmptyAlertErroressage( String alertSubType)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType.equals("Available Credit"))
        {
            Assert.assertEquals("The Available Credit amount must be a numeric value.", actualErrorMessageForInformatAmountFieldAvailableCreditAlert.getText());
            logger.info("Passed:The Available Credit amount must be a numeric value message is dispalyed ");
        }
        else if(alertSubType.equals("Balance Exceeds"))
        {
            Assert.assertEquals("The Account Balance Exceeds must be a numeric value.", actualErrorMessageForInformatAmountFieldBalanceExceedsAlert.getText());
            logger.info("Passed:The Account Balance Exceeds must be a numeric value message is dispayed");
        }
    }

    //debit posted Alert when all fields are empty
    public void DebitPostedAlertErrorForAllEmptyFields()
    {
        deditPostedAlertSetupLink.click();
        addDebitPostedAmount.clear();
        debitPostedSaveButton.click();
    }
    public void DebitPostedAlertErrorForRadioButton(String emailid1)
    {
        deditPostedAlertSetupLink.click();
        addDebitPostedAmount.sendKeys("1");
        new Select(debitPostedContactSelectdropdown).selectByIndex(0);
        new Select(selectDebitPostedEmailvalue).selectByValue("Add New Email");
        debitPostedContactEmailTextbox.sendKeys(emailid1);
        debitPostedSaveButton.click();
    }
    public void DebitPostedAlertErrorForAmountField(String emailid1)
    {
        deditPostedAlertSetupLink.click();
        selectDebitPostedAmount.click();
        addDebitPostedAmount.click();
        new Select(debitPostedContactSelectdropdown).selectByIndex(0);
        new Select(selectDebitPostedEmailvalue).selectByValue("Add New Email");
        debitPostedContactEmailTextbox.sendKeys(emailid1);
        debitPostedSaveButton.click();
    }

    public void debitPostedAlertErrorForIncorrectAmount(String emailid1,String amount)
    {
        deditPostedAlertSetupLink.click();
        selectDebitPostedAmount.click();
        addDebitPostedAmount.sendKeys(amount);
        new Select(debitPostedContactSelectdropdown).selectByIndex(0);
        new Select(selectDebitPostedEmailvalue).selectByValue("Add New Email");
        debitPostedContactEmailTextbox.sendKeys(emailid1);
        debitPostedSaveButton.click();
    }

    public void verifyErrorMessageForEmailIDFieldDebitPostedAlert()
    {
        Assert.assertEquals("An Address is required. Please enter a valid address.",actualErrorMessageForEmailIDDebitPostedAlert.getText());
        logger.info("An Address is required. Please enter a valid address is dispalyed successfully");

    }
    public void verifyErrorMessageForRadioButtonDebitPostedAlert()
    {
        Assert.assertEquals("Please indicate if you wish the alert to be sent when the debit is less than, equal to or greater than the amount you have selected.",actualErrorMessageForRadiobuttonDebitPostedAlert.getText());
        logger.info("Error message for radio button is dispalyed successfully");

    }
    public void verifyErrorMessageForAmountFieldDebitPostedAlert()
    {
        Assert.assertEquals("Please enter a valid dollar amount for the debit you would like to track.",actualErrorMessageForAmountFieldDebitPostedAlert.getText());
        logger.info("Please enter valid amount message is dispalyed successfully");
    }
    public void verifyErrorMessageForIncorrectformatAmountFieldDebitPostedAlert()
    {
        Assert.assertEquals("The debit amount must be a numeric value.",actualErrorMessageForInformatAmountFieldDebitPostedAlert.getText());
        logger.info("The debit amount must be a numeric value is dispalyed successfully");
    }

    //Travel Notification Error handling
    public void getErrorTranscationForTravelNotificationAlert(String emailid1)
    {
        transcationNotificationAlertSetupLink.click();
        notificationAmount.clear();
        new Select(notificationContactSelectdropdown).selectByIndex(0);
        new Select(selectNotificationEmailvalue).selectByValue("Add New Email");
        notificationContactEmailTextbox.sendKeys(emailid1);
        notificationSaveButton.click();
    }
    public void verifyErrorMessageForTravelNotificationAlert()
    {
        Assert.assertEquals("Please enter a valid dollar amount for the Authorization Amount field.",actualErrorMessageForTravelNotificationAlert.getText());
        logger.info("Please enter a valid dollar amount for the Authorization Amount field message is dispalyed");
    }

    //Payment Due Error handling
    public void getErrorPaymentDueAlerts(String emailId){
        setUpLinkForpaymentDue.click();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        new Select(paymentDueSelectdropdown).selectByIndex(0);
        new Select(paymentDueContactEmailvalue).selectByValue("Add New Email");
        paymentDueContactEmailTextbox.sendKeys(emailId);
        paymentDueSaveButton.click();
    }
    public void getErrorForAllFiledPaymentDueAlerts()
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        setUpLinkForpaymentDue.click();
        paymentDueSaveButton.click();
    }
    public void verifyErrorMessageForPaymentDueAlert() {
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        Assert.assertEquals("Please select the number of days prior to the due date that you would like the alert to be sent.", actualErrorMessageForPaymentDueAlert.getText());
        logger.info("Please select the number of days prior to the due date that you would like the alert to be sent message is dispalyed ");
    }

    //comman error for balance alert
    public void commonBalanceTranferSubTypes (String alertSubType,String otherAlertSubType)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType.equals("Available Credit") && otherAlertSubType.equals(otherAlertSubType))
        {
            availblecreditLinkAlert.click();
            availbleCreditAlertSetUpLink.click();
            logger.info("Passed:Available credit alert is selected");
            balanceExceedsSetupLink.click();
            logger.info("User has selected "+alertSubType+" and trying to select "+otherAlertSubType);

        }
        else if(alertSubType.equals("Balance Exceeds") &&otherAlertSubType.equals("Available Credit"))
        {
            balanceexceedsLinkAlert.click();
            balanceExceedsSetupLink.click();
            logger.info("Passed:Balance exceeds alert is selected");
            availbleCreditAlertSetUpLink.click();
            logger.info("User has selected "+alertSubType+" and trying to select "+otherAlertSubType);
        }
        else if(alertSubType.equals("Balance Exceeds") &&otherAlertSubType.equals("Balance Exceeds"))
        {
            logger.info("Both the alerts are same, please select the different sub alert type");
        }
        else if(alertSubType.equals("Available Credit") &&otherAlertSubType.equals("Available Credit"))
        {
            logger.info("Both the alerts are same, please select the different sub alert type");
        }
        else
        {
            logger.info("alert is not selected");
        }

    }

    public void invalidSelectionAlertPopUp()
    {
        Assert.assertEquals("Invalid selection",dialog1Title.getText());
        Assert.assertEquals("You can manage only one alert at a time. Please use the Cancel or Save buttons.",dialog1Desc.getText());
        logger.info("dialog is getting displayed with title Invalid selected with description when user selects other sub alert without closing the previous alert ");
        okbtnInDialog.click();
        logger.info("dialog is closed");
    }


    //comman error for Tanscation Alert
    public void commanErrorForTranscationSubTypes(String alertSubType1,String otherAlertSubType)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType1.equals("ATM Transaction") && otherAlertSubType.equals(otherAlertSubType))
        {
            atmTranscationAlertSetupLink.click();
            System.out.println("ATMTranscation alert is selected");
            cardNotPresentAlertSetupLink.click();
            logger.info("User has selected "+alertSubType1+" and trying to select "+otherAlertSubType);
        }
        else if(alertSubType1.equals("Card Not Present")&& otherAlertSubType.equals(otherAlertSubType))
        {
            cardNotPresentAlertSetupLink.click();
            System.out.println("cardNotPresent alert is selected");
            creditPostedAlertSetupLink.click();
            logger.info("User has selected "+alertSubType1+" and trying to select "+otherAlertSubType);

        }
        else if(alertSubType1.equals("Credit Posted")&& otherAlertSubType.equals(otherAlertSubType))
        {
            creditPostedAlertSetupLink.click();
            System.out.println("Credit Posted alert is selected");
            deditPostedAlertSetupLink.click();
            logger.info("User has selected "+alertSubType1+" and trying to select "+otherAlertSubType);
        }
        else if(alertSubType1.equals("Debit Posted")&& otherAlertSubType.equals(otherAlertSubType))
        {
            deditPostedAlertSetupLink.click();
            System.out.println("Debit Posted alert is selected");
            declineTranscationAlertSetupLink.click();
            logger.info("User has selected "+alertSubType1+" and trying to select "+otherAlertSubType);
        }
        else if(alertSubType1.equals("Decline Transaction")&& otherAlertSubType.equals(otherAlertSubType))
        {
            declineTranscationAlertSetupLink.click();
            System.out.println("Decline Transaction alert is selected");
            gasStationPurchaseAlertSetupLink.click();
            logger.info("User has selected "+alertSubType1+" and trying to select "+otherAlertSubType);
        }
        else if(alertSubType1.equals("Gas Station Purchase") && otherAlertSubType.equals(otherAlertSubType))
        {
            gasStationPurchaseAlertSetupLink.click();
            System.out.println("Gas Station Purchase alert is selected");
            internationaTranscationAlertSetupLink.click();
            logger.info("User has selected "+alertSubType1+" and trying to select "+otherAlertSubType);
        }
        else if(alertSubType1.equals("International Transaction")&& otherAlertSubType.equals(otherAlertSubType))
        {
            internationaTranscationAlertSetupLink.click();
            System.out.println("International Transaction alert is selected");
            gasStationPurchaseAlertSetupLink.click();
            logger.info("User has selected "+alertSubType1+" and trying to select "+otherAlertSubType);
        }
        else if(alertSubType1.equals("Transaction Notification")&& otherAlertSubType.equals(otherAlertSubType))
        {
            transcationNotificationAlertSetupLink.click();
            System.out.println("Transaction Notification alert is selected");
            internationaTranscationAlertSetupLink.click();
            logger.info("User has selected "+alertSubType1+" and trying to select "+otherAlertSubType);
        }
        else
        {
            System.out.println("Alert is not selected");
        }
    }

    //comman Error for Statement payment alert
    public void commanErrorForStatementAndPaymentSubTypes(String alertSubType,String otherAlertSubType) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if (alertSubType.equals("Online Statement Available") && otherAlertSubType.equals(otherAlertSubType)) {
            onlineStatmentAvailablelink.click();
            setUpLinkForOnlineStatmentAvailable.click();
            logger.info("Online Statement Available alert is selected");
            setUpLinkForAutoPayScheduledandAutoPayProcessed.click();
            logger.info("User has selected "+alertSubType+" and trying to select "+otherAlertSubType);
        }
        else if (alertSubType.equals("AutoPay Scheduled and AutoPay Processed")&& otherAlertSubType.equals(otherAlertSubType)) {
            AutoPayScheduledandAutoPayProcessedlink.click();
            setUpLinkForAutoPayScheduledandAutoPayProcessed.click();
            logger.info("AutoPay Scheduled and AutoPay Processed alert is selected");
            setUpLinkForpaymentDue.click();
            logger.info("User has selected "+alertSubType+" and trying to select "+otherAlertSubType);
        }
        else if (alertSubType.equals("Payment Due")&& otherAlertSubType.equals(otherAlertSubType)) {
            paymentDuelink.click();
            setUpLinkForpaymentDue.click();
            logger.info("Payment Due alert is selected");
            setUpLinkForpaymentOverdue.click();
            logger.info("User has selected "+alertSubType+" and trying to select "+otherAlertSubType);
        }
        else if (alertSubType.equals("Payment Overdue")&& otherAlertSubType.equals(otherAlertSubType)) {
            paymentOverduelink.click();
            setUpLinkForpaymentOverdue.click();
            logger.info("Payment Overdue alert is selected");
            setUpLinkForpaymentPosted.click();
            logger.info("User has selected "+alertSubType+" and trying to select "+otherAlertSubType);
        }
        else if (alertSubType.equals("Payment Posted")&& otherAlertSubType.equals(otherAlertSubType)) {
            paymentPostedlink.click();
            setUpLinkForpaymentPosted.click();
            logger.info("Payment Overdue alert is selected");
            setUpLinkForpaymentSchedule.click();
            logger.info("User has selected "+alertSubType+" and trying to select "+otherAlertSubType);
        }
        else if (alertSubType.equals("Payment Scheduled")&& otherAlertSubType.equals(otherAlertSubType)) {
            paymentSchedulelink.click();
            setUpLinkForpaymentSchedule.click();
            logger.info("Payment Overdue alert is selected");
            setUpLinkForpaymentPosted.click();
            logger.info("User has selected "+alertSubType+" and trying to select "+otherAlertSubType);
        }
        else
            logger.info("Please verify the subtype name of alert");
    }

    public void setUpAlertAlertForBalanceTranferSubTypesWithoutEmailValue (String alertSubType,String Amount)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType.equals("Available Credit"))
        {
            availblecreditLinkAlert.click();
            availbleCreditAlertSetUpLink.click();
            setUpAvailableAlertWithoutEmailValue(Amount);
            logger.info("Passed:Available credit alert is selected");
        }
        else if(alertSubType.equals("Balance Exceeds"))
        {
            balanceexceedsLinkAlert.click();
            balanceExceedsSetupLink.click();
            setUpBalanceExceedsAlertWithoutEmailValue(Amount);
            logger.info("Passed:Balance exceeds alert is selected");
        }
        else
        {
            System.out.println("alert is not selected");
        }
    }
    public void setUpAvailableAlertWithoutEmailValue(String Amount)
    {
        availbleCreditAlertAmount.sendKeys(Amount);
        new Select(availbleCreditEmailDropdown).selectByIndex(0);
        /*not inputting the vale value for email Id field to test the negative scenario*/
        String emailValue=new Select(availbleCreditAlertEmailValue).getFirstSelectedOption().getText();
        Assert.assertEquals(emailValue,"None Selected");
        //new Select(availbleCreditAlertEmailValue).selectByValue("Add New Email");

        //availbleCreditEmailTextBox.sendKeys();
        availbleCreditsaveButton.click();

    }
    public void setUpBalanceExceedsAlertWithoutEmailValue(String Amount)
    {
        balanceExceedsamount.sendKeys(Amount);
        new Select(balanceExceedsContactSelectdropdown).selectByIndex(0);
        /*not inputting the vale value for email Id field to test the negative scenario*/
        String emailValue=new Select(selectBalanceExceedsEmailvalue).getFirstSelectedOption().getText();
        Assert.assertEquals(emailValue,"None Selected");
        // new Select(selectBalanceExceedsEmailvalue).selectByValue("Add New Email");

        //  balanceExceedsContactEmailTextbox.sendKeys(emailId);
        balanceExceedsSaveButton.click();
    }



    public void setUpAlertForTranscationSubTypesWithEmailNonSelected(String alertSubType1)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType1.equals("ATM Transaction"))
        {
            atmTranscationAlertSetupLink.click();
            System.out.println("ATMTranscation alert is selected");
            atmTranscationSaveButton.click();
        }
        else if(alertSubType1.equals("Card Not Present"))
        {
            cardNotPresentAlertSetupLink.click();
            System.out.println("cardNotPresent alert is selected");
            cardNotPresentSaveButton.click();

        }
        else if(alertSubType1.equals("Credit Posted"))
        {
            creditPostedAlertSetupLink.click();
            System.out.println("Credit Posted alert is selected");
            creditPostedSaveButton.click();

        }
        else if(alertSubType1.equals("Debit Posted"))
        {
            deditPostedAlertSetupLink.click();
            addDebitPostedAmount.sendKeys("1");
            System.out.println("Debit Posted alert is selected");
            debitPostedSaveButton.click();
        }
        else if(alertSubType1.equals("Decline Transaction"))
        {
            declineTranscationAlertSetupLink.click();
            System.out.println("Decline Transaction alert is selected");
            declineTrascationSaveButton.click();
        }
        else if(alertSubType1.equals("Gas Station Purchase"))
        {
            gasStationPurchaseAlertSetupLink.click();
            System.out.println("Gas Station Purchase alert is selected");
            gasStationPurchaseSaveButton.click();
        }
        else if(alertSubType1.equals("International Transaction"))
        {
            internationaTranscationAlertSetupLink.click();
            System.out.println("International Transaction alert is selected");
            internationalTranscationalSaveButton.click();
        }
        else if(alertSubType1.equals("Transaction Notification"))
        {
            transcationNotificationAlertSetupLink.click();
            notificationAmount.sendKeys("1");
            System.out.println("Transaction Notification alert is selected");
            notificationSaveButton.click();
        }
        else
        {
            System.out.println("Alert is not selected");
        }
    }

    public void setUpAlertForStatementAndPaymentSubTypesWithNoEmailValue(String alertSubType) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if (alertSubType.equals("Online Statement Available")) {
            onlineStatmentAvailablelink.click();
            setUpLinkForOnlineStatmentAvailable.click();
            logger.info("Online Statement Available alert is selected");
            new Select(onlineStatmentAvailableSelectdropdown).selectByIndex(0);
            onlineStatmentAvailableSaveButton.click();
        }
        else if (alertSubType.equals("AutoPay Scheduled and AutoPay Processed")) {
            AutoPayScheduledandAutoPayProcessedlink.click();
            setUpLinkForAutoPayScheduledandAutoPayProcessed.click();
            logger.info("AutoPay Scheduled and AutoPay Processed alert is selected");
            new Select(AutoPayScheduledandAutoPayProcessedSelectdropdown).selectByIndex(0);
            AutoPayScheduledandAutoPayProcessedSaveButton.click();
        }
        else if (alertSubType.equals("Payment Due")) {
            paymentDuelink.click();
            setUpLinkForpaymentDue.click();
            logger.info("Payment Due alert is selected");
            new Select(numberOfDaysPriorToDueDateSelectdropdown).selectByIndex(2);
            new Select(paymentDueSelectdropdown).selectByIndex(0);
            paymentDueSaveButton.click();
        }
        else if (alertSubType.equals("Payment Overdue")) {
            paymentOverduelink.click();
            setUpLinkForpaymentOverdue.click();
            logger.info("Payment Overdue alert is selected");
            new Select(paymentOverdueSelectdropdown).selectByIndex(0);
            paymentOverdueSaveButton.click();
        }
        else if (alertSubType.equals("Payment Posted")) {
            paymentPostedlink.click();
            setUpLinkForpaymentPosted.click();
            logger.info("Payment Overdue alert is selected");
            new Select(paymentPostedSelectdropdown).selectByIndex(0);
            paymentPostedSaveButton.click();
        }
        else if (alertSubType.equals("Payment Scheduled")) {
            paymentSchedulelink.click();
            setUpLinkForpaymentSchedule.click();
            logger.info("Payment Overdue alert is selected");
            new Select(paymentScheduleSelectdropdown).selectByIndex(0);
            paymentScheduleSaveButton.click();
        }
        else
            logger.info("Please verify the subtype name of alert");
    }

    public void setSecurityAlertswithNoEmailValue(){
        new Select(securityContactSelectdropdown).selectByIndex(0);
        new Select(SelectSecurityContactEmailvalue).selectByValue("Add New Email");
        securitySaveButton.click();
    }

    public void editAlertForTranscationSubTypesWithoutChangingEmailvalue(String alertSubType1)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType1.equals("ATM Transaction"))
        {
            atmTranscationEditLink.click();
            logger.info("ATMTranscation alert is selected");
            new Select(atmTranscationContactSelectdropdown).selectByIndex(0);
            /*new Select(selectAtmTranscationEmailvalue).selectByValue("Add New Email");
            atmTranscationContactEmailTextbox.sendKeys(Editemailid1);*/
            atmTranscationSaveButton.click();
        }
        else if(alertSubType1.equals("Card Not Present"))
        {
            cardNotPresentEditLink.click();
            logger.info("cardNotPresent alert is selected");
            new Select(cardNotPresentContactSelectdropdown).selectByIndex(0);
            /*new Select(selectCardNotPresentEmailvalue).selectByValue("Add New Email");
            cardNotPresentContactEmailTextbox.sendKeys(Editemailid1);*/
            cardNotPresentSaveButton.click();

        }
        else if(alertSubType1.equals("Credit Posted"))
        {
            creditPostedEditLink.click();
            logger.info("Credit Posted alert is selected");
            new Select(creditPostedContactSelectdropdown).selectByIndex(0);
           /* new Select(selectCreditPostedEmailvalue).selectByValue("Add New Email");
            creditPostedContactEmailTextbox.sendKeys(Editemailid1);*/
            creditPostedSaveButton.click();

        }
        else if(alertSubType1.equals("Debit Posted"))
        {
            deditPostedAlertSetupLink.click();
            logger.info("dedit Posted alert is selected");
            new Select(debitPostedContactSelectdropdown).selectByIndex(0);
            /*new Select(selectDebitPostedEmailvalue).selectByValue("Add New Email");
            debitPostedContactEmailTextbox.sendKeys(Editemailid1);*/
            debitPostedSaveButton.click();
        }
        else if(alertSubType1.equals("Decline Transaction"))
        {
            declineTrascationEditLink.click();
            logger.info("Decline Transaction alert is selected");
            new Select(declineTrascationContactSelectdropdown).selectByIndex(0);
            /*new Select(selectDeclineTrascationEmailvalue).selectByValue("Add New Email");
            declineTrascationContactEmailTextbox.sendKeys(Editemailid1);*/
            declineTrascationSaveButton.click();
        }
        else if(alertSubType1.equals("Gas Station Purchase"))
        {
            gasStationPurchaseEditLink.click();
            logger.info("Gas Station Purchase alert is selected");
            new Select(gasStationPurchaseContactSelectdropdown).selectByIndex(0);
           /* new Select(selectGasStationPurchaseEmailvalue).selectByValue("Add New Email");
            gasStationPurchaseContactEmailTextbox.sendKeys(Editemailid1);*/
            gasStationPurchaseSaveButton.click();
        }
        else if(alertSubType1.equals("International Transaction"))
        {
            internationalTranscationalEditLink.click();
            logger.info("International Transaction alert is selected");
            new Select(internationalTranscationalContactSelectdropdown).selectByIndex(0);
            /*new Select(selectInternationalTranscationalEmailvalue).selectByValue("Add New Email");
            internationalTranscationaleContactEmailTextbox.sendKeys(Editemailid1);*/
            internationalTranscationalSaveButton.click();
        }
        else if(alertSubType1.equals("Transaction Notification"))
        {
            notificationEditLink.click();
            logger.info("Transaction Notification alert is selected");
            new Select(notificationContactSelectdropdown).selectByIndex(0);
            /*new Select(selectNotificationEmailvalue).selectByValue("Add New Email");
            notificationContactEmailTextbox.sendKeys(Editemailid1);*/
            notificationSaveButton.click();

        }
        else
        {
            logger.info("Alert is not selected");
        }
    }
    public void invalidSelectionAlertPopUpForTranscationSubTypes(String alertSubType1, String otherSubType)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType1.equals("ATM Transaction"))
        {

            atmTranscationEditLink.click();
            logger.info("ATMTranscation alert is selected");
            selectingOtherSubAlertTypeForTransaction(otherSubType);
        }
        else if(alertSubType1.equals("Card Not Present"))
        {
            cardNotPresentEditLink.click();
            logger.info("cardNotPresent alert is selected");
            selectingOtherSubAlertTypeForTransaction(otherSubType);


        }
        else if(alertSubType1.equals("Credit Posted"))
        {
            creditPostedEditLink.click();
            logger.info("Credit Posted alert is selected");
            selectingOtherSubAlertTypeForTransaction(otherSubType);


        }
        else if(alertSubType1.equals("Debit Posted"))
        {
            debitPostedEditLink.click();
            logger.info("dedit Posted alert is selected");
            selectingOtherSubAlertTypeForTransaction(otherSubType);

        }
        else if(alertSubType1.equals("Decline Transaction"))
        {
            declineTrascationEditLink.click();
            logger.info("Decline Transaction alert is selected");
            selectingOtherSubAlertTypeForTransaction(otherSubType);

        }
        else if(alertSubType1.equals("Gas Station Purchase"))
        {
            gasStationPurchaseEditLink.click();
            logger.info("Gas Station Purchase alert is selected");
            selectingOtherSubAlertTypeForTransaction(otherSubType);

        }
        else if(alertSubType1.equals("International Transaction"))
        {
            internationalTranscationalEditLink.click();
            logger.info("International Transaction alert is selected");
            selectingOtherSubAlertTypeForTransaction(otherSubType);

        }
        else if(alertSubType1.equals("Transaction Notification"))
        {
            notificationEditLink.click();
            logger.info("Transaction Notification alert is selected");
            selectingOtherSubAlertTypeForTransaction(otherSubType);


        }
        else
        {
            logger.info("Alert is not selected");
        }
    }
    public void selectingOtherSubAlertTypeForTransaction(String otherSubType){
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(otherSubType.equals("ATM Transaction"))
        {
            atmTranscationAlertSetupLink.click();
            System.out.println("ATMTranscation alert is selected");
        }
        else if(otherSubType.equals("Card Not Present"))
        {
            cardNotPresentAlertSetupLink.click();
            System.out.println("cardNotPresent alert is selected");

        }
        else if(otherSubType.equals("Credit Posted"))
        {
            creditPostedAlertSetupLink.click();
            System.out.println("Credit Posted alert is selected");


        }
        else if(otherSubType.equals("Debit Posted"))
        {
            deditPostedAlertSetupLink.click();
            System.out.println("Debit Posted alert is selected");

        }
        else if(otherSubType.equals("Decline Transaction"))
        {
            declineTranscationAlertSetupLink.click();
            System.out.println("Decline Transaction alert is selected");

        }
        else if(otherSubType.equals("Gas Station Purchase"))
        {
            gasStationPurchaseAlertSetupLink.click();
            System.out.println("Gas Station Purchase alert is selected");

        }
        else if(otherSubType.equals("International Transaction"))
        {
            internationaTranscationAlertSetupLink.click();
            System.out.println("International Transaction alert is selected");

        }
        else if(otherSubType.equals("Transaction Notification"))
        {
            transcationNotificationAlertSetupLink.click();
            System.out.println("Transaction Notification alert is selected");

        }
        else
        {
            System.out.println("Alert is not selected");
        }
    }
    public void invalidSelectionAlertPopUpForStatementAndPaymentSubTypes(String alertSubType, String otherSubType){
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if (alertSubType.equals("Online Statement Available")) {
            onlineStatmentAvailablelink.click();
            editLinkForOnlineStatmentAvailable.click();
            logger.info("Online Statement Available alert is selected");
            selectingOtherSubAlertTypeForStatementAndPayment(otherSubType);

        }
        else if (alertSubType.equals("AutoPay Scheduled and AutoPay Processed")) {
            AutoPayScheduledandAutoPayProcessedlink.click();
            editLinkForAutoPayScheduledandAutoPayProcessed.click();
            logger.info("AutoPay Scheduled and AutoPay Processed alert is selected");
            selectingOtherSubAlertTypeForStatementAndPayment(otherSubType);
        }
        else if (alertSubType.equals("Payment Due")) {
            paymentDuelink.click();
            editLinkForpaymentDue.click();
            logger.info("Payment Due alert is selected");
            selectingOtherSubAlertTypeForStatementAndPayment(otherSubType);
        }
        else if (alertSubType.equals("Payment Overdue")) {
            paymentOverduelink.click();
            editLinkForpaymentOverdue.click();
            logger.info("Payment Overdue alert is selected");
            selectingOtherSubAlertTypeForStatementAndPayment(otherSubType);
        }
        else if (alertSubType.equals("Payment Posted")) {
            paymentPostedlink.click();
            editUpLinkForpaymentPosted.click();
            logger.info("Payment Overdue alert is selected");
            selectingOtherSubAlertTypeForStatementAndPayment(otherSubType);
        }
        else if (alertSubType.equals("Payment Scheduled")) {
            paymentSchedulelink.click();
            editLinkForpaymentSchedule.click();
            logger.info("Payment Overdue alert is selected");
            selectingOtherSubAlertTypeForStatementAndPayment(otherSubType);
        }
        else
            logger.info("Please verify the subtype name of alert");
    }

    public void selectingOtherSubAlertTypeForStatementAndPayment(String otherSubType){
        if (otherSubType.equals("Online Statement Available")) {
            onlineStatmentAvailablelink.click();
            logger.info("User is trying to click on "+otherSubType);
        }
        else if (otherSubType.equals("AutoPay Scheduled and AutoPay Processed")) {
            AutoPayScheduledandAutoPayProcessedlink.click();
            logger.info("User is trying to click on "+otherSubType);
        }
        else if (otherSubType.equals("Payment Due")) {
            paymentDuelink.click();
            logger.info("User is trying to click on "+otherSubType);
        }
        else if (otherSubType.equals("Payment Overdue")) {
            paymentOverduelink.click();
            logger.info("User is trying to click on "+otherSubType);
        }
        else if (otherSubType.equals("Payment Posted")) {
            paymentPostedlink.click();
            logger.info("User is trying to click on "+otherSubType);
        }
        else if (otherSubType.equals("Payment Scheduled")) {
            paymentSchedulelink.click();
            logger.info("User is trying to click on "+otherSubType);
        }
        else
            logger.info("Please verify the subtype name of alert");


    }

    public void WithoutEditingExistingValuesForForBalanceTranferSubTypes (String alertSubType)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType.equals("Available Credit"))
        {
            availblecreditLinkAlert.click();
            availableCreditEditLink.click();
            logger.info("Available credit alert is selected");
            availbleCreditsaveButton.click();

        }
        else if(alertSubType.equals("Balance Exceeds"))
        {
            balanceexceedsLinkAlert.click();
            logger.info("Balance exceeds alert is selected");
            balanceExceedsEditLink.click();
            balanceExceedsSaveButton.click();
        }
        else
        {
            logger.info("alert is not selected");
        }
    }

    public void WithoutEditingExistingValuesForStatementAndPaymentSubTypes(String alertSubType) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if (alertSubType.equals("Online Statement Available")) {
            onlineStatmentAvailablelink.click();
            editLinkForOnlineStatmentAvailable.click();
            logger.info("Online Statement Available alert is selected");
            new Select(onlineStatmentAvailableSelectdropdown).selectByIndex(0);
            onlineStatmentAvailableSaveButton.click();
        }
        else if (alertSubType.equals("AutoPay Scheduled and AutoPay Processed")) {
            AutoPayScheduledandAutoPayProcessedlink.click();
            editLinkForAutoPayScheduledandAutoPayProcessed.click();
            logger.info("AutoPay Scheduled and AutoPay Processed alert is selected");
            new Select(AutoPayScheduledandAutoPayProcessedSelectdropdown).selectByIndex(0);
            AutoPayScheduledandAutoPayProcessedSaveButton.click();
        }
        else if (alertSubType.equals("Payment Due")) {
            paymentDuelink.click();
            editLinkForpaymentDue.click();
            logger.info("Payment Due alert is selected");
            new Select(numberOfDaysPriorToDueDateSelectdropdown).selectByIndex(2);
            new Select(paymentDueSelectdropdown).selectByIndex(0);
            paymentDueSaveButton.click();
        }
        else if (alertSubType.equals("Payment Overdue")) {
            paymentOverduelink.click();
            editLinkForpaymentOverdue.click();
            logger.info("Payment Overdue alert is selected");
            new Select(paymentOverdueSelectdropdown).selectByIndex(0);
            paymentOverdueSaveButton.click();
        }
        else if (alertSubType.equals("Payment Posted")) {
            paymentPostedlink.click();
            editUpLinkForpaymentPosted.click();
            logger.info("Payment Overdue alert is selected");
            new Select(paymentPostedSelectdropdown).selectByIndex(0);
            paymentPostedSaveButton.click();
        }
        else if (alertSubType.equals("Payment Scheduled")) {
            paymentSchedulelink.click();
            editLinkForpaymentSchedule.click();
            logger.info("Payment Overdue alert is selected");
            new Select(paymentScheduleSelectdropdown).selectByIndex(0);
            paymentScheduleSaveButton.click();
        }
        else
            logger.info("Please verify the subtype name of alert");
    }

    public void WithoutEditingExistingValuesOfSecurityAlerts(){

        new Select(securityContactSelectdropdown).selectByIndex(0);
        securitySaveButton.click();
    }
    public void getErrorMessageWhenEmailNonSelected(){
        String ActualErrorMessage="! "+emailNonSelectedErrorMessage.getText();
        Assert.assertEquals("! An Address is required. Please enter a valid address.",ActualErrorMessage);
        logger.info("Successfully getting the error message, when email id is not selected for the sub alert while setting up the alert. ");
    }

    public void getErrorMessageForIncorrectFormateOfEmail(){
        String ActualErrorMessage="! "+incorrectFormatEmailErrorMessage.getText();
        Assert.assertEquals("! The address entered in Address 1 is invalid. Please review and re-enter the address.",ActualErrorMessage);
        logger.info("Successfully getting the error message, When the email id is in incorrect format for the sub alert while setting up the alert. ");
    }

    public void getErrorMessagewhileinputtingExistingEmail(){
        String ActualErrorMessage="! "+existingemailwhieEditErrorMessage.getText();
        Assert.assertEquals("! No updates were made to this Alert.  Please make your updates below and select Save to save your changes.",ActualErrorMessage);
        logger.info("Successfully getting the error message, while trying to save without editing the pre-existing value when editing the alert. ");
    }

    public void selectingOtherSubAlertForForBalanceTranferSubTypes (String alertSubType,String otherAlertSubType)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType.equals("Available Credit") && otherAlertSubType.equals(otherAlertSubType))
        {
            availblecreditLinkAlert.click();
            availableCreditEditLink.click();
            logger.info("Available credit alert is selected");
//            availbleCreditsaveButton.click();
            balanceexceedsLinkAlert.click();
            logger.info("User has selected "+alertSubType+" and trying to select "+otherAlertSubType);

        }
        else if(alertSubType.equals("Balance Exceeds") &&otherAlertSubType.equals("Available Credit"))
        {
            balanceexceedsLinkAlert.click();
            logger.info("Balance exceeds alert is selected");
            balanceExceedsEditLink.click();
//            balanceExceedsSaveButton.click();
            availblecreditLinkAlert.click();
            logger.info("User has selected "+alertSubType+" and trying to select "+otherAlertSubType);
        }
        else if(alertSubType.equals("Balance Exceeds") &&otherAlertSubType.equals("Balance Exceeds")){
            logger.info("Both the alerts are same, please select the different sub alert type");

        }
        else if(alertSubType.equals("Available Credit") &&otherAlertSubType.equals("Available Credit")){
            logger.info("Both the alerts are same, please select the different sub alert type");

        }
        else
        {
            logger.info("alert is not selected");
        }
    }

    //Functionaity for Balance alert with ohone number
    public void setUpAlertForBalanceTranferSubTypesUsingTextField (String alertSubType,String Amount,String mobileNumber)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType.equals("Available Credit"))
        {
            availblecreditLinkAlert.click();
            availbleCreditAlertSetUpLink.click();
            setUpAvailableAlertUsingTextField(Amount,mobileNumber);
            logger.info("Passed:Available credit alert is selected");
        }
        else if(alertSubType.equals("Balance Exceeds"))
        {
            balanceexceedsLinkAlert.click();
            balanceExceedsSetupLink.click();
            setUpBalanceExceedsAlertUsingTextField(Amount,mobileNumber);
            logger.info("Passed:Balance exceeds alert is selected");
        }
        else
        {
            System.out.println("alert is not selected");
        }
    }
    public void setUpAvailableAlertUsingTextField(String Amount,String mobileNumber)
    {
        availbleCreditAlertAmount.sendKeys(Amount);
        new Select(availbleCreditEmailDropdown).selectByIndex(1);
        new Select(availbleCreditAlertTextValue).selectByValue("Add New Mobile");
        addAvailableCreditMobileNovalue.sendKeys(mobileNumber);
        availbleCreditsaveButton.click();

    }
    public void setUpBalanceExceedsAlertUsingTextField(String Amount,String mobileNumber)
    {
        balanceExceedsamount.sendKeys(Amount);
        new Select(balanceExceedsContactSelectdropdown).selectByIndex(1);
        new Select(BalanceExceedsAlertTextValue).selectByValue("Add New Mobile");
        addBalanceExceedsMobileNovalue.sendKeys(mobileNumber);
        balanceExceedsSaveButton.click();
    }

    //Functionality for Transcation alert using Mobile number
    public void setUpAlertForTranscationSubTypesUsingMobileNo(String alertSubType1,String mobileNumber)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType1.equals("ATM Transaction"))
        {
            atmTranscationAlertSetupLink.click();
            System.out.println("ATMTranscation alert is selected");
            setAtmTranscationAlertUsingMobileNo(mobileNumber);
        }
        else if(alertSubType1.equals("Card Not Present"))
        {
            cardNotPresentAlertSetupLink.click();
            System.out.println("cardNotPresent alert is selected");
            setCardNotPresentAlertUsingMobileNo(mobileNumber);

        }
        else if(alertSubType1.equals("Credit Posted"))
        {
            creditPostedAlertSetupLink.click();
            System.out.println("Credit Posted alert is selected");
            setCreditPostedAlertUsingMobileNo(mobileNumber);

        }
        else if(alertSubType1.equals("Debit Posted"))
        {
            deditPostedAlertSetupLink.click();
            System.out.println("Debit Posted alert is selected");
            setDebitPostedAlertUsingMobileNo(mobileNumber);
        }
        else if(alertSubType1.equals("Decline Transaction"))
        {
            declineTranscationAlertSetupLink.click();
            System.out.println("Decline Transaction alert is selected");
            setDeclineTranscationAlertUsingMobileNo(mobileNumber);
        }
        else if(alertSubType1.equals("Gas Station Purchase"))
        {
            gasStationPurchaseAlertSetupLink.click();
            System.out.println("Gas Station Purchase alert is selected");
            setGasStationPurchaseAlertUsingMobileNo(mobileNumber);
        }
        else if(alertSubType1.equals("International Transaction"))
        {
            internationaTranscationAlertSetupLink.click();
            System.out.println("International Transaction alert is selected");
            setInternationaTranscationAlertUsingMobileNo(mobileNumber);
        }
        else if(alertSubType1.equals("Transaction Notification"))
        {
            transcationNotificationAlertSetupLink.click();
            System.out.println("Transaction Notification alert is selected");
            setTranscationNotificationAlertUsingMobileNo(mobileNumber);
        }
        else
        {
            System.out.println("Alert is not selected");
        }
    }
    public void setAtmTranscationAlertUsingMobileNo(String mobileNumber)
    {
        new Select(atmTranscationContactSelectdropdown).selectByIndex(1);
        new Select(AtmTranscationAlertTextValue).selectByValue("Add New Mobile");
        addAtmTranscationMobileNovalue.sendKeys(mobileNumber);
        atmTranscationSaveButton.click();
    }
    public void setCardNotPresentAlertUsingMobileNo(String mobileNumber)
    {
        new Select(cardNotPresentContactSelectdropdown).selectByIndex(1);
        new Select(CradNotPresentAlertTextValue).selectByValue("Add New Mobile");
        addCardNotPresentMobileNovalue.sendKeys(mobileNumber);
        cardNotPresentSaveButton.click();
    }
    public void setCreditPostedAlertUsingMobileNo(String mobileNumber)
    {
        new Select(creditPostedContactSelectdropdown).selectByIndex(1);
        new Select(CreditPostedAlertTextValue).selectByValue("Add New Mobile");
        addCreditPostedMobileNovalue.sendKeys(mobileNumber);
        creditPostedSaveButton.click();
    }
    public void setDebitPostedAlertUsingMobileNo(String mobileNumber)
    {
        selectDebitPostedAmount.click();
        addDebitPostedAmount.sendKeys("1");
        new Select(debitPostedContactSelectdropdown).selectByIndex(1);
        new Select(DebitPostedAlertTextValue).selectByValue("Add New Mobile");
        addDebitPostedMobileNovalue.sendKeys(mobileNumber);
        debitPostedSaveButton.click();
    }

    public void setDeclineTranscationAlertUsingMobileNo(String mobileNumber)
    {
        new Select(declineTrascationContactSelectdropdown).selectByIndex(1);
        new Select(DeclineTranscationAlertTextValue).selectByValue("Add New Mobile");
        addDeclineTranscationMobileNovalue.sendKeys(mobileNumber);
        declineTrascationSaveButton.click();
    }
    public void setGasStationPurchaseAlertUsingMobileNo(String mobileNumber)
    {
        new Select(gasStationPurchaseContactSelectdropdown).selectByIndex(1);
        new Select(GasStationAlertTextValue).selectByValue("Add New Mobile");
        addGasStationMobileNovalue.sendKeys(mobileNumber);
        gasStationPurchaseSaveButton.click();
    }
    public void setInternationaTranscationAlertUsingMobileNo(String mobileNumber)
    {
        new Select(internationalTranscationalContactSelectdropdown).selectByIndex(1);
        new Select(InternationalAlertTextValue).selectByValue("Add New Mobile");
        addInternationalMobileNovalue.sendKeys(mobileNumber);
        internationalTranscationalSaveButton.click();

    }
    public void setTranscationNotificationAlertUsingMobileNo(String mobileNumber)
    {
        notificationAmount.sendKeys("1");
        new Select(notificationContactSelectdropdown).selectByIndex(1);
        new Select(notificationAlertTextValue).selectByValue("Add New Mobile");
        addNotificationMobileNovalue.sendKeys(mobileNumber);
        notificationSaveButton.click();
    }

    //Functionality for Statement and Payment alerts
    public void setUpAlertForStatementAndPaymentSubTypesUsingMobileNo(String alertSubType,String mobileNumber) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if (alertSubType.equals("Online Statement Available")) {
            onlineStatmentAvailablelink.click();
            setUpLinkForOnlineStatmentAvailable.click();
            logger.info("Online Statement Available alert is selected");
            setonlineStatmentAvailableAlertsUsingMobileNo(mobileNumber);
        }
        else if (alertSubType.equals("AutoPay Scheduled and AutoPay Processed")) {
            AutoPayScheduledandAutoPayProcessedlink.click();
            setUpLinkForAutoPayScheduledandAutoPayProcessed.click();
            logger.info("AutoPay Scheduled and AutoPay Processed alert is selected");
            setAutoPayScheduledandAutoPayAlertsUsingMobileNo(mobileNumber);
        }
        else if (alertSubType.equals("Payment Due")) {
            paymentDuelink.click();
            setUpLinkForpaymentDue.click();
            logger.info("Payment Due alert is selected");
            setPaymentDueAlertsUsingMobileNo(mobileNumber);
        }
        else if (alertSubType.equals("Payment Overdue")) {
            paymentOverduelink.click();
            setUpLinkForpaymentOverdue.click();
            logger.info("Payment Overdue alert is selected");
            setPaymentOverdueAlertsUsingMobileNo(mobileNumber);
        }
        else if (alertSubType.equals("Payment Posted")) {
            paymentPostedlink.click();
            setUpLinkForpaymentPosted.click();
            logger.info("Payment Overdue alert is selected");
            setPaymentPostedAlertsUsingMobileNo(mobileNumber);
        }
        else if (alertSubType.equals("Payment Scheduled")) {
            paymentSchedulelink.click();
            setUpLinkForpaymentSchedule.click();
            logger.info("Payment Overdue alert is selected");
            setPaymentScheduleAlertsUsingMobileNo(mobileNumber);
        }
        else
            logger.info("Please verify the subtype name of alert");
    }

    public void setAutoPayScheduledandAutoPayAlertsUsingMobileNo(String mobileNumber){
        new Select(AutoPayScheduledandAutoPayProcessedSelectdropdown).selectByIndex(1);
        new Select(AutopayScheduledAlertTextValue).selectByValue("Add New Mobile");
        addAutopayScheduledMobileNovalue.sendKeys(mobileNumber);
        AutoPayScheduledandAutoPayProcessedSaveButton.click();
    }

    public void setonlineStatmentAvailableAlertsUsingMobileNo(String mobileNumber){
        new Select(onlineStatmentAvailableSelectdropdown).selectByIndex(1);
        new Select(onlineStatmentAvailableContactEmailvalue).selectByValue("Add New Mobile");
        onlineStatmentAvailableContactEmailTextbox.sendKeys(mobileNumber);
        onlineStatmentAvailableSaveButton.click();
    }
    public void setPaymentDueAlertsUsingMobileNo(String mobileNumber){
        new Select(numberOfDaysPriorToDueDateSelectdropdown).selectByIndex(2);
        new Select(paymentDueSelectdropdown).selectByIndex(1);
        new Select(paymentDueContactEmailvalue).selectByValue("Add New Mobile");
        paymentDueContactEmailTextbox.sendKeys(mobileNumber);
        paymentDueSaveButton.click();
    }
    public void setPaymentOverdueAlertsUsingMobileNo(String mobileNumber){
        new Select(paymentOverdueSelectdropdown).selectByIndex(1);
        new Select(paymentOverdueContactEmailvalue).selectByValue("Add New Mobile");
        paymentOverdueContactEmailTextbox.sendKeys(mobileNumber);
        paymentOverdueSaveButton.click();
    }
    public void setPaymentPostedAlertsUsingMobileNo(String mobileNumber){
        new Select(paymentPostedSelectdropdown).selectByIndex(1);
        new Select(paymentPostedContactEmailvalue).selectByValue("Add New Mobile");
        paymentPostedContactEmailTextbox.sendKeys(mobileNumber);
        paymentPostedSaveButton.click();
    }
    public void setPaymentScheduleAlertsUsingMobileNo(String mobileNumber){
        new Select(paymentScheduleSelectdropdown).selectByIndex(1);
        new Select(paymentScheduleContactEmailvalue).selectByValue("Add New Email");
        paymentScheduleContactEmailTextbox.sendKeys(mobileNumber);
        paymentScheduleSaveButton.click();
    }


    //OMV functionality for balance alert
    public void setUpAlertForBalanceTranferSubTypesomv (String alertSubType,String Amount,String emailId)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType.equals("Available Credit"))
        {
            availbleCreditAlertSetUpLink.click();
            setUpAvailableAlertFunctionalityomv(Amount,emailId);
            logger.info("Passed:Available credit alert is selected");
        }
        else if(alertSubType.equals("Balance Exceeds"))
        {
            balanceExceedsSetupLink.click();
            setUpBalanceExceedsAlertFunctionalityomv(Amount,emailId);
            logger.info("Passed:Balance exceeds alert is selected");
        }
        else
        {
            System.out.println("alert is not selected");
        }
    }
    public void setUpAvailableAlertFunctionalityomv(String Amount,String emailId)
    {
        availbleCreditAlertAmount.sendKeys(Amount);
        if(availbleCreditAlertEmailRadiobuttonselection.isSelected())
        {
            new Select(availbleCreditAlertEmailValue).selectByValue("Add New Email");;
        }
        else
        {
            availbleCreditAlertEmailRadiobuttonselection.click();
            new Select(availbleCreditAlertEmailValue).selectByValue("Add New Email");
        }
        availbleCreditEmailTextBox.sendKeys(emailId);
        availbleCreditsaveButton.click();

    }
    public void setUpBalanceExceedsAlertFunctionalityomv(String Amount,String emailId)
    {
        balanceExceedsamount.sendKeys(Amount);
        if(balanceExceedsAlertEmailRadiobuttonselection.isSelected()) {
            new Select(selectBalanceExceedsEmailvalue).selectByValue("Add New Email");
        }else
        {
            balanceExceedsAlertEmailRadiobuttonselection.click();
            new Select(selectBalanceExceedsEmailvalue).selectByValue("Add New Email");
        }
        balanceExceedsContactEmailTextbox.sendKeys(emailId);
        balanceExceedsSaveButton.click();
    }

    //OMV functionality for Transcation Alert
    public void clickTranscationAlertsOmv()
    {
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        transcationAlerts.click();
        System.out.println("Transcation alert is selected");
    }

    public void setUpAlertForTranscationSubTypesomv(String alertSubType1,String emailid1)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType1.equals("ATM Transaction"))
        {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            atmTranscationAlertSetupLink.click();
            System.out.println("ATMTranscation alert is selected");
            setAtmTranscationAlertomv(emailid1);
        }
        else if(alertSubType1.equals("Card Not Present"))
        {
            cardNotPresentAlertSetupLink.click();
            System.out.println("cardNotPresent alert is selected");
            setCardNotPresentAlertomv(emailid1);

        }
        else if(alertSubType1.equals("Credit Posted"))
        {
            creditPostedAlertSetupLink.click();
            System.out.println("Credit Posted alert is selected");
            setCreditPostedAlertomv(emailid1);

        }
        else if(alertSubType1.equals("Debit Posted"))
        {
            deditPostedAlertSetupLink.click();
            System.out.println("Debit Posted alert is selected");
            setDebitPostedAlertomv(emailid1);
        }
        else if(alertSubType1.equals("Decline Transaction"))
        {
            declineTranscationAlertSetupLink.click();
            System.out.println("Decline Transaction alert is selected");
            setDeclineTranscationAlertomv(emailid1);
        }
        else if(alertSubType1.equals("Gas Station Purchase"))
        {
            gasStationPurchaseAlertSetupLink.click();
            System.out.println("Gas Station Purchase alert is selected");
            setGasStationPurchaseAlertomv(emailid1);
        }
        else if(alertSubType1.equals("International Transaction"))
        {
            internationaTranscationAlertSetupLink.click();
            System.out.println("International Transaction alert is selected");
            setInternationaTranscationAlertomv(emailid1);
        }
        else if(alertSubType1.equals("Transaction Notification"))
        {
            transcationNotificationAlertSetupLink.click();
            System.out.println("Transaction Notification alert is selected");
            setTranscationNotificationAlertomv(emailid1);
        }
        else
        {
            System.out.println("Alert is not selected");
        }
    }
    public void setAtmTranscationAlertomv(String emailid1)
    {
        if(atmTranscationAlertEmailRadiobuttonselection.isSelected())
        {
            new Select(selectAtmTranscationEmailvalue).selectByValue("Add New Email");
        }
        else
        {
            atmTranscationAlertEmailRadiobuttonselection.click();
            new Select(selectAtmTranscationEmailvalue).selectByValue("Add New Email");
        }
        atmTranscationContactEmailTextbox.sendKeys(emailid1);
        atmTranscationSaveButton.click();
    }
    public void setCardNotPresentAlertomv(String emailid1)
    {
        if(cardNotPresentAlertEmailRadiobuttonselection.isSelected())
        {
            new Select(selectCardNotPresentEmailvalue).selectByValue("Add New Email");
        }
        else
        {
            cardNotPresentAlertEmailRadiobuttonselection.click();
            new Select(selectCardNotPresentEmailvalue).selectByValue("Add New Email");
        }
        cardNotPresentContactEmailTextbox.sendKeys(emailid1);
        cardNotPresentSaveButton.click();
    }
    public void setCreditPostedAlertomv(String emailid1)
    {
        if(creditPostedAlertEmailRadiobuttonselection.isSelected())
        {
            new Select(selectCreditPostedEmailvalue).selectByValue("Add New Email");
        }
        else
        {
            creditPostedAlertEmailRadiobuttonselection.click();
            new Select(selectCreditPostedEmailvalue).selectByValue("Add New Email");
        }
        creditPostedContactEmailTextbox.sendKeys(emailid1);
        creditPostedSaveButton.click();
    }
    public void setDebitPostedAlertomv(String emailid1)
    {
        selectDebitPostedAmount.click();
        addDebitPostedAmount.sendKeys("1");
        if(debitPostedAlertEmailRadiobuttonselection.isSelected())
        {
            new Select(selectDebitPostedEmailvalue).selectByValue("Add New Email");
        }
        else
        {
            debitPostedAlertEmailRadiobuttonselection.click();
            new Select(selectDebitPostedEmailvalue).selectByValue("Add New Email");
        }
        debitPostedContactEmailTextbox.sendKeys(emailid1);
        debitPostedSaveButton.click();
    }

    public void setDeclineTranscationAlertomv(String emailid1)
    {

        if(declineTranscationAlertEmailRadiobuttonselection.isSelected())
        {
            new Select(selectDeclineTrascationEmailvalue).selectByValue("Add New Email");
        }
        else
        {
            declineTranscationAlertEmailRadiobuttonselection.click();
            new Select(selectDeclineTrascationEmailvalue).selectByValue("Add New Email");
        }
        declineTrascationContactEmailTextbox.sendKeys(emailid1);
        declineTrascationSaveButton.click();
    }
    public void setGasStationPurchaseAlertomv(String emailid1)
    {
        if(gasStationPurchaseAlertEmailRadiobuttonselection.isSelected())
        {
            new Select(selectGasStationPurchaseEmailvalue).selectByValue("Add New Email");
        }
        else
        {
            gasStationPurchaseAlertEmailRadiobuttonselection.click();
            new Select(selectGasStationPurchaseEmailvalue).selectByValue("Add New Email");
        }
        gasStationPurchaseContactEmailTextbox.sendKeys(emailid1);
        gasStationPurchaseSaveButton.click();
    }
    public void setInternationaTranscationAlertomv(String emailid1)
    {
        if(internationalTranscationAlertEmailRadiobuttonselection.isSelected())
        {
            new Select(selectInternationalTranscationalEmailvalue).selectByValue("Add New Email");
        }
        else
        {
            internationalTranscationAlertEmailRadiobuttonselection.click();
            new Select(selectInternationalTranscationalEmailvalue).selectByValue("Add New Email");
        }
        internationalTranscationaleContactEmailTextbox.sendKeys(emailid1);
        internationalTranscationalSaveButton.click();

    }
    public void setTranscationNotificationAlertomv(String emailid1)
    {
        notificationAmount.sendKeys("1");
        if(transcationNotificationAlertEmailRadiobuttonselection.isSelected())
        {
            new Select(selectNotificationEmailvalue).selectByValue("Add New Email");
        }
        else
        {
            transcationNotificationAlertEmailRadiobuttonselection.click();
            new Select(selectNotificationEmailvalue).selectByValue("Add New Email");
        }
        notificationContactEmailTextbox.sendKeys(emailid1);
        notificationSaveButton.click();
    }


    //Edit Balance Alert for OMV
    public void editAlertForBalanceTranferSubTypesomv (String alertSubType,String EditAmount)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType.equals("Available Credit"))
        {
            logger.info("Available credit alert is selected");
            availableCreditEditLink.click();
            editAvailableAlertFunctionalityomv(EditAmount);

        }
        else if(alertSubType.equals("Balance Exceeds"))
        {
            System.out.println("Balance exceeds alert is selected");
            balanceExceedsEditLink.click();
            editBalanceExceedsAlertFunctionalityomv(EditAmount);
        }
        else
        {
            logger.info("alert is not selected");
        }
    }
    public void editAvailableAlertFunctionalityomv(String EditAmount)
    {
        availbleCreditAlertAmount.clear();
        availbleCreditAlertAmount.sendKeys(EditAmount);
        availbleCreditsaveButton.click();

    }
    public void editBalanceExceedsAlertFunctionalityomv(String EditAmount)
    {
        balanceExceedsamount.clear();
        balanceExceedsamount.sendKeys(EditAmount);
        balanceExceedsSaveButton.click();

    }

    //Edit card status functionality for Balance ALert OMV

    public void editAlertByChangingStatusOfSubtypesForBalanceTranferSubTypesomv (String alertSubType)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType.equals("Available Credit"))
        {
            logger.info("Available credit alert is selected");
            availableCreditEditLink.click();
            System.out.println("Card status before editing:"+getStatusAvailableCreditomv.getText());
            String CardStatus=getStatusAvailableCreditomv.getText();
            if(CardStatus.equalsIgnoreCase("Active"))
            {
                getStatusAvailableCreditomvbutton.click();
            }
            else
            {
                getStatusAvailableCreditomvbutton.click();
            }
            availbleCreditsaveButton.click();
            System.out.println("Card status after editing:"+getStatusAvailableCreditomv.getText());
        }
        else if(alertSubType.equals("Balance Exceeds"))
        {
            logger.info("Balance exceeds alert is selected");
            balanceExceedsEditLink.click();
            System.out.println("Card status before editing:"+getStatusBalanceExceedsomv.getText());
            String CardStatus=getStatusBalanceExceedsomv.getText();
            if(CardStatus.equalsIgnoreCase("Active"))
            {
                getStatusBalanceExceedsomvbutton.click();
            }
            else
            {
                getStatusBalanceExceedsomvbutton.click();
            }
            balanceExceedsSaveButton.click();
            System.out.println("Card status after editing:"+getStatusBalanceExceedsomv.getText());
        }
        else
        {
            logger.info("alert is not selected");
        }
    }
    //OMV Edit functionality for Transcation Alert
    public void editAlertForTranscationSubTypesomv(String alertSubType1,String Editemailid1)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType1.equals("ATM Transaction"))
        {
            atmTranscationEditLink.click();
            logger.info("ATMTranscation alert is selected");
            editAtmTranscationAlertomv(Editemailid1);
        }
        else if(alertSubType1.equals("Card Not Present"))
        {
            cardNotPresentEditLink.click();
            logger.info("cardNotPresent alert is selected");
            editCardNotPresentAlertomv(Editemailid1);

        }
        else if(alertSubType1.equals("Credit Posted"))
        {
            creditPostedEditLink.click();
            logger.info("Credit Posted alert is selected");
            editCreditPostedAlertomv(Editemailid1);

        }
        else if(alertSubType1.equals("Debit Posted"))
        {
            deditPostedAlertSetupLink.click();
            logger.info("dedit Posted alert is selected");
            editDebitPostedAlertomv(Editemailid1);
        }
        else if(alertSubType1.equals("Decline Transaction"))
        {
            declineTrascationEditLink.click();
            logger.info("Decline Transaction alert is selected");
            editDeclineTranscationAlertomv(Editemailid1);
        }
        else if(alertSubType1.equals("Gas Station Purchase"))
        {
            gasStationPurchaseEditLink.click();
            logger.info("Gas Station Purchase alert is selected");
            editGasStationPurchaseAlertomv(Editemailid1);
        }
        else if(alertSubType1.equals("International Transaction"))
        {
            internationalTranscationalEditLink.click();
            logger.info("International Transaction alert is selected");
            editInternationaTranscationAlertomv(Editemailid1);
        }
        else if(alertSubType1.equals("Transaction Notification"))
        {
            notificationEditLink.click();
            logger.info("Transaction Notification alert is selected");
            editTranscationNotificationAlertomv(Editemailid1);
        }
        else
        {
            logger.info("Alert is not selected");
        }
    }
    public void editAtmTranscationAlertomv(String Editemailid1)
    {
        new Select(selectAtmTranscationEmailvalue).selectByValue("Add New Email");
        atmTranscationContactEmailTextbox.sendKeys(Editemailid1);
        atmTranscationSaveButton.click();
    }
    public void editCardNotPresentAlertomv(String Editemailid1)
    {
        new Select(selectCardNotPresentEmailvalue).selectByValue("Add New Email");
        cardNotPresentContactEmailTextbox.sendKeys(Editemailid1);
        cardNotPresentSaveButton.click();
    }
    public void editCreditPostedAlertomv(String Editemailid1)
    {
        new Select(selectCreditPostedEmailvalue).selectByValue("Add New Email");
        creditPostedContactEmailTextbox.sendKeys(Editemailid1);
        creditPostedSaveButton.click();
    }
    public void editDebitPostedAlertomv(String Editemailid1)
    {
        selectDebitPostedAmount.click();
        addDebitPostedAmount.sendKeys("1");
        new Select(selectDebitPostedEmailvalue).selectByValue("Add New Email");
        debitPostedContactEmailTextbox.sendKeys(Editemailid1);
        debitPostedSaveButton.click();
    }

    public void editDeclineTranscationAlertomv(String Editemailid1)
    {
        new Select(selectDeclineTrascationEmailvalue).selectByValue("Add New Email");
        declineTrascationContactEmailTextbox.sendKeys(Editemailid1);
        declineTrascationSaveButton.click();
    }
    public void editGasStationPurchaseAlertomv(String Editemailid1)
    {
        new Select(selectGasStationPurchaseEmailvalue).selectByValue("Add New Email");
        gasStationPurchaseContactEmailTextbox.sendKeys(Editemailid1);
        gasStationPurchaseSaveButton.click();
    }
    public void editInternationaTranscationAlertomv(String Editemailid1)
    {
        new Select(selectInternationalTranscationalEmailvalue).selectByValue("Add New Email");
        internationalTranscationaleContactEmailTextbox.sendKeys(Editemailid1);
        internationalTranscationalSaveButton.click();

    }
    public void editTranscationNotificationAlertomv(String Editemailid1)
    {
        notificationAmount.clear();
        notificationAmount.sendKeys("1.00");
        new Select(selectNotificationEmailvalue).selectByValue("Add New Email");
        notificationContactEmailTextbox.sendKeys(Editemailid1);
        notificationSaveButton.click();
    }


    public void editAlertByChangingStatusOfSubtypesForTranscationSubTypesomv(String alertSubType1)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType1.equals("ATM Transaction"))
        {
            atmTranscationEditLink.click();
            logger.info("ATM credit alert is selected");
            System.out.println("Card status before editing:"+getStatusAtmTranscationomvbutton.getText());
            String CardStatus=getStatusAtmTranscationomvbutton.getText();
            if(CardStatus.equalsIgnoreCase("Active"))
            {
                getStatusAtmTranscationomv.click();
            }
            else
            {
                getStatusAtmTranscationomv.click();
            }
            atmTranscationSaveButton.click();
            System.out.println("Card status after editing:"+getStatusAtmTranscationomvbutton.getText());
        }
        else if(alertSubType1.equals("Card Not Present"))
        {
            cardNotPresentEditLink.click();
            logger.info("card Not found  alert is selected");
            System.out.println("Card status before editing:"+getStatusCardNotPresentomvbutton.getText());
            String CardStatus=getStatusCardNotPresentomvbutton.getText();
            if(CardStatus.equalsIgnoreCase("Active"))
            {
                getStatusCardNotPresentomv.click();
            }
            else
            {
                getStatusCardNotPresentomv.click();
            }
            cardNotPresentSaveButton.click();
            System.out.println("Card status after editing:"+getStatusCardNotPresentomvbutton.getText());
        }
        else if(alertSubType1.equals("Credit Posted"))
        {
            creditPostedEditLink.click();
            logger.info("credit posted alert is selected");
            System.out.println("Card status before editing:"+getStatusCreditPostedomvbutton.getText());
            String CardStatus=getStatusCreditPostedomvbutton.getText();
            if(CardStatus.equalsIgnoreCase("Active"))
            {
                getStatusCreditPostedomv.click();
            }
            else
            {
                getStatusCreditPostedomv.click();
            }
            creditPostedSaveButton.click();
            System.out.println("Card status after editing:"+getStatusCreditPostedomvbutton.getText());

        }
        else if(alertSubType1.equals("Debit Posted"))
        {
            debitPostedEditLink.click();
            logger.info("debit posted alert is selected");
            System.out.println("Card status before editing:"+getStatusDebitPostedomvbutton.getText());
            String CardStatus=getStatusDebitPostedomvbutton.getText();
            if(CardStatus.equalsIgnoreCase("Active"))
            {
                getStatusDebitPostedomv.click();
            }
            else
            {
                getStatusDebitPostedomv.click();
            }
            debitPostedSaveButton.click();
            System.out.println("Card status after editing:"+getStatusDebitPostedomvbutton.getText());
        }
        else if(alertSubType1.equals("Decline Transaction"))
        {
            declineTrascationEditLink.click();
            logger.info("Decline Transcation alert is selected");
            System.out.println("Card status before editing:"+getStatusDeclineTranscationomvbutton.getText());
            String CardStatus=getStatusDeclineTranscationomvbutton.getText();
            if(CardStatus.equalsIgnoreCase("Active"))
            {
                getStatusDeclineTranscationomv.click();
            }
            else
            {
                getStatusDeclineTranscationomv.click();
            }
            declineTrascationSaveButton.click();
            System.out.println("Card status after editing:"+getStatusDeclineTranscationomvbutton.getText());
        }
        else if(alertSubType1.equals("Gas Station Purchase"))
        {
            gasStationPurchaseEditLink.click();
            logger.info("Gas Station alert is selected");
            System.out.println("Card status before editing:"+getStatusGasstationPurchaseomvbutton.getText());
            String CardStatus=getStatusGasstationPurchaseomvbutton.getText();
            if(CardStatus.equalsIgnoreCase("Active"))
            {
                getStatusGasstationPurchaseomv.click();
            }
            else
            {
                getStatusGasstationPurchaseomv.click();
            }
            gasStationPurchaseSaveButton.click();
            System.out.println("Card status after editing:"+getStatusGasstationPurchaseomvbutton.getText());
        }
        else if(alertSubType1.equals("International Transaction"))
        {
            internationalTranscationalEditLink.click();
            logger.info("International Transcation  alert is selected");
            System.out.println("Card status before editing:"+getStatusInternationalTranscationomvbutton.getText());
            String CardStatus=getStatusInternationalTranscationomvbutton.getText();
            if(CardStatus.equalsIgnoreCase("Active"))
            {
                getStatusInternationalTranscationomv.click();
            }
            else
            {
                getStatusInternationalTranscationomv.click();
            }
            internationalTranscationalSaveButton.click();
            System.out.println("Card status after editing:"+getStatusInternationalTranscationomvbutton.getText());
        }
        else if(alertSubType1.equals("Transaction Notification"))
        {
            notificationEditLink.click();
            logger.info(" Transaction Notification alert is selected");
            System.out.println("Card status before editing:"+getStatusTranscationNotificationomvbutton.getText());
            String CardStatus=getStatusTranscationNotificationomvbutton.getText();
            if(CardStatus.equalsIgnoreCase("Active"))
            {
                getStatusTranscationNotificationomv.click();
            }
            else
            {
                getStatusTranscationNotificationomv.click();
            }
            notificationSaveButton.click();
            System.out.println("Card status after editing:"+getStatusTranscationNotificationomvbutton.getText());
        }
        else
        {
            logger.info("Alert is not selected");
        }
    }

    public void deleteAlertForBalanceTranferSubTypesomv (String alertSubType)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType.equals("Available Credit"))
        {
            availableCreditEditLink.click();
            logger.info("Available credit alert is selected");
            deleteAvailableAlertFunctionalityomv();

        }
        else if(alertSubType.equals("Balance Exceeds"))
        {
            balanceExceedsEditLink.click();
            logger.info("Balance exceeds alert is selected");
            deleteBalanceExceedsAlertFunctionalityomv();
        }
        else
        {
            logger.info("alert is not selected");
        }
    }
    public void deleteAvailableAlertFunctionalityomv()
    {
        availbleCreditAlertDeleteLink.click();
        availbleCreditAlertContinueButton.click();
        logger.info("Available credit Alert is deleted successful ");
    }
    public void deleteBalanceExceedsAlertFunctionalityomv()
    {
        balanceExceedsAlertDeleteLink.click();
        balanceExceedsAlertContinueButton.click();
        logger.info("Balance Exceeds Alert is deleted successful");
    }

//Delete functionality for OMV Transcation alert
public void deleteAlertForTranscationSubTypesomv(String alertSubType1)
{
    JavascriptExecutor jse = (JavascriptExecutor) driver;
    jse.executeScript("scroll(0, 1000);");
    if(alertSubType1.equals("ATM Transaction"))
    {
        deleteAtmTranscationAlertomv();
        logger.info("ATMTranscation alert is deleted");
    }
    else if(alertSubType1.equals("Card Not Present"))
    {
        deleteCardNotPresentAlertomv();
        logger.info("cardNotPresent alert is deleted");
    }
    else if(alertSubType1.equals("Credit Posted"))
    {
        deleteCreditPostedAlertomv();
        logger.info("Credit Posted alert is deleted");

    }
    else if(alertSubType1.equals("Debit Posted"))
    {
        deleteDebitPostedAlertomv();
        logger.info("dedit Posted alert is deleted");
    }
    else if(alertSubType1.equals("Decline Transaction"))
    {
        deleteDeclineTranscationAlertomv();
        logger.info("Decline Transaction alert is deleted");
    }
    else if(alertSubType1.equals("Gas Station Purchase"))
    {
        deleteGasStationPurchaseAlertomv();
        logger.info("Gas Station Purchase alert is deleted");
    }
    else if(alertSubType1.equals("International Transaction"))
    {
        deleteInternationaTranscationAlertomv();
        logger.info("International Transaction alert is deleted");
    }
    else if(alertSubType1.equals("Transaction Notification"))
    {
        deleteTranscationNotificationAlertomv();
        logger.info("Transaction Notification alert is deleted");
    }
    else
    {
        logger.info("Alert is not selected");
    }
}
    public void deleteAtmTranscationAlertomv()
    {
        atmTranscationEditLink.click();
        atmTranscationAlertDeleteLink.click();
        atmTranscationAlertContinueButton.click();
    }
    public void deleteCardNotPresentAlertomv()
    {
        cardNotPresentEditLink.click();
        cardNotPresentAlertDeleteLink.click();
        cardNotPresentAlertContinueButton.click();
    }
    public void deleteCreditPostedAlertomv()
    {
        creditPostedEditLink.click();
        creditPostedAlertDeleteLink.click();
        creditPostedAlertContinueButton.click();
    }
    public void deleteDebitPostedAlertomv()
    {
        debitPostedEditLink.click();
        debitPostedAlertDeleteLink.click();
        debitPostedAlertContinueButton.click();
    }

    public void deleteDeclineTranscationAlertomv()
    {
        declineTrascationEditLink.click();
        declineTranscationAlertDeleteLink.click();
        declineTranscationAlertContinueButton.click();
    }
    public void deleteGasStationPurchaseAlertomv()
    {
        gasStationPurchaseEditLink.click();
        gasStationPurchaseAlertDeleteLink.click();
        gasStationPurchaseAlertContinueButton.click();
    }
    public void deleteInternationaTranscationAlertomv()
    {
        internationalTranscationalEditLink.click();
        internationalTranscationalAlertDeleteLink.click();
        internationalTranscationalAlertContinueButton.click();

    }
    public void deleteTranscationNotificationAlertomv()
    {
        notificationEditLink.click();
        notificationAlertDeleteLink.click();
        notificationAlertContinueButton.click();
    }

    //Debit posted error functionality for radio button for OMV
    public void setDebitPostedAlertRadiobuttonErroromv(String emailid1)
    {
        deditPostedAlertSetupLink.click();
        //selectDebitPostedAmount.click();
        addDebitPostedAmount.sendKeys("1");
        if(debitPostedAlertEmailRadiobuttonselection.isSelected())
        {
            new Select(selectDebitPostedEmailvalue).selectByValue("Add New Email");
        }
        else
        {
            debitPostedAlertEmailRadiobuttonselection.click();
            new Select(selectDebitPostedEmailvalue).selectByValue("Add New Email");
        }
        debitPostedContactEmailTextbox.sendKeys(emailid1);
        debitPostedSaveButton.click();
    }
    //Debit posted error functionality for Amount field for OMV
    public void DebitPostedAlertErrorForAmountFieldomv(String emailid1)
    {
        deditPostedAlertSetupLink.click();
        selectDebitPostedAmount.click();
        addDebitPostedAmount.clear();
        if(debitPostedAlertEmailRadiobuttonselection.isSelected())
        {
            new Select(selectDebitPostedEmailvalue).selectByValue("Add New Email");
        }
        else
        {
            debitPostedAlertEmailRadiobuttonselection.click();
            new Select(selectDebitPostedEmailvalue).selectByValue("Add New Email");
        }
        debitPostedContactEmailTextbox.sendKeys(emailid1);
        debitPostedSaveButton.click();
    }

    //Debit posted error functionality for IncorrectAmount field for OMV
    public void DebitPostedAlertErrorForIncorrectAmountFieldomv(String emailid1,String amount)
    {
        deditPostedAlertSetupLink.click();
        selectDebitPostedAmount.click();
        addDebitPostedAmount.sendKeys(amount);
        if(debitPostedAlertEmailRadiobuttonselection.isSelected())
        {
            new Select(selectDebitPostedEmailvalue).selectByValue("Add New Email");
        }
        else
        {
            debitPostedAlertEmailRadiobuttonselection.click();
            new Select(selectDebitPostedEmailvalue).selectByValue("Add New Email");
        }
        debitPostedContactEmailTextbox.sendKeys(emailid1);
        debitPostedSaveButton.click();
    }

    //Travel Notification Error handling
    public void getErrorTranscationForTravelNotificationAlertomv(String emailid1)
    {
        transcationNotificationAlertSetupLink.click();
        notificationAmount.clear();
        if(transcationNotificationAlertEmailRadiobuttonselection.isSelected())
        {
            new Select(selectNotificationEmailvalue).selectByValue("Add New Email");
        }
        else
        {
            transcationNotificationAlertEmailRadiobuttonselection.click();
            new Select(selectNotificationEmailvalue).selectByValue("Add New Email");
        }
        notificationContactEmailTextbox.sendKeys(emailid1);
        notificationSaveButton.click();
    }

    //Payment Due Omv Error handling
    public void getErrorPaymentDueAlertsomv(String emailId){
        setUpLinkForpaymentDue.click();
        logger.info("Payment Due alert is selected for setting up alert");
        //new Select(numberOfDaysPriorToDueDateSelectdropdown).selectByIndex(2);
        if(paymentDueEmailRadiobuttonselection.isSelected()){
            new Select(paymentDueContactEmailvalue).selectByValue("Add New Email");
        }else{
            paymentDueEmailRadiobuttonselection.click();
            new Select(paymentDueContactEmailvalue).selectByValue("Add New Email");
        }
        paymentDueContactEmailTextbox.sendKeys(emailId);
        paymentDueSaveButton.click();
    }


    public void setUpOMVAlertForStatementAndPaymentSubTypes(String alertSubType,String emailId) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if (alertSubType.equals("Online Statement Available")) {
            setUpLinkForOnlineStatmentAvailable.click();
            logger.info("Online Statement Available alert is selected for setting up alert");
            if(onlinestatementAvailableEmailRadiobuttonselection.isSelected()) {
                new Select(onlineStatmentAvailableContactEmailvalue).selectByValue("Add New Email");
            }else
            {
                onlinestatementAvailableEmailRadiobuttonselection.click();
                new Select(onlineStatmentAvailableContactEmailvalue).selectByValue("Add New Email");
            }
            onlineStatmentAvailableContactEmailTextbox.sendKeys(emailId);
            onlineStatmentAvailableSaveButton.click();
        }
        else if (alertSubType.equals("AutoPay Scheduled and AutoPay Processed")) {
            setUpLinkForAutoPayScheduledandAutoPayProcessed.click();
            logger.info("AutoPay Scheduled and AutoPay Processed alert is selected for setting up alert");
            if(AutoPayScheduledandAutoPayProcessedEmailRadiobuttonselection.isSelected()){
                new Select(AutoPayScheduledandAutoPayProcessedContactEmailvalue).selectByValue("Add New Email");
            }else
            {
                AutoPayScheduledandAutoPayProcessedEmailRadiobuttonselection.click();
                new Select(AutoPayScheduledandAutoPayProcessedContactEmailvalue).selectByValue("Add New Email");
            }
            AutoPayScheduledandAutoPayProcessedContactEmailTextbox.sendKeys(emailId);
            AutoPayScheduledandAutoPayProcessedSaveButton.click();
        }
        else if (alertSubType.equals("Payment Due")) {
            setUpLinkForpaymentDue.click();
            logger.info("Payment Due alert is selected for setting up alert");
            new Select(numberOfDaysPriorToDueDateSelectdropdown).selectByIndex(2);
            if(paymentDueEmailRadiobuttonselection.isSelected()){
                new Select(paymentDueContactEmailvalue).selectByValue("Add New Email");
            }else{
                paymentDueEmailRadiobuttonselection.click();
                new Select(paymentDueContactEmailvalue).selectByValue("Add New Email");
            }
            paymentDueContactEmailTextbox.sendKeys(emailId);
            paymentDueSaveButton.click();
        }
        else if (alertSubType.equals("Payment Overdue")) {
            setUpLinkForpaymentOverdue.click();
            logger.info("Payment Overdue alert is selected for setting up alert");
            if(paymentOverdueEmailRadiobuttonselection.isSelected()){
                new Select(paymentOverdueContactEmailvalue).selectByValue("Add New Email");
            }else{
                paymentOverdueEmailRadiobuttonselection.click();
                new Select(paymentOverdueContactEmailvalue).selectByValue("Add New Email");
            }
            paymentOverdueContactEmailTextbox.sendKeys(emailId);
            paymentOverdueSaveButton.click();
        }
        else if (alertSubType.equals("Payment Posted")) {
            setUpLinkForpaymentPosted.click();
            logger.info("Payment Posted alert is selected for setting up alert");
            if(paymentPostedEmailRadiobuttonselection.isSelected()){
                new Select(paymentPostedContactEmailvalue).selectByValue("Add New Email");
            }else{
                paymentPostedEmailRadiobuttonselection.click();
                new Select(paymentPostedContactEmailvalue).selectByValue("Add New Email");
            }
            paymentPostedContactEmailTextbox.sendKeys(emailId);
            paymentPostedSaveButton.click();
        }
        else if (alertSubType.equals("Payment Scheduled")) {

            setUpLinkForpaymentSchedule.click();
            logger.info("Payment Scheduled alert is selected for setting up alert");
            if(paymentScheduleEmailRadiobuttonselection.isSelected()){
                new Select(paymentScheduleContactEmailvalue).selectByValue("Add New Email");
            }else{
                paymentScheduleEmailRadiobuttonselection.click();
                new Select(paymentScheduleContactEmailvalue).selectByValue("Add New Email");
            }
            paymentScheduleContactEmailTextbox.sendKeys(emailId);
            paymentScheduleSaveButton.click();
        }
        else
            logger.info("Please input the valid alert subtype name for setting up alert");
    }

    @FindBy(how=How.XPATH,using = "//ons-switch[@name='Alert Status']") WebElement switchbuttonForCardStatus;
    @FindBy(how = How.XPATH,using = "//*[@class='omv-alert-status-indicator']")WebElement securityAlertStatus;

    public void editOMVAlertByChangingStatusOfSubTypeAlertsUnderStatementAndPayment(String alertSubType) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if (alertSubType.equals("Online Statement Available")) {
            editLinkForOnlineStatmentAvailable.click();
            logger.info("Card status Before editing for "+alertSubType+" subType is:"+getStatusOnlineStatementAvailable.getText());
            String CardStatus=getStatusOnlineStatementAvailable.getText();
            if(CardStatus.equalsIgnoreCase("Active")){

                switchbuttonForCardStatus.click();
            }else
            {
                switchbuttonForCardStatus.click();
            }
            onlineStatmentAvailableSaveButton.click();
            logger.info("Card status After editing for "+alertSubType+" subType is:"+getStatusOnlineStatementAvailable.getText());
        }
        else if (alertSubType.equals("AutoPay Scheduled and AutoPay Processed")) {
            editLinkForAutoPayScheduledandAutoPayProcessed.click();
            String CardStatus=getStatusAutopaySheduledAndProcessed.getText();
            logger.info("Card status before editing for "+alertSubType+" subType:"+getStatusAutopaySheduledAndProcessed.getText());
            if(CardStatus.equalsIgnoreCase("Active")){

                switchbuttonForCardStatus.click();
            }else
            {
                switchbuttonForCardStatus.click();
            }
            AutoPayScheduledandAutoPayProcessedSaveButton.click();
            logger.info("Card status After editing for "+alertSubType+" subType:"+getStatusAutopaySheduledAndProcessed.getText());
        }
        else if (alertSubType.equals("Payment Due")) {
            editLinkForpaymentDue.click();
            logger.info("Card status before editing for "+alertSubType+" subType:"+getStatusPaymentDue.getText());
            String CardStatus=getStatusPaymentDue.getText();
            if(CardStatus.equalsIgnoreCase("Active")){

                switchbuttonForCardStatus.click();
            }else
            {
                switchbuttonForCardStatus.click();
            }
            paymentDueSaveButton.click();
            logger.info("Card status After editing for "+alertSubType+" subType:"+getStatusPaymentDue.getText());
        }
        else if (alertSubType.equals("Payment Overdue")) {
            editLinkForpaymentOverdue.click();
            logger.info("Card status before editing for "+alertSubType+" subType:"+getStatusPaymentOverdue.getText());
            String CardStatus=getStatusPaymentOverdue.getText();
            if(CardStatus.equalsIgnoreCase("Active")){

                switchbuttonForCardStatus.click();
            }else
            {
                switchbuttonForCardStatus.click();
            }
            paymentOverdueSaveButton.click();
            logger.info("Card status After editing for "+alertSubType+" subType:"+getStatusPaymentOverdue.getText());
        }
        else if (alertSubType.equals("Payment Posted")) {
            editUpLinkForpaymentPosted.click();
            logger.info("Card status before editing for "+alertSubType+" subType:"+getStatusPaymentPosted.getText());
            String CardStatus=getStatusPaymentPosted.getText();
            if(CardStatus.equalsIgnoreCase("Active")){
                switchbuttonForCardStatus.click();
            }else
            {
                switchbuttonForCardStatus.click();
            }
            paymentPostedSaveButton.click();
            logger.info("Card status After editing for "+alertSubType+" subType:"+getStatusPaymentPosted.getText());
        }
        else if (alertSubType.equals("Payment Scheduled")) {
            editLinkForpaymentSchedule.click();
            logger.info("Card status before editing for "+alertSubType+" subType:"+getStatusPaymentSchedule.getText());
            String CardStatus=getStatusPaymentSchedule.getText();
            if(CardStatus.equalsIgnoreCase("Active")){
                switchbuttonForCardStatus.click();
            }else
            {
                switchbuttonForCardStatus.click();
            }
            paymentScheduleSaveButton.click();
            logger.info("Card status After editing for "+alertSubType+" subType:"+getStatusPaymentSchedule.getText());
        }
        else
            logger.info("Please verify the subtype name of alert");
    }

    public void editOMVAlertForStatementAndPaymentSubTypes(String alertSubType,String emailId)  {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if (alertSubType.equals("Online Statement Available")) {
            editLinkForOnlineStatmentAvailable.click();
            logger.info("Online Statement Available alert is selected for editing by email");
            onlinestatementAvailableEmailRadiobuttonselection.click();
            new Select(onlineStatmentAvailableContactEmailvalue).selectByValue("Add New Email");
            onlineStatmentAvailableContactEmailTextbox.sendKeys(emailId);
            onlineStatmentAvailableSaveButton.click();
        }
        else if (alertSubType.equals("AutoPay Scheduled and AutoPay Processed")) {
            editLinkForAutoPayScheduledandAutoPayProcessed.click();
            logger.info("AutoPay Scheduled and AutoPay Processed alert is selected for editing by email");
            AutoPayScheduledandAutoPayProcessedEmailRadiobuttonselection.click();
            new Select(AutoPayScheduledandAutoPayProcessedContactEmailvalue).selectByValue("Add New Email");
            AutoPayScheduledandAutoPayProcessedContactEmailTextbox.sendKeys(emailId);
            AutoPayScheduledandAutoPayProcessedSaveButton.click();
        }
        else if (alertSubType.equals("Payment Due")) {
            editLinkForpaymentDue.click();
            logger.info("Payment Due alert is selected for editing by email");
            paymentDueEmailRadiobuttonselection.click();
            new Select(paymentDueContactEmailvalue).selectByValue("Add New Email");
            paymentDueContactEmailTextbox.sendKeys(emailId);
            paymentDueSaveButton.click();
        }
        else if (alertSubType.equals("Payment Overdue")) {
            editLinkForpaymentOverdue.click();
            logger.info("Payment Overdue alert is selected for editing by email");
            paymentOverdueEmailRadiobuttonselection.click();
            new Select(paymentOverdueContactEmailvalue).selectByValue("Add New Email");
            paymentOverdueContactEmailTextbox.sendKeys(emailId);
            paymentOverdueSaveButton.click();
        }
        else if (alertSubType.equals("Payment Posted")) {
            editUpLinkForpaymentPosted.click();
            logger.info("Payment Posted alert is selected for editing by email");
            paymentPostedEmailRadiobuttonselection.click();
            new Select(paymentPostedContactEmailvalue).selectByValue("Add New Email");
            paymentPostedContactEmailTextbox.sendKeys(emailId);
            paymentPostedSaveButton.click();
        }
        else if (alertSubType.equals("Payment Scheduled")) {
            editLinkForpaymentSchedule.click();
            logger.info("Payment Scheduled alert is selected for editing by email");
            paymentScheduleEmailRadiobuttonselection.click();
            new Select(paymentScheduleContactEmailvalue).selectByValue("Add New Email");
            paymentScheduleContactEmailTextbox.sendKeys(emailId);
            paymentScheduleSaveButton.click();
        }
        else
            logger.info("Please verify the subtype name of alert");
    }
    public void setupomvSecurityAlerts(String emailId){
        logger.info("Security alert is selected for setup");
        securityEmailRadiobuttonselection.click();
        new Select(SelectSecurityContactEmailvalue).selectByValue("Add New Email");
        SecurityContactEmailTextbox.sendKeys(emailId);
        securitySaveButton.click();
    }
    public void editOMVSecurityAlerts(String emailId){
        logger.info("Security alert is selected for editing");
        securityEmailRadiobuttonselection.click();
        new Select(SelectSecurityContactEmailvalue).selectByValue("Add New Email");
        SecurityContactEmailTextbox.sendKeys(emailId);
        securitySaveButton.click();
    }
    public void editOMVSecurityAlertByChangingStatus(){
        String CardStatus=securityAlertStatus.getText();
        logger.info("Security alert card status before changing the status: "+CardStatus);
        if(CardStatus.equalsIgnoreCase("Active")){

            switchbuttonForCardStatus.click();
        }else
        {
            switchbuttonForCardStatus.click();
        }
        securitySaveButton.click();
        logger.info("Security alert card status after changing the status: "+securityAlertStatus.getText());

    }

    public void deleteSecurityAlert(){
        logger.info("Security has been selected for deleting");
        deleteSecutityAlertlink.click();
        continuebuttonForSecurity.click();
    }
    public void closeOMVSeeAgreementLinkAlertPage(){
        driver.switchTo().frame("popupIframe");
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(closeSeeAgreementpage));
        closeSeeAgreementpage.click();
    }
    public void setUpOMVAlertForBalanceTranferSubTypesWithNoEmailValue(String alertSubType,String Amount)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType.equals("Available Credit"))
        {
            logger.info(" Available credit alert is selected for setup");
            availbleCreditAlertSetUpLink.click();
            availbleCreditAlertAmount.sendKeys(Amount);
            if(availbleCreditAlertEmailRadiobuttonselection.isSelected())
            {
                new Select(availbleCreditAlertEmailValue).selectByValue("Add New Email");;
            }
            else
            {
                availbleCreditAlertEmailRadiobuttonselection.click();
                new Select(availbleCreditAlertEmailValue).selectByValue("Add New Email");
            }
            availbleCreditsaveButton.click();

        }
        else if(alertSubType.equals("Balance Exceeds"))
        {
            logger.info("Passed:Balance exceeds alert is selected for setup");
            balanceExceedsSetupLink.click();
            balanceExceedsamount.sendKeys(Amount);
            if(balanceExceedsAlertEmailRadiobuttonselection.isSelected()) {
                new Select(selectBalanceExceedsEmailvalue).selectByValue("Add New Email");
            }else
            {
                balanceExceedsAlertEmailRadiobuttonselection.click();
                new Select(selectBalanceExceedsEmailvalue).selectByValue("Add New Email");
            }
            balanceExceedsSaveButton.click();

        }
        else
        {
            System.out.println("alert is not selected");
        }
    }
    public void setUpOMVAlertForTranscationSubTypesWithNoEmailValue(String alertSubType1 )
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType1.equals("ATM Transaction"))
        {
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            atmTranscationAlertSetupLink.click();
            logger.info("ATMTranscation alert is selected for setup");
            if(atmTranscationAlertEmailRadiobuttonselection.isSelected())
            {
                new Select(selectAtmTranscationEmailvalue).selectByValue("Add New Email");
            }
            else
            {
                atmTranscationAlertEmailRadiobuttonselection.click();
                new Select(selectAtmTranscationEmailvalue).selectByValue("Add New Email");
            }
            atmTranscationSaveButton.click();
        }
        else if(alertSubType1.equals("Card Not Present"))
        {
            cardNotPresentAlertSetupLink.click();
            logger.info("cardNotPresent alert is selected for setup");
            if(cardNotPresentAlertEmailRadiobuttonselection.isSelected())
            {
                new Select(selectCardNotPresentEmailvalue).selectByValue("Add New Email");
            }
            else
            {
                cardNotPresentAlertEmailRadiobuttonselection.click();
                new Select(selectCardNotPresentEmailvalue).selectByValue("Add New Email");
            }
            cardNotPresentSaveButton.click();
        }
        else if(alertSubType1.equals("Credit Posted"))
        {
            creditPostedAlertSetupLink.click();
            logger.info("Credit Posted alert is selected for setup");
            if(creditPostedAlertEmailRadiobuttonselection.isSelected())
            {
                new Select(selectCreditPostedEmailvalue).selectByValue("Add New Email");
            }
            else
            {
                creditPostedAlertEmailRadiobuttonselection.click();
                new Select(selectCreditPostedEmailvalue).selectByValue("Add New Email");
            }
            creditPostedSaveButton.click();
        }
        else if(alertSubType1.equals("Debit Posted"))
        {
            deditPostedAlertSetupLink.click();
            logger.info("Debit Posted alert is selected for setup");
            selectDebitPostedAmount.click();
            addDebitPostedAmount.sendKeys("1");
            if(debitPostedAlertEmailRadiobuttonselection.isSelected())
            {
                new Select(selectDebitPostedEmailvalue).selectByValue("Add New Email");
            }
            else
            {
                debitPostedAlertEmailRadiobuttonselection.click();
                new Select(selectDebitPostedEmailvalue).selectByValue("Add New Email");
            }
            debitPostedSaveButton.click();
        }
        else if(alertSubType1.equals("Decline Transaction"))
        {
            declineTranscationAlertSetupLink.click();
            logger.info("Decline Transaction alert is selected for setup");
            if(declineTranscationAlertEmailRadiobuttonselection.isSelected())
            {
                new Select(selectDeclineTrascationEmailvalue).selectByValue("Add New Email");
            }
            else
            {
                declineTranscationAlertEmailRadiobuttonselection.click();
                new Select(selectDeclineTrascationEmailvalue).selectByValue("Add New Email");
            }
            declineTrascationSaveButton.click();
        }
        else if(alertSubType1.equals("Gas Station Purchase"))
        {
            gasStationPurchaseAlertSetupLink.click();
            logger.info("Gas Station Purchase alert is selected for setup");
            if(gasStationPurchaseAlertEmailRadiobuttonselection.isSelected())
            {
                new Select(selectGasStationPurchaseEmailvalue).selectByValue("Add New Email");
            }
            else
            {
                gasStationPurchaseAlertEmailRadiobuttonselection.click();
                new Select(selectGasStationPurchaseEmailvalue).selectByValue("Add New Email");
            }
            gasStationPurchaseSaveButton.click();
        }
        else if(alertSubType1.equals("International Transaction"))
        {
            internationaTranscationAlertSetupLink.click();
            logger.info("International Transaction alert is selected for setup");
            if(internationalTranscationAlertEmailRadiobuttonselection.isSelected())
            {
                new Select(selectInternationalTranscationalEmailvalue).selectByValue("Add New Email");
            }
            else
            {
                internationalTranscationAlertEmailRadiobuttonselection.click();
                new Select(selectInternationalTranscationalEmailvalue).selectByValue("Add New Email");
            }
            internationalTranscationalSaveButton.click();
        }
        else if(alertSubType1.equals("Transaction Notification"))
        {
            transcationNotificationAlertSetupLink.click();
            logger.info("Transaction Notification alert is selected for setup");
            notificationAmount.sendKeys("1");
            if(transcationNotificationAlertEmailRadiobuttonselection.isSelected())
            {
                new Select(selectNotificationEmailvalue).selectByValue("Add New Email");
            }
            else
            {
                transcationNotificationAlertEmailRadiobuttonselection.click();
                new Select(selectNotificationEmailvalue).selectByValue("Add New Email");
            }
            notificationSaveButton.click();
        }
        else
        {
            System.out.println("Alert is not selected");
        }
    }
    public void setUpOMVAlertForStatementAndPaymentSubTypesWithNoEmailValue(String alertSubType) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if (alertSubType.equals("Online Statement Available")) {
            setUpLinkForOnlineStatmentAvailable.click();
            logger.info("Online Statement Available alert is selected for setting up alert");
            if(onlinestatementAvailableEmailRadiobuttonselection.isSelected()) {
                new Select(onlineStatmentAvailableContactEmailvalue).selectByValue("Add New Email");
            }else
            {
                onlinestatementAvailableEmailRadiobuttonselection.click();
                new Select(onlineStatmentAvailableContactEmailvalue).selectByValue("Add New Email");
            }
            onlineStatmentAvailableSaveButton.click();
        }
        else if (alertSubType.equals("AutoPay Scheduled and AutoPay Processed")) {
            setUpLinkForAutoPayScheduledandAutoPayProcessed.click();
            logger.info("AutoPay Scheduled and AutoPay Processed alert is selected for setting up alert");
            if(AutoPayScheduledandAutoPayProcessedEmailRadiobuttonselection.isSelected()){
                new Select(AutoPayScheduledandAutoPayProcessedContactEmailvalue).selectByValue("Add New Email");
            }else
            {
                AutoPayScheduledandAutoPayProcessedEmailRadiobuttonselection.click();
                new Select(AutoPayScheduledandAutoPayProcessedContactEmailvalue).selectByValue("Add New Email");
            }
            AutoPayScheduledandAutoPayProcessedSaveButton.click();
        }
        else if (alertSubType.equals("Payment Due")) {
            setUpLinkForpaymentDue.click();
            logger.info("Payment Due alert is selected for setting up alert");
            new Select(numberOfDaysPriorToDueDateSelectdropdown).selectByIndex(2);
            if(paymentDueEmailRadiobuttonselection.isSelected()){
                new Select(paymentDueContactEmailvalue).selectByValue("Add New Email");
            }else{
                paymentDueEmailRadiobuttonselection.click();
                new Select(paymentDueContactEmailvalue).selectByValue("Add New Email");
            }
            paymentDueSaveButton.click();
        }
        else if (alertSubType.equals("Payment Overdue")) {
            setUpLinkForpaymentOverdue.click();
            logger.info("Payment Overdue alert is selected for setting up alert");
            if(paymentOverdueEmailRadiobuttonselection.isSelected()){
                new Select(paymentOverdueContactEmailvalue).selectByValue("Add New Email");
            }else{
                paymentOverdueEmailRadiobuttonselection.click();
                new Select(paymentOverdueContactEmailvalue).selectByValue("Add New Email");
            }
            paymentOverdueSaveButton.click();
        }
        else if (alertSubType.equals("Payment Posted")) {
            setUpLinkForpaymentPosted.click();
            logger.info("Payment Posted alert is selected for setting up alert");
            if(paymentPostedEmailRadiobuttonselection.isSelected()){
                new Select(paymentPostedContactEmailvalue).selectByValue("Add New Email");
            }else{
                paymentPostedEmailRadiobuttonselection.click();
                new Select(paymentPostedContactEmailvalue).selectByValue("Add New Email");
            }
            paymentPostedSaveButton.click();
        }
        else if (alertSubType.equals("Payment Scheduled")) {
            setUpLinkForpaymentSchedule.click();
            logger.info("Payment Scheduled alert is selected for setting up alert");
            if(paymentScheduleEmailRadiobuttonselection.isSelected()){
                new Select(paymentScheduleContactEmailvalue).selectByValue("Add New Email");
            }else{
                paymentScheduleEmailRadiobuttonselection.click();
                new Select(paymentScheduleContactEmailvalue).selectByValue("Add New Email");
            }
            paymentScheduleSaveButton.click();
        }
        else
            logger.info("Please input the valid alert subtype name for setting up alert");
    }
    public void setOMVSecurityAlertswithNoEmailValue(){
        securityEmailRadiobuttonselection.click();
        new Select(SelectSecurityContactEmailvalue).selectByValue("Add New Email");
        securitySaveButton.click();
    }

    public void WithoutEditingExistingValuesForOMVStatementAndPaymentSubTypes(String alertSubType)  {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if (alertSubType.equals("Online Statement Available")) {
            editLinkForOnlineStatmentAvailable.click();
            logger.info("Online Statement Available alert is selected for editing by email");
            onlinestatementAvailableEmailRadiobuttonselection.click();
            onlineStatmentAvailableSaveButton.click();
        }
        else if (alertSubType.equals("AutoPay Scheduled and AutoPay Processed")) {
            editLinkForAutoPayScheduledandAutoPayProcessed.click();
            logger.info("AutoPay Scheduled and AutoPay Processed alert is selected for editing by email");
            AutoPayScheduledandAutoPayProcessedEmailRadiobuttonselection.click();
            AutoPayScheduledandAutoPayProcessedSaveButton.click();
        }
        else if (alertSubType.equals("Payment Due")) {
            editLinkForpaymentDue.click();
            logger.info("Payment Due alert is selected for editing by email");
            paymentDueEmailRadiobuttonselection.click();
            paymentDueSaveButton.click();
        }
        else if (alertSubType.equals("Payment Overdue")) {
            editLinkForpaymentOverdue.click();
            logger.info("Payment Overdue alert is selected for editing by email");
            paymentOverdueEmailRadiobuttonselection.click();
            paymentOverdueSaveButton.click();
        }
        else if (alertSubType.equals("Payment Posted")) {
            editUpLinkForpaymentPosted.click();
            logger.info("Payment Posted alert is selected for editing by email");
            paymentPostedEmailRadiobuttonselection.click();
            paymentPostedSaveButton.click();
        }
        else if (alertSubType.equals("Payment Scheduled")) {
            editLinkForpaymentSchedule.click();
            logger.info("Payment Scheduled alert is selected for editing by email");
            paymentScheduleEmailRadiobuttonselection.click();
            paymentScheduleSaveButton.click();
        }
        else
            logger.info("Please verify the subtype name of alert");
    }
    public void WithoutEditingExistingValuesForOMVSecurityAlerts( ){
        logger.info("Security alert is selected for editing");
        securityEmailRadiobuttonselection.click();
        securitySaveButton.click();
    }


    public void editAlertForBalanceTranferSubTypesomv (String alertSubType )
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if(alertSubType.equals("Available Credit"))
        {
            logger.info("Available credit alert is selected");
            availableCreditEditLink.click();
            availbleCreditsaveButton.click();
        }
        else if(alertSubType.equals("Balance Exceeds"))
        {
            System.out.println("Balance exceeds alert is selected");
            balanceExceedsEditLink.click();
            balanceExceedsSaveButton.click();
        }
        else
        {
            logger.info("alert is not selected");
        }
    }
    public void WithoutEditingExistingValuesForOMVSForTranscationSubTypes(String alertSubType1) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        if (alertSubType1.equals("ATM Transaction")) {
            atmTranscationEditLink.click();
            logger.info("ATMTranscation alert is selected  for editing by email");

            atmTranscationSaveButton.click();
        } else if (alertSubType1.equals("Card Not Present")) {
            cardNotPresentEditLink.click();
            logger.info("cardNotPresent alert is selected  for editing by email for editing by email");

            cardNotPresentSaveButton.click();
        } else if (alertSubType1.equals("Credit Posted")) {
            creditPostedEditLink.click();
            logger.info("Credit Posted alert is selected for editing by email ");
            creditPostedSaveButton.click();
        } else if (alertSubType1.equals("Debit Posted")) {
            debitPostedEditLink.click();
            logger.info("dedit Posted alert is selected for editing by email");
            debitPostedSaveButton.click();
        } else if (alertSubType1.equals("Decline Transaction")) {
            declineTrascationEditLink.click();
            logger.info("Decline Transaction alert is selected for editing by email");
            declineTrascationSaveButton.click();
        } else if (alertSubType1.equals("Gas Station Purchase")) {
            gasStationPurchaseEditLink.click();
            logger.info("Gas Station Purchase alert is selected for editing by email");
            gasStationPurchaseSaveButton.click();
        } else if (alertSubType1.equals("International Transaction")) {
            internationalTranscationalEditLink.click();
            logger.info("International Transaction alert is selected for editing by email");
            internationalTranscationalSaveButton.click();
        } else if (alertSubType1.equals("Transaction Notification")) {
            notificationEditLink.click();
            logger.info("Transaction Notification alert is selected for editing by email");
            notificationSaveButton.click();
        } else {
            logger.info("Alert is not selected");
        }
    }
}


