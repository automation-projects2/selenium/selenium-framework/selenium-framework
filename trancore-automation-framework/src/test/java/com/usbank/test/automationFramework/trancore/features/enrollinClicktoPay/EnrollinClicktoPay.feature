@DR @EnrollInClickToPayWithCard @CardmemberServicesSuite @WIP @Regression

Feature:Verify the DR feature flow functionality
  Test cases for ensuring that user is able to see Enroll in Click to pay link available once customer
  is Fully booked verified.

  Scenario Outline:Verify Enroll In Click To Pay With Card functionality is working
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When I click Services Tab
    And I click Link - Enroll In Click To Pay With Card
    And the user has clicked on the Enroll button
    Then the user has navigated to Leaving site page and has clicked on the Next button

    Examples:
      | partner | testData |
