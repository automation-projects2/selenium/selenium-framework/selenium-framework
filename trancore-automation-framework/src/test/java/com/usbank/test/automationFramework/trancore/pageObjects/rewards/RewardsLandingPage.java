package com.usbank.test.automationFramework.trancore.pageObjects.rewards;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RewardsLandingPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(RewardsLandingPage.class);

    private T driver;

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Rewards')]")
    private WebElement linkToRewards;

    @FindBy(how = How.ID, using = "userFirstName")
    private WebElement welcomeUserInRewardsCenter;

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Back to account")
    private WebElement backToAccountLink;

    @FindBy(how = How.XPATH, using = "//div[@class='vcp_welcomeUser_message']")
    private WebElement welcomeUserInTrancore;


    public RewardsLandingPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public void waitForElement(WebElement element, int waitTime) {

        WebDriverWait wait = new WebDriverWait(driver, waitTime);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void clickLinkToRewardsLink() {

        waitForElement(linkToRewards, 20);
        linkToRewards.click();
    }

    public void rewardsCenterPageFirstNameDisplayed() {

        waitForElement(welcomeUserInRewardsCenter, 20);
        System.out.println(welcomeUserInRewardsCenter.getText());
        Assert.assertTrue(welcomeUserInRewardsCenter.isDisplayed());

    }

    public void clickBackToAccount() {
        backToAccountLink.click();
    }

    public void myAccountPageDisplayed() {

        waitForElement(welcomeUserInTrancore, 20);
        System.out.println(welcomeUserInTrancore.getText());
        Assert.assertTrue(welcomeUserInTrancore.isDisplayed());
    }

    public void updateTheURL() {
        String url = driver.getCurrentUrl();
        String finalURL = url.substring(0, 11) + "3" + url.substring(11);
        driver.navigate().to(finalURL);
    }

}
