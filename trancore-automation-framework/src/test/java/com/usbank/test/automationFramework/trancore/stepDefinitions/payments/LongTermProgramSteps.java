package com.usbank.test.automationFramework.trancore.stepDefinitions.payments;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

public class LongTermProgramSteps {
    @Autowired
    StepDefinitionManager stepDefinitionManager;
    WebDriver driver;

    @Then("verify that the user must be directed to Long term enrollment page and the title of the page should be Payment help – Long-term program")
    public void verifyThatTheUserMustBeDirectedToLongTermEnrollmentPageAndTheTitleOfThePageShouldBePaymentHelpLongTermProgram() {
        if(!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyLongTermEnrollmentPageTitle())
            Assert.fail("Long Term Enrollment page title is not as expected");
    }

    @Then("verify that the header of the page should be displayed as Long-term payment program")
    public void verifyThatTheHeaderOfThePageShouldBeDisplayedAsLongTermPaymentProgram() {
        if(!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyLongTermEnrollmentPageHeader())
            Assert.fail("Long Term Enrollment page header is not displayed");
//        stepDefinitionManager.captureFullPageScreenshotsWithScrolling();
    }

    @Then("verify that user must be displayed with the paragraph which starts with- We’re glad this long-term payment program")
    public void verifyThatUserMustBeDisplayedWithTheParagraphWhichStartsWithWeReGladThisLongTermPaymentProgram() {
        if(!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyWeAreGladThisLongTermParagraph())
            Assert.fail("We're glad this long-term paragraph is not displayed");
    }

    @Then("verify that user must be displayed with the long term program offer details - total balance, monthly payment and APR")
    public void verifyThatUserMustBeDisplayedWithTheLongTermProgramOfferDetailsTotalBalanceMonthlyPaymentAndAPR() {
        if (stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyTotalBalanceInLongTermProgram()) {
            if (stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyMonthlyPaymentInLongTermProgram()) {
                if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyAPRValueInLongTermProgram())
                    Assert.fail("APR In LongTerm Program is NOT displayed");
            } else
                Assert.fail("Monthly Payment In LongTerm Program is NOT displayed");
        } else
            Assert.fail("Total Balance In LongTerm Program is NOT displayed");
    }

    @Then("verify that user must be displayed with For 60 billing cycles subheader and three bullet points")
    public void verifyThatUserMustBeDisplayedWithFor60BillingCyclesSubheaderAndThreeBulletPoints() {
        if (stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyFor60BillingCyclesSubHeader()) {
            if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyFor60BillingCyclesBulletPoint1())
                Assert.fail("For 60 Billing Cycles Bullet Point1 is NOT displayed");
            else if(!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyFor60BillingCyclesBulletPoint2())
                Assert.fail("For 60 Billing Cycles Bullet Point2 is NOT displayed");
            else if(!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyFor60BillingCyclesBulletPoint3())
                Assert.fail("For 60 Billing Cycles Bullet Point3 is NOT displayed");
        } else
            Assert.fail("For 60 Billing Cycles SubHeader is NOT displayed");
    }

    @Then("verify that user must be displayed with credit reporting subheader and the paragraph starts with - If you’re enrolled in a long-term")
    public void verifyThatUserMustBeDisplayedWithCreditReportingSubheaderAndTheParagraphStartsWithIfYouReEnrolledInALongTerm() {
        if (stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyCreditReportingSubHeader()) {
            if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyCreditReportingParagraph())
                Assert.fail("Credit Reporting Paragraph is NOT displayed");
        } else
            Assert.fail("Credit Reporting SubHeader is NOT displayed");
    }

    @Then("verify that user must be displayed with Terms of the long term program subheader and six bullet points")
    public void verifyThatUserMustBeDisplayedWithTermsOfTheLongTermProgramSubheaderAndSixBulletPoints() {
        if (stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyTermsOfLongTermProgramSubHeader()) {
            if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyTermsOfLongTermProgramBulletPoint1())
                Assert.fail("Terms Of Long Term Program Bullet Point1 is NOT displayed");
            else if(!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyTermsOfLongTermProgramBulletPoint2())
                Assert.fail("Terms Of Long Term Program Bullet Point2 is NOT displayed");
            else if(!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyTermsOfLongTermProgramBulletPoint3())
                Assert.fail("Terms Of Long Term Program Bullet Point3 is NOT displayed");
            else if(!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyTermsOfLongTermProgramBulletPoint4())
                Assert.fail("Terms Of Long Term Program Bullet Point4 is NOT displayed");
            else if(!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyTermsOfLongTermProgramBulletPoint5())
                Assert.fail("Terms Of Long Term Program Bullet Point5 is NOT displayed");
//            else if(!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyTermsOfLongTermProgramBulletPoint6())
//                Assert.fail("Terms Of Long Term Program Bullet Point6 is NOT displayed");
        } else
            Assert.fail("Terms Of Long Term Program SubHeader is NOT displayed");

    }

    @Then("verify that user must be displayed with Important info about charge-offs subheader and the paragraph starts with - Charge-off is an accounting")
    public void verifyThatUserMustBeDisplayedWithImportantInfoAboutChargeOffsSubheaderAndTheParagraphStartsWithChargeOffIsAnAccounting() {
        if (stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyImportantInfoAboutChargeOffsSubHeader()) {
            if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyImportantInfoAboutChargeOffsParagraph())
                Assert.fail("Important Info About ChargeOffs Paragraph is NOT displayed");
        } else
            Assert.fail("Important Info About ChargeOffs SubHeader is NOT displayed");
    }

    @Then("verify that user must be displayed with paragraph starts with - If you have questions")
    public void verifyThatUserMustBeDisplayedWithParagraphStartsWithIfYouHaveQuestions() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyIfYouHaveQuestionsParagraph())
            Assert.fail("If You Have Questions Paragraph is NOT displayed");
    }

    @Then("verify that user must be displayed with paragraph starts with - If this Program sounds helpful")
    public void verifyThatUserMustBeDisplayedWithParagraphStartsWithIfThisProgramSoundsHelpful() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyIfThisProgramSoundsParagraph())
            Assert.fail("If This Program Sounds Paragraph is NOT displayed");
    }

    @Then("verify that user must be displayed with Agree with Terms & continue button")
    public void verifyThatUserMustBeDisplayedWithAgreeWithTermsContinueButton() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyAgreeWithTermsButtonInLongTermIsDisplayed())
            Assert.fail("Agree With Terms Button In Long Term Program is NOT displayed");
    }

    @Then("verify that user must be displayed with Cancel link in Long term Enrollment Screen")
    public void verifyThatUserMustBeDisplayedWithCancelLinkInLongTermEnrollmentScreen() {

    }

    @Then("verify that user clicks on the Agree with Terms & continue button in Long Term Enrollment Screen")
    public void verifyThatUserClicksOnTheAgreeWithTermsContinueButtonInLongTermEnrollmentScreen() {
        stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().clickAgreeWithTermsButtonInLongTerm().click();
//        stepDefinitionManager.captureScreenshot();
    }

    @Then("verify that the user must be directed to Long term Funding Account page and the title of the page should be Payment help – Long-term funding account")
    public void verifyThatTheUserMustBeDirectedToLongTermFundingAccountPageAndTheTitleOfThePageShouldBePaymentHelpLongTermFundingAccount() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyLongTermFundingAccountPageTitle())
            Assert.fail("Long Term Funding Account page title is not as expected");
    }

    @Then("verify that user should NOT be displayed with paragraph starts with - If you have questions")
    public void verifyThatUserShouldNOTBeDisplayedWithParagraphStartsWithIfYouHaveQuestions() {

    }

    @Then("verify that user should NOT be displayed with paragraph starts with - If this Program sounds helpful")
    public void verifyThatUserShouldNOTBeDisplayedWithParagraphStartsWithIfThisProgramSoundsHelpful() {
    }

    @Then("verify that user should NOT be displayed with Agree with Terms & continue button")
    public void verifyThatUserShouldNOTBeDisplayedWithAgreeWithTermsContinueButton() {
    }

    @Then("verify that user should NOT be displayed with Cancel link in Long term Enrollment Screen")
    public void verifyThatUserShouldNOTBeDisplayedWithCancelLinkInLongTermEnrollmentScreen() {
    }

    @Then("verify that user must be displayed with phone enrollment message in place of Agree terms button in Long term enrollment screen")
    public void verifyThatUserMustBeDisplayedWithPhoneEnrollmentMessageInPlaceOfAgreeTermsButtonInLongTermEnrollmentScreen() {
//        stepDefinitionManager.captureFullPageScreenshotsWithScrolling();

        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyPhoneEnrollmentMessage())
            Assert.fail("Phone enrollment message in Long Term enrollment screen is NOT displayed correctly");
        else if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyPhoneEnrollmentMessageTimings())
            Assert.fail("Phone enrollment timings in Long Term enrollment screen is NOT displayed correctly");
        else if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyPhoneEnrollmentMessageReturnToOriginationLinkIsDisplayed())
            Assert.fail("Phone enrollment message return to origination link is NOT displayed");
    }

    @Then("verify that the header of the LTH Funding Account page is displayed")
    public void verifyThatTheHeaderOfTheLTHFundingAccountPageIsDisplayed() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyLongTermFundingAccountPageHeader())
            Assert.fail("Long Term Funding Account page header is NOT displayed");
//        stepDefinitionManager.captureFullPageScreenshotsWithScrolling();

    }

    @Then("verify that user must be displayed with the paragraph which starts with- We will withdraw")
    public void verifyThatUserMustBeDisplayedWithTheParagraphWhichStartsWithWeWillWithdraw() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyWeWillWithdrawParagraphInFundingAccountPageIsDisplayed())
            Assert.fail("We will withdraw paragraph in LTH funding account screen is NOT displayed");
    }

    @Then("verify that user must be displayed with the paragraph which starts with- Your First Program Payment")
    public void verifyThatUserMustBeDisplayedWithTheParagraphWhichStartsWithYourFirstProgramPayment() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyYourFirstProgramPaymentParagraphInFundingAccountPageIsDisplayed())
            Assert.fail("Your First Program Payment Paragraph In LTH Funding Account Page is NOT displayed");
    }

    @Then("verify that user must be displayed with Select Or Add A Funding Account SubHeader")
    public void verifyThatUserMustBeDisplayedWithSelectOrAddAFundingAccountSubHeader() {
        if (stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifySelectOrAddAFundingAccountSubHeaderInLTHFundingAccountScreen())
            Assert.fail("Select Or Add A Funding Account SubHeader In LTH Funding Account Page is NOT displayed");
    }

    @Then("verify that user must be displayed with Add funding account radio button")
    public void verifyThatUserMustBeDisplayedWithAddFundingAccountRadioButton() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyAddFundingAccountRadioOptionInLTHFundingAccountScreenIsDisplayed())
            Assert.fail("Add Funding Account Radio Option In LTH Funding Account Page is NOT displayed");
    }

    @Then("verify that user must be displayed with Label - Choose Account Type")
    public void verifyThatUserMustBeDisplayedWithLabelChooseAccountType() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyAccountTypeSubHeaderInLTHFundingAccountScreenIsDisplayed())
            Assert.fail("Account Type Sub Header In LTH Funding Account Page is NOT displayed");
    }

    @Then("verify that user must be displayed with Checking radio button")
    public void verifyThatUserMustBeDisplayedWithCheckingRadioButton() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyCheckingRadioOptionInLTHFundingAccountScreenIsDisplayed())
            Assert.fail("Checking Radio Option In LTH Funding Account Page is NOT displayed");
    }

    @Then("verify that user must be displayed with Savings radio button")
    public void verifyThatUserMustBeDisplayedWithSavingsRadioButton() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifySavingsRadioOptionInLTHFundingAccountScreenIsDisplayed())
            Assert.fail("Savings Radio Option in LTH Funding Account Page is NOT displayed");
    }

    @Then("verify that user must be displayed with Label - Routing number")
    public void verifyThatUserMustBeDisplayedWithLabelRoutingNumber() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyRoutingNumberLabelInLTHFundingAccountScreen())
            Assert.fail("Routing Number Label in LTH Funding Account Page is NOT displayed");
    }

    @Then("verify that user must be displayed with Routing number Input Box")
    public void verifyThatUserMustBeDisplayedWithRoutingNumberInputBox() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyRoutingNumberInputBoxInLTHFundingAccountScreen())
            Assert.fail("Routing Number Input Box in LTH Funding Account Page is NOT displayed");
    }

    @Then("verify that user must be displayed with Routing number helping text")
    public void verifyThatUserMustBeDisplayedWithRoutingNumberHelpingText() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyRoutingNumberHelpingTextInLTHFundingAccountScreenIsDisplayed())
            Assert.fail("Routing Number Helping Text in LTH Funding Account Page is NOT displayed");
    }

    @Then("verify that user must be displayed with Label - Account number")
    public void verifyThatUserMustBeDisplayedWithLabelAccountNumber() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyAccountNumberLabelInLTHFundingAccountScreen())
            Assert.fail("Account Number Label in LTH Funding Account Page is NOT displayed");
    }

    @Then("verify that user must be displayed with Account number Input Box")
    public void verifyThatUserMustBeDisplayedWithAccountNumberInputBox() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyAccountNumberInputBoxInLTHFundingAccountScreen())
            Assert.fail("Account Number Input Box in LTH Funding Account Page is NOT displayed");
    }

    @Then("verify that user must be displayed with Show Hide button in the Account number Input Box")
    public void verifyThatUserMustBeDisplayedWithShowHideButtonInTheAccountNumberInputBox() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyAccountNumberShow_HideButtonInLTHFundingAccountScreen())
            Assert.fail("Show_Hide Button in LTH Funding Account Page is NOT displayed");
    }

    @Then("verify that user must be displayed with Account number helping text")
    public void verifyThatUserMustBeDisplayedWithAccountNumberHelpingText() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyAccountNumberHelpingTextInLTHFundingAccountScreenIsDisplayed())
            Assert.fail("Account Number Helping Text in LTH Funding Account Page is NOT displayed");
    }

    @Then("verify that user must be displayed with the paragraph which starts with- This account will be available")
    public void verifyThatUserMustBeDisplayedWithTheParagraphWhichStartsWithThisAccountWillBeAvailable() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyThisAccountWillBeAvailableParagraphInLTHFundingAccountScreenIsDisplayed())
            Assert.fail("This Account Will Be Available Paragraph in LTH Funding Account Page is NOT displayed");
    }

    @Then("verify that user must be displayed with the paragraph which starts with- If you need to make other payment")
    public void verifyThatUserMustBeDisplayedWithTheParagraphWhichStartsWithIfYouNeedToMakeOtherPayment() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyIfYouNeedToMakeOtherPaymentParagraphInLTHFundingAccountScreenIsDisplayed())
            Assert.fail("If You Need To Make Other Payment Paragraph in LTH Funding Account Page is NOT displayed");
    }

    @Then("verify that user must be displayed with the paragraph which starts with- By submitting Program enrollment")
    public void verifyThatUserMustBeDisplayedWithTheParagraphWhichStartsWithBySubmittingProgramEnrollment() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyBySubmittingProgramEnrollmentParagraphInLTHFundingAccountScreenIsDisplayed())
            Assert.fail("By submitting Program enrollment Paragraph in LTH Funding Account Page is NOT displayed");
    }

    @Then("verify that user must be displayed with the Submit Plan enrollment button")
    public void verifyThatUserMustBeDisplayedWithTheSubmitPlanEnrollmentButton() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifySubmitPlanEnrollmentButtonInLTHFundingAccountScreenIsDisplayed())
            Assert.fail("Submit Plan Enrollment Button in LTH Funding Account Page is NOT displayed");
    }

    @Then("verify that user must be displayed with the Cancel link in LTH Funding Account page")
    public void verifyThatUserMustBeDisplayedWithTheCancelLinkInLTHFundingAccountPage() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyCancelLinkInLTHFundingAccountScreenIsDisplayed())
            Assert.fail("Cancel link in LTH Funding Account Page is NOT displayed");
    }

    @When("the user selects the Add funding account radio button")
    public void theUserSelectsTheAddFundingAccountRadioButton() {
        stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().chooseAddFundingAccountRadioOptionInLTHFundingAccountScreen().click();
//        stepDefinitionManager.captureFullPageScreenshotsWithScrolling();
    }

    @When("the user clicks the Submit Plan enrollment button in LTH Funding Account page")
    public void theUserClicksTheSubmitPlanEnrollmentButtonInLTHFundingAccountPage() {
        stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().clickSubmitPlanEnrollmentButtonInLTHFundingAccountScreen();

    }

    @Then("verify that the page level error header displayed as 3 items require action in the LTH Funding Account screen")
    public void verifyThatThePageLevelErrorHeaderDisplayedAs3ItemsRequireActionInTheLTHFundingAccountScreen() {
    }

    @Then("verify that the page level error header displayed as 2 items require action in the LTH Funding Account screen")
    public void verifyThatThePageLevelErrorHeaderDisplayedAs2ItemsRequireActionInTheLTHFundingAccountScreen() {

    }

    @Then("verify that the page level error message for Account Type displayed as There’s an issue with your account type.")
    public void verifyThatThePageLevelErrorMessageForAccountTypeDisplayedAsThereSAnIssueWithYourAccountType() {
        stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().clickAccountNumberShow_HideButtonInLTHFundingAccountScreen().click();
//        stepDefinitionManager.captureFullPageScreenshotsWithScrolling();
    }

    @And("verify that the field level error message for Account Type displayed as Please make a selection.")
    public void verifyThatTheFieldLevelErrorMessageForAccountTypeDisplayedAsPleaseMakeASelection() {
    }

    @Then("verify that the page level error message for Routing number displayed as There’s an issue with your Routing number.")
    public void verifyThatThePageLevelErrorMessageForRoutingNumberDisplayedAsThereSAnIssueWithYourRoutingNumber() {
    }

    @And("verify that the field level error message for Routing number displayed as Please enter the routing number for your funding account.")
    public void verifyThatTheFieldLevelErrorMessageForRoutingNumberDisplayedAsPleaseEnterTheRoutingNumberForYourFundingAccount() {
    }

    @Then("verify that the page level error message for Account number displayed as There’s an issue with your Account number.")
    public void verifyThatThePageLevelErrorMessageForAccountNumberDisplayedAsThereSAnIssueWithYourAccountNumber() {
    }

    @And("verify that the field level error message for Account number displayed as Please enter the account number for your funding account.")
    public void verifyThatTheFieldLevelErrorMessageForAccountNumberDisplayedAsPleaseEnterTheAccountNumberForYourFundingAccount() {
    }

    @When("the user inputs invalid characters - alphabets for Routing number and Account number")
    public void theUserInputsInvalidCharactersAlphabetsForRoutingNumberAndAccountNumber() {
        stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().inputRoutingNumberInLTHFundingAccountScreen().sendKeys("AbcdEf");
        stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().inputAccountNumberInLTHFundingAccountScreen().sendKeys("AbcdEf");
//        stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().clickAccountNumberShow_HideButtonInLTHFundingAccountScreen().click();
    }

    @Then("verify that the field level error message for Routing number displayed as That doesn’t look right. You can enter only 9 numbers and no other characters. Please try again.")
    public void verifyThatTheFieldLevelErrorMessageForRoutingNumberDisplayedAsThatDoesnTLookRightYouCanEnterOnly9NumbersAndNoOtherCharactersPleaseTryAgain() {
        stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().clickAccountNumberShow_HideButtonInLTHFundingAccountScreen().click();
//        stepDefinitionManager.captureFullPageScreenshotsWithScrolling();

    }

    @Then("verify that the field level error message for Account number displayed as That doesn’t look right. You can enter only numbers, up to 17 digits. Please try again.")
    public void verifyThatTheFieldLevelErrorMessageForAccountNumberDisplayedAsThatDoesnTLookRightYouCanEnterOnlyNumbersUpTo17DigitsPleaseTryAgain() {
//        stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().clickAccountNumberShow_HideButtonInLTHFundingAccountScreen().click();
//        stepDefinitionManager.captureScreenshot();

    }

    @When("the user inputs invalid characters - special characters for Routing number and Account number")
    public void theUserInputsInvalidCharactersSpecialCharactersForRoutingNumberAndAccountNumber() {
        stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().inputRoutingNumberInLTHFundingAccountScreen().sendKeys(",.#$%&^");
        stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().inputAccountNumberInLTHFundingAccountScreen().sendKeys("@");
//        stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().clickAccountNumberShow_HideButtonInLTHFundingAccountScreen().click();

    }

    @When("the user inputs Routing number with a value of less than Nine digits")
    public void theUserInputsRoutingNumberWithAValueOfLessThanNineDigits() {
        stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().inputRoutingNumberInLTHFundingAccountScreen().sendKeys("12320");
        stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().clickAccountNumberShow_HideButtonInLTHFundingAccountScreen().click();
    }

    @When("the user inputs Routing number with a value doesnt start with zero, one, two and three")
    public void theUserInputsRoutingNumberWithAValueDoesntStartWithZeroOneTwoAndThree() {
        stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().inputRoutingNumberInLTHFundingAccountScreen().sendKeys("400000000");
        stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().clickAccountNumberShow_HideButtonInLTHFundingAccountScreen().click();
    }

    @Then("verify that the field level error message for Routing number displayed as That number doesn’t look right. Please try again.")
    public void verifyThatTheFieldLevelErrorMessageForRoutingNumberDisplayedAsThatNumberDoesntLookRightPleaseTryAgain() {
        stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().clickAccountNumberShow_HideButtonInLTHFundingAccountScreen().click();
//        stepDefinitionManager.captureScreenshot();
    }

    @When("the user inputs Routing Number which is not existing in ICS DB")
    public void theUserInputsRoutingNumberWhichIsNotExistingInICSDB() {
        stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().inputRoutingNumberInLTHFundingAccountScreen().sendKeys("123205099");
    }

    @When("the user inputs Account Number in LTH Funding account screen")
    public void theUserInputsAccountNumberInLTHFundingAccountScreen() {
        stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().inputAccountNumberInLTHFundingAccountScreen().sendKeys("123456987");
//        stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().clickAccountNumberShow_HideButtonInLTHFundingAccountScreen().click();
    }

    @When("the user inputs valid Routing Number in LTH Funding account screen")
    public void theUserInputsValidRoutingNumberInLTHFundingAccountScreen() {
        stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().inputRoutingNumberInLTHFundingAccountScreen().sendKeys("123205054");
    }

    @When("the user selects Account Type as Checking in LTH Funding account screen")
    public void theUserSelectsAccountTypeAsCheckingInLTHFundingAccountScreen() {
        stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().chooseCheckingRadioOptionInLTHFundingAccountScreen().click();
    }

    @When("the user selects Account Type as Savings in LTH Funding account screen")
    public void theUserSelectsAccountTypeAsSavingsInLTHFundingAccountScreen() {
        stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().chooseSavingsRadioOptionInLTHFundingAccountScreen().click();

    }

    @When("the user selects the existing funding account radio button in LTH Funding account screen")
    public void theUserSelectsTheExistingFundingAccountRadioButtonInLTHFundingAccountScreen() {
        stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().selectAnExistingFundingAccountInLTHFundingAccountPage().click();
    }

    @Then("verify that user must NOT be displayed with Add funding account radio button")
    public void verifyThatUserMustNOTBeDisplayedWithAddFundingAccountRadioButton() {
    }


    @Then("verify that the page level error message for Funding account field displayed as You need to select or add a funding account..")
    public void verifyThatThePageLevelErrorMessageForFundingAccountFieldDisplayedAsYouNeedToSelectOrAddAFundingAccount() {
//        stepDefinitionManager.captureFullPageScreenshotsWithScrolling();

    }

    @And("verify that the field level error message for Funding account field displayed as Please make a selection.")
    public void verifyThatTheFieldLevelErrorMessageForFundingAccountFieldDisplayedAsPleaseMakeASelection() {
    }

    @Then("verify that the page level error header displayed as {int} items require action")
    public void verifyThatThePageLevelErrorHeaderDisplayedAsItemsRequireAction(int arg0) {
    }

    @Then("verify that user must be displayed with Select a Funding Account SubHeader")
    public void verifyThatUserMustBeDisplayedWithSelectAFundingAccountSubHeader() {
        if (stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifySelectOrAddAFundingAccountSubHeaderInLTHFundingAccountScreen())
            Assert.fail("Select A Funding Account SubHeader In LTH Funding Account Page is NOT displayed");
    }

    @Then("verify that the user must be directed to Long term Funding Account page and the title of the page should be Payment help – add account")
    public void verifyThatTheUserMustBeDirectedToLongTermFundingAccountPageAndTheTitleOfThePageShouldBePaymentHelpAddAccount() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyLongTermFundingAccountPageTitle_AddFundingAccount())
            Assert.fail("LTH Funding Account Page title is NOT as expected");
    }

    @Then("verify that the header of the LTH Funding Account page is displayed should be Add a funding account.")
    public void verifyThatTheHeaderOfTheLTHFundingAccountPageIsDisplayedShouldBeAddAFundingAccount() {
        if (!stepDefinitionManager.getPageObjectManager().getLongTermPlanDetailsPage().verifyLongTermFundingAccountPageHeader_AddFundingAccount())
            Assert.fail("LTH Funding Account Page header is NOT displayed");
    }
}
