package com.usbank.test.automationFramework.trancore.pageObjects.googlePay;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class GooglePayPushProvision<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(GooglePayPushProvision.class);

    private T driver;
    public static int EXPLICIT_WAIT_TIME_SECONDS = 30;

    @FindBy(how = How.ID, using = "addToGooglePay")
    WebElement lnkGooglePay;

    public By lnkGPay = By.id("addToGooglePay");

    @FindBy(how = How.XPATH, using = "//img[@data-testid='GooglePayLogo']")
    WebElement imgGooglePayLink;

    @FindBy(how = How.XPATH, using = "//img[@title='Help']")
    private WebElement btnImage;

    @FindBy(how = How.XPATH, using = "//td[@class='vcp_accountSummary_col']/a[startswith(text(),)]")
    private WebElement lnkAccount;

    @FindBy(id = "tc_PostedTab")
    private WebElement tabPosted;

    @FindBy(how = How.ID, using = "subHeader")
    WebElement pageHeading;

    @FindBy(how = How.ID, using = "sendNewCode")
    WebElement btnSendNewCode;

    @FindBy(how = How.XPATH, using = "//span[(@id ='featurePaySubTxt') and (text() ='Card added')]")
    WebElement lnkCardAdded;

    @FindBy(how = How.ID, using = "cardAddedText")
    WebElement cardAddedText;


    public GooglePayPushProvision(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    public boolean verifyGooglePayLink() {
        boolean linkPresent, elementPresent;
        //new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkGooglePay));
        new WebDriverWait(driver, 05); //added implicit wait as because if explicit wait will not find the element for x amount of time then it will get timeout and show failure for that


        linkPresent = isElementPresent(lnkGPay);
        if (linkPresent) {
            elementPresent = true;
            Assert.assertTrue("Google Pay link is present and displayed successfully", linkPresent);
            logger.info("PASSED: Google Pay link is present and displayed successfully ");
        } else {
            elementPresent = false;
            logger.info("Google Pay link is not present");
        }
        return elementPresent;
    }

    public boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;

        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean verifyGooglePayImage() {
        boolean googleLinkImagePresent;
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(imgGooglePayLink));
        if (imgGooglePayLink.isDisplayed()) {
            googleLinkImagePresent = true;
            Assert.assertTrue("Google Pay link Image is present and displayed successfully", googleLinkImagePresent);
            logger.info("PASSED: Google Pay link Image is present and displayed successfully ");
        } else {
            googleLinkImagePresent = false;
            Assert.assertFalse("Google Pay link Image is not present ", googleLinkImagePresent);
            logger.error("Failed: Google Pay link Image is not present ");
        }
        return googleLinkImagePresent;
    }

    public void openSelectedAccount(String accountType, String accountHolder) {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnImage));
        boolean isAccountClicked = false;
        if (accountType.equals("Combined")) {

            accountHolder = accountHolder.substring(accountHolder.length() - 4);
            logger.info("Last four digits : " + accountHolder);

            logger.info("Combined account Passed");
        }

        List<WebElement> accounts = driver.findElements(By.xpath("//td[@class='vcp_accountSummary_col']/a"));
        for (WebElement getAccount : accounts) {
            String getData = getAccount.getText();
            logger.info("Account details : " + getData);
            if (getData.equals(accountHolder)) {
                getAccount.click();
            }

        }

        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(tabPosted));
    }

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(ele));
        ele.click();
    }

    public void clickOnGooglePayLink() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkGooglePay));
        //new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkAddToSamsungPay));
        clickElement(driver, lnkGooglePay);
        Assert.assertTrue("User Clicked on Google Pay link ", true);
        logger.info("PASSED: User Clicked on Google Pay link successfully ");

    }

    public void verifyPushProvisionInitiationScreen() {
        String headingText = pageHeading.getText();
        Assert.assertTrue("MisMatch in header of Google Pay", headingText.equalsIgnoreCase("Add to Google Pay"));
        logger.info("PASSED: Google Pay heading :\"" + headingText + "\"  displayed successfully ");
    }

    public void verifySendAnewCodeButtonHidden() {

        Assert.assertFalse("Send a new Code  Button is not present ", btnSendNewCode.isDisplayed());
        logger.info("PASSED: Send a new Code Button is present and displayed successfully ");
    }

    public void verifyOTPFailureWrapUpScreen() {
        String expectedValue = "Add to Google Pay";
        verifyTextDisplayed(pageHeading, expectedValue);
    }

    public void verifyTextDisplayed(WebElement element, String expectedValue) {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(element));
        String originalValue = element.getText();
        Assert.assertTrue("MisMatch in the  heading: " + originalValue + " not displayed as expected", originalValue.equals(expectedValue));
        logger.info("PASSED: '" + originalValue + "' text displayed successfully");
    }

    public boolean verifyAddedStatus() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkCardAdded));
        boolean addedPresent;
        String status = lnkCardAdded.getText();
        if (status.equals("Card added")) {
            addedPresent = true;
            Assert.assertTrue("Google Pay link with card added status is present and displayed successfully", true);
            logger.info("PASSED: Google Pay link with card added status is present and displayed successfully ");
        } else {
            addedPresent = false;
            logger.info("Google Pay link with card added status is not present");
        }
        return addedPresent;

    }

    public void verifyInfoScreen() {
        String headingText = pageHeading.getText();
        Assert.assertTrue("MisMatch in heading  of Info screen", headingText.equalsIgnoreCase("Add to Google Pay"));
        logger.info("PASSED: Info Screen heading :\"" + headingText + "\"  displayed successfully ");
        String addedText = cardAddedText.getText();
        Assert.assertTrue("MisMatch in text  of Info screen", addedText.startsWith("Your card has already been added to "));
        logger.info("PASSED: Info Screen heading :\"" + addedText + "\"  displayed successfully ");
    }

    public void verifyGooglePayLinkNotDisplayed() {
        if (driver.findElements(By.xpath("//a[@id ='addToGooglePay']")).size() == 0) {
            logger.info("PASSED: Google Pay link is not present ");
        } else {
            logger.error("FAILED: Google Pay link is present and displayed successfully ");
        }
    }

}



