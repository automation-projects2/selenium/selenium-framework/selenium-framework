package com.usbank.test.automationFramework.trancore.pageObjects.payments;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class LongTermPlanDetailsPage {

    private static final Logger logger = LogManager.getLogger(LongTermPlanDetailsPage.class);
    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(id = "parentCollApp")
    private WebElement longTermEnrollmentWholePage;

    @FindBy(xpath = "//*[@class='longTermEnroll']/div[1]/h1")
    private WebElement longTermEnrollmentPageHeader;

    @FindBy(xpath = "//*[@class='longTermEnroll']/div[2]/p")
    private WebElement weAreGladThisLongTermProgramParagraph;

    @FindBy(xpath = "//*[@id='confirmationPaymentDetails']/div/span[1]/p/strong")
    private WebElement totalBalanceInLongTermProgram;

    @FindBy(xpath = "//*[@id='confirmationPaymentDetails']/div/span[2]/p/strong")
    private WebElement monthlyPaymentInLongTermProgram;

    @FindBy(xpath = "//*[@id='confirmationPaymentDetails']/div/span[3]/p/strong")
    private WebElement aprValueInLongTermProgram;

    @FindBy(xpath = "//*[@class='longTermEnroll']/div[2]/div[2]/h2")
    private WebElement for60BillingCyclesSubHeader;

    @FindBy(xpath = "//*[@class='longTermEnroll']/div[2]/div[2]/div/ul/li[1]")
    private WebElement for60BillingCyclesBulletPoint1;

    @FindBy(xpath = "//*[@class='longTermEnroll']/div[2]/div[2]/div/ul/li[2]")
    private WebElement for60BillingCyclesBulletPoint2;

    @FindBy(xpath = "//*[@class='longTermEnroll']/div[2]/div[2]/div/ul/li[3]")
    private WebElement for60BillingCyclesBulletPoint3;

    @FindBy(xpath = "//*[@class='longTermEnroll']/div[2]/div[3]/h2")
    private WebElement creditReportingSubHeader;

    @FindBy(xpath = "//*[@class='longTermEnroll']/div[2]/div[3]/p")
    private WebElement creditReportingParagraph;

    @FindBy(xpath = "//*[@class='longTermEnroll']/div[2]/div[4]/h2")
    private WebElement termsOfLongTermProgramSubHeader;

    @FindBy(xpath = "//*[@class='longTermEnroll']/div[2]/div[4]/div/ul/li[1]")
    private WebElement termsOfLongTermProgramBulletPoint1;

    @FindBy(xpath = "//*[@class='longTermEnroll']/div[2]/div[4]/div/ul/li[2]")
    private WebElement termsOfLongTermProgramBulletPoint2;

    @FindBy(xpath = "//*[@class='longTermEnroll']/div[2]/div[4]/div/ul/li[3]")
    private WebElement termsOfLongTermProgramBulletPoint3;

    @FindBy(xpath = "//*[@class='longTermEnroll']/div[2]/div[4]/div/ul/li[4]")
    private WebElement termsOfLongTermProgramBulletPoint4;

    @FindBy(xpath = "//*[@class='longTermEnroll']/div[2]/div[4]/div/ul/li[5]")
    private WebElement termsOfLongTermProgramBulletPoint5;

    @FindBy(xpath = "//*[@class='longTermEnroll']/div[2]/div[4]/div/ul/li[6]")
    private WebElement termsOfLongTermProgramBulletPoint6;

    @FindBy(xpath = "//*[@class='longTermEnroll']/div[2]/div[5]/h2")
    private WebElement importantInfoAboutChargeOffsSubHeader;

    @FindBy(xpath = "//*[@class='longTermEnroll']/div[2]/div[5]/p")
    private WebElement importantInfoAboutChargeOffsParagraph;

    @FindBy(xpath = "//*[@class='lthIneligibleMsg']/p[1]")
    private WebElement ifYouHaveQuestionsParagraph;

    @FindBy(xpath = "//*[@class='lthIneligibleMsg']/p[2]")
    private WebElement ifThisProgramSoundsParagraph;

    @FindBy(name = "Agree with Terms & continue")
    private WebElement agreeWithTermsButtonInLongTerm;

    @FindBy(xpath = "//*[@class='lthIneligibleMsg']/p[1]")
    private WebElement phoneEnrollmentParagraph;

    @FindBy(xpath = "//*[@class='omvTieringElement']")
    private WebElement phoneEnrollmentTimings;

    @FindBy(xpath = "//*[@id='returnToOrigination']")
    private WebElement phoneEnrollmentReturnToOriginationLink;

    @FindBy(xpath = "//*[@class='vcp_welcomeUser_message']")
    private WebElement welcomeUserMessageInTrancore;

//     ===== Long term funding account screen

    @FindBy(id = "selectFundingAccountHeader")
    private WebElement longTermFundingAccountHeader;

    @FindBy(id = "fundingAccountParagraph")
    private WebElement weWillWithdrawParagraphInFundingAccountPage;

    @FindBy(id = "importantNoteParagraph")
    private WebElement yourFirstProgramPaymentParagraphInFundingAccountPage;

    @FindBy(id = "fundingAccountLegend")
    private WebElement selectOrAddAFundingAccountSubHeader;

    @FindBy(id = "chooseAccountType")
    private WebElement chooseAccountTypeSubHeader;

    @FindBy(id = "Checking")
    private WebElement checkingRadioOption;

    @FindBy(id = "Savings")
    private WebElement savingsRadioOption;

    @FindBy(id = "Add funding account")
    private WebElement addFundingAccountRadioOption;

    @FindBy(id = "label_routingNumber")
    private WebElement routingNumberLabel;

    @FindBy(id = "input_routingNumber")
    private WebElement input_routingNumber;

    @FindBy(id = "info-label_routingNumber")
    private WebElement routingNumberHelpingText;

    @FindBy(id = "label_accountNumber")
    private WebElement accountNumberLabel;

    @FindBy(id = "input_accountNumber")
    private WebElement input_accountNumber;

    @FindBy(id = "info-label_accountNumber")
    private WebElement accountNumberHelpingText;

    @FindBy(className = "show-hide__button")
    private WebElement accountNumberShow_HideButton;

    @FindBy(id = "addFundingDisclaimer")
    private WebElement addFundingDisclaimer;

    @FindBy(id = "pymtOptionsContactParagraph")
    private WebElement pymtOptionsContactParagraph;

    @FindBy(id = "enrollmentDisclaimerParagraph")
    private WebElement enrollmentDisclaimerParagraph;

    @FindBy(name = "Submit Program enrollment")
    private WebElement submitPlanEnrollmentB;

    @FindBy(id = "cancelLink")
    private WebElement cancelLink;

    @FindBy(id = "Add funding account")
    private WebElement addFundingAccount;

    @FindAll({
            @FindBy(xpath = "//*[contains(text(),'Checking ...')][1]"),
            @FindBy(xpath = "//*[contains(text(),'Savings ...')][1]"),
    })
    private WebElement existingFundingAccount;

    public LongTermPlanDetailsPage(WebDriver inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 30);
    }

    public boolean verifyLongTermEnrollmentPageTitle() {
        wait.until(ExpectedConditions.visibilityOf(longTermEnrollmentPageHeader));
        logger.info("Long Term Enrollment page title is as expected: " + driver.getTitle());
        return driver.getTitle().equals("Credit Card Account Access: Payment help Long term program");
    }

    public boolean verifyLongTermEnrollmentPageHeader() {
        wait.until(ExpectedConditions.visibilityOf(longTermEnrollmentPageHeader));
        logger.info("Long Term Enrollment page header displayed: " + longTermEnrollmentPageHeader.getText());
        return longTermEnrollmentPageHeader.getText().equals("Long-term payment program");
    }

    public boolean verifyWeAreGladThisLongTermParagraph() {
        String last4Digits = getLast4DigitsOfAccountNumber();
        logger.info("We're glad this long-term paragraph displayed: " + weAreGladThisLongTermProgramParagraph.getText());
        return weAreGladThisLongTermProgramParagraph.getText().equals("We’re glad this long-term payment program (also referred to as the “Program”) will work for you. The Program will be in effect for the 60 billing cycles following enrollment and your APR (annual percentage rate) will be lowered. At the end of the Program, your balance will be paid in full if all payments are made according to the terms agreed to here. These are the details for your account ending in "+last4Digits+".");
    }

    public boolean verifyTotalBalanceInLongTermProgram() {
        logger.info("Total Balance In LongTerm Program displayed: " + totalBalanceInLongTermProgram.getText());
        return totalBalanceInLongTermProgram.isDisplayed();
    }

    public boolean verifyMonthlyPaymentInLongTermProgram() {
        logger.info("Monthly Payment In LongTerm Program displayed: " + monthlyPaymentInLongTermProgram.getText());
        return monthlyPaymentInLongTermProgram.isDisplayed();
    }

    public boolean verifyAPRValueInLongTermProgram() {
        logger.info("APR In LongTerm Program displayed: " + aprValueInLongTermProgram.getText());
        return aprValueInLongTermProgram.isDisplayed();
    }

    public boolean verifyFor60BillingCyclesSubHeader() {
        logger.info("For 60 Billing Cycles SubHeader displayed: " + for60BillingCyclesSubHeader.getText());
        return for60BillingCyclesSubHeader.getText().equals("Key information");
    }

    public boolean verifyFor60BillingCyclesBulletPoint1() {
        logger.info("For 60 Billing Cycles Bullet Point1 displayed: " + for60BillingCyclesBulletPoint1.getText());
        return for60BillingCyclesBulletPoint1.getText().equals("No new charges will be allowed.");
    }

    public boolean verifyFor60BillingCyclesBulletPoint2() {
        logger.info("For 60 Billing Cycles Bullet Point2 displayed: " + for60BillingCyclesBulletPoint2.getText());
        return for60BillingCyclesBulletPoint2.getText().equals("No new fees will be billed during the Program, except for those that have been assessed and may appear on your next statement.");
    }

    public boolean verifyFor60BillingCyclesBulletPoint3() {
        logger.info("For 60 Billing Cycles Bullet Point3 displayed: " + for60BillingCyclesBulletPoint3.getText());
        return for60BillingCyclesBulletPoint3.getText().equals("You may pay the remaining balance at any time without penalty.");
    }

    public boolean verifyCreditReportingSubHeader() {
        logger.info("Credit Reporting SubHeader displayed: " + creditReportingSubHeader.getText());
        return creditReportingSubHeader.getText().equals("Credit reporting");
    }

    public boolean verifyCreditReportingParagraph() {
        logger.info("Credit Reporting Paragraph displayed: " + creditReportingParagraph.getText());
        return creditReportingParagraph.getText().equals("If you're enrolled in a long-term program, we continue to report your account to the credit agencies in a delinquent status until there's no longer a past-due balance. We can't advise you as to whether or how this would impact future attempts to secure credit. (Business card accounts are not reported to consumer credit agencies.)");
    }

    public boolean verifyTermsOfLongTermProgramSubHeader() {
        logger.info("Terms Of Long Term Program SubHeader displayed: " + termsOfLongTermProgramSubHeader.getText());
        return termsOfLongTermProgramSubHeader.getText().equals("Terms of the Long-term Program");
    }

    public boolean verifyTermsOfLongTermProgramBulletPoint1() {
        logger.info("Terms Of Long Term Program Bullet Point1 displayed: " + termsOfLongTermProgramBulletPoint1.getText());
        return termsOfLongTermProgramBulletPoint1.getText().equals("When you enroll in the Program your account is closed.");
    }

    public boolean verifyTermsOfLongTermProgramBulletPoint2() {
        logger.info("Terms Of Long Term Program Bullet Point2 displayed: " + termsOfLongTermProgramBulletPoint2.getText());
        return termsOfLongTermProgramBulletPoint2.getText().equals("We set up monthly withdrawals from your preferred funding account.");
    }

    public boolean verifyTermsOfLongTermProgramBulletPoint3() {
        logger.info("Terms Of Long Term Program Bullet Point3 displayed: " + termsOfLongTermProgramBulletPoint3.getText());
        return termsOfLongTermProgramBulletPoint3.getText().equals("Your APR will be lowered for the duration of the Program.");
    }

    public boolean verifyTermsOfLongTermProgramBulletPoint4() {
        logger.info("Terms Of Long Term Program Bullet Point4 displayed: " + termsOfLongTermProgramBulletPoint4.getText());
        return termsOfLongTermProgramBulletPoint4.getText().equals("The Program will remain in effect until the account is paid in full as long as you make the monthly minimum payment.");
    }

    public boolean verifyTermsOfLongTermProgramBulletPoint5() {
        logger.info("Terms Of Long Term Program Bullet Point5 displayed: " + termsOfLongTermProgramBulletPoint5.getText());
        return termsOfLongTermProgramBulletPoint5.getText().equals("We will reserve the right to accelerate charge‑off of your account in the event of default as defined in your Cardmember Agreement.");
    }

    public boolean verifyTermsOfLongTermProgramBulletPoint6() {
        logger.info("Terms Of Long Term Program Bullet Point6 displayed: " + termsOfLongTermProgramBulletPoint6.getText());
        return termsOfLongTermProgramBulletPoint6.getText().equals("We may continue to report a delinquent status to credit reporting agencies until your account no longer has a past due balance (not applicable for business card accounts).");
    }

    public boolean verifyImportantInfoAboutChargeOffsSubHeader() {
        logger.info("Important Info About ChargeOffs SubHeader displayed: " + importantInfoAboutChargeOffsSubHeader.getText());
        return importantInfoAboutChargeOffsSubHeader.getText().equals("Important info about charge‑offs");
    }

    public boolean verifyImportantInfoAboutChargeOffsParagraph() {
        logger.info("Important Info About ChargeOffs Paragraph displayed: " + importantInfoAboutChargeOffsParagraph.getText());
        return importantInfoAboutChargeOffsParagraph.getText().equals("Charge‑off is an accounting term which means that the creditor considers a debt uncollectable. This can be due to things like an agreement not to collect an amount, an account being many months delinquent, or failure to perform a settlement agreement. These amounts are reported to credit reporting agencies and may appear on credit reports. Charged‑off debt is still owed, and creditors may still seek to collect it, unless a settlement is successfully completed.");
    }

    public boolean verifyIfYouHaveQuestionsParagraph() {
        logger.info("If You Have Questions Paragraph displayed: " + ifYouHaveQuestionsParagraph.getText());
        return ifYouHaveQuestionsParagraph.isDisplayed();
    }

    public boolean verifyIfThisProgramSoundsParagraph() {
        logger.info("If This Program Sounds Paragraph displayed: " + ifThisProgramSoundsParagraph.getText());
        return ifThisProgramSoundsParagraph.getText().equals("If this Program sounds helpful, please continue and we'll set up some payments.");
    }

    public boolean verifyAgreeWithTermsButtonInLongTermIsDisplayed() {
        logger.info("Agree With Terms Button In Long Term Program displayed: " + agreeWithTermsButtonInLongTerm.getText());
        return agreeWithTermsButtonInLongTerm.getText().equals("Agree with Terms & continue");
    }

    public WebElement clickAgreeWithTermsButtonInLongTerm() {        return agreeWithTermsButtonInLongTerm;    }

    public boolean verifyPhoneEnrollmentMessage() {
        wait.until(ExpectedConditions.visibilityOf(phoneEnrollmentParagraph));
        logger.info("Phone enrollment message in Long Term enrollment screen displayed: " + phoneEnrollmentParagraph.getText());
//        return phoneEnrollmentParagraph.getText().equals("An enrollment specialist will help you through the rest of the process. Please call us at 877-838-4347 during the hours below:");
        return phoneEnrollmentParagraph.getText().equals("An enrollment specialist will help you through the rest of the process. Please call us at 866-790-5307 during the hours below:");

    }

    public boolean verifyPhoneEnrollmentMessageTimings() {
        logger.info("Phone enrollment message timings displayed: " + phoneEnrollmentTimings.getText());
        return phoneEnrollmentTimings.getText().equals("Monday through Friday: 7 a.m. to 9 p.m. CT\n" + "Saturday: 7 a.m. to 5 p.m. CT");
    }

    public boolean verifyPhoneEnrollmentMessageReturnToOriginationLinkIsDisplayed() {
        logger.info("Phone enrollment message return to origination link displayed: " + phoneEnrollmentReturnToOriginationLink.getText());
        return phoneEnrollmentReturnToOriginationLink.isDisplayed();
    }

    public String getLast4DigitsOfAccountNumber() {
        wait.until(ExpectedConditions.visibilityOf(welcomeUserMessageInTrancore));
        String welcomeText = welcomeUserMessageInTrancore.getText();
        return welcomeText.substring(welcomeText.lastIndexOf(" ") + 1);
    }


//     =================== Long term Funding Account Screen =========================================

    public boolean verifyLongTermFundingAccountPageTitle() {
        wait.until(ExpectedConditions.visibilityOf(longTermFundingAccountHeader));
        logger.info("Long Term Funding Account page title is as expected: " + driver.getTitle());
        return driver.getTitle().equals("Credit Card Account Access: Payment help: select account");
    }

    public boolean verifyLongTermFundingAccountPageTitle_AddFundingAccount() {
        wait.until(ExpectedConditions.visibilityOf(longTermFundingAccountHeader));
        logger.info("Long Term Funding Account page title is as expected: " + driver.getTitle());
        return driver.getTitle().equals("Credit Card Account Access: Payment help: add account");
    }

    public boolean verifyLongTermFundingAccountPageHeader() {
        wait.until(ExpectedConditions.visibilityOf(longTermFundingAccountHeader));
        logger.info("Long Term Funding Account page header displayed: " + longTermFundingAccountHeader.getText());
        return longTermFundingAccountHeader.getText().equals("Select your funding account.");
    }

    public boolean verifyLongTermFundingAccountPageHeader_AddFundingAccount() {
        wait.until(ExpectedConditions.visibilityOf(longTermFundingAccountHeader));
        logger.info("Long Term Funding Account page header displayed: " + longTermFundingAccountHeader.getText());
        return longTermFundingAccountHeader.getText().equals("Add your funding account.");
    }

    public boolean verifyWeWillWithdrawParagraphInFundingAccountPageIsDisplayed() {
        logger.info("We will withdraw paragraph in LTH funding account screen displayed: " + weWillWithdrawParagraphInFundingAccountPage.getText());
        return weWillWithdrawParagraphInFundingAccountPage.isDisplayed();
    }

    public boolean verifyYourFirstProgramPaymentParagraphInFundingAccountPageIsDisplayed() {
        logger.info("Your First Program Payment Paragraph In LTH Funding Account Page displayed: " + yourFirstProgramPaymentParagraphInFundingAccountPage.getText());
        return yourFirstProgramPaymentParagraphInFundingAccountPage.isDisplayed();
//        return yourFirstProgramPaymentParagraphInFundingAccountPage.getText().equals("Your first Program payment will be scheduled for July 14. Payments will then be collected each month following, for the duration of the Program. After the Program begins, you may see on your statements that the Minimum Payment Due is higher than the Program payment amount of $32.00. This is because monthly statements are programmed to add any past due amount to the Program payment amount. As you progress through the long-term payment program, the minimum due amount you see will match the Program payment amount. Don’t worry we only withdraw the agreed upon Program payments from your chosen account.");
    }

    public boolean verifySelectOrAddAFundingAccountSubHeaderInLTHFundingAccountScreen() {
        logger.info("Select Or Add A Funding Account SubHeader In LTH Funding Account Page displayed: " + selectOrAddAFundingAccountSubHeader.getText());
        return !selectOrAddAFundingAccountSubHeader.getText().equals("Select or add a funding account.");
    }

    public boolean verifySelectAFundingAccountSubHeaderInLTHFundingAccountScreen() {
        logger.info("Select A Funding Account SubHeader In LTH Funding Account Page displayed: " + selectOrAddAFundingAccountSubHeader.getText());
        return selectOrAddAFundingAccountSubHeader.getText().equals("Select a funding account.");
    }

    public WebElement chooseAddFundingAccountRadioOptionInLTHFundingAccountScreen() {        return addFundingAccountRadioOption;    }
    public WebElement chooseCheckingRadioOptionInLTHFundingAccountScreen() {        return checkingRadioOption;    }
    public WebElement chooseSavingsRadioOptionInLTHFundingAccountScreen() {        return savingsRadioOption;    }

    public WebElement inputRoutingNumberInLTHFundingAccountScreen() {
        input_routingNumber.sendKeys(Keys.CONTROL + "a");
        input_routingNumber.sendKeys(Keys.DELETE);
        return input_routingNumber;    }

    public WebElement inputAccountNumberInLTHFundingAccountScreen() {
        input_accountNumber.sendKeys(Keys.CONTROL + "a");
        input_accountNumber.sendKeys(Keys.DELETE);
        return input_accountNumber;    }

    public WebElement clickAccountNumberShow_HideButtonInLTHFundingAccountScreen() {        return accountNumberShow_HideButton;    }

    public boolean verifyAddFundingAccountRadioOptionInLTHFundingAccountScreenIsDisplayed() {
        logger.info("Add Funding Account Radio Option In LTH Funding Account Page displayed: " + addFundingAccountRadioOption.getText());
        return addFundingAccountRadioOption.getText().equals("Add funding account");
    }

    public boolean verifyAccountTypeSubHeaderInLTHFundingAccountScreenIsDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(chooseAccountTypeSubHeader));
        logger.info("Account Type Sub Header In LTH Funding Account Page displayed: " + chooseAccountTypeSubHeader.getText());
        return chooseAccountTypeSubHeader.getText().equals("Choose account type.");
    }

    public boolean verifyCheckingRadioOptionInLTHFundingAccountScreenIsDisplayed() {
        logger.info("Checking Radio Option In LTH Funding Account Page displayed: " + checkingRadioOption.getText());
        return checkingRadioOption.getText().equals("Checking");
    }

    public boolean verifySavingsRadioOptionInLTHFundingAccountScreenIsDisplayed() {
        logger.info("Savings Radio Option in LTH Funding Account Page displayed: " + savingsRadioOption.getText());
        return savingsRadioOption.getText().equals("Savings");
    }

    public boolean verifyRoutingNumberLabelInLTHFundingAccountScreen() {
        logger.info("Routing Number Label in LTH Funding Account Page displayed: " + routingNumberLabel.getText());
        return routingNumberLabel.getText().equals("Routing number");
    }

    public boolean verifyAccountNumberLabelInLTHFundingAccountScreen() {
        logger.info("Account Number Label in LTH Funding Account Page displayed: " + accountNumberLabel.getText());
        return accountNumberLabel.getText().equals("Account number");
    }

    public boolean verifyRoutingNumberInputBoxInLTHFundingAccountScreen() {
        logger.info("Routing Number Input Box in LTH Funding Account Page displayed: " + input_routingNumber.getText());
        return input_routingNumber.isDisplayed();
    }

    public boolean verifyAccountNumberInputBoxInLTHFundingAccountScreen() {
        logger.info("Account Number Input Box in LTH Funding Account Page displayed: " + input_accountNumber.getText());
        return input_accountNumber.isDisplayed();
    }

    public boolean verifyAccountNumberShow_HideButtonInLTHFundingAccountScreen() {
        logger.info("Show_Hide Button in LTH Funding Account Page displayed: " + accountNumberShow_HideButton.getText());
        return accountNumberLabel.isDisplayed();
    }

    public boolean verifyRoutingNumberHelpingTextInLTHFundingAccountScreenIsDisplayed() {
        logger.info("Routing Number Helping Text in LTH Funding Account Page displayed: " + routingNumberHelpingText.getText());
        return routingNumberHelpingText.getText().equals("9 numbers");
    }

    public boolean verifyAccountNumberHelpingTextInLTHFundingAccountScreenIsDisplayed() {
        logger.info("Account Number Helping Text in LTH Funding Account Page displayed: " + accountNumberHelpingText.getText());
        return accountNumberHelpingText.getText().equals("Max. 17 numbers");
    }

    public boolean verifyThisAccountWillBeAvailableParagraphInLTHFundingAccountScreenIsDisplayed() {
        logger.info("This Account Will Be Available Paragraph in LTH Funding Account Page displayed: " + addFundingDisclaimer.getText());
        return addFundingDisclaimer.getText().equals("This account will be available as a funding account to other signers on the credit card.");
    }

    public boolean verifyIfYouNeedToMakeOtherPaymentParagraphInLTHFundingAccountScreenIsDisplayed() {
        logger.info("If You Need To Make Other Payment Paragraph in LTH Funding Account Page displayed: " + pymtOptionsContactParagraph.getText());
        return pymtOptionsContactParagraph.isDisplayed();
    }

    public boolean verifyBySubmittingProgramEnrollmentParagraphInLTHFundingAccountScreenIsDisplayed() {
        logger.info("By submitting Program enrollment Paragraph in LTH Funding Account Page displayed: " + enrollmentDisclaimerParagraph.getText());
        return enrollmentDisclaimerParagraph.getText().equals("By submitting Program enrollment, I attest that I am an owner on the account to be debited and I authorize all electronic payments as specified above.");
    }

    public void clickSubmitPlanEnrollmentButtonInLTHFundingAccountScreen() {
        submitPlanEnrollmentB.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    public WebElement clickCancelLinkInLTHFundingAccountScreen() {        return cancelLink;    }

    public boolean verifySubmitPlanEnrollmentButtonInLTHFundingAccountScreenIsDisplayed() {
        logger.info("Submit Plan Enrollment Button in LTH Funding Account Page displayed: " + submitPlanEnrollmentB.getText());
        return submitPlanEnrollmentB.getText().equals("Submit Program enrollment");
    }

    public boolean verifyCancelLinkInLTHFundingAccountScreenIsDisplayed() {
        logger.info("Cancel link in LTH Funding Account Page displayed: " + cancelLink.getText());
        return cancelLink.getText().equals("Cancel");
    }

public WebElement selectExistingFundingAccountInLTHFundingAccountScreen(){
    logger.info("Selecting the funding account : " + existingFundingAccount.getText());
    return existingFundingAccount;
}

public WebElement selectAnExistingFundingAccountInLTHFundingAccountPage(){
    int size = driver.findElements(By.xpath("//*[@id='fundingAccountFieldSet']/div/div")).size();
    if(size>0) {
        logger.info("Selecting the funding account : " + driver.findElement(By.xpath("//*[@id='fundingAccountFieldSet']/div/div[1]")).getText());
    }
    else
        logger.info("No existing funding accounts present on the account ");

    return driver.findElement(By.xpath("//*[@id='fundingAccountFieldSet']/div/div[1]"));
}
}
