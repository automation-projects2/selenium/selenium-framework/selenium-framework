@AlertHistory
Feature: My Alert History Management Features
  Verify that user can create multiple alerts associated with Alert page

  Scenario Outline :  Verify that user can generate multiple alerts
    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    When the user is on the account page
    Then the user selects an option under profile settings

    Examples:
      | partner  | user_name | password    |


