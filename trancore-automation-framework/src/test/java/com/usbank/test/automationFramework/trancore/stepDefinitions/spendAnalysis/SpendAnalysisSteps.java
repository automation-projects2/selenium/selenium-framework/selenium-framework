package com.usbank.test.automationFramework.trancore.stepDefinitions.spendAnalysis;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

public class SpendAnalysisSteps {

    @Autowired
    private StepDefinitionManager stepDefinitionManager;

    @Then("the user is able to view the current spend analysis of the year, month, & quarter")
    public void theUserIsAbleToViewTheCurrentSpendAnalysis() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getSpendAnalysisPage().getBtnSelectAYear().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getSpendAnalysisPage().clickAListedYear();
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getSpendAnalysisPage().getBtnYear().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getSpendAnalysisPage().clickOnYear();
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getSpendAnalysisPage().getLnkLogOut().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getSpendAnalysisPage().logout();
    }

}
