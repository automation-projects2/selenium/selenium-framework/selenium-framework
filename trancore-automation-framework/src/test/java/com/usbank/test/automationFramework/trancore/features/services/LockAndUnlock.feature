@LockAndUnlock
Feature: LockAndUnlock
  This test case ensures that the Lock And Unlock feature automation script is working

  Scenario Outline: Verify that the user can Lock & Unlock Card
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When I click Services Tab
    And I click Link - Lock or unlock card
    And the user can either click on Lock Card or Unlock Card Button depending on user account
    Then the user clicks on Return To Services Link


    Examples:
      | partner | testData   |

  Scenario Outline: Verify that the user can Unlock Card via Banner
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When I click Services Tab
    And I click Link - Lock or unlock card
    Then the user heads to the MyAccount Page to Unlock Card from the Banner


    Examples:
      | partner | testData   |

  Scenario Outline: Verify that the user can access View FAQ Link
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When I click Services Tab
    And I click Link - Lock or unlock card
    And the user can either click on Lock Card or Unlock Card Button depending on user account
    Then the user clicks on the View FAQ Link


    Examples:
      | partner | testData   |

  Scenario Outline: Verify that the user can access Report It Lost Or Stolen link
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When I click Services Tab
    And I click Link - Lock or unlock card
    And the user can either click on Lock Card or Unlock Card Button depending on user account
    Then the user clicks on Report It Lost Or Stolen Link


    Examples:
      | partner | testData   |
