# new feature
# Tags: optional
@SingleOwner
Feature: User should be able to submit the credit line increase by providing the income,asset and housing information for both consumer and Joint consumer.
  Scenario Outline: User(Consumer) should be able to submit the credit line increase by providing the income,asset and housing information.
    Given the user has navigated to the <partner> login page
    And the user has logged into with <user_name> username and <password> password
    And the user clicks on the Services tab
    And the user clicks on the Credit Line Increase link
    When the user should be navigated to Income And Assets page
    And the user enter <Pri_IncomeSource> Primary Income Source at Income And Assets page
    And the user enters <Owner_PriIncome> Owner Annual Income at Income And Assets page
    And the user clicks on Continue to Housing and should navigate to Housing Information page
    And the user enters <MonthlyHousingPayments> monthly housing payments at Housing Info page
    And the user selects radio button for <OwnedHome> Owned Current Home at Housing Info page
    And the user clicks on Review my Request button and navigates to Review my request page
    Then validate <Owner_PriIncome>, <Pri_IncomeSource>, <MonthlyHousingPayments> and <OwnedHome> at Review My Request Page
    When the user clicks on Submit My Request button at Review My Request Page
    Then the user navigates to Receive Request page
    And the user click on Finish and return to service link at Received request page
    And the customer should return to Service Tab

    Examples:
      |  partner  | user_name   | password | Owner_PriIncome | Pri_IncomeSource | MonthlyHousingPayments | OwnedHome |

  @JointOwner
  Scenario Outline: User(Joint Consumer) should be able to submit the credit line increase by providing the income,asset and housing information.
    Given the user has navigated to the <partner> login page
    And the user has logged into with <user_name> username and <password> password
    And the user clicks on the Services tab
    And the user clicks on the Credit Line Increase link
    When the user should be navigated to Income And Assets page
    And the user enter <Pri_IncomeSource> Primary Income Source at Income And Assets page
    And the user enters <Owner_PriIncome> Owner Annual Income at Income And Assets page
    And the user enter <CoOwner_IncomeSource> CoOwner Income Source at Income And Assets page
    And the user enter <CoOwner_Income> CoOwner Income at Income And Assets page
    And the user clicks on Continue to Housing and should navigate to Housing Information page
    And the user selects radio button for <SameAddress> live at same address at Housing Info page
    And the user enters <MonthlyHousingPayments> monthly housing payments at Housing Info page
    And the user selects radio button for <OwnedHome> Owned Current Home at Housing Info page
    And the user clicks on Review my Request button and navigates to Review my request page
    Then validate <Owner_PriIncome>, <Pri_IncomeSource>, <CoOwner_Income>, <CoOwner_IncomeSource>, <MonthlyHousingPayments> and <OwnedHome> for joint account
    When the user clicks on Submit My Request button at Review My Request Page
    Then the user navigates to Receive Request page
    And the user click on Finish and return to service link at Received request page
    And the customer should return to Service Tab

    Examples:

      | partner | user_name  | password | Owner_PriIncome | Pri_IncomeSource | MonthlyHousingPayments | OwnedHome |CoOwner_Income|CoOwner_IncomeSource|SameAddress|
