package com.usbank.test.automationFramework.trancore.pageObjects.services;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.NoSuchElementException;
import java.util.logging.Logger;

public class SpendAnalysisPage<T extends WebDriver> {

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'2019')]")
    private WebElement btnSelectAYear;

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Year')]")
    private WebElement btnYear;

    @FindBy(how = How.LINK_TEXT, using = "LOG OUT")
    private WebElement lnkLogOut;

    private T driver;

    private static Logger log = Logger.getLogger(SpendAnalysisPage.class.getName());

    public SpendAnalysisPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public void clickAListedYear() {
        btnSelectAYear.click();
    }

    public WebElement getBtnSelectAYear() {
        return btnSelectAYear;
    }

    public void clickOnYear() {
        btnYear.click();
    }

    public WebElement getBtnYear() {
        return btnYear;
    }

    //Do NOT delete Spend Analysis Page has to force Logout of script
    public void logout() {
        try {
            boolean isLinkThere = lnkLogOut.isDisplayed();
            if (isLinkThere) {
                WebDriverWait wait = new WebDriverWait(driver, 10);
                wait.until(ExpectedConditions.elementToBeClickable(lnkLogOut));
                //wait.until(ExpectedConditions.attributeToBe(driver.findElement(By.xpath("//div[@class='modal']")),"display","none"));
                lnkLogOut.click();
            }
        } catch (NoSuchElementException e) {
            log.info("this is page is unable to find the Log Out Link Or an error ");
        }
    }

    public WebElement getLnkLogOut() {
        return lnkLogOut;
    }


}
