package com.usbank.test.automationFramework.trancore.pageObjects.virtualCard;

import com.usbank.test.automationFramework.trancore.pageObjects.combinedAccounts.CombinedAccountsPage;
//import jdk.nashorn.internal.AssertsEnabled;
import cucumber.api.Scenario;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.junit.Assert;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.List;

import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.SECONDS;
public class virtualCard {
    private static final Logger logger = LogManager.getLogger(CombinedAccountsPage.class);
    private WebDriver driver;
    public static int EXPLICIT_WAIT_TIME_SECONDS = 30;

    public virtualCard(WebDriver inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
        driver.manage().timeouts().implicitlyWait(3,SECONDS);
    }
    @FindAll({
            @FindBy(how = How.LINK_TEXT, using = "Services"),
            @FindBy(how = How.LINK_TEXT, using = "SERVICES")
    })
    private List<WebElement> lnkServices;
    @FindBy(how = How.LINK_TEXT, using = "See your card number")
    private WebElement lnkVirtualCard;

    @FindBy(how = How.ID, using = "sendOTP")
    private WebElement btnSendOTP;

    @FindBy(how = How.ID, using = "otpAndContinue")
    private WebElement btnOTPContinue;

   // @FindBy(how = How.ID, using = "P1")
    @FindBy(how=How.XPATH, using = ".//input[@id='P1']//parent::div")
    private WebElement radioPhoneNumber;

    @FindBy(how = How.ID, using = "E1")
    private WebElement radioEmail;

    @FindBy(how = How.ID, using = "otp-value")
    private WebElement txtOTPValue;

    @FindBy(how = How.CLASS_NAME, using = "continueBtn showCardBtn")
    private WebElement imgVirtualCard;

    @FindBy(how = How.LINK_TEXT, using = "Return to Services")
    private WebElement lnkReturnToServices;

    @FindBy(how = How.ID, using = "sendANewCode")
    private WebElement btnsendANewCode;

    @FindBy(how=How.XPATH,using="//img[@title='Help']")
    private WebElement btnImage;

    public void verifyPartnerLoginPage(){

    }
    public void navigateToVC()
    {
         clickServices();
        clickVirtualCard();
    }
    public void verifySingleOTPPage(String contact)
    {
        String message1 ="We’ll send a code to "+ contact +" to use in the next step.";
       // List<WebElement> getMessagess = driver.findElements(By.xpath("//div[@class='tieringElement']"));
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//div[@class='tieringElement'])[1]")));
        WebElement getMessage = driver.findElement(By.xpath("(//div[@class='tieringElement'])[1]"));
       String getMessages = getMessage.getText();
      // boolean status=verifyMessage(message1,getMessages);
        System.out.println("the status is:" +getMessages);
        Assert.assertEquals("Single OTP page not found",message1,getMessages);
    }
    public void verifyMultipleOTPPage()
    {
        String message1="Which email or phone should we use to send a one-time code?";
        List <WebElement> getTextValues = driver.findElements(By.xpath("//div[@id='shieldLibraryComponent']/div/p"));
        boolean status = verifyMessage(message1, getTextValues);
        Assert.assertTrue("Multiple OTP Page Verification Status", true);
    }
    private boolean verifyMessage(String message1,List <WebElement> msgList){
        boolean status= false;
        for(WebElement getindividualText : msgList) {
            if ((getindividualText.getText().contains(message1))) {
                status=true;
                break;
            }
        }
        return status;
    }
    public void verifytheErrorMessage(String errorMessage){
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='errorMsg']/p")));
        WebElement getErrorFromPage = driver.findElement(By.xpath("//div[@id='errorMsg']/p"));
        if(errorMessage.equals(getErrorFromPage.getText())){
            Assert.assertTrue("Error Message Status", true);
        }
        else{
            Assert.assertTrue("Error message Status", false);
        }
    }
    public void verifyVCPage(){
        try {
            new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(imgVirtualCard));
            imgVirtualCard.click();
            driver.manage().timeouts().implicitlyWait(1,MINUTES);
            Assert.assertTrue("Virtual Card page Status", true);
        }catch(NoSuchElementException e){
            Assert.assertTrue("Virtual Card page Status", false);
        }

    }
    public void verifyContactUsForHelpPage(){
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkReturnToServices));
        WebElement getHeader = driver.findElement(By.xpath("//h1[@id='subHeader']/span"));
        String contactUsHeader = "Contact us for help";
        if(contactUsHeader.equals(getHeader.getText()))
        try{
            Assert.assertTrue("Contact Us page Status", true);
        } catch(Exception e){
            Assert.assertTrue("Contact Us page Status", false);
        }
    }

    private void clickServices(){
        if (!lnkServices.isEmpty()) {
            lnkServices.get(0).click();
            Assert.assertTrue("Services Link present", true);
        } else {
            Assert.assertTrue("Services Link not present", false);
        }
    }
    public void verifySendCodebtn(){
        try{
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(btnsendANewCode));
        }catch(Exception e){
            Assert.assertTrue("Send A Code Button Not present", true);
        }
    }
    public void clickSendOTP(){
       new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='E1']")));

        try{

            radioPhoneNumber.click();
            btnSendOTP.click();
            Assert.assertTrue("Send One Time Passcode button present", true);
        } catch(NoSuchElementException e) {
            try{
                radioEmail.click();
                btnSendOTP.click();
                Assert.assertTrue("Send One Time Passcode button present", true);
            } catch(NoSuchElementException err) {
                try {
                    btnSendOTP.click();
                    Assert.assertTrue("Send One Time Passcode button present", true);
                } catch (NoSuchElementException ex) {
                    Assert.assertTrue("Send One Time Passcode button not present", false);
                }
            }
        }
    }

    public void errorCheck(String errorMessage, Integer clickCount){
        switch(clickCount) {
            case 3:{
                new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnSendOTP));
                break;
            }
            default:{
                break;
            }
        }
        verifytheErrorMessage(errorMessage);
    }
    public void clickContinuewithOTP(String OTP){
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnOTPContinue));
        try{
            txtOTPValue.sendKeys(OTP);
            btnOTPContinue.click();
            Assert.assertTrue("Continue button present", true);
        } catch(NoSuchElementException e) {
            Assert.assertTrue("Continue button not present", false);
        }
    }

    public void ableToEnterOTP(String OTP){
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnOTPContinue));
        try{
            txtOTPValue.sendKeys(OTP);
            btnOTPContinue.click();
            Assert.assertTrue("Continue button present", true);
        } catch(NoSuchElementException e) {
            Assert.assertTrue("Continue button not present", false);
        }
    }
    private void clickVirtualCard(){
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkVirtualCard));
        try{
            lnkVirtualCard.click();
            Assert.assertTrue("Virtual Card Link present", true);
        } catch(NoSuchElementException e) {
            Assert.assertTrue("Virtual Card Link not present", false);
        }
    }

    public void clickServicesTab() {
        clickServices();
    }

    public void noVirtualCardLink()throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(10,SECONDS);

        if(driver.findElements(By.linkText("Virtual card")).size()!=0)
        try{
            logger.info("FAILED: Virtual card link present");
            Assert.assertFalse(false);
        }  catch(NoSuchElementException e){
            logger.info("PASSED: Virtual card link not present");
            Assert.assertFalse(true);
        }
    }

    public void contactUsForHelpDisplayed() throws InterruptedException{
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkReturnToServices));
        WebElement getHeader = driver.findElement(By.xpath("//*[@id=\"subHeader\"]/span"));
        String contactUsHeader = "Contact us for help";
        if(contactUsHeader.equals(getHeader.getText()))
            try{
                Assert.assertTrue("Contact Us page Status", true);
            } catch(Exception e){
                Assert.assertTrue("Contact Us page Status", false);
            }
    }

    public void displayVirtualCard() throws InterruptedException {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkReturnToServices));
        WebElement getHeader = driver.findElement(By.xpath("//div/h1[text()='See your card number']"));
        String contactUsHeader = "See your card number";
        if(contactUsHeader.equals(getHeader.getText()))
            try{
                Assert.assertTrue("Virtual Card Credentials Displayed", true);
            } catch(Exception e){
                Assert.assertTrue("Virtual Card Credentials are not Displayed", false);
            }
    }

    public String openSelectedAccount(String accountNumber) {
        {
            new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnImage));
            boolean isAccountClicked=false;
            String lastFourDigits = accountNumber.substring(accountNumber.length() - 4);
            List <WebElement> openAccount = driver.findElements(By.xpath("//td[@class='vcp_accountSummary_col']"));
            Integer getLength=0;
            String selectedAccountHolderName = "";
            WebElement lnkName = null;
            for(WebElement getAccount : openAccount){
                String getData = getAccount.getText();
                getLength = getLength +1;
                if (getData.equals(lastFourDigits)) {
                    WebElement lnkData = openAccount.get(getLength-2);
                    String[] getName = lnkData.getText().split("\n");
                    selectedAccountHolderName = getName[0];
                    driver.findElement(By.linkText(getName[0])).click();
                    isAccountClicked = true;
                    break;
                }
            }
            Assert.assertTrue(isAccountClicked);
            return selectedAccountHolderName;
        }
    }
}
