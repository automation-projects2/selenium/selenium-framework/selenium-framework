# new feature
# Tags: optional

Feature: Add/View/Delete Payment Accounts of type Checking/Savings

   #checked
  Scenario Outline: Validate that funding account of type Checking/Savings is added successfully

    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    And the user clicks the Payments Tab
    And the user clicks on Make a payment button
    And the user clicks on Add or delete payment accounts link
    And the user enters the secret answer
    And the user clicks on add an account button
    Then the user selects the <account_type>
    Then the user gives the <routing_number> info
    Then the user confirms the <routing_number>
    Then the user inputs the required <account_number>
    Then the user reconfirms the <account_number> entered
    Then the user clicks on the Next button
    Then user clicks the Submit Button
    Then the user navigates to Set up payment account page step four of four and then verify that Add payment account successful message displayed

    Examples:
      | partner | user_name  | password    | account_type | account_number  |routing_number|


  Scenario Outline: Validate that funding account added above is displaying in 'View Payment Accounts' page

    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    And the user clicks the Payments Tab
    And the user clicks on Make a payment button
    And the user clicks on Add or delete payment accounts link
    And the user enters the secret answer
    Then verify that user landed to ViewPaymentAccounts page
    Then the user search for the <account_type> AccountType and <routing_number> RoutingNumber and <account_number> Account Number and then verify record present or not

    Examples:
      | partner | user_name  | password    | account_type | account_number  |routing_number|

  Scenario Outline: Validate Delete funding account

    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    And the user clicks the Payments Tab
    And the user clicks on Make a payment button
    And the user clicks on Add or delete payment accounts link
    And the user enters the secret answer
    Then verify that user landed to ViewPaymentAccounts page
    Then the user search for the <account_type> AccountType and <routing_number> RoutingNumber and <account_number> Account Number and then the user clicks on the Delete link
    Then the user is navigated to the Delete Confirmation dialog prompt and clicks on Continue button
    Then Payment Account Delete successful and the user navigated back to ID shield Question
    Then verify that user landed to ViewPaymentAccounts page
    Then verify that <account_type> AccountType and <routing_number> RoutingNumber and <account_number> Account Number record is not present
    Examples:
      | partner | user_name  | password    | account_type | account_number  |routing_number|
