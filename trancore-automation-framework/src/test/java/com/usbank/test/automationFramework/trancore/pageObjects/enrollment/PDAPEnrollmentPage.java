package com.usbank.test.automationFramework.trancore.pageObjects.enrollment;

import com.usbank.test.trancore.PDAPData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;

public class PDAPEnrollmentPage {

    private static final Logger logger = LogManager.getLogger(EnrollmentPage.class);

    private WebDriver driver;

    public PDAPEnrollmentPage(WebDriver inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }
    @FindBy(name="locationCode")
    private WebElement txtLocationCode;

    @FindBy(xpath = "//select[@id='locationSelect']")
    private WebElement drpLocationCode;

    @FindBy(name="offerId")
    private WebElement txtOfferID;

    @FindBy(name="encodeSecureToken")
    private WebElement chkboxSecureToken;

    @FindBy(xpath="//div[text()='Target Environment-PDAP ']/following-sibling::div/select[@name='domain']")
    private WebElement drpPDAPEnvironment;

    @FindBy(name="submit")
    private WebElement buttonGo;

    @FindBy(xpath="//button[contains(@id,'group1')]")
    private WebElement buttonApplyHDConsumerCard;

    @FindBy(id = "primaryApplicantNameFirst")
    private WebElement txtFirstName;

    @FindBy(id = "primaryApplicantNameLast")
    private WebElement txtLastName;

    @FindBy(id = "primaryApplicantSsn")
    private WebElement txtSSN;

    @FindBy(id = "primaryApplicantBirthDate")
    private WebElement txtDate;

    @FindBy(how = How.XPATH, using = "//label[@for='primaryApplicantAreYouAUSCitizen0']")
    private WebElement rdoUS;

    @FindBy(id = "primaryApplicantEmail")
    private WebElement txtEmail;

    @FindBy(id = "primaryApplicantCellPhone")
    private WebElement txtPhone;

    @FindBy(id = "primaryApplicantAddressLine1")
    private WebElement txtAddressLine1;

    @FindBy(id = "primaryApplicantAddressLine2")
    private WebElement txtAddressLine2;

    @FindBy(id = "primaryApplicantAddressCity")
    private WebElement txtCity;

    @FindBy(id = "primaryApplicantAddressState")
    private WebElement txtState;

    @FindBy(id = "primaryApplicantAddressZipCode")
    private WebElement txtZip;

    @FindBy(id = "primaryApplicantEmploymentStatus")
    private WebElement drpEmployementStatus;

    @FindBy(id = "primaryApplicantAnnualIncome")
    private WebElement txtIncome;

    @FindBy(id = "primaryApplicantPrimaryAnnualIncomeSource")
    private WebElement drpIncomeSource;

    @FindBy(how = How.XPATH, using = "//label[@for='primaryApplicantHousingStatus0']")
    private WebElement rdoHousingStatus;

    @FindBy(id = "primaryApplicantHousingMonthlyPayment")
    private WebElement txtMonthlyPayment;

    @FindBy(xpath = "//button[@id='button.submitButton']")
    private WebElement btnSubmit;

    @FindBy(xpath = "//*[text()[contains(.,'Error')]]")
    private WebElement errorCode;

    @FindBy(xpath = "//button[@id='']")
    private WebElement selectThisCard;

    @FindBy(xpath = "//select[@id='primaryApplicantOccupation']")
    private WebElement drpOccupation;

    public void navigateToPDAP(String baseUrl) {driver.get(baseUrl);}
    public void selectLocationCode(PDAPData customerData) {txtLocationCode.sendKeys(customerData.getLocationcode());txtLocationCode.sendKeys(Keys.CONTROL+"t");}
    public void clickSecureToken(){chkboxSecureToken.click();}
    public void selectPDAPEnvironment(PDAPData customerData){drpPDAPEnvironment.sendKeys(customerData.getPdapEnvironment());}
    //public void selectBMWPDAPEnvironment(){drpPDAPEnvironment.sendKeys("IT1 (Elan)");}
    public void enterOfferID(PDAPData customerData){txtOfferID.sendKeys(customerData.getOfferID());}
    public void clickGoButton(){buttonGo.click();
    ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
    driver.switchTo().window(tabs.get(1));
    }
    public void clickApplyHDConsumerCard(){
             buttonApplyHDConsumerCard.click();}

    public void populateDetails(PDAPData customerData) {
        waitForPageLoad(txtFirstName);
        txtFirstName.sendKeys(customerData.getFirstname());
        txtLastName.sendKeys(customerData.getLastname());
        txtSSN.sendKeys(customerData.getSsn());
        txtDate.sendKeys("06/27/1994");
        rdoUS.click();
        txtEmail.sendKeys("nasirabanu.jamaldeen@usbank.com");
        txtPhone.sendKeys("6128391771");
        txtAddressLine1.sendKeys("2291 Belfast St W");
        drpEmployementStatus.sendKeys("Full-time employment");
        drpOccupation.sendKeys("Engineer");
        //txtAddressLine2.sendKeys("2026");
        txtCity.sendKeys("Rosemount");
        txtState.sendKeys("Minnesota");
        txtZip.sendKeys("55068");
        txtIncome.sendKeys("80000");
        drpIncomeSource.sendKeys("Employment income");
        rdoHousingStatus.click();
        txtMonthlyPayment.sendKeys("1200");
        btnSubmit.click();

    }


    public void submitEnrollmentForm() {
        btnSubmit.click();
    }

    public void clickSelectThisCard() {
        selectThisCard.click();
    }

    public WebElement getErrorCode() {
        return errorCode;
    }

    public void waitForPageLoad(WebElement getValue) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(getValue));
    }

}
