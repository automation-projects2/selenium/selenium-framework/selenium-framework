package com.usbank.test.automationFramework.trancore.pageObjects.eSignAgreement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Arrays;
import java.util.List;

public class EsignEnrollPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(EsignEnrollPage.class);

    private static final int EXPLICIT_WAIT_TIME_SECONDS = 30;
    private final T driver;

    public EsignEnrollPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);

    }

    /*<--- Manage Employee Page Objects --->*/
    @FindBy(how = How.XPATH, using = "//a[@title='Services' or @title='SERVICES']")
    private WebElement linkServicesTab;

    @FindBy(how = How.LINK_TEXT, using = "Manage employees")
    private WebElement manageEmployeeLink;

    // @FindBy(how = How.LINK_TEXT, using = "//*[@id=\"Pagination\"]/a")
    @FindBy(how = How.LINK_TEXT, using = "Next")
    private WebElement nextButton;

    @FindBy(how = How.LINK_TEXT, using = "Previous")
    private WebElement previousBtn;

    @FindBy(how = How.XPATH, using = "(//table[@class='manage-ae-table']//tr)[1]//td[5]//a[contains(text(),'Edit Spending Limit')]")
    private WebElement editSpendingLimitLink;

    @FindBy(how = How.ID, using = "spendingLimit")
    private WebElement spendingLimitInput;

    @FindBy(how = How.NAME, using = "update")
    private WebElement updateSpendingLimit;

    @FindBy(how = How.NAME, using = "Return")
    private WebElement returntoManageEmployee;


    /*<---Enrollment Page Objects--->*/

    @FindBy(how = How.ID, using = "cardNumber")
     private WebElement txtCreditCardAccountNumber;

    @FindBy(how = How.ID, using = "signaturePanelCode-tel-input")
    private WebElement txtCVV;

    @FindBy(how = How.ID, using = "ssn-tel-input")
    private WebElement txtSSN;

    @FindBy(how = How.ID, using = "zipCode")
    private WebElement txtZipCode;

    @FindBy(how = How.ID, using = "loginId1")
    private WebElement txtPersonalId;


    @FindBy(how = How.ID, using = "loginId2")
    private WebElement txtPersonalId2;

    @FindBy(how = How.ID, using = "password1")
    private WebElement txtPassword;

    @FindBy(how = How.ID, using = "password2")
    private WebElement txtPassword2;

    @FindBy(how = How.ID, using = "emailAddress1")
    private WebElement txtEmail;

    @FindBy(how = How.ID, using = "emailAddress2")
    private WebElement txtEmail2;

    @FindBy(how = How.NAME, using = "CANCEL")
    private WebElement btnCancel;

    @FindBy(how = How.ID_OR_NAME, using = "enrollFormSubmit")
    private WebElement btnSubmit;

    @FindBy(how = How.ID_OR_NAME, using = "showPin")
    private WebElement lnkShowPin;
    @FindBy(how = How.ID_OR_NAME, using = "pin")
    private WebElement txtShowPin;

    /*<--- Login again objects --->*/

    @FindBy(how = How.ID, using = "userId")
    private WebElement loginUserID;

    @FindBy(how = How.ID, using = "nextButton")
    private WebElement loginContinueBtn;

    @FindBy(how = How.ID, using = "loginPassword0")
    private WebElement loginPassword;

    @FindBy(how = How.ID, using = "loginButton")
    private WebElement loginPasswordBtn;

    @FindBy(how = How.XPATH, using = "//*[@id=\"formId\"]/div[6]/button[1]")
    private WebElement remindmeLaterBtn;

    @FindAll({
            @FindBy(how = How.XPATH, using = "//span[contains(text(),'ENROLL')]"),
            @FindBy(how = How.XPATH, using = "//span[contains(text(),'Enroll')]")
    })
    private WebElement btnEnroll;

    /*<---E-Sign Page Objects--->*/

    @FindBy(id = "esignLinkContainer")
    // @FindBy(how = How.XPATH, using = "//*[@id=\"esignLink\"]")
    // @FindBy(how = How.XPATH, using = "//*[@id=\"esignLinkContainer\"]")
    //@FindBy(how = How.XPATH, using ="//a[@id='esignLink']")
    private WebElement lnkESignConsentAgreement;
    @FindBy(id = "esignLink")
    private WebElement esignLinkview;

    @FindAll({
            @FindBy(how = How.ID_OR_NAME, using = "CLOSE WINDOW"),
            @FindBy(how = How.ID_OR_NAME, using = "closewindow")
    })
    private WebElement btnCloseWindow;

    @FindBy(how = How.ID_OR_NAME, using = "IAgreeCheckbox")
    private WebElement ckbIAgreeCheckbox;

    @FindBy(how = How.NAME, using = "CANCEL ENROLLMENT")
    private WebElement btnEAgreeCancel;

    @FindBy(how = How.ID, using = "continue")
    private WebElement btnContinue;

    @FindBy(id = "challengeQuestion")
    private WebElement lblQuestion;

    @FindBy(id = "answer")
    private WebElement txtAnswer;

    /*<---------------ID Shield Page Objects--------------->*/

    @FindBy(how = How.NAME, using = "Set Up ID Shield")
    private WebElement btnSetUpIDShield;

    @FindBy(how = How.LINK_TEXT, using = "Show More Questions")
    private WebElement lnkShowMoreQuestions;

    //Partial Xpath to find a checkbox
    @FindBy(how = How.XPATH, using = "//input[@type='checkbox']")
    private WebElement checkBox;

    //Partial Xpath to the text input
    /*@FindBy(how = How.XPATH, using = "//input[@type='text']")
    private List<WebElement> textBoxes;*/

    /*@FindBy(how = How.XPATH, using = "//input[@type='text']")
    private WebElement textBoxes2;*/

    /*@FindBy(how = How.ID, using = "//input[@type='answer'")
    private WebElement sendAnswers;*/

    @FindBy(how = How.ID, using = "alwaysAskMeToAnwser")
    private WebElement rbAlwaysAskMeToAnswer;

    @FindBy(how = How.ID, using = "next")
    private WebElement btnNext;

    /*@FindBy(how = How.CSS, using = "[id*=questionText]")
    private WebElement lstOfSecQuestions;*/

    @FindBy(how = How.ID, using = "radio0")
    private WebElement rbImageButton;

    @FindBy(how = How.ID, using = "imagePhrase")
    private WebElement txtIDShieldImageAndSoundPhrase;

    @FindBy(how = How.ID, using = "next")
    private WebElement btnNextButtonOnStep2of4IDShieldPage;

    @FindBy(how = How.XPATH, using = "//button[@name='Submit']")
    private WebElement btnSubmitOnStep3Of4IDShieldPage;

    @FindBy(how = How.XPATH, using = "//button[@name='Go To Transaction Details']")
    private WebElement btnGoToTransactionDetails;

    /*<---My Account Page Objects--->*/

    @FindAll({
            @FindBy(how = How.LINK_TEXT, using = "LOG OUT"),
            @FindBy(how = How.LINK_TEXT, using = "Log Out")
    })
    private WebElement lnkLogOut;

    /*---Navigating to Enrollment Page---*/

    public void navigateToEnrollment() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnEnroll));
        btnEnroll.click();
    }

    /*---Populates Data for Enrollment Page---*/
    public void populateEnrollDetails(List<String> values) {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(txtCreditCardAccountNumber));
        txtCreditCardAccountNumber.sendKeys(values.get(0));
        txtCVV.sendKeys(values.get(1));
        txtSSN.sendKeys(values.get(2));
        txtZipCode.sendKeys(values.get(3));
        txtPersonalId.sendKeys(values.get(4));
        txtPersonalId2.sendKeys(values.get(4));
        txtPassword.sendKeys(values.get(5));
        txtPassword2.sendKeys(values.get(5));
        txtEmail.sendKeys(values.get(6));
        txtEmail2.sendKeys(values.get(6));
        String pinNumber = values.get(7);
        if (!pinNumber.equals("")) {
            lnkShowPin.click();
            new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(txtShowPin));
            txtShowPin.sendKeys(pinNumber);
        } else {
            txtZipCode.sendKeys(values.get(3));
        }

    }

    /*---Click on Submit Button in Enroll Page---*/

    public void submitEnrollmentForm() throws InterruptedException {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnSubmit));
        synchronized (driver) {
            driver.wait(3000);
        }
        btnSubmit.click();
    }

    /*---Shows Esign Agreement Consent Page---*/

    public void viewEsignConsentPage() throws InterruptedException {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(ckbIAgreeCheckbox));
        synchronized (driver) {
            driver.wait(3000);
        }
        Assert.assertTrue(ckbIAgreeCheckbox.isDisplayed());
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkESignConsentAgreement));

        synchronized (driver) {
            driver.wait(3000);
        }
        Assert.assertTrue(lnkESignConsentAgreement.isDisplayed());
    }

    public void clearBrowsercache() throws InterruptedException {
        driver.manage().deleteAllCookies();
        synchronized (driver) {
            driver.wait(8000);
        }
        driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");
    }

    /*<---Performs a "Back" operation in the currently active Browser window--->*/

    public void browserbackbutton() throws InterruptedException {

        synchronized (driver) {
            driver.wait(3000);
        }
        driver.navigate().back();
        synchronized (driver) {
            driver.wait(5000);
        }
        Assert.assertTrue(lnkESignConsentAgreement.isDisplayed());
    }

    public void loginAgain(String personalID, String password) throws InterruptedException {
        synchronized (driver) {
            driver.wait(3000);
        }
        enterPersonalID(personalID);
        loginContinueBtn.click();
        synchronized (driver) {
            driver.wait(3000);
        }
        enterPassword(password);
        loginPasswordBtn.click();
        synchronized (driver) {
            driver.wait(3000);
        }

    }

    private void enterPassword(String password) {
        loginPassword.sendKeys(password);
    }

    private void enterPersonalID(String personalID) {
        loginUserID.sendKeys(personalID);
    }


    /*--- Click on Cancel in Esign page so that Web ID will not be created in the Casper*/
    /*--- Click on Cancel Button Initially to cancel the Enrollment ---*/
    /*--- Click on Cancell Button After reading term and conditions of Esign Enrollment ---*/
    public void cancelWebIDfromCasper() throws InterruptedException {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnCancel));
        synchronized (driver) {
            driver.wait(3000);
        }
        btnCancel.click();
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnEAgreeCancel));
        synchronized (driver) {
            driver.wait(3000);
        }
        btnEAgreeCancel.click();
    }

    /*--- Clicks on Check box and Checks it's Disabled or Not ---*/
    public void checkboxisdisabled() throws InterruptedException {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(ckbIAgreeCheckbox));
        synchronized (driver) {
            driver.wait(3000);
        }
        ckbIAgreeCheckbox.click();
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(checkBox));
        synchronized (driver) {
            driver.wait(3000);
        }
        Assert.assertFalse(checkBox.isEnabled());
    }

    /*--- Click On View Esing consent Agreement Link ---*/
    public void clickOnViewEsignConsentAgreement() throws InterruptedException {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(esignLinkview));
        synchronized (driver) {
            driver.wait(3000);
        }
        esignLinkview.click();
        driver.switchTo().activeElement();
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 500);");
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnCloseWindow));
        synchronized (driver) {
            driver.wait(3000);
        }
        btnCloseWindow.click();

    }

    public void selectCheckboxandContinue() throws InterruptedException {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(ckbIAgreeCheckbox));
        synchronized (driver) {
            driver.wait(3000);
        }
        ckbIAgreeCheckbox.click();
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnCancel));
        synchronized (driver) {
            driver.wait(3000);
        }
        btnContinue.click();
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnSetUpIDShield));
        Assert.assertTrue(btnSetUpIDShield.isDisplayed());
        synchronized (driver) {
            driver.wait(3000);
        }
    }

    /*---ID Shield Page---*/
    public void selectIDShieldQuestion() throws InterruptedException {

        String password = "testing123";
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnSetUpIDShield));
        synchronized (driver) {
            driver.wait(3000);
        }
        btnSetUpIDShield.click();
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkShowMoreQuestions));
        synchronized (driver) {
            driver.wait(3000);
        }
        lnkShowMoreQuestions.click();
        clickOnCheckBoxAndCaptureSecurityQuestions();
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(rbAlwaysAskMeToAnswer));
        synchronized (driver) {
            driver.wait(3000);
        }
        rbAlwaysAskMeToAnswer.click();
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnNext));
        synchronized (driver) {
            driver.wait(3000);
        }
        btnNext.click();
        synchronized (driver) {
            driver.wait(3000);
        }
        rbImageButton.click();
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(txtIDShieldImageAndSoundPhrase));
        txtIDShieldImageAndSoundPhrase.click();
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(txtIDShieldImageAndSoundPhrase));
        txtIDShieldImageAndSoundPhrase.sendKeys(password);
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnNextButtonOnStep2of4IDShieldPage));
        btnNextButtonOnStep2of4IDShieldPage.click();
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnSubmitOnStep3Of4IDShieldPage));
        synchronized (driver) {
            driver.wait(3000);
        }
        btnSubmitOnStep3Of4IDShieldPage.click();
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnGoToTransactionDetails));
        synchronized (driver) {
            driver.wait(3000);
        }
        btnGoToTransactionDetails.click();
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkLogOut));
        synchronized (driver) {
            driver.wait(3000);
        }
        lnkLogOut.click();

    }

    private void clickOnCheckBoxAndCaptureSecurityQuestions() throws InterruptedException {

        WebDriverWait wait = new WebDriverWait(driver, 10);

        List<WebElement> captureSecurityQuestions = driver.findElements(By.cssSelector("[id*=questionText]"));
        List<WebElement> textBox = driver.findElements(By.xpath("//input[@type='text']"));
        List<WebElement> checkBoxes = driver.findElements(By.xpath("//input[@type='checkbox']"));

        String sec = "What are the last five digits of your favorite credit card?";
        String sec0 = "What are the last five digits of your favorite frequent flyer card?";
        String sec1 = "What are the last five digits of your student id?";
        String sec2 = "What is the month and day of your anniversary?";
        String sec3 = "What is the month and year of your birth?";
        String sec4 = "What is your phone number?";
        String sec5 = "What time of day were you born?";
        String sec6 = "What date did you get married?";
        String sec7 = "What year was your eldest child born?";
        String sec8 = "In what year the eldest sibling born?";
        String sec9 = "What year was your father born?";
        String sec10 = "What was the name of your best friend in high-school?";
        String sec11 = "What country would you most like to visit?";
        String sec12 = "What is your work email address?";
        String sec13 = "What is your dream job?";
        String sec14 = "What was your favorite game as a child?";
        String sec15 = "Where would you most like to go on vacation?";
        String sec16 = "What was your favorite TV show as a child?";
        String sec17 = "What was the street number of the house in which you grew up?";
        String sec18 = "What was the model of your first car?";
        String sec19 = "What is the name of your favorite roommate?";


        String[] securityQuestionData = new String[]{sec, sec0, sec1, sec2, sec3, sec4, sec5, sec6, sec7, sec8, sec9, sec10, sec11, sec12, sec13, sec14, sec15, sec16, sec17, sec18, sec19};

        List<String> list = Arrays.asList(securityQuestionData);
        int Questioncount = 0;
        int j = 0;

        for (int i = 0; i < 10; i++) {
            String storeSecurityQuestion = captureSecurityQuestions.get(i).getText();
            String lastWordOfSecurityQuestion = storeSecurityQuestion.substring(storeSecurityQuestion.lastIndexOf(" ") + 1, storeSecurityQuestion.length() - 1);
            try {
                if (ignoreSecurityQuestions(securityQuestionData, storeSecurityQuestion)) {
                    checkBoxes.get(i).click();
                    synchronized (driver) {
                        driver.wait(2000);
                    }
                    textBox.get(j).click();
                    synchronized (driver) {
                        driver.wait(2000);
                    }
                    textBox.get(j).sendKeys(lastWordOfSecurityQuestion);
                    synchronized (driver) {
                        driver.wait(2000);
                    }
                    System.out.println("Selected Security Question " + i + ": " + storeSecurityQuestion);
                    j++;
                    Questioncount = Questioncount + 1;
                    if (Questioncount >= 3) {
                        break;
                    }
                } else {
                    if (storeSecurityQuestion.equalsIgnoreCase(sec2) ||
                            storeSecurityQuestion.equalsIgnoreCase(sec3) ||
                            storeSecurityQuestion.equalsIgnoreCase(sec5)) {
                        j = j + 2;
                    } else if (storeSecurityQuestion.equalsIgnoreCase(sec4) ||
                            storeSecurityQuestion.equalsIgnoreCase(sec6)) {
                        j = j + 3;
                    } else j++;
                }
            } catch (Exception exception) {
                synchronized (driver) {
                    driver.wait(2000);
                }
            }
        }
    }

    /*<--- This is where it checks to see if the security questions matches within the array --->*/
    private static boolean ignoreSecurityQuestions(final String[] array, final String value) {

        for (String i : array) {
            if (i.equals(value)) {
                return false;
            }
        }
        return true;
    }

    /*--- This is to ensure when user logs in again user shouldn't see the E-sign Agreement Page --->*/
    public void dontseeesignAgreement() throws InterruptedException {
        synchronized (driver) {
            driver.wait(3000);
        }
        try {
            if (remindmeLaterBtn.isDisplayed() && remindmeLaterBtn != null) {
                synchronized (driver) {
                    driver.wait(3000);
                }
                Assert.assertTrue(remindmeLaterBtn.isDisplayed());
                remindmeLaterBtn.click();
                synchronized (driver) {
                    driver.wait(3000);
                }
                Assert.assertTrue(lnkLogOut.isDisplayed());
                lnkLogOut.click();
            }
        } catch (Exception exception) {
            synchronized (driver) {
                driver.wait(3000);
            }
            Assert.assertTrue(lnkLogOut.isDisplayed());
            lnkLogOut.click();
        }
    }

    public void login(String personalID, String password) throws InterruptedException {
        synchronized (driver) {
            driver.wait(3000);
        }
        enterPersonalID(personalID);
        loginContinueBtn.click();
        synchronized (driver) {
            driver.wait(3000);
        }
        String secretAnswer = getShieldQuestionAnswer();
        enterSecretAnswer(secretAnswer);
        synchronized (driver) {
            driver.wait(3000);
        }
        loginPasswordBtn.click();
        synchronized (driver) {
            driver.wait(3000);
        }
        enterPassword(password);
        synchronized (driver) {
            driver.wait(3000);
        }
        loginPasswordBtn.click();
        synchronized (driver) {
            driver.wait(3000);
        }
    }

    private void enterSecretAnswer(String secretAnswer) {
        txtAnswer.sendKeys(secretAnswer);
    }

    private String getShieldQuestionAnswer() {
        String questionText = lblQuestion.getText();
        String lastWord = questionText.substring(questionText.lastIndexOf(" ") + 1, questionText.length() - 1);
        return lastWord;
    }

    public void clickServicesTab() throws InterruptedException {
        synchronized (driver) {
            driver.wait(3000);
        }
        Assert.assertTrue(linkServicesTab.isDisplayed());
        linkServicesTab.click();
    }

    public void clickMangeEmployee() throws InterruptedException {
        synchronized (driver) {
            driver.wait(3000);
        }
        Assert.assertTrue(manageEmployeeLink.isDisplayed());
        manageEmployeeLink.click();
        /*if (manageEmployeeLink.isDisplayed()) {
            manageEmployeeLink.click();
        }*/
    }

    public void editSpendingLimitandreturn(String SpendingLimit) throws InterruptedException {
        synchronized (driver) {
            driver.wait(3000);
        }
        try {
            while (nextButton != null && nextButton.isDisplayed()) {
                synchronized (driver) {
                    driver.wait(5000);
                }
                nextButton.click();
                synchronized (driver) {
                    driver.wait(3000);
                }
            }
        } catch (Exception exception) {
            Assert.assertTrue(editSpendingLimitLink.isDisplayed());
            synchronized (driver) {
                driver.wait(3000);
            }
            editSpendingLimitLink.click();
            synchronized (driver) {
                driver.wait(3000);
            }
            spendingLimitInput.sendKeys(SpendingLimit);
            synchronized (driver) {
                driver.wait(3000);
            }
            updateSpendingLimit.click();
            synchronized (driver) {
                driver.wait(3000);
            }
            returntoManageEmployee.click();
            synchronized (driver) {
                driver.wait(3000);
            }
        }
    }

    public void clickEditSpendingLimit() {
        Assert.assertTrue(editSpendingLimitLink.isDisplayed());
        editSpendingLimitLink.click();
    }

    public void checkNextandPreviousBtns() throws InterruptedException {
        if (previousBtn == null && nextButton.isDisplayed()) {
            synchronized (driver) {
                driver.wait(3000);
            }
            Assert.assertFalse(previousBtn.isDisplayed());
            Assert.assertTrue(nextButton.isDisplayed());
        }
        try {
            while (nextButton != null && nextButton.isDisplayed()) {
                synchronized (driver) {
                    driver.wait(3000);
                }
                nextButton.click();
                synchronized (driver) {
                    driver.wait(3000);
                }
            }
        } catch (Exception exception) {
            synchronized (driver) {
                driver.wait(3000);
            }
        }
    }
}