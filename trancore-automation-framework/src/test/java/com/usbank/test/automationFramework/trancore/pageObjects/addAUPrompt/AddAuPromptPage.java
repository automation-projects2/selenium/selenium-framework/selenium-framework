package com.usbank.test.automationFramework.trancore.pageObjects.addAUPrompt;

import com.usbank.test.automationFramework.trancore.pageObjects.combinedAccounts.CombinedAccountsPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.SECONDS;

public class AddAuPromptPage {
    private static final Logger logger = LogManager.getLogger(CombinedAccountsPage.class);

    private WebDriver driver;
    public static int EXPLICIT_WAIT_TIME_SECONDS = 20;

    public AddAuPromptPage(WebDriver inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
        driver.manage().timeouts().implicitlyWait(2,SECONDS);
    }
    @FindBy(how = How.XPATH, using = "//*[@data-testid='h2Header']//span")
    private WebElement subHeading;

    @FindBy(how = How.ID, using = "requestAddAU")
    private WebElement btnAddAuthorisedUser;

    @FindAll({
            @FindBy(how = How.ID, using = "cliReinforcementPrompt"),
            @FindBy(how = How.ID, using = "estatementsPromptReminderLink"),
            @FindBy(how = How.ID, using = "payPalRemindMeLater"),
            @FindBy(how = How.ID, using = "addAURemindMeLater")
    })

    public void VerifyAddAUInterstial() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        int size = driver .findElements(By.partialLinkText("Remind")).size();
        if (size==0)
            logger.info("Short term program link is NOT displayed");
            lnkRemindMeLater.click();
        }
    }


}
