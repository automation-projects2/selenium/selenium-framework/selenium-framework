package com.usbank.test.automationFramework.trancore.pageObjects.creditLineIncrease;

import com.usbank.test.automationFramework.trancore.pageObjects.services.AddAuthorizedUserPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class IncomePromptPage<T extends WebDriver> {
    private static final Logger logger = LogManager.getLogger(AddAuthorizedUserPage.class);

    private static final long EXPLICIT_WAIT_TIME_SECONDS = 30;

    private T driver;

    @FindBy(how = How.TAG_NAME, using = "h2")
    public WebElement headerName;

    @FindBy(how = How.ID, using = "primaryIncome_inpt")
    private WebElement textFieldPrimaryIncome;

    @FindBy(how = How.ID, using = "coOwner_inpt")
    private WebElement textFieldCoIncome;


    @FindBy(how = How.ID, using = "primaryIncomeSource")
    private WebElement dropdownPriIncomeSource;

    @FindBy(how = How.ID, using = "coOwnerIncomeSource")
    private WebElement dropdownCoIncomeSource;


    @FindBy(how = How.NAME, using = "Submit")
    private WebElement buttonSubmit;

    //@FindBy(how = How.LINK_TEXT, using = "Remind Me Later")
    @FindBy(how = How.LINK_TEXT, using = "Remind me next time")
    private WebElement buttonRemindMeLater;


    public IncomePromptPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public void verifyHeaderIncomePrompt(){
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(buttonSubmit));
        assertEquals("please update your income", (headerName.getText().toLowerCase()));

    }
    public void setTextFieldPrimaryIncome(String PrimaryIncome){
        textFieldPrimaryIncome.clear();
        textFieldPrimaryIncome.sendKeys(PrimaryIncome);
    }

    public void setTextFieldCoIncome(String CoIncome){
        textFieldCoIncome.sendKeys(CoIncome);
    }

    public void setDropdownPriIncomeSource(String PriIncomeSource){
        dropdownPriIncomeSource.sendKeys(PriIncomeSource);

    }

    public void setDropdownCoIncomeSource(String CoIncomeSource){
        dropdownCoIncomeSource.sendKeys(CoIncomeSource);
    }

    public void clickRemindMeLater(){
        buttonRemindMeLater.click();
    }

    public void clickSubmitButton(){
        buttonSubmit.click();
    }

    public void verifyErrorText(){
        List<WebElement> errorText = driver.findElements(By.className("errortext"));
        if(errorText.size()==2){
            assertEquals("Please enter income for both owners.",errorText.get(0).getText());
            assertEquals("Please select a primary source of income for both owners.",errorText.get(1).getText());

        }
        else if((textFieldPrimaryIncome.getText()=="")||(textFieldCoIncome.getText()=="")&&(errorText.size()==1)){
            assertEquals("Please enter income for both owners.",errorText.get(0).getText());
        }
        else if((dropdownPriIncomeSource.getText()=="Select")||(dropdownCoIncomeSource.getText()=="Select")&&(errorText.size()==1)){
            assertEquals("Please enter income for both owners.",errorText.get(0).getText());
        }
    }
    public void verifyPriOwnwerFields(){
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(textFieldPrimaryIncome));
        assertEquals("Select\n" +
                "Employment Income\n" +
                "Business Ownership/Sole Proprietorship\n" +
                "Government Program\n" +
                "Inheritance\n" +
                "Investments\n" +
                "Rental Income\n" +
                "Social Security\n" +
                "Pension/Retirement Income\n" +
                "Sale of Property\n" +
                "Trust Fund Disbursements",dropdownPriIncomeSource.getText());

    }
    public void verifyCoOwnwerFields(){
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(textFieldCoIncome));
        assertEquals("Select\n" +
                "Employment Income\n" +
                "Business Ownership/Sole Proprietorship\n" +
                "Government Program\n" +
                "Inheritance\n" +
                "Investments\n" +
                "Rental Income\n" +
                "Social Security\n" +
                "Pension/Retirement Income\n" +
                "Sale of Property\n" +
                "Trust Fund Disbursements",dropdownCoIncomeSource.getText());
    }

    public void CombinedAccountsValidation(String ComIncome, String ComIncSource){
        verifyPriOwnwerFields();
        clickSubmitButton();
        verifyErrorText();
        setTextFieldPrimaryIncome(ComIncome);
        clickSubmitButton();
        verifyErrorText();
        setDropdownPriIncomeSource(ComIncSource);
        clickSubmitButton();

    }
    public void verifySingleUserErrorText(){
        List<WebElement> errorText = driver.findElements(By.xpath("//tr//td[@class='errortext'][2]"));
        if(errorText.size()==2){
            assertEquals("Please enter your income.",errorText.get(0).getText());
            assertEquals("Please select a primary source of income.",errorText.get(1).getText());



        }
        else if((textFieldPrimaryIncome.getText()=="")&&(errorText.size()==1)){
            assertEquals("Please enter your income.",errorText.get(0).getText());
        }
        else if((dropdownPriIncomeSource.getText()=="Select")&&(errorText.size()==1)){
            assertEquals("Please select a primary source of income.",errorText.get(0).getText());
        }
    }



}
