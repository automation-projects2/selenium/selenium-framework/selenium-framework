# samsung Pay
@samsungPay

Feature: Samsung Pay
  This test cases ensures that the Samsung Pay feature is working is able to complete the Samsung pay flow

  @Regression @SamsungPayPushProvision
  Scenario Outline:Verify user is able to complete the Samsung Pay flow
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user has navigated to the services page
    Then display Add to Samsung Pay link
    And Samsung Pay Image is displayed
    And user click on Add to Samsung Pay link
    And display push provision initiation screen
    And verify continue button is displayed
    And the user clicks on the Continue button
    Then display Secondary Authentication Screen
    And verify contact information text is displayed
    And verify the contact methods
    And verify  send a one time code button is displayed
    And the user clicks on the Send a one time code button
    Then display OTP verification screen
    And verify enter OTP text is displayed
    And verify enter six digit code heading is displayed
    And verify input field to enter the OTP  is displayed
    And verify continue button is displayed
    And verify send a new code button is displayed
    And verify use a another method button is displayed for multiple contact user
    Then the user enters the <otp> incorrectly for the first time
    And the user clicks on the Continue button
    And verify the error message displayed in the <otp> verification screen for the first attempt
    And the user click on the Cancel Link
    Then the user is on OTP verification screen second attempt
    And the user enters the <second_otp> incorrectly for the second attempt
    And the user clicks on the Continue button
    Then verify the error messages displayed in the <second_otp> verification screen for the second attempt

    Examples:
      | partner | testData      | otp  | second_otp   |