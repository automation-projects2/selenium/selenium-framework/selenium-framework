package com.usbank.test.automationFramework.trancore.pageObjects.wrapup;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WrapUpUnsupportedReasonPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(WrapUpUnsupportedReasonPage.class);

    private T driver;


    @FindBy(xpath = "//h1[text()='Contact us for help.']")
    private WebElement headingContactUs;

    @FindBy(xpath = "//strong[text()='Coming soon']")
    private WebElement comingSoonText;


    public WrapUpUnsupportedReasonPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    public void waitForPageLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(headingContactUs));
    }

    public String getComingSoonText() {
        return comingSoonText.getText();
    }


}
