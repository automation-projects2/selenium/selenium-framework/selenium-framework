package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LessThan45Page<T extends WebDriver> {

    protected static final Logger logger = LogManager.getLogger(LessThan45Page.class);

    protected T driver;


    public LessThan45Page(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement btnContinue;

    @FindBy(xpath = "//*[text()='New card issued recently']")
    private WebElement lblLessThan45;

    @FindBy(id = "5016-Y")
    private WebElement rdbYesFirstQuestion;

    @FindBy(id = "5016-N")
    private WebElement rdbNoFirstQuestion;

    @FindBy(id = "5017-Y")
    private WebElement rdbYesSecondQuestion;

    @FindBy(id = "5017-N")
    private WebElement rdbNoSecondQuestion;

    public WebElement getLblLessThan45() {
        return lblLessThan45;
    }

    public void waitForLessThan45PageToLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(lblLessThan45));
    }

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        ele.click();
    }

    public void clickFirstQuestionNoRadioButton() {
        waitForLessThan45PageToLoad();
        clickElement(driver, rdbNoFirstQuestion);
    }

    public void clickSecondQuestionNoRadioButton() {
        waitForLessThan45PageToLoad();
        clickElement(driver, rdbNoSecondQuestion);
    }

    public void clickContinue() {
        clickElement(driver, btnContinue);
    }
}
