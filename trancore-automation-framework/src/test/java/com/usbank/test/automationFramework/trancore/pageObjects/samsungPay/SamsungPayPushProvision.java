package com.usbank.test.automationFramework.trancore.pageObjects.samsungPay;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class SamsungPayPushProvision<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(SamsungPayPushProvision.class);
    private T driver;
    public static int EXPLICIT_WAIT_TIME_SECONDS = 30;

    public SamsungPayPushProvision(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.XPATH, using = "//a[@id ='addToSamsungPay']")
    WebElement lnkAddToSamsungPay;

    @FindBy(how = How.XPATH, using = "//img[@data-testid='SamsungPayLogo']")
    WebElement imgAddToSamsungPayLink;

    @FindBy(how = How.ID, using = "subHeader")
    WebElement pageHeading;

    @FindBy(how = How.XPATH, using = "//div[@id='layoutContentBody']//div//div//img")
    WebElement imgAddToSamsungPayInitiationScreen;

    @FindBy(how = How.XPATH, using = "//div[contains(text(),'A quicker, more secure way to pay.')]")
    WebElement agreeTextAddToSamsungPayInitiationScreen;

    @FindBy(how = How.XPATH, using = "//div[contains(text(),'Start using Samsung Pay© today at millions of merchants.')]")
    WebElement samsungWalletTextAddToSamsungPayInitiationScreen;

    @FindBy(how = How.ID, using = "Continue")
    WebElement btnContinue;

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Cancel')]")
    WebElement btnCancel;

    @FindBy(how = How.XPATH, using = "//a[@id ='Cancel']")
    WebElement lnkCancel;

    @FindBy(how = How.ID, using = "contactInfoText")
    WebElement contactInfoText;

    public By AddToSamsungPay = By.xpath("//a[@id ='addToSamsungPay']");

    @FindBy(how = How.ID, using = "agreeContinueText")
    WebElement agreeContinueText;

    @FindBy(how = How.ID, using = "sendOneTimeCode")
    WebElement btnSendOneTimeCode;

    public By rdoFirstEmailcls = By.xpath("//input[@id='email0']//following::label");

    public By rdoFirstPhonecls = By.xpath("//input[@id='phone0']//following::label");

    public By btnUseAnotherMethod = By.id("useAnotherMethod");

    public By btnUseSendaNewCode = By.id("sendNewCode");

    @FindBy(how = How.XPATH, using = "//input[@id='email0']//following::label")
    WebElement rdoFirstEmail;

    @FindBy(how = How.XPATH, using = "//input[@id='phone0']//following::label")
    WebElement rdoFirstPhone;

    @FindBy(how = How.ID, using = "chooseContactMethod")
    WebElement chooseContactMethodHeading;

    @FindBy(how = How.ID, using = "enterOtpText")
    WebElement enterOtpText;

    @FindBy(how = How.XPATH, using = "//div[@id='enterCodeText']/label")
    WebElement enterCodeText;

    @FindBy(how = How.ID, using = "OTP")
    WebElement otpField;

    @FindBy(how = How.ID, using = "sendNewCode")
    WebElement btnSendNewCode;

    @FindBy(how = How.ID, using = "useAnotherMethod")
    WebElement btnAnotherMethod;

    @FindBy(how = How.XPATH, using = "//input[@name ='contactToSendOTP']")
    WebElement rdocontactMethod;

    @FindBy(how = How.ID, using = "enterOtpErrorText")
    WebElement errorMessage;//UICheck

    @FindBy(how = How.ID, using = "otpErrorText")
    WebElement errorMessageInTheSecondAttempt;//servercheck

    @FindBy(how = How.ID, using = "sendNewCode")
    WebElement btnSendANewCode;

    @FindBy(how = How.XPATH, using = "//div[@class ='tieringElement']")
    WebElement confirmationMessage;

    @FindBy(how = How.ID, using = "featurePayFailureTxt")
    WebElement lblOTPfailureScreentxt;

    @FindBy(how = How.XPATH, using = "//a[@title ='Return to services']")
    WebElement lnkReturnToServices;

    @FindBy(how = How.ID, using = "featurePayFailureWrapUpTxt")
    WebElement lblOTPfailureWrapUpScreentxt;

    @FindBy(how = How.XPATH, using = "//span[text()='Samsung Pay']//following-sibling::span[(@id ='featurePaySubTxt') and (text() ='Card added')] ")
    WebElement lnkCardAdded;

    @FindBy(how = How.ID, using = "cardAddedText")
    WebElement cardAddedText;
    @FindBy(how = How.XPATH, using = "//img[@title='Help']")
    private WebElement btnImage;
    @FindBy(id = "tc_PostedTab")
    private WebElement tabPosted;

    @FindBy(how = How.XPATH, using = " //div[@class='tieringElement']")
    private WebElement errorInformation;

    public boolean emailPresent;
    public boolean multipleContactType, firstCodeOtpError = false;
    public String expectedValue, contact;
    Integer otpCounter = 0;
    Integer sessionOldValue = 1;
    Integer sessionCounter = 0;
    Integer continueCounter = 0;

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(ele));
        ele.click();
    }

    public boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;

        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public void verifyAddToSamsungPayLinkNotDisplayed() {
        if (driver.findElements(By.xpath("//a[@title ='Add to Samsung Pay']")).size() == 0) {
            logger.info("PASSED: Add to Samsung Pay link is not present ");
        } else {
            logger.error("FAILED: Add to Samsung Pay link is present and displayed successfully ");
        }
    }

    public boolean verifyAddToSamsungPayImage() {
        boolean samsungPayLinkImagePresent;
        if (imgAddToSamsungPayLink.isDisplayed()) {
            samsungPayLinkImagePresent = true;
            Assert.assertTrue("Add to Samsung Pay link Image is present and displayed successfully", samsungPayLinkImagePresent);
            logger.info("PASSED: Add to Samsung Pay link Image is present and displayed successfully ");
        } else {
            samsungPayLinkImagePresent = false;
            Assert.assertFalse("Add to Samsung Pay link Image is not present ", samsungPayLinkImagePresent);
            logger.error("Failed: Add to Samsung Pay link Image is not present ");
        }
        return samsungPayLinkImagePresent;
    }

    public void clickOnAddToSamsungPayLink() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkAddToSamsungPay));
        clickElement(driver, lnkAddToSamsungPay);
        Assert.assertTrue("User clicked on  Samsung Pay link ", true);
        logger.info("PASSED: User clicked on  Samsung Pay link successfully ");
    }

    public void verifyPushProvisionInitiationScreen() {
        String headingText = pageHeading.getText();
        Assert.assertTrue("MisMatch in header of Add to Samsung Pay", headingText.equalsIgnoreCase("Add to Samsung Pay"));
        logger.info("PASSED: Add to Samsung Pay heading :\"" + headingText + "\"  displayed successfully ");
    }

    public void verifyContinueButton() {
        boolean buttonPresent;
        if (btnContinue.isDisplayed()) {
            buttonPresent = true;
            Assert.assertTrue("Continue Button is present and displayed successfully", buttonPresent);
            logger.info("PASSED: Continue Button is present and displayed successfully ");
        } else {
            buttonPresent = false;
            Assert.assertFalse("Continue Button is not present ", buttonPresent);
            logger.error("Failed: Continue Button is not present ");
        }
    }

    public boolean verifyAddToSamsungPayLink() {
        boolean linkPresent, elementPresent;
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkAddToSamsungPay));
        linkPresent = isElementPresent(AddToSamsungPay);
        if (linkPresent) {
            elementPresent = true;
            Assert.assertTrue("Add to Samsung Pay link is present and displayed successfully", linkPresent);
            logger.info("PASSED: Add to Samsung Pay link is present and displayed successfully ");
        } else {
            elementPresent = false;
            logger.error("FAILED: Add to Samsung Pay link is not present");
        }
        return elementPresent;
    }

    public void clickOnContinueButton() {
        clickElement(driver, btnContinue);
        Assert.assertTrue("User clicked on Continue button ", true);
        logger.info("PASSED: User clicked on Continue button successfully ");
        continueCounter++;
        logger.info("Number of Continue Button Clicked " + continueCounter);
    }

    //need to be deleted
    public boolean verifySamsungPayImageIsDisplayedOnPushProvision() {
        boolean samsungPayImageIsPresent;
        if (imgAddToSamsungPayInitiationScreen.isDisplayed()) {
            samsungPayImageIsPresent = true;
            Assert.assertTrue("Add to Samsung Pay Image on push provision initiation screen is present and displayed successfully", samsungPayImageIsPresent);
            logger.info("PASSED: Add to Samsung Pay Image on push provision initiation screen is present and displayed successfully ");
        } else {
            samsungPayImageIsPresent = false;
            Assert.assertFalse("Add to Samsung Pay Image on push provision initiation screen ", samsungPayImageIsPresent);
            logger.error("Failed: Add to Samsung Pay Image on push provision initiation screen is not present ");
        }
        return samsungPayImageIsPresent;
    }


    public void verifySamsungPayAgreeTextInitiationScreen() {
        String agreeText = agreeTextAddToSamsungPayInitiationScreen.getText();
        Assert.assertTrue("MisMatch in agree text of Add to Samsung Pay", agreeText.equalsIgnoreCase("A quicker, more secure way to pay."));
        logger.info("PASSED: Add to Samsung Pay agree text :" + agreeText + "  displayed successfully ");
    }

    public void verifySamsungPayWalletText() {
        String WalletText = samsungWalletTextAddToSamsungPayInitiationScreen.getText();
        Assert.assertTrue("MisMatch in Wallet text of Add to Samsung Pay", WalletText.equalsIgnoreCase("Start using Samsung Pay© today at millions of merchants."));
        logger.info("PASSED: Add to Samsung Pay Wallet text :" + WalletText + "  displayed successfully ");
    }

    public boolean verifyCancelButton() {
        Boolean buttonPresent;
        if (btnCancel.isDisplayed()) {
            buttonPresent = true;
            Assert.assertTrue("Cancel Button is present and displayed successfully", buttonPresent);
            logger.info("PASSED: Cancel Button is present and displayed successfully ");
        } else {
            buttonPresent = false;
            Assert.assertFalse("Cancel Button is not present ", buttonPresent);
            logger.error("Failed: Cancel Button is not present ");
        }
        return buttonPresent;
    }

    public void verifySecondaryAuthenticationScreen() {
        String expectedValue = "One-time passcode";
        verifyTextDisplayed(pageHeading, expectedValue);
    }

    public boolean verifyContactInformationText() {
        String contactText = contactInfoText.getText();
        if (contactText.startsWith("Which email or phone")) {
            multipleContactType = true;
            expectedValue = "Which email or phone should we use to send a one-time code?";
            verifyTextDisplayed(contactInfoText, expectedValue);
        } else {
            multipleContactType = false;
            String[] arrOfStr = contactText.split(" ", 13);
            contact = arrOfStr[5].trim();
            //start with is used because email is coming in the text field which is dynamic
            Assert.assertTrue("MisMatch in the contact information heading of Secondary Authentication Screen for single contact user", ((contactText.startsWith("We'll send a code to") && contactText.endsWith("to use in the next step."))));
            logger.info("PASSED: '" + contactText + " 'text displayed successfully");
        }
        logger.info("<Multiple>: " + multipleContactType);
        return multipleContactType;
    }

    public void verifyWarningInformationText() {
        String warningText = agreeContinueText.getText();
        if (warningText.startsWith("By selecting a cellular number,"))
            expectedValue = "By selecting a cellular number, you agree to receive a one-time text message with an authorization code. Message and data rates may apply.";
        else
            expectedValue = "By continuing with a cellular number, you agree to receive a one-time text message with an authorization code. Message and data rates may apply.";
        verifyTextDisplayed(agreeContinueText, expectedValue);
    }

    public void clickOnCancelButton() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnCancel));
        clickElement(driver, btnCancel);
        Assert.assertTrue("User clicked on Cancel button ", true);
        logger.info("PASSED: User clicked on Cancel button successfully ");
    }

    public void clickOnCancelLink() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkCancel));
        clickElement(driver, lnkCancel);
        Assert.assertTrue("User clicked on Cancel Link ", true);
        logger.info("PASSED: User clicked on Cancel Link successfully ");
    }

    public void verifyBackToSerivcesPage() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(pageHeading));
        String headingText = pageHeading.getText();
        Assert.assertTrue("Invalid Page", headingText.equalsIgnoreCase("Services"));
        if (!headingText.isEmpty() && headingText.equalsIgnoreCase("Services")) {
            logger.info("PASSED: User is on Services Page and " + headingText + " Page displayed successfully ");
        } else {
            logger.error("FAILED: User Failed to Redirect Back to " + headingText + " Page ");
        }
    }

    public void clickOnSendAOneTimeCode(Integer sessionCnt) {
        sessionCounter = sessionCnt;
        if (sessionOldValue.intValue() != sessionCounter.intValue()) {
            firstCodeOtpError = false;
        }
        if (otpCounter == 2 && multipleContactType) {
            firstCodeOtpError = false;
        }
        if (multipleContactType && !firstCodeOtpError) {
            clickElement(driver, rdocontactMethod);
        }
        clickElement(driver, btnSendOneTimeCode);
        Assert.assertTrue("User Clicked on Send a one time code", true);
        logger.info("PASSED: User Clicked on Send a one time code ");
        //code to fix......count
        if (sessionOldValue.intValue() == sessionCounter.intValue()) {
            otpCounter++;
        } else {
            otpCounter = 0;
            otpCounter++;
            sessionOldValue = sessionCounter;
        }
        //otpCounter++;
        logger.info("No of OTP requested: " + otpCounter);
    }

    public void verifyTextDisplayed(WebElement element, String expectedValue) {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(element));
        String originalValue = element.getText();
        Assert.assertTrue("MisMatch in the  heading: " + originalValue + " not displayed as expected", originalValue.equals(expectedValue));
        logger.info("PASSED: '" + originalValue + "' text displayed successfully");
    }

    public void VerifyButtonDisplayed(WebElement button, String btnName) {
        Assert.assertTrue(btnName + "  Button is not present ", button.isDisplayed());
        logger.info("PASSED:" + btnName + "  Button is present and displayed successfully ");
    }

    public void verifySendAoneTimeCode() {
        String btnName = " Send a One Time Code";
        VerifyButtonDisplayed(btnSendOneTimeCode, btnName);
    }

    public void verifyContactMethods() {
        if (multipleContactType) {
            new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(chooseContactMethodHeading));
            if (isElementPresent(rdoFirstEmailcls)) {
                emailPresent = true;
                contact = rdoFirstEmail.getText();
                Assert.assertTrue(contact + "  Email is not present ", contact.startsWith("Email: "));
                logger.info("Passed:Contact method '" + contact + "' is displayed");
            } else if (isElementPresent(rdoFirstPhonecls)) {
                emailPresent = false;
                contact = rdoFirstPhone.getText();
                Assert.assertTrue(contact + "  Phone no is not present ", contact.startsWith("Text: "));
                logger.info("Passed:'" + contact + "' is displayed");
            } else {
                logger.info("Contact method is not displayed");
            }
        } else {
            new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(contactInfoText));
            String contactText = contactInfoText.getText();
            Assert.assertTrue(contact + "  Contact method is not present ", contactText.contains(contact));
            logger.info("Passed:'" + contact + "' is displayed");
        }
    }

    public void verifyEnterOtpText() {
        if (multipleContactType) {
            String[] arrOfStr = contact.split(":", 2);
            contact = arrOfStr[1].trim();
        }
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(enterOtpText));
        expectedValue = "Please enter the one-time passcode sent to " + contact.trim();
        verifyTextDisplayed(enterOtpText, expectedValue);
    }

    public void verifyEnterSixDigitCodeHeading() {
        String expectedValue = "Enter 6-digit code";
        verifyTextDisplayed(enterCodeText, expectedValue);
    }

    public void verifyInputField(WebElement element, String fieldName) {
        Assert.assertTrue("Input field for " + fieldName + " is not displayed ", element.isDisplayed());
        logger.info("PASSED:Input field for " + fieldName + " is  displayed ");
    }

    public boolean verifyInputFieldToEnterOtp() {
        String fieldName = "Enter 6-digit code";
        verifyInputField(otpField, fieldName);
        return true;
    }

    /*
        public boolean verifySendANewCodeButton() {
            String btnName = " Send a new Code";
            VerifyButtonDisplayed(btnSendNewCode, btnName);
            return multipleContactType;
        }
    */
    public boolean verifySendANewCodeButton() {
        String btnName = " Send a new Code";
        if (otpCounter >= 2) {
            Assert.assertFalse("Send a new code button is present ", isElementPresent(btnUseSendaNewCode));
            logger.info("Passed: Send a new code button is not present ");
        } else
            VerifyButtonDisplayed(btnSendNewCode, btnName);
        return multipleContactType;
    }

    public void verifyUseAnotherMethodButton() {
        String btnName = "Use Another method";
        logger.info("No of OTP requested: " + otpCounter);
        if (otpCounter >= 2) {
            Assert.assertFalse("Use Another Method Button  is present ", isElementPresent(btnUseAnotherMethod));
            logger.info("Passed: Use Another Method Button is not present ");
//            otpCounter = 0;
        } else
            VerifyButtonDisplayed(btnAnotherMethod, btnName);
    }


    public void verifyErrorMessages(String otp) {
        String error = null, expectedError;
        boolean alphabetCheck = verifyAlphaNumeric(otp);
        int length = otp.length();
        expectedError = "Please enter the 6-digit code we sent. The code is numbers only, no other characters are allowed.";
        if (length < 6 && alphabetCheck == false) {
            new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(errorMessage));
            error = errorMessage.getText();
            if (otpCounter >= 2) {
                Assert.assertTrue("Error message :'" + expectedError + "' not displayed successfully", error.equals(expectedError));
            } else {
                Assert.assertTrue("Error message :'" + expectedError + "' not displayed successfully", error.equals(expectedError));
            }
        } else if (length == 6 && alphabetCheck == false) {//incorrect OTP
            new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(errorMessageInTheSecondAttempt));
            expectedError = "It looks like the code entered doesn't match what we sent or is expired. Please try again.";
            error = errorMessageInTheSecondAttempt.getText();
            if (otpCounter >= 2) {
                Assert.assertTrue("Error message :'" + expectedError + "' not displayed successfully", error.equals(expectedError));
            } else {
                Assert.assertTrue("Error message :'" + expectedError + "' not displayed successfully", error.equals(expectedError));
            }
        } else if (length <= 6 && alphabetCheck == true) {
//            expectedError = "Please enter only numeric characters";
            new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(errorMessage));
            error = errorMessage.getText();
            if (otpCounter >= 2) {
                Assert.assertTrue("Error message :'" + expectedError + "' not displayed successfully", error.equals(expectedError));
            } else {
                Assert.assertTrue("Error message :'" + expectedError + "' not displayed successfully", error.equals(expectedError));
            }
        }
        logger.info("PASSED: Error message :'" + error + "'displayed successfully");
    }

    public void enterTheOTP(String otp) {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(otpField));
        do {
            otpField.click();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            otpField.clear();
            otpField.sendKeys(otp);
        } while (!(otp.equals(otpField.getAttribute("value"))));

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public boolean verifyAlphaNumeric(String s) {
        return s != null && s.matches("(([A-Z].*[0-9])|([0-9].*[A-Z]))|(([0-9].*[a-z])|([a-z].*[0-9]))|(^[a-zA-Z]*$)");
    }

    public void verifyErrorMessageInSecondaryAuthentication() {
        String error, expectedError;
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        expectedError = "It looks like the code entered doesn't match the one we sent. Can you try again?";
        error = errorMessageInTheSecondAttempt.getText();
        if (multipleContactType)
            firstCodeOtpError = true;
//        if (continueCounter >=4){
        Assert.assertTrue("Error message :'" + expectedError + "' not displayed successfully", error.equals(expectedError));
        logger.info("Error message :'" + error + "' not displayed successfully");
//        }

    }

    public void clickOnSendANewCode() {
        clickElement(driver, btnSendANewCode);
        Assert.assertTrue("User Clicked on Send a New code button", true);
        logger.info("PASSED: User Clicked on Send a new code button");
    }

    public void verifyConfirmationMessage() {
        String expectedValue = "Your code was successfully sent to " + contact;
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'Your code was successfully sent')]")));
        verifyTextDisplayed(confirmationMessage, expectedValue);
    }

    public void clickOnAnotherMethodButton() {
        if (multipleContactType) {
            clickElement(driver, btnAnotherMethod);
            Assert.assertTrue("Use Another Method Button is not clicked Successfully", true);
            logger.info("PASSED: User clicked on Use Another Method Button");
        } else
            logger.info("PASSED:Use Another Method is not present for single contact");
    }

    public void verifyOTPCodeFailureScreenMessage() {
        String error, expectedError;
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lblOTPfailureScreentxt));
        expectedError = "We're sorry, we haven't been able to match the codes we received to the ones we sent you.";
        error = lblOTPfailureScreentxt.getText();

        Assert.assertTrue("Text :'" + expectedError + "' not displayed successfully", error.equals(expectedError));
        logger.info("PASSED: Text :'" + error + "' displayed successfully");
    }

    public void verifyReturnToSerivcesLink() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkReturnToServices));
        Assert.assertTrue(" Return to Services Link is not present ", lnkReturnToServices.isDisplayed());
        logger.info("PASSED: Return to Services Link displayed successfully ");
    }

    public void verifyOTPFailureWrapUpScreen() {
        String expectedValue = "Add to Samsung Pay";
        verifyTextDisplayed(pageHeading, expectedValue);
    }

    public void verifyOTPFailureWrapUpScreenMessage() {
        String error, expectedError;
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lblOTPfailureWrapUpScreentxt));
        expectedError = "We can't add your card at this time. If you need further assistance please call us at the number on the back of your card.";
        error = lblOTPfailureWrapUpScreentxt.getText();
        logger.info(error);
        logger.info(expectedError);
        Assert.assertTrue("Text :'" + expectedError + "' not displayed successfully", error.equals(expectedError));
        logger.info("PASSED: Text :'" + error + "' displayed successfully");
    }

    public void ClickReturnToSerivcesLink() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkReturnToServices));
        clickElement(driver, lnkReturnToServices);
        Assert.assertTrue("Return to service link is not clicked Successfully", true);
        logger.info("PASSED: Return to service link clicked");
    }

    public boolean verifyAddedStatus() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkCardAdded));
        boolean addedPresent;
        String status = lnkCardAdded.getText();
        if (status.equals("Card added")) {
            addedPresent = true;
            Assert.assertTrue("Samsung Pay link with card added status is present and displayed successfully", true);
            logger.info("PASSED: Samsung Pay link with card added status is present and displayed successfully ");
        } else {
            addedPresent = false;
            logger.info("samsung Pay link with card added status is not present");
        }
        return addedPresent;
    }

    public void verifyInfoScreen() {
        String headingText = pageHeading.getText();
        Assert.assertTrue("MisMatch in heading  of Info screen", headingText.equalsIgnoreCase("Add to Samsung Pay"));
        logger.info("PASSED: Info Screen heading :\"" + headingText + "\"  displayed successfully ");
        String addedText = cardAddedText.getText();
        Assert.assertTrue("MisMatch in text  of Info screen", addedText.startsWith("Your card has already been added to "));
        logger.info("PASSED: Info Screen heading :\"" + addedText + "\"  displayed successfully ");
    }

    public void verifySetUpNotCompletePage() {
        String text = errorInformation.getText();
        Assert.assertTrue("MisMatch in the body of set up not complete screen", text.startsWith("Before we can continue"));
        logger.info("PASSED:Set up not complete screen  :\"" + text + "\"  displayed successfully ");
    }

}

