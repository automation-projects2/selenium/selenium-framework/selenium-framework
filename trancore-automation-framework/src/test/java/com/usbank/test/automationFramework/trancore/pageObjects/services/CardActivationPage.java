package com.usbank.test.automationFramework.trancore.pageObjects.services;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.internal.StringUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class CardActivationPage {

    private static final Logger logger = LogManager.getLogger(CardActivationPage.class);

    private static final long EXPLICIT_WAIT_TIME_SECONDS = 30;

    private WebDriver driver;

    @FindBy(how = How.TAG_NAME, using = "h2")
    private WebElement headername;

    @FindBy(how = How.ID, using = "last4SSN-tel-input")
    private WebElement txtFldLast4SSN;

    @FindBy(how = How.ID, using = "securityCode-tel-input")
    private WebElement txtFldCVV;

    @FindBy(how = How.CLASS_NAME, using = "ca-return-msg")
    private WebElement bodyTextUnsucessfulCardAct;

    @FindBy(how = How.CLASS_NAME, using = "ca-button")
    private WebElement buttonReturntoCardActivation;

    @FindBy(how = How.CLASS_NAME, using = "ca-recurring-billers-link")
    private WebElement buttonReviewRecurringChargesMerchants;

    @FindBy(how = How.ID, using = "cardActivationDialogHeader")
    private WebElement dialogHeaderName;

    @FindBy(how = How.ID, using = "cardActivationBody")
    private WebElement dialogBodyText;

    @FindBy(how = How.CLASS_NAME, using = "omv_close")
    private WebElement dialogCloseButton;

    public CardActivationPage(WebDriver inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    public void verifyHeaderName() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(txtFldCVV));
        assertEquals("card activation", (headername.getText()).toLowerCase());
    }

    public void setTxtFldLast4SSN(String Last4SSN) {
        txtFldLast4SSN.clear();
        txtFldLast4SSN.sendKeys(Last4SSN);
    }

    public void setTxtFldCVV(String CVV) {
        txtFldCVV.clear();
        txtFldCVV.sendKeys(CVV);
    }

    public void clickSubmitButton() {
        List<WebElement> btnSubmit = driver.findElements(By.className("svc-btn-secondary"));
        for (int i = 0; i <= btnSubmit.size(); i++) {
            if (btnSubmit.get(i).getText().equals("Submit")) {
                btnSubmit.get(i).click();
                break;
            }
        }
    }

    public void clickCancelButton() {
        List<WebElement> btnCancel = driver.findElements(By.className("svc-btn-secondary"));
        for (int i = 0; i <= btnCancel.size(); i++) {
            if (btnCancel.get(i).getText().equals("Cancel")) {
                btnCancel.get(i).click();
                break;
            }
        }
    }

    public void validateErrorText() {
        System.out.println("text of CVV" + txtFldCVV.getAttribute("value"));
        if ((txtFldCVV.getAttribute("value").equals("")) && (txtFldLast4SSN.getAttribute("value").equals(""))) {
            List<WebElement> errorTexts = driver.findElements(By.className("ca-errortext"));
            assertEquals("-", errorTexts.get(0).getText());
            assertEquals("Please enter the Last Four Digits of your Social Security Number.\n" +
                    "Please enter a Security Code.", errorTexts.get(1).getText());
            assertEquals("Please enter the Last Four Digits of your Social Security Number.", errorTexts.get(2).getText());
            assertEquals("Please enter a Security Code.", errorTexts.get(3).getText());
        } else if (txtFldLast4SSN.getAttribute("value").equals("")) {
            List<WebElement> errorTextSSN = driver.findElements(By.className("ca-errortext"));
            assertEquals("-", errorTextSSN.get(0).getText());
            assertEquals("Please enter the Last Four Digits of your Social Security Number.", errorTextSSN.get(1).getText());
            assertEquals("Please enter the Last Four Digits of your Social Security Number.", errorTextSSN.get(2).getText());
        } else if (txtFldCVV.getAttribute("value").equals("")) {
            List<WebElement> errorTextCVV = driver.findElements(By.className("ca-errortext"));
            assertEquals("-", errorTextCVV.get(0).getText());
            assertEquals("Please enter a Security Code.", errorTextCVV.get(1).getText());
            assertEquals("Please enter a Security Code.", errorTextCVV.get(2).getText());
        }
    }

    public void ValidateUnsucessfulCardActivation() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(buttonReturntoCardActivation));
        assertEquals("card activation unsuccessful", headername.getText().toLowerCase());
        assertEquals("Return to Card Activation to re-submit your request.", bodyTextUnsucessfulCardAct.getText());
    }

    public void ValidateSucessfulCardActivation() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(buttonReviewRecurringChargesMerchants));
        assertEquals("Card Activation Successful", headername.getText());

    }

    public void clickReturnToCardActivation() {
        buttonReturntoCardActivation.click();
    }

    public void clickReviewRecurringChargesMerchants() {
        buttonReviewRecurringChargesMerchants.click();
    }

    public void validateErrorTextBlankFields() {
        System.out.println("text of CVV" + txtFldCVV.getAttribute("value"));
        if ((txtFldCVV.getAttribute("value").equals("")) && (txtFldLast4SSN.getAttribute("value").equals(""))) {
            List<WebElement> errorTexts = driver.findElements(By.className("ca-errortext"));
            assertEquals("-", errorTexts.get(0).getText());
            assertEquals("Please enter the Last Four Digits of your Social Security Number.\n" +
                    "Please enter a Security Code.", errorTexts.get(1).getText());
            assertEquals("Please enter the Last Four Digits of your Social Security Number.", errorTexts.get(2).getText());
            assertEquals("Please enter a Security Code.", errorTexts.get(3).getText());
        } else if (txtFldLast4SSN.getAttribute("value").equals("")) {
            List<WebElement> errorTextSSN = driver.findElements(By.className("ca-errortext"));
            assertEquals("-", errorTextSSN.get(0).getText());
            assertEquals("Please enter the Last Four Digits of your Social Security Number.", errorTextSSN.get(1).getText());
            assertEquals("Please enter the Last Four Digits of your Social Security Number.", errorTextSSN.get(2).getText());
        } else if (txtFldCVV.getAttribute("value").equals("")) {
            List<WebElement> errorTextCVV = driver.findElements(By.className("ca-errortext"));
            assertEquals("-", errorTextCVV.get(0).getText());
            assertEquals("Please enter a Security Code.", errorTextCVV.get(1).getText());
            assertEquals("Please enter a Security Code.", errorTextCVV.get(2).getText());
        }
    }

    // This Function is for validating Combined errors of blank and invalid text for SSN and CVV
//    developed by Sanjay K(Line Of Sight)
    public void validateErrorTextforInvalidAndBlankFields() {
        String SSNflag = "0";
        String CVVflag = "0";
        String txtSSNValue = txtFldLast4SSN.getAttribute("value");
        System.out.println("Length of SSN value" + txtSSNValue.length());
        if (!(StringUtil.isNumeric(txtSSNValue)) && (txtSSNValue.length() <= 4) && (txtSSNValue.length() > 0)) {
            SSNflag = "a";
        } else if ((StringUtil.isNumeric(txtSSNValue)) && (txtSSNValue.length() < 4) && (txtSSNValue.length() > 0)) {
            SSNflag = "a";
        } else if (txtSSNValue.equals("")) {
            SSNflag = "b";
        } else {
            SSNflag = "c";
        }
        String txtCVVValue = txtFldCVV.getAttribute("value");
        if (!(StringUtil.isNumeric(txtCVVValue)) && ((txtCVVValue.length()) <= 3) && (txtCVVValue.length() > 0)) {
            CVVflag = "1";
        }
        if ((StringUtil.isNumeric(txtCVVValue)) && ((txtCVVValue.length()) < 3) && (txtCVVValue.length() > 0)) {
            CVVflag = "1";
        } else if (txtCVVValue.equals("")) {
            CVVflag = "2";
        }
        String errorFlag = SSNflag + CVVflag;
        System.out.println("ERROR FlAG IS " + errorFlag);
        switch (errorFlag) {
            case ("a1"): {
                List<WebElement> errorTexts = driver.findElements(By.className("ca-errortext"));
                assertEquals("-", errorTexts.get(0).getText());
                assertEquals("Social Security Number field can only contain 4-digit numerical values.\n" +
                        "Security Code field can only contain 3-digit numerical values.", errorTexts.get(1).getText());
                assertEquals("Social Security Number field can only contain 4-digit numerical values.", errorTexts.get(2).getText());
                assertEquals("Security Code field can only contain 3-digit numerical values.", errorTexts.get(3).getText());
                break;
            }
            case ("a2"): {
                List<WebElement> errorTexts = driver.findElements(By.className("ca-errortext"));
                assertEquals("-", errorTexts.get(0).getText());
                assertEquals("Social Security Number field can only contain 4-digit numerical values.\n" +
                        "Please enter a Security Code.", errorTexts.get(1).getText());
                assertEquals("Social Security Number field can only contain 4-digit numerical values.", errorTexts.get(2).getText());
                assertEquals("Please enter a Security Code.", errorTexts.get(3).getText());
                break;
            }
            case ("a0"): {
                List<WebElement> errorTextCVV = driver.findElements(By.className("ca-errortext"));
                assertEquals("-", errorTextCVV.get(0).getText());
                assertEquals("Social Security Number field can only contain 4-digit numerical values.", errorTextCVV.get(1).getText());

            }
            case ("b1"): {
                List<WebElement> errorTexts = driver.findElements(By.className("ca-errortext"));
                assertEquals("-", errorTexts.get(0).getText());
                assertEquals("Please enter the Last Four Digits of your Social Security Number.\n" +
                        "Security Code field can only contain 3-digit numerical values.", errorTexts.get(1).getText());
                assertEquals("Please enter the Last Four Digits of your Social Security Number.", errorTexts.get(2).getText());
                assertEquals("Security Code field can only contain 3-digit numerical values.", errorTexts.get(3).getText());
                break;
            }
            case ("b2"): {
                List<WebElement> errorTexts = driver.findElements(By.className("ca-errortext"));
                assertEquals("-", errorTexts.get(0).getText());
                assertEquals("Please enter the Last Four Digits of your Social Security Number.\n" +
                        "Please enter a Security Code.", errorTexts.get(1).getText());
                assertEquals("Please enter the Last Four Digits of your Social Security Number.", errorTexts.get(2).getText());
                assertEquals("Please enter a Security Code.", errorTexts.get(3).getText());
                break;
            }
            case ("b0"): {
                List<WebElement> errorText = driver.findElements(By.className("ca-errortext"));
                assertEquals("-", errorText.get(0).getText());
                assertEquals("Please enter the Last Four Digits of your Social Security Number.", errorText.get(1).getText());

            }
            case ("c1"): {
                List<WebElement> errorText = driver.findElements(By.className("ca-errortext"));
                assertEquals("-", errorText.get(0).getText());
                assertEquals("Security Code field can only contain 3-digit numerical values.", errorText.get(1).getText());
                break;
            }
            case ("c2"): {
                List<WebElement> errorText = driver.findElements(By.className("ca-errortext"));
                assertEquals("-", errorText.get(0).getText());
                assertEquals("Please enter a Security Code.", errorText.get(1).getText());
                break;
            }
            case ("c0"): {
                System.out.println("No error message displayed");
            }
        }
    }

    public void validateAlreadyCardActivatedDialogueBox() {
        assertEquals("Card(s) Already Activated", dialogHeaderName.getText());
        assertEquals("Our records indicate you have already activated your card(s).", dialogBodyText.getText());
        dialogCloseButton.click();
    }
}
