#Author: Ben Pemberton (bmpembe)
@EsignAgreementSuite @EsignLogin @WIP @Regression
Feature: E-sign Login

  @TC001
  Scenario Outline: E-Sign_Login_Validate that System shall not re-prompt a user who has already provided E-SIGN
    Given the user has navigated to the <partner> login page on <environment> environment
    And the user navigates to the Enrollment Page, enters all details and clicks Submit
      | <creditCardNumber> | <cvv> | <ssn> | <zip> | <personalID> | <password> | <email> | <pin> |
    And user checks E-SIGN Consent Agreement once done closes the E-SIGN Consent Agreement window
    When user clicks checkbox and continues to enrollment process and sees ID shield page
    And completes the enrollment setup for the selected account
    And the user has navigated to the <partner> login page on <environment> environment
    Then the user has logged into with <personalID> username and <password> password
    And  user should not see the e-sign agreement page

    Examples:
      | partner | creditCardNumber | cvv | ssn  | zip   | personalID      | password | email                | pin | environment |

  @TC002
  Scenario Outline:  E-Sign_Validate that Browser back button is blocked when E-Sign page is in focus
    Given the user has navigated to the <partner> login page on <environment> environment
    And the user navigates to the Enrollment Page, enters all details and clicks Submit
      | <creditCardNumber> | <cvv> | <ssn> | <zip> | <personalID> | <password> | <email> | <pin> |
    When user is able to see E-Sign Consent Page and clears the browser cache
    And the user has navigated to the <partner> login page on <environment> environment
    Then the user has logged in with recent <personalID> username and <password> password
    And  user clicks on the back button but still can see the E-sign Consent Page


    Examples:
      | partner | creditCardNumber | cvv | ssn  | zip   | personalID  | password  | email              | pin | environment |
