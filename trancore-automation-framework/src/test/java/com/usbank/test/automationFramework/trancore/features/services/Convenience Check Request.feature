#Author: Ben Pemberton (bmpembe)
@CardmemberServicesSuite @ConvenienceCheckRequest @WIP @Regression
Feature: Convenience Check Request

  @ConvenienceCheck
  Scenario Outline: Request Convenience Checks
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When I click Services Tab
    And I click link - Convenience Check Request
    And the user can see customer details on the page
    Then the user clicks on the Submit button on the Request Convenience Page
    And user get the Convenience Check Request confirmation page

    Examples:
      | partner | testData      |


  @ConvenienceCheck @Cancel
  Scenario Outline: Request Convenience Checks cancel redirects to Services page
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When I click Services Tab
    And I click link - Convenience Check Request
    And the user can see customer details on the page
    Then the user clicks on the Cancel button on the Request Convenience Page
    Then display the Services page

    Examples:
      | partner | testData      |