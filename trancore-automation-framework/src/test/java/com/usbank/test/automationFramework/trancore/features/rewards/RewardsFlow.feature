@Rewards

Feature: This Feature verifies whether user is able to verify Rewards functionality

  @RewardsRedirection
  Scenario Outline: Verify that the user is able to redirect to Rewards Page

    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    And the user clicks the Rewards Tab
    And the user clicks on Link To Rewards button
    Then the user is navigated to Rewards Center Page
    When the user clicks on Back to Account
    Then the user should be navigated back to Trancore My Account page

    Examples:
      | partner  | user_name     | password   |