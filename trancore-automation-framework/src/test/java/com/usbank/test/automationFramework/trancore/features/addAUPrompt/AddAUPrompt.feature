@AddAuPrompt
Feature: Add AU prompt
  This test case ensures that the Add AU Prompt feature is working fine

  @VerifyAddAUPromptLoginProfile
  Scenario Outline: Verify that Add AU Prompt Displayed when login with Login Profile
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    Then while login will verify Add AU Prompt

    Examples:
      | partner | testData      |


  @VerifyAddAUPromptWithLoginandPassword
  Scenario Outline: Verify that Add AU Prompt Displayed with Regular login with username and password
    Given the user has navigated to the <partner> login page
    And the user has logged into application with <user_name> username and <password> password
    And Add AU Feature Flag in On
    Then show Add AU interstitial

    Examples:
      | partner | user_name  | password   |
