package com.usbank.test.automationFramework.trancore.stepDefinitions.payments;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static java.lang.Thread.*;

public class MakePaymentSteps {

    @Autowired
    StepDefinitionManager stepDefinitionManager;
    WebDriver driver;

    List<String> values = null;
    String EditDate ="";


    @Then("user clicks on Edit Scheduled Payment Link")
    public void userClicksOneditScheduledPaymentLink() {

        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().clickOnEditScheduledPay();
    }

    @Then("user has navigated to Payments schedule Page")
    public void userHasNavigatedToPaymentsSchedulePage() {
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().clickOnViewPaymentScheduledLink();
    }

    @Then("user edits (.+) Amount and Date of Payment")
    public void userEditsAmountAndDateOfPayment(String Amount) {
        EditDate = stepDefinitionManager.getPageObjectManager().getMakePaymentPage().generateRandomDate();
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().editDateAndAmountOfScheduledPay(Amount, EditDate);
    }

    @Then("user submits the Payment")
    public void userSubmitsThePayment() {
      stepDefinitionManager.getPageObjectManager().getMakePaymentPage().submitPayment();

    }


    @Then("user verifies the updates (.+) Amount and Date made in Payments Schedule Page")
    public void userVerifiesTheUpdatesAmountAmountAndDateDateMadeInPaymentsSchedulePage(String Amount)  {

       stepDefinitionManager.getPageObjectManager().getMakePaymentPage().verifyEditPayment(Amount, EditDate);
    }

    @Then("user deletes all existing Scheduled Payments if present")
    public void userDeletesAllExistingScheduledPaymentsIfPresent() throws InterruptedException {
        sleep(3000);
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().deleteAllExistingScheduledPay();
    }


    @Then("the user selects (.+) Payment Type for Making the Payment")
    public void theUserSlelctsPaymentTypePaymentTypeForMakingPayment(String PaymentType) {

        String Date = stepDefinitionManager.getPageObjectManager().getMakePaymentPage().generateRandomDate();
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().enterPaymentDetails(PaymentType,Date);

    }

    @Then("user deletes recently made Payment")
    public void userDeletesRecentlyMadePayment() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().deletePayment();
    }

    @Then("the user verifies values of recently made payment")
    public void theUserVerifiesValuesOfRecentlyMadePayment() throws InterruptedException{
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().verifySubmittedPayment(values);

    }

    @Then("user collects the values to be submitted for Payment")
    public void userCollectsTheValuesToBeSubmittedForPayment() {
        values = stepDefinitionManager.getPageObjectManager().getMakePaymentPage().collectPaymentValues();
    }


    @Then("the user clicks on the Next button to go to next page")
    public void theUserClicksOnTheNextButtonToGoToNextPage() {
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().clickNextButton();
    }

    @Then("user clicks on Alerts link in the Submission Page")
    public void userClicksOnAlertsLinkInTheSubmissionPage() {
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().clickAlertLink();
    }

    @Then("user verifies whether he landed on Alerts Page")
    public void userVerifiesWhetherHeLandedOnAlertsPage() {
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().verifyAlertsPageLanding();
    }

    @Then("the user clicks on Payment History Link")
    public void theUserClicksOnpaymentHistoryLink() {
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().clickPaymantHistoryLink();
    }

    @Then("user verifies whether he landed on Payments History Page")
    public void userVerifiesWhetherHeLandedOnPaymentsHistoryPage() {
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().verifyPaymnetHistoryPageLanding();
    }

    @Then("the user clicks on the Cancel Button in first Page of Make a Payment")
    public void theUserClicksOnTheCancelButtonInFirstPageOfMakeAPayment() {
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().clickCancel1();
    }

    @Then("the user clicks on the Cancel Button in second Page of Make a Payment")
    public void theUserClicksOnTheCancelButtonInSecondPageOfMakeAPayment() {
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().clickCancel2();
    }

    @Then("the user clicks on Back button to go to Previous Page")
    public void theUserClicksOnBackButtonToGoToPreviousPage() {
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().clickBack();
    }

    @Then("user then rejects the Cancellation by clicking No on the pop up")
    public void userThenRejectsTheCancellationByClickingNoOnThePopUp() {
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().clickPopUpNo();
    }

    @Then("user then confirms the Cancellation by clicking Yes on the pop up")
    public void userThenConfirmsTheCancellationByClickingYesOnThePopUp() {
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().clickPopUpYes();
    }

    @Then("user verifies whether he lands back on the Payments tab after Cancellation")
    public void userVerifiesWhetherHeLandsBackOnThePaymentsTabAfterCancellation() {
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().verifyPaymentsLandingPage();
    }

    @Then("user checks and add Funding Account (.+) and (.+) and (.+) if not present already")
    public void userChecksAndAddFundingAccountAccount_typeAndAccount_numberAndRouting_numberIfNotPresentAlready(String accountType, String accountNumber, String routingNumber) throws InterruptedException {
        sleep(3000);
        boolean FAPresent = stepDefinitionManager.getPageObjectManager().getMakePaymentPage().checkFundingAccountPresent();

        if (!FAPresent)
        {
            sleep(3000);
            stepDefinitionManager.getPageObjectManager().getMakePaymentPage().inputSecurityQuestion();
            stepDefinitionManager.getPageObjectManager().getMakePaymentPage().clickNextButton();
            stepDefinitionManager.getPageObjectManager().getPaymentAccountPage().selectAccountType(accountType);
            stepDefinitionManager.getPageObjectManager().getPaymentAccountPage().enterRoutingNumber(routingNumber);
            stepDefinitionManager.getPageObjectManager().getPaymentAccountPage().confirmRoutingNumber(routingNumber);
            stepDefinitionManager.getPageObjectManager().getPaymentAccountPage().enterAccountNumber(accountNumber);
            stepDefinitionManager.getPageObjectManager().getPaymentAccountPage().confirmAccountNumber(accountNumber);
            stepDefinitionManager.getPageObjectManager().getPaymentAccountPage().clickNext();
            stepDefinitionManager.getPageObjectManager().getPaymentAccountPage().clickSubmit();
            stepDefinitionManager.getPageObjectManager().getPaymentAccountPage().clickPaymentsButtonInAddFundingAccountPage4of4();
            stepDefinitionManager.getPageObjectManager().getMakePaymentPage().clickMakePaymentsLink();

        }
    }
}
