package com.usbank.test.automationFramework.trancore.pageObjects.profile;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class ContactInformation<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(ContactInformation.class);

    private T driver;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'Add company phone number')]")
    private WebElement linkAddCompanyPhone;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'Cancel')]")
    private WebElement btnCancel;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'ll do it later')]")
    private WebElement btnIWillDoItLater;

    @FindBy(how = How.XPATH, using = "//*[@id=\"subHeader\"]/span[contains(text(),'Contact information')]")
    private WebElement HdrContactInformation;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'Continue adding number')]")
    private WebElement btnContinueAddingPhoneNumber;

    @FindBy(how = How.XPATH, using = "(//*[contains(text(),'Add company phone number')])[3]")
    private WebElement msgContinueAddingPhoneNumber;

    @FindBy(how = How.ID, using = "companyPhoneNumber")
    private WebElement txtAddPhoneNumber;

    @FindBy(how = How.XPATH, using = "//*[@id=\"layoutContentBody\"]//*[contains(text(),'Add company phone number')]")
    private WebElement btnAddCompanyPhoneNumber;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'Success! Thanks for adding your phone number.')]")
    private WebElement msgSuccess;

    @FindBy(how = How.LINK_TEXT, using = "Edit company phone number")
    private WebElement lnkEditCompanyPhone;

    @FindBy(how = How.XPATH, using = "//*[@id=\"newCompanyPhoneNumber\"]")
    private WebElement txtEditCompanyPhone;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'t leave')]")
    private WebElement btnNoLeave;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'Edit company phone number')]")
    private WebElement HdrEditCompanyPhone;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'Update company phone number')]")
    private WebElement btnUpdatePhoneNumber;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'Success! Thanks for updating your phone number.')]")
    private WebElement updateSuccessMsg;

    public ContactInformation(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    public void clickAddCompanyPhone() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        linkAddCompanyPhone.click();
    }

    public void clickCancelonAddComapnyPhone() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        btnCancel.click();
    }

    public void clickIwillDoitLater() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        btnIWillDoItLater.click();

    }

    public void validateNavigationtoContactInformationPage() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {

            e.printStackTrace();
        }
        String ContactInformation = HdrContactInformation.getText();
        System.out.println("ContactInformation" + ContactInformation);

    }

    public void clickContinueAddingPhonenumber() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        btnContinueAddingPhoneNumber.click();

    }

    public void validateNavigationtoAddCompanyPhoneNumberPage() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (msgContinueAddingPhoneNumber.isDisplayed()) {
            System.out.println("User Navigated back to Add Company Phone Number page");
        }
    }

    public void addCompanyPhoneNumber() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        txtAddPhoneNumber.sendKeys("7207107877");
    }

    public void clickAddCompanyPhoneNumber() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        btnAddCompanyPhoneNumber.click();
    }

    public void validateSuccessMessage() {
        if (msgSuccess.isDisplayed()) {
            System.out.println("Phone number added successfully");
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clickonEditCompanyPhone() {
        lnkEditCompanyPhone.click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void editandCancelPhone() {


        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        txtEditCompanyPhone.click();
        txtEditCompanyPhone.clear();
        txtEditCompanyPhone.sendKeys("7207107245");
        btnCancel.click();


    }

    public void clickDontleaveonCancelPhonedialog() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        btnNoLeave.click();
    }

    public void validateNavigationtoEditCompanyPhoneNumberPage() {
        if (HdrEditCompanyPhone.isDisplayed()) {
            System.out.println("Edit Company phone number pag displayed successfully");
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void EnterValidPhoneNumber() {
        txtEditCompanyPhone.clear();
        txtEditCompanyPhone.sendKeys("7207107866");

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void clickOnUpdatePhonenumberButton() {
        btnUpdatePhoneNumber.click();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void validateUpdatePhoneNumberSuccessMsg() {
        if (updateSuccessMsg.isDisplayed()) {
            System.out.println("Phonenumber updated successfully");
        } else {
            System.out.println("Phonenumber NOT updated successfully");
        }
    }
}
