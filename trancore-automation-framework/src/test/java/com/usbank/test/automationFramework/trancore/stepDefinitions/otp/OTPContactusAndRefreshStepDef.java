package com.usbank.test.automationFramework.trancore.stepDefinitions.otp;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import org.springframework.beans.factory.annotation.Autowired;

public class OTPContactusAndRefreshStepDef {
    @Autowired
    StepDefinitionManager stepDefinitionManager;

    @Then("the customer should be directed to Contact us for help screen")
    public void the_customer_should_be_directed_to_Contact_us_for_help_screen() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getContactUsForHelpPage().VerifyHeaderName();
    }

    @Then("ContactUsForHelp should display the service down message")
    public void contactusforhelp_should_display_the_service_down_message() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getContactUsForHelpPage().verifytextServiceCall();
    }

    @Then("customer should refresh the page")
    public void customer_should_refresh_the_page() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }


    @Then("Profile page should be displayed")
    public void profile_page_should_be_displayed() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getProfileLandingPage().verifyHeaderName();
    }

    @And("the customer clicks on Return link at Contact us Page")
    public void theCustomerClicksOnReturnLinkAtContactUsPage() {
        stepDefinitionManager.getPageObjectManager().getContactUsForHelpPage().clickReturnTo();
    }

    @And("the customer should verify the text at Contact us Page")
    public void theCustomerShouldVerifyTheTextAtContactUsPage() {
        stepDefinitionManager.getPageObjectManager().getContactUsForHelpPage().verifytextOTPMax();
    }
    @When("the customer should be directed to Internal Error Page")
	 public void the_customer_should_be_directed_to_Internal_Error_Page() {
	     stepDefinitionManager.getPageObjectManager().getInternalErrorPage().verifyHeaderName();
	 }

}
