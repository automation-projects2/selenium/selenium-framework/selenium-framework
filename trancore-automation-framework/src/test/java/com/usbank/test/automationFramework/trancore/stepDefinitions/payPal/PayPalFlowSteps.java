package com.usbank.test.automationFramework.trancore.stepDefinitions.payPal;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.Then;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

@RunWith(Cucumber.class)

public class PayPalFlowSteps {
    @Autowired
    StepDefinitionManager stepDefinitionManager;
    boolean elementPresent, payPalFlag;

    @Then("verify Add to paypal link present or not")
    public void verify_Add_to_paypal_link_present_or_not() throws IOException {
        // Write code here that turns the phrase above into concrete actions
        elementPresent = stepDefinitionManager.getPageObjectManager().getServicesLandingPage().verifyAddToPayPalLink();
        stepDefinitionManager.getPageObjectManager().getCaptureScreenShots().takeScreenshot(" servicePage");

    }

    @Then("click on Add to PayPal link if link  is present")
    public void clickOnAddToPayPalLinkIfLinkIsPresent() throws IOException {
        // Write code here that turns the phrase above into concrete actions
        elementPresent = stepDefinitionManager.getPageObjectManager().getServicesLandingPage().verifyAddToPayPalLink();
    }


    @Then("^the user navigated to the Agree and Continue page")
    public void the_user_navigated_to_the_Agree_and_Continue_page() throws InterruptedException, IOException {
        if (elementPresent) {
            stepDefinitionManager.getPageObjectManager().getAgreeAndContinuePage().verifyAgreeAndContinuePageDisplayed();
            stepDefinitionManager.getPageObjectManager().getAgreeAndContinuePage().verifyAgreeAndContinueButton();
            stepDefinitionManager.getPageObjectManager().getCaptureScreenShots().takeScreenshot(" Agree and Continue Page");
        }
    }

    @Then("^the user clicks on Agree and Continue button$")
    public void the_user_clicks_on_Agree_and_Continue_button() throws InterruptedException {
        if (elementPresent)
            stepDefinitionManager.getPageObjectManager().getAgreeAndContinuePage().agreeAndContinueButtonClick();
    }


    @Then("the user selects a (.+) and clicks on send a new code button")
    public void the_user_selects_a_and_clicks_on_send_a_new_code_button(String contact) {
        // Write code here that turns the phrase above into concrete actions
        if (elementPresent) {
            stepDefinitionManager.getPageObjectManager().getContactMethodPage().selectContactMethod(contact);
            stepDefinitionManager.getPageObjectManager().getContactMethodPage().ClickOnSendAOneTimeCodeButton();
        }

    }

    @Then("add to paypal link is not present for user with (.+)$")
    public void add_to_paypal_link_is_not_present_for_user_with_inactive(String status) throws IOException {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getServicesLandingPage().verifyAddToPayPalLink();
       // stepDefinitionManager.getPageObjectManager().getCaptureScreenShots().takeScreenshot(" servicePage");
    }


    @Then("the user has navigated to the services page")
    public void the_User_Has_Navigated_To_The_ServicesPage() throws IOException {
        stepDefinitionManager.getPageObjectManager().getWebNavigationPage().clickServicesLinkadded();
     //   stepDefinitionManager.getPageObjectManager().getCaptureScreenShots().takeScreenshot(" servicePage");

    }


    @Then("click on send a new code button")
    public void clickOnSendANewCodeButton() {
        stepDefinitionManager.getPageObjectManager().getContactMethodPage().ClickOnSendAOneTimeCodeButton();
    }

    @Then("the user navigated to the contact page and  select (.+)$")
    public void theUserNavigatedToTheContactPageAndSelectContact() throws IOException {
        if (elementPresent) {
            stepDefinitionManager.getPageObjectManager().getContactMethodPage().verifyContactMethodPageHeading();
        }
    }

    @Then("the user navigated to the Enter OTP page")
    public void the_user_navigated_to_the_Enter_OTP_page() throws IOException {
        // Write code here that turns the phrase above into concrete actions
        if (elementPresent) {
            stepDefinitionManager.getPageObjectManager().getContactMethodPage().verifyEnterOTPpage();
        }
    }

    @Then("the user enters the (.+) which is send to the contact")
    public void the_user_enters_the_which_is_send_to_the(String otp) throws IOException {
        // Write code here that turns the phrase above into concrete actions
        if (elementPresent) {
            payPalFlag = stepDefinitionManager.getPageObjectManager().getContactMethodPage().enterOTPAndVerifyMessage(otp);
        }
    }


    @Then("the user navigates to payPal flow with (.+) and  (.+)$")
    public void theUserNavigatesToPayPalFlowWithContactAndPayPalPassword(String userName, String Password) {
        if (payPalFlag) {
            stepDefinitionManager.getPageObjectManager().getContactMethodPage().payPalFlowCompletion(userName, Password);

        }
    }

    @Then("verify paypal link is not present for ineligible user")
    public void verifyPaypalLinkIsNotPresentForIneligibleUser() throws IOException {
        stepDefinitionManager.getPageObjectManager().getServicesLandingPage().verifyPayPalLinkNotPresent();
    }

    @Then("the user navigated to the contact page")
    public void theUserNavigatedToTheContactPage() throws IOException {
        if (elementPresent) {
            stepDefinitionManager.getPageObjectManager().getContactMethodPage().verifyContactMethodPageHeading();
        }
    }


}