package com.usbank.test.automationFramework.trancore.stepDefinitions.otp;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class CancelOTPProfileFraudStepDef {
    @Autowired
    StepDefinitionManager stepDefinitionManager;

    @When("the customer clicks on Cancel at OTP Page")
    public void the_customer_clicks_on_Cancel_at_OTP_Page() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getOTPScreenPage().ClickCancel();
    }

    @Then("the customer should return to Profile homepage")
    public void the_customer_should_return_to_Profile_homepage() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getProfileLandingPage().verifyHeaderName();

    }


}
