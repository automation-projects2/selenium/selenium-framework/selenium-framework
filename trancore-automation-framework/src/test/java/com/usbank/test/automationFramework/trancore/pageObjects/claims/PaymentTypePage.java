package com.usbank.test.automationFramework.trancore.pageObjects.claims;

import com.usbank.test.automationFramework.trancore.pageObjects.waitForPageToLoad.WaitForPageToLoad;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class PaymentTypePage<T extends WebDriver> {
    @FindBy(how = How.XPATH, using = "//h1[@id='servicingAppHeader']")
    private WebElement lblPaymentTypeHeader;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'What other form of payment did you use for this tr')]")
    private WebElement vrbWhatOtherFormOfPayment;

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Continue')]")
    private WebElement btnContinueOnPaymentTypePage;

    private T driver;

    WaitForPageToLoad callPageLoadMethod;

    private static final Logger log = LogManager.getLogger(PaymentTypePage.class.getName());

    public PaymentTypePage(T inDriver) {
        driver = inDriver;
        callPageLoadMethod = new WaitForPageToLoad(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getLblPaymentTypeHeader() {
        return lblPaymentTypeHeader;
    }

    public void whatOtherFormOfPaymentDidYouUseForThisTransaction() {
        log.info(vrbWhatOtherFormOfPayment.getText());
    }

    /*
     * Each of these methods goes through a different flow/outcome but, certain forms of
     * payments are within the same flow just a change of the STRING variable is
     * needed to enter a new flow
     *
     * for example:
     * you need to change the variable formOfPayment Check simply uncomment the next line
     */
    public void selectAReasonForPaymentYouUsedForThisTransactionCashOrCheck() {

        String formOfPayment = "Cash";
        //String formOfPayment = "Check";

        List<WebElement> listOfReasons = driver.findElements(By.name("radio510"));
        List<WebElement> listOfText = driver.findElements(By.className("svc-radioButton"));

        int listSize = listOfReasons.size();

        for (int i = 0; i < listSize; i++) {

            String reasonText = listOfText.get(i).getText();

            log.info("Form Of Payment " + i + ": " + reasonText);
            if (reasonText.equalsIgnoreCase(formOfPayment)) {
                listOfReasons.get(i).click();
            }
        }
    }

    public void selectAReasonForPaymentYouUsedForThisTransactionATMDebitCardOrOtherCreditCard() {

        String formOfPayment = "ATM/Debit card";
        //String formOfPayment = "Other credit card, prepaid card, or gift card";

        List<WebElement> listOfReasons = driver.findElements(By.name("radio510"));
        List<WebElement> listOfText = driver.findElements(By.className("svc-radioButton"));

        int listSize = listOfReasons.size();

        for (int i = 0; i < listSize; i++) {

            String reasonText = listOfText.get(i).getText();

            log.info("Form Of Payment " + i + ": " + reasonText);
            if (reasonText.equalsIgnoreCase(formOfPayment)) {
                listOfReasons.get(i).click();
            }
        }
    }

    public void selectAReasonForPaymentYouUsedForThisTransactionOther() {

        String formOfPayment = "Other";

        List<WebElement> listOfReasons = driver.findElements(By.name("radio510"));
        List<WebElement> listOfText = driver.findElements(By.className("svc-radioButton"));

        int listSize = listOfReasons.size();

        for (int i = 0; i < listSize; i++) {

            String reasonText = listOfText.get(i).getText();

            log.info("Form Of Payment " + i + ": " + reasonText);
            if (reasonText.equalsIgnoreCase(formOfPayment)) {
                listOfReasons.get(i).click();
            }
        }
    }

    public void clickOnContinueBtnOnPaymentTypePage() {
        btnContinueOnPaymentTypePage.click();
    }

    public void paymentTypePagePBOMFlowOne() throws InterruptedException {
        log.info("**************** Payment Type Page Reasoning One ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblPaymentTypeHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblPaymentTypeHeader, 3);
        whatOtherFormOfPaymentDidYouUseForThisTransaction();
        selectAReasonForPaymentYouUsedForThisTransactionCashOrCheck();
        clickOnContinueBtnOnPaymentTypePage();
    }

    public void paymentTypePagePBOMFlowTwo() throws InterruptedException {
        log.info("**************** Payment Type Page Reasoning Two ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblPaymentTypeHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblPaymentTypeHeader, 3);
        whatOtherFormOfPaymentDidYouUseForThisTransaction();
        selectAReasonForPaymentYouUsedForThisTransactionATMDebitCardOrOtherCreditCard();
        clickOnContinueBtnOnPaymentTypePage();
    }

    public void paymentTypePagePBOMFlowThree() throws InterruptedException {
        log.info("**************** Payment Type Page Reasoning Three ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblPaymentTypeHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblPaymentTypeHeader, 3);
        whatOtherFormOfPaymentDidYouUseForThisTransaction();
        selectAReasonForPaymentYouUsedForThisTransactionOther();
        clickOnContinueBtnOnPaymentTypePage();
    }

}
