package com.usbank.test.automationFramework.trancore.stepDefinitions.otp;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class ProfileOTPStepDef {
	@Autowired
	StepDefinitionManager stepDefinitionManager;

	@And("customer click on Profilepage")
	public void customer_click_on_Profilepage() {
		// Write code here that turns the phrase above into concrete actions
		stepDefinitionManager.getPageObjectManager().getWebNavigationPage().clickProfileLink();

	}

	@And("the customer clicks the contact information link")
	public void he_customer_clicks_the_contact_information_link() {
		// Write code here that turns the phrase above into concrete actions
		stepDefinitionManager.getPageObjectManager().getProfileLandingPage().clickContactInformationPage();

	}

	@Then("the customer should be directed to the Contact Information")
	public void the_customer_should_be_directed_to_the_Contact_Information() {
		// Write code here that turns the phrase above into concrete actions
		stepDefinitionManager.getPageObjectManager().getContactinfoPage().verifyHeaderName();
	}

	@Then("the customer should be directed to the OTP Screen")
	public void the_customer_should_be_directed_to_the_OTP_Screen() {
		// Write code here that turns the phrase above into concrete actions
		stepDefinitionManager.getPageObjectManager().getOTPScreenPage().verifyHeaderName();
	}

	@When("^the customer enters (.+)passcode at Enter OTP Page$")
	public void the_customer_enters_passcode_at_Enter_OTP_Page(String passcode) {

		// Write code here that turns the phrase above into concrete actions
		stepDefinitionManager.getPageObjectManager().getEnterOTPPage().SetEnterOTP(passcode);

	}

	@When("the customer clicks on Continue at Enter OTP Page")
	public void the_customer_clicks_on_Continue() {
		// Write code here that turns the phrase above into concrete actions
		stepDefinitionManager.getPageObjectManager().getEnterOTPPage().ClickContinue();
	}


	@And("the customer should be directed to Update Mailing Address Page")
	public void theCustomerShouldBeDirectedToUpdateMailingAddressPage() {
		stepDefinitionManager.getPageObjectManager().getUpdateMailingAddress().verifyHeaderName();
	}

	@And("the customer clicks on Update Mailing Address at Contact Info Page")
	public void theCustomerClicksOnUpdateMailingAddressAtContactInfoPage() {
		stepDefinitionManager.getPageObjectManager().getContactinfoPage().clickUpdateMailingAddress();
	}

	@And("the customer clicks on Add Email Address at Contact Info Page")
	public void theCustomerClicksOnAddEmailAddressAtContactInfoPage() {
		stepDefinitionManager.getPageObjectManager().getContactinfoPage().clickAddEmailAddress();
	}

	@And("the customer should be directed to Add Email Address Page")
	public void theCustomerShouldBeDirectedToAddEmailAddressPage() {
		stepDefinitionManager.getPageObjectManager().getAddEmailAddressPage().verifyHeaderName();
	}
}
