package com.usbank.test.automationFramework.trancore.stepDefinitions.lostOrStolen;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class LostOrStolenSteps {


    @Autowired
    private StepDefinitionManager stepDefinitionManager;

    @When("the user clicks on the Services tab and is able to click the Report Card Lost Or Stolen Link")
    public void the_user_clicks_on_the_services_tab_and_is_able_to_see_the_Lost_Or_Stolen_link() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getLostOrStolenPage().clickOnServicesPage();
        stepDefinitionManager.getPageObjectManager().getLostOrStolenPage().clickOnReportCardLostOrStolen();
    }

    @And("the user clicks on the Continue Button to proceed into Report Card Lost or Stolen flow")
    public void theUserClicksOnTheContinueButtonToProceedIntoReportCardLostOrStolenFlow() {
        stepDefinitionManager.getPageObjectManager().getLostOrStolenPage().clickContinueQuickQuestionPage();
    }

    @And("the user clicks on the Yes I Recognize All These Purchases Radio Button")
    public void theUserClicksOnTheYesIRecognizeAllThesePurchasesRadioButton() {
        stepDefinitionManager.getPageObjectManager().getReviewRecentTransactionPage().selectRadioButtonYes();
    }

    @And("the user clicks the Continue Button on Review Recent Transactions Page")
    public void theUserClicksTheContinueButtonOnReviewRecentTransactions() {
        stepDefinitionManager.getPageObjectManager().getLostOrStolenPage().clickOnContinueButtonReviewTransactionsPage();
    }


    @And("^the user enters in the missing date (.+) for the Lost Or Stolen on the Quick Question Page$")
    public void theUserEntersInTheMissingDateDateForTheLostOrStolenOnTheQuickQuestionPage(String missingDate) {
        stepDefinitionManager.getPageObjectManager().getLostOrStolenPage().enterMissingDate(missingDate);
    }


    @And("the user is on the Quick Question Page and clicks on the Continue Button")
    public void theUserIsOnTheQuickQuestionPageAndClicksOnTheContinueButton() {
        stepDefinitionManager.getPageObjectManager().getLostOrStolenPage().clickContinueQuickQuestionPage();
    }

    @And("the user is on the Card Delivery Page and selects Standard Delivery with the Continue Button")
    public void theUserIsOnTheCardDeliveryPageAndSelectsStandardDeliveryWithTheContinueButton() {
        stepDefinitionManager.getPageObjectManager().getLostOrStolenPage().clickOnStandDelivery();
       // stepDefinitionManager.getPageObjectManager().getLostOrStolenPage().clickOnContinueButtonOnCardDeliveryPage();
    }

    @Then("the user is on the Review And Submit Your Request page and clicks on Report Card Lost Or Stolen button")
    public void theUserIsOnTheReviewAndSubmitYourRequestPageAndClicksOnReportCardLostOrStolenButton() {
        stepDefinitionManager.getPageObjectManager().getLostOrStolenPage().captureReviewAndSubmitRequestDetails();
        stepDefinitionManager.getPageObjectManager().getLostOrStolenPage().clickOnReportCardLostOrStolenButton();
    }

    @Then("the user clicks on Return To Services Button")
    public void theUserClicksOnReturnToServicesButton() {
        stepDefinitionManager.getPageObjectManager().getLostOrStolenPage().clickOnReturnToServicesButton();
    }

    @Then("verify the page title for lost and stolen landing page")
    public void verifyThePageTitleForLostAndStolenLandingPage() {
        stepDefinitionManager.getPageObjectManager().getssoDeeplinkLoginPage().verifyLostAndStolenPageTitle();
    }

    @Then("click on OTP button on One time passcode page")
    public void clickOnOTPButtonOnOneTimePasscodePage() {
        stepDefinitionManager.getPageObjectManager().getLostOrStolenPage().clickSendOtpPasscodeButton();
    }

    @And("Enter OTP and click on continue button")
    public void enterOTPAndClickOnContinueButton() {
        stepDefinitionManager.getPageObjectManager().getLostOrStolenPage().clickOnContinueOtpPasscodeButton();
    }
    @And("the customer select standard delivery and clicks on Continue button")
    public void theCustomerSelectStandardDeliveryAndClicksOnContinueButton() {
        stepDefinitionManager.getPageObjectManager().getLostOrStolenPage().clickOnStandDelivery();
        //stepDefinitionManager.getPageObjectManager().getCardDeliveryPage().continueButtonClick();

    }

    @And("the customer should be directed to Review and Submit Your Request page")
    public void theCustomerShouldBeDirectedToReviewAndSubmitYourRequestPage() {
        stepDefinitionManager.getPageObjectManager().getReviewandSubmitCardReplacementOrderpage().verifyHeaderName();
    }

    @And("the customer clicks on ReportCardLostStolen and directs to All set page")
    public void theCustomerClicksOnReportCardLostStolenAndDirectsToAllSetPage() {
        stepDefinitionManager.getPageObjectManager().getReviewandSubmitCardReplacementOrderpage().clickSubmitOrder();
        stepDefinitionManager.getPageObjectManager().getReviewandSubmitCardReplacementOrderpage().verifyConfirmationHeaderName();
    }

    @And("the customer clicks on return to service link and directs to Service Tab")
    public void theCustomerClicksOnReturnToServiceLinkAndDirectsToServiceTab() {

    }


    @And("the user verifies the radio button error text at Recent Review Transaction Page")
    public void theUserVerifiesTheRadioButtonErrorTextAtRecentReviewTransactionPage() {
        stepDefinitionManager.getPageObjectManager().getReviewRecentTransactionPage().verifyErrortext();
    }
    @Then("customer click on CardActivation link")
    public void customer_click_on_CardActivation_link() {
        stepDefinitionManager.getPageObjectManager().getLostOrStolenPage().clickOnCardActivationLink();
    }
    @Then("the user navigated to the  card activation enrollment page ssn (.+) and cvv (.+)")
    public void the_user_navigated_to_the_card_activation_enrollment_page_ssn_and_cvv(String ssn, String cvv) {
        stepDefinitionManager.getPageObjectManager().getLostOrStolenPage().enrollmentPageofCardActivation(ssn,cvv);
    }
    @Then("the user verifies card activation (.+) text message")
    public void theUserVerifiesCardActivationSuccessmessageTextMessage(String successmessage) {
    stepDefinitionManager.getPageObjectManager().getLostOrStolenPage().verifiesCardSuccessMsg(successmessage);

    }

    @And("the customer clicks on continue in Review recent transactions page")
    public void theCustomerClicksOnContinueInReviewRecentTransactionsPage() {
        stepDefinitionManager.getPageObjectManager().getReportCardLostOrStolenPage().ClickContinue();
    }

    @Then("customer verifies card activation status")
    public void customerVerifiesCardActivationStatus() {
        stepDefinitionManager.getPageObjectManager().getLostOrStolenPage().verifyCardActivationStatus();
    }

    @And("the customer clicks Report Card Lost or Stolen link in service Tab with (.+) and (.+) and (.+)")
    public void theCustomerClicksReportCardLostOrStolenLinkInServiceTabWithContactAndPasscode(String contact, String passcode, String missingDate) throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getLostOrStolenPage().reportLostAndStolenCard(contact,passcode,missingDate);
    }

    @And("the user is on the Quick Question Page error text by clicking on Continue button.")
    public void theUserIsOnTheQuickQuestionPageErrorTextByClickingOnContinueButton() {
        stepDefinitionManager.getPageObjectManager().getLostOrStolenPage().clickContinueQuickQuestionPage();
        stepDefinitionManager.getPageObjectManager().getLostOrStolenPage().verifyErrortextQuickQuestion();
    }
}
