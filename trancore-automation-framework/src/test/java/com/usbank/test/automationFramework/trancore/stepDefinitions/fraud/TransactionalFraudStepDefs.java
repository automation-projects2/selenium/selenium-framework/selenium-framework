package com.usbank.test.automationFramework.trancore.stepDefinitions.fraud;

import com.usbank.test.automationFramework.trancore.pageObjects.fraud.*;
import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TransactionalFraudStepDefs {

    @Autowired
    private StepDefinitionManager stepDefinitionManager;

    @And("the user navigate through Dispute suspect type page")
    public void theUserNavigateThroughDisputeSuspectTypePage() throws Throwable {
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getDisputePurchaseTypePage().navigateThroughDisputePurchaseTypePage();
    }

    @And("the user navigates through Fraud App Check page")
    public void theUserNavigatesThroughFraudAppCheckPage() throws Throwable {
        Thread.sleep(1000);
        //stepDefinitionManager.getPageObjectManager().getReviewRecentTransactionPage().clickPopupContinue();
        stepDefinitionManager.getPageObjectManager().getFraudAppChkPage().navigateThroughFraudAppChkPage();
    }

    @And("the user navigates through Check Profile Info page by selecting No radio button")
    public void theUserNavigatesThroughCheckProfileInfoPageBySelectingNoRadioButton() throws Throwable{
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getChkProfileInfoPage().clickNoradioBtn();
    }

    @Then("the user is on Wrap up Fraud Page and returns to origination")
    public void theUserIsOnWrapUpFraudPageAndReturnsToOrigination() throws Throwable{
        Thread.sleep(1000);
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getWrapUpFraudPage().getLblContactUs().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getWrapUpFraudPage().clickReturnToOriginationLink();

    }

    @And("the user navigates through Fraud App Check page by selecting No radio button")
    public void theUserNavigatesThroughFraudAppCheckPageBySelectingNoRadioButton() throws Throwable{
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getFraudAppChkPage().selectNoRadioButton();
    }

    @When("the user is on Unauthorized Account Activity Fraud App check page and click cancel")
    public void theUserIsOnUnauthorizedAccountActivityFraudAppCheckPageAndClickCancel() throws Throwable{
        Thread.sleep(1000);
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getUnAuthAccActFraudAppChkPage().getLblDeactivateCard().isDisplayed());

    }

    @Then("the user gets Cancel Fraud Modal popup")
    public void theUserGetsCancelFraudModalPopup() throws Throwable{
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getUnAuthAccActFraudAppChkPage().clickCancelBtn();
    }

    @When("the user is on Check Profile Info page and click on continue")
    public void theUserIsOnCheckProfileInfoPageAndClickonContinue() throws Throwable {
        Thread.sleep(1000);
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getChkProfileInfoPage().getLblCheckProfInfo().isDisplayed());
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getChkProfileInfoPage().getLblContactInfo().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getChkProfileInfoPage().navigateToCheckProfileInfoPage();
        stepDefinitionManager.getPageObjectManager().getChkProfileInfoPage().clickContinue();
    }

    @Then("Check Profile Info page should be displayed properly")
    public void checkProfileInfoPageShouldBeDisplayedProperly() throws Throwable {
        Thread.sleep(1000);
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getChkProfileInfoPage().getLblCheckProfInfo().isDisplayed());
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getChkProfileInfoPage().getLblContactInfo().isDisplayed());
    }

    @When("the user is on Fraud Other Transactions page")
    public void theUserIsOnFraudOtherTransactionsPage() throws Throwable {
        Thread.sleep(1000);
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getOtherTransactionsPage().getLblAccActivity().isDisplayed());
            }

    @Then("Fraud Other Transactions page should be displayed properly")
    public void fraudOtherTransactionsPageShouldBeDisplayedProperly() throws Throwable {
        Thread.sleep(1000);
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getOtherTransactionsPage().getLblAccActivity().isDisplayed());
    }

    @When("the user selects first radio button and click continue on Fraud Other Transactions page")
    public void theUserSelectsFirstRadioButtonAndClickContinueOnFraudOtherTransactionsPage() throws Throwable {
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getOtherTransactionsPage().clickFirstRadioButton();
        stepDefinitionManager.getPageObjectManager().getOtherTransactionsPage().clickContinue();
    }

    @Then("Card Issuance page should be displayed properly")
    public void cardIssuancePageShouldBeDisplayedProperly() throws Throwable {
        Thread.sleep(1000);
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getCardIssuancePage().getLblCardDeliveryOptions().isDisplayed());
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getCardIssuancePage().getLblDeliveryAddress().isDisplayed());
    }

    @When("the user selects third radio button and click continue on Fraud Other Transactions page")
    public void theUserSelectsThirdRadioButtonAndClickContinueOnFraudOtherTransactionsPage() throws Throwable {
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getOtherTransactionsPage().clickThirdRadioButton();
        stepDefinitionManager.getPageObjectManager().getOtherTransactionsPage().clickContinue();
    }

    @Then("Review Transactions page should be displayed properly")
    public void reviewTransactionsPageShouldBeDisplayedProperly() throws Throwable {
        Thread.sleep(1000);
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getReviewTransactionsPage().getReviewOtherTransactionsLabel().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getReviewTransactionsPage().clickYesRadioBtn();
    }

    @Then("Review Transactions page should be displayed properly and user selects No radio button")
    public void reviewTransactionsPageShouldBeDisplayedProperlyAndUserSelectsNoRadioButton() throws Throwable{
        Thread.sleep(1000);
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getReviewTransactionsPage().getReviewOtherTransactionsLabel().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getReviewTransactionsPage().clickNoRadioBtn();
    }

    @When("^the user navigates through Card Issuance page$")
    public void theUserNavigatesThroughCardIssuancePage() throws Throwable {
        stepDefinitionManager.getPageObjectManager().getCardIssuancePage().waitForCardIssuancePageToLoad();
        stepDefinitionManager.getPageObjectManager().getCardIssuancePage().clickContinue();
    }

    @And("the user navigates through Check Profile Info page")
    public void theUserNavigatesThroughCheckProfileInfoPage() throws Throwable  {
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getChkProfileInfoPage().navigateToCheckProfileInfoPage();
        stepDefinitionManager.getPageObjectManager().getChkProfileInfoPage().clickContinue();
    }

    @When("the user navigates through Fraud Other Transactions page by Yes radio button")
    public void theUserNavigatesThroughFraudOtherTransactionsPageByYesRadioButton() throws Throwable{
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getOtherTransactionsPage().clickSecondradioButton();
        stepDefinitionManager.getPageObjectManager().getOtherTransactionsPage().clickContinue();
    }

    @And("the user navigates through Fraud Other Transactions page")
    public void theUserNavigatesThroughFraudOtherTransactionsPage() throws Throwable {
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getOtherTransactionsPage().clickFirstRadioButton();
        stepDefinitionManager.getPageObjectManager().getOtherTransactionsPage().clickContinue();
    }


    @And("the user navigates through Fraud Discovery page through Not Received")
    public void theUserNavigatesThroughFraudDiscoveryPageThroughNotReceived() throws Throwable {
        stepDefinitionManager.getPageObjectManager().getFraudDiscoveryPage().selectAnyPreviousDate();
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getFraudDiscoveryPage().selectThirdRadiobuttonInFirstQuestionSet();
        stepDefinitionManager.getPageObjectManager().getFraudDiscoveryPage().selectSecondRadiobuttonInSecondQuestionSet();
        stepDefinitionManager.getPageObjectManager().getFraudDiscoveryPage().clickContinue();
    }

    @And("the user navigates through Fraud Discovery page through LostStolen")
    public void theUserNavigatesThroughFraudDicoveryPageThroughLostStolen() throws Throwable{
        stepDefinitionManager.getPageObjectManager().getFraudDiscoveryPage().selectAnyPreviousDate();
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getFraudDiscoveryPage().selectFirstRadioButtonInFirstQuestionSet();
        stepDefinitionManager.getPageObjectManager().getFraudDiscoveryPage().clickContinue();
    }

    @And("the user navigates through Fraud Suspect page")
    public void theUserNavigatesThroughFraudSuspectPage() throws Throwable {
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getFraudSuspectPage().clickNoRadioButton();
        stepDefinitionManager.getPageObjectManager().getFraudSuspectPage().clickContinue();
    }

    @And("the user navigates through Fraud Suspect page by clicking Yes radio button")
    public void theUserNavigatesThroughFraudSuspectPageBYClickingYesRadioButton() throws Throwable {
        Thread.sleep(1000);
        FraudSuspectPage spctPage= stepDefinitionManager.getPageObjectManager().getFraudSuspectPage();
        spctPage.getRdbYes().click();
        spctPage.getTxtAreaPersonName().sendKeys("Neil");
        spctPage.getTxtAreaRelationship().sendKeys("Family Doctor");
        spctPage.getTxtAreaPhoneNo().sendKeys("829-688-4780");
        spctPage.getTxtAreaAddress().sendKeys("10506 Justin Dr, Urbandale, IA 50322");
        spctPage.clickContinue();

    }

    @And("the user navigates through Police Report page by clicking Yes radio button")
    public void theUserNavigatesThroughPoliceReportPageByClickingYesRadioButton() throws Throwable{
        Thread.sleep(1000);
        PoliceReportPage polPage = stepDefinitionManager.getPageObjectManager().getPoliceReportPage();
        polPage.getRdbYes().click();
        polPage.getTxtAreaAgency().sendKeys("Federal Law Enforcement");
        polPage.getTxtAreaCaseNo().sendKeys("IA-6439000");
        polPage.getTxtAreaPoliceContact().sendKeys("Holyoke Police Department");
        polPage.clickContinue();

    }

    @Then("the user enters details in Card use page and click continue")
    public void theUserEntersDetailsInCardUsePageAndClickContinue() throws Throwable{
        Thread.sleep(1000);
        CardUsePage cardPage = stepDefinitionManager.getPageObjectManager().getCardUsePage();
        //to enter the date
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        cal.add(Calendar.DATE, -4);
        System.out.println("Previous date was: "+dateFormat.format(cal.getTime()));
        String newDate = dateFormat.format(cal.getTime());
        cardPage.getDateField().sendKeys(newDate);
        // to enter the detailed description
        cardPage.getTxtArea().sendKeys("Supermarket");
        Thread.sleep(1000);
        cardPage.clickContinue();
    }

    @When("the user enters details in Card in Hand page and click continue")
    public void theUserEntersDetailsInCardInHandPageAndClickContinue() throws Throwable{
        Thread.sleep(1000);
        CardInHandPage inHandPage =stepDefinitionManager.getPageObjectManager().getCardInHandPage();
        //to enter the date
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        cal.add(Calendar.DATE, -4);
        System.out.println("Previous date was: "+dateFormat.format(cal.getTime()));
        String newDate = dateFormat.format(cal.getTime());
        inHandPage.getDateField().sendKeys(newDate);

        //to enter description
        inHandPage.getTxtArea().sendKeys("Toll gate");
        Thread.sleep(1000);

        inHandPage.clickContinue();

    }

    @Then("the user navigates to Travel page by clicking Yes radio Button")
    public void TheUserNavigatesToTravelPageByClickingYesRadioButton() throws Throwable{
        Thread.sleep(1000);
        TravelPage trvlPage = stepDefinitionManager.getPageObjectManager().getTravelPage();
        trvlPage.getRdbYes().click();
        trvlPage.getTxtArea().sendKeys("Traveled from super market to house");

        // to enter trip start date
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        cal.add(Calendar.DATE, -9);
        System.out.println("Previous date was: "+dateFormat.format(cal.getTime()));
        String newDate = dateFormat.format(cal.getTime());
        trvlPage.getTripStartDate().sendKeys(newDate);

        //to enter trip end date
        Calendar cal1 = Calendar.getInstance();
        DateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yyyy");
        cal1.add(Calendar.DATE,-4);
        System.out.println( dateFormat.format(cal1.getTime()));
        String newDate1 = dateFormat.format(cal1.getTime());
        trvlPage.getTripEndDate().sendKeys(newDate1);


        trvlPage.clickContinue();

    }

    @When("the user enters the information in Other Info Page and click continue")
    public void theUserEntersTheInformationInOtherInfoPageAndClickContinue() throws Throwable{
        Thread.sleep(1000);
        OtherInfoPage oInfoPage = stepDefinitionManager.getPageObjectManager().getOtherInfoPage();
        oInfoPage.getTxtArea().sendKeys("There are no more information that I can remember");
        oInfoPage.clickContinue();
    }

    @When("the user navigates through Police Report page to Possess Cards page")
    public void theUserNavigatesThroughPoliceReportPageToPossessCardsPage() throws Throwable {
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getPoliceReportPage().clickNoRadioButton();
        stepDefinitionManager.getPageObjectManager().getPoliceReportPage().clickContinue();
    }

    @And ("the user navigates through Police Report page")
    public void theUserNavigatedthroughPoliceReportPage() throws Throwable {
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getPoliceReportPage().clickNoRadioButton();
        stepDefinitionManager.getPageObjectManager().getPoliceReportPage().clickContinue();
    }

    @Then("Possess cards page is displayed properly")
    public void possessCardsPageIsDisplayedProperly() throws Throwable {
        Thread.sleep(1000);
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPossessCardsPage().getLblPossessCards().isDisplayed());
    }

    @Then("Card Use page should be displayed properly")
    public void cardUsePageShouldBeDisplayedProperly() throws Throwable {
        Thread.sleep(1000);
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getCardUsePage().getLblCardUse().isDisplayed());
    }

    @Then ("Card in Hand page should be displayed properly")
    public void cardInHandPageShouldBeDisplayedProperly() throws Throwable {
        Thread.sleep(1000);
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getCardInHandPage().getLblCardInHand().isDisplayed());
    }
    @When("the user selects Yes radio button and click continue on Possess Cards page")
    public void theUserSelectsYesRadioButtonAndClickContinueOnPossessCardsPage() throws Throwable {
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getPossessCardsPage().clickYesRadioButton();
        stepDefinitionManager.getPageObjectManager().getPossessCardsPage().clickContinue();
    }

    @Then("Card Lost Stolen page should be displayed properly")
    public void cardLostStolenPageShouldBeDisplayedProperly() throws Throwable {
        Thread.sleep(1000);
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getCardLostStolenPage().getLblCardLostStolen().isDisplayed());
    }

    @When("the user selects No radio button and click continue on Possess Cards page")
    public void theUserSelectsNoRadioButtonAndClickContinueOnPossessCardsPage() throws Throwable {
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getPossessCardsPage().clickNoRadioButton();
        stepDefinitionManager.getPageObjectManager().getPossessCardsPage().clickContinue();
    }

    @When("the user selects Not Received radio button and click continue on Card Lost Stolen page")
    public void theUserSelectsNotReceivedRadioButtonAndClickContinueOnCardLostStolenPage() throws Throwable {
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getCardLostStolenPage().clickNotReceivedRadioButton();
        stepDefinitionManager.getPageObjectManager().getCardLostStolenPage().clickContinue();
    }

    @When("the user the user selects No radio button and click continue on Police Report page")
    public void theUserTheUserSelectsNoRadioButtonAndClickContinueOnPoliceReportPage() throws Throwable  {
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getPoliceReportPage().clickNoRadioButton();
        stepDefinitionManager.getPageObjectManager().getPoliceReportPage().clickContinue();
    }

    @Then("Less Than 45 page should be displayed properly")
    public void lessThan45PageShouldBeDisplayedProperly() throws Throwable {
        Thread.sleep(1000);
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getLessThan45Page().getLblLessThan45().isDisplayed());
    }

    @When("the user selects any radio button in first question and No radio button in second question on Less Than 45 page")
    public void theUserSelectsAnyRadioButtonInFirstQuestionAndNoRadioButtonInSecondQuestionOnLessThan45Page()
            throws Throwable {
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getLessThan45Page().clickFirstQuestionNoRadioButton();
        stepDefinitionManager.getPageObjectManager().getLessThan45Page().clickSecondQuestionNoRadioButton();
        stepDefinitionManager.getPageObjectManager().getLessThan45Page().clickContinue();
    }

    @Then("Address Changed page should be displayed properly")
    public void addressChangedPageShouldBeDisplayedProperly() throws Throwable {
        Thread.sleep(1000);
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getAddressChangedPage().getLblAddressChanged().isDisplayed());
    }

    @When("the user selects Yes radio button in first question and Yes radio button in second question on Address Changed page")
    public void theUserSelectsYesRadioButtonInFirstQuestionAndYesRadioButtonInSecondQuestionOnAddressChangedPage()
            throws Throwable {
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getAddressChangedPage().clickFirstQuestionYesRadioButton();
        stepDefinitionManager.getPageObjectManager().getAddressChangedPage().clickSecondQuestionYesRadioButton();
        stepDefinitionManager.getPageObjectManager().getAddressChangedPage().clickContinue();
    }

    @When("the user selects checkbox and click continue on Card Use page")
    public void theUserSelectsCheckboxAndClickContinueOnCardUsePage() throws Throwable {
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getCardUsePage().clickDonotRememberCheckbox();
        stepDefinitionManager.getPageObjectManager().getCardUsePage().clickContinue();
    }

    @When("the user selects checkbox and click continue on Card In Hand page")
    public void theUserSelectsCheckboxAndClickContinueOnCardInHandPage() throws Throwable {
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getCardInHandPage().clickDoNotRememberCheckbox();
        stepDefinitionManager.getPageObjectManager().getCardInHandPage().clickContinue();
    }
    @Then("Travel page should be displayed properly")
    public void travelPageShouldBeDisplayedProperly() throws Throwable {
        Thread.sleep(1000);
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getTravelPage().getLblTravel().isDisplayed());
    }

    @When("the user selects No radio button and click continue on Travel page")
    public void theUserSelectsNoRadioButtonAndClickContinueOnTravelPage() throws Throwable {
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getTravelPage().clickNoRadioButton();
        stepDefinitionManager.getPageObjectManager().getTravelPage().clickContinue();
    }

    @Then ("Mobile Wallet transaction Page should be displayed properly")
    public void mobileWalletTransactionPageShouldBeDisplayedProperly() throws Throwable {
        Thread.sleep(1000);
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getMobileWalletPage().getLblMobileWallet().isDisplayed());
    }

    @When("the user selects Yes or No radio button and click Continue on Mobile Wallet Page")
    public void theUserSelectsYesOrNoRadioButtonAndClickContinueOnMobileWalletPage() throws Throwable {
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getMobileWalletPage().clickFirstQuestionYesRadioButton();
        stepDefinitionManager.getPageObjectManager().getMobileWalletPage().clickSecondQuestionNoRadioButton();
        stepDefinitionManager.getPageObjectManager().getMobileWalletPage().clickContinue();
    }

    @Then("Other Info page should be displayed properly")
    public void otherInfoPageShouldBeDisplayedProperly() throws Throwable {
        Thread.sleep(3000);
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getOtherInfoPage().getLblOtherInfo().isDisplayed());
    }

    @When("the user click continue on Other Info page without information")
    public void theUserClickContinueOnOtherInfoPageWithoutInformation() throws Throwable {
        Thread.sleep(1000);
        stepDefinitionManager.getPageObjectManager().getOtherInfoPage().clickContinue();
    }

    @Then("Mailing Info page should be displayed properly")
    public void mailingInfoPageShouldBeDisplayedProperly() throws Throwable {
        Thread.sleep(3000);
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getMailingInfoPage().getLblMailingInfo().isDisplayed());
    }

    @When("an Alternate Address is selected and hit Continue")
    public void alternateAddressSelectedAndHitContinue(){
        MailingInfoPage page = stepDefinitionManager.getPageObjectManager().getMailingInfoPage();
        page.getRdbAlternateAddress().click();

        Select addresseeSelect = new Select(page.getSddAddresseeName());
        addresseeSelect.selectByIndex(1);

        page.getTxtMailingAddress().sendKeys("1 Meridian Crossing");
        new Select(page.getSddMailingState()).selectByVisibleText("Minnesota");
        page.getTxtMailingCity().sendKeys("Richfield");
        page.getTxtMailingZipFive().sendKeys("55423");
        page.getTxtPhoneNumber().sendKeys("701-701-7010");

        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        cal.add(Calendar.DATE, 160);
        System.out.println( dateFormat.format(cal.getTime()));

        String newDate = dateFormat.format(cal.getTime());
        page.getTxtValidUntil().sendKeys(newDate);

        page.clickContinue();

    }

    @Then("Contact Validation page displays")
    public void validateContactValidationPage(){
        ContactValidationPage page = stepDefinitionManager.getPageObjectManager().getContactValidationPage();
        page.waitForMailingInfoPageToLoad();


    }

    @When("an usps address is selected and click continue")
    public void radioOptionIsDefaultedToUspsAddressAndClickContinue() throws Throwable {
        Thread.sleep(3000);
        stepDefinitionManager.getPageObjectManager().getContactValidationPage().getRdbUspsAddress().click();
        stepDefinitionManager.getPageObjectManager().getContactValidationPage().getBtnContinue().click();
    }
    @When("radio option is defaulted to Mail update to address on file and click continue")
    public void radioOptionIsDefaultedToMailUpdateToAddressOnFileAndClickContinue() throws Throwable {
        Thread.sleep(3000);
        stepDefinitionManager.getPageObjectManager().getMailingInfoPage().clickContinue();
    }

    @When("radio option is defaulted to Mail update to address on file and phoneNumber is entered and click continue")
    public void radioOptionIsDefaultedToMailUpdateToAddressOnFileAndEnterPhoneNumberAndClickContinue() throws Throwable {
        Thread.sleep(3000);
        MailingInfoPage page = stepDefinitionManager.getPageObjectManager().getMailingInfoPage();
        page.getTxtDefaultPhoneNbr().sendKeys("701-701-7010");
        page.clickContinue();
    }
    @Then("Fraud Review page should be displayed properly")
    public void fraudReviewPageShouldBeDisplayedProperly() throws Throwable{
        Thread.sleep(3000);
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getReviewPage().getLblReviewPage().isDisplayed());
    }

    @When("the user click continue on Review Page")
    public void fraudReviewPageContinueButtonClick() throws Throwable{
        Thread.sleep(3000);
        stepDefinitionManager.getPageObjectManager().getReviewPage().clickContinue();
    }

    @Then("Fraud Confirmation page should display")
    public void fraudConfirmationPageDisplay() throws Throwable{
        stepDefinitionManager.getPageObjectManager().getFraudConfirmationPage().waitForConfirmationPageToLoad();
    }

    @When("the user clicks on return to transaction details link")
    public void fraudConfirmationPageReturnToTransactionDetailsLinkClick() throws Throwable{
        Thread.sleep(3000);
        stepDefinitionManager.getPageObjectManager().getFraudConfirmationPage().getTransactionDetailsLink().click();
    }

    @Then("myAccount page should display")
    public void myAccountPageDisplay() throws Throwable{
        stepDefinitionManager.getPageObjectManager().getMyAccountPage().waitForMyAccountPageToLoad();
    }
}