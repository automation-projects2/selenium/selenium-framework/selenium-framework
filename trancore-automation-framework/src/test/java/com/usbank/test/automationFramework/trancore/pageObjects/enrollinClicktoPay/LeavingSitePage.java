package com.usbank.test.automationFramework.trancore.pageObjects.enrollinClicktoPay;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LeavingSitePage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(LeavingSitePage.class);
    private T driver;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Leaving Site')]")
    WebElement leavingsitepage;

    @FindBy(how = How.NAME, using = "Next")
    WebElement nextButton;

    @FindBy(how = How.XPATH, using = "//button[@name='Next']")
    WebElement btnNext;

    public LeavingSitePage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public void clickOnNextBtn() throws InterruptedException {
        Assert.assertTrue(btnNext.isDisplayed());
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@name='Next']"))).click();
        Thread.sleep(8000);
    }

    public WebElement getNextButton() {
        return btnNext;
    }
}
