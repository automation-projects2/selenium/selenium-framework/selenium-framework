package com.usbank.test.automationFramework.trancore.pageObjects.profile;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;


public class AddEmailAddressPage {

    private static final Logger logger = LogManager.getLogger(AddEmailAddressPage.class);
    private static final long EXPLICIT_WAIT_TIME_SECONDS = 30;
    private WebDriver driver;

    @FindBy(how = How.TAG_NAME, using = "h1")
    private WebElement headerName;

    @FindBy(how = How.ID, using = "emailAddress")
    private WebElement txtfieldAddEmailAddress;

    @FindBy(how = How.CLASS_NAME, using = "omv_fullbutton")
    private WebElement buttonAddEmailAddress;

    @FindBy(how = How.LINK_TEXT, using = "Cancel")
    private WebElement buttonCancel;


    public AddEmailAddressPage(WebDriver inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public void verifyHeaderName() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(txtfieldAddEmailAddress));
        assertEquals("add email address", headerName.getText().toLowerCase());

    }

    public void clickAddEmailAddress() {
        buttonAddEmailAddress.click();
    }

    public void clickCancelButton() {
        buttonCancel.click();
    }

}



	