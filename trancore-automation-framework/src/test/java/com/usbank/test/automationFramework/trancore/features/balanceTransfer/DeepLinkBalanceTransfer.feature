@deepLinkBalanceTransfer
Feature: Balance Transfer
  Test cases for validating user interactions with Balance Transfer

  Scenario Outline: Verify user lands on BT offer page after clicking on the deep link

    Given the user user clicks on link <deepLink> that received in marketing email
    Then the user has navigated to the login page and logs in using <testData> profile
    Then the user landed on MFA page and enters the security question answers
    Then the system displays the Balance Transfer Offer page
    And click on View details link and Select this Offer button
    And Click view terms and Conditions and select I agree checkbox in balance transfer request page
    And Fill the appropriate details <Payee> and <AccountNumber> and <TransferAmount> and click continue button
    Then the user lands on balance transfer review and submit page
    And the user clicks on submit button
    Then user lands on Balance transfer confirmation page

    Examples:
      | deepLink                                                                        | testData      | Payee            | AccountNumber | TransferAmount |


  Scenario Outline: Verify non enrolled user lands on BT offer page after clicking on the deep link

    Given the non enrolled user clicks on link <deepLink> that received in marketing email
    Then the user has navigated to the login page and clicks on enroll button
    Then the user landed on enrollment  page and enters the details of Account Holder <Card_Num> and <CVV> and <SSN> and <ZipORPIN> and <PinFlag> and <Personal_Id> and <Password> and <Email_id> and clicks Submit
    And after enrolling successfully user will able to see the transaction details button
    When the user clicks on GoTo Transaction Details button
    Then the user landed on MFA page and enters the security question answers
    Then the system displays the Balance Transfer Offer page
    And click on View details link and Select this Offer button
    And Click view terms and Conditions and select I agree checkbox in balance transfer request page
    And Fill the appropriate details <Payee> and <AccountNumber> and <TransferAmount> and click continue button
    Then the user lands on balance transfer review and submit page
    And the user clicks on submit button
    Then user lands on Balance transfer confirmation page

    Examples:

      | deepLink                                                                        | Card_Num         | CVV | SSN  | ZipORPIN | PinFlag | Personal_Id | Password  | Email_id        | Payee            | AccountNumber | TransferAmount |
