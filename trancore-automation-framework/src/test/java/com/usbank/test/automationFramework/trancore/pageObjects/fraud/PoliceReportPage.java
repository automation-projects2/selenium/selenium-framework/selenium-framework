package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PoliceReportPage<T extends WebDriver> {

    protected static final Logger logger = LogManager.getLogger(PoliceReportPage.class);

    protected T driver;


    public PoliceReportPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "5009-Y")
    private WebElement rdbYes;

    @FindBy(id = "5009-N")
    private WebElement rdbNo;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement btnContinue;

    @FindBy(xpath = "//*[text()='Filing a police report'")
    private WebElement lblPoliceReportPage;

    @FindBy(id = "5011")
    private WebElement txtAreaAgency;

    @FindBy(id = "5010")
    private WebElement txtAreaCaseNo;

    @FindBy(id = "5012")
    private WebElement txtAreaPoliceContact;

    public WebElement getRdbNo() {
        return rdbNo;
    }

    public WebElement getRdbYes() {
        return rdbYes;
    }

    public WebElement getTxtAreaAgency() {
        return txtAreaAgency;
    }

    public WebElement getTxtAreaCaseNo() {
        return txtAreaCaseNo;
    }

    public WebElement getTxtAreaPoliceContact() {
        return txtAreaPoliceContact;
    }

    public WebElement getLblPoliceReportPage() {
        return lblPoliceReportPage;
    }

    public void waitForPoliceReportPageToLoad() {

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(getRdbYes()));
    }

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        ele.click();
    }

    public void clickNoRadioButton() {
        waitForPoliceReportPageToLoad();
        clickElement(driver, rdbNo);
    }

    public void clickYesRadioButton() {
        waitForPoliceReportPageToLoad();
        clickElement(driver, rdbYes);
    }

    public void clickContinue() {
        clickElement(driver, btnContinue);
    }

}
