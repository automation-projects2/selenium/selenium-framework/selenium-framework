@SpendAnalysisSuite @SpendAnalysis @WIP @Regression
Feature: Spend Analysis

  Scenario Outline: Spend Analysis
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When I click Services Tab
    And I click link - Spend Analysis
    Then the user is able to view the current spend analysis of the year, month, & quarter

    Examples:
      | partner       | testData |
