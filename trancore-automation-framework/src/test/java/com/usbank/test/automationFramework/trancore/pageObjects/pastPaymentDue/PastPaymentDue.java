package com.usbank.test.automationFramework.trancore.pageObjects.pastPaymentDue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class PastPaymentDue<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(PastPaymentDue.class);
    private T driver;

    @FindBy(how = How.XPATH, using = "//*[@id=\"mainNav\"]//*[contains(text(),'MY ACCOUNT')]")
    WebElement MyAccountTab;

    @FindBy(how = How.XPATH, using = "//*[@id=\"tc_accountBlock\"]//*[contains(text(),'Select Account')]")
    WebElement LnkSelectAccount;

    @FindBy(how = How.XPATH, using = "(//span[contains(text(),'Make a payment')])[1]")
    WebElement LnkMakeAPayment;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Please make a payment as soon as you can.')]")
    WebElement msgMakeaPayment;

    @FindBy(how = How.XPATH, using = "//*[@id=\"layoutTopWrapper\"]/div[1]/div[2]/div[3]/div/div[1]")
    WebElement msgAccountEndingIn;

    @FindBy(how = How.XPATH, using = "//*[@id=\"layoutContentBody\"]/div/div[3]/div/select/option[1]")
    WebElement msgSelectAccount;


    public PastPaymentDue(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public void clickSelectAccountLink() {
        LnkSelectAccount.click();
    }

    public void clickonMakeAPayementLink() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        LnkMakeAPayment.click();
    }

    public void clickMyAccountTab() {
        MyAccountTab.click();
    }

    public void validateMessage() {
        if (msgMakeaPayment.isDisplayed()) {
            logger.info("Please make a payment as soon as you can message displayed");
        } else {
            logger.info("Please make a payment as soon as you can message NOT displayed");
        }


        String DropdownAccount = msgSelectAccount.getText().substring(msgSelectAccount.getText().length() - 4);
        String EndingwithMessage = msgAccountEndingIn.getText().substring(msgAccountEndingIn.getText().length() - 4);
        System.out.println(DropdownAccount);
        System.out.println(EndingwithMessage);
        if (DropdownAccount.equals(EndingwithMessage)) {
            logger.info("Payment account displayed succefuly" + EndingwithMessage);
        } else {
            logger.info("Payment account NOT displayed succefuly" + EndingwithMessage);
        }

    }
}
