package com.usbank.test.automationFramework.trancore.pageObjects.payments;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PaymentOptionsPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(PaymentOptionsPage.class);

    private T driver;
    private WebDriverWait wait;


    @FindBy(xpath = "//html/head/title")
    private WebElement pageTitle;

    @FindBy(xpath = "//*[@id='onlineSettlementsContent']/p")
    private WebElement onlineSettlementsContent;

    @FindBy(xpath = "//*[@id='PaymentAssistanceContent']/p")
    private WebElement paymentAssistanceContent;

    @FindBy(xpath = "//*[text() ='Go to Payments']")
    private WebElement goToPaymentLink;

    @FindBy(xpath = "//*[text() ='Take me to payment assistance']")
    private WebElement takeMeToPaymentAssistanceLink;

    @FindBy(xpath = "//*[text() ='See my settlement offer']")
    private WebElement goToSeeMySettlementOfferLink;

    @FindBy(xpath = "//*[text() ='Go back to Payment Options']")
    private WebElement goBackToPaymentOptionsLink;

    @FindBy(xpath = "//button[@id='onlineSettlementEnrollButton']")
    private WebElement acceptStlmtOfferBtn;

    @FindBy(xpath = "//html/head/title")
    private WebElement getPage;

    //TODO: Remove this declaration after Team Jalapenos builds Payment Options page with Short Term Plan Link

    @FindBy(linkText = "TEAM WIZARDS TEMPORARY LINK FOR SHORT TERM PLAN")
    private WebElement temporaryWizardsShortTermLink;

    //Short term program - page objects added by Madhav
    @FindBy(xpath = "//*[@data-test='short-term-assistance-content']/h2")
    private WebElement shortTermHeader;

    @FindBy(xpath = "//*[@data-test='short-term-assistance-content']/div/p")
    private WebElement shortTermParagraph;

    @FindBy(linkText = "I'm interested in the short-term plan")
    private WebElement shortTermLink;

    @FindBy(className = "contentAreaSubHeader")
    private WebElement paymentOptionsLandingPageHeader;

    @FindBy(className = "contentAreaSubHeader")
    private WebElement paymentOptionsPageHeader;

    //In-house long term program - page objects added by Madhav
    @FindBy(xpath = "//*[@data-test='payment-assistance-content']/h2")
    private WebElement longTermHeader;

    @FindBy(xpath = "//*[@data-test='payment-assistance-content']/div/div[1]/p")
    private WebElement longTermParagraph;

    @FindBy(linkText = "I'm interested in the long-term program")
    private WebElement longTermLink;

    // Settlement Offer
    // @FindBy(id = "onlineSettlementsHeader")
    @FindBy(xpath = "//*[@data-test='onlineSettlement-content']/h2")
    private WebElement onlineSettlementsHeader;

    @FindBy(xpath = "//*[@data-test='onlineSettlement-content']/div/p")
    private WebElement onlineSettlementsParagraphText;

    @FindBy(linkText = "See my settlement offer")
    private WebElement onlineSettlementsLink;

    public PaymentOptionsPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 20);

    }

    @FindBy(xpath = "//*[@id='subHeader']/span")
    private WebElement headingPaymentOptions;


    @FindBy(xpath = "//*[@id='subHeader']/span")
    private WebElement headingSettlementOffer;

    public WebElement getHeadingPaymentOptions() {
        try {
            waitForPageLoad();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return headingPaymentOptions;

    }

    public WebElement getHeadingEnrollInSettlement() {
        try {
            waitForPageLoad();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return headingSettlementOffer;

    }

    public WebElement getOnlineSettlementsContent() {
        return onlineSettlementsContent;
    }

    public WebElement getPaymentAssistanceContent() {
        return paymentAssistanceContent;
    }

    //TODO: Remove this method after Team Jalapenos builds Payment Options page with Short Term Plan Link
    public WebElement getTemporaryWizardsShortTermLink() {
        return temporaryWizardsShortTermLink;
    }

    public void waitForPageLoad() throws InterruptedException {

        int max = 8;
        int counter = 0;
        boolean pageLoaded = false;

        while (pageLoaded == false && counter < max) {

            if (pageTitle.getText().equalsIgnoreCase("Credit Card Account Access:  Payment solutions")) {
                pageLoaded = true;
            } else {

                Thread.sleep(2000);
                counter++;

            }
        }
    }

    public void clickOnGoToPaymentLink() {

        if (goToPaymentLink.isDisplayed()) {
            logger.info("Go to Payment link is displayed");
            goToPaymentLink.click();

        } else {
            logger.info("There is no Go to payment link displayed");
        }

    }

    public void clickOnTakeMeToPaymentAssistanceLink() {
        if (takeMeToPaymentAssistanceLink.isDisplayed()) {
            logger.info("Take me to Payment Assistance link is displayed");
            //takeMeToPaymentAssistanceLink.click();
        } else {
            logger.info("There is no Take me to Payment Assistance link displayed");
        }
    }

    public void clickOnSeeMySettlementOfferLink() {

        if (goToSeeMySettlementOfferLink.isDisplayed()) {
            logger.info("Go to See my settlement offer link is displayed");
            goToSeeMySettlementOfferLink.click();

        } else {
            logger.info("There is no See my settlement offer link displayed");
        }

    }

    public void clickOnBackToPaymentOptionsLink() {

        if (goBackToPaymentOptionsLink.isDisplayed()) {
            logger.info("Go back to Payment Solutions link is displayed");
            goBackToPaymentOptionsLink.click();

        } else {
            logger.info("There is no Go back to Payment Solutions link displayed");
        }

    }

    public void clickEnrollBtn() {

        logger.info("============= Accept Settlement Offer button is clicked successfully ==============");
        acceptStlmtOfferBtn.click();
    }

    public void getPageName() {

        getPage.isDisplayed();
        logger.info("============= Settlement enrollment confirmation page displayed ==============");

    }

    public void getPageName1() {

        getPage.isDisplayed();
        logger.info("============= Payment Solutions page displayed ==============");

    }

    public boolean verifyShortTermProgramHeaderISDisplayed() {
        logger.info("Short-term program header is displayed: " + shortTermHeader.getText());
        return shortTermHeader.getText().equals("Short-term plan");
    }

    public boolean verifyShortTermProgramParagraphISDisplayed(){
        logger.info("Short-term program paragraph is displayed: " + shortTermParagraph.getText());
        return shortTermParagraph.getText().equals("For six billing cycles, your monthly minimum payment will be reduced. All fees will be waived, and your interest rate may be lower. Your charging privileges will be suspended but may be restored at the end of the plan.");
//    public boolean verifyShortTermProgramParagraphISDisplayed() {
//        logger.info("Short-term program header is displayed: " + shortTermParagraph.getText());
//        return shortTermParagraph.getText().equals("Short-term plan");
    }

    public boolean verifyShortTermLinkISDisplayed() {
        logger.info("Short-term program link : " + shortTermLink.getText());
        return shortTermLink.getText().equals("I'm interested in the short-term plan");
    }

    public WebElement getShortTermLink() {
        wait.until(ExpectedConditions.visibilityOf(shortTermLink));
        logger.info("Short-term program link : " + shortTermLink.getText());
        return shortTermLink;
    }

    public boolean verifyLongTermProgramHeaderISDisplayed(){
        wait.until(ExpectedConditions.visibilityOf(longTermHeader));
        logger.info("Long-term program header is displayed: " + longTermHeader.getText());
        return longTermHeader.getText().equals("Long-term program");
    }

    public boolean verifyLongTermProgramParagraphISDisplayed(){
        logger.info("Long-term program paragraph is displayed: " + longTermParagraph.getText());
        return longTermParagraph.getText().equals("You’ll be able to pay your balance with 60 monthly payments. The APR (annual percentage rate) is reduced and your monthly minimum payments may be lower. The account will be closed and the account status will be reported to credit agencies. (Business card accounts are not reported to consumer credit agencies.)");
    }

    public boolean verifyLongTermLinkISDisplayed(){
        logger.info("Long-term program link : " +longTermLink.getText());
        return longTermLink.getText().equals("I'm interested in the long-term program");
    }

    public WebElement clickLongTermLink(){        return longTermLink;    }

    public boolean verifySettlementProgramHeaderISDisplayed(){
        wait.until(ExpectedConditions.visibilityOf(onlineSettlementsHeader));
        logger.info("Settlement program header is displayed: " + onlineSettlementsHeader.getText());
        return onlineSettlementsHeader.getText().equals("Settlement offer");
    }

    public boolean verifySettlementProgramParagraphISDisplayed(){
        logger.info("Settlement program paragraph is displayed: " + onlineSettlementsParagraphText.getText());
        return onlineSettlementsParagraphText.getText().equals("Settle your total balance by paying a portion of the amount owed in a short repayment period. Your account will close, and the account status will be reported to credit agencies. (Business card accounts are not reported to consumer credit agencies.)");
    }

    public boolean verifySettlementProgramLinkISDisplayed(){
        logger.info("Settlement program link : " +onlineSettlementsLink.getText());
        return onlineSettlementsLink.getText().equals("See my settlement offer");
    }

    public void verifyShortTermProgramHeaderIsNotDisplayed(){
        int size = driver .findElements(By.xpath("//*[@data-test='short-term-assistance-content']/h2")).size();
        if (size==0)
            logger.info("Short term program header is NOT displayed");
    }

    public void verifyShortTermProgramLinkIsNotDisplayed(){
        int size = driver .findElements(By.partialLinkText("I'm interested in the short-term plan")).size();
        if (size==0)
            logger.info("Short term program link is NOT displayed");
    }

    public void validatePaymentOptionsLandingPageHeader() {
        wait.until(ExpectedConditions.visibilityOf(paymentOptionsPageHeader));
        logger.info("Payment Options Landing Page Text is displayed: " + paymentOptionsPageHeader.getText());
        Assert.assertTrue(paymentOptionsPageHeader.isDisplayed());
    }

    public boolean validateTitleOfThePaymentOptionsLandingPage(){
        wait.until(ExpectedConditions.visibilityOf(paymentOptionsPageHeader));
        logger.info("Payment Options landing page title is as expected: " + driver.getTitle());
        return driver.getTitle().equals("Credit Card Account Access: Payment options");
    }

}


