package com.usbank.test.automationFramework.trancore.stepDefinitions.payments;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

public class HardshipQuestionnaireSteps {
    @Autowired
    StepDefinitionManager stepDefinitionManager;
    WebDriver driver;

    @Then("user must be directed to Questionnaire1 page and the title of the page should be Length of hardship")
    public void userMustBeDirectedToQuestionnairePageAndTheTitleOfThePageShouldBeLengthOfHardship() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().verifyQuestionnairePageTitle())
            Assert.fail("Questionnaire1 page title is not as expected");
    }

    @Then("verify that the header of the Questionnaire page should be displayed as We want to help.")
    public void verifyThatTheHeaderOfTheQuestionnairePageShouldBeDisplayedAsWeWantToHelp() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().verifyQuestionnaire1PageHeaderDisplayed())
            Assert.fail("Questionnaire1 page header is not displayed");
//        stepDefinitionManager.captureFullPageScreenshotsWithScrolling();
    }

    @Then("verify that user must be displayed with the verbiage which starts with- If you're experiencing")
    public void verifyThatUserMustBeDisplayedWithTheVerbiageWhichStartsWithIfYouReExperiencing() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().verifyTheParagraphStartingWIthIfYouAreExperiencingIsDisplayed())
            Assert.fail("if you are experiencing - paragraph is not displayed");
    }

    @Then("verify that user must be displayed with question for length of hardship which starts with- Before we start")
    public void verifyThatUserMustBeDisplayedWithQuestionForLengthOfHardshipWhichStartsWithBeforeWeStart() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().verifyQuestionnaire1LengthOfHardshipQuestionDisplayed())
            Assert.fail("length of hardship question is not displayed");
    }

    @Then("verify that user must be displayed with first radio button as Less than 4 months")
    public void verifyThatUserMustBeDisplayedWithFirstRadioButtonAsLessThanMonths() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().verifyLengthOfHardshipFirstRadioButtonDisplayed())
            Assert.fail("Less than 4 months radio button is not displayed");
    }

    @Then("verify that user must be displayed with second radio button as 4 to 12 months")
    public void verifyThatUserMustBeDisplayedWithSecondRadioButtonAsToMonths() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().verifyLengthOfHardshipSecondRadioButtonDisplayed())
            Assert.fail("4 to 12 months radio button is not displayed");
    }

    @Then("verify that user must be displayed with third radio button as 1 to 3 years")
    public void verifyThatUserMustBeDisplayedWithThirdRadioButtonAs1To3Years() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().verifyLengthOfHardshipThirdRadioButtonDisplayed())
            Assert.fail("1 to 3 years radio button is not displayed");
    }

    @Then("verify that user must be displayed with fourth radio button as 3 to 5 years")
    public void verifyThatUserMustBeDisplayedWithFourthRadioButtonAs3To5Years() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().verifyLengthOfHardshipFourthRadioButtonDisplayed())
            Assert.fail("3 to 5 years radio button is not displayed");
    }

    @Then("verify that user must be displayed with Continue button")
    public void verifyThatUserMustBeDisplayedWithContinueButton() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().verifyContinueButtonDisplayed())
            Assert.fail("Continue button is not displayed");
//        stepDefinitionManager.getPageObjectManager().getCaptureScreenShots().takeScreenshot(" demo file name");
    }

    @Then("verify that user must be displayed with Cancel link")
    public void verifyThatUserMustBeDisplayedWithCancelLink() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().verifyCancelLinkDisplayed())
            Assert.fail("Cancel link is not displayed");
    }

    @When("the user selects first radio button Less than 4 months and the user clicks on Continue button")
    public void theUserSelectsFirstRadioButtonLessThan4MonthsAndTheUserClicksOnContinueButton() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().clickLengthOfHardshipFirstRadioButton().click();
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().clickContinueButton().click();
    }

//    @Then("user must be directed to Questionnaire2 page and the header of the page should be Tell us more about your hardship")
//    public void userMustBeDirectedToQuestionnaire2PageAndTheHeaderOfThePageShouldBeTellUsMoreAboutYourHardship() {
//        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnaire2Page().verifyQuestionnaire2PageHeaderDisplayed())
//            Assert.fail("Questionnaire2 page is not displayed");
//    }
//
//    @Then("verify that the title of the page should be displayed as Questions")
//    public void verifyThatTheTitleOfThePageShouldBeDisplayedAsQuestions() {
//        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnaire2Page().verifyQuestionnaire2PageTitle())
//            Assert.fail("Questionnaire2 title is not available");
//    }

    @And("the user inputs Current Monthly income")
    public void theUserInputsCurrentMonthlyIncome() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().inputCurrentMonthlyIncome().sendKeys("1234");
    }

    @And("the user inputs Current Monthly Expenses")
    public void theUserInputsCurrentMonthlyExpenses() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().inputCurrentMonthlyExpenses().sendKeys("1234");
    }

    @And("the user selects reason for your hardship from the dropdown in the Questionnaire screen")
    public void theUserSelectsReasonForYourHardshipFromTheDropdownInTheQuestionnaireScreen() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().selectHardshipReason().selectByIndex(1);
//        stepDefinitionManager.captureFullPageScreenshotsWithScrolling();
    }

    @And("the user clicks on Continue button")
    public void theUserClicksOnContinueButton() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().clickContinueButton().sendKeys(Keys.RETURN);
    }

    @Then("verify that the user is displayed with the paragraph starts with To recommend a plan that fits")
    public void verifyThatTheUserIsDisplayedWithTheParagraphStartsWithToRecommendAPlanThatFits() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().verifyToRecommendAPlanParagraphDisplayed())
            Assert.fail("To recommend a plan paragraph is not displayed");
    }

    @Then("verify that the input box displayed with label current monthly income")
    public void verifyThatTheInputBoxDisplayedWithLabelCurrentMonthlyIncome() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().currentMonthlyIncomeSubHeaderIsDisplayed())
            Assert.fail("Current monthly income sub header is not displayed");
        else if (stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().currentMonthlyIncomeSubHeaderIsDisplayed())
            if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().currentMonthlyIncomeInputBoxIsDisplayed())
                Assert.fail("Current monthly income input box is not displayed");
    }

    @Then("verify that the user is displayed with the disclosure paragraph for monthly income")
    public void verifyThatTheUserIsDisplayedWithTheDisclosureParagraphForMonthlyIncome() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().currentMonthlyIncomeDisclosureParaIsDisplayed())
            Assert.fail("Current monthly income Disclosure paragraph is not displayed");
    }

    @Then("verify that the input box displayed with label current monthly expenses")
    public void verifyThatTheInputBoxDisplayedWithLabelCurrentMonthlyExpenses() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().currentMonthlyExpensesSubHeaderIsDisplayed())
            Assert.fail("Current monthly expenses sub header is not displayed");
        else if (stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().currentMonthlyExpensesSubHeaderIsDisplayed())
            if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().currentMonthlyExpensesInputBoxIsDisplayed())
                Assert.fail("Current monthly expenses input box is not displayed");
    }

    @Then("verify that the user is displayed with the helper text for monthly expenses")
    public void verifyThatTheUserIsDisplayedWithTheHelperTextForMonthlyExpenses() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().currentMonthlyExpensesHelperTextIsDisplayed())
            Assert.fail("Current monthly expenses helper text is not displayed");
    }

    @Then("verify that the dropdown displayed with default value = select one and with title Please share the reason for your hardship")
    public void verifyThatTheDropdownDisplayedWithDefaultValueSelectOneAndWithTitlePleaseShareTheReasonForYourHardship() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().reasonForHardshipSubHeaderIsDisplayed())
            Assert.fail("Reason for Hardship sub header is not displayed");
        else if (stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().reasonForHardshipSubHeaderIsDisplayed())
            if ((stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().reasonForHardshipDefaultValueIsDisplayed()) != 1)
                Assert.fail("Reason for Hardship dropdown default value - Select One is not displayed");
    }

    @Then("verify that the dropdown values displayed as COVID-19 pandemic,Excessive debt obligation,Loss of income,Unemployment,Other")
    public void verifyThatTheDropdownValuesDisplayedAsCOVID19PandemicExcessiveDebtObligationLossOfIncomeUnemploymentOther() {
        if ((stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validateTheReasonForHardshipDropdownValues()) != 5)
            Assert.fail("Reason for Hardship dropdown 5 values are NOT displayed");
    }

    @When("the user user clicks on continue button in the Questionnaire screen")
    public void theUserUserClicksOnContinueButtonInTheQuestionnaireScreen() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().clickContinueButton().sendKeys(Keys.RETURN);
    }

    @Then("verify that the page level error message displayed as Please select your length of hardship.")
    public void verifyThatThePageLevelErrorMessageDisplayedAsPleaseSelectYourLengthOfHardship() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().verifyPageLevelErrorForLengthOfHardship())
            Assert.fail("Page level error for length of hardship is NOT displayed");
    }

    @Then("verify that the page level error message displayed for radio button as Please select your length of hardship.")
    public void verifyThatThePageLevelErrorMessageDisplayedForRadioButtonAsPleaseSelectYourLengthOfHardship() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validatePageLevelErrorForIncomeWhenOneFieldsErrorExists();
//        if(!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().verifyPageLevelErrorForLengthOfHardship())
//            Assert.fail("Page level error for length of hardship is NOT displayed");
//        stepDefinitionManager.captureFullPageScreenshotsWithScrolling();
    }

    @And("verify that the field level error message displayed as Please make a selection.")
    public void verifyThatTheFieldLevelErrorMessageDisplayedAsPleaseMakeASelection() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().verifyFieldLevelErrorForLengthOfHardship())
            Assert.fail("Field level error for length of hardship is NOT displayed");
    }

    @When("the user selects first radio button Less than 4 months")
    public void theUserSelectsFirstRadioButtonLessThan4Months() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().clickLengthOfHardshipFirstRadioButton().click();
    }

    @Then("verify that the page level error message for income displayed as There’s an issue with your monthly income.")
    public void verifyThatThePageLevelErrorMessageForIncomeDisplayedAsThereSAnIssueWithYourMonthlyIncome() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validatePageLevelErrorForIncomeWhenAllThreeFieldsErrorExists();
    }

    @And("verify that field level error message for income displayed as Please enter your monthly income. If you have none, enter zero.")
    public void verifyThatFieldLevelErrorMessageForIncomeDisplayedAsPleaseEnterYourMonthlyIncomeIfYouHaveNoneEnterZero() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validateFieldLevelErrorWhenIncomeIsMissingInQuestionnaire2())
            Assert.fail("Field Level error when monthly income is missing NOT displayed");
    }

    @And("verify that the page level error message for expenses displayed as There’s an issue with your monthly expenses.")
    public void verifyThatThePageLevelErrorMessageForExpensesDisplayedAsThereSAnIssueWithYourMonthlyExpenses() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validatePageLevelErrorForExpensesWhenAllThreeFieldsErrorExists();
    }

    @And("verify that field level error message for income displayed as Please enter your monthly expenses. If you have none, enter zero.")
    public void verifyThatFieldLevelErrorMessageForIncomeDisplayedAsPleaseEnterYourMonthlyExpensesIfYouHaveNoneEnterZero() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validateFieldLevelErrorWhenExpensesIsMissingInQuestionnaire2())
            Assert.fail("Field Level error when monthly expenses is missing is NOT displayed");
    }

    @And("verify that the page level error message for the hardship reason displayed as There’s an issue with your reason for hardship.")
    public void verifyThatThePageLevelErrorMessageForTheHardshipReasonDisplayedAsThereSAnIssueWithYourReasonForHardship() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validatePageLevelErrorForReasonForHardshipWhenAllThreeFieldsErrorExists();
    }

    @And("verify that field level error message for hardship reason displayed as Please make a selection.")
    public void verifyThatFieldLevelErrorMessageForHardshipReasonDisplayedAsPleaseMakeASelection() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validateFieldLevelErrorWhenReasonForHardshipIsMissingInQuestionnaire2())
            Assert.fail("Field Level error for reason for hardship when no value selected is NOT displayed");
    }

    @When("the user inputs invalid characters for income and expenses")
    public void theUserInputsInvalidCharactersForIncomeAndExpenses() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnaire2Page().inputCurrentMonthlyIncome().sendKeys("abcv");
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnaire2Page().inputCurrentMonthlyExpenses().sendKeys("abcv");
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnaire2Page().selectHardshipReason().selectByIndex(1);
    }

    @Then("verify that page level error for income and expenses still displays and the field error message should be displayed as You may enter zero or whole dollars. Enter numbers and no other characters including periods or decimal points.")
    public void verifyThatPageLevelErrorForIncomeAndExpensesStillDisplaysAndTheFieldErrorMessageShouldBeDisplayedAsYouMayEnterZeroOrWholeDollarsEnterNumbersAndNoOtherCharactersIncludingPeriodsOrDecimalPoints() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validatePageLevelErrorForIncomeWhenAllThreeFieldsErrorExists();
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validateFieldLevelErrorWhenIncomeContainsInvalidCharactersInQuestionnaire2())
            Assert.fail("Field Level error when monthly income has invalid characters is not displayed");
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validatePageLevelErrorForExpensesWhenAllThreeFieldsErrorExists();
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validateFieldLevelErrorWhenExpensesContainsInvalidCharactersInQuestionnaire2())
            Assert.fail("Field Level error when monthly expenses has invalid characters is not displayed");
//                stepDefinitionManager.captureScreenshot();
    }

    @When("the user inputs value greater than 999999 for income and expenses")
    public void theUserInputsValueGreaterThan999999ForIncomeAndExpenses() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().inputCurrentMonthlyIncome().sendKeys("9999991");
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().inputCurrentMonthlyExpenses().sendKeys("1000000");
        }

    @Then("verify that page level error for income and expenses still displays and the field error message should be displayed as Please enter an amount that is less than $1,000,000.")
    public void verifyThatPageLevelErrorForIncomeAndExpensesStillDisplaysAndTheFieldErrorMessageShouldBeDisplayedAsPleaseEnterAnAmountThatIsLessThan$1000000() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validatePageLevelErrorForIncomeWhenAllThreeFieldsErrorExists();
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validateFieldLevelErrorWhenIncomeExceeds999999InQuestionnaire2())
            Assert.fail("Field Level Error when Monthly income exceeds $999,999 is not displayed");
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validatePageLevelErrorForExpensesWhenAllThreeFieldsErrorExists();
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validateFieldLevelErrorWhenExpensesExceeds999999InQuestionnaire2())
            Assert.fail("Field Level Error when Monthly expenses exceeds $999,999 is not displayed");
//        stepDefinitionManager.captureScreenshot();
    }

    @When("the user inputs value which begins with one zero or multiple zeros for income and expenses")
    public void theUserInputsValueWhichBeginsWithOneZeroOrMultipleZerosForIncomeAndExpenses() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().inputCurrentMonthlyIncome().sendKeys("01");
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().inputCurrentMonthlyExpenses().sendKeys("0000114");
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().selectHardshipReason().selectByIndex(1);
//        stepDefinitionManager.captureScreenshot();
    }

    @Then("verify that page level error for income and expenses still displays and the field error message should be displayed as Please remove zeros before whole dollar amounts.")
    public void verifyThatPageLevelErrorForIncomeAndExpensesStillDisplaysAndTheFieldErrorMessageShouldBeDisplayedAsPleaseRemoveZerosBeforeWholeDollarAmounts() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validatePageLevelErrorForIncomeWhenAllThreeFieldsErrorExists();
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validateFieldLevelErrorWhenIncomeBeginsWithZerosInQuestionnaire2())
            Assert.fail("Field Level Error when Monthly Income begins with one or more zeros is not displayed");
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validatePageLevelErrorForExpensesWhenAllThreeFieldsErrorExists();
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validateFieldLevelErrorWhenExpensesBeginsWithZerosInQuestionnaire2())
            Assert.fail("Field Level Error when Monthly Expenses begins with one or more zeros is not displayed");
    }

    @When("the user inputs valid values for income, expenses and the user selects value for reason for hardship")
    public void theUserInputsValidValuesForIncomeExpensesAndTheUserSelectsValueForReasonForHardship() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().inputCurrentMonthlyIncome().sendKeys("1");
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().inputCurrentMonthlyExpenses().sendKeys("999999");
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().selectHardshipReason().selectByIndex(1);
    }

    @Then("verify that the field level error for income, expenses, reason for hardship disappears but the page level error messages still displays")
    public void verifyThatTheFieldLevelErrorForIncomeExpensesReasonForHardshipDisappearsButThePageLevelErrorMessagesStillDisplays() {
    }

    @When("the user user clicks on continue button in the Questionnaire1 screen")
    public void theUserUserClicksOnContinueButtonInTheQuestionnaire1Screen() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().clickContinueButton().click();
    }

    @When("the user selects second radio button 4 to 12 months")
    public void theUserSelectsSecondRadioButton4To12Months() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().clickLengthOfHardshipSecondRadioButton().click();
    }

    @When("the user selects third radio button 1 to 3 years")
    public void theUserSelectsThirdRadioButton1To3Years() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().clickLengthOfHardshipThirdRadioButton().click();
    }

    @When("the user selects fourth radio button 3 to 5 years")
    public void theUserSelectsFourthRadioButton3To5Years() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().clickLengthOfHardshipFourthRadioButton().click();
    }

    @Then("verify that the short term header, paragraph and the short term link displayed")
    public void verifyThatTheShortTermHeaderParagraphAndTheShortTermLinkDisplayed() {
        if (stepDefinitionManager.getPageObjectManager().getPaymentOptionsPage().verifyShortTermProgramHeaderISDisplayed()) {
            if (stepDefinitionManager.getPageObjectManager().getPaymentOptionsPage().verifyShortTermProgramParagraphISDisplayed()) {
                if (!stepDefinitionManager.getPageObjectManager().getPaymentOptionsPage().verifyShortTermLinkISDisplayed())
                    Assert.fail("Short-term link is NOT displayed in Payment Options landing page");
            } else
                Assert.fail("Short-term paragraph is NOT displayed in Payment Options landing page");
        } else
            Assert.fail("Short-term header is NOT displayed in Payment Options landing page");
    }

    @When("the user inputs valid value for income and click on continue button")
    public void theUserInputsValidValueForIncomeAndClickOnContinueButton() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().inputCurrentMonthlyIncome().sendKeys("221");
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().clickContinueButton().click();
    }

    @Then("verify that the number of fields required action displayed as 2 items require action:")
    public void verifyThatTheNumberOfFieldsRequiredActionDisplayedAs2ItemsRequireAction() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validatePageLevelErrorForExpensesWhenAllThreeFieldsErrorExists();
    }

    @Then("verify that the page level error message for expenses displayed as There’s an issue with your monthly expenses with in 2 items required action")
    public void verifyThatThePageLevelErrorMessageForExpensesDisplayedAsThereSAnIssueWithYourMonthlyExpensesWithIn2ItemsRequiredAction() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validatePageLevelErrorForExpensesWhenTwoFieldsErrorExists();
    }

    @Then("verify that the page level error message for income displayed as There’s an issue with your monthly income with in 2 items required action")
    public void verifyThatThePageLevelErrorMessageForIncomeDisplayedAsThereSAnIssueWithYourMonthlyIncomeWithIn2ItemsRequiredAction() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validatePageLevelErrorForIncomeWhenTwoFieldsErrorExists();
    }

    @Then("verify that the field error message should be displayed as Please enter an amount that is less than $1,000,000.")
    public void verifyThatTheFieldErrorMessageShouldBeDisplayedAsPleaseEnterAnAmountThatIsLessThan$1000000() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validateFieldLevelErrorWhenIncomeExceeds999999InQuestionnaire2())
            Assert.fail("Field Level Error when Monthly income exceeds $999,999 is not displayed");
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validateFieldLevelErrorWhenExpensesExceeds999999InQuestionnaire2())
            Assert.fail("Field Level Error when Monthly expenses exceeds $999,999 is not displayed");
    }

    @Then("verify that the field error message for income and expenses should be displayed as Please remove zeros before whole dollar amounts.")
    public void verifyThatTheFieldErrorMessageForIncomeAndExpensesShouldBeDisplayedAsPleaseRemoveZerosBeforeWholeDollarAmounts() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validateFieldLevelErrorWhenIncomeBeginsWithZerosInQuestionnaire2())
            Assert.fail("Field Level Error when Monthly Income begins with one or more zeros is not displayed");
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validateFieldLevelErrorWhenExpensesBeginsWithZerosInQuestionnaire2())
            Assert.fail("Field Level Error when Monthly Expenses begins with one or more zeros is not displayed");
    }

    @Then("verify that the page level error message for expenses displayed as There’s an issue with your monthly expenses")
    public void verifyThatThePageLevelErrorMessageForExpensesDisplayedAsThereAnIssueWithYourMonthlyExpenses() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validatePageLevelErrorForExpensesWhenOneFieldsErrorExists();
    }

    @Then("verify that the page level error message for income displayed as There’s an issue with your monthly income")
    public void verifyThatThePageLevelErrorMessageForIncomeDisplayedAsThereAnIssueWithYourMonthlyIncome() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validatePageLevelErrorForIncomeWhenAllThreeFieldsErrorExists();
    }

    @Then("verify that the field error message for expenses should be displayed as Please remove zeros before whole dollar amounts.")
    public void verifyThatTheFieldErrorMessageForExpensesShouldBeDisplayedAsPleaseRemoveZerosBeforeWholeDollarAmounts() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validateFieldLevelErrorWhenExpensesBeginsWithZerosInQuestionnaire2())
            Assert.fail("Field Level Error when Monthly Expenses begins with one or more zeros is not displayed");
    }

    @Then("verify that the field error message for income should be displayed as Please remove zeros before whole dollar amounts.")
    public void verifyThatTheFieldErrorMessageForIncomeShouldBeDisplayedAsPleaseRemoveZerosBeforeWholeDollarAmounts() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validateFieldLevelErrorWhenIncomeBeginsWithZerosInQuestionnaire2())
            Assert.fail("Field Level Error when Monthly Income begins with one or more zeros is not displayed");
    }

    // Long term Work Out
    @Then("user must be directed to Questionnaire page and the title of the page should be Questions to help set up payment plan")
    public void userMustBeDirectedToQuestionnairePageAndTheTitleOfThePageShouldBeQuestionsToHelpSetUpPaymentPlan() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().verifyQuestionnairePageTitle())
            Assert.fail("Questionnaire page title is not as expected");

    }

    @Then("verify that user must be displayed with the verbiage which starts with- We understand that at any time")
    public void verifyThatUserMustBeDisplayedWithTheVerbiageWhichStartsWithWeUnderstandThatAtAnyTime() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().verifyTheParagraphStartingWIthWeUnderstandThatAtAnyTime())
            Assert.fail("We understand that at any time - paragraph is not displayed");
    }


    @Then("verify that the page level error header displayed as 4 items require action in the Questionnaire screen")
    public void verifyThatThePageLevelErrorHeaderDisplayedAs4ItemsRequireActionInTheQuestionnaireScreen() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validatePageLevelErrorHeaderNumberOfItemsRequireActionIs4();
//        stepDefinitionManager.captureFullPageScreenshotsWithScrolling();
    }

    @Then("verify that the page level error header displayed as 3 items require action in the Questionnaire screen")
    public void verifyThatThePageLevelErrorHeaderDisplayedAs3ItemsRequireActionInTheQuestionnaireScreen() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validatePageLevelErrorHeaderNumberOfItemsRequireActionIs3();
//        stepDefinitionManager.captureFullPageScreenshotsWithScrolling();
    }

    @Then("verify that the page level error header displayed as 2 items require action in the Questionnaire screen")
    public void verifyThatThePageLevelErrorHeaderDisplayedAs2ItemsRequireActionInTheQuestionnaireScreen() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().validatePageLevelErrorHeaderNumberOfItemsRequireActionIs2();
//        stepDefinitionManager.captureFullPageScreenshotsWithScrolling();
    }

    @Then("user must be directed to Payment Options landing page and the title of the page should be Payment Options")
    public void userMustBeDirectedToPaymentOptionsLandingPageAndTtheTitleOfThePageShouldBePaymentOptions() {
        if (!stepDefinitionManager.getPageObjectManager().getPaymentOptionsPage().validateTitleOfThePaymentOptionsLandingPage())
            Assert.fail("Payment Options Landing Page title is not as expected");

    }

    @Then("verify that the long term program header, paragraph and the long term link displayed")
    public void verifyThatTheLongTermProgramHeaderParagraphAndTheLongTermLinkDisplayed() {
        if (stepDefinitionManager.getPageObjectManager().getPaymentOptionsPage().verifyLongTermProgramHeaderISDisplayed()) {
            if (stepDefinitionManager.getPageObjectManager().getPaymentOptionsPage().verifyLongTermProgramParagraphISDisplayed()) {
                if (!stepDefinitionManager.getPageObjectManager().getPaymentOptionsPage().verifyLongTermLinkISDisplayed())
                    Assert.fail("Long-term program link is NOT displayed in Payment Options landing page");
            } else
                Assert.fail("Long-term program paragraph is NOT displayed in Payment Options landing page");
        } else
            Assert.fail("Long-term program header is NOT displayed in Payment Options landing page");
//        stepDefinitionManager.captureFullPageScreenshotsWithScrolling();
    }

    @Then("verify that the settlement program header, paragraph and the settlement link displayed")
    public void verifyThatTheSettlementProgramHeaderParagraphAndTheSettlementLinkDisplayed() {
        if (stepDefinitionManager.getPageObjectManager().getPaymentOptionsPage().verifySettlementProgramHeaderISDisplayed()) {
            if (stepDefinitionManager.getPageObjectManager().getPaymentOptionsPage().verifySettlementProgramParagraphISDisplayed()) {
                if (!stepDefinitionManager.getPageObjectManager().getPaymentOptionsPage().verifySettlementProgramLinkISDisplayed())
                    Assert.fail("Settlement Offer link is NOT displayed in Payment Options landing page");
            } else
                Assert.fail("Settlement Offer paragraph is NOT displayed in Payment Options landing page");
        } else
            Assert.fail("Settlement Offer header is NOT displayed in Payment Options landing page");
    }

    @And("the user clicks on Payments Tab")
    public void theUserClicksOnPaymentsTab() {
        stepDefinitionManager.getPageObjectManager().getWebNavigationPage().clickPaymentsLink();
    }

    @Then("verify that short term program is not displayed in Payment Options landing page")
    public void verifyThatShortTermProgramIsNotDisplayedInPaymentOptionsLandingPage() {
        stepDefinitionManager.getPageObjectManager().getPaymentOptionsPage().verifyShortTermProgramHeaderIsNotDisplayed();
        stepDefinitionManager.getPageObjectManager().getPaymentOptionsPage().verifyShortTermProgramLinkIsNotDisplayed();

    }

    @When("the user click on the Cancel Link in Questionnaire screen")
    public void theUserClickOnTheCancelLinkInQuestionnaireScreen() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().clickCancelLink().sendKeys(Keys.RETURN);
//        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().clickCancelLink().click();
    }

    @Then("the user should be displayed with cancel pop-up")
    public void theUserShouldBeDisplayedWithCancelPopUp() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().verifyTheCancelPopUp().isDisplayed());
    }

    @When("the user click on the close icon in the cancel popup")
    public void theUserClickOnTheCloseIconInTheCancelPopup() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().clickCloseIcon().sendKeys(Keys.RETURN);
    }

    @Then("verify that the user is still on the same Questionnaire page")
    public void verifyThatTheUserIsStillOnTheSameQuestionnairePage() {
        if (!stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().verifyQuestionnairePageTitle())
            Assert.fail("User session in Questionnaire screen is not retained");
    }

    @When("the user click on No, go back button")
    public void theUserClickOnNoGoBackButton() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().clickNoGoBackButton().click();
    }

    @When("the user click on Yes, leave button")
    public void theUserClickOnYesLeaveButton() {
        stepDefinitionManager.getPageObjectManager().getHardshipQuestionnairePage().clickYesLeaveButton().click();
    }

    @Then("the user should be re-directed to Payments page")
    public void theUserShouldBeReDirectedToPaymentsPage() {   }

    @When("the user click on the long term program link")
    public void theUserClickOnTheLongTermProgramLink() {
        stepDefinitionManager.getPageObjectManager().getPaymentOptionsPage().clickLongTermLink().click();
    }


    @Then("verify that the header of the page should be displayed as We want to help.")
    public void verifyThatTheHeaderOfThePageShouldBeDisplayedAsWeWantToHelp() {
    }

}
