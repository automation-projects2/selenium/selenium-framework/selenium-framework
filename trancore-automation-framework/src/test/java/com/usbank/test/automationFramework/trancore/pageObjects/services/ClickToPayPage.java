package com.usbank.test.automationFramework.trancore.pageObjects.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ClickToPayPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(ClickToPayPage.class);

    private T driver;

    @FindBy(xpath = "//span[text()='Click to pay with card']")
    private WebElement headingClickToPayWithCard;

    public ClickToPayPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    public WebElement getHeadingClickToPayWithCard() {
        return headingClickToPayWithCard;
    }

    public void waitForPageLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(headingClickToPayWithCard));
    }

}
