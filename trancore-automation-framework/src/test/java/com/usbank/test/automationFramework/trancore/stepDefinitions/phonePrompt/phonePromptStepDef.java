package com.usbank.test.automationFramework.trancore.stepDefinitions.phonePrompt;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class phonePromptStepDef {

    @Autowired
    StepDefinitionManager stepDefinitionManager;
    @And("the user should be directed to Please update your contact information Page")
    public void theUserShouldBeDirectedToPleaseUpdateYourContactInformationPage() {
        stepDefinitionManager.getPageObjectManager().getPhonePromptPage().VerifyHeaderName();
    }

    @When("the user clicks on Remind me next link at Contact Information Page")
    public void theUserClicksOnRemindMeNextLinkAtContactInformationPage() {
        stepDefinitionManager.getPageObjectManager().getPhonePromptPage().clickRemindMeNextTime();
    }


    @And("the user specifies <cell_phone> for {string}")
    public void theUserSpecifiesCell_phoneForDoYouHaveACellPhone() {

    }

    @And("the user specifies (.+) for Do you have a cell phone statement")
    public void theUserSpecifiesCell_phoneForDoYouHaveACellPhoneStatement(String Value) {
        stepDefinitionManager.getPageObjectManager().getPhonePromptPage().selectRadioButtonDoYouHaveACellPhone(Value);
    }
}
