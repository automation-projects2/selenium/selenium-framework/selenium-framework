package com.usbank.test.automationFramework.trancore.pageObjects.services;

import com.usbank.test.driver.DriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.NoSuchElementException;

import static java.util.concurrent.TimeUnit.SECONDS;

public class ServicesLandingPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(ServicesLandingPage.class);
    private T driver;
    private DriverManager driverManager;
    public static int EXPLICIT_WAIT_TIME_SECONDS = 30;

    @FindBy(how = How.LINK_TEXT, using = "SERVICES")
    private WebElement linkServicesPage;

    /* added to click on manage employee link*/
    @FindBy(how = How.LINK_TEXT, using = "Manage employees")
    private WebElement linkManageEmployeepage;

    @FindBy(how = How.ID, using = "paypalTxt")
    private WebElement addToPayPalLink;

    @FindBy(how = How.XPATH, using = " //a[@title ='Add to Paypal Added']")
    private WebElement addToPayPalLinkAdded;

    @FindBy(how = How.XPATH, using = "  //button[@title ='Update my card with PayPal']")
    private WebElement updateMyCard;
//    @FindBy(how = How.XPATH, using = "  //a[@title ='Enroll in Click to pay with card']")
//    private WebElement lnkPay;

    @FindBy(how = How.XPATH, using = " //h2[text()='Control']")
    private WebElement controlLink;

    @FindBy(how = How.XPATH, using = " //a[@title ='Return to Services']")
    private WebElement returnToServicesLink;

    @FindBy(how = How.XPATH, using = "//a[@title='Services' or@title='SERVICES']")
    private WebElement servicesLink;

    @FindBy(how = How.ID, using = "paypalTxt")
    private WebElement lnkaddToPayPal;

    @FindBy(how = How.ID, using = "subHeader")
    WebElement subHeading;

    public By lnkPayPal = By.id("paypalTxt");
    public By lnkPayPalSubText = By.id("paypalSubTxt");
    @FindBy(how = How.ID, using = "paypalSubTxt")
    private WebElement lnksubTxt;

    @FindBy(how = How.TAG_NAME, using = "h1")
    private WebElement headername;

    @FindBy(how = How.LINK_TEXT, using = "Lock or unlock card")
    private WebElement linklockUnclockCard;

    @FindBy(how = How.LINK_TEXT, using = "Report card lost or stolen")
    private WebElement linkReportLostStolenCard;

    @FindBy(how = How.LINK_TEXT, using = "Order a new card")
    private WebElement linkOrderANewCard;

    @FindBy(how = How.LINK_TEXT, using = "Add authorized user")
    private WebElement linkAddAuthorizedUser;

    @FindBy(how = How.LINK_TEXT, using = "Credit limit increase")
    private WebElement linkCreditLineIncrease;

    @FindBy(how = How.LINK_TEXT, using = "Card activation")
    private WebElement linkCardActivation;

    @FindBy(how = How.XPATH, using = "//a[@title ='Enroll in Click to pay with card']")
    private WebElement enrollInClickToPayWithCard;

    @FindBy(how = How.XPATH, using = "//img[@data-testid='PayPalLogo']")
    WebElement imgPayPalLink;

    public boolean verifyEnrollInClickToPayWithCard() throws IOException {
        driver.manage().timeouts().implicitlyWait(5, SECONDS);

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,1000)");
        boolean elementPresent;
//List<WebElement> getData=driver.findElements(By.xpath("//a[@title ='Enroll in Click to pay with card']"));

        if (enrollInClickToPayWithCard.isDisplayed()) {

            logger.info("===========Enroll In Click To Pay With Card link is displayed============");
            enrollInClickToPayWithCard.click();
            elementPresent = true;


        } else {
            elementPresent = false;
            Assert.assertFalse("Enroll In Click To Pay With Card link is not displayed", elementPresent);
            logger.info("============== Enroll In Click To Pay With Card link is not displayed================");

        }
        return elementPresent;
    }

    public ServicesLandingPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    //    private CaptureScreenShots captureScreenshots ;
//    public ServicesLandingPage() {
//        captureScreenshots = new CaptureScreenShots(driverManager.getDriver());
//    }
    public void clickServicesLink() {
        linkServicesPage.click();
    }

    public void clickMangeEmployeeLink() {
        Assert.assertTrue(linkManageEmployeepage.isDisplayed());

        if (linkManageEmployeepage.isDisplayed()) {
            linkManageEmployeepage.click();
        }

    }


//        /*This will verify Add To pay Pal link presnt or not */
//    public boolean verifyAddToPayPalLink( ) throws IOException {
//
//       WebDriverWait wait = new WebDriverWait(driver,30);
//       driver.navigate().refresh();
//        JavascriptExecutor js = (JavascriptExecutor) driver;
//        js.executeScript("window.scrollBy(0,1000)");
//        boolean elementPresent ;
//        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkaddToPayPal));
//                if(driver.findElements(By.xpath("//a[@title ='Add to Paypal']")).size() != 0){
//
//                    logger.info("PASSED: Add to paypal link is present");
//                    clickElement(driver,addToPayPalLink);
//                    elementPresent = true;
//                }
//                else if (driver.findElements(By.xpath("//a[@title ='Add to Paypal Added']")).size() != 0){
//                    logger.info("PASSED: Add to paypal link Added is present with Added Status");
//                    clickElement(driver,addToPayPalLinkAdded);
//                    new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(returnToServicesLink));
//                   String heading = "There’s a problem with adding your card.";
//                   logger.info(subHeading.getText());
//                  if(!(subHeading.getText().equals(heading))) {
//                      updateMyCard.click();
//                      elementPresent = true;
//                  }
//                  else
//                      elementPresent = false;
//
//
//                }
//                else{
//                    elementPresent = false;
//                    Assert.assertFalse("PayPal link is not displayed ", elementPresent);
//                    logger.info("PASSED: Add to paypal link is not present");
//
//                }
//
//  return elementPresent;
//        }

    /*This will verify Add To pay Pal link presnt or not */
    public boolean verifyAddToPayPalLink() throws IOException {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        driver.navigate().refresh();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,1000)");
        boolean lnkPresent, subLnkPresent;
        String subLnkText;
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkaddToPayPal));
        lnkPresent = isElementPresent(lnkPayPal);
        subLnkPresent = isElementPresent(lnkPayPalSubText);
        subLnkText = lnksubTxt.getText();
        if (lnkPresent && subLnkText.trim().equals("Add to digital wallet")) {
            logger.info("PASSED: PayPal link is present with  Add to digital wallet status");
            verifyPayPalLogo();
            clickElement(driver, addToPayPalLink);
            lnkPresent = true;
        } else if (lnkPresent && subLnkText.trim().equals("Card added")) {
            logger.info("PASSED:PayPal link is present with  Card Added status ");
            verifyPayPalLogo();
            clickElement(driver, addToPayPalLink);
            new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(returnToServicesLink));
            String heading = "There’s a problem with adding your card.";
            logger.info(subHeading.getText());
            if (!(subHeading.getText().equals(heading))) {
                updateMyCard.click();
                lnkPresent = true;
            } else
                lnkPresent = false;
        } else {
            lnkPresent = false;
            Assert.assertFalse("PayPal link is not displayed ", lnkPresent);
            logger.info("PASSED: PayPal link is not present");
        }
        return lnkPresent;
    }


    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(ele));
        ele.click();
    }

//    public void clickAddToPayPalLink() {
//        if(driver.findElements(By.xpath("//a[@title ='Add to Paypal']")).size() != 0)
//        addToPayPalLink.click();
//        else {
//            logger.info("========== PayPal link with Added status  displayed =========");
//            addToPayPalLinkAdded.click();
//        }
//        WebDriverWait wait = new WebDriverWait(driver,30);
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("SERVICES")));
//        String url = driver.getCurrentUrl();
//        logger.info("========== "+url);
//
//        if(url.contains("card_added")){
//            updateMyCard.click();
//        }
//        else if (url.contains("card_not_eligible")){
//            returnToServicesLink.click();
//
//        }


    protected boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public void verifyPayPalLinkNotPresent() throws IOException {
        if (driver.findElements(By.xpath("//*[@id=\"paypalTxt\"]")).size() == 0)
            logger.info("PASSED: PayPal link is not present");
        else
            logger.error("Failed: PayPal link is present");
    }

    public void verifyPayPalLogo() {
        boolean payPalLogoPresent;
        if (imgPayPalLink.isDisplayed()) {
            payPalLogoPresent = true;
            Assert.assertTrue("PayPal link Image is present and displayed successfully", payPalLogoPresent);
            logger.info("PASSED: PayPal link Image is present and displayed successfully ");
        } else {
            payPalLogoPresent = false;
            Assert.assertFalse("Add to Samsung Pay link Image is not present ", payPalLogoPresent);
            logger.error("Failed: PayPal link Image is not present ");
        }

    }

    public void verifyHeadername() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(linkReportLostStolenCard));
        Assert.assertEquals("services", (headername.getText()).toLowerCase());
    }

    public void clickLockUnLockCard() {
        linklockUnclockCard.click();
    }

    public void clickLostStolenCard() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(linkReportLostStolenCard));
        linkReportLostStolenCard.click();
    }

    public void clickOrderANewCard() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(linkOrderANewCard));
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        linkOrderANewCard.click();
    }

    public void clickAddAuthorizedUser() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(linkAddAuthorizedUser));
        linkAddAuthorizedUser.click();
    }

    public void clickCreditLineIncrease() {
        Assert.assertTrue("Credit Limit Increase link is not displayed", linkCreditLineIncrease.isDisplayed());
        linkCreditLineIncrease.click();
    }

    public void clickCardActivation() {
        linkCardActivation.click();
    }
}
