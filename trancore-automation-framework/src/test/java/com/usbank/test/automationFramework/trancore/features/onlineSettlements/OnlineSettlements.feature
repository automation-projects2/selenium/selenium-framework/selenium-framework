Feature: Online Settlements


  Scenario Outline: User must be directed to Payment Options page and check for Online Settlements teamsite content

    Given the user who is eligible for online collections and whose account is past due logs into <partner> with <user_name> and <password>
    When the user clicks on link to learn about payment options
    Then the user must be directed to Payment Options page
    And the Online Settlements content is displayed

    Examples:
      | partner | user_name  | password    |
      | Elan    | waw9826    | wizards2020    |


  Scenario Outline: User must be directed to Payments options page when clicking on Go to payment link

    Given the user who is eligible for online collections and whose account is past due logs into <partner> with <user_name> and <password>
    When the user clicks on link to learn about payment options
    And the user must be directed to Payment Options page
    Then verify if the payment link is displayed and clicking on it goes to payment Landing page

    Examples:
      | partner | user_name  | password    |
      | Elan    | waw9826    | wizards2020    |


  Scenario Outline: User must be directed to User Enrollment page when clicking on See my settlement offer link

    Given the user who is eligible for online collections and whose account is past due logs into <partner> with <user_name> and <password>
    When the user clicks on link to learn about payment options
    Then the user must be directed to Payment Options page
    When User click on the Settlement offer link
    Then User goes to User Enrollment page
    When user clicks on Enroll button
    Then the user must be directed to Settlement enrollment confirmation page

    Examples:
      | partner | user_name  | password       |
      | Elan    | waw5580    | wizards2020    |

  Scenario Outline: User must be directed to Payment Options page when clicking on Go back to Payment Options link
    Given the user who is eligible for online collections and whose account is past due logs into <partner> with <user_name> and <password>
    When the user clicks on link to learn about payment options
    When User click on the Settlement offer link
    Then User goes to User Enrollment page
    When the user clicks on Go back to Payment Options link
    Then the user must be directed to Payment Options page

    Examples:
      | partner | user_name  | password       |
      | Elan    | waw5580    | wizards2020    |

  Scenario Outline: Checking the teamsite content if the user is enrolled in Payment Assistance

    Given the user who is eligible for online collections and whose account is past due logs into <partner> with <user_name> and <password>
    When the user clicks on link to learn about payment options
    Then the user must be directed to Payment Options page
    And the payment assistance content is displayed

    Examples:
      | partner   | user_name  | password    |
      | Elan    | waw9826    | wizards2020    |

   # ---Use Multiple Business account for this scenario----
  Scenario Outline: Qualified Business User must be directed to Payments option page from Select Account page

    Given the user who is eligible for online collections and whose account is past due logs into <partner> with <user_name> and <password>
    When the business user clicks on link to learn about payment options in Select Account page
    Then the user must be directed to Payment Options page

    Examples:

      | partner | user_name  | password    |
      | ABC    | waw0568   | wizards2020   |


    #---- Use Consumer Combined account for this scenario-----
  Scenario Outline: Qualified Consumer Combined User must be directed to Payments option page from Select Account page

    Given the user who is eligible for online collections and whose business account is past due logs into <partner> with <user_name> and <password>
    When the consumer combined user clicks on link to learn about payment options in Select Account page
    Then the user must be directed to Payment Options page

    Examples:

      | partner | user_name  | password    |
      | Elan    | waw8361   | wizards2020   |

  # use account that is not qualified with online collection
  Scenario Outline: Disqualified User must not be directed to Payments option page from Select Account page

    Given the user who is not eligible for online collections and whose account is not past due logs into <partner> with <user_name> and <password>
    When the user is on Select Account page
    Then user must not be directed to Payment Options page

    Examples:

      | partner | user_name  | password    |
      | Elan    | waw8361   | wizards2020   |

  Scenario Outline: User must be directed to Payments Options page from Payments page

    Given the user who is eligible for online collections and whose account is past due logs into <partner> with <user_name> and <password>
    When the user clicks on link to learn about payment options in Payments page
    Then the user must be directed to Payment Options page

    Examples:
      | partner | user_name  | password    |
      | Elan    | waw8361   | wizards2020   |
