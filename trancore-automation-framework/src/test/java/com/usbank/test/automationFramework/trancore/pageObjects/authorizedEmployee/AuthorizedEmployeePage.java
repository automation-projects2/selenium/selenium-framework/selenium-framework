package com.usbank.test.automationFramework.trancore.pageObjects.authorizedEmployee;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;

import static java.util.concurrent.TimeUnit.SECONDS;

public class AuthorizedEmployeePage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(AuthorizedEmployeePage.class);

    private T driver;
    public static int EXPLICIT_WAIT_TIME_SECONDS = 30;

    public AuthorizedEmployeePage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    public void verifyCreditLimitIncreaseLink() throws IOException, InterruptedException {

        driver.manage().timeouts().implicitlyWait(10, SECONDS);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        synchronized (driver) {
            driver.wait(2000);
        }
        js.executeScript("scroll(0, 500);");
        synchronized (driver) {
            driver.wait(2000);
        }
        driver.manage().timeouts().implicitlyWait(10, SECONDS);

        if (driver.findElements(By.xpath("//a[@title ='Company credit limit increase']")).size() != 0) {

            logger.info("PASSED: Credit Limit Increase link present");
        } else {
            logger.info("PASSED: Credit Limit Increase link not present");
        }
    }
}