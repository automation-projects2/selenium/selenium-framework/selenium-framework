@payPalFlow

Feature:Verify the Paypal flow functionality
  Test cases for ensuring that user is able to add card to paypal wallet

  @PresenceOfPayPalLink
  Scenario Outline:Verify user is able to complete paypal flow till contact page when the user is eligible and tenure check is passed,feature flag is ON
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user has navigated to the services page
    Then  click on Add to PayPal link if link  is present
    Then the user navigated to the Agree and Continue page
    And the user clicks on Agree and Continue button
    Then the user navigated to the contact page
    Then click on send a new code button
    Then the user navigated to the Enter OTP page
    Then the user enters the <OTP> which is send to the contact
    Then the user navigates to payPal flow with <contact> and  <PayPal Password>

    Examples:
      | partner | testData      | OTP    | contact      | PayPal Password |


  @AddToPayPalE2E
  Scenario Outline:Verify user is able to complete E2E paypal flow and user return back to Trancore from paypal
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user has navigated to the services page
    Then  click on Add to PayPal link if link  is present
    Then the user navigated to the Agree and Continue page
    And the user clicks on Agree and Continue button
    Then the user navigated to the contact page
    Then click on send a new code button
    Then the user navigated to the Enter OTP page
    Then the user enters the <OTP> which is send to the contact
    Then the user navigates to payPal flow with <contact> and  <PayPal Password>

    Examples:
      | partner | testData      | contact                           | OTP    | PayPal Password |


  @AbsenceOfPayPalLink
  Scenario Outline:Verify paypal link is not displayed  for ineligible user
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user has navigated to the services page
    Then  verify paypal link is not present for ineligible user

    Examples:
      | partner | testData |