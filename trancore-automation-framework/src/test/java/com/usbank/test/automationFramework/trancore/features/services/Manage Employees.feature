@CardmemberServicesSuite @ManageEmployees @WIP @Regression
Feature: Verify Manage Employees functionality.

  Scenario Outline: Verify that the Manage Employees script is working
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When I click Services Tab
    And I click link - Manage Employees
    And the user clicks on the Add Employee button
    And the user has landed on the Add Employee Information page
    And the user enters in the First Name <FirstName>, Middle Name  <MiddleName> , Last Name <LastName>
    And the user selects a suffix <suffix>
    And the user enters in the SSN <SSN>
    And the user enters in the DateOfBirth <DateOfBirth>
    And the user enters in the Work Phone Number <phoneNumber>
    And the user enters in a Spending Limit <spendingLimit>
    And the user clicks on the Continue button on the Add Employee Page
    Then the user verifies and Submits on the Review Employee Information page

    Examples:
      | partner | testData | FirstName | MiddleName | LastName | suffix | SSN       | DateOfBirth | phoneNumber | spendingLimit |


  @PresenceOfEditSpendingLimitLink
  Scenario Outline: Verify the Edit Spending link should be displayed for AO in status 0 and 7 on Manage Employee dashboard for All partner.
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When I click Services Tab
    And I click link - Manage Employees
    Then the Manage Employees page/dashboard opens
    And the Edit Spending Limit link displayed for AO with <status>

    Examples:
      | partner | testData | status |

  @AbsenceOfEditSpendingLimitLink
  Scenario Outline: Verify the Display limit adjustments not allowed message getting displayed when Edit Spending link not present for AO in status 1,3 and 5 on Manage Employee dashboard for All partner.
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When I click Services Tab
    And I click link - Manage Employees
    Then the Manage Employees page/dashboard opens
    And the Edit Spending Limit link not displayed for AO with <status>

    Examples:
      | partner | testData | status |
