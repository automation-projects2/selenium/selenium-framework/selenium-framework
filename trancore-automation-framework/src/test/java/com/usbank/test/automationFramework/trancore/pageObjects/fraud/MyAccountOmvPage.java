package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class MyAccountOmvPage<T extends WebDriver> extends MyAccountPage {


    public MyAccountOmvPage(T inDriver) {
        super(inDriver);
    }

    /**
     * TransactionDate and Description are required, Amount is not required
     *
     * @param transactionDate
     * @param description
     * @param amount          TBD!
     * @return
     */
    public Integer getMatchingRowIndex(String tabName, String transactionDate, String description, Double amount) {

        if (StringUtils.isEmpty(transactionDate))
            throwError("Transaction Date is null or not valid:" + transactionDate);

        if (StringUtils.isEmpty(description))
            throwError("Description is null:" + description);

        // Get table
        switch (tabName.toLowerCase()) {

            case TAB_NAME_POSTED: {
                WebElement postedTable = driver.findElement(By.id(HTML_ID_POSTED_TABLE)).findElement(By.tagName("tbody"));
                List<WebElement> postedTable_rows = postedTable.findElements(By.tagName("tr"));

                if (CollectionUtils.isEmpty(postedTable_rows)) {
                    return null;
                } else {
                    for (int index = 0; index < postedTable_rows.size(); index++) {
                        WebElement row = postedTable_rows.get(index);
                        // Find first row that matches txn date and description
                        List<WebElement> cells = row.findElements(By.tagName("td"));
                        if (cells.size() > 2) {

                            WebElement transactionDetailData = cells.get(3);
                            List<WebElement> transactionDetailElements = transactionDetailData.findElements(By.tagName("p"));
                            String txnDate = transactionDetailElements.get(0).getAttribute("innerHTML");

                            String txnDescription = transactionDetailElements.get(1).getAttribute("innerHTML");

                            //String txnDate = dateElement.getAttribute("innerHTML");

                            logger.info("txn Date  -  " + txnDate + "; transaction Description: " + txnDescription);
                            if (txnDate.equalsIgnoreCase(transactionDate)) {

                                if (txnDescription.trim().equalsIgnoreCase(description.trim())) {
                                    logger.info("found matching transaction!");
                                    return index;
                                }
                            }
                        }

                    }

                }
                break;
            }
            case TAB_NAME_PENDING:
                break;
            case TAB_NAME_RECURRING:
                break;

            default: {
                throwError("Not Yet Implemented:" + tabName.toLowerCase());
            }
        }

        // no match
        return null;
    }



    public Integer expandTransactionDetails(String tabName, String transactionDate, String description) {
        Integer index = getMatchingRowIndex(tabName, transactionDate, description);
        logger.info("Matching Row Index: " + index);
        // found match
        if (index != null && index.intValue() > -1) {
            WebElement postedTable = driver.findElement(By.id(HTML_ID_POSTED_TABLE)).findElement(By.tagName("tbody"));
            List<WebElement> postedTable_rows = postedTable.findElements(By.tagName("tr"));

            // get hidden moreDescDetails_row to get click links
            List<WebElement> cells = postedTable_rows.get(index).findElements(By.tagName("td"));
            WebElement transactionDetailData = cells.get(3);
            List<WebElement> transactionDetailElements = transactionDetailData.findElements(By.tagName("p"));
            WebElement txnDate = transactionDetailElements.get(0);
            txnDate.click();
        }
        return index;
    }


    public Integer clickDisputeTransactionForTxnDateAndDescription(String tabName, String transactionDate,
                                                                      String description) {

        Integer index = expandTransactionDetails(tabName, transactionDate, description);

        driver.switchTo().activeElement();



//        if (index != null && index.intValue() > -1) {
//            WebElement postedTable = driver.findElement(By.id(HTML_ID_POSTED_TABLE)).findElement(By.tagName("tbody"));
//            List<WebElement> postedTable_rows = postedTable.findElements(By.tagName("tr"));
//
//            // get hidden moreDescDetails_row to get click links
//            WebElement detailsRow = postedTable_rows.get(index + 1);
//            WebElement lnkDisputeTransaction = detailsRow.findElement(By.tagName("a"));
//            lnkDisputeTransaction.click();
//        }
        return index;
    }

}
