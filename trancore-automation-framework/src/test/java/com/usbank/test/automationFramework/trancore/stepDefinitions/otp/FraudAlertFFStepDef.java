package com.usbank.test.automationFramework.trancore.stepDefinitions.otp;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class FraudAlertFFStepDef {
    @Autowired
    StepDefinitionManager stepDefinitionManager;

    @When("customer click on Alerts")
    public void customer_click_on_Alerts() {
        stepDefinitionManager.getPageObjectManager().getWebNavigationPage().clickAlertsLink();
    }

    @When("customer clicks on Fraud Alerts")
    public void customer_clicks_on_Fraud_Alerts() {
        // Write code here that turns the phrase above into concrete actions

        stepDefinitionManager.getPageObjectManager().getAlertsLandingPage().clickFraudAlertTab();
    }

    @When("the customer clicks the Update Contact information link")
    public void the_customer_clicks_the_Update_Contact_information_link() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getAlertsLandingPage().clickUpdateContactInfo();
    }


    @Then("the customer should return to Alerts HomeTab")
    public void the_customer_should_return_to_Alerts_HomeTab() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getAlertsLandingPage().verifyHeaderName();
    }

}
