package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TravelPage<T extends WebDriver> {

    protected static final Logger logger = LogManager.getLogger(TravelPage.class);

    protected T driver;


    public TravelPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "5035-Y")
    private WebElement rdbYes;

    @FindBy(id = "5035-N")
    private WebElement rdbNo;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement btnContinue;

    @FindBy(xpath = "//*[text()='Were you traveling?']")
    private WebElement lblTravel;

    @FindBy(id = "5036")
    private WebElement txtArea;

    @FindBy(id = "5037")
    private WebElement tripStartDate;

    @FindBy(id = "5038")
    private WebElement tripEndDate;

    public WebElement getTripStartDate() {
        return tripStartDate;
    }

    public WebElement getTripEndDate() {
        return tripEndDate;
    }

    public WebElement getTxtArea() {
        return txtArea;
    }

    public WebElement getLblTravel() {
        return lblTravel;
    }

    public WebElement getRdbYes() {
        return rdbYes;
    }

    public void waitForTravelPageToLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(getRdbYes()));
    }

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        ele.click();
    }

    public void clickNoRadioButton() {
        waitForTravelPageToLoad();
        clickElement(driver, rdbNo);
    }

    public void clickContinue() {
        clickElement(driver, btnContinue);
    }
}
