package com.usbank.test.automationFramework.trancore.pageObjects.payments;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class ShortTermFundingAccountPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(ShortTermFundingAccountPage.class);
    private T driver;
    private int EXPLICIT_WAIT_TIME_SECONDS = 30;

    @FindBy(how = How.ID, using = "selectFundingAccountHeader")
    private WebElement selectFundingAccountHeading;

    @FindBy(how = How.ID, using = "agree-terms-button")
    private WebElement submitPlanButton;

    @FindBy(how = How.XPATH, using = "//fieldset[@id='fundingAccountFieldSet']//child::div")
    private List<WebElement> fundingAccountRadioDivs;

    @FindBy(how = How.XPATH, using = "//fieldset[@id='accountTypeFieldSet']//child::div")
    private List<WebElement> accountTypeRadioDivs;

    @FindBy(how = How.NAME, using = "routingNumber")
    private WebElement routingNumberInput;

    @FindBy(how = How.NAME, using = "accountNumber")
    private WebElement accountNumberInput;

    @FindBy(how = How.ID, using = "DD--text")
    private WebElement checkingAccountRadioText;

    @FindBy(how = How.ID, using = "SV--text")
    private WebElement savingsAccountRadioText;

    @FindBy(how = How.XPATH, using = "//div[@id='addFundingDisclaimer']/p")
    private WebElement addFundingDisclaimerParagraph;

    @FindBy(how = How.XPATH, using = "//div[@id='pymtOptionsContactParagraph']/p")
    private WebElement paymentOptionsContactParagraph;

    @FindBy(how = How.XPATH, using = "//div[@id='enrollmentDisclaimerParagraph']/p/strong")
    private WebElement enrollmentDisclaimerParagraph;

    @FindBy(how=How.ID, using = "importantNoteParagraph")
    private WebElement importantNoteParagraph;


    public ShortTermFundingAccountPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public boolean isSelectFundingAccountHeadingDisplayed() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(selectFundingAccountHeading));

        WebDriverWait wait = new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS);
        wait.until(ExpectedConditions.visibilityOf(selectFundingAccountHeading));
        String headingText = selectFundingAccountHeading.getText();
        if (headingText.equals("Select your funding account."))
            return true;

        return false;
    }

    public boolean isAddFundingAccountHeadingDisplayed() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(selectFundingAccountHeading));

        WebDriverWait wait = new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS);
        wait.until(ExpectedConditions.visibilityOf(selectFundingAccountHeading));
        String headingText = selectFundingAccountHeading.getText();
        if (headingText.equals("Add your funding account."))
            return true;

        return false;
    }

    public int getExplicitWaitTimeSeconds() {
        return EXPLICIT_WAIT_TIME_SECONDS;
    }


    public WebElement getSubmitPlanButton() {
        return submitPlanButton;
    }

    public WebElement getRoutingNumberInput() {
        return routingNumberInput;
    }

    public WebElement getAccountNumberInput() {
        return accountNumberInput;
    }

    public WebElement getCheckingAccountRadioText() {
        return checkingAccountRadioText;
    }

    public WebElement getSavingsAccountRadioText() {
        return savingsAccountRadioText;
    }

    public List<WebElement> getFundingAccountRadioDivs() {
        return fundingAccountRadioDivs;
    }

    public List<WebElement> getAccountTypeRadioDivs() {
        return accountTypeRadioDivs;
    }

    public WebElement getAddFundingDisclaimerParagraph() {
        return addFundingDisclaimerParagraph;
    }

    public WebElement getPaymentOptionsContactParagraph() {
        return paymentOptionsContactParagraph;
    }

    public WebElement getEnrollmentDisclaimerParagraph() {
        return enrollmentDisclaimerParagraph;
    }

    public WebElement getSelectFundingAccountHeading() {
        return selectFundingAccountHeading;
    }

    public WebElement getImportantNoteParagraph() {
        return importantNoteParagraph;
    }
}
