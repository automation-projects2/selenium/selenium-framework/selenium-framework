package com.usbank.test.automationFramework.trancore.pageObjects.payments;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HardshipQuestionnaire1Page <T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(HardshipQuestionnaire1Page.class);
    private T driver;
    private WebDriverWait wait;

    @FindBy(xpath = "//h1[@data-test='revampquestionnaire-page-header']")
    private WebElement pageHeading;

    @FindBy(xpath = "//*[contains(text(),'experiencing economic hardship')]")
    private WebElement ifYouAreExperiencingParagraph;

    @FindBy(xpath = "//*[contains(text(),'Before we start, please')]")
    private WebElement lengthOfHardshipQuestion;

    @FindBy(id = "radiolengthOfHardship1--text")
    private WebElement radioOptionLessThan4Months;

    @FindBy(id = "lengthOfHardship2")
    private WebElement radioOption4to12Months;

    @FindBy(id = "lengthOfHardship3")
    private WebElement radioOption1to3Years;

    @FindBy(id = "lengthOfHardship4")
    private WebElement radioOption3to5Years;

    @FindBy(name = "Continue")
    private WebElement btnContinue;

    @FindBy(id = "cancelLink")
    private WebElement cancelLink;

    @FindBy(id="input_currentMonthlyIncome")
    private WebElement currentMonthlyIncome;

    @FindBy(id="input_currentMonthlyExpenses")
    private WebElement currentMonthlyExpenses;

    @FindBy(id="select-hardshipReason")
    private WebElement hardshipReason;

    public HardshipQuestionnaire1Page(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 30);
    }


    public boolean verifyQuestionnaire1PageTitle() {
        wait.until(ExpectedConditions.visibilityOf(pageHeading));
        logger.info("Questionnaire1 page title is as expected: " + driver.getTitle());
        return !driver.getTitle().equals("Credit Card Account Access: Length of hardship");
    }

    public boolean verifyQuestionnaire1PageHeaderDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(pageHeading));
        logger.info("Questionnaire1 page header displayed: " + pageHeading.getText());
        return pageHeading.getText().equals("We want to help.");
    }

    public boolean verifyTheParagraphStartingWIthIfYouAreExperiencingIsDisplayed(){
        logger.info("paragraph verbiage is displayed: " + ifYouAreExperiencingParagraph.getText());
        return ifYouAreExperiencingParagraph.getText().equals("If you're experiencing economic hardship, we have payment plans to help get you back on track.");
    }

    public boolean verifyQuestionnaire1LengthOfHardshipQuestionDisplayed(){
        logger.info("Length Of Hardship Question is displayed: " + lengthOfHardshipQuestion.getText());
        return lengthOfHardshipQuestion.getText().equals("Before we start, please tell us how long you expect your hardship to last.");
    }

    public boolean verifyLengthOfHardshipFirstRadioButtonDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(radioOptionLessThan4Months));
        logger.info("Length Of Hardship - First Radio Button is displayed: " + radioOptionLessThan4Months.getText());
        return radioOptionLessThan4Months.getText().equals("Less than 4 months");
    }

    public boolean verifyLengthOfHardshipSecondRadioButtonDisplayed(){
        logger.info("Length Of Hardship - Second Radio Button is displayed: " + radioOption4to12Months.getText());
        return radioOption4to12Months.getText().equals("4 to 12 months");
    }

    public boolean verifyLengthOfHardshipThirdRadioButtonDisplayed(){
        logger.info("Length Of Hardship - Third Radio Button is displayed: " + radioOption1to3Years.getText());
        return radioOption1to3Years.getText().equals("1 to 3 years");
    }

    public boolean verifyLengthOfHardshipFourthRadioButtonDisplayed(){
        logger.info("Length Of Hardship - Fourth Radio Button is displayed: " + radioOption3to5Years.getText());
        return radioOption3to5Years.getText().equals("3 to 5 years");
    }

    public boolean verifyContinueButtonDisplayed(){
        logger.info("Continue Button is displayed: " + btnContinue.getText());
        return btnContinue.getText().equals("Continue");
    }

    public boolean verifyCancelLinkDisplayed(){
        logger.info("Cancel Link is displayed: " + cancelLink.getText());
        return cancelLink.getText().equals("Cancel");
    }

    public WebElement getLengthOfHardshipFirstRadioButton(){ return radioOptionLessThan4Months;   }
    public WebElement getLengthOfHardshipSecondRadioButton(){
        return radioOption4to12Months;
    }
    public WebElement getLengthOfHardshipThirdRadioButton(){
        return radioOption1to3Years;
    }
    public WebElement getLengthOfHardshipFourthRadioButton(){
        return radioOption3to5Years;
    }
    public WebElement getContinueButton(){     return btnContinue;    }
    public WebElement getCancelLink(){
        return cancelLink;
    }

    public WebElement getPageHeading() {
        return pageHeading;
    }

    public WebElement getCurrentMonthlyIncome() {
        return currentMonthlyIncome;
    }

    public WebElement getCurrentMonthlyExpenses() {
        return currentMonthlyExpenses;
    }

    public WebElement getHardshipReason() {
        return hardshipReason;
    }

}
