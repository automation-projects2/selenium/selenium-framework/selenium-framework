package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OtherTransactionsPage<T extends WebDriver> {

    protected static final Logger logger = LogManager.getLogger(OtherTransactionsPage.class);

    protected T driver;


    public OtherTransactionsPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "radioNo")
    private WebElement rdbNo;

    @FindBy(id = "radioYes")
    private WebElement rdbYes;

    @FindBy(id = "radioReview")
    private WebElement rdbReview;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement btnContinue;

    @FindBy(xpath = "//*[text()='Account activity']")
    private WebElement lblAccActivity;

    public WebElement getRdbYes() {
        return rdbYes;
    }

    public WebElement getLblAccActivity() {
        return lblAccActivity;
    }

    public void waitForOtherTransactionsPageToLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(lblAccActivity));
        wait.until(ExpectedConditions.elementToBeClickable(getRdbYes()));
    }

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        ele.click();
    }

    public void clickFirstRadioButton() {
        waitForOtherTransactionsPageToLoad();
        clickElement(driver, rdbNo);
    }

    public void clickThirdRadioButton() {
        waitForOtherTransactionsPageToLoad();
        clickElement(driver, rdbReview);
    }

    public void clickSecondradioButton() {
        waitForOtherTransactionsPageToLoad();
        clickElement(driver, rdbYes);
    }

    public void clickContinue() {
        waitForOtherTransactionsPageToLoad();
        clickElement(driver, btnContinue);
    }

    public void clickRadioNo() {
        waitForOtherTransactionsPageToLoad();
        clickElement(driver, rdbNo);
    }
}
