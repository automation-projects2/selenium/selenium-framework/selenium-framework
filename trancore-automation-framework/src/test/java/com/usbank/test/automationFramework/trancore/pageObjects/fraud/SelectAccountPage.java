package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SelectAccountPage<T extends WebDriver> {
    protected static final Logger logger = LogManager.getLogger(SelectAccountPage.class);

    protected static final String HTML_CLASS_ACCOUNT_TABLE = "tc_Table marginTop10px";
    protected static final int INDEX_CELL_ACCOUNT = 0;


    protected T driver;


    public SelectAccountPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@id = 'subHeader']/span")
    private WebElement lblManageAccounts;

    @FindBy(xpath = "//a[@href='/onlineCard/managePayment.do?phase=start']")
    private WebElement linkPaymentPage;

    @FindBy(xpath = "//a[@data-testid='paymentSolutionsLink']")
    private WebElement linkLearnAbout;

    @FindBy(xpath = "//*[@class='vcp_welcomeUser_message']")
    private WebElement accNumber;

    @FindBy(xpath = "//table[@class='tc_Table marginTop10px']/tbody")
    private WebElement accTable;

    @FindBy(xpath = "//*[@id='mgInformation-children-notification']/div/span[1]")
    private WebElement messageGranularityHeader;

    @FindBy(xpath = "//*[@id='mgInformation-children-notification']/div/span[2]")
    private WebElement messageGranularityBody;

    @FindBy(id = "mgInformation--close-btn")
    private WebElement messageGranularityBtn;

    @FindBy(xpath = "//*[@id='mgError-children-notification']/div/span[1]")
    private WebElement messageGranularityErrorHeader;

    @FindBy(xpath = "//*[@id='mgError-children-notification']/div/p/span")
    private WebElement messageGranularityErrorBody;

    @FindBy(xpath = "//*[@id='mgError-children-notification']/div/div/a")
    private WebElement messageGranularityPaymentLink;

    @FindBy(xpath = "//*[@id='mgError-children-notification']/div/div/a[2]")
    private WebElement messageGranularityPaymentOptionsLink;

    @FindBy(xpath = "//*[@id='mgError-children-notification']/div/div/a[2]")
    private WebElement messageGranularityChargeOffLink;

    @FindBy(xpath = "//*[@id='mgError-children-notification']/div/p/a")
    private WebElement messageGranularityInLineChargeOffLink;

    @FindBy(xpath = "//a[contains(text(),'800-944-2706')]")
    private WebElement phoneNumberLink;


    @FindBy(xpath = "//a[contains(text(),'Learn about charge-offs')]")
    private WebElement chargeOffLink;

    public void waitForSelectAccountPageToLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(lblManageAccounts));
    }

    public void clickPaymentTab() {
        linkPaymentPage.click();
    }

    public WebElement getAccTable(){
        return accTable;

    }

    public WebElement getMessageGranularityErrorHeader() {return messageGranularityErrorHeader;}

    public WebElement getMessageGranularityErrorBody() { return messageGranularityErrorBody;}


    public void clickLinkLearnAboutPaymentSolutions() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(linkLearnAbout));
        linkLearnAbout.click();
    }

    public WebElement getHeadingManageAccounts() {
        return lblManageAccounts;

    }
    public WebElement getAccNumber()
    {
        return accNumber;
    }


    public void clickOnLinkForPastDueForBusinessAccounts() {

        //fetch the account number
        String accHolderName = accNumber.getText();

        String valueOfAccNumber = accHolderName.substring(accHolderName.lastIndexOf(" ") + 1, accHolderName.length() - 0);
        logger.info("The last 4 digit of account number for the logged in user is :" + valueOfAccNumber);
        // find the link from the table for the logged in user account number
        WebElement accountTable = driver.findElement(By.xpath("//table[@class='tc_Table marginTop10px']")).findElement(By.tagName("tbody"));

        List<WebElement> accountTable_rows = accountTable.findElements(By.tagName("tr"));
        logger.info("The size of row is" + accountTable_rows.size());
        for (int i = 0; i < accountTable_rows.size(); i++) {
            List<WebElement> rowInfo = accountTable_rows.get(i).findElements(By.tagName("td"));

            String accNo = rowInfo.get(1).getText();
            logger.info("The account number in the table " + accNo);
            if (valueOfAccNumber.equals(accNo)) {
                WebElement link = rowInfo.get(i).findElement(By.xpath("//*[@data-testid='PaymentSolutionsLink']"));

                if (link.isDisplayed()) {
                    link.click();
                    break;
                } else
                    continue;
            }

        }
    }

    public void clickOnLinkForPastDueForConsumerCombAccounts() {

        //fetch the account number
        String accHolderName = accNumber.getText();

        String valueOfAccNumber = accHolderName.substring(accHolderName.lastIndexOf(" ") + 1, accHolderName.length() - 0);
        logger.info("The last 4 digit of account number for the logged in user is :" + valueOfAccNumber);
        // find the link from the table for the logged in user account number
        WebElement accountTable = driver.findElement(By.xpath("//table[@class='tc_Table marginTop10px']")).findElement(By.tagName("tbody"));

        List<WebElement> accountTable_rows = accountTable.findElements(By.tagName("tr"));

        for (int i = 0; i < accountTable_rows.size(); i++) {
            List<WebElement> rowInfo = accountTable_rows.get(i).findElements(By.tagName("td"));
            String accNo = rowInfo.get(0).findElement(By.tagName("a")).getText();
            logger.info("The account number in the table " + accNo);
            if (valueOfAccNumber.equals(accNo)) {
                WebElement link = rowInfo.get(0).findElement(By.linkText("Learn about our Payment Options"));

                if (link.isDisplayed()) {
                    link.click();
                    break;
                } else
                    logger.info("The account is past due yet no link displayed");
                continue;
            }

            break;
        }
    }

    public void checkForNoLinkDisplay() {
        WebElement accountTable = driver.findElement(By.xpath("//table[@class='tc_Table marginTop10px']")).findElement(By.tagName("tbody"));

        List<WebElement> accountTable_rows = accountTable.findElements(By.tagName("tr"));

        for (int i = 0; i < accountTable_rows.size(); i++) {
            WebElement link = accountTable_rows.get(i).findElement(By.linkText("Learn about our payment solutions"));

            if (link.isDisplayed()) {
                logger.info("The account is not eligible but still link is displayed");
                break;
            } else
                logger.info("The account is past due and not eligible and so there is no link displayed");
            break;
        }
    }
}



