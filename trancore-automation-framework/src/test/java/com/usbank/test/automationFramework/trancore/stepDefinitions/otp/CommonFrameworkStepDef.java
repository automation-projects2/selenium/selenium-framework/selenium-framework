package com.usbank.test.automationFramework.trancore.stepDefinitions.otp;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;


public class CommonFrameworkStepDef {
    @Autowired
    StepDefinitionManager stepDefinitionManager;

    //Navigation steps relates to OTP

    @Then("the customer clicks on Logout Button")
    public void the_customer_clicks_on_Logout_Button() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getWebNavigationPage().clickLogoutButton();
    }

    @Then("the customer should be directed to Login Page")
    public void the_customer_should_be_directed_to_Login_Page() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getLoginPage().verifyLogOutConfirmation();
    }

    @Given("the customer clicks on Order a new card at Service Tab")
    public void the_customer_clicks_on_Order_a_new_card_at_Service_Tab() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getServicesLandingPage().clickOrderANewCard();
    }

    @Given("the customer should select the Reason (.+) to Order a new Card")
    public void the_customer_should_select_the_Reason_to_Order_a_new_Card(String Reason) {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getOrderANewCardPage().SelectReasonOrderANewCard(Reason);
    }

    @Then("the customer should be directed to Card Delivery Page")
    public void the_customer_should_be_directed_to_Card_Delivery_Page() {
        // Write code here that turns the phrase above into concrete actions
        //stepDefinitionManager.getPageObjectManager().getCardDeliveryPage().verifyHeaderName();
    }

    @And("the customer should be directed to Order a New Card Page")
    public void theCustomerShouldBeDirectedToOrderANewCardPage() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getOrderANewCardPage().verifyHeaderName();
    }

    @Given("the customer clicks on Continue at Order A New Card Page")
    public void the_customer_clicks_on_Continue_at_Order_A_New_Card_Page() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getOrderANewCardPage().ClickContinue();
    }

    @Then("the customer should be directed to Card Replacement Select User Page")
    public void theCustomerShouldBeDirectedToCardReplacementSelectUserPage() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getCardReplacementSelectUsersPage().verifyHeaderName();
    }
    
    @Then("the customer should be directed to Card Replacement Delivery Options Page")
    public void theCustomerShouldBeDirectedToCardReplacementDeliveryOptionsPage() {
        // Write code here that turns the phrase above into concrete actions
        //stepDefinitionManager.getPageObjectManager().getCardDeliveryPage().verifyHeaderName();
    }
    @And("the customer answers sheild question")
    public void theCustomerAnswersSheildQuestion() {
        stepDefinitionManager.getPageObjectManager().getLoginPage().completeStepUpCheck();
    }




    @When("^the customer select the user (.+) for card replacement$")
    public void the_customer_select_the_user_for_card_replacement(String user){
    	stepDefinitionManager.getPageObjectManager().getCardReplacementSelectUsersPage().selectUser(user);
    }

    @When("the customer clicks on Continue at select user Page")
    public void the_customer_clicks_on_Continue_at_select_user_Page() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getCardReplacementSelectUsersPage().continueButtonClick();
    }
    @When("the customer clicks on cancel button in card delivery Page")
    public void the_customer_clicks_on_Cancel_at_card_delivery_Page() {
        // Write code here that turns the phrase above into concrete actions
       // stepDefinitionManager.getPageObjectManager().getCardDeliveryPage().cancelButtonClick();
    }
    
    @When("the customer selected yes i Leave button in card delivery Page")
    public void the_customer_selected_yes_i_Leave_button_in_card_delivery_Page() {
        // Write code here that turns the phrase above into concrete actions
       // stepDefinitionManager.getPageObjectManager().getCardDeliveryPage().ClickYesILeaveButton();
    }
    @When("the customer select standard delivery")
    public void the_customer_select_standard_delivery() {
        // Write code here that turns the phrase above into concrete actions
        //stepDefinitionManager.getPageObjectManager().getCardDeliveryPage().ClickStandardDeliveryOption();
    }
    @When("the customer clicks on Continue at card delivery Page")
    public void the_customer_clicks_on_Continue_at_card_delivery_Page() {
    	// stepDefinitionManager.getPageObjectManager().getCardDeliveryPage().continueButtonClick();
    }
    @When("customer should be directed to Card replacement review page")
    public void customer_should_be_directed_to_Card_replacement_review_page() {
    	 stepDefinitionManager.getPageObjectManager().getReviewandSubmitCardReplacementOrderpage().verifyHeaderName();
    }
    
    @When("the customer clicks on Submit order button")
    public void the_customer_clicks_on_Submit_order_button() {
    	 stepDefinitionManager.getPageObjectManager().getReviewandSubmitCardReplacementOrderpage().clickSubmitOrder();
    }
    @Then("customer should be directed to Card replacement confirmation page")
    public void customer_should_be_directed_to_Card_replacement_confirmation_page() {
    	 stepDefinitionManager.getPageObjectManager().getReviewandSubmitCardReplacementOrderpage().verifyConfirmationHeaderName();
    }
    @When("the customer clicks on cancel button")
    public void the_customer_clicks_on_cancel_button() {
    	 stepDefinitionManager.getPageObjectManager().getReviewandSubmitCardReplacementOrderpage().cancelButtonClick();
    }
  //  @Given("^the user has (.+) username and (.+) password$")
    //public void the_user_has_account_status(String username, String password) {
      //  stepDefinitionManager.getPageObjectManager().login(username, password);
    //}




}
