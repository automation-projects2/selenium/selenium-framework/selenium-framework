package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ReviewTransactionsPage<T extends WebDriver> {

    protected static final Logger logger = LogManager.getLogger(ReviewTransactionsPage.class);

    protected T driver;


    public ReviewTransactionsPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "radioYes")
    private WebElement rdbYes;

    @FindBy(id = "radioNo")
    private WebElement rdbNo;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement btnContinue;

    @FindBy(xpath = "//*[text()='Review other transactions']")
    private WebElement lblReviewOtherTransactions;

    public WebElement getRdbYes() {
        return rdbYes;
    }

    public WebElement getReviewOtherTransactionsLabel() {
        return lblReviewOtherTransactions;
    }

    public void waitForReviewTransactionsToLoad() {

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(getRdbYes()));
    }

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        ele.click();
    }

    public void clickYesRadioBtn() {
        waitForReviewTransactionsToLoad();
        clickElement(driver, rdbYes);
        clickElement(driver, btnContinue);
    }

    public void clickNoRadioBtn() {
        waitForReviewTransactionsToLoad();
        clickElement(driver, rdbNo);
        clickElement(driver, btnContinue);
    }
}
