@Login
Feature: Income Prompt

      
  Scenario Outline: Providing income in Income prompt

    Given the user has navigated to the <partner> login page
    And the user has logged with <user_name> username and <password> password
    When Provide <income> as income, <income_source> as income source and click on submit button
    Then the user has landed in My Accounts page

    Examples:
      | partner | user_name  | password |income|income_source|

Scenario Outline: By clicking on remind me later in Income prompt

    Given the user has navigated to the <partner> login page
    And the user has logged with <user_name> username and <password> password
    When the user selected remind me later
    Then the user has landed in My Accounts page

    Examples:
      | partner | user_name  | password |

Scenario Outline: Validation of Error messages on clicking on submit without providing income details

    Given the user has navigated to the <partner> login page
    And the user has logged with <user_name> username and <password> password
    When the user selected submit button without providing income details
    Then Error messages for single user should be displayed

    Examples:
      | partner | user_name  | password |

Scenario Outline: Validation of Error message on providing only income

    Given the user has navigated to the <partner> login page
    And the user has logged with <user_name> username and <password> password
    When the user provided <income> as income and click on submit button
    Then Error messages for single user should be displayed

    Examples:
      | partner | user_name  | password |income|

Scenario Outline: Validation of Error message on providing only income source

    Given the user has navigated to the <partner> login page
    And the user has logged with <user_name> username and <password> password
    When the user provided <income_source> as income source and click on submit button
    Then Error messages for single user should be displayed

    Examples:
      | partner | user_name  | password |income_source|
