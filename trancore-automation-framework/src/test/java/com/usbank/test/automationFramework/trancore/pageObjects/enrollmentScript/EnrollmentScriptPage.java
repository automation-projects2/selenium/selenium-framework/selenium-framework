package com.usbank.test.automationFramework.trancore.pageObjects.enrollmentScript;


import com.usbank.test.automationFramework.trancore.pageObjects.services.BalanceTransferPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Arrays;

public class EnrollmentScriptPage<T extends WebDriver> {

    private T driver;
    public static int EXPLICIT_WAIT_TIME_SECONDS = 30;
    private static final Logger logger = LogManager.getLogger(EnrollmentScriptPage.class);

    public EnrollmentScriptPage(T inDriver) {

        driver = inDriver;
        PageFactory.initElements(driver, this);

    }

    /*<---------------Enrollment Page Objects--------------->*/

    @FindAll({
            @FindBy(how = How.NAME, using = "cardNumber"),
            @FindBy(how = How.ID, using = "cardnumber"),
            @FindBy(how = How.ID, using = "cardNumber")
    })
    private WebElement txtCreditCardAccountNumber;

    @FindAll({
            @FindBy(how = How.NAME, using = "signaturePanelCode"),
            @FindBy(how = How.ID, using = "signaturePanelCode-tel-input")
    })
    private WebElement txtCVV;

    @FindAll({
            @FindBy(how = How.NAME, using = "ssn"),
            @FindBy(how = How.ID, using = "ssn-tel-input")
    })
    private WebElement txtSSN;

    @FindAll({
            @FindBy(how = How.ID, using = "zipCodeRadio")
    })
    private WebElement rdoZipCode; //zipcode radio

    @FindAll({
            @FindBy(how = How.NAME, using = "zipCode")
    })
    private WebElement txtZipCode; //zipcode input field

    @FindAll({
            @FindBy(how = How.ID, using = "pinNumberRadio")
    })
    private WebElement rdoPinNumber; //PinNumber radio

    @FindAll({
            @FindBy(how = How.NAME, using = "pinNumber")
    })
    private WebElement txtPinNumber; //PinNumber input field

    @FindAll({
            @FindBy(how = How.NAME, using = "loginId1"),
            @FindBy(how = How.ID, using = "loginId1")
    })
    private WebElement txtPersonalId;

    @FindAll({
            @FindBy(how = How.NAME, using = "loginId2"),
            @FindBy(how = How.ID, using = "loginId2")
    })
    private WebElement txtPersonalId2;

    @FindAll({
            @FindBy(how = How.NAME, using = "password1"),
            @FindBy(how = How.ID, using = "password1")
    })
    private WebElement txtPassword;

    @FindAll({
            @FindBy(how = How.NAME, using = "password2"),
            @FindBy(how = How.ID, using = "password2")
    })
    private WebElement txtPassword2;

    @FindAll({
            @FindBy(how = How.NAME, using = "emailAddress1"),
            @FindBy(how = How.ID, using = "emailAddress1")
    })
    private WebElement txtEmail;

    @FindAll({
            @FindBy(how = How.NAME, using = "emailAddress2"),
            @FindBy(how = How.ID, using = "emailAddress2")
    })
    private WebElement txtEmail2;


    @FindBy(how = How.XPATH, using = "//span[(@role ='link') and (text() ='Cancel')]")
    private WebElement btnCancel;

    @FindAll({
            @FindBy(how = How.NAME, using = "Submit"),
            @FindBy(how = How.ID, using = "enrollFormSubmit")
    })
    private WebElement btnSubmit;

    @FindAll({
            @FindBy(how = How.XPATH, using = "//span[contains(text(),'ENROLL')]"),
            @FindBy(how = How.XPATH, using = "//span[contains(text(),'Enroll')]")
    })
    private WebElement btnEnroll;

    /*<---------------E-Sign Page Objects--------------->*/

    @FindBy(how = How.ID, using = "esignLink")
    private WebElement lnkESignConsentAgreement;

    @FindBy(how = How.ID, using = "closewindow")
    private WebElement btnCloseWindow;

    @FindBy(how = How.ID, using = "IAgreeCheckbox")
    private WebElement ckbIAgreeCheckbox;

    @FindBy(how = How.NAME, using = "CONTINUE")
    private WebElement btnContinue;

    /*<---------------ID Shield Page Objects--------------->*/

    @FindBy(how = How.NAME, using = "Set Up ID Shield")
    private WebElement btnSetUpIDShield;

    @FindBy(how = How.LINK_TEXT, using = "Show More Questions")
    private WebElement lnkShowMoreQuestions;


    @FindBy(how = How.ID, using = "alwaysAskMeToAnwser")
    private WebElement rbAlwaysAskMeToAnswer;

    @FindBy(how = How.ID, using = "next")
    private WebElement btnNext;

    @FindBy(how = How.NAME, using = "Next")
    private WebElement btnNameNext;

    @FindAll({
            @FindBy(how = How.ID, using = "security-shield-choice1")
    })
    private WebElement rbImageButton;

    @FindBy(how = How.ID, using = "imagePhrase")
    private WebElement txtIDShieldImageAndSoundPhrase;

    @FindBy(how = How.ID, using = "next")
    private WebElement btnNextButtonOnStep2of4IDShieldPage;

    @FindBy(how = How.XPATH, using = "//button[@name='Submit']")
    private WebElement btnSubmitOnStep3Of4IDShieldPage;

    @FindBy(how = How.XPATH, using = "//button[@name='Go To Transaction Details']")
    private WebElement btnGoToTransactionDetails;

    @FindBy(how = How.XPATH, using = "//li[@id='error']")
    private WebElement errorText;

    @FindBy(how = How.XPATH, using = "(//div[@class='environmentInfo'])[2]")
    private WebElement errorCode;

    @FindBy(how = How.XPATH, using = "(//div[@id='cardnumber']//label)")
    private WebElement lblCreditCardNumber;

    @FindBy(how = How.XPATH, using = "(//p[@message='Please enter your valid 16-digit credit card number.'])")
    private WebElement errorMessageCreditCardNumber;

    @FindBy(how = How.XPATH, using = "(//label[@id='signaturePanelCode_lbl']//span)")
    private WebElement lblSecurityCode;

    @FindBy(how = How.XPATH, using = "(//p[@message='Please enter your valid 3-digit security code.'])")
    private WebElement errorMessageSecurityCode;

    @FindBy(how = How.XPATH, using = "(//div[@id='ssn-tel-input']//label)")
    private WebElement lblSsn;

    @FindBy(how = How.XPATH, using = "(//p[@message='Please enter the last 4 digits of your SSN.'])")
    private WebElement errorMessageSsn;

    @FindBy(how = How.XPATH, using = "(//p[@message='The SSN you entered is invalid. Please try again.'])")
    private WebElement errorMessageInvalidSsn;

    @FindBy(how = How.XPATH, using = "(//p[@message ='Please enter your ZIP Code.'])")
    private WebElement errorMessageEmptyZipcode;

    @FindBy(how = How.XPATH, using = "(//div[@id='zipcode']//label)")
    private WebElement lblZipCode;

    @FindBy(how = How.XPATH, using = "(//p[@message='Your ZIP code must be 5 digits.'])")
    private WebElement errorMessageZipCode;

    @FindBy(how = How.XPATH, using = "(//p[@message='Please enter a valid 5-digit ZIP code using only numbers. '])")
    private WebElement errorMessageZeroZipCode;

    @FindBy(how = How.XPATH, using = "(//p[@message='Your ZIP code must be numeric.'])")
    private WebElement errorMessageNonNumericZipCode;

    @FindBy(how = How.XPATH, using = "(//label[@id='fourDigitPin'])")
    private WebElement lblFourDigitPin;

    @FindBy(how = How.XPATH, using = "(//p[@message='Please enter a valid 4-digit PIN using only numbers.'])")
    private WebElement errorMessageEmptyPinNumber;

    @FindBy(how = How.XPATH, using = "//div[@id='loginid1']//label")
    private WebElement lblPersonalId;

    @FindBy(how = How.XPATH, using = "(//p[@message='Please enter your Personal ID.'])")
    private WebElement errorMessageEmptyPersonalId;

    @FindBy(how = How.XPATH, using = "(//p[contains(@message,'Your Personal ID is too short. Please enter a different Personal ID.')])")
    private WebElement errorMessageShortPersonalId;

    @FindBy(how = How.XPATH, using = "//p[@message='Your Personal ID is too long. Please enter a different Personal ID.']")
    private WebElement errorMessageLongPersonalId;

    @FindBy(how = How.XPATH, using = "(//p[contains(@message,'#. Please enter a different Personal ID.')])")
    private WebElement errorMessageHashPersonalId;

    @FindBy(how = How.XPATH, using = "//p[contains(@message,'underscore _ or contain more than one underscore in a row. Please enter a different Personal ID.')]")
    private WebElement errorMessageUnderScorePersonalId;

    @FindBy(how = How.XPATH, using = "//p[@message='Please re-enter your Personal ID.']")
    private WebElement errorMessageEmptyPersonalId2;

    @FindBy(how = How.XPATH, using = "//div[@id='loginid2']//label")
    private WebElement lblPersonalId2;

    @FindBy(how = How.XPATH, using = "//p[contains(@message,'match. Please confirm your Personal ID and try again.')]")
    private WebElement errorMessagePersonalId2;

    @FindBy(how = How.XPATH, using = "//div[@id='password1']//label")
    private WebElement lblPassword;

    @FindBy(how = How.XPATH, using = "//div[@id='password2']//label")
    private WebElement lblPassword2;

    @FindBy(how = How.XPATH, using = "//p[@message='Please enter your password.']")
    private WebElement errorMessageEmptyPassword;

    @FindBy(how = How.XPATH, using = "(//p[contains(@message,'Your password is too short. Please enter a different password.')])")
    private WebElement errorMessageShortPassword;

    @FindBy(how = How.XPATH, using = "'//p[@message='Your password is too long. Please enter a different password.']")
    private WebElement errorMessageLongPassword;

    @FindBy(how = How.XPATH, using = "(//p[@message='Your password is missing required characters. Please try again.'])")
    private WebElement errorMessageNumberPassword;

    @FindBy(how = How.XPATH, using = "//p[@message='Please re-enter your password.']")
    private WebElement errorMessageEmptyPassword2;

    @FindBy(how = How.XPATH, using = "//p[contains(@message,'match. Please confirm your password and try again.')]")
    private WebElement errorMessagePassword2;

    @FindBy(how = How.XPATH, using = "//div[@id='emailaddress1']//label")
    private WebElement lblemailaddress1;

    @FindBy(how = How.XPATH, using = "//div[@id='emailaddress2']//label")
    private WebElement lblemailaddress2;

    @FindBy(how = How.XPATH, using = "//p[@message='Please enter your email address.']")
    private WebElement errorMessageEmptyEmailAddress;

    @FindBy(how = How.XPATH, using = "//p[@message='Please re-enter your email address.']")
    private WebElement errorMessageEmptyEmailAddress2;

    @FindBy(how = How.XPATH, using = "//p[@message='Your email address and re-enter email address do not match.']")
    private WebElement errorMessageEmailAddress2;


    /*---------------Enrollment Page---------------*/
    public void navigateToEnrollment() {

        //driver.get(baseUrl + "/onlineCard/publicEnrollUser.do");

        //Will need to have a WAIT call for Enroll webpage to load
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(btnEnroll));
        btnEnroll.click();
    }

    public void populateEnrollForm(String creditCardNumber, String cvvNumber, String ssnNUmber, String zipCodeOrPinNumber, String PinFlag, String personalID, String password, String emailAddress) throws InterruptedException {
        /*
        PingFlag =0 => ZipCode Selected
        PingFlag =1 => Pin Number Selected
         */
        txtCreditCardAccountNumber.sendKeys(creditCardNumber);
        txtCVV.sendKeys(cvvNumber);
        txtSSN.sendKeys(ssnNUmber);
        Thread.sleep(1000);
        if (PinFlag.equalsIgnoreCase("1")) {
            rdoPinNumber.click();
            txtPinNumber.sendKeys(zipCodeOrPinNumber);
        } else if (PinFlag.equalsIgnoreCase("0")) {
            rdoZipCode.click();
            txtZipCode.sendKeys(zipCodeOrPinNumber);
        }
        Thread.sleep(1000);
        txtPersonalId.sendKeys(personalID);
        txtPersonalId2.sendKeys(personalID);
        txtPassword.sendKeys(password);
        txtPassword2.sendKeys(password);
        txtEmail.sendKeys(emailAddress);
        txtEmail2.sendKeys(emailAddress);

    }

    public void submitEnrollmentForm() throws InterruptedException {
        btnSubmit.click();
        Thread.sleep(3000);

        try {
            boolean k = errorText.isDisplayed();
            if (k) {
                System.out.println(errorCode.getText());
            }

        } catch (NoSuchElementException e) {
            System.out.println("No Error, Continue with Enrollment");
        }

    }

    /*---------------Esign Page---------------*/
    public void clickOnViewEsignConsentAgreement() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(lnkESignConsentAgreement));
        lnkESignConsentAgreement.click();
        driver.switchTo().activeElement();
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 500);");
        btnCloseWindow.click();
        ckbIAgreeCheckbox.click();
        btnContinue.click();

    }

    ////span[normalize-space()='Enroll in ID Shield']
    //name="Set Up ID Shield" button name
    public void setupIDShield() {
        //WebDriverWait wait = new WebDriverWait(driver, 30);
        //wait.until(ExpectedConditions.elementToBeClickable(btnSetUpIDShield));
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnSetUpIDShield));
        btnSetUpIDShield.click();
    }

    //name="Next" button next
    public void inputIDShieldQuestionsInEnrolIDShieldPageTwo() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnNext));
        lnkShowMoreQuestions.click();
        clickOnCheckBoxAndCaptureSecurityQuestions();
        rbAlwaysAskMeToAnswer.click();
        btnNext.click();
    }

    //wait for name="Next" next button
    public void inputImageAndPhrase(String password) {
        //String phrase = "testing123";
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnNameNext));
        rbImageButton.click();
        txtIDShieldImageAndSoundPhrase.click();
        txtIDShieldImageAndSoundPhrase.sendKeys(password);
        btnNextButtonOnStep2of4IDShieldPage.click();
    }

    //name="Submit"  wait for submit button
    public void submitImageAndPhrase() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnSubmitOnStep3Of4IDShieldPage));
        btnSubmitOnStep3Of4IDShieldPage.click();
    }

    //wait for button name name="Go To Transaction Details"
    public void clickTransactionDetailsButton() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnGoToTransactionDetails));
        btnGoToTransactionDetails.click();
    }

    private void clickOnCheckBoxAndCaptureSecurityQuestions() {

        int j = 0;//counter to check the number of answers inputted
        for (int i = 0; i < 10; i++) {
            String storeSecurityQuestion = driver.findElement(By.xpath("//*[@id='questionText" + i + "']")).getText();
            String lastWordOfSecurityQuestion = storeSecurityQuestion.substring(storeSecurityQuestion.lastIndexOf(" ") + 1, storeSecurityQuestion.length() - 1);

            String[] ques = new String[]{"address", "born", "id", "card", "number", "married", "birth", "anniversary", "license", "up", "college", "school", "high-school", "child"};
            boolean result = Arrays.stream(ques).anyMatch(lastWordOfSecurityQuestion::equals);
            if (result) {
                System.out.println("Question is invalid");
            } else {
                if (j <= 2) {
                    driver.findElement(By.xpath("//*[@id='checkbox" + i + "']")).click();
                    driver.findElement(By.xpath("//*[@id='answer0x" + i + "']")).click();
                    driver.findElement(By.xpath("//*[@id='answer0x" + i + "']")).sendKeys(lastWordOfSecurityQuestion);
                    System.out.println("Question " + i + ": " + storeSecurityQuestion);
                    j++;
                } else {
                    System.out.println("Loop Break");
                    break;
                }
            }

        }
    }

    public void checkEnrollmentError() {

        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(errorText));

        String errorString = errorText.getText();
        String actualError = "We are unable to verify the information you've entered. Please review your entry or call Technical Support at 1-877-410-1758 for assistance.";
        Assert.assertEquals(errorString, actualError);
        System.out.println(errorCode.getText());

    }

    public String verifyFieldLevelErrorInCreditCardNumber(String creditCardNumber) {

        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(txtCreditCardAccountNumber));
        String actualError = "Please enter your valid 16-digit credit card number.";
//        if(!(creditCardNumber.matches("^[0-9]+$"))){
        txtCreditCardAccountNumber.click();
        lblCreditCardNumber.click();
        String emptyErrorString = errorMessageCreditCardNumber.getText();
        Assert.assertEquals(emptyErrorString, actualError);
        txtCreditCardAccountNumber.sendKeys(creditCardNumber);
        lblCreditCardNumber.click();
        String errorString = errorMessageCreditCardNumber.getText();
        Assert.assertEquals(errorString, actualError);
        return errorString;
    }

    public String verifyFieldLevelErrorInSecurityCode(String cvv) {
        String actualError = "Please enter your valid 3-digit security code.";
        txtCVV.click();
        lblSecurityCode.click();
        String emptyErrorString = errorMessageSecurityCode.getText();
        Assert.assertEquals(emptyErrorString, actualError);
        txtCVV.sendKeys(cvv);
        lblSecurityCode.click();
        String errorString = errorMessageSecurityCode.getText();
        Assert.assertEquals(errorString, actualError);
        return errorString;
    }

    public void displayErrorMessage(String errorMessage) {
        System.out.println("Error message '" + errorMessage + "' displayed successfully");
    }

    public String verifyFieldLevelErrorInSsn(String ssn) {
        String errorString = null;
        Integer count = ssn.length();
        String errorMessage = "Please enter the last 4 digits of your SSN.";
        txtSSN.click();
        lblSsn.click();
        String emptyErrorString = errorMessageSsn.getText();
        Assert.assertEquals(emptyErrorString, errorMessage);
        System.out.println("Error message '" + emptyErrorString + "' displayed successfully");
        if (count < 4 && ssn.matches("^[0-9]+$")) {
            txtSSN.sendKeys(ssn);
            lblSsn.click();
            errorString = errorMessageSsn.getText();
            Assert.assertEquals(errorString, errorMessage);
        } else {
            String actualError = "The SSN you entered is invalid. Please try again.";
            txtSSN.sendKeys(ssn);
            lblSsn.click();
            errorString = errorMessageInvalidSsn.getText();
            Assert.assertEquals(errorString, actualError);
        }
        return errorString;
    }

    public String selectVerificationMethod(String pinFlag) {

        if (pinFlag.equalsIgnoreCase("0")) {
            WebDriverWait wait = new WebDriverWait(driver, 20);
            wait.until(ExpectedConditions.elementToBeClickable(rdoZipCode));
            rdoZipCode.click();
            System.out.println("Zip code is selected as a method of verification");

        } else if (pinFlag.equalsIgnoreCase("1")) {
            WebDriverWait wait = new WebDriverWait(driver, 20);
            wait.until(ExpectedConditions.elementToBeClickable(rdoPinNumber));
            rdoPinNumber.click();
            System.out.println("4 Digit Pin number  is selected as a method of verification");

        }
        return pinFlag;
    }

    public String verifyFieldLevelErrorInZipCode(String pinFlag, String zipCode) {
        String errorString = null;
        txtZipCode.click();
        lblZipCode.click();
        String actualError = "Please enter your ZIP Code.";
        String emptyErrorString = errorMessageEmptyZipcode.getText();
        Assert.assertEquals(actualError, emptyErrorString);
        System.out.println("Error message '" + emptyErrorString + "' displayed successfully");

        Integer count = zipCode.length();
        if (count < 5) {
            txtZipCode.sendKeys(zipCode);
            lblZipCode.click();
            errorString = errorMessageZipCode.getText();
            actualError = "Your ZIP code must be 5 digits.";
            Assert.assertEquals(actualError, errorString);

        } else if (count == 5 && zipCode.equals("00000")) {
            System.out.println("Entered");
            txtZipCode.sendKeys(zipCode);
            lblZipCode.click();
            actualError = "Please enter a valid 5-digit ZIP code using only numbers.";
            errorString = errorMessageZeroZipCode.getText();
            Assert.assertEquals(actualError, errorString);
        } else if (count == 5 && !(zipCode.matches(("^[0-9]+$")))) {
            txtZipCode.sendKeys(zipCode);
            lblZipCode.click();
            actualError = "Your ZIP code must be numeric.";
            errorString = errorMessageNonNumericZipCode.getText();
            Assert.assertEquals(actualError, errorString);
        }

        return errorString;

    }

    public String verifyFieldLevelErrorInPinNumber(String pinFlag, String pinNumber) {
        txtPinNumber.click();
        lblFourDigitPin.click();
        String actualError = "Please enter a valid 4-digit PIN using only numbers.";
        String errorString = errorMessageEmptyPinNumber.getText();
        Assert.assertEquals(actualError, errorString);
        txtPinNumber.sendKeys(pinNumber);
        lblFourDigitPin.click();
        errorString = errorMessageEmptyPinNumber.getText();
        Assert.assertEquals(actualError, errorString);
        return errorString;
    }

    public String verifyFieldLevelErrorInPersonalId(String personalId) {
        txtPersonalId.click();
        lblPersonalId.click();
        String actualError = "Please enter your Personal ID.";
        String errorString = errorMessageEmptyPersonalId.getText();
        Assert.assertEquals(actualError, errorString);
        System.out.println("Error message '" + errorString + "' is displayed ");
        Integer length = personalId.length();
        if (length < 7) {
            txtPersonalId.sendKeys(personalId);
            lblPersonalId.click();
            actualError = "Your Personal ID is too short. Please enter a different Personal ID.";
            errorString = errorMessageShortPersonalId.getText();
            Assert.assertEquals(actualError, errorString);
        } else if (length > 22) {
            txtPersonalId.sendKeys(personalId);
            lblPersonalId.click();
            actualError = "Your Personal ID is too long. Please enter a different Personal ID.";
            errorString = errorMessageLongPersonalId.getText();
            Assert.assertEquals(actualError, errorString);
        } else if (personalId.startsWith("#")) {
            txtPersonalId.sendKeys(personalId);
            lblPersonalId.click();
            actualError = "Personal ID can't begin with #. Please enter a different Personal ID.";
            errorString = errorMessageHashPersonalId.getText();
            Assert.assertEquals(actualError, errorString);
        } else if (personalId.startsWith("_")) {
            txtPersonalId.sendKeys(personalId);
            lblPersonalId.click();
            actualError = "Your Personal ID can't begin or end with an underscore _ or contain more than one underscore in a row. Please enter a different Personal ID.";
            errorString = errorMessageUnderScorePersonalId.getText();
            Assert.assertEquals(actualError, errorString);
        }
        return errorString;
    }

    public String verifyFieldLevelErrorInReEnterPersonalId(String personalId) {
        txtPersonalId2.click();
        lblPersonalId2.click();
        String actualError = "Please re-enter your Personal ID.";
        String errorString = errorMessageEmptyPersonalId2.getText();
        Assert.assertEquals(actualError, errorString);
        System.out.println("Error message '" + errorString + "' is displayed ");
        txtPersonalId2.sendKeys(personalId);
        lblPersonalId2.click();
        String actualPersonalId = txtPersonalId.getAttribute("value");
        System.out.println("" + actualPersonalId);
        if (personalId.equals(actualPersonalId)) {
            System.out.println("Personal Id entered correctly");
        } else {
            actualError = "The Personal IDs you entered don't match. Please confirm your Personal ID and try again.";
            errorString = errorMessagePersonalId2.getText();
            Assert.assertEquals(actualError, errorString);

        }
        return errorString;

    }

    public String verifyFieldLevelErrorInPassword(String password) {
        txtPassword.click();
        lblPassword.click();
        String errorString = errorMessageEmptyPassword.getText();
        String actualError = "Please enter your password.";
        Assert.assertEquals(errorString, actualError);
        System.out.println("Error message '" + errorString + "' is displayed ");
        Integer length = password.length();
        if (length < 8) {
            txtPassword.sendKeys(password);
            lblPassword.click();
            actualError = "Your password is too short. Please enter a different password.";
            errorString = errorMessageShortPassword.getText();
            Assert.assertEquals(actualError, errorString);
        } else if (length > 24) {
            txtPassword.sendKeys(password);
            lblPassword.click();
            actualError = "Your password is too long. Please enter a different password.";
            errorString = errorMessageLongPassword.getText();
            Assert.assertEquals(actualError, errorString);
        } else if ((password.matches(("^[0-9]+$"))) || password.matches("^[a-zA-Z]*$")) {
            txtPassword.sendKeys(password);
            lblPassword.click();
            actualError = "Your password is missing required characters. Please try again.";
            errorString = errorMessageNumberPassword.getText();
            Assert.assertEquals(actualError, errorString);
        }

        return errorString;
    }

    public String verifyFieldLevelErrorInConfirmPassword(String password) {
        txtPassword2.click();
        lblPassword2.click();
        String actualError = "Please re-enter your password.";
        String errorString = errorMessageEmptyPassword2.getText();
        Assert.assertEquals(actualError, errorString);

        txtPassword2.sendKeys(password);
        lblPassword2.click();
        String actualPassword = txtPassword.getAttribute("value");
        if (password.equals(actualPassword)) {
            System.out.println("Password correctly");
        } else {
            actualError = "The passwords you entered don't match. Please confirm your password and try again.";
            errorString = errorMessagePassword2.getText();
            Assert.assertEquals(actualError, errorString);

        }
        return errorString;

    }

    public String verifyFieldLevelErrorInEmail(String emailAddress) {
        txtEmail.click();
        lblemailaddress1.click();
        String errorString = null;
        //need to get the correct error message
//        String errorString = errorMessageEmptyEmailAddress.getText();
//        String actualError = "";
//        Assert.assertEquals(errorString.actualError);
        txtEmail.sendKeys(emailAddress);
//need to get confirmation regarding the validation
//        actualError  =".";
//        errorString = errorMessagePassword2.getText();
//        Assert.assertEquals(actualError,errorString);
        return errorString;
    }

    public String verifyFieldLevelErrorInReEnterEmail(String emailAddress) {
        txtEmail2.click();
        lblemailaddress2.click();
        String errorString = errorMessageEmptyEmailAddress2.getText();
        String actualError = "Please re-enter your email address.";
        Assert.assertEquals(errorString, actualError);
        System.out.println("Error message '" + errorString + "' is displayed ");
        txtEmail2.sendKeys(emailAddress);
        lblemailaddress2.click();
        String actualEmail = txtEmail.getAttribute("value");
        if (emailAddress.equals(actualEmail)) {
            System.out.println("Email entered correctly");
        } else {
            actualError = "Your email address and re-enter email address do not match.";
            errorString = errorMessageEmailAddress2.getText();
            Assert.assertEquals(actualError, errorString);

        }
        return errorString;
    }

    public void verifyTransactionDetailsButton() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnGoToTransactionDetails));
        Assert.assertTrue(btnGoToTransactionDetails.isDisplayed());
        logger.info("Transaction details button displayed");
    }
}
