package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TransactionDetailsModel<T extends WebDriver> {

    protected static final Logger logger = LogManager.getLogger(TransactionDetailsModel.class);

    protected T driver;


    public TransactionDetailsModel(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    @FindBy(linkText = "Dispute this transaction")
    private WebElement lnkDisputeTransaction;

    public WebElement getLnkDisputeTransaction() {

        return lnkDisputeTransaction;
    }

    public void waitForModelToLoad() {

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(getLnkDisputeTransaction()));
    }

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        ele.click();
    }

    public void clickLnkDisputeTransaction() {
        waitForModelToLoad();
        clickElement(driver, lnkDisputeTransaction);
    }
}
