package com.usbank.test.automationFramework.trancore.stepDefinitions;

import com.usbank.test.automationFramework.trancore.pageObjects.PageObjectManager;
import com.usbank.test.component.TestDataManager;
import com.usbank.test.enums.BrowserType;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * https://groups.google.com/forum/#!topic/cukes/ke7MhnjqQGQ
 * Not allowed to extend step definition class so have to inject features
 */

public class StepDefinitionManager {
    public Scenario localScenario=null;
    private static final Logger logger = LogManager.getLogger(StepDefinitionManager.class);
    //counter variable to keep track of session if session completed or not
    public static Integer sessionCounter = 0;

    @Autowired
    private PageObjectManager pageObjectManager;

    @Autowired
    private TestDataManager testDataManager;

    @Autowired
    private AutomationConfig automationConfig;

    @Before
    public void setup(Scenario scenario) {
        logger.info("***************");
        logger.info("Starting Scenario");
        printScenarioInfo(scenario);
        //Incrementing counter everytime when new flow / session starts
        sessionCounter++;
        pageObjectManager.initPageObjectDriver(RunMode.valueOf(automationConfig.getRunMode()), BrowserType.valueOf(automationConfig.getBrowserType()));
        localScenario = scenario;
    }

    public void captureScreenshot() {
            byte[] screenShot = ((TakesScreenshot) pageObjectManager.getDriver()).getScreenshotAs(OutputType.BYTES);
            localScenario.embed(screenShot, "image/png");
    }
    public void captureFullPageScreenshotsWithScrolling() {
//        byte[] screenShot = ((TakesScreenshot) pageObjectManager.getDriver()).getScreenshotAs(OutputType.BYTES);
        WebDriver driver=pageObjectManager.getDriver();
        JavascriptExecutor jsExec = (JavascriptExecutor)driver;
        //Returns a Long, Representing the Height of the window’s content area.
        Long windowHeight = (Long) jsExec.executeScript("return window.innerHeight;");
        //Returns a Long, Representing the Height of the complete WebPage a.k.a. HTML document.
        Long webpageHeight = (Long) jsExec.executeScript("return document.body.scrollHeight;");
        //Marker to keep track of the current position of the scroll point
        //Long currentWindowScroll = Long.valueOf(0);
        //Using java's boxing feature to create a Long object from native long value.
        Long currentWindowScroll = 0L;
        do{
            jsExec.executeScript("window.scrollTo(0, " + currentWindowScroll + ");");
            Actions act = new Actions(driver);
            act.pause(5000).perform();
            byte[] screenShot = ((TakesScreenshot) pageObjectManager.getDriver()).getScreenshotAs(OutputType.BYTES);
            localScenario.embed(screenShot, "image/png");
            currentWindowScroll = currentWindowScroll + windowHeight;
        }while(currentWindowScroll <= webpageHeight);
    }
    @After
    public void cleanup(Scenario scenario) {
        /*added the code embed screenshot in html for pass or failed scenario's*/
        System.out.println(scenario);
        try {
            if (scenario.isFailed()) {
                logger.info(scenario.getName() + " is failed");
                byte[] screenShot = ((TakesScreenshot) pageObjectManager.getDriver()).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenShot, "image/png");
            } else {
                logger.info(scenario.getName() + " is passed");
                byte[] screenShot = ((TakesScreenshot) pageObjectManager.getDriver()).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenShot, "image/png");

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        pageObjectManager.doLogOut();
        logger.info("Ending Scenario");
        logger.info("***************");

//        final byte[] screenshot = ((TakesScreenshot) pageObjectManager.getDriver()).getScreenshotAs(OutputType.BYTES);
//        scenario.embed(screenshot, "image/png");
        pageObjectManager.getDriver().quit();

    }

    private void printScenarioInfo(Scenario scenario) {
        logger.info("Scenario Name: " + scenario.getName());
        logger.info("Scenario Id: " + scenario.getId());
    }

    public PageObjectManager getPageObjectManager() {
        return pageObjectManager;
    }

    public TestDataManager getTestDataManager() {
        return testDataManager;
    }

    public AutomationConfig getAutomationConfig() {
        return automationConfig;
    }

    public void log(String message) {
        logger.info(message);
    }


}
