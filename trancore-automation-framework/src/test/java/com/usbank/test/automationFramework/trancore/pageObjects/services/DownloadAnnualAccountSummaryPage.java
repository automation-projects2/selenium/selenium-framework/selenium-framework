package com.usbank.test.automationFramework.trancore.pageObjects.services;

import com.usbank.test.component.SpringConfiguration;
import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Logger;

import static java.util.Objects.requireNonNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfiguration.class)
public class DownloadAnnualAccountSummaryPage<T extends WebDriver> {

    private static final long EXPLICIT_WAIT_TIME_SECONDS = 20;
    @FindBy(how = How.LINK_TEXT, using = "Download annual account summary")
    private WebElement lnkDownloadAnnualAccountSummary;

    @FindBy(how = How.XPATH, using = "//select[@id='format']")
    private WebElement comboBoxFormat;

    @FindBy(how = How.ID, using = "year")
    private WebElement comboBoxYear;

    @FindBy(how = How.XPATH, using = "//button[@class='download action btn btn-primary pull-right mb-20']")
    private WebElement btnDownload;

    @FindBy(how = How.XPATH, using = "//a[@class='return action btn btn-primary']")
    private WebElement btnBackToServices;

    @FindBy(how = How.LINK_TEXT, using = "Back to Services")
    private WebElement linkBackToServices;

    @FindBy(how = How.LINK_TEXT, using = "SpendAnalysis")
    private WebElement linkSpendAnalysis;

    private T driver;

    private static Logger log = Logger.getLogger(DownloadAnnualAccountSummaryPage.class.getName());


    private AutomationConfig automationConfig;

    public DownloadAnnualAccountSummaryPage(T inDriver, AutomationConfig automationConfig) {
        driver = inDriver;
        this.automationConfig = automationConfig;
        PageFactory.initElements(driver, this);
    }
    /*public BrowserType getBrowserType() {
        return this.driverManager.getBrowserType();
    }*/

    public void clickLinkDownloadAnnualAccountSummary() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkDownloadAnnualAccountSummary));
        lnkDownloadAnnualAccountSummary.click();
    }

    public WebElement getLnkDownloadAnnualAccountSummary() {
        return lnkDownloadAnnualAccountSummary;
    }

    public void viewAndDownloadAnnualAccountSummary() throws Exception {

        Thread.sleep(8000);
        Select format = new Select(comboBoxFormat);
        format.selectByIndex(0);
        Select year = new Select(comboBoxYear);
        year.selectByVisibleText("2018");
        btnDownload.click();
        Thread.sleep(2000);
        //System.out.println(driverManager.getBrowserType());
        String b = automationConfig.getBrowserType();
        System.out.println(b + " this is the current browser");
        //BrowserType b = BrowserType.valueOf(automationConfig.getBrowserType());
        if (b.equals("CHROME")) {
            System.out.println("This will execute for GC");
            waitForDownloadToComplete();
        }
        if (b.equals("FIREFOX")) {
            System.out.println("FF");
            waitForDownloadToComplete();
        }

        if (b.equals("IE")) {
            System.out.println("THIS is IE");
        }

       /* switch (b) {
            case FIREFOX:
                System.out.println("FF");
                break;
            case IE:
                System.out.println("IE");
                break;
            case EDGE:
                System.out.println("EDGE");
                break;
            case CHROME:
                System.out.println("This will execute for GC");
                waitForDownloadToComplete();
                break;
        }*/

    }

    public WebElement getBtnDownload() {
        return btnDownload;
    }

    public WebElement getComboBoxYear() {
        return comboBoxYear;
    }

    public WebElement getComboBoxFormat() {
        return comboBoxFormat;
    }

    public void waitForDownloadToComplete() {
        String currentWindow = driver.getWindowHandle();
        String fileName = System.getProperty("user.home");
        String userDownloads = new String(fileName + "/Downloads/");
        System.out.println("Path below has the download account summary file");
        System.out.println(userDownloads);
        File downloadDir = new File(userDownloads);
        File[] downloadDirFiles = downloadDir.listFiles();
        Arrays.sort(requireNonNull(downloadDirFiles), LastModifiedFileComparator.LASTMODIFIED_REVERSE);
        System.out.println("\nLast Modified Ascending Order (LASTMODIFIED_REVERSE)");
        String dFile = requireNonNull(downloadDirFiles)[0].getName();
        System.out.println(dFile);
        driver.switchTo().window(currentWindow);

    }

    public static void displayFiles(File[] downloadDirFiles) {
        for (File file : downloadDirFiles) {
            System.out.printf("File: %-20s Last Modified:" + new Date(file.lastModified()) + "\n", file.getName());
        }
    }

    public void clickLinkBackToServices() {
        linkBackToServices.click();
    }

    public WebElement getLinkBackToServices() {
        return linkBackToServices;
    }

    public void clickButtonBackToServices() {
        btnBackToServices.click();
    }

    public WebElement getBtnBackToServices() {
        return btnBackToServices;
    }

    public void clickLinkSpendAnalysis() {
        linkSpendAnalysis.click();
    }

    public WebElement getLinkSpendAnalysis() {
        return linkSpendAnalysis;
    }
}





