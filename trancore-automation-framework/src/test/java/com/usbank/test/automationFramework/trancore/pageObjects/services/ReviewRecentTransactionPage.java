package com.usbank.test.automationFramework.trancore.pageObjects.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

public class ReviewRecentTransactionPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(ReviewRecentTransactionPage.class);
    private static final long EXPLICIT_WAIT_TIME_SECONDS = 30;
    private T driver;

    @FindBy(how = How.TAG_NAME, using = "h2")
    private WebElement headername;

    @FindBy(xpath = "//h2/span")
    private WebElement reviewRecentheader;

    @FindBy(how = How.ID, using = "reviewRecentContinue")
    private WebElement linkContinue;

    @FindBy(how = How.ID, using = "reviewRecentCancel")
    private WebElement linkCancel;

    @FindBy(how = How.CLASS_NAME, using = "usb-button button--secondary button--default ")
    private WebElement linkYesLeave;

    @FindBy(how = How.ID, using = "radioButton0")
    private WebElement radiobuttonYes;

    @FindBy(how = How.ID, using = "radioButton1")
    private WebElement radiobuttonNo;

    @FindBy(how = How.NAME, using = "Continue")
    private WebElement buttonPopupContinue;

    @FindBy(how = How.CLASS_NAME, using = "errortext ")
    private WebElement errortext;

    public ReviewRecentTransactionPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }


    public void verifyHeaderName() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(linkContinue));
        assertEquals("review recent transactions", (headername.getText()).toLowerCase());
    }

    public void verifyHeaderNameLS() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(reviewRecentheader));
        assertEquals("review recent transactions", (reviewRecentheader.getText()).toLowerCase());
    }

    public void ClickContinue() {
        //To Scroll down before clicking Continue
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,350)", "");
        linkContinue.click();


    }

    public void clickPopupContinue() {

        buttonPopupContinue.click();


    }

    public void ClickCancel() {
        linkCancel.click();
        linkYesLeave.click();

    }

    public void selectRadioButtonYes() {
        radiobuttonYes.click();
    }

    public void selectRadioButtonNo() {
        radiobuttonNo.click();
    }

    public void verifyErrortext() {

        assertEquals("Please make a selection before continuing.", (errortext.getText()));
    }


}

