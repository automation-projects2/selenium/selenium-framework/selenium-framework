package com.usbank.test.automationFramework.trancore.pageObjects.contactUsForHelp;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class ContactUsForHelpPage<T extends WebDriver> {


    private static final long EXPLICIT_WAIT_TIME_SECONDS = 30;

    private T driver;

    @FindBy(how = How.ID, using = "subHeader")
    private WebElement headerName;

    @FindBy(how = How.TAG_NAME, using = "p")
    private WebElement ServiceMsg;

    @FindBy(how = How.CLASS_NAME, using = "tieringElement maxPageWidth")
    private WebElement OtpMaxMsg;

    @FindBy(how = How.CLASS_NAME, using = "lockOrUnlock-button-margin")
    private WebElement linkReturnTo1;

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Return to ")
    private WebElement linkReturnTo;

    public ContactUsForHelpPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    public ContactUsForHelpPage() {
        // TODO Auto-generated constructor stub
    }

    public void VerifyHeaderName() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(linkReturnTo1));

        assertEquals("contact us for help", (headerName.getText()).toLowerCase());
    }

    public void verifytextServiceCall() {
        assertEquals("There's a problem on our end. Please give us a call at 1-888-551-5144 and one of our customer service representatives will be happy to help.", getText());

    }

    public void pageRefresh() {
        driver.navigate().refresh();
    }

    public String getText() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(linkReturnTo1));
        List<WebElement> textMessage = driver.findElements(By.className("tieringElement"));
        String textMsgContactUs = (textMessage.get(0).getText());
        textMsgContactUs = textMsgContactUs.split("Return")[0];
        textMsgContactUs = textMsgContactUs.replaceAll("[0-9]", "");
        textMsgContactUs = textMsgContactUs.replaceAll("-", "");
        System.out.println(textMsgContactUs);
        return textMsgContactUs;
    }

    public void verifytextOTPMax() {

        assertEquals("Hmm, it looks like this account requires some extra attention.\n" +
                "Please give us a call at  and one of our customer service representatives will be happy to help.\n", getText());


    }

    public void clickReturnTo() {
        linkReturnTo.click();
    }

}
