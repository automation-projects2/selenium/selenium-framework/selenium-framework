package com.usbank.test.automationFramework.trancore.pageObjects.profile;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class ContactinfoPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(ContactinfoPage.class);
    private static final long EXPLICIT_WAIT_TIME_SECONDS = 30;
    private T driver;

    @FindBy(how = How.TAG_NAME, using = "h1")
    private WebElement headerName;

    @FindBy(how = How.LINK_TEXT, using = "Update mailing address")
    private WebElement linkUpdatemailingaddress;

    @FindBy(how = How.LINK_TEXT, using = "Add email address")
    private WebElement linkAddEmailAddress;

    @FindBy(how = How.LINK_TEXT, using = "Add phone number")
    private WebElement linkAddPhoneNumber;

    @FindBy(how = How.XPATH, using = "//a[text()='Add email address']")
    private WebElement linkAddEmail;


    @FindBy(how = How.XPATH, using = "//input[@id='emailAddress']")
    private WebElement enterEmailAddress;


    @FindBy(how = How.XPATH, using = "//button[text()='Add email address']")
    private WebElement addEmailAddressButton;


    @FindBy(how = How.XPATH, using = "//h1[text()='Contact information']/following::span[text()]")
    private WebElement addEmailSuccessmessage;

    @FindBy(how = How.XPATH, using = "//li[@class='rte-email-address'][2]//following::a[@name='edit']")
    private WebElement linkEditEmail;

    @FindBy(how = How.XPATH, using = "//li[@class='rte-email-address'][2]//following::a[@name='remove']")
    private WebElement linkRemoveEmail;

    @FindBy(how = How.XPATH, using = "//button[text()='Update my email address']")
    private WebElement updateEmailAddressButton;

    @FindBy(how = How.XPATH, using = "//input[@id='newEmailAddress']")
    private WebElement enterNewEmailAddress;


    @FindBy(how = How.XPATH, using = "//button[text()='No, keep']")
    private WebElement noKeepButton;

    @FindBy(how = How.XPATH, using = "//button[text()='Yes, remove']")
    private WebElement yesRemoveButton;

    @FindBy(how = How.XPATH, using = "//button[text()='Cancel']")
    private WebElement cancelButton;

    @FindBy(how = How.XPATH, using = "//h1[@id='cancelModalHeading']//following::button[contains(text(),'No')]")
    private WebElement noDontLeaveButton;


    @FindBy(how = How.XPATH, using = "//h2/span[text()='Phone numbers']/following::a[text()='Add phone number']")
    private WebElement addPhoneNumberLink;

    @FindBy(how = How.XPATH, using = "//input[@id='phoneNumber']")
    private WebElement inputPhoneNumber;

    @FindBy(how = How.XPATH, using = "//button[text()='Add new phone number']")
    private WebElement addNewPhoneNumberButton;


    @FindBy(how = How.XPATH, using = "//h1[text()='Contact information']/following::p")
    private WebElement addPhoneSuccessMessage;

    @FindBy(how = How.XPATH, using = "//input[@id='newPhoneNumber']")
    private WebElement inputUpdatedPhone;

    @FindBy(how = How.XPATH, using = "//button[text()='Update my phone number']")
    private WebElement updatePhoneButton;

    @FindBy(how = How.XPATH, using = "//h2/span[text()='Mailing address']/following::a[text()='Update mailing address']")
    private WebElement updateMailingAddressLink;

    @FindBy(how = How.XPATH, using = "//input[@id='mailingAddressLine1']")
    private WebElement addressLine1;

    @FindBy(how = How.XPATH, using = "//input[@id='mailingApartmentUnitNum']")
    private WebElement addressLine2;

    @FindBy(how = How.XPATH, using = "//input[@id='mailingCity']")
    private WebElement city;


    @FindBy(how = How.XPATH, using = "//input[@id='mailingZip']")
    private WebElement zipCode;

    @FindBy(how = How.XPATH, using = "//button[text()='Update mailing address']")
    private WebElement updateMailButton;

    @FindBy(how = How.XPATH, using = "//input[@id='mailingCheckbox']")
    private WebElement mailingCheckbox;

    @FindBy(how = How.XPATH, using = "//button[text()='Use the address I gave you']")
    private WebElement useAddressIGaveButton;

    @FindBy(how = How.XPATH, using = "//h2/span[text()='Phone numbers']/following::a[text()='Remove'][last()]")
    private WebElement removePhoneLink;

    @FindBy(how = How.LINK_TEXT, using = "Update mailing address")
    private WebElement linkUpdateMailingAddress;

    public ContactinfoPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }


    public void verifyHeaderName() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(linkAddPhoneNumber));
        //if (linkAddPhoneNumber.isDisplayed()) {
        if (headerName.getText().toLowerCase().contains("step 1 of 3")) {
            assertEquals("update contact information - step 1 of 3", headerName.getText().toLowerCase());
        } else {
            assertEquals("contact information", headerName.getText().toLowerCase());
        }

    }

    public void clickAddEmail() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()='Add email address']")));
        linkAddEmail.click();
    }

    public void enterEmailAddress(String setValue) {

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='emailAddress']")));
        enterEmailAddress.click();
        enterEmailAddress.clear();
        enterEmailAddress.sendKeys(setValue);
        cancelButton.click();
        noDontLeaveButton.click();

    }

    public void clickAddEmailAddressButton() {

        addEmailAddressButton.click();
    }

    public void verifySuccessMessage() {
        assertEquals("success! thanks for adding your email address.", addEmailSuccessmessage.getText().toLowerCase());
    }

    public void clickEditEmail(String email) {

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@class='rte-email-address'][2]//following::a[@name='edit']")));
        String x = email;
        driver.findElement(By.xpath("//h2/span/span[text()='Email addresses']/following::li[text()='" + x + "']/span/a[text()='Edit']")).click();

    }

    public void clickUpdateEmailAddressButton() {
        updateEmailAddressButton.click();

    }

    public void verifyUpdateEmailSuccessMessage() throws InterruptedException {

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Contact information']/following::p")));
        assertTrue("Success message", addPhoneSuccessMessage.getText().toLowerCase().contains("success! thanks for updating your email address."));
    }


    public void enterNewEmailAddress(String email) {

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='newEmailAddress']")));
        driver.findElement(By.xpath("//input[@id='newEmailAddress']")).clear();
        enterNewEmailAddress.sendKeys(email);

    }

    public void clickRemoveEmail(String email) {

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@class='rte-email-address'][2]//following::a[@name='remove']")));
        String x = email;
        driver.findElement(By.xpath("//h2/span/span[text()='Email addresses']/following::li[text()='" + x + "']/span/a[text()='Remove']")).click();
    }

    public void clickYesRemove() {

        yesRemoveButton.click();
    }

    public void verifyRemoveEmailSuccessMessage() throws InterruptedException {
        Thread.sleep(5000);
        assertEquals("success! we removed your email address.", addEmailSuccessmessage.getText().toLowerCase());
    }

    public void clickAddPhoneNumberLink() {
        addPhoneNumberLink.click();
    }

    public void enterNewPhoneNumber(String phoneNumber) {
        inputPhoneNumber.click();
        inputPhoneNumber.sendKeys(phoneNumber);
        cancelButton.click();
        noDontLeaveButton.click();
    }

    public void clickAddPhoneNumberButton() {
        addNewPhoneNumberButton.click();
    }

    public void verifyAddPhoneNumberSuccessMessage() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Contact information']/following::p")));
        assertTrue("Success message", addPhoneSuccessMessage.getText().toLowerCase().contains("success! thanks for adding your phone number."));
    }


    public void clickEditPhoneNumberLink(String phoneNumber) throws InterruptedException {
        String x = phoneNumber;
        driver.findElement(By.xpath("//h2/span[text()='Phone numbers']/following::li[text()='" + x + "']/span/a[text()='Edit']")).click();
        Thread.sleep(5000);

    }

    public void enterUpdatedPhoneNumber(String phoneNumber) {
        inputUpdatedPhone.click();
        inputUpdatedPhone.clear();
        inputUpdatedPhone.sendKeys(phoneNumber);
    }

    public void clickUpdatePhoneNumberButton() {
        updatePhoneButton.click();
    }

    public void verifyUpdatePhoneNumberSuccessMessage() throws InterruptedException {
        Thread.sleep(5000);
        assertTrue("Success message", addPhoneSuccessMessage.getText().toLowerCase().contains("success! thanks for updating your phone number."));
    }

    public void clickUpdateMailingAddressLink() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2/span[text()='Mailing address']/following::a[text()='Update mailing address']")));
        updateMailingAddressLink.click();
    }

    public void enterUpdatedMailingAddress(String address1, String address2, String city1, String state1, String zipcode1) {
        addressLine1.click();
        addressLine1.sendKeys(address1);
        addressLine2.click();
        addressLine2.sendKeys(address2);
        city.click();
        city.sendKeys(city1);
        Select state = new Select(driver.findElement(By.id("mailingState")));
        state.selectByVisibleText(state1);
        zipCode.sendKeys(zipcode1);
        if (mailingCheckbox.isDisplayed()) {
            mailingCheckbox.click();
        }

    }


    public void clickUpdateMailingAddressButton() {
        updateMailButton.click();
    }

    public void clickUseAddressIGaveButton() {
        if (useAddressIGaveButton.isDisplayed()) {
            useAddressIGaveButton.click();
        }
    }


    public void verifyUpdateMailingAddressSuccessMessage() throws InterruptedException {
        Thread.sleep(10000);
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Contact information']/following::span[text()]")));
        assertEquals("success! thanks for updating your address.", addEmailSuccessmessage.getText().toLowerCase());
    }

    public void clickRemovePhoneLink(String phoneNumber) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2/span[text()='Phone numbers']/following::a[text()='Remove'][last()]")));
        String x = phoneNumber;
        driver.findElement(By.xpath("//h2/span[text()='Phone numbers']/following::li[text()='" + x + "']/span/a[text()='Remove']")).click();
        Thread.sleep(10000);

    }

    public void verifyRemovePhoneSuccessMessage() throws InterruptedException {
        Thread.sleep(10000);
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Contact information']/following::p")));
        assertTrue("Success message", addPhoneSuccessMessage.getText().toLowerCase().contains("success! we removed your phone number."));
    }



    public void clickUpdateMailingAddress() {
        linkUpdateMailingAddress.click();
    }

    public void clickAddEmailAddress() {
        linkAddEmailAddress.click();
    }

    public void clickAddPhoneNumber() {
        linkAddPhoneNumber.click();
    }
}



	