package com.usbank.test.automationFramework.trancore.pageObjects.claims;

import com.usbank.test.automationFramework.trancore.pageObjects.waitForPageToLoad.WaitForPageToLoad;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class VerifyingMailingAddress <T extends WebDriver>{

    @FindBy(how = How.XPATH, using = "//h1[@id='servicingAppHeader']")
    private WebElement lblVerifyMailingAddressHeader;

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Continue')]")
    private WebElement btnContinue;

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Cancel')]")
    private WebElement btnCancel;

    @FindBy(how = How.XPATH, using = "//a[@text='Edit address')]")
    private WebElement linkEditAddress;

    private T driver;

    WaitForPageToLoad callPageLoadMethod = new WaitForPageToLoad(driver);

    private static final Logger log = LogManager.getLogger(VerifyingMailingAddress.class.getName());

    public VerifyingMailingAddress(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);

    }
    public WebElement getlblVerifyMailingAddressHeader() {
        return lblVerifyMailingAddressHeader;
    }

    public WebElement getLinkEditAddress() { return linkEditAddress;}


    public void clickOnContinueBtnOnVerifyingMailingAddress() {

        btnContinue.click();
    }

    public void clickOnCancelBtnOnVerifyingMailingAddress() {
        btnCancel.click();
    }


    public void verifyMailingAddressEntered() throws InterruptedException {
        log.info("**************** Verifying Mailing Address page ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblVerifyMailingAddressHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblVerifyMailingAddressHeader, 5);
        btnContinue.click();

    }

    public void waitForPageLoad() {

            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(ExpectedConditions.elementToBeClickable(btnContinue));

        }
    }


