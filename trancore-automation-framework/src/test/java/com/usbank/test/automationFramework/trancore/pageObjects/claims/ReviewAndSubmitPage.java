package com.usbank.test.automationFramework.trancore.pageObjects.claims;

import com.usbank.test.automationFramework.trancore.pageObjects.waitForPageToLoad.WaitForPageToLoad;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ReviewAndSubmitPage<T extends WebDriver> {
    @FindBy(how = How.XPATH, using = "//h1[@id='servicingAppHeader']")
    private WebElement lblReviewAndSubmitHeader;

    @FindBy(how = How.XPATH, using = "//p[contains(text(),'Please review the details below before submitting ')]")
    private WebElement vrbPleaseReviewTheDetails;

    @FindBy(how = How.XPATH, using = "//body/div[@id='root']/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[5]/div[1]/div[1]/section[1]/div[1]/div[2]/div[3]/div[1]")
    private WebElement vrbDisputeDetails;

    @FindBy(how = How.XPATH, using = "//body/div[@id='root']/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[5]/div[1]/div[1]/section[1]/div[1]/div[2]/div[3]/div[3]")
    private WebElement vrbAddressDetails;

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Submit')]")
    private WebElement btnSubmit;

    private T driver;

    WaitForPageToLoad callPageLoadMethod;

    private static final Logger log = LogManager.getLogger(ReviewAndSubmitPage.class.getName());

    public ReviewAndSubmitPage(T inDriver) {
        driver = inDriver;
        callPageLoadMethod = new WaitForPageToLoad(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getLblReviewAndSubmitHeader() {
        return lblReviewAndSubmitHeader;
    }

    public void pleaseReviewTheDetailsText() {
        log.info(vrbPleaseReviewTheDetails.getText());
    }

    public void disputeDetailsText() {
        log.info(vrbDisputeDetails.getText());
    }

    public void addressDetailsText() {
        log.info(vrbAddressDetails.getText());
    }

    public void clickOnTheSubmitBtnOnReviewAndSubmitPage() {
        btnSubmit.click();
    }

    public void reviewAndSubmitPagePBOMFlowOne() throws InterruptedException {
        log.info("**************** Review & Submit Page ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblReviewAndSubmitHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblReviewAndSubmitHeader, 10);
        pleaseReviewTheDetailsText();
        disputeDetailsText();
        addressDetailsText();
        //clickOnTheSubmitBtnOnReviewAndSubmitPage();

    }
    public void waitForPageLoad() {

        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOf(vrbPleaseReviewTheDetails));
    }
}
