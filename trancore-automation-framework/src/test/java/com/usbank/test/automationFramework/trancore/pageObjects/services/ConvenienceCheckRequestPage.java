package com.usbank.test.automationFramework.trancore.pageObjects.services;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.logging.Logger;

public class ConvenienceCheckRequestPage<T extends WebDriver> {

    private static Logger logger = Logger.getLogger(ConvenienceCheckRequestPage.class.getName());

    @FindBy(how = How.XPATH, using = "//div[contains(text(),'-')]")
    private WebElement vrbFirstMiddleLastNames;

    @FindBy(how = How.XPATH, using = "//div[@id='layoutContentBody']")
    private WebElement vrbUserInformation;

    @FindBy(how = How.ID, using = "subHeader")
    private WebElement lblSubHeading;

/*    @FindBy(how = How.XPATH, using = "//*[@id=\"requestChecksForm\"]/div[1]/span")
    private WebElement lblnameheading;*/

    @FindBy(how = How.XPATH, using = "//button[@name='Submit']")
    private WebElement btnSubmit;

    @FindBy(how = How.ID, using = "layoutContentBody")
    private WebElement confirmationContainer;

    @FindBy(how = How.CLASS_NAME, using = "tranCoreButton")
    WebElement btnAsubmit;

    @FindBy(how = How.XPATH, using = "//*[@id=\"requestChecksForm\"]/div[14]/div/a")
    WebElement lnkCancel;

    public static int EXPLICIT_WAIT_TIME_SECONDS = 30;

    private T driver;


    public ConvenienceCheckRequestPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public void checkPageValidation() {
        boolean subHeadingPresent = false;
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lblSubHeading));

        if (lblSubHeading.isDisplayed()) {
            subHeadingPresent = true;
            Assert.assertTrue("Request Convenience Checks page displayed successfully", subHeadingPresent);
            logger.info("PASSED: Request Convenience Checks page displayed successfully ");
            String subheadingText = lblSubHeading.getText();
            Assert.assertTrue("MisMatch in header ", subheadingText.equalsIgnoreCase("Request Convenience Checks"));
        }

        if (subHeadingPresent) {
//to be conti
            /*
            String str = vrbUserInformation.getText();
            System.out.println("String text whole: "+str);
            str.replace("CANCELSUBMIT", "");
            System.out.println("String afterware : \n"+str);
            logger.info(str.replace("CANCELSUBMIT", ""));


*/

        }

    }

    public WebElement getSubmitButton() {
        return btnSubmit;
    }

    public void clickOnSubmitButton() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnSubmit));
        if (btnSubmit.isDisplayed()) {
            Assert.assertTrue("Submit button is present and displayed successfully", true);
            logger.info("PASSED: Submit button is present and displayed successfully ");

            btnSubmit.click();
            logger.info("PASSED: Submit button Clicked successfully ");
        } else {
            logger.info("Submit button is not present");
        }

    }

    public void checkRequestConfirmation() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(confirmationContainer));
        String headingText = confirmationContainer.getText();
        headingText = headingText.substring(0, headingText.indexOf('\n'));
        System.out.println("Heading text output:  \n" + headingText);

        if (confirmationContainer.isDisplayed()) {
            Assert.assertTrue("MisMatch in Confirmation message", headingText.equalsIgnoreCase("Thank you for submitting a Convenience Check Request.  After submitting this request you can expect to receive your checks via U.S. Mail within 10 business days."));
            logger.info("PASSED: Request Convenience Checks Confirmation page displayed successfully ");
        }
        btnAsubmit.click();
    }

    public void clickCancelButton() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(confirmationContainer));
        lnkCancel.click();
        logger.info("PASSED: Cancel button click successfully ");
    }
}
