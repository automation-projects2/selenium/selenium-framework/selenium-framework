package com.usbank.test.automationFramework.trancore.pageObjects.profile;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class MessagesLandingPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(MessagesLandingPage.class);

    private T driver;

    public MessagesLandingPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }
}
