package com.usbank.test.automationFramework.trancore.stepDefinitions.freeCreditScore;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

public class FreeCreditScoreSteps {

    @Autowired
    private StepDefinitionManager stepDefinitionManager;

    @And("the user clicks on Continue Button within the Free Credit Score Pop-up window to leave Trancore Card Member Services page")
    public void theUserIsPromptedWithAPopUpWindowToLeaveTrancoreCardMemberServicesPage() throws InterruptedException {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getFreeCreditScorePage().getBtnContinue().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getFreeCreditScorePage().clickContinueBtn();
    }


    @Then("the user clicks on Cancel button within the Free Credit Score Pop-up window to return to Trancore Card Member Services page")
    public void theUserClicksOnCancelButtonWithinTheFreeCreditScorePopUpWindowToLeaveTrancoreCardMemberServicesPage() throws InterruptedException {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getFreeCreditScorePage().getBtnCancel().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getFreeCreditScorePage().clickCancelBtn();
    }
}
