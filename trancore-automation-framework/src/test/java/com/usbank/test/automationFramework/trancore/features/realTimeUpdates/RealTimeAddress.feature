@RealTimeAddress

Feature: Real time address

  Scenario Outline: Customer should able to successfully update the address when the real time address flag is turned ON

    Given the user has navigated to the <partner> login page
    And the user has logged into with <user_name> username and <password> password
    And customer click on Profilepage
    And the customer clicks the contact information link
    And the customer should be directed to the OTP Screen
    And the customer should select the contact <Contact> at Send OTP Page
    When the customer clicks on Send one-time passcode at Send OTP Page
    And the customer enters <passcode> passcode at Enter OTP Page
    And the customer clicks on Continue at Enter OTP Page
    Then the customer should be directed to the Contact Information
    And the customer clicks on update mailing address link
    And the customer enters the address <Address line1> <Address2> <City> <State> <Zipcode> for update
    And the customer clicks on update mailing address button
    And the customer clicks on use the address I gave you button if present
    Then the mailing address is updated successfully

    Examples:

      | partner | user_name  | password  | passcode  | Contact |Address line1|Address2|City|State|Zipcode|
