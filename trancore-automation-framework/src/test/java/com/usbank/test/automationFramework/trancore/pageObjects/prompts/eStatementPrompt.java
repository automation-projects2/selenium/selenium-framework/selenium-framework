package com.usbank.test.automationFramework.trancore.pageObjects.prompts;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class eStatementPrompt {

    private static final Logger logger = LogManager.getLogger(eStatementPrompt.class);
    private WebDriver driver;
    private int EXPLICIT_WAIT_TIME_SECONDS = 30;

    @FindBy(how= How.ID, using="estatementsPromptReminderLink")
    private WebElement remindLaterLink;

    public eStatementPrompt(WebDriver inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public WebElement getRemindLaterLink() {
        return remindLaterLink;
    }

    public void waitForPageLoad() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(remindLaterLink));

    }
}
