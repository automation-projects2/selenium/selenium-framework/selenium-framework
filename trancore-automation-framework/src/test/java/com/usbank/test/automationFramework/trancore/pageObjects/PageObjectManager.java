package com.usbank.test.automationFramework.trancore.pageObjects;

import com.usbank.test.automationFramework.trancore.pageObjects.addAUPrompt.AddAuPromptPage;
import com.usbank.test.automationFramework.trancore.pageObjects.admin.AuthorizedOwnerPage;
import com.usbank.test.automationFramework.trancore.pageObjects.admin.CheckStatus2Page;
import com.usbank.test.automationFramework.trancore.pageObjects.alerts.AlertsHistoryPage;
import com.usbank.test.automationFramework.trancore.pageObjects.alerts.AlertsLandingPage;
import com.usbank.test.automationFramework.trancore.pageObjects.alerts.LandingForAlertsPage;
import com.usbank.test.automationFramework.trancore.pageObjects.authorizedEmployee.AuthorizedEmployeePage;
import com.usbank.test.automationFramework.trancore.pageObjects.claims.*;
import com.usbank.test.automationFramework.trancore.pageObjects.combinedAccounts.CombinedAccountsPage;
import com.usbank.test.automationFramework.trancore.pageObjects.contactUsForHelp.ContactUsForHelpPage;
import com.usbank.test.automationFramework.trancore.pageObjects.creditLineIncrease.*;
import com.usbank.test.automationFramework.trancore.pageObjects.eSignAgreement.EsignEnrollPage;
import com.usbank.test.automationFramework.trancore.pageObjects.enrollinClicktoPay.LeavingSitePage;
import com.usbank.test.automationFramework.trancore.pageObjects.enrollinClicktoPay.PayWithCardPage;
import com.usbank.test.automationFramework.trancore.pageObjects.enrollment.EnrollmentPage;
import com.usbank.test.automationFramework.trancore.pageObjects.enrollment.PDAPEnrollmentPage;
import com.usbank.test.automationFramework.trancore.pageObjects.enrollmentScript.EnrollmentScriptPage;
import com.usbank.test.automationFramework.trancore.pageObjects.enrollmentScript.ModifiedEnrollmentScriptPage;
import com.usbank.test.automationFramework.trancore.pageObjects.fraud.*;
import com.usbank.test.automationFramework.trancore.pageObjects.frequentlyAskedQuestions.FrequentlyAskedQuestionsPage;
import com.usbank.test.automationFramework.trancore.pageObjects.googlePay.GooglePayPushProvision;
import com.usbank.test.automationFramework.trancore.pageObjects.internalError.InternalErrorPage;
import com.usbank.test.automationFramework.trancore.pageObjects.login.*;
import com.usbank.test.automationFramework.trancore.pageObjects.messageGranularity.ChargeOffPage;
import com.usbank.test.automationFramework.trancore.pageObjects.messages.SecureMessagePage;
import com.usbank.test.automationFramework.trancore.pageObjects.myAccount.AccountSummary;
import com.usbank.test.automationFramework.trancore.pageObjects.myAccount.OnlineStatements.DiscontinuePaperStatementsStep1;
import com.usbank.test.automationFramework.trancore.pageObjects.myAccount.OnlineStatements.OnlineStatementsPage;
import com.usbank.test.automationFramework.trancore.pageObjects.onlineSettlements.OnlineSettlementsPage_Jalapenos;
import com.usbank.test.automationFramework.trancore.pageObjects.otp.EnterOTPPage;
import com.usbank.test.automationFramework.trancore.pageObjects.otp.OTPScreenPage;
import com.usbank.test.automationFramework.trancore.pageObjects.pastPaymentDue.PastPaymentDue;
import com.usbank.test.automationFramework.trancore.pageObjects.payPal.AgreeAndContinuePage;
import com.usbank.test.automationFramework.trancore.pageObjects.payPal.ContactMethodPage;
import com.usbank.test.automationFramework.trancore.pageObjects.payments.*;
import com.usbank.test.automationFramework.trancore.pageObjects.phonePrompt.PhonePromptPage;
import com.usbank.test.automationFramework.trancore.pageObjects.profile.*;
import com.usbank.test.automationFramework.trancore.pageObjects.prompts.eStatementPrompt;
import com.usbank.test.automationFramework.trancore.pageObjects.rewards.RewardsAndBenefitsPage;
import com.usbank.test.automationFramework.trancore.pageObjects.rewards.RewardsLandingPage;
import com.usbank.test.automationFramework.trancore.pageObjects.samsungPay.SamsungPayPushProvision;
import com.usbank.test.automationFramework.trancore.pageObjects.selectAccount.SelectAccountPage;
import com.usbank.test.automationFramework.trancore.pageObjects.services.*;
import com.usbank.test.automationFramework.trancore.pageObjects.unAuthenticatedActivation.UnAuthenticatedActivation;
import com.usbank.test.automationFramework.trancore.pageObjects.virtualCard.virtualCard;
import com.usbank.test.automationFramework.trancore.pageObjects.waitForPageToLoad.WaitForPageToLoad;
import com.usbank.test.automationFramework.trancore.pageObjects.wrapup.WrapUpUnsupportedReasonPage;
import com.usbank.test.driver.DriverManager;
import com.usbank.test.driver.DriverManagerFactory;
import com.usbank.test.enums.BrowserType;
import com.usbank.test.helper.CaptureScreenShots;
import com.usbank.test.objects.EnvironmentData;
import com.usbank.test.objects.trancore.User;
import io.appium.java_client.AppiumDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @description class to manage the page objects for the site being tests class
 * and can be @autowired into your step definition
 */
@Component
//@Scope("cucumber-glue")
public class PageObjectManager {
    private static final String REGEX_BASE_URL = "[Hh][Tt][Tt][Pp][Ss]?://.+/";
    private static final Logger logger = LogManager.getLogger(PageObjectManager.class);

    //Autowired in contstructor, Don't autowire testDataManager!  Pass variables in via stepdefinitions
    private AutomationConfig automationConfig;
    private DriverManager driverManager;

    //Page Objects Go here!
    private ContactInformation contactInformation;
    private PastPaymentDue pastPaymentDue;
    private PayWithCardPage payWithCardpage;
    private LeavingSitePage leavingSitepage;
    private PilotPage pilotPage;
    private SSOLoginPage ssoLoginPage;
    private LoginPage loginPage;
    private MyAccountPage myAccountPage;
    private MyAccountOmvPage myAccountOmvPage;
    private EnrollmentPage enrollmentPage;
    private PDAPEnrollmentPage pdapenrollmentPage;
    private ESignAgreementPage eSignAgreementPage;
    private IdShieldPage idShieldPage;
    private WebNavigationPage webNavigationPage;
    private ServicesLandingPage servicesLandingPage;
    private AgreeAndContinuePage agreeAndContinuePage;
    private ContactMethodPage contactMethodPage;
    private CaptureScreenShots captureAcreenshots;
    private CheckStatus2Page checkStatus2Page;
    private TransactionDetailsModel transactionDetailsModel;
    private DisputePurchaseTypePage disputePurchaseTypePage;
    private UnAuthAccActFraudAppChkPage unAuthAccActFraudAppChkPage;
    private FraudAppChkPage fraudAppChkPage;
    private ChkProfileInfoPage chkProfileInfoPage;
    private OtherTransactionsPage otherTransactionsPage;
    private CardIssuancePage cardIssuancePage;
    private ServicesPage servicesPage;
    private ClickToPayPage clickToPayPage;
    private ReviewTransactionsPage reviewTransactionsPage;
    private FraudDiscoveryPage fraudDiscoveryPage;
    private FraudSuspectPage fraudSuspectPage;
    private PoliceReportPage policeReportPage;
    private PossessCardsPage possessCardsPage;
    private CardUsePage cardUsePage;
    private CardInHandPage cardInHandPage;
    private TravelPage travelPage;
    private OtherInfoPage otherInfoPage;
    private MobileWalletPage mobileWalletPage;
    private MailingInfoPage mailingInfoPage;
    private ReviewPage reviewPage;
    private CardLostStolenPage cardLostStolenPage;
    private LessThan45Page lessThan45Page;
    private AddressChangedPage addressChangedPage;
    private EmailPromptPage emailPromptPage;
    private DigitalWalletPage digitalWalletPage;
    private LockAndUnlockPage lockOrUnlockCardPage;
    private DisputeReasonSelectionPage disputeReasonSelectionPage;
    private WrapUpUnsupportedReasonPage wrapUpUnsupportedReasonPage;
    private ContactValidationPage contactValidationPage;
    private FraudConfirmationPage fraudConfirmationPage;
    private IDverificationPage iDverificationPage;
    private WrapUpFraudPage wrapUpFraudPage;
    private BalanceTransferPage balanceTransferPage;
    private EnrollmentScriptPage enrollmentScriptPage;
    private ModifiedEnrollmentScriptPage modifiedenrollmentScriptPage;
    private EsignEnrollPage esignEnrollPage;
    private FrequentlyAskedQuestionsPage frequentlyAskedQuestionsPage;
    private LockAndUnlockPage lockAndUnlockPage;
    private LostOrStolenPage lostOrStolenPage;
    private TravelNotificationPage travelNotificationPage;
    private SamsungPayPushProvision samsungPayPage;
    private MakePaymentPages makePaymentPage;
    private PaymentAccountAddViewDeletePage paymentAccountPage;
    private AutoPaySetupEditDeletePages autoPayPage;
    private CombinedAccountsPage CombinedAccountsPage;
    private com.usbank.test.automationFramework.trancore.pageObjects.fraud.SelectAccountPage selectAccountPage;
    private RewardsLandingPage rewardsPage;
    private SecureMessagePage secureMessagePage;
    private AuthorizedOwnerPage AuthorizedOwnerPage;
    private AuthorizedEmployeePage authorizedEmployeePage;
    private GooglePayPushProvision googlePayPage;
    private AddAuthorizedUserPage addAuthorizedUserPage;
    private AddJointOwnerPage addJointOwnerPage;
    private ConvenienceCheckRequestPage convenienceCheckRequestPage;
    private SpendAnalysisPage spendAnalysisPage;
    private DownloadAnnualAccountSummaryPage downloadAnnualAccountSummaryPage;
    private AlertsLandingPage alertsLandingPage;
    private AlertsHistoryPage alertsHistoryPage;
    private ProfileLandingPage profileLandingPage;
    private StepUpIdshieldPage stepUpIdshieldPage;
    private ChangeIDshieldImagePage changeIDshieldImagePage;
    private PaymentOptionsPage paymentOptionsPage;
    private PaymentsLandingPage paymentsLandingPage;
    private ContactinfoPage contactInfoPage;
    private OTPScreenPage otpScreenPage;
    private EnterOTPPage enterOTPPage;
    private ContactUsForHelpPage contactUsForHelpPage;
    private ReportCardLostOrStolenPage reportCardLostOrStolenPage;
    private ReviewRecentTransactionPage reviewRecentTransactionPage;
    private OrderANewCardPage orderANewCardPage;
    private CardDeliveryPage cardDeliveryPage;
    private CardReplacementSelectUsersPage cardReplacementSelectUsersPage;
    private ManageEmployeesPage manageEmployeesPage;
    private FreeCreditScorePage freeCreditScorePage;
    private AddAuthorizedRepresentativePDFPage addAuthorizedRepresentativePDFPage;
    private OnlineSettlementsPage_Jalapenos onlineSettlementsPage;
    private LandingForAlertsPage landingForAlertsPage;
    private SSODeeplinkLoginPage ssoDeeplinkLoginPage;
    private IncomeAndEsignPage incomeAndEsignPage;
    private DisputeThisTransactionPage disputeThisTransactionPage;
    private ReasonForDisputePage reasonForDisputePage;
    private TellUsMorePage tellUsMorePage;
    private ContactingMerchantPage contactingMerchantPage;
    private PurchaseDetailsPage purchaseDetailsPage;
    private PaymentTypePage paymentTypePage;
    private PurchaseDocumentPage purchaseDocumentPage;
    private DisputeCaseContactInfoPage disputeCaseContactInfoPage;
    private OtherCardUsedPage otherCardUsedPage;
    private VerifyOtherFormOfPaymentPage verifyOtherFormOfPaymentPage;
    private OtherFormOfPaymentPage otherFormOfPaymentPage;
    private ReviewAndSubmitPage reviewAndSubmitPage;
    private ChangePasswordPage changePasswordPage;
    private RewardsAndBenefitsPage rewardsAndBenefitsPage;
    private ChangePersonalIDPage changePersonalIDPage;
    private ReviewAndSubmitCardReplacementOrderPage reviewandSubmitCardReplacementOrderpage;
    private IncomeAndAssetsPage incomeAndAssetsPage;
    private HouseInformationPage housingInformation;
    private ReviewYourRequestPage reviewYourRequestPage;
    private ReceivedRequestPage receivedRequestPage;
    private CallUsNowToFinish callUsNowToFinish;
    private IncomePromptPage incomePromptPage;
    private AccountSummary accountSummary;
    private SelectAccountPage slctAccountPage;
    private InternalErrorPage internalErrorPage;
    private PhonePromptPage phonePromptPage;
    private ShortTermPlanDetailsPage shortTermPlanDetailsPage;
    private ShortTermFundingAccountPage shortTermFundingAccountPage;
    private HardshipQuestionnairePage hardshipQuestionnairePage;
    private HardshipQuestionnaire1Page hardshipQuestionnaire1Page;
    private HardshipQuestionnaire2Page hardshipQuestionnaire2Page;
    private OnlineStatementsPage onlineStatementsPage;
    private DiscontinuePaperStatementsStep1 discontinuePaperStatementsStep1;
    private eStatementPrompt eStatementPrompt;
    private UnAuthenticatedActivation unAuthenticatedActivation;
    private VerifyingMailingAddress verifyingMailingAddress;
    private WaitForPageToLoad waitForPageToLoad;
    private LoginAssistancePage loginAssistancePage;
    private RetrievePersonalID retrievePersonalID;
    private PersonalIDRetrieved personalIDRetrieved;
    private AddNewPhoneNumberPage addNewPhoneNumberPage;
    private UpdateMailingAddress updateMailingAddress;
    private AddEmailAddressPage addEmailAddressPage;
    private RetrievePasswordPage retrievePasswordPage;
    private FAQPage faqPage;
    private CardActivationPage cardActivationPage;
    private AddAuPromptPage addAUPromptPage;
    private ChargeOffPage chargeOffPage;
    private LongTermPlanDetailsPage longTermPlanDetailsPage;


    private virtualCard virtualCard;

    @Autowired
    public PageObjectManager(AutomationConfig automationConfig) {
        this.automationConfig = automationConfig;
    }

    public void initPageObjectDriver(RunMode runMode, BrowserType browserType) {
        if (this.driverManager != null && this.driverManager.getDriver() != null) {
            this.driverManager.quitDriver();
        }
        this.driverManager = DriverManagerFactory.initializeDriver(runMode, browserType);
        pilotPage = new PilotPage(driverManager.getDriver(), automationConfig.getPilotURL());
        ssoLoginPage = new SSOLoginPage(driverManager.getDriver(), automationConfig.getPilotURL());
        loginPage = new LoginPage(driverManager.getDriver());
        myAccountPage = new MyAccountPage(driverManager.getDriver());
        myAccountOmvPage = new MyAccountOmvPage(driverManager.getDriver());
        enrollmentPage = new EnrollmentPage(driverManager.getDriver());
        pdapenrollmentPage = new PDAPEnrollmentPage(driverManager.getDriver());
        eSignAgreementPage = new ESignAgreementPage(driverManager.getDriver());
        idShieldPage = new IdShieldPage(driverManager.getDriver());
        webNavigationPage = new WebNavigationPage(driverManager.getDriver());
        rewardsAndBenefitsPage = new RewardsAndBenefitsPage(driverManager.getDriver());
        servicesLandingPage = new ServicesLandingPage(driverManager.getDriver());
        agreeAndContinuePage = new AgreeAndContinuePage(driverManager.getDriver());
        captureAcreenshots = new CaptureScreenShots(driverManager.getDriver());
        contactMethodPage = new ContactMethodPage(driverManager.getDriver());
        transactionDetailsModel = new TransactionDetailsModel(driverManager.getDriver());
        contactInformation = new ContactInformation(driverManager.getDriver());
        disputePurchaseTypePage = new DisputePurchaseTypePage(driverManager.getDriver());
        unAuthAccActFraudAppChkPage = new UnAuthAccActFraudAppChkPage(driverManager.getDriver());
        wrapUpFraudPage = new WrapUpFraudPage(driverManager.getDriver());
        fraudAppChkPage = new FraudAppChkPage(driverManager.getDriver());
        chkProfileInfoPage = new ChkProfileInfoPage(driverManager.getDriver());
        otherTransactionsPage = new OtherTransactionsPage(driverManager.getDriver());
        cardIssuancePage = new CardIssuancePage(driverManager.getDriver());
        servicesPage = new ServicesPage(driverManager.getDriver());
        clickToPayPage = new ClickToPayPage(driverManager.getDriver());
        reviewTransactionsPage = new ReviewTransactionsPage(driverManager.getDriver());
        fraudDiscoveryPage = new FraudDiscoveryPage(driverManager.getDriver());
        fraudSuspectPage = new FraudSuspectPage(driverManager.getDriver());
        policeReportPage = new PoliceReportPage(driverManager.getDriver());
        possessCardsPage = new PossessCardsPage(driverManager.getDriver());
        cardUsePage = new CardUsePage(driverManager.getDriver());
        cardInHandPage = new CardInHandPage(driverManager.getDriver());
        travelPage = new TravelPage(driverManager.getDriver());
        otherInfoPage = new OtherInfoPage(driverManager.getDriver());
        mobileWalletPage = new MobileWalletPage(driverManager.getDriver());
        mailingInfoPage = new MailingInfoPage(driverManager.getDriver());
        reviewPage = new ReviewPage(driverManager.getDriver());
        cardLostStolenPage = new CardLostStolenPage(driverManager.getDriver());
        lessThan45Page = new LessThan45Page(driverManager.getDriver());
        addressChangedPage = new AddressChangedPage(driverManager.getDriver());
        emailPromptPage = new EmailPromptPage(driverManager.getDriver());
        digitalWalletPage = new DigitalWalletPage(driverManager.getDriver());
        lockAndUnlockPage = new LockAndUnlockPage(driverManager.getDriver());
        disputeReasonSelectionPage = new DisputeReasonSelectionPage(driverManager.getDriver());
        wrapUpUnsupportedReasonPage = new WrapUpUnsupportedReasonPage(driverManager.getDriver());
        contactValidationPage = new ContactValidationPage(driverManager.getDriver());
        fraudConfirmationPage = new FraudConfirmationPage(driverManager.getDriver());
        iDverificationPage = new IDverificationPage(driverManager.getDriver());
        balanceTransferPage = new BalanceTransferPage((driverManager.getDriver()));
        lockAndUnlockPage = new LockAndUnlockPage(driverManager.getDriver());
        enrollmentScriptPage = new EnrollmentScriptPage((driverManager.getDriver()));
        modifiedenrollmentScriptPage = new ModifiedEnrollmentScriptPage(driverManager.getDriver());
        esignEnrollPage = new EsignEnrollPage((driverManager.getDriver()));
        frequentlyAskedQuestionsPage = new FrequentlyAskedQuestionsPage(driverManager.getDriver());
        lostOrStolenPage = new LostOrStolenPage((driverManager.getDriver()));
        travelNotificationPage = new TravelNotificationPage(driverManager.getDriver());
        pastPaymentDue = new PastPaymentDue(driverManager.getDriver());
        samsungPayPage = new SamsungPayPushProvision(driverManager.getDriver());
        samsungPayPage = new SamsungPayPushProvision(driverManager.getDriver());
        checkStatus2Page = new CheckStatus2Page(driverManager.getDriver());
        CombinedAccountsPage = new CombinedAccountsPage(driverManager.getDriver());
        alertsLandingPage = new AlertsLandingPage(driverManager.getDriver());
        alertsHistoryPage = new AlertsHistoryPage(driverManager.getDriver());
        profileLandingPage = new ProfileLandingPage(driverManager.getDriver());
        stepUpIdshieldPage = new StepUpIdshieldPage(driverManager.getDriver());
        changeIDshieldImagePage = new ChangeIDshieldImagePage(driverManager.getDriver());
        paymentOptionsPage = new PaymentOptionsPage(driverManager.getDriver());
        paymentsLandingPage = new PaymentsLandingPage((driverManager.getDriver()));
        selectAccountPage = new com.usbank.test.automationFramework.trancore.pageObjects.fraud.SelectAccountPage((driverManager.getDriver()));
        checkStatus2Page = new CheckStatus2Page(driverManager.getDriver());
        makePaymentPage = new MakePaymentPages(driverManager.getDriver());
        // paymentsLandingPage = new PaymentsLandingPage(driverManager.getDriver());
        paymentAccountPage = new PaymentAccountAddViewDeletePage(driverManager.getDriver());
        autoPayPage = new AutoPaySetupEditDeletePages(driverManager.getDriver());
        CombinedAccountsPage = new CombinedAccountsPage(driverManager.getDriver());
        rewardsPage = new RewardsLandingPage(driverManager.getDriver());
        secureMessagePage = new SecureMessagePage((driverManager.getDriver()));
        AuthorizedOwnerPage = new AuthorizedOwnerPage(driverManager.getDriver());
        authorizedEmployeePage = new AuthorizedEmployeePage(driverManager.getDriver());
        virtualCard = new virtualCard(driverManager.getDriver());
        payWithCardpage = new PayWithCardPage(driverManager.getDriver());
        leavingSitepage = new LeavingSitePage(driverManager.getDriver());
        googlePayPage = new GooglePayPushProvision(driverManager.getDriver());
        contactInfoPage = new ContactinfoPage(driverManager.getDriver());
        otpScreenPage = new OTPScreenPage(driverManager.getDriver());
        enterOTPPage = new EnterOTPPage(driverManager.getDriver());
        alertsLandingPage = new AlertsLandingPage(driverManager.getDriver());
        contactUsForHelpPage = new ContactUsForHelpPage(driverManager.getDriver());
        reportCardLostOrStolenPage = new ReportCardLostOrStolenPage(driverManager.getDriver());
        reviewRecentTransactionPage = new ReviewRecentTransactionPage(driverManager.getDriver());
        orderANewCardPage = new OrderANewCardPage(driverManager.getDriver());
        //cardDeliveryPage = new CardDeliveryPage(driverManager.getDriver());
        cardReplacementSelectUsersPage = new CardReplacementSelectUsersPage(driverManager.getDriver());
        addAuthorizedUserPage = new AddAuthorizedUserPage(driverManager.getDriver());
        addJointOwnerPage = new AddJointOwnerPage(driverManager.getDriver());
        convenienceCheckRequestPage = new ConvenienceCheckRequestPage(driverManager.getDriver());
        spendAnalysisPage = new SpendAnalysisPage(driverManager.getDriver());
        manageEmployeesPage = new ManageEmployeesPage(driverManager.getDriver());
        freeCreditScorePage = new FreeCreditScorePage(driverManager.getDriver());
        addAuthorizedRepresentativePDFPage = new AddAuthorizedRepresentativePDFPage(driverManager.getDriver());
        onlineSettlementsPage = new OnlineSettlementsPage_Jalapenos(driverManager.getDriver());
        downloadAnnualAccountSummaryPage = new DownloadAnnualAccountSummaryPage(driverManager.getDriver(), automationConfig);
        disputeThisTransactionPage = new DisputeThisTransactionPage(driverManager.getDriver());
        reasonForDisputePage = new ReasonForDisputePage(driverManager.getDriver());
        tellUsMorePage = new TellUsMorePage(driverManager.getDriver());
        contactingMerchantPage = new ContactingMerchantPage(driverManager.getDriver());
        purchaseDetailsPage = new PurchaseDetailsPage(driverManager.getDriver());
        paymentTypePage = new PaymentTypePage(driverManager.getDriver());
        purchaseDocumentPage = new PurchaseDocumentPage(driverManager.getDriver());
        disputeCaseContactInfoPage = new DisputeCaseContactInfoPage(driverManager.getDriver());
        otherCardUsedPage = new OtherCardUsedPage((driverManager.getDriver()));
        verifyOtherFormOfPaymentPage = new VerifyOtherFormOfPaymentPage(driverManager.getDriver());
        otherFormOfPaymentPage = new OtherFormOfPaymentPage(driverManager.getDriver());
        verifyingMailingAddress = new VerifyingMailingAddress(driverManager.getDriver());
        reviewAndSubmitPage = new ReviewAndSubmitPage(driverManager.getDriver());
        changePasswordPage = new ChangePasswordPage(driverManager.getDriver());
        changePersonalIDPage = new ChangePersonalIDPage(driverManager.getDriver());
        reviewandSubmitCardReplacementOrderpage = new ReviewAndSubmitCardReplacementOrderPage(driverManager.getDriver());
        incomeAndAssetsPage = new IncomeAndAssetsPage(driverManager.getDriver());
        housingInformation = new HouseInformationPage(driverManager.getDriver());
        reviewYourRequestPage = new ReviewYourRequestPage(driverManager.getDriver());
        receivedRequestPage = new ReceivedRequestPage(driverManager.getDriver());
        callUsNowToFinish = new CallUsNowToFinish(driverManager.getDriver());
        incomePromptPage = new IncomePromptPage(driverManager.getDriver());
        accountSummary = new AccountSummary(driverManager.getDriver());
        slctAccountPage = new SelectAccountPage(driverManager.getDriver());
        internalErrorPage = new InternalErrorPage(driverManager.getDriver());
        phonePromptPage = new PhonePromptPage(driverManager.getDriver());
        shortTermPlanDetailsPage = new ShortTermPlanDetailsPage(driverManager.getDriver());
        shortTermFundingAccountPage = new ShortTermFundingAccountPage(driverManager.getDriver());
        loginAssistancePage = new LoginAssistancePage(driverManager.getDriver());
        retrievePersonalID = new RetrievePersonalID(driverManager.getDriver());
        personalIDRetrieved = new PersonalIDRetrieved(driverManager.getDriver());
        addNewPhoneNumberPage = new AddNewPhoneNumberPage(driverManager.getDriver());
        updateMailingAddress = new UpdateMailingAddress(driverManager.getDriver());
        addEmailAddressPage = new AddEmailAddressPage(driverManager.getDriver());
        retrievePasswordPage = new RetrievePasswordPage(driverManager.getDriver());
        faqPage = new FAQPage(driverManager.getDriver());
        cardActivationPage = new CardActivationPage(driverManager.getDriver());
        landingForAlertsPage = new LandingForAlertsPage(driverManager.getDriver());
        ssoDeeplinkLoginPage = new SSODeeplinkLoginPage(driverManager.getDriver(), automationConfig.getPilotURL());
        incomeAndEsignPage = new IncomeAndEsignPage(driverManager.getDriver());
        hardshipQuestionnaire1Page = new HardshipQuestionnaire1Page(driverManager.getDriver());
        hardshipQuestionnaire2Page = new HardshipQuestionnaire2Page(driverManager.getDriver());
        hardshipQuestionnairePage = new HardshipQuestionnairePage(driverManager.getDriver());
        eStatementPrompt = new eStatementPrompt(driverManager.getDriver());
        onlineStatementsPage = new OnlineStatementsPage(driverManager.getDriver());
        unAuthenticatedActivation = new UnAuthenticatedActivation(driverManager.getDriver());
        addAUPromptPage = new AddAuPromptPage(driverManager.getDriver());
        //Message Granularity
        chargeOffPage = new ChargeOffPage(driverManager.getDriver());
        longTermPlanDetailsPage = new LongTermPlanDetailsPage(driverManager.getDriver());

    }

    @PostConstruct
    public void setup() {

    }

    @PreDestroy
    public void close() {
        driverManager.quitDriver();
    }


    public void openLandingPage(String pilotPartner, List<EnvironmentData> environmentDataList) throws Exception {
        String environment = automationConfig.getPilotEnv();
        System.out.println("Environment: " + environment);
        Iterator<EnvironmentData> iterator = environmentDataList.iterator();
        boolean environmentFound = false;

        while (iterator.hasNext()) {
            EnvironmentData environmentData = iterator.next();
            if (pilotPartner.equalsIgnoreCase(environmentData.getSystem()) && environment.equalsIgnoreCase(environmentData.getEnvironment())) {
                String url = environmentData.getValue();
                if (automationConfig.getView().equalsIgnoreCase("OMV")) {
                    if (url.indexOf('?') >= 0) {
                        url += "&mKey=" + MobileKey.valueOf(pilotPartner.toUpperCase()).getMobileKey();
                    } else {
                        url += "?mKey=" + MobileKey.valueOf(pilotPartner.toUpperCase()).getMobileKey();
                    }
                }
                System.out.println("URL: " + url);
                driverManager.getDriver().get(url);
                environmentFound = true;
                break;
            }
        }
        if (environmentFound == false) {
            throw new Exception("There is no entry for the combination of Environment " + environment + " and Partner " + pilotPartner + " in the Environment Data File. Please add an entry to file.");
        }
    }


    public void openLandingPageUsingSSO(String ssoPartner, String ssoHashid) {
        logger.info("SSO Login Page: " + ssoPartner + "," + automationConfig.getSSOEnv() + "," + automationConfig.getPilotDomain());
        String parentWindow = driverManager.getDriver().getWindowHandle();

        //Set mobile key if View is set to OMV in Property Config.
        String mobileKey = automationConfig.getView().equalsIgnoreCase("omv") ? automationConfig.getMobileKeyForPartner(ssoPartner) : null;

        ssoLoginPage.enterSSOPageInformation(ssoPartner, automationConfig.getSSOEnv(), ssoHashid, mobileKey);

        for (String curWindow : driverManager.getDriver().getWindowHandles()) {
            if (curWindow != parentWindow) {
                driverManager.getDriver().switchTo().window(curWindow);
            }
        }
    }

    public void openDeepLinkLandingPageUsingSSO(String ssoPartner, String ssoHashid, String ssoDestination) {
        logger.info("SSO Deeplink Login Page: " + ssoPartner + "," + automationConfig.getSSOEnv() + "," + automationConfig.getPilotDomain());
        String parentWindow = driverManager.getDriver().getWindowHandle();

        //Set mobile key if View is set to OMV in Property Config.
        String mobileKey = automationConfig.getView().equalsIgnoreCase("omv") ? automationConfig.getMobileKeyForPartner(ssoPartner) : null;

        ssoDeeplinkLoginPage.enterSSODeeplinkPageInformation(ssoPartner, automationConfig.getSSOEnv(), ssoHashid, mobileKey, ssoDestination);

        for (String curWindow : driverManager.getDriver().getWindowHandles()) {
            if (curWindow != parentWindow) {
                driverManager.getDriver().switchTo().window(curWindow);
            }
        }
        incomeAndEsignPage.checkForIncomeAndEsign();
    }

    public void login(String personalID, String password) {
        loginPage.loginToLandingPage(personalID, password);
        //TODO: Add check to see if remind me next time appears and click until it does not appear
        //ADD AU Prompt
//        getAddAUPromptPage().VerifyAddAUInterstial();
        getMyAccountPage().waitForMyAccountPageToLoad();
    }

    public void loginFromSelectAccountTab(String personalID, String password) {
        loginPage.loginToLandingPage(personalID, password);
        //TODO: Add check to see if remind me next time appears and click until it does not appear
        getSelectAccountPage().waitForSelectAccountPageToLoad();
    }


    public void loginToUserAccount(String personalID, String password) {
        loginPage.loginToLandingPage(personalID, password);
    }

    public void navigatedToAccountPage() {
        getMyAccountPage().waitForMyAccountPageToLoad();
    }

    public void resetPassword(String personalID) {
        loginPage.loginAndClickForgotPassword(personalID);

    }

    public void enterInvalidID(String invalidUser) {
        loginPage.enterInValidPersonalID(invalidUser);
        loginPage.clickSubmit();
    }

    public void invalidLogin(String personalID) {
        loginPage.loginWithInvalidID(personalID);
    }

    public void invalidErrorMessage(String partner) {
        loginPage.validateInvalidIDErrorMessageInResetPasswordPage(partner);
    }

    public void noErrorMessageDisplay(String partner) {
        loginPage.validateNoErrorMessageinResetPasswordPage(partner);
    }

    public void invalidLoginError(String partnername) throws Throwable {
        loginPage.validateInvalidIDErrorMessage(partnername);
    }


    public void loginWithIdPassword(String personalID, String password) {
        loginPage.loginToLandingPage(personalID, password);
    }

    public void openUrl(String url) {
        getDriver().get(url);
    }


    public String getBaseUrl() {
        String stringToSearch = driverManager.getDriver().getCurrentUrl();

        Pattern p = Pattern.compile(REGEX_BASE_URL);   // the pattern to search for
        Matcher m = p.matcher(stringToSearch);

        // if we find a match, get the group
        if (m.find()) {
            // we're only looking for one group, so get it
            String baseUrl = m.group(0);

            // print the group out for verification
            logger.debug("Current Base Url:" + baseUrl);
            return baseUrl;
        }

        return "";
    }

    public void clickLandingNav(String tab) {
        switch (tab.toLowerCase()) {

            case "enroll":
                webNavigationPage.clickEnrollTab();
                break;
            case "login":
                webNavigationPage.clickLoginTab();
                break;
            case "contact":
                webNavigationPage.clickContactUsTab();
                break;
        }

    }

    /*added to logout*/
    public void doLogOut() {
        //to handle logout for omv view
        try {
            if (automationConfig.getView().equalsIgnoreCase("omv")) {
                loginPage.clickOmvLogOut();
            } else {
                loginPage.clickLogOut();
            }
        } catch (NoSuchElementException nsee) {
            logger.info("Logout link is not seen");
        }
        /*move to parent window*/
        String parentWindow = driverManager.getDriver().getWindowHandle();
        for (String curWindow : driverManager.getDriver().getWindowHandles()) {
            if (curWindow != parentWindow) {
                driverManager.getDriver().switchTo().window(parentWindow);
            }
        }
    }

    /*added to logout*/
    public void getLogOut() {
        try {
            //to handle logout for omv view
            if (automationConfig.getView().equalsIgnoreCase("omv")) {
                loginPage.clickOmvLogOut();
            } else {
                loginPage.clickLogOut();
            }
        } catch (NoSuchElementException nsee) {
            logger.info("Logout link is not seen");
        }
        /*move to parent window*/
        String parentWindow = driverManager.getDriver().getWindowHandle();
        for (String curWindow : driverManager.getDriver().getWindowHandles()) {
            if (curWindow != parentWindow) {
                driverManager.getDriver().switchTo().window(parentWindow);
            }
        }
    }

    /**
     * @param user
     * @param initPageObjects
     * @param browserType
     * @param e               environmentData file
     */
    public void loginWithUser(User user, boolean initPageObjects, BrowserType browserType, Optional<EnvironmentData> e) {
        //How to get User!
        //TestEnvironment testEnvironment = TestEnvironment.fromString(automationConfig.getPilotEnv());
        //user = testDataManager.getUserData(testEnvironment.toString(), regressionProfile.toString());

        //How to get TestData File
        //Optional<EnvironmentData> e = testDataManager.getEnvironmentsByEnvironmentSystem(environment, partner.value);
        //logger.info("loginToProfile-" + environment + ":" + user.getLoginId() + ":" + user.getPassword());

        RunMode currentRunmode = RunMode.valueOf(automationConfig.getRunMode());

        Assert.assertFalse("User Name is Empty!", user.getLoginId().isEmpty());
        Assert.assertFalse("User PW is Empty!", user.getPassword().isEmpty());
        Assert.assertNotNull("There are no user profiles loaded!", user);
        logger.info("user: " + user);
        logger.info("user enrolledInPaperlessStatements: " + user.getIsEnrolledInPaperlessStatements());

        if (initPageObjects) {
            logger.warn("Initializing Page Object Manager Driver!");
            initPageObjectDriver(currentRunmode, browserType);
        }
        if (!currentRunmode.equals(RunMode.REMOTE_SEETESTNATIVE)) {
            openUrl(e.get().getValue());
        } else if (driverManager.getDriver() instanceof AppiumDriver) {
            AppiumDriver driver = (AppiumDriver) driverManager.getDriver();
            driverManager.getDriver().findElement(By.id("Settings")).click();
            String settingEnv = addChar(automationConfig.getPilotEnv(), ' ', automationConfig.getPilotEnv().length() - 1);
            driverManager.getDriver().findElement(By.id(settingEnv)).click();
            logger.info(driver.getCapabilities().toString());
            driver.context("WEB");
        }
        getWebNavigationPage().waitForPrivacyPolicy();
        login(user.getLoginId(), user.getPassword());
        getMyAccountPage().waitForMyAccountPageToLoad();
    }


    public WebDriver getDriver() {
        return driverManager.getDriver();
    }

    /*-
    Page Objects Getters
     */

    public EnrollmentPage getEnrollmentPage() {
        return enrollmentPage;
    }
    public PDAPEnrollmentPage getPDAPEnrollmentPage() {
        return pdapenrollmentPage;
    }

    public MyAccountPage getMyAccountPage() {
        return myAccountPage;
    }

    public ESignAgreementPage getESignAgreementPage() {
        return eSignAgreementPage;
    }

    public ContactInformation getContactInformation() {
        return contactInformation;
    }

    public WebNavigationPage getWebNavigationPage() {
        return webNavigationPage;
    }
    public RewardsAndBenefitsPage getRewardsAndBenefitsPage(){
        return rewardsAndBenefitsPage;
    }

    public MyAccountOmvPage getMyAccountOmvPage() {
        return myAccountOmvPage;
    }

    public IdShieldPage getIdShieldPage() {
        return idShieldPage;
    }


    public LoginPage getLoginPage() {
        return loginPage;
    }

    public CheckStatus2Page getCheckStatus2Page() {
        return checkStatus2Page;
    }


    public TransactionDetailsModel getTransactionDetailsModel() {
        return transactionDetailsModel;
    }

    public DisputePurchaseTypePage getDisputePurchaseTypePage() {
        return disputePurchaseTypePage;
    }

    public FraudAppChkPage getFraudAppChkPage() {
        return fraudAppChkPage;
    }

    public UnAuthAccActFraudAppChkPage getUnAuthAccActFraudAppChkPage() {
        return unAuthAccActFraudAppChkPage;
    }

    public ChkProfileInfoPage getChkProfileInfoPage() {
        return chkProfileInfoPage;
    }

    public OtherTransactionsPage getOtherTransactionsPage() {
        return otherTransactionsPage;
    }

    public CardIssuancePage getCardIssuancePage() {
        return cardIssuancePage;
    }

    public ServicesPage getServicesPage() {
        return servicesPage;
    }

    public ClickToPayPage getClickToPayPage() {
        return clickToPayPage;
    }

    public ReviewTransactionsPage getReviewTransactionsPage() {
        return reviewTransactionsPage;
    }

    public ContactinfoPage getContactinfoPage() {
        return contactInfoPage;
    }

    public OTPScreenPage getOTPScreenPage() {
        return otpScreenPage;
    }

    public EnterOTPPage getEnterOTPPage() {
        return enterOTPPage;
    }

    public ContactUsForHelpPage getContactUsForHelpPage() {
        return contactUsForHelpPage;
    }

    public ReportCardLostOrStolenPage getReportCardLostOrStolenPage() {
        return reportCardLostOrStolenPage;
    }

    public ReviewRecentTransactionPage getReviewRecentTransactionPage() {
        return reviewRecentTransactionPage;
    }


    public OrderANewCardPage getOrderANewCardPage() {
        return orderANewCardPage;
    }

   // public CardDeliveryPage getCardDeliveryPage() {
       // return cardDeliveryPage;
   // }

    public CardReplacementSelectUsersPage getCardReplacementSelectUsersPage() {
        return cardReplacementSelectUsersPage;
    }

    public FraudDiscoveryPage getFraudDiscoveryPage() {
        return fraudDiscoveryPage;
    }

    public FraudSuspectPage getFraudSuspectPage() {
        return fraudSuspectPage;
    }

    public PoliceReportPage getPoliceReportPage() {
        return policeReportPage;
    }

    public PossessCardsPage getPossessCardsPage() {
        return possessCardsPage;
    }

    public CardUsePage getCardUsePage() {
        return cardUsePage;
    }

    public CardInHandPage getCardInHandPage() {
        return cardInHandPage;
    }

    public TravelPage getTravelPage() {
        return travelPage;
    }

    public OtherInfoPage getOtherInfoPage() {
        return otherInfoPage;
    }

    public MobileWalletPage getMobileWalletPage() {
        return mobileWalletPage;
    }

    public MailingInfoPage getMailingInfoPage() {
        return mailingInfoPage;
    }

    public ReviewPage getReviewPage() {
        return reviewPage;
    }

    public CardLostStolenPage getCardLostStolenPage() {
        return cardLostStolenPage;
    }

    public LessThan45Page getLessThan45Page() {
        return lessThan45Page;
    }

    public AddressChangedPage getAddressChangedPage() {
        return addressChangedPage;
    }

    public EmailPromptPage getEmailPromptPage() {
        return emailPromptPage;
    }

    public DigitalWalletPage getDigitalWalletPage() {
        return digitalWalletPage;
    }

    public FraudConfirmationPage getFraudConfirmationPage() {
        return fraudConfirmationPage;
    }

    public LockAndUnlockPage getLockOrUnlockCardPage() {
        return lockOrUnlockCardPage;
    }

    public DisputeReasonSelectionPage getDisputeReasonSelectionPage() {
        return disputeReasonSelectionPage;
    }

    /* added to access  ServicesLandingPage */
    public ServicesLandingPage getServicesLandingPage() {
        return servicesLandingPage;
    }

    /*added to access Agree And Continue page*/
    public AgreeAndContinuePage getAgreeAndContinuePage() {
        return agreeAndContinuePage;
    }

    /*added to access Contact method page*/
    public ContactMethodPage getContactMethodPage() {
        return contactMethodPage;
    }

    /*added to access Googlepay page*/
    public GooglePayPushProvision getGooglePayPage() {
        return googlePayPage;
    }

    /*added to take screen shot*/
    public CaptureScreenShots getCaptureScreenShots() {
        return captureAcreenshots;
    }

    public BalanceTransferPage getBalanceTransferPage() {
        return balanceTransferPage;
    }

    public LockAndUnlockPage getLockAndUnlockPage() {
        return lockAndUnlockPage;
    }

    public EnrollmentScriptPage getEnrollmentScriptPage() {
        return enrollmentScriptPage;
    }

    public ModifiedEnrollmentScriptPage getModifiedenrollmentScriptPage() { return modifiedenrollmentScriptPage; }

    public LostOrStolenPage getLostOrStolenPage() {
        return lostOrStolenPage;
    }

    public TravelNotificationPage getTravelNotificationPage() {
        return travelNotificationPage;
    }


    public PastPaymentDue getPastPaymentDue() {
        return pastPaymentDue;
    }

    public CombinedAccountsPage getCombinedAccountsPage() {
        return CombinedAccountsPage;
    }

    public AuthorizedOwnerPage getAuthorizedOwner() {
        return AuthorizedOwnerPage;
    }

    public AuthorizedEmployeePage getAuthorizedEmployee() {
        return authorizedEmployeePage;
    }

	public virtualCard  getVirtualCard() {
	    return virtualCard;
	}

    /*added to access SamsungPay page*/
    public SamsungPayPushProvision getsamsungPayPagePage() {
        return samsungPayPage;
    }

    public PaymentAccountAddViewDeletePage getPaymentAccountPage() {
        return paymentAccountPage;
    }

    public AutoPaySetupEditDeletePages getAutoPayPage() {
        return autoPayPage;
    }

    public RewardsLandingPage getRewardsPage() {
        return rewardsPage;
    }


    public AlertsLandingPage getAlertsLandingPage() {
        return alertsLandingPage;
    }

    public ProfileLandingPage getProfileLandingPage() {
        return profileLandingPage;
    }

    public WrapUpUnsupportedReasonPage getWrapUpUnsupportedReason() {
        return wrapUpUnsupportedReasonPage;
    }

    public WrapUpFraudPage getWrapUpFraudPage() {
        return wrapUpFraudPage;
    }

    public ContactValidationPage getContactValidationPage() {
        return contactValidationPage;
    }

    public IDverificationPage getIDverificationPage() {
        return iDverificationPage;
    }

    public void setDriver(DriverManager driverManager) {
        this.driverManager = driverManager;
    }

    public BrowserType getBrowserType() {
        return this.driverManager.getBrowserType();
    }


    public PayWithCardPage getpayWithCard() {
        return payWithCardpage;
    }

    public LeavingSitePage getLeavingSitepage() {
        return leavingSitepage;
    }

    public AlertsHistoryPage getAlertsHistoryPage() {
        return alertsHistoryPage;
    }

    public StepUpIdshieldPage getStepUpIdshieldPage() {
        return stepUpIdshieldPage;
    }

    public ChangeIDshieldImagePage getChangeIDshieldImagePage() {
        return changeIDshieldImagePage;
    }

    public PaymentOptionsPage getPaymentOptionsPage() {
        return paymentOptionsPage;
    }

    public PaymentsLandingPage getPaymentsLandingPage() {
        return paymentsLandingPage;
    }

    public OnlineSettlementsPage_Jalapenos getOnlineSettlementsPage() {
        return onlineSettlementsPage;
    }

    public com.usbank.test.automationFramework.trancore.pageObjects.fraud.SelectAccountPage getSelectAccountPage() {
        return selectAccountPage;
    }

    // Secure Messaging
    public SecureMessagePage getSecureMessagePage() {
        return secureMessagePage;
    }

    public EsignEnrollPage getEsignEnrollNewObj() {
        return esignEnrollPage;
    }

    public FrequentlyAskedQuestionsPage getFrequentlyAskedQuestions() {
        return frequentlyAskedQuestionsPage;
    }

    public AddAuthorizedUserPage getAddAuthorizedUserPage() {
        return addAuthorizedUserPage;
    }

    public AddJointOwnerPage getAddJointOwnerPage() {
        return addJointOwnerPage;
    }

    public ConvenienceCheckRequestPage getConvenienceCheckRequestPage() {
        return convenienceCheckRequestPage;
    }

    public SpendAnalysisPage getSpendAnalysisPage() {
        return spendAnalysisPage;
    }

    public ManageEmployeesPage getManageEmployeesPage() {
        return manageEmployeesPage;
    }

    public MakePaymentPages getMakePaymentPage() {
        return makePaymentPage;
    }

    public AddAuthorizedRepresentativePDFPage getAddAuthorizedRepresentativePDFPage() {
        return addAuthorizedRepresentativePDFPage;
    }

    public FreeCreditScorePage getFreeCreditScorePage() {
        return freeCreditScorePage;
    }

    public ReviewAndSubmitCardReplacementOrderPage getReviewandSubmitCardReplacementOrderpage() {
        return reviewandSubmitCardReplacementOrderpage;
    }

    public void navigateToDirectUrl(String url) {
        loginPage.navigateToDeepLinkLoginPage(url);
    }

    public CallUsNowToFinish getCallUsNowToFinish() {
        return callUsNowToFinish;
    }

    public IncomeAndAssetsPage getIncomeAndAssets() {
        return incomeAndAssetsPage;
    }

    public HouseInformationPage getHousingInformation() {
        return housingInformation;
    }

    public ReviewYourRequestPage getReviewYourRequest() {
        return reviewYourRequestPage;
    }

    public ReceivedRequestPage getReceivedRequest() {
        return receivedRequestPage;
    }

    public IncomePromptPage getIncomePrompt() {
        return incomePromptPage;
    }

    public AccountSummary getAccountSummary() {
        return accountSummary;
    }

    public SelectAccountPage getSelectAccount() {
        return slctAccountPage;
    }

    public InternalErrorPage getInternalErrorPage() {
        return internalErrorPage;
    }

    public PhonePromptPage getPhonePromptPage() {
        return phonePromptPage;
    }


    public DisputeThisTransactionPage getDisputeThisTransactionPage() {
        return disputeThisTransactionPage;
    }

    public ReasonForDisputePage getReasonForDisputePage() {
        return reasonForDisputePage;
    }

    public TellUsMorePage getTellUsMorePage() {
        return tellUsMorePage;
    }

    public ContactingMerchantPage getContactingMerchantPage() {
        return contactingMerchantPage;
    }

    public PurchaseDetailsPage getPurchaseDetailsPage() {
        return purchaseDetailsPage;
    }

    public PaymentTypePage getPaymentTypePage() {
        return paymentTypePage;
    }

    public PurchaseDocumentPage getPurchaseDocumentPage() {
        return purchaseDocumentPage;
    }

    public DisputeCaseContactInfoPage getDisputeCaseContactInfoPage() {
        return disputeCaseContactInfoPage;
    }

    public OtherCardUsedPage getOtherCardUsedPage() {
        return otherCardUsedPage;
    }

    public VerifyOtherFormOfPaymentPage getVerifyOtherFormOfPaymentPage() {
        return verifyOtherFormOfPaymentPage;
    }
    public VerifyingMailingAddress getVeryfingMailingAddress() {
        return verifyingMailingAddress;
    }
    public OtherFormOfPaymentPage getOtherFormOfPaymentPage() {
        return otherFormOfPaymentPage;
    }

    public ReviewAndSubmitPage getReviewAndSubmitPage() {
        return reviewAndSubmitPage;
    }

    public ChangePasswordPage getChangePasswordPage() {
        return changePasswordPage;
    }

    public ChangePersonalIDPage getChangePersonalIDPage() {
        return changePersonalIDPage;
    }

    public LandingForAlertsPage getLandingPageForAlerts() {
        return landingForAlertsPage;
    }

    public SSODeeplinkLoginPage getssoDeeplinkLoginPage() {
        return ssoDeeplinkLoginPage;
    }

    public IncomeAndEsignPage getIncomePromptPage() {
        return incomeAndEsignPage;
    }

    public DownloadAnnualAccountSummaryPage getDownloadAnnualAccountSummaryPage() {
        return downloadAnnualAccountSummaryPage;

    }

    public ShortTermPlanDetailsPage getShortTermPlanDetailsPage() {
        return shortTermPlanDetailsPage;
    }

    public ShortTermFundingAccountPage getShortTermFundingAccountPage() {
        return shortTermFundingAccountPage;
    }

    public HardshipQuestionnairePage getHardshipQuestionnairePage() {
        return hardshipQuestionnairePage;
    }

    public HardshipQuestionnaire1Page getHardshipQuestionnaire1Page() {
        return hardshipQuestionnaire1Page;
    }

    public HardshipQuestionnaire2Page getHardshipQuestionnaire2Page() {
        return hardshipQuestionnaire2Page;
    }

    public OnlineStatementsPage getOnlineStatementsPage() {
        return onlineStatementsPage;
    }

    public DiscontinuePaperStatementsStep1 getDiscontinuePaperStatementsStep1() {
        return discontinuePaperStatementsStep1;
    }


    public LoginAssistancePage getLoginAssistancePage() {
        return loginAssistancePage;
    }

    public RetrievePersonalID getRetrievePersonalID() {
        return retrievePersonalID;
    }

    public PersonalIDRetrieved getPersonalIDRetrieved() {
        return personalIDRetrieved;
    }

    public AddNewPhoneNumberPage getAddNewPhoneNumberPage() {
        return addNewPhoneNumberPage;
    }

    public UpdateMailingAddress getUpdateMailingAddress() {
        return updateMailingAddress;
    }

    public AddEmailAddressPage getAddEmailAddressPage() {
        return addEmailAddressPage;
    }

    public RetrievePasswordPage getRetrievePasswordPage() {
        return retrievePasswordPage;
    }

    public FAQPage getFaqPage() {
        return faqPage;
    }

    public CardActivationPage getCardActivationPage() {
        return cardActivationPage;
    }

    public UnAuthenticatedActivation getUnAuthenticatedActivation() {
        return unAuthenticatedActivation;
    }

    public AddAuPromptPage getAddAUPromptPage() {
        return addAUPromptPage;
    }

    public ChargeOffPage getChargeOffPage() {
        return chargeOffPage;
    }

    public LongTermPlanDetailsPage getLongTermPlanDetailsPage() {
        return longTermPlanDetailsPage;
    }

    public static String addChar(String str, char ch, int position) {
        return str.substring(0, position) + ch + str.substring(position);
    }


}