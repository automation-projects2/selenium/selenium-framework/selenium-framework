package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FraudSuspectPage<T extends WebDriver> {

    protected static final Logger logger = LogManager.getLogger(FraudSuspectPage.class);

    protected T driver;


    public FraudSuspectPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "5004-Y")
    private WebElement rdbYes;

    @FindBy(id = "5004-N")
    private WebElement rdbNo;

    @FindBy(id = "5005")
    private WebElement txtAreaPersonName;

    @FindBy(id = "5006")
    private WebElement txtAreaRelationship;

    @FindBy(id = "5007")
    private WebElement txtAreaPhoneNo;

    @FindBy(id = "5008")
    private WebElement txtAreaAddress;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement btnContinue;

    @FindBy(xpath = "//*[text()='Who might have been involved?'")
    private WebElement lblSuspectPage;

    public WebElement getRdbNo() {
        return rdbNo;
    }

    public WebElement getRdbYes() {
        return rdbYes;
    }

    public WebElement getTxtAreaPersonName() {
        return txtAreaPersonName;
    }

    public WebElement getTxtAreaRelationship() {
        return txtAreaRelationship;
    }

    public WebElement getTxtAreaPhoneNo() {
        return txtAreaPhoneNo;
    }

    public WebElement getTxtAreaAddress() {
        return txtAreaAddress;
    }


    public WebElement getLblSuspectPage() {
        return lblSuspectPage;
    }


    public void waitForFraudSuspectPageToLoad() {

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(getRdbNo()));
    }

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        ele.click();
    }

    public void clickNoRadioButton() {
        waitForFraudSuspectPageToLoad();
        clickElement(driver, rdbNo);
    }

    public void clickYesRadioButton() {
        waitForFraudSuspectPageToLoad();
        clickElement(driver, rdbYes);
    }

    public void clickContinue() {
        clickElement(driver, btnContinue);
    }

}
