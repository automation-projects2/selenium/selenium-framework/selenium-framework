package com.usbank.test.automationFramework.trancore.pageObjects.internalError;

import com.usbank.test.objects.EnvironmentData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

public class InternalErrorPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(InternalErrorPage.class);

    private static final long EXPLICIT_WAIT_TIME_SECONDS = 30;

    private T driver;
    private EnvironmentData environmentData;

    private AutomationConfig automationConfig;


    @FindBy(how = How.ID, using = "subHeader")
    private WebElement headerName;

    @FindBy(how = How.CLASS_NAME, using = "omv_pageText--block")
    private WebElement bodyText;


    public InternalErrorPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    public InternalErrorPage(AutomationConfig automationConfig) {
        this.automationConfig = automationConfig;
    }

    public void verifyHeaderName() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(bodyText));
        assertEquals("internal error", headerName.getText().toLowerCase());
    }

    public void navigateToRecentReviewTransactionPage(){
        String url = driver.getCurrentUrl();
        int end = url.indexOf("customer");
        url = url.substring(0,end);

        url = url + "reportCardLostStolen.do?phase=reviewRecentTransactions";
        System.out.println("url to access Review Recent Transaction "+ url);
                driver.navigate().to(url);
    }
    public void navigateToUpdateContactInformationPage(){
        String url = driver.getCurrentUrl();
        int end = url.indexOf("profile.do");
        url = url.substring(0,end);
        url = url + "changeAddress.do?phase=start";
        System.out.println("url to update contact information is "+ url);
        driver.navigate().to(url);
    }


    public void navigateToUpdateMailingAddressPage() {
        String url = driver.getCurrentUrl();
        int end = url.indexOf("profile.do");
        url = url.substring(0, end);
        url = url + "updateMailingAddress.do";
        System.out.println("url to Update Mailing Address is " + url);
        driver.navigate().to(url);

    }

    public void navigateToAddEmailAddressPage() {
        String url = driver.getCurrentUrl();
        int end = url.indexOf("onlineCard");
        url = url.substring(0, end);
        url = url + "onlineCard/addEmailAddress.do?phase=landingPage";
        System.out.println("url to Add Email Address is " + url);
        driver.navigate().to(url);
    }

    public void navigateToAddAuthorizedAU() {
        String url = driver.getCurrentUrl();
        int end = url.indexOf("onlineCard");
        url = url.substring(0, end);
        url = url + "onlineCard/authorizedUser.do?phase=redirectToAU";
        System.out.println("url to Add Author Address is " + url);
        driver.navigate().to(url);
    }

    public void navigateToCardReplacementDeliveryOptionsPage() {
        String url = driver.getCurrentUrl();
        int end = url.indexOf("onlineCard");
        url = url.substring(0, end);
        url = url + "onlineCard/cardReplacement.do?phase=deliveryOptions";
        System.out.println("url to CardReplacement DeliveryOptions Page is " + url);
        driver.navigate().to(url);
    }

	public void navigateToCardReplacementSelectUsersPage() {
		String url = driver.getCurrentUrl();
        int end = url.indexOf("onlineCard");
        url = url.substring(0, end);
        url = url + "onlineCard/cardReplacement.do?phase=cardSelection";
        System.out.println("url to CardReplacement Select Users Page is " + url);
        driver.navigate().to(url);

	}
}


