# google Pay
@googlePay

Feature: Google Pay
  This test cases ensures that the Google Pay feature is working is able to complete the Google pay flow

  @GooglePayPushProvision @PrimaryOwner @JointOwner
  Scenario Outline:Verify user is Trancore primary or joint owner is able to see the Google Pay link
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user has navigated to the services page
    Then display google Pay link
    And Google Pay Image is displayed

    Examples:
      | partner | testData      |
      | Elan    | ELAN_CONSUMER |
      | Elan    | ELAN_CONSUMER |


  @GooglePayPushProvision @CoSigner
  Scenario Outline:Verify cosigner user is not able to see the Google Pay link
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user has navigated to the services page
    And the user is a cosigner
    Then  do not display google Pay link and image

    Examples:
      | partner | testData      |
      | Elan    | ELAN_COSIGNER |
      | Elan    | ELAN_COSIGNER |


  @GooglePayPushProvision @BusinessAccount
  Scenario Outline:Verify business account user is not able to see the link
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user has navigated to the services page
    And the user is Business user
    Then  do not display google Pay link and image

    Examples:
      | partner | testData          |
      | Elan    | ELAN_BUSINESS_UAT |
      | Elan    | ELAN_BUSINESS_UAT |

  @GooglePayPushProvision @TenuredUser
  Scenario Outline:Verify tenured user is able to see the Google Pay link
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user has navigated to the services page
    And the user is a tenured
    Then display google Pay link and image

    Examples:
      | partner | testData      |
      | Elan    | ELAN_CONSUMER |
      | Elan    | ELAN_CONSUMER |


  @GooglePayPushProvision @Eligibility
  Scenario Outline:Verify eligible user is able to see the Google Pay link
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user has navigated to the services page
    And the user is eligible
    Then display google Pay link and image

    Examples:
      | partner | testData      |
      | Elan    | ELAN_CONSUMER |
      | Elan    | ELAN_CONSUMER |


  @GooglePayPushProvision @CombinedAccount
  Scenario Outline:Verify user with combined account  is able to see the Google Pay link
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user has navigated to the services page
    And the user is eligible
    Then display google Pay link and image
    Then the user select the Select Account tab
    And verify the account type "Combined" and then select the account <accountHolder>
    Then the user has navigated to the services page
    And if the account is eligible
    Then display google Pay link and image

    Examples:
      | partner           | testData            | accountHolder    |
      | HarleyDavidson    | HD_COMBINED_ACCOUNT | 4037670340428274 |


  @GooglePayPushProvision @DisplayPushProvisionInitiationScreen
  Scenario Outline:Verify eligible user is able to see push provision initiation screen
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user has navigated to the services page
    And the user is eligible
    Then display google Pay link and image
    And the user clicks on google pay link
    Then display push provision initiation screen to the user
    And verify continue button is displayed
    And verify cancel button is displayed

    Examples:
      | partner | testData      |
      | Elan    | ELAN_CONSUMER |
      | Elan    | ELAN_CONSUMER |

  @GooglePayPushProvision @DisplaySecondaryAuthenticationScreen
  Scenario Outline:Verify user is able to display Secondary Authentication Screen after clicking on continue button
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user is on Push Provision Initiation page of google pay
    And the user clicks on the Continue button
    Then display Secondary Authentication Screen
    And verify contact information text is displayed
    And verify the contact methods
    And Verify warning information is displayed
    And verify  send a one time code button is displayed
    And verify cancel button is displayed

    Examples:
      | partner | testData      |
      | Elan    | ELAN_CONSUMER |
      | Elan    | ELAN_CONSUMER |


  @GooglePayPushProvision @DisplayOTPVerificationScreen
  Scenario Outline:Verify user is able to display OTP verification Screen after clicking on send a one time code button
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user is on Secondary Authentication Screen of google pay
    And the user clicks on the Send a one time code button
    Then display OTP verification screen
    And verify enter OTP text is displayed
    And verify enter six digit code heading is displayed
    And verify input field to enter the OTP  is displayed
    And verify continue button is displayed
    And verify send a new code button is displayed
    And verify use a another method button is displayed for multiple contact user

    Examples:
      | partner | testData      |
      | Elan    | ELAN_CONSUMER |
      | Elan    | ELAN_CONSUMER |


  @GooglePayPushProvision @ConfirmationMessageInOtpVerificationScreen
  Scenario Outline:Verify confirmation message is displayed in OTP verification screen after clicking on send a new code button
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user is on OTP verification screen of the google pay
    And the user click on Send a new code button
    Then code will be sent to the user and will stay on the same page
    And verify confirmation message is displayed

    Examples:
      | partner | testData      |
      | Elan    | ELAN_CONSUMER |
      | Elan    | ELAN_CONSUMER |

  @GooglePayPushProvision @UIValidationOnOTPScreen
  Scenario Outline:verify error messages are displayed when the user enters incorrect OTP
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user is on OTP verification screen of the google pay
    And the user enters the <otp> incorrectly
    And the user clicks on the Continue button
    Then verify the error message displayed in the <otp> verification screen

    Examples:
      | partner | testData      | otp    |
      | Elan    | ELAN_CONSUMER | 65432  |
      | Elan    | ELAN_CONSUMER | 123678 |


  @GooglePayPushProvision @DisplaySecondaryAuthenticationScreenWhenUseAnotherMethodButtonClicked
  Scenario Outline:verify Secondary Authentication Screen is  displayed when the user clicks on theUse Another Method Button
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user is on OTP verification screen of the google pay
    And the user clicks on Use Another Method Button
    Then display Secondary Authentication Screen for selecting the contact

    Examples:
      | partner | testData      |
      | Elan    | ELAN_CONSUMER |
      | Elan    | ELAN_CONSUMER |


  @GooglePayPushProvision @SecondaryAuthenticationScreenCancelButton @CancelButton
  Scenario Outline:Verify user click on Cancel button on secondary authentication screen redirects him to Services Page
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user is on Secondary Authentication Screen of google pay
    And the user click on the Cancel button
    Then display the Services page

    Examples:
      | partner | testData      |
      | Elan    | ELAN_CONSUMER |
      | Elan    | ELAN_CONSUMER |

  @GooglePayPushProvision @NonMatchingOtpFirstCode3rdAttempt
  Scenario Outline:Verify when user enters non-matching OTP first code 3rd attempt display secondary authentication screen with appropriate error message
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user is on OTP verification screen of the google pay
    Then user attempted three Invalid <otp> attempts
    Then display Secondary Authentication Screen
    And verify appropriate error message displayed
    And verify  send a one time code button is displayed
    And verify cancel button is displayed

    Examples:
      | partner | testData      | otp    |
      | Elan    | ELAN_CONSUMER | 65432  |
      | Elan    | ELAN_CONSUMER | 123678 |

  @GooglePayPushProvision @HideSendANewCodeButton
  Scenario Outline:Verify user has sent 2 OTP codes and get to the OTP Verification screen hide "Send a New code" button
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user is on Secondary Authentication Screen of google pay for first attempt
    And the user click on the Cancel Link
    Then the user is on Secondary Authentication Screen of google pay for second attempt
    And verify send a new code button is hidden

    Examples:
      | partner | testData      |
      | Elan    | ELAN_CONSUMER |
      | Elan    | ELAN_CONSUMER |


  @GooglePayPushProvision @NonMatchingOtpSecondCode3rdAttemptDisplayOTPFailureScreen
  Scenario Outline:Verify When User enters non-matching OTP Second code 3rd attempt display OTP Code Failure screen
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user is on OTP verification screen of google pay after second OTP is send
    Then user attempted three Invalid <otpcode> attempts
    Then display OTP code Failure screen

    Examples:
      | partner | testData      | otpcode |
      | Elan    | ELAN_CONSUMER | 654321  |
      | Elan    | ELAN_CONSUMER | 123678  |

  @GooglePayPushProvision @UserExceeds2OTPCountAndDisplayOTPFailureWrapUpScreen
  Scenario Outline:Verify When User clicks google Pay after exceeding 2 OTP count display OTP Code Failure screen
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user is on the services page and exceeds two OTP <otp> count
    And the user clicks on google pay link
    Then display OTP Failure Wrap Up screen of google pay

    Examples:
      | partner | testData      | otp    |
      | Elan    | ELAN_CONSUMER | 65432  |
      | Elan    | ELAN_CONSUMER | 123678 |

  @GooglePayPushProvision @SendANewCodeButtonWhenTheUserEntersTheFirstCodeOTPIncorrectlyThreeTimes
  Scenario Outline:verify Send a new code button is displayed and functional when the user enters the first code OTP incorrectly for three times
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user is on secondary authentication screen of google pay and enter the first code <otp> incorrectly for three times
    Then the user clicks on the Send a one time code button
    And the OTP code is sent to the user
    Then display OTP verification screen

    Examples:
      | partner | testData      | otp    |
      | Elan    | ELAN_CONSUMER | 654322 |
      | Elan    | ELAN_CONSUMER | 123678 |


  @GooglePayPushProvision @HideUseAnotherMethodButton
  Scenario Outline:Verify user has sent 2 OTP codes and get to the OTP Verification screen hide "Use another method" button
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user is on OTP verification screen of google pay after second OTP is send
    Then verify use a another method button is hidden for multiple contact user

    Examples:
      | partner | testData      |
      | Elan    | ELAN_CONSUMER |
      | Elan    | ELAN_CONSUMER |

  @GooglePayPushProvision @DisplayAddedStatus
  Scenario Outline:Verify card added status is displayed along with the google pay link in the service page
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    And the user has added the card manually
    When the user has navigated to the services page
    Then display google Pay link with card added status

    Examples:
      | partner | testData      |
      | Elan    | ELAN_CONSUMER |
      | Elan    | ELAN_CONSUMER |

  @GooglePayPushProvision @DisplayInfoScreen
  Scenario Outline:Verify Info screen is displayed to the user after clicking on added status
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    And the user has added the card manually
    When the user has navigated to the services page
    Then display google Pay link with card added status
    And the user clicks on google pay link
    Then display info screen

    Examples:
      | partner | testData      |
      | Elan    | ELAN_CONSUMER |
      | Elan    | ELAN_CONSUMER |


  @GooglePayPushProvision @BusinessUser @GooglePayLinkPresent
  Scenario Outline:Verify Google pay link is displayed for user with business account
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user has navigated to the services page
    And the user has the eligible account
    Then display google Pay link and image

    Examples:
      | partner | testData      |
      | Elan    | ELAN_CONSUMER |
      | Elan    | ELAN_CONSUMER |


  @GooglePayPushProvision @BusinessAccount @CentralBill
  Scenario Outline:Verify Google pay link is not displayed to the user with Central bill account
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    Then the user select the Select Account tab
    And verify the account type "CentralBill" and then select the account <accountHolder>
    Then the user has navigated to the services page
    And if the account is  a CB account
    Then  do not display google Pay link and image

    Examples:
      | partner | testData          | accountHolder               |
      | Elan    | ELAN_BUSINESS_UAT | RICHARD R MORRISON-GRADNOSKE|

