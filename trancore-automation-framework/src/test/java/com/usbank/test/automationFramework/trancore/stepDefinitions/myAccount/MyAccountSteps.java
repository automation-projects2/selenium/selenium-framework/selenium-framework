package com.usbank.test.automationFramework.trancore.stepDefinitions.myAccount;

import com.usbank.test.automationFramework.trancore.pageObjects.fraud.MyAccountPage;
import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

@RunWith(Cucumber.class)
public class MyAccountSteps {

    @Autowired
    private StepDefinitionManager stepDefinitionManager;

    private static final String OMV = "OMV";

    private static final String TAB_NAME_POSTED = "posted";

    private static final String TAB_NAME_PENDING ="pending";

    @Autowired
    private AutomationConfig config;

    @When("^the user clicks the POSTED Tab$")
    public void the_user_clicks_the_posted_tab() throws Throwable {

       stepDefinitionManager.getPageObjectManager().getMyAccountPage().clicktabPosted();

    }

    @When("^the user is on the account page$")
    public void the_user_is_on_the_account_page() throws Throwable {
        stepDefinitionManager.getPageObjectManager().getMyAccountPage();
    }

    @Then("^the user selects an option under profile settings")
    public void the_user_selects_an_option_under_profile_settings() throws Throwable {
        stepDefinitionManager.getPageObjectManager().getAlertsLandingPage().AlertHistory();
    }

    @When("^the user clicks the PENDING Tab$")
    public void the_user_clicks_the_pending_tab() throws Throwable {

        stepDefinitionManager.getPageObjectManager().getMyAccountPage().clicktabPending();
    }


    @And("^the user selects transaction with (.+) and (.+)$")
    public void the_user_selects_transaction(String transactionDate, String transactionDescription){

        MyAccountPage myAccountPage = null;

        if(config.getView().equalsIgnoreCase("OMV")) {
            myAccountPage = stepDefinitionManager.getPageObjectManager().getMyAccountOmvPage();
        }else{
            myAccountPage = stepDefinitionManager.getPageObjectManager().getMyAccountPage();
        }

        myAccountPage.clickDisputeTransactionForTxnDateAndDescription(TAB_NAME_POSTED, transactionDate, transactionDescription );

        if(config.getView().equalsIgnoreCase("OMV"))
        stepDefinitionManager.getPageObjectManager().getTransactionDetailsModel().clickLnkDisputeTransaction();
    }

    @And("^the user selects pending transaction with (.+) and (.+)$")
    public void the_user_selects_pending_transaction(String pendingTransactionDate, String pendingTransactionDescription) {

        MyAccountPage myAccountPage = null;

        if(config.getView().equalsIgnoreCase("OMV")) {
            myAccountPage = stepDefinitionManager.getPageObjectManager().getMyAccountOmvPage();
        }else{
            myAccountPage = stepDefinitionManager.getPageObjectManager().getMyAccountPage();
        }

        myAccountPage.clickPendingDisputeTransactionForTxnDateAndDescription(TAB_NAME_PENDING, pendingTransactionDate, pendingTransactionDescription );

        if(config.getView().equalsIgnoreCase("OMV"))
            stepDefinitionManager.getPageObjectManager().getTransactionDetailsModel().clickLnkDisputeTransaction();
    }

}