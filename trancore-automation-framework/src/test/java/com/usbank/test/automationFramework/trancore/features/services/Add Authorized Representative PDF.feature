@CardmemberServicesSuite @AddAuthorizedRepPDF @WIP @Regression
Feature: Verify Add Authorized Representative PDF functionality

  @TC001
  Scenario Outline: Add Authorized Representative PDF
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When I click Services Tab
    And I click link -  Add Authorized Representative PDF
    Then I can view the Add Authorized Representative PDF

    Examples:
      | partner     | testData |
