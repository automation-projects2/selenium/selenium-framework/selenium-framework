package com.usbank.test.automationFramework.trancore.pageObjects.rewards;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.concurrent.TimeUnit;

public class RewardsAndBenefitsPage {
    @Autowired
    StepDefinitionManager stepDefinitionManager;

    private WebDriver driver;
    public  JavascriptExecutor js = (JavascriptExecutor)driver;
    private static final Logger logger = LogManager.getLogger(RewardsAndBenefitsPage.class);
    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Rewards')]")
    private WebElement linkToRewards;
    @FindAll({@FindBy(how = How.LINK_TEXT, using = "REWARDS & BENEFITS"), @FindBy(how = How.LINK_TEXT, using = "Rewards & Benefits")})
    private WebElement linkRewardsAndBenefitsPage;
    @FindBy(how = How.XPATH, using = "//h1[contains(text(),'Rewards & Benefits Summary')]")
    private WebElement rewardsAndBenefitsSummary;
    @FindBy(how = How.XPATH, using = "//h3[contains(text(),'CURRENTLY EARNING')]")
    private WebElement currentPoints;
    @FindBy(how = How.XPATH, using = "//h3[contains(text(),'REWARDS BALANCE AND EARNINGS')]")
    private WebElement rewardsBalanceAndEarnings;
    @FindBy(how = How.XPATH, using = "//h3[contains(text(),'PAST REWARDS EARNED')]")
    private WebElement pastPointsEarned;
    @FindBy(how = How.XPATH, using = "//h3[contains(text(),'PAST REWARDS EARNED')]")
    private WebElement pastRewardsEarned;
    @FindBy(how = How.XPATH, using = "//h3[contains(text(),'POINTS EXPIRING')]")
    private WebElement pointsExpired;
    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Redeem points at Rewards Center')]")
    private WebElement redeemPointsAtRewardsCenterLink;
    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Redeem rewards at Rewards Center')]")
    private WebElement elanRedeemPointsAtRewardsCenterLink;
    @FindBy(how = How.XPATH, using = "//h3[contains(text(),'Account Management')]")
    private WebElement accountManagement;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Automatic Bill Pay')]")
    private WebElement automaticBillPayLink;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'24/7 Account Access')]")
    private WebElement accountAccess;
    @FindBy(how=How.XPATH, using = "//h3[contains(text(),'Mobile App')]")
    private WebElement mobileApp;
    @FindBy(how=How.XPATH, using = "//h3[contains(text(),'Support')]")
    private WebElement support;
    @FindBy(how=How.XPATH, using = "//h3[contains(text(),'Special Offers')]")
    private WebElement specialOffers;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Free credit score')]")
    private WebElement freecreditScore;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Lock/Unlock card')]")
    private WebElement lockUnlockCode;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Digital Wallet')]")
    private WebElement digitalWallet;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Cardmember Support')]")
    private WebElement cardMemberSupport;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Auto Bill Pay')]")
    private WebElement autoBillPay;
    @FindBy(how=How.XPATH, using = "//h3[contains(text(),'Travel Benefits')]")
    private WebElement travelBenefits;
    @FindBy(how=How.XPATH, using = "//h3[contains(text(),'Security')]")
    private WebElement security;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Auto Rental Collision Damage Waiver')]")
    private WebElement autoRentalCollisionDamageWaiver;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Contactless/Chip Security')]")
    private WebElement contactlessChipSecurity;
    @FindBy(how=How.XPATH, using = "//a[contains(text(),'Travel And Emergency Assistance Services')]")
    private WebElement TravelAndEmergencyAssistanceServices;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Emergency Cash and Card Replacement')]")
    private WebElement emergencyCashAndCardReplacement;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Free Credit Score')]")
    private WebElement elanFreeCreditScore;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Lock/Unlock Card')]")
    private WebElement elanLockUnlockCard;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Account Alerts')]")
    private WebElement accountAlerts;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Zero Fraud Liability')]")
    private WebElement elanZeroFraudLiability;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Fraud Protection')]")
    private WebElement fraudProtection;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Redeem points for H-D™ Gift Cards')]")
    private WebElement redeemPointsGiftCards;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Mobile app')]")
    private WebElement elamMobileApp;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Check Balances and Review Transactions')]")
    private WebElement checkBalancesAndReviewTransactions;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Enroll in Security Alerts')]")
    private WebElement enrollinSecurityAlerts;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Spend Analysis & Annual Summary')]")
    private WebElement spendAnalysisAndAnnualSummary;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Live U.S. Based Cardmember Service Representatives')]")
    private WebElement liveUSBasedCardMember;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Card Protection: Zero Fraud Liability')]")
    private WebElement zeroFraudLiability;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Online Credit Management Tools')]")
    private WebElement onlineCreditManagementTools;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Payment Date Flexibility')]")
    private WebElement paymentDateFlexibility;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Lost/Stolen Card Reporting')]")
    private WebElement lostStolenCardReporting;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Add Authorized Users')]")
    private WebElement addAuthorizedUsers;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Account Alerts')]")
    private WebElement elanaccountAlerts;
    @FindBy(how=How.XPATH, using = "//div[contains(text(),'Online Statements')]")
    private WebElement onlineStatements;
   //@FindBy(how=How.XPATH, using = "//div/a[contains(text(),'See all my benefits')]")
   @FindBy(how=How.ID, using = "linkoutBenefits")
    private WebElement seeAllMyBenefitsLink;

    public RewardsAndBenefitsPage(WebDriver inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public void waitForElement(WebElement element, int waitTime){

        WebDriverWait wait=new WebDriverWait(driver, waitTime);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }
    public void clickRewardsAndBenefitsLink(){
        waitForElement(linkRewardsAndBenefitsPage, 500);
        linkRewardsAndBenefitsPage.click();
    }
    public void rewardsAndBenefitsSummaryDisplayed(String partner){
        if(partner.equalsIgnoreCase("HarleyDavidson")) {
        waitForElement(rewardsAndBenefitsSummary, 500);
        Assert.assertTrue("R&B Summary is not Displayed",rewardsAndBenefitsSummary.isDisplayed());
    }else if(partner.equalsIgnoreCase("Elan")){
        waitForElement(rewardsAndBenefitsSummary, 20);
        Assert.assertTrue("R&B Rewards And Benefits Summary is not Displayed", rewardsAndBenefitsSummary.isDisplayed());
    }
}
    public void rewardsAndBenefitsCurrentPointsDisplayed(String partner){
        if(partner.equalsIgnoreCase("HarleyDavidson")) {
            waitForElement(currentPoints, 20);
            Assert.assertTrue("R&B currentPoints is not Displayed", currentPoints.isDisplayed());
        }else if(partner.equalsIgnoreCase("Elan")){
            waitForElement(rewardsBalanceAndEarnings, 20);
            Assert.assertTrue("R&B currentPoints is not Displayed", rewardsBalanceAndEarnings.isDisplayed());
        }
    }
    public void rewardsAndBenefitsPastPointsEarnedDisplays(String partner){
        if(partner.equalsIgnoreCase("HarleyDavidson")) {
            waitForElement(pastPointsEarned, 20);
            Assert.assertTrue("R&B Past Points Earned is not Displayed", pastPointsEarned.isDisplayed());
        } else if(partner.equalsIgnoreCase("Elan")){
            waitForElement(pastRewardsEarned, 20);
            Assert.assertTrue("R&B PointsEarned is not Displayed", pastRewardsEarned.isDisplayed());
        }
    }
    public void rewardsAndBenefitsPointsExpired(String partner){
        if(partner.equalsIgnoreCase("HarleyDavidson")) {
            waitForElement(pointsExpired, 20);
            Assert.assertTrue("R&B Points Expired is not Displayed", pointsExpired.isDisplayed());
        }else if(partner.equalsIgnoreCase("Elan")){
            System.out.println("Elan partner will not diaplay point expired" );
        }
    }
    public void rewardsAndBenefitsRedeemPointLink(String partner){
        if(partner.equalsIgnoreCase("HarleyDavidson")) {
        waitForElement(redeemPointsAtRewardsCenterLink, 20);
        Assert.assertTrue("R&B Redeem Point Link is not Displayed",redeemPointsAtRewardsCenterLink.isDisplayed());
        redeemPointsAtRewardsCenterLink.click();
        } else if(partner.equalsIgnoreCase("Elan")){
            waitForElement(elanRedeemPointsAtRewardsCenterLink, 20);
            Assert.assertTrue("R&B  Redeem point rewards link is not Displayed", elanRedeemPointsAtRewardsCenterLink.isDisplayed());
            elanRedeemPointsAtRewardsCenterLink.click();
        }
    }
    public void rewardsAndBenefitsAccountManagementLink(String partner){
        if(partner.equalsIgnoreCase("HarleyDavidson")) {
        waitForElement(accountManagement, 20);
        Assert.assertTrue("R&B Account Management Link is not Displayed",accountManagement.isDisplayed());
        Assert.assertTrue("R&B Automatic bill pay is not displayed",automaticBillPayLink.isDisplayed());
        Assert.assertTrue("R&B Free credit score is not displayed",freecreditScore.isDisplayed());
        Assert.assertTrue("R&B Lock Unlock Code is not displayed",lockUnlockCode.isDisplayed());
        Assert.assertTrue("R&B Card Member Support is not displayed",cardMemberSupport.isDisplayed());
        Assert.assertTrue("R&B R&B Zero Fraud Liability is not Displayed is not displayed",zeroFraudLiability.isDisplayed());
        Assert.assertTrue("R&B Lost Stolen Card Reporting  Score is not displayed",lostStolenCardReporting.isDisplayed());
        Assert.assertTrue("R&B Account Alert is not displayed",accountAlerts.isDisplayed());
        } else if(partner.equalsIgnoreCase("Elan")) {
            waitForElement(accountManagement, 20);
            Assert.assertTrue("R&B Account Management Link is not Displayed",accountManagement.isDisplayed());
            Assert.assertTrue("R&B Account Access is not displayed",accountAccess.isDisplayed());
            Assert.assertTrue("R&B Mobile App is not displayed",elamMobileApp.isDisplayed());
            Assert.assertTrue("R&B Digital Wallet is not displayed",digitalWallet.isDisplayed());
            Assert.assertTrue("R&B Auto Bill Pay is not displayed",autoBillPay.isDisplayed());
            Assert.assertTrue("R&B Payment Date Flexibility is not Displayed is not displayed",paymentDateFlexibility.isDisplayed());
            Assert.assertTrue("R&B Add Authorized Users is not displayed",addAuthorizedUsers.isDisplayed());
            Assert.assertTrue("R&B Online statements is not displayed",onlineStatements.isDisplayed());
        }
    }
    public void rewardsAndBenefitsTravelBenefitsLink(String partner){
        if(partner.equalsIgnoreCase("HarleyDavidson")) {
        waitForElement(travelBenefits, 20);
        Assert.assertTrue("R&B Travel Benefits Link is not Displayed",travelBenefits.isDisplayed());
        Assert.assertTrue("R&B Auto Rental Collision Damage Waiver is not Displayed",autoRentalCollisionDamageWaiver.isDisplayed());
        Assert.assertTrue("R&B Emergency Cash And Card Replacement Link is not Displayed",emergencyCashAndCardReplacement.isDisplayed());
        } else if(partner.equalsIgnoreCase("Elan")) {
            waitForElement(security, 20);
            Assert.assertTrue("R&B Travel Benefits Link is not Displayed",security.isDisplayed());
            Assert.assertTrue("R&B Contactless Chip Security is not Displayed",contactlessChipSecurity.isDisplayed());
            Assert.assertTrue("R&B Free Credit Score is not Displayed",elanFreeCreditScore.isDisplayed());
            Assert.assertTrue("R&B Lock Unlock Card is not Displayed",elanLockUnlockCard.isDisplayed());
            Assert.assertTrue("R&B Account Alerts is not Displayed",elanaccountAlerts.isDisplayed());
            Assert.assertTrue("R&B Zero Fraud Liability is not Displayed",elanZeroFraudLiability.isDisplayed());
            Assert.assertTrue("R&B Fraud Protection is not Displayed",fraudProtection.isDisplayed());
        }
    }
    public void rewardsAndBenefitsMobileAppLink(String partner){
        if(partner.equalsIgnoreCase("HarleyDavidson")) {
        waitForElement(mobileApp, 20);
        Assert.assertTrue("R&B Mobile App Link is not Displayed",mobileApp.isDisplayed());
        Assert.assertTrue("R&B Redeem points gift card is not Displayed",redeemPointsGiftCards.isDisplayed());
        Assert.assertTrue("R&B Lost/Stolen Card Reporting is not Displayed",checkBalancesAndReviewTransactions.isDisplayed());
        Assert.assertTrue("R&B Mobile App Link is not Displayed",enrollinSecurityAlerts.isDisplayed());
        } else if(partner.equalsIgnoreCase("Elan")) {
            waitForElement(security, 20);
            Assert.assertTrue("R&B Support is not Displayed",support.isDisplayed());
            Assert.assertTrue("R&B Support is not Displayed",specialOffers.isDisplayed());
            Assert.assertTrue("R&B Travel Benefits Link is not Displayed",elamMobileApp.isDisplayed());
            Assert.assertTrue("R&B Travel Benefits Link is not Displayed",liveUSBasedCardMember.isDisplayed());
            Assert.assertTrue("R&B Travel Benefits Link is not Displayed",onlineCreditManagementTools.isDisplayed());
            Assert.assertTrue("R&B Travel Benefits Link is not Displayed",spendAnalysisAndAnnualSummary.isDisplayed());

        }
    }
    public void rewardsAndBenefitsSeeAllBenefitsLinkAndFootNotes() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,450)");
        Actions act = new Actions(driver);
        act.moveToElement(seeAllMyBenefitsLink).click().build().perform();
         seeAllMyBenefitsLink.click();
        }

}
