#Author: Ben Pemberton (bmpembe)
@CardmemberServicesSuite @DownloadAnnualAccountSummary @WIP @Regression
Feature: Download Annual Account Summary


  @TC001
  Scenario Outline: Download Annual Account Summary_pdf
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When I click Services Tab
    And I click link - Download Annual Account Summary
    Then the user is able to download and view the Annual Account Summary PDF for the year he selected

    Examples:
      | partner       | testData |

  @TC002
  Scenario Outline: Back to Services from Download Annual Account Summary
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When I click Services Tab
    And I click link - Download Annual Account Summary
    And I Click Link - Back to Services to navigate back to services page

    Examples:
      | partner | testData |

  @TC003
  Scenario Outline: Back to Services from Download Annual Account Summary
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When I click Services Tab
    And I click link - Download Annual Account Summary
    And I Click Button - Back to Services to navigate back to services page

    Examples:
      | partner | testData |

  @TC004
  Scenario Outline: Spend Analysis from Download Annual Account Summary
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When I click Services Tab
    And I click link - Download Annual Account Summary
    And I click link - Spend Analysis to navigate to Spend Analysis page

    Examples:
      | partner | testData |
