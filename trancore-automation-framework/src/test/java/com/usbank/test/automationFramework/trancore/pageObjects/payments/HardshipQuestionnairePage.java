package com.usbank.test.automationFramework.trancore.pageObjects.payments;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class HardshipQuestionnairePage {

    private static final Logger logger = LogManager.getLogger(HardshipQuestionnairePage.class);
    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(xpath = "//h1[@data-test='revampquestionnaire-page-header']")
    private WebElement pageHeading;

    @FindBy(xpath = "//*[contains(text(),'We understand that at any time')]")
    private WebElement weUnderstandThatAtAnyTimeParagraph;

    @FindBy(xpath = "//*[contains(text(),'experiencing economic hardship')]")
    private WebElement ifYouAreExperiencingParagraph;

    @FindBy(xpath = "//*[contains(text(),'Before we start, please')]")
    private WebElement lengthOfHardshipQuestion;

    @FindBy(id = "radiolengthOfHardship1")
    private WebElement radioOptionLessThan4Months;

    @FindBy(id = "radiolengthOfHardship2")
    private WebElement radioOption4to12Months;

    @FindBy(id = "radiolengthOfHardship3")
    private WebElement radioOption1to3Years;

    @FindBy(id = "radiolengthOfHardship4")
    private WebElement radioOption3to5Years;

    @FindBy(name = "Continue")
    private WebElement btnContinue;

    @FindBy(id = "cancelLink")
    private WebElement cancelLink;

//    @FindBy(xpath = "//*[@data-testid='component-usb-single-notification']")
//    private WebElement pageLevelErrorForLengthOfHardship;
//
//    @FindBy(className = "field-level-error-text")
//    private WebElement fieldLevelErrorForLengthOfHardship;

    @FindBy(id = "recommendPlanParagraph")
    private WebElement toRecommendAPlanParagraph;

    @FindBy(id = "label_currentMonthlyIncome")
    private WebElement currentMonthlyIncomeSubHeader;

    @FindBy(name = "currentMonthlyIncome")
    private WebElement currentMonthlyIncome;

    @FindBy(id = "helper-text_currentMonthlyIncome")
    private WebElement disclosureForMonthlyIncome;

    @FindBy(id = "label_currentMonthlyExpenses")
    private WebElement currentMonthlyExpensesSubHeader;

    @FindBy(name = "currentMonthlyExpenses")
    private WebElement currentMonthlyExpenses;

    @FindBy(id = "helper-text_currentMonthlyExpenses")
    private WebElement helperTextForMonthlyExpenses;

    @FindBy(xpath = "//*[@id='label-text-hardShip_reason']")
    private WebElement reasonForHardshipSubHeader;

    @FindBy(className = "questionnaire-select-class")
    private WebElement reasonForHardship;

    // Continue and Cancel page objects will be re-used from Questionnaire1 page

    @FindBy(xpath = "//*[@data-testid='component-usb-multiple-notification']")
    private WebElement pageLevelErrorTotalMessage;

    @FindBy(xpath = "//*[@data-testid='component-usb-multiple-notification']/h2")
    private WebElement numberOfItemsRequireAction;

    // page level errors

    @FindBy(xpath = "//*[@data-testid='component-usb-multiple-notification']/ul/li[1]")
    private WebElement pageLevelErrorForLengthOfHardship;

    @FindBy(id = "lenghthOfHarsShipError")
    private WebElement fieldLevelErrorForLengthOfHardship;

    @FindBy(xpath = "//*[@data-testid='component-usb-multiple-notification']/ul/li[2]")
    private WebElement pageLevelErrorForIncome;

    @FindBy(xpath = "//*[@data-testid='component-usb-multiple-notification']/ul/li[3]")
    private WebElement pageLevelErrorForExpenses;

    @FindBy(xpath = "//*[@data-testid='component-usb-multiple-notification']/ul/li[4]")
    private WebElement pageLevelErrorForReasonForHardship;

    @FindBy(xpath = "//*[@id='multiple-single-notification--link']")
    private WebElement pageLevelErrorWhenOneErrorExists;

    // Field level errors

    @FindBy(xpath = "//*[@id='error-text_currentMonthlyIncome']/span[2]")
    private WebElement fieldLevelErrorForIncome;

    @FindBy(xpath = "//*[@id='error-text_currentMonthlyExpenses']/span[2]")
    private WebElement fieldLevelErrorForExpenses;

    @FindBy(xpath = "//*[@id='error-hardshipReason']")
    private WebElement fieldLevelErrorForReasonForHardship;

    // Cancel pop-up
    @FindBy(className = "collections-modal-noFocus")
    private WebElement cancelPopUp;

    @FindBy(id = "closeModal")
    private WebElement closeIconInCancelPopUp;

    @FindBy(id = "go-back-button")
    private WebElement goBackButtonInCancelPopUp;

    @FindBy(id = "yes-leave-button")
    private WebElement yesLeaveButtonInCancelPopUp;

    @FindBy(xpath = "//*[@data-test-id='MakeAPayment']")
    private WebElement makeAPaymentButtonInPaymentsLandingPage;

    public HardshipQuestionnairePage(WebDriver inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 30);
    }

    public boolean verifyQuestionnairePageTitle() {
        wait.until(ExpectedConditions.visibilityOf(lengthOfHardshipQuestion));
        logger.info("Questionnaire page title is as expected: " + driver.getTitle());
        //return driver.getTitle().equals("Credit Card Account Access: Length of hardship");
        return driver.getTitle().equals("Credit Card Account Access: Questions to help set up payment plan");
    }

    public boolean verifyQuestionnaire1PageHeaderDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(lengthOfHardshipQuestion));
        logger.info("Questionnaire1 page header displayed: " + pageHeading.getText());
        return pageHeading.getText().equals("We want to help.");
    }

    public boolean verifyTheParagraphStartingWIthWeUnderstandThatAtAnyTime() {
        logger.info("We Understand That At Any Time paragraph verbiage is displayed: " + weUnderstandThatAtAnyTimeParagraph.getText());
        // return ifYouAreExperiencingParagraph.getText().equals("If you're experiencing economic hardship, whatever the reason, we can work with you to help you get back on track. To make a recommendation, we'll need to collect some information about your monthly income and expenses.");
        return weUnderstandThatAtAnyTimeParagraph.getText().equals("We understand that at any time, anyone can face a financial challenge.");
    }

    public boolean verifyTheParagraphStartingWIthIfYouAreExperiencingIsDisplayed() {
        logger.info("paragraph verbiage is displayed: " + ifYouAreExperiencingParagraph.getText());
         return ifYouAreExperiencingParagraph.getText().equals("If you're experiencing economic hardship, whatever the reason, we can work with you to help you get back on track. To make a recommendation, we'll need to collect some information about your monthly income and expenses.");
//        return ifYouAreExperiencingParagraph.getText().equals("If you're experiencing economic hardship, we have payment plans to help get you back on track.");
    }

    public boolean verifyQuestionnaire1LengthOfHardshipQuestionDisplayed() {
        logger.info("Length Of Hardship Question is displayed: " + lengthOfHardshipQuestion.getText());
        return lengthOfHardshipQuestion.getText().equals("Before we start, please tell us how long you expect your hardship to last.");
    }

    public boolean verifyLengthOfHardshipFirstRadioButtonDisplayed() {
        logger.info("Length Of Hardship - First Radio Button is displayed: " + radioOptionLessThan4Months.getText());
        return radioOptionLessThan4Months.getText().equals("Less than 4 months");
    }

    public boolean verifyLengthOfHardshipSecondRadioButtonDisplayed() {
        logger.info("Length Of Hardship - Second Radio Button is displayed: " + radioOption4to12Months.getText());
        return radioOption4to12Months.getText().equals("4 to 12 months");
    }

    public boolean verifyLengthOfHardshipThirdRadioButtonDisplayed() {
        logger.info("Length Of Hardship - Third Radio Button is displayed: " + radioOption1to3Years.getText());
        return radioOption1to3Years.getText().equals("1 to 3 years");
    }

    public boolean verifyLengthOfHardshipFourthRadioButtonDisplayed() {
        logger.info("Length Of Hardship - Fourth Radio Button is displayed: " + radioOption3to5Years.getText());
        return radioOption3to5Years.getText().equals("3 to 5 years");
    }

    public boolean verifyContinueButtonDisplayed() {
        logger.info("Continue Button is displayed: " + btnContinue.getText());
        return btnContinue.getText().equals("Continue");
    }

    public boolean verifyCancelLinkDisplayed() {
        logger.info("Cancel Link is displayed: " + cancelLink.getText());
        return cancelLink.getText().equals("Cancel");
    }

    public WebElement getLengthOfHardshipFirstRadioButton() {
        return radioOptionLessThan4Months;
    }

    public WebElement getLengthOfHardshipSecondRadioButton() {
        return radioOption4to12Months;
    }

    public WebElement getLengthOfHardshipThirdRadioButton() {
        return radioOption1to3Years;
    }

    public WebElement getLengthOfHardshipFourthRadioButton() {
        return radioOption3to5Years;
    }

    public WebElement getContinueButton() {
        return btnContinue;
    }

    public WebElement getCancelLink() {
        return cancelLink;
    }

    public WebElement clickLengthOfHardshipFirstRadioButton() {
        return radioOptionLessThan4Months;
    }

    public WebElement clickLengthOfHardshipSecondRadioButton() {
        return radioOption4to12Months;
    }

    public WebElement clickLengthOfHardshipThirdRadioButton() {
        return radioOption1to3Years;
    }

    public WebElement clickLengthOfHardshipFourthRadioButton() {
        return radioOption3to5Years;
    }

    public boolean verifyQuestionnaire2PageTitle() {
        wait.until(ExpectedConditions.visibilityOf(toRecommendAPlanParagraph));
        logger.info("Questionnaire2 page title is as expected: " + driver.getTitle());
        return driver.getTitle().equals("Credit Card Account Access: Questions");
    }

    public boolean verifyToRecommendAPlanParagraphDisplayed() {
        logger.info("To recommend a plan paragraph displayed: " + toRecommendAPlanParagraph.getText());
        return toRecommendAPlanParagraph.getText().equals("To recommend a plan that fits your present circumstances we'll need to collect some information about your monthly income and expenses. We'll also ask about the type of hardship you're experiencing.");
    }

    public boolean currentMonthlyIncomeSubHeaderIsDisplayed() {
        logger.info("Current Monthly Income Sub Header displayed: " + currentMonthlyIncomeSubHeader.getText());
        return currentMonthlyIncomeSubHeader.getText().equals("Current monthly income");
    }

    public boolean currentMonthlyIncomeInputBoxIsDisplayed() {
        logger.info("Current Monthly Income Input Box displayed: " + currentMonthlyIncome.getText());
        return currentMonthlyIncome.getText().equals("");
    }

    public boolean currentMonthlyIncomeDisclosureParaIsDisplayed() {
        logger.info("Current Monthly Income Disclosure paragraph is displayed: " + disclosureForMonthlyIncome.getText());
        return disclosureForMonthlyIncome.getText().equals("Alimony, child support or separate maintenance income need not be revealed if you don't wish to have it considered as a basis for repaying this obligation.");
    }

    public boolean currentMonthlyExpensesSubHeaderIsDisplayed() {
        logger.info("Current Monthly Expenses Sub Header displayed: " + currentMonthlyExpensesSubHeader.getText());
        return currentMonthlyExpensesSubHeader.getText().equals("Current monthly expenses");
    }

    public boolean currentMonthlyExpensesInputBoxIsDisplayed() {
        logger.info("Current Monthly Expenses Input Box displayed: " + currentMonthlyExpenses.getText());
        return currentMonthlyExpenses.getText().equals("");
    }

    public boolean currentMonthlyExpensesHelperTextIsDisplayed() {
        logger.info("Current Monthly Expenses Helper Text is displayed: " + helperTextForMonthlyExpenses.getText());
        return helperTextForMonthlyExpenses.getText().equals("Housing, medical, food, utilities, phone, etc.");
    }

    public WebElement inputCurrentMonthlyIncome() {
        currentMonthlyIncome.sendKeys(Keys.CONTROL + "a");
        currentMonthlyIncome.sendKeys(Keys.DELETE);
        return currentMonthlyIncome;
    }

    public WebElement inputCurrentMonthlyExpenses() {
        currentMonthlyExpenses.sendKeys(Keys.CONTROL + "a");
        currentMonthlyExpenses.sendKeys(Keys.DELETE);
        return currentMonthlyExpenses;
    }

    public boolean reasonForHardshipSubHeaderIsDisplayed() {
        logger.info("Reason for Hardship Sub Header displayed: " + reasonForHardshipSubHeader.getText());
        return reasonForHardshipSubHeader.getText().equals("Please share the reason for your hardship.");
    }

    public int reasonForHardshipDefaultValueIsDisplayed() {
        Select hardshipReasons = new Select(reasonForHardship);
        List<WebElement> reasonsList = hardshipReasons.getOptions();
        for (WebElement reason : reasonsList) {
            if (reason.getText().equals("Select one")) {
                logger.info("Reason for Hardship default value: " + reason.getText());
            }
        }
        return 1;
    }

    public int validateTheReasonForHardshipDropdownValues() {
        Select hardshipReasons = new Select(reasonForHardship);
        List<WebElement> reasonsList = hardshipReasons.getOptions();
        int count = 0;
        for (WebElement reason : reasonsList) {
            if (reason.getText().equals("COVID-19 pandemic") || reason.getText().equals("Excessive debt obligation")
                    || reason.getText().equals("Loss of income") || reason.getText().equals("Unemployment") || reason.getText().equals("Other")) {
                logger.info("Reason for Hardship dropdown value: " + reason.getText());
                count++;
            }
        }
        return count;
    }

    public Select selectHardshipReason() {
        return new Select(reasonForHardship);
    }

    // page level errors
    public void validatePageLevelErrorHeaderNumberOfItemsRequireActionIs4() {
        wait.until(ExpectedConditions.visibilityOf(pageLevelErrorTotalMessage));
        logger.info("Number of fields require action : " + numberOfItemsRequireAction.getText());
        Assert.assertTrue(numberOfItemsRequireAction.getText().equals("4 items require action:"));
    }

    public void validatePageLevelErrorHeaderNumberOfItemsRequireActionIs3() {
        wait.until(ExpectedConditions.visibilityOf(pageLevelErrorTotalMessage));
        logger.info("Number of fields require action : " + numberOfItemsRequireAction.getText());
        Assert.assertTrue(numberOfItemsRequireAction.getText().equals("3 items require action:"));
    }

    public void validatePageLevelErrorHeaderNumberOfItemsRequireActionIs2() {
        wait.until(ExpectedConditions.visibilityOf(pageLevelErrorTotalMessage));
        logger.info("Number of fields require action : " + numberOfItemsRequireAction.getText());
        Assert.assertTrue(numberOfItemsRequireAction.getText().equals("2 items require action:"));
    }

    public void validatePageLevelErrorForIncomeWhenAllThreeFieldsErrorExists() {
        wait.until(ExpectedConditions.visibilityOf(pageLevelErrorTotalMessage));
        logger.info("Page Level Error for Monthly Income is displayed : " + pageLevelErrorForIncome.getText());
        Assert.assertTrue(pageLevelErrorForIncome.getText().equals("There's an issue with yourmonthly income."));
    }

    public void validatePageLevelErrorForExpensesWhenAllThreeFieldsErrorExists() {
        wait.until(ExpectedConditions.visibilityOf(pageLevelErrorTotalMessage));
        logger.info("Page Level Error for Monthly Expenses is displayed : " + pageLevelErrorForExpenses.getText());
        Assert.assertTrue(pageLevelErrorForExpenses.getText().equals("There's an issue with yourmonthly expenses."));
    }

    public void validatePageLevelErrorForReasonForHardshipWhenAllThreeFieldsErrorExists() {
        wait.until(ExpectedConditions.visibilityOf(pageLevelErrorTotalMessage));
        logger.info("Page Level Error for reason for hardship is displayed : " + pageLevelErrorForReasonForHardship.getText());
        Assert.assertTrue(pageLevelErrorForReasonForHardship.getText().equals("There's an issue with yourreason for hardship."));
    }

    public void validatePageLevelErrorForIncomeWhenTwoFieldsErrorExists() {
        logger.info("Page Level Error for Monthly Income is displayed : " + pageLevelErrorForIncome.getText());
        Assert.assertTrue(pageLevelErrorForIncome.getText().equals("There's an issue with yourmonthly income."));
    }

    public void validatePageLevelErrorForExpensesWhenTwoFieldsErrorExists() {
        wait.until(ExpectedConditions.visibilityOf(pageLevelErrorTotalMessage));
        logger.info("Page Level Error for Monthly Expenses is displayed : " + pageLevelErrorForExpenses.getText());
        Assert.assertTrue(pageLevelErrorForExpenses.getText().equals("There's an issue with yourmonthly expenses."));
    }

    public void validatePageLevelErrorForReasonForHardshipWhenTwoFieldsErrorExists() {
        wait.until(ExpectedConditions.visibilityOf(pageLevelErrorTotalMessage));
        logger.info("Page Level Error for reason for hardship is displayed : " + pageLevelErrorForReasonForHardship.getText());
        Assert.assertTrue(pageLevelErrorForReasonForHardship.getText().equals("There's an issue with yourreason for hardship."));
    }

    public void validatePageLevelErrorForIncomeWhenOneFieldsErrorExists() {
        wait.until(ExpectedConditions.visibilityOf(pageLevelErrorWhenOneErrorExists));
        logger.info("Page Level Error for Length of Hardship is displayed : " + pageLevelErrorWhenOneErrorExists.getText());
        Assert.assertTrue(pageLevelErrorWhenOneErrorExists.getText().equals("Please tell us yourlength of hardship."));
    }

    public void validatePageLevelErrorForExpensesWhenOneFieldsErrorExists() {
        wait.until(ExpectedConditions.visibilityOf(pageLevelErrorTotalMessage));
        logger.info("Page Level Error for Monthly Expenses is displayed : " + pageLevelErrorForExpenses.getText());
        Assert.assertTrue(pageLevelErrorForExpenses.getText().equals("There's an issue with yourmonthly expenses."));
    }

    public void validatePageLevelErrorForReasonForHardshipWhenOneFieldsErrorExists() {
        wait.until(ExpectedConditions.visibilityOf(pageLevelErrorTotalMessage));
        logger.info("Page Level Error for reason for hardship is displayed : " + pageLevelErrorForReasonForHardship.getText());
        Assert.assertTrue(pageLevelErrorForReasonForHardship.getText().equals("There's an issue with yourreason for hardship."));
    }

    // Field Level Error Message Validation

    public boolean validateFieldLevelErrorWhenIncomeIsMissingInQuestionnaire2() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        logger.info("Field Level error when monthly income is missing: " + fieldLevelErrorForIncome.getText());
        return fieldLevelErrorForIncome.getText().equals("Please enter your monthly income. If you have none, enter zero.");
    }

    public boolean validateFieldLevelErrorWhenExpensesIsMissingInQuestionnaire2() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        logger.info("Field Level error when monthly expenses is missing: " + fieldLevelErrorForExpenses.getText());
        return fieldLevelErrorForExpenses.getText().equals("Please enter your monthly expenses. If you have none, enter zero.");
    }

    public boolean validateFieldLevelErrorWhenReasonForHardshipIsMissingInQuestionnaire2() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        logger.info("Field Level error when reason for hardship value is not selected: " + fieldLevelErrorForReasonForHardship.getText());
        return fieldLevelErrorForReasonForHardship.getText().equals("Please make a selection.");
    }

    public boolean validateFieldLevelErrorWhenIncomeContainsInvalidCharactersInQuestionnaire2() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        logger.info("Field Level error when monthly income has invalid characters: " + fieldLevelErrorForIncome.getText());
        return fieldLevelErrorForIncome.getText().equals("You may enter zero or whole dollars. Enter numbers and no other characters including periods or decimal points.");
    }

    public boolean validateFieldLevelErrorWhenExpensesContainsInvalidCharactersInQuestionnaire2() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        logger.info("Field Level error when monthly expenses has invalid characters:" + fieldLevelErrorForExpenses.getText());
        return fieldLevelErrorForExpenses.getText().equals("You may enter zero or whole dollars. Enter numbers and no other characters including periods or decimal points.");
    }

    public boolean validateFieldLevelErrorWhenIncomeExceeds999999InQuestionnaire2() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        logger.info("Field Level Error when Monthly income exceeds $999,999: " + fieldLevelErrorForIncome.getText());
        return fieldLevelErrorForIncome.getText().equals("Please enter an amount that is less than $1,000,000.");
    }

    public boolean validateFieldLevelErrorWhenExpensesExceeds999999InQuestionnaire2() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        logger.info("Field Level Error when Monthly expenses exceeds $999,999:" + fieldLevelErrorForExpenses.getText());
        return fieldLevelErrorForExpenses.getText().equals("Please enter an amount that is less than $1,000,000.");
    }

    public boolean validateFieldLevelErrorWhenIncomeBeginsWithZerosInQuestionnaire2() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        logger.info("Field Level Error when Monthly Income begins with one or more zeros: " + fieldLevelErrorForIncome.getText());
        return fieldLevelErrorForIncome.getText().equals("Please remove zeros before whole dollar amounts.");
    }

    public boolean validateFieldLevelErrorWhenExpensesBeginsWithZerosInQuestionnaire2() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        logger.info("Field Level Error when Monthly Expenses begins with one or more zeros: " + fieldLevelErrorForExpenses.getText());
        return fieldLevelErrorForExpenses.getText().equals("Please remove zeros before whole dollar amounts.");
    }

    public boolean verifyPageLevelErrorForLengthOfHardship() {
        wait.until(ExpectedConditions.visibilityOf(pageLevelErrorForLengthOfHardship));
        logger.info("Page level error for length of hardship is displayed: " + pageLevelErrorForLengthOfHardship.getText());
        return pageLevelErrorForLengthOfHardship.getText().equals("Please tell us yourlength of hardship.");
    }

    public boolean verifyFieldLevelErrorForLengthOfHardship() {
        logger.info("Field level error for length of hardship is displayed: " + fieldLevelErrorForLengthOfHardship.getText());
        return fieldLevelErrorForLengthOfHardship.getText().equals("Please make a selection.");
    }

    public WebElement clickContinueButton() {
        wait.until(ExpectedConditions.visibilityOf(btnContinue));
        return btnContinue;
    }

    public WebElement clickCancelLink() {
        wait.until(ExpectedConditions.visibilityOf(cancelLink));
        return cancelLink;
    }

    public WebElement verifyTheCancelPopUp(){
        wait.until(ExpectedConditions.visibilityOf(cancelPopUp));
        return cancelPopUp;
    }
    public WebElement clickCloseIcon() {        return closeIconInCancelPopUp;    }
    public WebElement clickNoGoBackButton() {        return goBackButtonInCancelPopUp;    }
    public WebElement clickYesLeaveButton() {        return yesLeaveButtonInCancelPopUp;    }

    public void verifyPaymentsLandingPageIsDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(makeAPaymentButtonInPaymentsLandingPage));
        logger.info("In Payments landing Page, the button displayed: " + makeAPaymentButtonInPaymentsLandingPage.getText());
        Assert.assertTrue(makeAPaymentButtonInPaymentsLandingPage.isDisplayed());
    }

}
