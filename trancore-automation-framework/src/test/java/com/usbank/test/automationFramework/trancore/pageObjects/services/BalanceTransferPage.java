package com.usbank.test.automationFramework.trancore.pageObjects.services;

import com.usbank.test.helper.SeleniumExtentionsWait;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;


public class BalanceTransferPage<T extends WebDriver> extends SeleniumExtentionsWait {

    private static final Logger logger = LogManager.getLogger(BalanceTransferPage.class);

    @FindBy(id = "challengeQuestion_lbl")
    private WebElement lblQuestion;

    @FindBy(id = "answer")
    private WebElement txtAnswer;

    @FindBy(how = How.LINK_TEXT, using = "Balance transfer")
    private WebElement linkBalanceTransfer;

    @FindBy(how = How.XPATH, using = "//*[@id=\'strutsForm\']/div[2]/button")
    private WebElement btnSubmit;

    @FindBy(how = How.XPATH, using = "//button[@title='Select this offer']")
    private WebElement btnSelectThisOffer;

    @FindBy(how = How.TAG_NAME, using = "body")
    private WebElement verifyText;

    @FindAll({
            @FindBy(how = How.LINK_TEXT, using = "//a[@title='View balance transfer terms and conditions']"),
            @FindBy(how = How.XPATH, using = "//a[@class='lnk-default bt-lnk-default']"),
            @FindBy(how = How.LINK_TEXT, using = "View balance transfer terms and conditions"),
            @FindBy(how = How.XPATH, using = "//body/div[@id='root']/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[5]/div[1]/div[1]/div[1]/div[1]/div[2]/div[4]/div[1]/p[2]/a[1]")
    })
    private WebElement linkTermsAndConditions;

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'View E-sign Consent Agreement')]")
    private WebElement linkESignConsentAgreement;

    @FindBy(how = How.CSS, using = "div.elan-app div:nth-child(1) div:nth-child(1) div:nth-child(1) section.omvTermsDetails.bt-popup.omvOfferInfo > button.svc-btn.hideInOmv:nth-child(8)")
    private WebElement btnCloseWindow;

    @FindBy(how = How.ID, using = "IAgreeCheckbox")
    private WebElement ckbCheckBox;

    @FindBy(how = How.XPATH, using = "//select[@id='childForms[0].payeeField_inpt']")
    private WebElement lstSelectAPayee;

    @FindBy(how = How.XPATH, using = "//input[@id='childForms[0].payeeName_inpt']")
    private WebElement txtCustomPayee;

    @FindBy(how = How.CSS, using = "#childForms\\[0\\]\\.payeeAccount_inpt")
    private WebElement txtAccountNumber;

    @FindBy(how = How.XPATH, using = "//input[@id='childForms[0].addressStreet_inpt']")
    private WebElement txtPayeeAddress;

    @FindBy(how = How.XPATH, using = "//input[@id='childForms[0].addressSuite_inpt']")
    private WebElement txtSuite;

    @FindBy(how = How.XPATH, using = "//input[@id='childForms[0].transferAmount_inpt']")
    private WebElement txtTransferAmount;

    @FindBy(how = How.XPATH, using = "//input[@id='childForms[0].addressCity_inpt']")
    private WebElement txtCity;

    @FindBy(how = How.XPATH, using = "//select[@id='childForms[0].addressState_inpt']")
    private WebElement lstSelectState;

    @FindBy(how = How.XPATH, using = "//input[@id='childForms[0].addressZip_inpt']")
    private WebElement txtZipCode;

    @FindBy(how = How.XPATH, using = "//button[@class='primary omv_button--full svc-btn omv-btn']")
    private WebElement btnContinue;

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Submit')]")
    private WebElement btnFinalSubmission;

    @FindBy(how = How.ID, using = "addFormButton")
    private WebElement btnAddAnotherTransfer;

    @FindBy(how = How.XPATH, using = "//a[@class='remove-form-x']")
    private WebElement btnRemoveBalanceTransfer;

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Yes, remove')]")
    private WebElement btnYesRemove;

    @FindBy(how = How.ID, using = "selectOfferButton")
    private WebElement btnSelectADifferentOffer;

    @FindBy(how = How.XPATH, using = "//button[@class='secondary svc-btn bt-secondary']")
    private WebElement btnSecondCancel;

    @FindBy(how = How.XPATH, using = "//button[@class='secondary omv_button--hollow svc-btn']")
    private WebElement btnCancel;

    @FindBy(how = How.XPATH, using = "//button[@id='confirmationRequestBT_btn']")
    private WebElement btnRequestAnotherBalanceTransfer;

    @FindBy(how = How.XPATH, using = "//button[@class='tranCoreButton buttonpad omv_button--full']")
    private WebElement btnSubmitSecurityQuestion;

    @FindAll({
            @FindBy(how = How.CSS, using = "div.elan-app div.middleWrapper:nth-child(3) div.layoutPageContent2:nth-child(2) div.layoutPageContent3 div.tc_page_shadow div.balanceTransferRedirectToApp div.singleBlock div:nth-child(1) div:nth-child(1) div:nth-child(2) div.svc-page.svc-header > h1.contentAreaSubHeader:nth-child(1)"),
            @FindBy(how = How.XPATH, using = "//body/div[@id='root']/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[5]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/h1[1]"),
            @FindBy(how = How.CSS, using = "div.associated-app div.middleWrapper:nth-child(3) div.layoutPageContent2:nth-child(2) div.layoutPageContent3 div.balanceTransferRedirectToApp div.varContentBg div:nth-child(1) div:nth-child(1) div:nth-child(2) div.svc-page.svc-header > h1.contentAreaSubHeader:nth-child(1)"),
            @FindBy(how = How.CSS, using = "div.fidelity-app div.contentContainer:nth-child(3) div.middleWrapper div.layoutPageContent2:nth-child(2) div.layoutPageContent3 div.tc_page_shadow div.balanceTransferRedirectToApp div.singleBlock div:nth-child(1) div:nth-child(1) div:nth-child(2) div.svc-page.svc-header > h1.contentAreaSubHeader:nth-child(1)"),
    })
    private WebElement balanceTransferOfferHeader;

    @FindBy(how = How.XPATH, using = "//h2[@id='subHeader']//span")
    private WebElement shieldQuestionHeading;

    @FindBy(how = How.XPATH, using = "//*[@id=\"btapp-mainContent\"]/div[2]/div[1]/h1")
    private WebElement balanceTransferHeading;

    @FindBy(how = How.CLASS_NAME, using = "contentAreaSubHeader")
    private WebElement balanceTransferHeadings;

    @FindBy(how = How.XPATH, using = "//h1[text()='Balance transfer review & submit']")
    private WebElement balanceTransferReviewAndSubmitHeading;

    @FindBy(how = How.XPATH, using = "//h1[text()='Balance transfer confirmation']")
    private WebElement balanceTransferConfirmationHeading;


    private T driver;

    public static int EXPLICIT_WAIT_TIME_SECONDS = 30;

    private static final Logger log = LogManager.getLogger(BalanceTransferPage.class);

    public BalanceTransferPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public String getShieldQuestionAnswer() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lblQuestion));
        String questionText = lblQuestion.getText();
        String lastWord = questionText.substring(questionText.lastIndexOf(" ") + 1, questionText.length() - 1);
        txtAnswer.sendKeys(lastWord);
        btnSubmitSecurityQuestion.click();
        return lastWord;
    }

    public void clickOnViewDetailsLinkAndSelectThisOffer() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(balanceTransferOfferHeader));
        boolean linkPresent = true;
        List<WebElement> viewDetailsLinks;

        while (linkPresent) {
            viewDetailsLinks = driver.findElements(By.linkText("View details"));
            for (int i = 0; i < viewDetailsLinks.size(); i++) {
                new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(viewDetailsLinks.get(i)));
                viewDetailsLinks.get(0).click();
                btnSelectThisOffer.click();
                break;
            }
            break;
        }
    }


    public void verifyAndClickTermsAndConditionLink() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(linkTermsAndConditions));
        Assert.assertTrue(getLinkTermsAndConditions().isDisplayed());
        linkTermsAndConditions.click();
    }

    public WebElement getLinkTermsAndConditions() {
        return linkTermsAndConditions;
    }

    public void clickOnCheckBox() {
        ckbCheckBox.click();
    }

    public WebElement getCkbCheckBox() {
        return ckbCheckBox;
    }

    public void switchBetweenWindows() {

        String parentWindow = driver.getWindowHandle();

        for (String subWindow : driver.getWindowHandles()) {
            driver.switchTo().window(subWindow);
        }

        driver.close();
        driver.switchTo().window(parentWindow);
    }

    public void selectAPayee(String selectPayee) {

        lstSelectAPayee.click();
        List<WebElement> listItems = driver.findElements(By.xpath("//select[@id='childForms[0].payeeField_inpt']"));

        for (WebElement item : listItems) {
            log.info(item.getText());
            log.info("*************This Is The End Of The List*************");
            Select drpdwnPayee = new Select(driver.findElement(By.xpath("//select[@id='childForms[0].payeeField_inpt']")));
            drpdwnPayee.selectByVisibleText(selectPayee);
        }

    }

    public WebElement getLstSelectAPayee() {
        return lstSelectAPayee;
    }

    public void enterCustomPayee(String customPayeeName) {
        txtCustomPayee.click();
        txtCustomPayee.sendKeys(customPayeeName);
    }

    public WebElement getTxtCustomPayee() {
        return txtCustomPayee;
    }

    public void enterAccountNumber(String accountNumber) {
        txtAccountNumber.click();
        txtAccountNumber.sendKeys(accountNumber);
    }

    public WebElement getTxtAccountNumber() {
        return txtAccountNumber;
    }

    public void enterPayeeAddress(String payeeAddress) {
        txtPayeeAddress.click();
        txtPayeeAddress.sendKeys(payeeAddress);
    }

    public WebElement getTxtPayeeAddress() {
        return txtPayeeAddress;
    }

    public void enterSuiteNumber(String suiteNumber) {
        txtSuite.click();
        txtSuite.sendKeys(suiteNumber);
    }

    public WebElement getTxtSuite() {
        return txtSuite;
    }

    public void enterTransferAmount(String amount) {
        txtTransferAmount.sendKeys(amount);
    }

    public WebElement getTxtTransferAmount() {
        return txtTransferAmount;
    }

    public void enterCity(String city) {
        txtCity.click();
        txtCity.sendKeys(city);
    }

    public WebElement getTxtCity() {
        return txtCity;
    }

    public void selectAState(String state) {

        lstSelectState.click();
        List<WebElement> listItems = driver.findElements(By.xpath("//select[@id='childForms[0].addressState_inpt']"));

        for (WebElement item : listItems) {
            log.info(item.getText());
            log.info("*************This Is The End Of The List*************");
            Select drpdwnPayee = new Select(driver.findElement(By.xpath("//select[@id='childForms[0].addressState_inpt']")));
            drpdwnPayee.selectByVisibleText(state);
        }

    }

    public WebElement getLstSelectState() {
        return lstSelectState;
    }

    public void enterZipCode(String zipcode) {
        txtZipCode.click();
        txtZipCode.sendKeys(zipcode);
    }

    public WebElement getTxtZipCode() {
        return txtZipCode;
    }

    public void clickOnContinueButton() {
        btnContinue.click();
    }

    public WebElement getBtnContinue() {
        return btnContinue;
    }

    public void clickOnFinalSubmission() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnFinalSubmission));
        btnFinalSubmission.click();
    }

    public WebElement getBtnFinalSubmission() {
        return btnFinalSubmission;
    }

    public void clickOnAddAnotherTransferButton() {
        btnAddAnotherTransfer.click();
    }

    public WebElement getBtnAddAnotherTransfer() {
        return btnAddAnotherTransfer;
    }

    public void clickAndCloseRemoveBalanceTransfer() {
        btnRemoveBalanceTransfer.click();
        Actions action = new Actions(driver);
        action.sendKeys(Keys.ESCAPE);
        driver.switchTo().activeElement();
        btnYesRemove.click();
    }

    public WebElement getBtnRemoveBalanceTransfer() {
        return btnRemoveBalanceTransfer;
    }

    public void clickAndCloseSelectADifferentOffer() {
        btnSelectADifferentOffer.click();
        btnSelectADifferentOffer.sendKeys(Keys.ESCAPE);
        driver.switchTo().activeElement();
    }

    public WebElement getBtnSelectADifferentOffer() {
        return btnSelectADifferentOffer;
    }

    public void clickAndCloseCancel() {
        btnCancel.click();
        btnCancel.sendKeys(Keys.ESCAPE);
    }

    public WebElement getBtnCancel() {
        return btnCancel;
    }

    public void clickAndCloseCancelButton() {
        btnSecondCancel.click();
        btnSecondCancel.sendKeys(Keys.ESCAPE);
    }

    public WebElement getBtnSecondCancel() {
        return btnSecondCancel;
    }

    public void clickOnRequestAnotherBalanceTransfer() {
        btnRequestAnotherBalanceTransfer.click();
    }

    public WebElement getBtnRequestAnotherBalanceTransfer() {
        return btnRequestAnotherBalanceTransfer;
    }

    public void enterSecretAnswer(String secretAnswer) {

        txtAnswer.sendKeys(secretAnswer);
    }

    public void VerifyAndAnswerSeceretQuestion() {
        String pgHeading = shieldQuestionHeading.getText();
        String actualPgHeading = "ID Shield Question";
        Assert.assertEquals(pgHeading, actualPgHeading);
        logger.info("========= Page heading:" + pgHeading + " displayed successfully =========");
        String secretAnswer = getShieldQuestionAnswer();

    }

    public void verifyBalanceOfferPage() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(balanceTransferHeading));
        String pgHeading = balanceTransferHeading.getText();
        String actualPgHeading = "Select View Details to display additional information for the Balance Transfer Offer.";
        Assert.assertEquals(pgHeading, actualPgHeading);
        logger.info("========= Page heading:" + pgHeading + " displayed successfully =========");

    }

    public void verifyBalanceTransferConfirmationPage() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(balanceTransferConfirmationHeading));
        String pgHeading = balanceTransferConfirmationHeading.getText();
        String actualPgHeading = "Balance transfer confirmation";
        Assert.assertEquals(pgHeading, actualPgHeading);
        logger.info("========= Page heading:" + pgHeading + " displayed successfully =========");

    }

    public void verifyBalanceTransferRequestPagePage() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(balanceTransferHeadings));
        String pgHeading = balanceTransferHeading.getText();
        String actualPgHeading = "Balance transfer request";
        Assert.assertEquals(pgHeading, actualPgHeading);
        logger.info("========= Page heading:" + pgHeading + " displayed successfully =========");

    }

    public void verifyBalanceTransferReviewAndSubmitPage() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(balanceTransferReviewAndSubmitHeading));
        String pgHeading = balanceTransferReviewAndSubmitHeading.getText();
        String actualPgHeading = "Balance transfer review & submit";
        Assert.assertEquals(pgHeading, actualPgHeading);
        logger.info("========= Page heading:" + pgHeading + " displayed successfully =========");

    }


}
