package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

public class CallUsNowToFinish<T extends WebDriver> {
    private static final Logger logger = LogManager.getLogger(CallUsNowToFinish.class);
    private static final long EXPLICIT_WAIT_TIME_SECONDS = 30;
    private T driver;

    @FindBy(how = How.TAG_NAME, using = "h1")
    private WebElement headerName;

    @FindBy(how = How.ID, using = "returnToOrigination")
    private WebElement linkReturn;

    public CallUsNowToFinish(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public void verifyHeadername() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(linkReturn));
        assertEquals("Call us now to finish.", (headerName.getText()));
    }

    public void clickReturn() {
        linkReturn.click();
    }

}
