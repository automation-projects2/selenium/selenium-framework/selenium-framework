package com.usbank.test.automationFramework.trancore.pageObjects.alerts;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class AlertsLandingPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(AlertsLandingPage.class);
    private static int countOfAlerts = 50;
    private T driver;

    public AlertsLandingPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.LINK_TEXT, using = "ALERTS")
    private WebElement linkAlertsPage;

    @FindBy(id = "alerts_security_tab")
    private WebElement SecurityAlertTab;

    @FindBy(id = "alerts_account_tab")
    private WebElement AccountAlertTab;

    @FindBy(id = "alerts_fraud_tab")
    private WebElement FraudAlertTab;

    @FindBy(xpath = "//span[text()='Manage Alerts']")
    private WebElement headingManageAlerts;

    @FindBy(xpath = "//*[text() ='Profile']")
    private WebElement linkProfile;

    @FindBy(xpath = "//*[text() = 'View or Change ID Shield Image/Sound and Phrase']")
    private WebElement linkChangeImageSoundPhrase;

    @FindBy(xpath = "//div/*[@id='challengeQuestion_lbl']")
    private WebElement securityQuestion;

    @FindBy(id = "answer")
    private WebElement textBox;

    @FindBy(linkText = "Alerts History")
    private WebElement linkAlertsHistory;

    @FindBy(how = How.TAG_NAME, using = "h2")
    private WebElement headername;

    @FindBy(how = How.NAME, using = "Update Contact Information")
    private WebElement linkFraudAlertsUpdateContactInfo;


    public void clickAlertsLink() {
        linkAlertsPage.click();
    }


    public void AlertHistory() throws InterruptedException {


        countOfAlerts++;
        linkProfile.click();
        linkChangeImageSoundPhrase.click();
        System.out.println(countOfAlerts);
        if (waitForWebElement("//*[@id='challengeQuestion_lbl']")) {
            String storeSecurityQuestion = driver.findElement(By.id("challengeQuestion_lbl")).getText();
            System.out.println(storeSecurityQuestion);
            String lastWordOfSecurityQuestion = storeSecurityQuestion.substring(storeSecurityQuestion.lastIndexOf(" ") + 1, storeSecurityQuestion.length() - 1);
            textBox.sendKeys(lastWordOfSecurityQuestion);
        }
        if (waitForWebElement("//*[text() = 'SUBMIT']"))
            driver.findElement(By.xpath("//*[text() = 'SUBMIT']")).click();
        if (waitForWebElement("//*[text() = 'Change']"))
            driver.findElement(By.xpath("//*[text() = 'Change']")).click();
        if (waitForWebElement("//*[@id='radio3']"))
            driver.findElement(By.id("radio3")).click();
        if (waitForWebElement("//*[@id='imagePhrase']"))
            driver.findElement(By.id("imagePhrase")).sendKeys("wizards" + " " + countOfAlerts);
        if (waitForWebElement("//*[@id='next']"))
            driver.findElement(By.id("next")).click();
        if (waitForWebElement("//*[text() = 'Submit']"))
            driver.findElement(By.xpath("//*[text() = 'Submit']")).click();
        if (waitForWebElement("//*[text() = 'Return to Profile']"))
            driver.findElement(By.xpath("//*[text() = 'Return to Profile']")).click();
        logger.info("The alerts have been triggered");


    }


    public void waitForPageLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(linkAlertsHistory));
    }

    public boolean waitForWebElement(String element) throws InterruptedException {
        int x = 0;


        while (x <= 30) {
            List<WebElement> listWeb = driver.findElements((By.xpath(element)));
            if (listWeb.size() == 1)
                return true;
            else {
                x++;
                Thread.sleep(100);
            }

        }
        throw new NoSuchElementException("");

    }


    public void clickAlertsHistory() {

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(linkAlertsHistory));
        linkAlertsHistory.click();
    }

    public void clickFraudAlertTab() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(FraudAlertTab));
        FraudAlertTab.click();
    }

    public void clickUpdateContactInfo() {
        linkFraudAlertsUpdateContactInfo.click();
    }

    public void verifyHeaderName() {
        // TODO Auto-generated method stub

        Assert.assertEquals("manage alerts", (headername.getText()).toLowerCase());

    }

}
