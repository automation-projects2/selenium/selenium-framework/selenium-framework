package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DisputePurchaseTypePage<T extends WebDriver> {

    protected static final Logger logger = LogManager.getLogger(DisputePurchaseTypePage.class);

    protected T driver;


    public DisputePurchaseTypePage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "radioNo")
    private WebElement rdbNo;

    @FindBy(id = "radioYes")
    private WebElement rdbYes;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement btnContinue;

    @FindBy(name = "Cancel")
    private WebElement btnCancel;

    @FindBy(id = "radioM")
    private WebElement radioBtnMerchandise;

    @FindBy(id = "radioS")
    private WebElement radioBtnService;

    public WebElement getRdbNo() {
        return rdbNo;
    }

    public void clickRdbYes() {
        clickElement(driver, rdbYes);
    }

    public void waitForDisputePurchaseTypeToLoad() {

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(getRdbNo()));
    }

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        ele.click();
    }

    public void navigateThroughDisputePurchaseTypePage() {
        waitForDisputePurchaseTypeToLoad();
        clickElement(driver, rdbNo);
        clickElement(driver, btnContinue);
    }

    public void selectRadioBtnMerchandise() {
        clickElement(driver, radioBtnMerchandise);
    }

    public void selectRadioBtnService() {
        clickElement(driver, radioBtnService);
    }

    public void clickContinueButton() {
        clickElement(driver, btnContinue);
    }
}
