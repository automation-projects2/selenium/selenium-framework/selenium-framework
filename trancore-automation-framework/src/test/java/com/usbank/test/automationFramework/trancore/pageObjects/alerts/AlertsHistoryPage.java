package com.usbank.test.automationFramework.trancore.pageObjects.alerts;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class AlertsHistoryPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(AlertsHistoryPage.class);

    private T driver;

    @FindBy(xpath = "//span[text()='Alerts History']")
    private WebElement headingAlertsHistory;

    @FindBy(xpath = "//*[@id='layoutContentBody']/div/div[2]/table/tbody")
    private WebElement tableAlertHistory;

    public AlertsHistoryPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }


    public int getAlertCount(){

        Boolean isTableAlertHistoryPresent = driver.findElements(By.xpath("//*[@id='layoutContentBody']/div/div[2]/table/tbody")).size()>0;

        if(isTableAlertHistoryPresent){
            List<WebElement> alertTableRows = tableAlertHistory.findElements(By.tagName("tr"));
            return alertTableRows.size();
        }else{
            return 0;
        }
    }

    public void waitForPageLoad(){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOf(headingAlertsHistory));
    }

}
