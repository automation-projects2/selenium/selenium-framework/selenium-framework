package com.usbank.test.automationFramework.trancore.pageObjects.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


public class LockAndUnlockPage<T extends WebDriver> {

    @FindBy(how = How.XPATH, using = "//*[@id=\'layoutContentBody\']/div/div[3]/button")
    private WebElement btnLockOrUnlockCard;

    @FindBy(how = How.XPATH, using = "//*[@id=\"layoutContentBody\"]/div/div[1]/p/span[2]")
    private WebElement txtLockOrUnlock;

    @FindBy(how = How.LINK_TEXT, using = "View FAQ")
    private WebElement lnkViewFAQ;

    @FindBy(how = How.LINK_TEXT, using = "report it lost or stolen")
    private WebElement lnkReportItLostOrStolen;

    @FindBy(how = How.LINK_TEXT, using = "Return to Services")
    private WebElement lnkReturnToServices;

    private WebElement lnkMyAccount;

    @FindBy(how = How.XPATH, using = "//div[@id='tc_cardLock']")
    private WebElement bnrLockOrUnlockCard;

    @FindBy(how = How.XPATH, using = "//button[contains(@class,'tranCoreButton buttonpad lockOrUnlock-button')]")
    private WebElement btnUnlockCardFromBanner;

    private T driver;

    private static final Logger log = LogManager.getLogger(LockAndUnlockPage.class.getName());

    public LockAndUnlockPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public void lockOrUnlockCard() {

        String cardUnlockText = "This card is active and ready for use.";
        String cardLockedText = "This card is currently locked.";
        String bodyText = txtLockOrUnlock.getText();

        if (bodyText.equals(cardLockedText)) {
            Assert.assertTrue(btnLockOrUnlockCard.isDisplayed());
            log.info(bodyText);
            btnLockOrUnlockCard.click();
        } else if (bodyText.equals(cardUnlockText)) {
            Assert.assertTrue(btnLockOrUnlockCard.isDisplayed());
            log.info(bodyText);
            btnLockOrUnlockCard.click();
        }

    }

    public void unlockCardFromBanner() {

        String cardUnlockText = "This card is active and ready for use.";
        String cardLockedText = "This card is currently locked.";
        String bodyText = txtLockOrUnlock.getText();

        if (bodyText.equals(cardLockedText)) {
            Assert.assertTrue(btnLockOrUnlockCard.isDisplayed());
            log.info(bodyText);
            clickOnMyAccountPage();
            btnUnlockCardFromBanner.click();
        } else if (bodyText.equals(cardUnlockText)) {
            Assert.assertTrue(btnLockOrUnlockCard.isDisplayed());
            log.info(bodyText);
            btnLockOrUnlockCard.click();
            clickOnMyAccountPage();
            btnUnlockCardFromBanner.click();
        }

    }

    public void clickOnMyAccountPage() {
        String bodyText = driver.findElement(By.tagName("body")).getText();
        if (bodyText.contains("My Account")) {
            driver.findElement(By.linkText("My Account")).click();
        } else if (bodyText.contains("MY ACCOUNT")) {
            driver.findElement(By.linkText("MY ACCOUNT")).click();
        }

    }

    public WebElement getBtnLockOrUnlockCard() {
        return btnLockOrUnlockCard;
    }

    public void clickOnViewFAQLink() {
        Assert.assertTrue(lnkViewFAQ.isDisplayed());
        lnkViewFAQ.click();
    }

    public WebElement getLnkViewFAQ() {
        return lnkViewFAQ;
    }

    public void clickOnReportItLostOrStolen() {
        Assert.assertTrue(lnkReportItLostOrStolen.isDisplayed());
        lnkReportItLostOrStolen.click();
    }

    public WebElement getLnkReportItLostOrStolen() {
        return lnkReportItLostOrStolen;
    }

    public void clickOnReturnToServices() {
        Assert.assertTrue(lnkReturnToServices.isDisplayed());
        lnkReturnToServices.click();
    }

    public WebElement getLnkReturnToServices() {
        return lnkReturnToServices;
    }
}

