package com.usbank.test.automationFramework.trancore.stepDefinitions.rewards;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class RewardsSteps {

    @Autowired
    StepDefinitionManager stepDefinitionManager;

    @And("the user clicks the Rewards Tab")
    public void theUserClicksTheRewardsTab() {

        stepDefinitionManager.getPageObjectManager().getWebNavigationPage().clickRewardsLink();
    }
    @And("the user clicks the Rewards and Benefits Tab")
    public void theUserClicksTheRewardsAndBenefitsTab() {
        stepDefinitionManager.captureScreenshot();
        stepDefinitionManager.getPageObjectManager().getRewardsAndBenefitsPage().clickRewardsAndBenefitsLink();

    }

    @Then("the user verifies that Rewards And Benefits Summary displays for selected (.+)")
    public void theUserVerifiesThatRewardsAndBenefitsSummaryDisplays(String partner){
        stepDefinitionManager.getPageObjectManager().getRewardsAndBenefitsPage().rewardsAndBenefitsSummaryDisplayed(partner);
        //stepDefinitionManager.captureScreenshot();
    }
    @Then("the user verifies that Current points displays for selected (.+)")
    public void theUserVerifiesThatCurrentPointsDisplays(String partner){
        stepDefinitionManager.getPageObjectManager().getRewardsAndBenefitsPage().rewardsAndBenefitsCurrentPointsDisplayed(partner);
        //stepDefinitionManager.captureScreenshot();
    }
    @Then("the user verifies that Past points earned displays for selected (.+)")
    public void theUserVerifiesThatPastPointsEarnedDisplays(String partner){
        stepDefinitionManager.getPageObjectManager().getRewardsAndBenefitsPage().rewardsAndBenefitsPastPointsEarnedDisplays(partner);
    }
    @Then("the user verifies that points expired displays for selected (.+)")
    public void theUserVerifiesThatPointsExpiredDisplays(String partner){
        stepDefinitionManager.getPageObjectManager().getRewardsAndBenefitsPage().rewardsAndBenefitsPointsExpired(partner);
    }
    @Then("the user verifies that Redeem Points Link displays for selected (.+)")
    public void theUserVerifiesThatRedeemPointsLinkDisplays(String partner){
        //stepDefinitionManager.captureScreenshot();
        stepDefinitionManager.getPageObjectManager().getRewardsAndBenefitsPage().rewardsAndBenefitsRedeemPointLink(partner);
            }
    @Then("the user verifies that Account Management Link and sub links displays for selected (.+)")
    public void theUserVerifiesThatAccountManagementLinkAndSubLinksDisplays(String partner){
       stepDefinitionManager.getPageObjectManager().getRewardsAndBenefitsPage().rewardsAndBenefitsAccountManagementLink(partner);
        //stepDefinitionManager.captureScreenshot();
    }
    @Then("the user verifies that Travel Protection Link and sub links displays for selected (.+)")
    public void theUserVerifiesThatTravelProtectionLinkAndSubLinksDisplays(String partner){
        stepDefinitionManager.getPageObjectManager().getRewardsAndBenefitsPage().rewardsAndBenefitsTravelBenefitsLink(partner);
    }
    @Then("the user verifies that Card Protection Link and sub links displays for selected (.+)")
    public void theUserVerifiesThatCardProtectionLinkAndSubLinksDisplays(String partner){
        stepDefinitionManager.getPageObjectManager().getRewardsAndBenefitsPage().rewardsAndBenefitsMobileAppLink(partner);
        //stepDefinitionManager.captureScreenshot();
    }
    @Then("the user verifies that See All Benefits Link & Foot Notes displays")
    public void theUserVerifiesThatSeeAllBenefitsLinkAndFootNotesDisplays() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getRewardsAndBenefitsPage().rewardsAndBenefitsSeeAllBenefitsLinkAndFootNotes();
        //stepDefinitionManager.captureScreenshot();
    }

    @And("the user clicks on Link To Rewards button")
    public void theUserClicksOnLinkToRewardsButton() {

        stepDefinitionManager.getPageObjectManager().getRewardsPage().clickLinkToRewardsLink();
    }

    @Then("the user is navigated to Rewards Center Page")
    public void theUserIsNavigatedToRewardsCenterPage() {

        stepDefinitionManager.getPageObjectManager().getRewardsPage().rewardsCenterPageFirstNameDisplayed();

    }

    @When("the user clicks on Back to Account")
    public void theUserClicksOnBackToAccount() {

        stepDefinitionManager.getPageObjectManager().getRewardsPage().clickBackToAccount();

    }

    @Then("the user should be navigated back to Trancore My Account page")
    public void theUserShouldBeNavigatedBackToTrancoreMyAccountPage() {
        //stepDefinitionManager.getPageObjectManager().getRewardsPage().updateTheURL();
        stepDefinitionManager.getPageObjectManager().getRewardsPage().myAccountPageDisplayed();

    }
}
