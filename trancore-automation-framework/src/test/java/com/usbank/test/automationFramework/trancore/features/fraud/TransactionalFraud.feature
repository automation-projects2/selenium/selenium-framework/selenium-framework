@Fraud
Feature: Transactional Fraud
  Transactional Fraud Claim Submission


   #checkedmessage
  Scenario Outline: Validate Less Than 45 Order Card page and Continue button functionality of Address Changed page

    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    And the user clicks the POSTED Tab
    And the user selects transaction with <transaction_date> and <transaction_description>
    And the user navigate through Dispute suspect type page
    And the user navigates through Fraud App Check page
    And the user navigates through Check Profile Info page
    And the user navigates through Fraud Other Transactions page
    And the user navigates through Card Issuance page
    And the user navigates through Fraud Discovery page through Not Received
    And the user navigates through Fraud Suspect page
    When the user the user selects No radio button and click continue on Police Report page
    Then Less Than 45 page should be displayed properly
    When the user selects any radio button in first question and No radio button in second question on Less Than 45 page
    Then Address Changed page should be displayed properly
    When the user selects Yes radio button in first question and Yes radio button in second question on Address Changed page
    Then Card Use page should be displayed properly

    Examples:
      | partner | user_name  | password    | transaction_date | transaction_description            |

  Scenario Outline: Validate Review Transaction page and Card Lost Stolen page

    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    And the user clicks the POSTED Tab
    And the user selects transaction with <transaction_date> and <transaction_description>
    And the user navigate through Dispute suspect type page
    And the user navigates through Fraud App Check page
    When the user is on Check Profile Info page and click on continue
    Then Fraud Other Transactions page should be displayed properly
    When the user selects third radio button and click continue on Fraud Other Transactions page
    Then Review Transactions page should be displayed properly
    And the user navigates through Card Issuance page
    And the user navigates through Fraud Discovery page through Not Received
    And the user navigates through Fraud Suspect page
    And the user navigates through Police Report page to Possess Cards page
    When the user selects No radio button and click continue on Possess Cards page
    Then Card Lost Stolen page should be displayed properly
    When the user selects Not Received radio button and click continue on Card Lost Stolen page
    Then Card Use page should be displayed properly

    Examples:
      | partner   | user_name | password    | transaction_date | transaction_description                     |
      | Elan | waw1196  | wizards2020 | 11/12/20        | TLF*KENNEDYS FLOWER SH 616-9566747 MI    |


  Scenario Outline: Validate Continue button functionality for Card In Hand page

    Given the user has navigated to the <partner> login page using SSO <hashid>
    And the user clicks the POSTED Tab
    And the user selects transaction with <transaction_date> and <transaction_description>
    And the user navigate through Dispute suspect type page
    And the user navigates through Fraud App Check page
    And the user navigates through Check Profile Info page
    And the user navigates through Fraud Other Transactions page
    And the user navigates through Card Issuance page
    And the user navigates through Fraud Discovery page through LostStolen
    And the user navigates through Fraud Suspect page
    And the user navigates through Police Report page
    Then Card Use page should be displayed properly
    When the user selects checkbox and click continue on Card Use page
    Then Card in Hand page should be displayed properly
    When the user selects checkbox and click continue on Card In Hand page
    Then Travel page should be displayed properly

    Examples:
      | partner   | hashid                                                           | transaction_date | transaction_description                |

  Scenario Outline: Validate Review Transactions page by selecting No radio button

    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    And the user clicks the POSTED Tab
    And the user selects transaction with <transaction_date> and <transaction_description>
    And the user navigate through Dispute suspect type page
    And the user navigates through Fraud App Check page
    When the user is on Check Profile Info page and click on continue
    Then Fraud Other Transactions page should be displayed properly
    When the user selects third radio button and click continue on Fraud Other Transactions page
    Then Review Transactions page should be displayed properly and user selects No radio button
    Then the user is on Wrap up Fraud Page and returns to origination

    Examples:
      | partner | user_name  | password     |transaction_date | transaction_description       |

  # Cancel Fraud Modal pop up -1
  Scenario Outline: Fraud Iteration #2 for verifying Cancel Fraud Knockout from Fraud App check page

    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    And the user clicks the POSTED Tab
    And the user selects transaction with <transaction_date> and <transaction_description>
    And the user navigate through Dispute suspect type page
    And the user navigates through Fraud App Check page by selecting No radio button
    When the user is on Unauthorized Account Activity Fraud App check page and click cancel
    Then the user gets Cancel Fraud Modal popup



    Examples:
      | partner | user_name  | password     |transaction_date | transaction_description       |


  # Cancel Fraud Modal pop up -2
  Scenario Outline: Fraud Iteration #2 for verifying Cancel Fraud Knockout from check profile info page

    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    And the user clicks the POSTED Tab
    And the user selects transaction with <transaction_date> and <transaction_description>
    And the user navigate through Dispute suspect type page
    And the user navigates through Fraud App Check page
    And the user navigates through Check Profile Info page by selecting No radio button
    When the user is on Unauthorized Account Activity Fraud App check page and click cancel
    Then the user gets Cancel Fraud Modal popup

    Examples:
      | partner | user_name  | password     |transaction_date | transaction_description       |


  # Cancel Fraud Modal pop up -3
  Scenario Outline: Fraud Iteration #3 for verifying Wrap up Fraud from Fraud Other Transaction page

    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    And the user clicks the POSTED Tab
    And the user selects transaction with <transaction_date> and <transaction_description>
    And the user navigate through Dispute suspect type page
    And the user navigates through Fraud App Check page
    And the user navigates through Check Profile Info page
    When the user navigates through Fraud Other Transactions page by Yes radio button
    Then the user is on Wrap up Fraud Page and returns to origination


    Examples:
      | partner | user_name  | password     |transaction_date | transaction_description       |
