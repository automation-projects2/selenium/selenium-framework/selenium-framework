package com.usbank.test.automationFramework.trancore.stepDefinitions.services;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import static org.hamcrest.CoreMatchers.is;

public class ServicesStepDefs {

    @Autowired
    private StepDefinitionManager stepDefinitionManager;

    protected static final Logger logger = LogManager.getLogger(ServicesStepDefs.class);

    @When("I click Services Tab")
    public void I_click_Services_Tab(){
        stepDefinitionManager.getPageObjectManager().getMyAccountPage().clickLinkServices();
     //   stepDefinitionManager.getPageObjectManager().getServicesPage().waitForPageLoad();
    }

    @And("I click Link Enroll in Click to pay with card")
    public void I_click_Link_Enroll_in_Click_to_pay_with_card(){

        stepDefinitionManager.getPageObjectManager().getServicesPage().clickLinkEnrollInClickToPayWithCard();
        stepDefinitionManager.getPageObjectManager().getClickToPayPage().waitForPageLoad();
    }

    @Then("I must be directed to Click to Pay page")
       public void I_must_be_directed_to_Click_to_Pay_page(){

        logger.info("Click to pay with Card Heading: " + stepDefinitionManager.getPageObjectManager().getClickToPayPage().getHeadingClickToPayWithCard().getText());

        Assert.assertEquals("Click to pay with card",
        stepDefinitionManager.getPageObjectManager().getClickToPayPage().getHeadingClickToPayWithCard().getText());
    }

    @And("I click Link Setup or manage my digital wallet")
    public void i_click_link_setup_or_manage_my_digital_wallet(){
        stepDefinitionManager.getPageObjectManager().getServicesPage().clickLinkSetupOrManageMyDigitalWallet();
        //stepDefinitionManager.getPageObjectManager().getDigitalWalletPage().waitForPageLoad();
    }

    @Then("I must be directed to Digital Wallet page")
    public void i_must_be_directed_to_digital_wallet_page(){
        Assert.assertEquals("DigitalWallet is Available without any issue",stepDefinitionManager.getPageObjectManager().getDigitalWalletPage().getHeadingDigitalWallet().isDisplayed(),true);
        //Assert.assertThat("DigitalWallet is Available without any issue", is(stepDefinitionManager.getPageObjectManager().getDigitalWalletPage().getHeadingDigitalWallet().isDisplayed()));
    }

    @And("I visit helpful Mobile Payments Website")
    public void iVisitHelpfulMobilePaymentsWebsite() {
        Assert.assertEquals("Url is working fine", stepDefinitionManager.getPageObjectManager().getDigitalWalletPage().checkMobilePaymentWebsite(),true);
    }

    @And("I click Link - Lock or unlock card")
    public void i_click_link_Lock_or_unlock_card(){
        stepDefinitionManager.getPageObjectManager().getServicesPage().clickLinkLockOrUnlockCard();
    }


    @And("I click Link - Travel Notification")
    public void i_click_Link_Travel_Notification(){
        stepDefinitionManager.getPageObjectManager().getServicesPage().clickLinkTravelNotification();
    //    stepDefinitionManager.getPageObjectManager().getTravelNotificationPage().waitForPageLoad();

    }

    @Then("I must be directed to Travel Notification Page")
    public void I_must_be_directed_to_Travel_Notification_Page(){

       Assert.assertEquals(stepDefinitionManager.getPageObjectManager().getTravelNotificationPage().getHeadingTravelNotification().isDisplayed(), true);
    }

    @And("I click Link - Add Authorized User")
    public void iClickLinkAddAuthorizedUser() {
        stepDefinitionManager.getPageObjectManager().getServicesPage().clickLinkAddAuthorizedUser();
    }

    @And("I click link - Joint Owner PDF")
    public void iClickLinkJointOwnerPDF() {
        stepDefinitionManager.getPageObjectManager().getServicesPage().clickLinkAddJointOwnerPDF();
    }

    @And("I click link - Convenience Check Request")
    public void iClickLinkConvenienceCheckRequest() {
        stepDefinitionManager.getPageObjectManager().getServicesPage().clickLinkRequestConvenienceChecks();
    }

    @And("I click link - Spend Analysis")
    public void iClickLinkSpendAnalysis() {
        //stepDefinitionManager.getPageObjectManager().getServicesPage().clickLinkSpendAnalysis();
    }

    @And("I click link - Manage Employees")
    public void iClickLinkManageEmployees() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getServicesPage().clickLinkManageEmployee();
    }

    @And("I click link -  Add Authorized Representative PDF")
    public void iClickLinkAddAuthorizedRepresentativePDF() {
        stepDefinitionManager.getPageObjectManager().getServicesPage().clickLinkAddAuthorizedRepresentativePDF();
    }

    @And("I click Link - Free Credit Score")
    public void iClickLinkFreeCreditScore() {
        stepDefinitionManager.getPageObjectManager().getServicesPage().clickLinkFreeCreditScore();
    }

    @And("I click Link - Enroll In Click To Pay With Card")
    public void iClickLinkEnrollInClickToPayWithCard() {
        stepDefinitionManager.getPageObjectManager().getServicesPage().clickLinkEnrollInClickToPayWithCard();
    }

    @And("I click link - Balance Transfer")
    public void iclickLinkBalanceTransfer() {
        stepDefinitionManager.getPageObjectManager().getServicesPage().clickLinkBalanceTransfer();
    }
}
