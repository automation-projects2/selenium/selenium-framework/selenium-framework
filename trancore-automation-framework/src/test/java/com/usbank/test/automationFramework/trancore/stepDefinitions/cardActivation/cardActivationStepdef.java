package com.usbank.test.automationFramework.trancore.stepDefinitions.cardActivation;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class cardActivationStepdef {

    @Autowired
    StepDefinitionManager stepDefinitionManager;

    @And("the user click on Card Activation link at Service Tab")
    public void theUserClickOnCardActivationLinkAtServiceTab() {
        stepDefinitionManager.getPageObjectManager().getServicesLandingPage().clickCardActivation();
    }

    @And("the user is navigated to Card Activation Page")
    public void theUserIsNavigatedToCardActivationPage() {
        stepDefinitionManager.getPageObjectManager().getCardActivationPage().verifyHeaderName();

    }

    @And("the user clicks on Submit Button at Card Activation Page")
    public void theUserClicksOnSubmitButtonAtCardActivationPage() {
        stepDefinitionManager.getPageObjectManager().getCardActivationPage().clickSubmitButton();
    }

    @Then("the user validates the displayed error messages for Card Activation Page")
    public void theUserValidatesTheDisplayedErrorMessagesForCardActivationPage() {
        stepDefinitionManager.getPageObjectManager().getCardActivationPage().validateErrorText();
    }

    @And("the user clicks on Cancel button at Card Activation Page")
    public void theUserClicksOnCancelButtonAtCardActivationPage() {
        stepDefinitionManager.getPageObjectManager().getCardActivationPage().clickCancelButton();
    }

    @And("the user enter (.+) CVV code at Card Activation Page")
    public void theUserEnterCVVCVVCodeAtCardActivationPage(String CVVCode) {
        stepDefinitionManager.getPageObjectManager().getCardActivationPage().setTxtFldCVV(CVVCode);
    }

    @And("the user enters (.+) last 4 SSN at Card Activation Page")
    public void theUserEntersSSNSSNAtCardActivationPage(String Last4SSN) {
        stepDefinitionManager.getPageObjectManager().getCardActivationPage().setTxtFldLast4SSN(Last4SSN);
    }

    @Then("the user is navigated to Card Activation Unsuccessful Page")
    public void theUserIsNavigatedToCardActivationUnsuccessfulPage() {
        stepDefinitionManager.getPageObjectManager().getCardActivationPage().ValidateUnsucessfulCardActivation();

    }

    @Then("the user clicks on Return to Card Activation Page")
    public void theUserClicksOnReturnToCardActivationPage() {
        stepDefinitionManager.getPageObjectManager().getCardActivationPage().clickReturnToCardActivation();
    }

    @Then("the card is activated and user is navigated to Card Activation Page")
    public void theCardIsActivatedAndUserIsNavigatedToCardActivationPage() {
        stepDefinitionManager.getPageObjectManager().getCardActivationPage().ValidateSucessfulCardActivation();
    }

    @Then("the user validates the Invalid Error messages for Card Activation Page")
    public void theUserValidatesTheInvalidErrorMessagesForCardActivationPage() {
        stepDefinitionManager.getPageObjectManager().getCardActivationPage().validateErrorTextforInvalidAndBlankFields();
    }

    @Then("the user should be able to validate already Card Activated dialog box and close it")
    public void theUserShouldBeAbleToValidateAlreadyCardActivatedDialogBoxAndCloseIt() {
        stepDefinitionManager.getPageObjectManager().getCardActivationPage().validateAlreadyCardActivatedDialogueBox();
    }
}
