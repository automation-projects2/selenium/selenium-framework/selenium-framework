package com.usbank.test.automationFramework.trancore.pageObjects.creditLineIncrease;

import com.usbank.test.automationFramework.trancore.pageObjects.services.AddAuthorizedUserPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

public class HouseInformationPage<T extends WebDriver> {
    private static final Logger logger = LogManager.getLogger(AddAuthorizedUserPage.class);

    private static final long EXPLICIT_WAIT_TIME_SECONDS = 30;

    private T driver;

    @FindBy(how = How.TAG_NAME, using = "h2")
    private WebElement headerName;

    @FindBy(how = How.TAG_NAME, using = "h1")
    private WebElement subHeader;

    @FindBy(how = How.ID, using = "addressCheckBox")
    private WebElement checkboxJointAddress;

    @FindBy(how = How.NAME, using = "monthlyPayment")
    private WebElement textFieldMonthlyPayment;

    @FindBy(how = How.ID, using = "coOwnerMonthlyPayment")
    private WebElement textFieldJointMonthlyPayment;

    @FindBy(how = How.ID, using = "housingFormSubmit")
    private WebElement buttonReviewMyRequest;

    public HouseInformationPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);

    }

    public void verifyHeaderHousingInfomation() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(buttonReviewMyRequest));
        logger.info(headerName.getText().toLowerCase());
        assertEquals("Credit limit increase\n" +
                "Step 2 of 3", (headerName.getText()));

        assertEquals("housing", subHeader.getText().toLowerCase());
    }

    public void enterMonthlyPayments(String MonthlyPayments) {
        Assert.assertTrue("Combined Monthly Housing Payment is not displayed",textFieldMonthlyPayment.isDisplayed());
        textFieldMonthlyPayment.sendKeys(MonthlyPayments);
    }

    public void enterJointMonthlyPayments(String JointMonthlyPayments) {
        textFieldJointMonthlyPayment.sendKeys(JointMonthlyPayments);
    }

    public void setCheckboxJointAddress() {
        checkboxJointAddress.click();
    }

    public void selectSameAddress(String SameAddress){
        if(SameAddress.equalsIgnoreCase("Yes")||(SameAddress.equalsIgnoreCase("No"))){
            WebElement radioButtonSelectAddress = driver.findElement(By.id("clmHousing"+SameAddress+"RadioBtn"));
            radioButtonSelectAddress.click();
        } else {
            logger.error(SameAddress+"is bad data, Only Yes or No should be provided");
        }
    }

    public void selectPrimaryCurrentHome(String PrimaryHome) {
        if (PrimaryHome.equals("Yes") || (PrimaryHome.equals("No"))) {
            WebElement radioButtonCurrentHome = driver.findElement(By.xpath("//label[contains(text(),\"own your current home?\")]/following-sibling::label[@id='clmHousing"+PrimaryHome+"RadioBtn']"));
            radioButtonCurrentHome.click();

        } else {
            logger.error(PrimaryHome + " is bad data, Only Yes or No should be provided");
        }
    }

    public void selectCoOwnerCurrentHome(String CoOwnerCurrentHome) {
        if (CoOwnerCurrentHome.equals("Yes")) {
            WebElement radioButtonCurrentHome = driver.findElement(By.id("jointCurrentHomeYes"));
            radioButtonCurrentHome.click();

        } else if (CoOwnerCurrentHome.equals("No")) {
            WebElement radioButtonCurrentHome = driver.findElement(By.id("jointNo"));
            radioButtonCurrentHome.click();
        } else {
            logger.error(CoOwnerCurrentHome + " is bad data, Only Yes or No should be provided");
        }
    }

    public void clickReviewMyRequest() {
        Assert.assertTrue("Revire my Request button is not displayed", buttonReviewMyRequest.isDisplayed());
        buttonReviewMyRequest.click();
    }


}
