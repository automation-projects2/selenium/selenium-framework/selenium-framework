package com.usbank.test.automationFramework.trancore.stepDefinitions.virtualCard;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class virtualCardSteps {
    @Autowired
    StepDefinitionManager stepDefinitionManager;
    Integer clickCount=0;

    @When("^the user clicked Virtual Card link from Services tab$")
    public void click_on_VC() throws InterruptedException {
       //stepDefinitionManager.captureScreenshot();
        stepDefinitionManager.getPageObjectManager().getVirtualCard().navigateToVC();
    }
    @Then("^Single OTP page is displayed with (.+) contact$")
    public void single_otp_page_displayed(String contact) throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getVirtualCard().verifySingleOTPPage(contact);
        //stepDefinitionManager.captureScreenshot();
    }
    @Then("^Multiple OTP page is displayed$")
    public void multiple_otp_page_displayed() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getVirtualCard().verifyMultipleOTPPage();
    }
    @Then("^OTP page is displayed on click of send One Time Passcode button$")
    public void display_otp_page_on_sendOTP_button_click() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getVirtualCard().clickSendOTP();
    }
    @Then("^the user enter (.+) OTP and submit$")
    public void user_enter_OTP_and_submit(String OTP) throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getVirtualCard().clickContinuewithOTP(OTP);
        clickCount= 1;
    }

    @Then("^the (.+) Error displays$")
    public void the_error_displays(String errorMessage) throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getVirtualCard().errorCheck(errorMessage,clickCount);
    }
    @Then("^the user enter invalid (.+) OTP and submit three times$")
    public void user_enter_wrong_OTP_and_submit_thrice(String OTP) throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getVirtualCard().clickContinuewithOTP(OTP);
        stepDefinitionManager.getPageObjectManager().getVirtualCard().clickContinuewithOTP(OTP);
        stepDefinitionManager.getPageObjectManager().getVirtualCard().clickContinuewithOTP(OTP);
        clickCount= 3;
    }
    @Then("^Send New Code button is hidden$")
    public void send_new_code_button_is_hidden() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getVirtualCard().verifySendCodebtn();
    }
    @Then("^the user is redirected to ContactUsForHelp page$")
    public void the_user_redirected_to_contactus_for_help() throws InterruptedException {
        //stepDefinitionManager.getPageObjectManager().getVirtualCard().verifyContactUsForHelpPage();
        stepDefinitionManager.getPageObjectManager().getVirtualCard().contactUsForHelpDisplayed();
    }
    @And("^the user enter valid OTP$")
    public void the_user_enter_valid_OTP() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getVirtualCard().clickContinuewithOTP("111111");
    }
    @Then("^the user is redirected to VirtualCard page$")
    public void the_user_is_redirected_to_VC() throws InterruptedException {
        //stepDefinitionManager.getPageObjectManager().getVirtualCard().clickContinuewithOTP("111111");
    }

    @And("the user clicks on Services tab")
    public void theUserClicksOnServicesTab() throws InterruptedException {
            stepDefinitionManager.getPageObjectManager().getVirtualCard().clickServicesTab();
    }

    @Then("user should NOT see Virtual Card link")
    public void userShouldNOTSeeVirtualCardLink() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getVirtualCard().noVirtualCardLink();
    }

    @Then("the user is able to view VirtualCard Credentials")
    public void theUserIsAbleToViewVirtualCardCredentials() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getVirtualCard().displayVirtualCard();
            }

    @Then("^the user select CB (.+) account$")
    public void select_ae_account(String account) throws InterruptedException {
         stepDefinitionManager.getPageObjectManager().getVirtualCard().openSelectedAccount(account);
    }

        @Then("^the user able to enter (.+) OTP and submit$")
    public void user_able_to_enter_OTP_and_submit(String OTP) throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getVirtualCard().ableToEnterOTP(OTP);
        }
}

