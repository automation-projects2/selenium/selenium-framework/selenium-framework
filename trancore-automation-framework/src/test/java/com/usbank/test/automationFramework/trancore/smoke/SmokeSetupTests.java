package com.usbank.test.automationFramework.trancore.smoke;

import com.usbank.test.automationFramework.trancore.regression.RegressionBase;
import com.usbank.test.enums.BrowserType;
import org.junit.Test;


public class SmokeSetupTests extends RegressionBase {

    @Test
    public void testOpenPartnerPage() {
        pageObjectManager.initPageObjectDriver(RunMode.valueOf(automationConfig.getRunMode()), BrowserType.valueOf(automationConfig.getBrowserType()));
        openPatnerPage();
    }

}
