package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CardLostStolenPage<T extends WebDriver> {

    protected static final Logger logger = LogManager.getLogger(CardLostStolenPage.class);

    protected T driver;


    public CardLostStolenPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement btnContinue;

    @FindBy(xpath = "//*[text()='Sorry to hear you don’t have the card(s).']")
    private WebElement lblCardLostStolen;

    @FindBy(id = "5028-Lost")
    private WebElement rdbLost;

    @FindBy(id = "5028-Stolen")
    private WebElement rdbStolen;

    @FindBy(id = "5028-Not Received As Issued")
    private WebElement rdbNotReceived;

    public WebElement getLblCardLostStolen() {
        return lblCardLostStolen;
    }

    public void waitForCardLostStolenPageToLoad() {

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(lblCardLostStolen));
    }

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        ele.click();
    }

    public void clickLostRadioButton() {
        waitForCardLostStolenPageToLoad();
        clickElement(driver, rdbLost);
    }

    public void clickStolenRadioButton() {
        waitForCardLostStolenPageToLoad();
        clickElement(driver, rdbStolen);
    }

    public void clickNotReceivedRadioButton() {
        waitForCardLostStolenPageToLoad();
        clickElement(driver, rdbNotReceived);
    }

    public void clickContinue() {
        clickElement(driver, btnContinue);
    }
}
