package com.usbank.test.automationFramework.trancore.stepDefinitions.incomePrompt;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class incomePromptCLIStepDef {

    @Autowired
    StepDefinitionManager stepDefinitionManager;





    @And("the user enter (.+) CoOwner Income Source at Income Prompt Page")
    public void theUserEnterCo_IncSourceCoOwnerIncomeSourceAtIncomePromptPage(String Co_IncSource) {
        stepDefinitionManager.getPageObjectManager().getIncomePrompt().setDropdownCoIncomeSource(Co_IncSource);
    }
    @Given("the user should be navigated to Income Prompt page")
    public void the_user_should_be_navigated_to_Income_Prompt_page() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getIncomePrompt().verifyHeaderIncomePrompt();
    }

    @Given("the user should be able to see textfields and DropDown buttons for Joint Owners IncomePrompt Page")
    public void the_user_should_be_able_to_see_textfields_and_DropDown_buttons_for_Joint_Owners_IncomePrompt_Page() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getIncomePrompt().verifyPriOwnwerFields();
        stepDefinitionManager.getPageObjectManager().getIncomePrompt().verifyCoOwnwerFields();
    }

    @When("the user clicks Submit and validates error message for all fields of Joint Owners Income Prompt Page")
    public void the_user_clicks_Submit_and_validates_error_message_for_all_fields_of_Joint_Owners_Income_Prompt_Page() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getIncomePrompt().clickSubmitButton();
        stepDefinitionManager.getPageObjectManager().getIncomePrompt().verifyErrorText();
    }

    @When("the user enters (.+) Primary Income at Income Prompt Page")
    public void the_user_enters_Primary_Income_at_Income_Prompt_Page(String PriIncome) {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getIncomePrompt().setTextFieldPrimaryIncome(PriIncome);
    }

    @When("the user enters (.+) CoOwner Income at Income Prompt Page")
    public void the_user_enters_CoOwner_Income_at_Income_Prompt_Page(String CoIncome) {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getIncomePrompt().setTextFieldCoIncome(CoIncome);
    }

    @Then("the user clicks on the Submit Button at Income Prompt Page")
    public void the_user_clicks_on_the_Submit_Button_at_Income_Prompt_Page() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getIncomePrompt().clickSubmitButton();
    }

    @Then("the user validate error for Owners Income Source at Income Prompt page")
    public void the_user_validate_error_for_Owners_Income_Source_at_Income_Prompt_page() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getIncomePrompt().verifyErrorText();
    }

    @Then("the user enters (.+) Primary Income Source at Income Prompt Page")
    public void the_user_enters_Employment_Income_Primary_Income_Source_at_Income_Prompt_Page(String Pri_IncSource) {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getIncomePrompt().setDropdownPriIncomeSource(Pri_IncSource);
    }

    }
