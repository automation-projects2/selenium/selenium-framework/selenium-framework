package com.usbank.test.automationFramework.trancore.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class BasePage<T extends WebDriver> {
    protected T driver;

    public BasePage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }
}
