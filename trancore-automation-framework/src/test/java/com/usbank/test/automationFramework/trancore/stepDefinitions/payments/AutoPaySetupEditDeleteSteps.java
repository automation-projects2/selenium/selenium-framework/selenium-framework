package com.usbank.test.automationFramework.trancore.stepDefinitions.payments;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class AutoPaySetupEditDeleteSteps {

    @Autowired
    StepDefinitionManager stepDefinitionManager;

    private static final Logger logger = LogManager.getLogger(AutoPaySetupEditDeleteSteps.class);


    @And("the user clicks the Setup AutoPay button")
    public void theUserClicksTheSetupAutoPayButton() {
        stepDefinitionManager.getPageObjectManager().getAutoPayPage().clickSetUpAutoPayButton();
    }

    @Then("the user selects (.+) Payment Amount and Payment Day")
    public void the_User_Selects_Payment_Amount_And_Payment_Day( String paymentAmount) {
        stepDefinitionManager.getPageObjectManager().getAutoPayPage().selectPaymentAmount(paymentAmount);
        stepDefinitionManager.getPageObjectManager().getAutoPayPage().selectPaymentDay();

    }

    @Then("the user clicks on the Next button in Setup AutoPay 1of3 page")
    public void theUserClicksOnTheNextButtonInSetupAutoPay1Of2Page() {

        stepDefinitionManager.getPageObjectManager().getAutoPayPage().clickNextInSetUpAutoPayPage1();
        stepDefinitionManager.getPageObjectManager().getAutoPayPage().acceptAutoPayTermsAndConditions();
    }

    @Then("user clicks the Submit Button in Setup AutoPay 2of3 page")
    public void userClicksTheSubmitButtonInSetupAutoPay2Of2Page() {
        stepDefinitionManager.getPageObjectManager().getAutoPayPage().submitButtonInSetUpAutoPayPage2of3();
    }

    @Then("the user navigates to Setup AutoPay 3of3 page and verify that Setup AutoPay successful message displayed")
    public void theUserNavigatesToSetupAutoPay3Of3PageAndVerifyThatSetupAutoPaySuccessfulMessageDisplayed() {

        stepDefinitionManager.getPageObjectManager().getAutoPayPage().theUserNavigatesToSetupAutoPay3Of3Page();
        stepDefinitionManager.getPageObjectManager().getAutoPayPage().verifySetupAutoPaySuccessfulMessage();
    }

    @Then("the user click on Edit Scheduled Payments")
    public void theUserClickOnEditScheduledPayments() {

        stepDefinitionManager.getPageObjectManager().getAutoPayPage().clickOnEditScheduledPaymentsInAutoPaySetUpPage30f3();

    }

    @Then("verify that AutoPay Setup is displaying")
    public void verifyThatAutoPaySetupIsDisplaying() {

        stepDefinitionManager.getPageObjectManager().getAutoPayPage().verifyAutoPaySetUpInViewPaymentSchedule();
    }

    @Then("the user clicks the View AutoPay button")
    public void theUserClicksTheViewAutoPayButton() {
        System.out.println("View AutoPay Def ");
        stepDefinitionManager.getPageObjectManager().getAutoPayPage().clickViewAutoPayButton();

    }

    @Then("user clicks on the AutoPay Setup Delete hyperlink")
    public void userClicksOnTheAutoPaySetupDeleteHyperlink() {

        stepDefinitionManager.getPageObjectManager().getAutoPayPage().deleteInAutoPaySetup();
    }

    @Then("Delete Confirmation Dialog displays and the user clicks Continue button")
    public void deleteConfirmationDialogDisplaysAndTheUserClicksContinueButton() throws InterruptedException{
    stepDefinitionManager.getPageObjectManager().getAutoPayPage().deletePopUpInAutoPaySetup();

    }

    @Then("verify that delete confirmation message displayed in Payment Schedule")
    public void verifyThatDeleteConfirmationMessageDisplayedInPaymentSchedule() throws InterruptedException{
        stepDefinitionManager.getPageObjectManager().getAutoPayPage().deleteAutoPayConfirmationMessageInPaymentSchedule();
    }

    @Then("user clicks on the Edit AutoPay Setup hyperlink")
    public void userClicksOnTheEditAutoPaySetupHyperlink() {

    stepDefinitionManager.getPageObjectManager().getAutoPayPage().clickEditAutoPaySetupLink();

    }

    @Then("the user clicks on the Next button in Edit AutoPay 1of3 page")
    public void theUserClicksOnTheNextButtonInEditAutoPay1Of2Page() {

        stepDefinitionManager.getPageObjectManager().getAutoPayPage().clickNextInSetUpAutoPayPage1();

    }

    @Then("user clicks the Submit Button in Edit AutoPay 2of3 page")
    public void userClicksTheSubmitButtonInEditAutoPay2Of2Page() {
        stepDefinitionManager.getPageObjectManager().getAutoPayPage().submitButtonInSetUpAutoPayPage2of3();
    }

    @Then("the user navigates to Edit AutoPay 3of3 page and verify that edit AutoPay successful message displayed")
    public void theUserNavigatesToEditAutoPay3Of3PageAndVerifyThatEditAutoPaySuccessfulMessageDisplayed() {

        stepDefinitionManager.getPageObjectManager().getAutoPayPage().theUserNavigatesToEditAutoPay3Of3Page();
        stepDefinitionManager.getPageObjectManager().getAutoPayPage().verifyEditAutoPaySuccessfulMessage();
    }


    @Then("the user click on View Scheduled Payments")
    public void theUserClickOnViewScheduledPayments() {
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().clickOnViewPaymentScheduledLink();

    }
}
