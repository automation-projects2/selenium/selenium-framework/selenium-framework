package com.usbank.test.automationFramework.trancore.pageObjects.otp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class OTPScreenPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(OTPScreenPage.class);

    private static final long EXPLICIT_WAIT_TIME_SECONDS = 30;

    private T driver;


    @FindBy(how = How.TAG_NAME, using = "h1")
    private WebElement OtpHeader;

    @FindBy(how = How.ID, using = "sendOTP")
    private WebElement linkSendOTP;

    @FindBy(xpath = "//button[text()='Send a one-time code']")
    private WebElement btnSendOTP;

    @FindBy(how = How.ID, using = "returnToServices")
    private WebElement linkCancel;

    @FindBy(how = How.CLASS_NAME, using = "errortext")
    private WebElement errorMessage;

    @FindBy(xpath = "//h1/span")
    private WebElement txtHeader;

    public OTPScreenPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }


    public void verifyHeaderName() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(txtHeader));
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        String headename = txtHeader.getText();
        assertEquals("one-time passcode", headename.toLowerCase());
    }

    public void ClickSendOTP() throws InterruptedException {
        //new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.elementToBeClickable(linkSendOTP));
        System.out.println("Waiting here");
        Thread.sleep(5000);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", linkSendOTP);
        //linkSendOTP.click();


    }

    public void selectContactForOTP(String Value) throws InterruptedException {

        if (!Value.equals("SingleContact")) {
            String type;
            if (Value.matches("[0-9]+")) {
                type = "OnlyNumbers";
            } else {
                type = "emailonly";
            }
            System.out.println("Type is : "+type);
             List<WebElement> selectRadioButton = driver.findElements(By.className("label__container"));
            switch (type) {
                case ("OnlyNumbers"): {
                    for (int i = 0; i < selectRadioButton.size(); i++) {

                        if (selectRadioButton.get(i).getText().equals("Text: ***-***-" + Value)) {
                            selectRadioButton.get(i).click();
                            break;
                        }
                    }
                    break;
                }

                case ("emailonly"): {

                     //driver.findElement(By.xpath("//p[text()='Email: "+Value+"']")).click();
                    for (int i = 0; i < selectRadioButton.size(); i++) {
                        System.out.println("Email is first: "+selectRadioButton.get(i).getText());
                        Thread.sleep(15000);
                        if (selectRadioButton.get(i).getText().equals("Email: " + Value)) {
                            //selectRadioButton.get(i).click();
                            System.out.println("Email is: "+selectRadioButton.get(i).getText());
                             selectRadioButton.get(i).click();
                            break;
                        }
                    }
                    break;
                }

            }

        }
    }


    public void ClickCancel() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        linkCancel.click();
    }

    public void verifyErrorMsg() {
        if (linkSendOTP.isDisplayed()) {
            assertEquals("It looks like you're having some trouble. Can we send you a new code?", errorMessage.getText());
        }

    }
}


