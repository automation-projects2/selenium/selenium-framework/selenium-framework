package com.usbank.test.automationFramework.trancore.stepDefinitions.addAUPrompt;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class AddAUPromptSteps {

    @Autowired
    StepDefinitionManager stepDefinitionManager;

    @And("Add AU Feature Flag in On")
    public void addAUFeatureFlagInOn() {

    }

    @Then("show Add AU interstitial")
    public void showAddAUInterstitial() {
        stepDefinitionManager.getPageObjectManager().getAddAUPromptPage().VerifyAddAUInterstial();
    }

    @And("the user has logged into application with (.+) username and (.+) password")
    public void theUserHasLoggedIntoApplicationWithUser_nameUsernameAndPasswordPassword(String username,String password) {
        stepDefinitionManager.getPageObjectManager().loginToUserAccount(username, password);
    }

    @Then("while login will verify Add AU Prompt")
    public void whileLoginWillVerifyPrompt() {
    }
}
