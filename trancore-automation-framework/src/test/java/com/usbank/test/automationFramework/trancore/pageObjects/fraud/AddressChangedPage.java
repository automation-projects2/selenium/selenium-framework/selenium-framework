package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddressChangedPage<T extends WebDriver> {

    protected static final Logger logger = LogManager.getLogger(AddressChangedPage.class);

    protected T driver;


    public AddressChangedPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement btnContinue;

    @FindBy(xpath = "//*[text()='Check the card delivery address.']")
    private WebElement lblAddressChanged;

    @FindBy(id = "5019-Y")
    private WebElement rdbYesFirstQuestion;

    @FindBy(id = "5019-N")
    private WebElement rdbNoFirstQuestion;

    @FindBy(id = "5020-Y")
    private WebElement rdbYesSecondQuestion;

    @FindBy(id = "5020-N")
    private WebElement rdbNoSecondQuestion;

    public WebElement getLblAddressChanged() {
        return lblAddressChanged;
    }

    public void waitForAddressChangedPageToLoad() {

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(lblAddressChanged));
    }

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        ele.click();
    }

    public void clickFirstQuestionYesRadioButton() {
        waitForAddressChangedPageToLoad();
        clickElement(driver, rdbYesFirstQuestion);
    }

    public void clickSecondQuestionYesRadioButton() {
        waitForAddressChangedPageToLoad();
        clickElement(driver, rdbYesSecondQuestion);
    }

    public void clickContinue() {
        clickElement(driver, btnContinue);
    }
}
