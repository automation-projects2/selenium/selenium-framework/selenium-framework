package com.usbank.test.automationFramework.trancore.pageObjects.profile;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.logging.Logger;

public class ChangePersonalIDPage<T extends WebDriver> {

    @FindBy(how = How.XPATH, using = "//a[@title='Change Your Personal ID']")
    private WebElement lnkChangeYourPersonalID;

    @FindBy(how = How.ID, using = "challengeQuestion_lbl")
    private WebElement lblQuestion;

    @FindBy(how = How.ID, using = "answer")
    private WebElement txtAnswer;

    @FindBy(how = How.XPATH, using = "//button[@type='submit']")
    private WebElement btnSubmit;

    @FindBy(how = How.XPATH, using = "//input[@name='loginId1']")
    private WebElement txtNewPersonalID;

    @FindBy(how = How.XPATH, using = "//input[@name='loginId2']")
    private WebElement txtConfirmNewPersonalID;

    @FindBy(how = How.XPATH, using = "//input[@name='password']")
    private WebElement txtPassword;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Thank you.  Your request has been processed.')]")
    private WebElement vrbSuccessMessage;

    @FindBy(how = How.XPATH, using = "//a[@name='Return to Profile']")
    private WebElement btnReturnToProfile;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Profile')]")
    private WebElement profilePage;

    @FindBy(how = How.XPATH, using = "//td[contains(text(),'Please enter the New Personal ID.')]")
    private WebElement vrbPersonalIdBlank;

    @FindBy(how = How.XPATH, using = "//td[contains(text(),'The personal IDs you entered do not match.  Please')]")
    private WebElement vrbPersonalConfirmIdBlank;

    @FindBy(how = How.XPATH, using = "//td[contains(text(),'Please enter the current Password.')]")
    private WebElement vrbPasswordBlank;

    @FindBy(how = How.XPATH, using = "//td[contains(text(),\"We are unable to verify the information you've ent\")]")
    private WebElement vrbPasswordWrong;

    @FindBy(how = How.XPATH, using = "//td[contains(text(),'New Personal ID can not be less than 7 characters.')]")
    private WebElement vrbShortPersonalId;

    @FindBy(how = How.XPATH, using = "//td[contains(text(),\"We are unable to verify the information you've ent\")]")
    private WebElement vrbPersonalIdReUsed;

    @FindBy(how = How.XPATH, using = "//td[contains(text(),'Your Personal ID must contain only letters, number')]")
    private WebElement vrbNotAllowedSpecialCharacter;

    private T driver;

    private static Logger log = Logger.getLogger(ChangePasswordPage.class.getName());

    public ChangePersonalIDPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public void clickChangeYourPersonalIDlink() {

        lnkChangeYourPersonalID.click();
    }

    public WebElement getChangeYourPersonalIDlink() {
        return lnkChangeYourPersonalID;
    }

    public String getShieldQuestionAnswer() {

        String questionText = lblQuestion.getText();
        String lastWord = questionText.substring(questionText.lastIndexOf(" ") + 1, questionText.length() - 1);
        txtAnswer.sendKeys(lastWord);
        btnSubmit.click();
        return lastWord;

    }

    public void enterPersonalID(String newPersonalId) {

        txtNewPersonalID.sendKeys(newPersonalId);
    }

    public WebElement getNewPersonalID() {
        return txtNewPersonalID;
    }

    public void enterConfirmNewPersonalID(String ConfNewPersonalId) {
        txtConfirmNewPersonalID.sendKeys(ConfNewPersonalId);
    }

    public WebElement getConfirmNewPersonalID() {
        return txtConfirmNewPersonalID;
    }

    public void enterPassword(String password) {
        txtPassword.sendKeys(password);
    }

    public WebElement getPassword() {
        return txtPassword;
    }

    public void clickSubmitButton() {
        btnSubmit.click();
    }

    public WebElement getBtnSubmit() {
        return btnSubmit;
    }

    public void checkSuccessMessage() {
        WebDriverWait wait = new WebDriverWait(driver, 30);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Thank you.  Your request has been processed.')]")));
        String message = vrbSuccessMessage.getText();


        Assert.assertEquals("Your assert condition is failed", "Thank you.  Your request has been processed.", message);
        log.info("PASSED: Personal ID is changed successfully");
    }

    public void clickReturnToProfileLink() {
        btnReturnToProfile.click();
        String txtProfile = profilePage.getText();

        if (txtProfile.equalsIgnoreCase("Profile")) {
            log.info("Successfully returned to Profile page");
        } else {
            log.info("Returned to Profile page is not successful");
        }
    }

    public void checkErrorMessageForBlankNewPersonalId() {
        String blankNewPersonalId = vrbPersonalIdBlank.getText();
        Assert.assertEquals("Your assert condition is failed", "Please enter the New Personal ID.", blankNewPersonalId);
        log.info("PASSED: Please enter the current Password.");

    }

    public void checkErrorMessageForBlankConfirmNewPersonalId() {
        String blankConfNewPersonalId = vrbPersonalConfirmIdBlank.getText();
        Assert.assertEquals("Your assert condition is failed", "The personal IDs you entered do not match.  Please re-enter your personal IDs.", blankConfNewPersonalId);
        log.info("PASSED: The personal IDs you entered do not match.  Please re-enter your personal IDs.");

    }

    public void checkErrorMessageForBlankPassword() {
        String blankPassword = vrbPasswordBlank.getText();
        Assert.assertEquals("Your assert condition is failed", "Please enter the current Password.", blankPassword);
        log.info("PASSED: Please enter the current Password.");

    }

    public void checkErrorMessageForWrongPassword() {
        String wrongPassword = vrbPasswordWrong.getText();
        Assert.assertEquals("Your assert condition is failed", "We are unable to verify the information you've entered.  Please review your entry or call Technical Support at 1-877-334-0460 for assistance.", wrongPassword);
        log.info("PASSED: We are unable to verify the information you've entered.  Please review your entry or call Technical Support at 1-877-334-0460 for assistance.");

    }

    public void checkErrorMessageForShortPersonalId() {
        String shortPersonalId = vrbShortPersonalId.getText();
        Assert.assertEquals("Your assert condition is failed", "New Personal ID can not be less than 7 characters.", shortPersonalId);
        log.info("PASSED: New Personal ID can not be less than 7 characters.");

    }

    public void checkErrorMessageForReusedPersonalId() {
        String reUsedPersonalId = vrbPersonalIdReUsed.getText();
        Assert.assertEquals("Your assert condition is failed", "We are unable to verify the information you've entered.  Please review your entry or call Technical Support at 1-877-334-0460 for assistance.", reUsedPersonalId);
        log.info("PASSED: We are unable to verify the information you've entered.  Please review your entry or call Technical Support at 1-877-334-0460 for assistance.");

    }

    public void checkErrorMessageForNotAllowedSpecialCharacter() {
        String notAllowedSpecialCharacter = vrbNotAllowedSpecialCharacter.getText();
        Assert.assertEquals("Your assert condition is failed", "Your Personal ID must contain only letters, numbers and the following special characters @#$%^*_. .  Please re-enter your Personal ID.", notAllowedSpecialCharacter);
        log.info("PASSED: Your Personal ID must contain only letters, numbers and the following special characters @#$%^*_. .  Please re-enter your Personal ID.");

    }


}
