package com.usbank.test.automationFramework.trancore.pageObjects.services;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.logging.Logger;

public class ManageEmployeesPage<T extends WebDriver> {

    @FindBy(how = How.XPATH, using = "//button[@name='Add employee']")
    private WebElement btnAddEmployee;

    //This H1 heading is located in IT Env.
    /*@FindBy(how = How.XPATH, using = "//h1[@id='servicingAppHeader']")
    private WebElement lblAddEmployee;*/

    //This H1 heading is located in UAT Env.
    @FindBy(how = How.XPATH, using = "//h1[@class='svc-content-heading manage-ae-heading']")
    private WebElement lblAddEmployee;

    @FindBy(how = How.XPATH, using = "//p[contains(text(),'Complete this short form to add a new employee to')]")
    private WebElement vrbShortForm;

    @FindBy(how = How.XPATH, using = "//p[contains(text(),\"We're serious about your security so we use this i\")]")
    private WebElement vrbEmployeeIdentityInformation;

    @FindBy(how = How.XPATH, using = "//input[@name='firstName']")
    private WebElement txtFirstName;

    @FindBy(how = How.XPATH, using = "//input[@name='middleName']")
    private WebElement txtMiddleName;

    @FindBy(how = How.XPATH, using = "//input[@name='lastName']")
    private WebElement txtLastName;

    @FindBy(how = How.XPATH, using = "//select[@name='suffix']")
    private WebElement lstSuffix;

    @FindBy(how = How.XPATH, using = "//input[@name='securityNumber']")
    private WebElement txtSSN;

    @FindBy(how = How.XPATH, using = "//input[@id='dob']")
    private WebElement txtDateOfBirth;

    @FindBy(how = How.XPATH, using = "//input[@name='phoneNumber']")
    private WebElement txtPhoneNumber;

    @FindBy(how = How.XPATH, using = "//input[@name='spendingLimit']")
    private WebElement txtSpendingLimit;

    @FindBy(how = How.XPATH, using = "//button[@name='continue']")
    private WebElement btnContinue;

    @FindBy(how = How.XPATH, using = "//div[@class='manageAe-content-body']")
    private WebElement vrbReviewEmployeeInformation;

    @FindBy(how = How.XPATH, using = "//button[@name='continue']")
    private WebElement btnSubmit;

    /*
     * Code Refactoring was done to bring EmployeeDashBoardPage Obj. which has been deleted into ManageEmployeePage Obj.
     * file for a cleaner file structure on the CSS Selectors.
     * */

    @FindBy(how = How.ID, using = "servicingAppHeader")
    private WebElement employeeHeading;

    @FindBy(how = How.XPATH, using = "(//table[@class='manage-ae-table']//tr)[1]//td[5]//a[contains(text(),'Edit Spending Limit')]")
    private WebElement linkEditSpendingLimitForAO;

    @FindBy(how = How.XPATH, using = "(//table[@class='manage-ae-omv-table']//tr)[1]//td[5]//a[contains(text(),'Edit Spending Limit')]")
    private WebElement OmvlinkEditSpendingLimitForAO;

    @FindBy(how = How.XPATH, using = "//div[@class='manageemployee-form-desc']/p")
    private List<WebElement> displayEditSpendingLimitMessage;

    private T driver;

    private static Logger log = Logger.getLogger(ManageEmployeesPage.class.getName());

    public ManageEmployeesPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public void clickOnAddEmployeeButton() {
        btnAddEmployee.click();
    }

    public WebElement getBtnAddEmployee() {
        return btnAddEmployee;
    }

    public void addEmployeesInformation() {
        log.info(lblAddEmployee.getText());
        log.info(vrbShortForm.getText());
        log.info(vrbEmployeeIdentityInformation.getText());
    }

    public WebElement getLblAddEmployee() {
        return lblAddEmployee;
    }

    public void enterFirstMiddleLastNames(String firstName, String middleName, String lastName) {
        txtFirstName.sendKeys(firstName);
        txtMiddleName.sendKeys(middleName);
        txtLastName.sendKeys(lastName);
    }

    public WebElement getTxtFirstName() {
        return txtFirstName;
    }

    public WebElement getTxtMiddleName() {
        return txtMiddleName;
    }

    public WebElement getTxtLastName() {
        return txtLastName;
    }

    public void selectASuffix(String suffix) {
        lstSuffix.click();
        List<WebElement> listItems = driver.findElements(By.xpath("//select[@name='suffix']"));

        for (WebElement item : listItems) {
            log.info(item.getText());
            log.info("*************This Is The End Of The List*************");
            Select drpdwnPayee = new Select(driver.findElement(By.xpath("//select[@name='suffix']")));
            drpdwnPayee.selectByVisibleText(suffix);
        }
    }

    public WebElement getLstSuffix() {
        return lstSuffix;
    }

    public void enterSSN(String sSN) {
        txtSSN.sendKeys(sSN);
    }

    public WebElement getTxtSSN() {
        return txtSSN;
    }

    public void enterDateOfBirth(String dateOfBirth) {
        txtDateOfBirth.sendKeys(dateOfBirth);
    }

    public WebElement getTxtDateOfBirth() {
        return txtDateOfBirth;
    }

    public void enterPhoneNumber(String phoneNUmber) {
        txtPhoneNumber.sendKeys(phoneNUmber);
    }

    public WebElement getTxtPhoneNumber() {
        return txtPhoneNumber;
    }

    public void enterSpendingLimit(String spendingLimit) {
        txtSpendingLimit.sendKeys(spendingLimit);
    }

    public WebElement getTxtSpendingLimit() {
        return txtSpendingLimit;
    }

    public void clickContinueBtnOnAddEmployeePage() {
        btnContinue.click();
    }

    public WebElement getBtnContinue() {
        return btnContinue;
    }

    public void reviewAndSubmitEmployeeInformation() {
        String str = vrbReviewEmployeeInformation.getText();
        str.replace("SUBMITCANCEL", "");
        log.info(str.replace("SUBMITCANCEL", ""));
        btnSubmit.click();
    }

    public WebElement getVrbReviewEmployeeInformation() {
        return vrbReviewEmployeeInformation;
    }

    public WebElement getBtnSubmit() {
        return btnSubmit;
    }

    public void verifyManageEmployeeDashBoadDisplayed() {
        boolean value = employeeHeading.isDisplayed();
        if (value == true) {
            String employeeHeadingText = employeeHeading.getText();
            Assert.assertTrue("Mismatch in page header of Manage Employee", employeeHeadingText.contentEquals("Manage employees"));
            log.info("Manage employee page displayed successfully");
        }

    }

    public WebElement getEmployeeHeading() {
        return employeeHeading;
    }

    public void verifyEditSpendingLimitLink(int status) {

        switch (status) {
            case 0:
                try {
                    Assert.assertTrue("Failed:Edit spending limit link not displayed for AO with status 0",
                            linkEditSpendingLimitForAO.isDisplayed());
                } catch (Exception e) {
                    Assert.assertTrue("Failed:Edit spending limit link not displayed for AO with status 0",
                            OmvlinkEditSpendingLimitForAO.isDisplayed());
                }
                break;

            case 7:
                try {
                    Assert.assertTrue("Failed:Edit spending limit link not displayed for AO with status 7",
                            linkEditSpendingLimitForAO.isDisplayed());
                } catch (Exception e) {
                    Assert.assertTrue("Failed:Edit spending limit link not displayed for AO with status 7",
                            OmvlinkEditSpendingLimitForAO.isDisplayed());
                }
                break;

            case 1:
                try {
                    Assert.assertFalse("Failed:Edit spending limit link displayed for AO with status 1",
                            linkEditSpendingLimitForAO.isDisplayed());

                    for (int i = 1; i <= displayEditSpendingLimitMessage.size(); i++) {

                        if (i == 1) {
                            Assert.assertEquals("Spending limit adjustments aren't currently available.",
                                    displayEditSpendingLimitMessage.get(i).getText());
                        } else
                            Assert.assertEquals(
                                    "Please call us at 1-866-951-1390 if you need to adjust any spending limits.",
                                    displayEditSpendingLimitMessage.get(i).getText());
                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    try {
                        Assert.assertFalse("Failed:Edit spending limit link displayed for AO with status 1",
                                OmvlinkEditSpendingLimitForAO.isDisplayed());

                        for (int i = 1; i <= displayEditSpendingLimitMessage.size(); i++) {

                            if (i == 1) {
                                Assert.assertEquals("Spending limit adjustments aren't currently available.",
                                        displayEditSpendingLimitMessage.get(i).getText());
                            } else
                                Assert.assertEquals(
                                        "Please call us at 1-866-951-1390 if you need to adjust any spending limits.",
                                        displayEditSpendingLimitMessage.get(i).getText());
                        }
                    } catch (Exception e2) {
                        log.info("Passed:Edit Spending link not displayed for AO with status 1 successfully as expected ");
                    }
                    log.info("Passed:Edit Spending link not displayed for AO with status 1 successfully as expected ");

                }
                break;

            case 3:
                try {
                    Assert.assertFalse("Failed:Edit spending limit link displayed for AO with status 3",
                            linkEditSpendingLimitForAO.isDisplayed());
                    for (int i = 1; i <= displayEditSpendingLimitMessage.size(); i++) {

                        if (i == 1) {
                            Assert.assertEquals("Spending limit adjustments aren't currently available.",
                                    displayEditSpendingLimitMessage.get(i).getText());
                        } else
                            Assert.assertEquals(
                                    "Please call us at 1-866-951-1390 if you need to adjust any spending limits.",
                                    displayEditSpendingLimitMessage.get(i).getText());

                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    try {
                        Assert.assertFalse("Failed:Edit spending limit link displayed for AO with status 1",
                                OmvlinkEditSpendingLimitForAO.isDisplayed());

                        for (int i = 1; i <= displayEditSpendingLimitMessage.size(); i++) {

                            if (i == 1) {
                                Assert.assertEquals("Spending limit adjustments aren't currently available.",
                                        displayEditSpendingLimitMessage.get(i).getText());
                            } else
                                Assert.assertEquals(
                                        "Please call us at 1-866-951-1390 if you need to adjust any spending limits.",
                                        displayEditSpendingLimitMessage.get(i).getText());
                        }
                    } catch (Exception e2) {
                        // TODO: handle exception
                        log.info("Passed:Edit Spending link not displayed for AO with status 3 successfully as expected");
                    }
                    log.info("Passed:Edit Spending link not displayed for AO with status 3 successfully as expected");
                }
                break;

            case 5:
                try {
                    Assert.assertFalse("Failed:Edit spending limit link displayed for AO with status 5",
                            linkEditSpendingLimitForAO.isDisplayed());
                    for (int i = 1; i <= displayEditSpendingLimitMessage.size(); i++) {

                        if (i == 1) {
                            Assert.assertEquals("Spending limit adjustments aren't currently available.",
                                    displayEditSpendingLimitMessage.get(i).getText());
                        } else
                            Assert.assertEquals(
                                    "Please call us at 1-866-951-1390 if you need to adjust any spending limits.",
                                    displayEditSpendingLimitMessage.get(i).getText());

                    }

                } catch (Exception e) {
                    // TODO: handle exception
                    try {
                        Assert.assertFalse("Failed:Edit spending limit link displayed for AO with status 1",
                                OmvlinkEditSpendingLimitForAO.isDisplayed());

                        for (int i = 1; i <= displayEditSpendingLimitMessage.size(); i++) {

                            if (i == 1) {
                                Assert.assertEquals("Spending limit adjustments aren't currently available.",
                                        displayEditSpendingLimitMessage.get(i).getText());
                            } else
                                Assert.assertEquals(
                                        "Please call us at 1-866-951-1390 if you need to adjust any spending limits.",
                                        displayEditSpendingLimitMessage.get(i).getText());
                        }
                    } catch (Exception e2) {
                        // TODO: handle exception
                        log.info("Passed:Edit Spending link not displayed for AO with status 5 successfully as expected");
                    }
                    log.info("Passed:Edit Spending link not displayed for AO with status 5 successfully as expected");
                }
                break;

        }
    }

    public WebElement getLinkEditSpendingLimitForAO() {
        return linkEditSpendingLimitForAO;
    }

}
