package com.usbank.test.automationFramework.trancore.pageObjects.creditLineIncrease;

import com.usbank.test.automationFramework.trancore.pageObjects.services.AddAuthorizedUserPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

public class IncomeAndAssetsPage<T extends WebDriver> {
    private static final Logger logger = LogManager.getLogger(AddAuthorizedUserPage.class);

    private static final long EXPLICIT_WAIT_TIME_SECONDS = 60;

    private T driver;

    @FindBy(how = How.TAG_NAME, using = "h2")
    private WebElement headerName;

    @FindBy(how = How.TAG_NAME, using = "h1")
    private WebElement subHeader;

    @FindBy(how = How.NAME, using = "annualIncome")
    private WebElement textFieldAnnualIncome;

    @FindBy(how = How.NAME, using = "primaryIncomeSource")
    private  WebElement selectPrimaryIncomeSource;

    @FindBy(how = How.ID, using = "monetaryAssets")
    private WebElement textFieldMonetaryAssets;

    @FindBy(how = How.ID, using = "coOwnerMonetaryAssets")
    private WebElement textFieldJointMonetaryAssets;

    @FindBy(how = How.ID, using = "continueButton")
    private  WebElement buttonContinueToHousing;

    @FindBy(how = How.ID, using = "cancelButton")
    private WebElement buttonCancel;

    @FindBy(how = How.XPATH, using = "//input[@name='coOwnerAnnualIncome']")
    private WebElement textFieldJOwnerIncome;

    @FindBy(how = How.NAME, using = "jointIncomeSource")
    private WebElement selectJointIncomeSource;

    @FindBy(how = How.ID, using = "annualIncomeError")
    private WebElement errortxtAnnualIncome;

    @FindBy(how = How.ID, using = "svc-error")
    private WebElement errortxtAnnualIncomeSource;

    @FindBy(how = How.ID, using = "coOwnerAnnualIncomeError")
    private WebElement errortxtCoOwnerIncomeSource;


    public IncomeAndAssetsPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    public void verifyHeaderIncomeAndAssets(){
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(buttonContinueToHousing));
        assertEquals("Credit limit increase\n" +
                "Step 1 of 3", (headerName.getText()));
        assertEquals("income and assets",subHeader.getText().toLowerCase());
    }

    public void selectPrimaryIncomeSource(String IncomeSource){
        Assert.assertTrue("Primary Source of Income not displayed",selectPrimaryIncomeSource.isDisplayed());
        new Select(selectPrimaryIncomeSource).selectByVisibleText(IncomeSource);
    }

    public void enterYourMonetaryAssets(String MonetaryAssets){
        textFieldMonetaryAssets.sendKeys(MonetaryAssets);
    }

    public void enterYourJointMonetaryAssets(String JointMonetaryAssets) {
        textFieldMonetaryAssets.sendKeys(JointMonetaryAssets);
    }

    public void enterAnnualIncome(String Income){
        Assert.assertTrue("Annual Income not displayed",textFieldAnnualIncome.isDisplayed());
            textFieldAnnualIncome.click();
            textFieldAnnualIncome.sendKeys(Income);
        }


    public void clickContinueToHousing(){
        Assert.assertTrue("",buttonContinueToHousing.isDisplayed());
        buttonContinueToHousing.click();
    }
    public void clickCancel(){
        buttonCancel.click();
    }

    public void setJOwnerIncome(String JointOwnerIncomeSource){
        Assert.assertTrue("Co_Owner Annual income not displayed",textFieldJOwnerIncome.isDisplayed());
        textFieldJOwnerIncome.click();
        textFieldJOwnerIncome.sendKeys(JointOwnerIncomeSource);
    }

    public void SelectJointIncomeSource(String JointOwnerIncomeSource){
        Assert.assertTrue("Co_Owner income source is not displayed",selectJointIncomeSource.isDisplayed());
        new Select(selectJointIncomeSource).selectByVisibleText(JointOwnerIncomeSource);
    }



}
