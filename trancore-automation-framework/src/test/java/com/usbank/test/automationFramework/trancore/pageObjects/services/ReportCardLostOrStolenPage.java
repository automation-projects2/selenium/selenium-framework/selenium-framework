package com.usbank.test.automationFramework.trancore.pageObjects.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

public class ReportCardLostOrStolenPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(ReportCardLostOrStolenPage.class);

    private static final long EXPLICIT_WAIT_TIME_SECONDS = 30;

    private T driver;

    private WebDriverWait wait3;

    @FindBy(how = How.TAG_NAME, using = "h2")
    private WebElement headername;

    @FindBy(how = How.ID, using = "lostStolenContinue")
    private WebElement linkContinue;

    @FindBy(name = "continue")
    private WebElement buttonContinue;


    @FindBy(how = How.LINK_TEXT, using = "Cancel")
    private WebElement linkCancel;

    public ReportCardLostOrStolenPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }


    public void verifyHeaderName() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(linkContinue));
        assertEquals("services", (headername.getText()).toLowerCase());

    }

    public void ClickContinue() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        linkContinue.click();
    }
    public void ClickContinueButton() {
        //JavascriptExecutor executor = (JavascriptExecutor)driver;
        //executor.executeScript("arguments[0].click();", buttonContinue);
         linkContinue.click();
          }

    public void ClickCancel() {
        linkCancel.click();
    }
}

