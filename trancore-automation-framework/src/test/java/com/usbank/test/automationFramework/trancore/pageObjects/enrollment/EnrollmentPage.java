package com.usbank.test.automationFramework.trancore.pageObjects.enrollment;

import com.usbank.test.objects.trancore.TrancoreAccount;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EnrollmentPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(EnrollmentPage.class);

    private T driver;

    public EnrollmentPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    @FindAll({
            @FindBy(how = How.NAME, using = "cardNumber"),
            @FindBy(id = "cardNumber")
    })
    private WebElement txtAccountNumber;

    @FindAll({
            @FindBy(how = How.NAME, using = "signaturePanelCode"),
            @FindBy(id = "signaturePanelCode-tel-input")
    })
    private WebElement txtCVV2;

    @FindAll({
            @FindBy(how = How.NAME, using = "ssn"),
            @FindBy(id = "ssn-tel-input")
    })
    private WebElement txtSSN;


    @FindBy(how = How.ID, using = "zipCodeRadio")
    private WebElement rdoZipCode;


   @FindBy(how = How.NAME, using = "zipCode")
    private WebElement txtZipCode;

    @FindAll({
            @FindBy(how = How.NAME, using = "loginId1"),
            @FindBy(id = "loginId1")
    })
    private WebElement txtPersonalId;

    @FindAll({
            @FindBy(how = How.NAME, using = "loginId2"),
            @FindBy(id = "loginId2")
    })
    private WebElement txtPersonalId2;

    @FindAll({
            @FindBy(how = How.NAME, using = "password1"),
            @FindBy(id = "password1")
    })
    private WebElement txtPassword;

    @FindAll({
            @FindBy(how = How.NAME, using = "password2"),
            @FindBy(id = "password2")
    })
    private WebElement txtPassword2;

    @FindAll({
            @FindBy(how = How.NAME, using = "emailAddress1"),
            @FindBy(id = "emailAddress1")
    })
    private WebElement txtEmail;

    @FindAll({
            @FindBy(how = How.NAME, using = "emailAddress2"),
            @FindBy(id = "emailAddress2")
    })
    private WebElement txtEmail2;

    /*
    @FindBy(name = "Cancel")
    private WebElement btnCancel;
    */

    @FindBy(how = How.XPATH, using = "//span[(@role ='link') and (text() ='Cancel')]")
    private WebElement btnCancel;

    @FindAll({
            @FindBy(how = How.NAME, using = "Submit"),
            @FindBy(id = "enrollFormSubmit")
    })
    private WebElement btnSubmit;

    @FindBy(xpath = "//*[text()[contains(.,'Error')]]")
    private WebElement errorCode;

    public void navigateToEnrollment(String baseUrl) {
        driver.get(baseUrl);
    }

    public void populateEnrollForm(TrancoreAccount account) {

        waitForPageLoad();
        txtAccountNumber.sendKeys(account.getAccountNumber());
        txtCVV2.sendKeys(account.getSecurityCode());
        txtSSN.sendKeys(account.getSsn());
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(rdoZipCode));
        rdoZipCode.click();
        txtZipCode.sendKeys(account.getZipCode());
        txtPersonalId.sendKeys(account.getPersonalId());
        txtPersonalId2.sendKeys(account.getPersonalId());
        txtPassword.sendKeys(account.getPassword());
        txtPassword2.sendKeys(account.getPassword());

    }

    public void submitEnrollmentForm() {
        btnSubmit.click();
    }

    public WebElement getErrorCode() {
        return errorCode;
    }

    public void waitForPageLoad() {

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(txtAccountNumber));

    }

}
