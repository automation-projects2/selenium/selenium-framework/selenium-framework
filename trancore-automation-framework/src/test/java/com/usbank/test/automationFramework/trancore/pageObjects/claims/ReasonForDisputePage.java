package com.usbank.test.automationFramework.trancore.pageObjects.claims;

import com.usbank.test.automationFramework.trancore.pageObjects.waitForPageToLoad.WaitForPageToLoad;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class ReasonForDisputePage<T extends WebDriver> {
    @FindBy(how = How.XPATH, using = "//h1[@id='servicingAppHeader']")
    private WebElement lblReasonForDisputeHeader;

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Continue')]")
    private WebElement btnContinue;

    private T driver;
    @FindAll
    ({@FindBy(how = How.ID, using="PQSCR"), @FindBy(how = How.ID, using="PQMCR")})
    private WebElement rdbCancelledServiceOrMerchandise;

    @FindBy(how = How.ID, using = "PE#PBOM")
    private WebElement rdbPaidMerchandiseByOtherMeans;


    WaitForPageToLoad callPageLoadMethod;

    private static final Logger log = LogManager.getLogger(ReasonForDisputePage.class.getName());

    public ReasonForDisputePage(T inDriver) {
        driver = inDriver;
        callPageLoadMethod = new WaitForPageToLoad(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getLblReasonForDisputeHeader() {
        return lblReasonForDisputeHeader;
    }

    public void selectAReasonForDispute() {
        String reasonForDispute = "I paid for this service with another form of payment.";

        List<WebElement> listOfReasons = driver.findElements(By.name("radionatureOfDispute"));
        List<WebElement> listOfText = driver.findElements(By.className("svc-radioButton"));

        int listSize = listOfReasons.size();

        for (int i = 0; i < listSize; i++) {

            String reasonText = listOfText.get(i).getText();

            log.info("Dispute Reasoning " + i + ": " + reasonText);
            if (reasonText.equalsIgnoreCase(reasonForDispute)) {
                listOfReasons.get(i).click();
            }
        }

    }

    public void selectAReasonForDisputeTwo() {
        String reasonForDispute = "I was charged the wrong amount.";

        List<WebElement> listOfReasons = driver.findElements(By.name("radionatureOfDispute"));
        List<WebElement> listOfText = driver.findElements(By.className("svc-radioButton"));

        int listSize = listOfReasons.size();

        for (int i = 0; i < listSize; i++) {

            String reasonText = listOfText.get(i).getText();

            log.info("Dispute Reasoning " + i + ": " + reasonText);
            if (reasonText.equalsIgnoreCase(reasonForDispute)) {
                listOfReasons.get(i).click();
            }
        }

    }

    public void selectAReasonForDisputeThree() {
        String reasonForDispute = "I was charged more than once on this card for this merchandise.";

        List<WebElement> listOfReasons = driver.findElements(By.name("radionatureOfDispute"));
        List<WebElement> listOfText = driver.findElements(By.className("svc-radioButton"));

        int listSize = listOfReasons.size();

        for (int i = 0; i < listSize; i++) {

            String reasonText = listOfText.get(i).getText();

            log.info("Dispute Reasoning " + i + ": " + reasonText);
            if (reasonText.equalsIgnoreCase(reasonForDispute)) {
                listOfReasons.get(i).click();
            }
        }

    }

    public void clickContinueBtnOnReasonForDisputePage() {
        btnContinue.click();
    }

    public void reasonForDisputePagePBOMFlow() throws InterruptedException {
        log.info("**************** Reason For Dispute Page Reasoning One ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblReasonForDisputeHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblReasonForDisputeHeader, 5);
        selectAReasonForDispute();
        clickContinueBtnOnReasonForDisputePage();
    }

   public void reasonForDisputeWrongAmountFlow() throws InterruptedException
   {log.info("**************** Reason For Dispute Page Reasoning Two ****************");
       callPageLoadMethod.waitExplicitlyForPageToLoad(lblReasonForDisputeHeader);
       callPageLoadMethod.waitImplicitlyForPageToLoad(lblReasonForDisputeHeader, 5);
       selectAReasonForDisputeTwo();
       clickContinueBtnOnReasonForDisputePage();

   }

    public void reasonForDisputeDuplicateTransactionFlow() throws InterruptedException {
        log.info("**************** Reason For Dispute Page Reasoning Three ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblReasonForDisputeHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblReasonForDisputeHeader, 5);
        selectAReasonForDisputeThree();
        clickContinueBtnOnReasonForDisputePage();
    }

    public void waitForPageLoad() {

        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOf(rdbCancelledServiceOrMerchandise));
    }

    public WebElement getRdbPaidMerchandiseByOtherMeans() {
        return rdbPaidMerchandiseByOtherMeans;
    }
}
