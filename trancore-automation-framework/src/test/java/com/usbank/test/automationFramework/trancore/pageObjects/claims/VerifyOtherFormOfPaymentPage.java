package com.usbank.test.automationFramework.trancore.pageObjects.claims;

import com.usbank.test.automationFramework.trancore.pageObjects.waitForPageToLoad.WaitForPageToLoad;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class VerifyOtherFormOfPaymentPage<T extends WebDriver> {
    @FindBy(how = How.XPATH, using = "//h1[@id='servicingAppHeader']")
    private WebElement lblVerifyOtherFormOfPaymentHeader;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Is the same merchant listed on the account summary')]")
    private WebElement vrbIsTheSameMerchantListedOnTheAccount;

    @FindBy(how = How.XPATH, using = "//input[@id='33-Y']")
    private WebElement rbYes;

    @FindBy(how = How.XPATH, using = "//input[@id='33-N']")
    private WebElement rbNo;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'How is the merchant listed on your receipt for you')]")
    private WebElement vrbHowIsTheMerchantListed;

    @FindBy(how = How.XPATH, using = "//textarea[@id='101']")
    private WebElement txtHowIsTheMerchantListed;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Transaction Amount')]")
    private WebElement vrbTransactionAmount;

    @FindBy(how = How.XPATH, using = "//input[@id='100']")
    private WebElement txtTransactionAmount;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Transaction Date')]")
    private WebElement vrbTransactionDate;

    @FindBy(how = How.XPATH, using = "//input[@id='99']")
    private WebElement txtTransactionDate;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Visa card number (last 4 digits)')]")
    private WebElement vrbVisaCardNumber;

    @FindBy(how = How.XPATH, using = "//input[@id='103']")
    private WebElement txtVisaCardNumber;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Bank or credit card company that issued the VISA c')]")
    private WebElement vrbBankOrCreditCardComapnyThatIssued;

    @FindBy(how = How.XPATH, using = "//textarea[@id='512']")
    private WebElement txtBankOrCreditCardComapnyThatIssued;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Reference number')]")
    private WebElement vrbReferenceNumber;

    @FindBy(how = How.XPATH, using = "//input[@id='102']")
    private WebElement txtReferenceNumber;

    @FindBy(how = How.XPATH, using = "//label[contains(text(),\"I don't have all of this information at this time\")]")
    private WebElement vrbIDontHaveAllOfThisInfo;

    @FindBy(how = How.XPATH, using = "//input[@id='513']")
    private WebElement ckbIDontHaveAllOfThisInfo;

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Continue')]")
    private WebElement btnContinueBtnOnVerifyOtherFormOfPaymentPage;

    private T driver;

    WaitForPageToLoad callPageLoadMethod;

    private static final Logger log = LogManager.getLogger(VerifyOtherFormOfPaymentPage.class.getName());

    public VerifyOtherFormOfPaymentPage(T inDriver) {
        driver = inDriver;
        callPageLoadMethod = new WaitForPageToLoad(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement lblVerifyOtherFormOfPaymentHeader() {
        return lblVerifyOtherFormOfPaymentHeader;
    }

    public void isTheSameMerchantListedOnTheAccountText() {
        log.info(vrbIsTheSameMerchantListedOnTheAccount.getText());
    }

    public void clickOnYes() {
        rbYes.click();
    }

    public void clickOnNo() {
        rbNo.click();
    }

    public void howIsTheMerchantListedText() {
        log.info(vrbHowIsTheMerchantListed.getText());
    }

    public void enterInHowIsTheMerchantListed() {
        txtHowIsTheMerchantListed.sendKeys("asdf");
    }

    public void transactionAmountText() {
        log.info(vrbTransactionAmount.getText());
    }

    public void enterInTransactionAmount() {
        txtTransactionAmount.sendKeys("1");
    }

    public void transactionDateText() {
        log.info(vrbTransactionDate.getText());
    }

    /*TODO:
     * I need to find a way to get a dynamic date
     */
    public void enterInTransactionDate() {
        txtTransactionDate.sendKeys("11112020");
    }

    public void visaCardNumberText() {
        log.info(vrbVisaCardNumber.getText());
    }

    public void enterInVisaCardNumber() {
        txtVisaCardNumber.sendKeys("1234");
    }

    public void bankOrCredtCardCompanyIssuedVisaCardText() {
        log.info(vrbBankOrCreditCardComapnyThatIssued.getText());
    }

    public void enterInBankOrCredtCardCompanyIssuedVisaCard() {
        txtBankOrCreditCardComapnyThatIssued.sendKeys("asdf");
    }

    public void referenceNumberText() {
        log.info(vrbReferenceNumber.getText());
    }

    public void enterInReferenceNumber() {
        txtReferenceNumber.sendKeys("123456789009876543211234");
    }

    public void iDontHaveAllOfThisInfoText() {
        log.info(vrbIDontHaveAllOfThisInfo.getText());
    }

    public void clickOnIDontHaveAllOfThisInfoCkb() {
        ckbIDontHaveAllOfThisInfo.click();
    }

    public void clickOnContinueBtnOnVerifyOtherFormOfPaymentPage() {
        btnContinueBtnOnVerifyOtherFormOfPaymentPage.click();
    }

    public void verifyOtherFormOfPaymentPBOMFlowOne() throws InterruptedException {
        log.info("**************** Verify Other Form Of Payment Page Reasoning One ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblVerifyOtherFormOfPaymentHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblVerifyOtherFormOfPaymentHeader, 5);
        isTheSameMerchantListedOnTheAccountText();
        clickOnYes();
        transactionAmountText();
        enterInTransactionAmount();
        transactionDateText();
        enterInTransactionDate();
        visaCardNumberText();
        enterInVisaCardNumber();
        bankOrCredtCardCompanyIssuedVisaCardText();
        enterInBankOrCredtCardCompanyIssuedVisaCard();
        referenceNumberText();
        enterInReferenceNumber();
        clickOnContinueBtnOnVerifyOtherFormOfPaymentPage();
    }

    public void verifyOtherFormOfPaymentPBOMFlowTwo() throws InterruptedException {
        log.info("**************** Verify Other Form Of Payment Page Reasoning Two ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblVerifyOtherFormOfPaymentHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblVerifyOtherFormOfPaymentHeader, 5);
        isTheSameMerchantListedOnTheAccountText();
        clickOnNo();
        howIsTheMerchantListedText();
        enterInHowIsTheMerchantListed();
        transactionAmountText();
        enterInTransactionAmount();
        transactionDateText();
        enterInTransactionDate();
        visaCardNumberText();
        enterInVisaCardNumber();
        bankOrCredtCardCompanyIssuedVisaCardText();
        enterInBankOrCredtCardCompanyIssuedVisaCard();
        referenceNumberText();
        enterInReferenceNumber();
        clickOnContinueBtnOnVerifyOtherFormOfPaymentPage();
    }

    public void verifyOtherFormOfPaymentPBOMFlowThree() throws InterruptedException {
        log.info("**************** Verify Other Form Of Payment Page Reasoning Three ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblVerifyOtherFormOfPaymentHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblVerifyOtherFormOfPaymentHeader, 5);
        isTheSameMerchantListedOnTheAccountText();
        clickOnYes();
        iDontHaveAllOfThisInfoText();
        clickOnIDontHaveAllOfThisInfoCkb();
        clickOnContinueBtnOnVerifyOtherFormOfPaymentPage();
    }

}
