package com.usbank.test.automationFramework.trancore.runner;

import com.github.peterwippermann.junit4.parameterizedsuite.ParameterizedSuite;
import com.usbank.test.automationFramework.trancore.regression.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runners.Suite.SuiteClasses;


//https://dzone.com/articles/introducing-a-parameterized-test-suite-for-junit-4
//https://github.com/PeterWippermann/parameterized-suite
@RunWith(ParameterizedSuite.class)
//@SuiteClasses({RegressionSetupTests.class, BalanceTransferTests.class, DigitalWalletTests.class, ManagePaymentsRegressionTests.class, AlertsRegressionTests.class, EnrollInClickToPayTests.class, PayPalTests.class, ConvenienceCheckRequestTests.class, LockAndUnlockTests.class, ClaimsTests.class, ShortTermPlanTests.class, DiscontinuePaperStatementsStep1RegressionTests.class})
@SuiteClasses({RegressionSetupTests.class, CLIJointOwnerTests.class})
public class RegressionTestSuite {
    @Parameters(name = "Regression Tests: {0}, {1}")
    public static Object[] params() {
        return new Object[][]{{TrancorePartner.ELAN, RegressionProfile.ELAN_CONSUMER}};
    }

    /**
     * Always provide a target for the defined parameters - even if you only want to access them in the suite's child classes.
     * <p>
     * TrancorePartner is only used for WEB Currently
     */
    @Parameter(0)
    public TrancorePartner partner;

    @Parameter(1)
    public RegressionProfile regressionProfile;
}
