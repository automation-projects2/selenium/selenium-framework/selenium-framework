package com.usbank.test.automationFramework.trancore.pageObjects.payments;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ShortTermPlanDetailsPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(ShortTermPlanDetailsPage.class);
    private T driver;
    private int EXPLICIT_WAIT_TIME_SECONDS = 30;

    @FindBy(how = How.ID, using = "shortTermPaymentHeader")
    private WebElement shortTermPlanDetailsPageHeading;

    @FindBy(how=How.XPATH, using = "//*[@id='creditReportingParagraph']/p")
    private WebElement creditReportingParagraph;

    @FindBy(how = How.ID, using = "creditReportingHeader")
    private WebElement creditReportingHeading;

    @FindBy(how = How.ID, using = "termsOfShortTermPlanHeader")
    private WebElement shortTermsPlanTermsHeading;

    @FindBy(how = How.ID, using = "agree-terms-button")
    private WebElement agreeAndContinueButton;

    @FindBy(how = How.ID, using = "cancelLink")
    private WebElement cancelLink;

    @FindBy(how = How.ID, using = "cancelModalHeader")
    private WebElement cancelModalHeading;

    @FindBy(how = How.XPATH, using = "//*[@id='cancelModalParagraph']/p")
    private WebElement cancelModalParagraph;

    @FindBy(how=How.ID, using="termsOfShortTermPlanList")
    private WebElement shortTermPlanTermsList;

    @FindBy(how=How.ID, using="yes-leave-button")
    private WebElement cancelModalLeaveBtn;


    public ShortTermPlanDetailsPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }


    public boolean isShortTermPlanDetailsPageDisplayed() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(shortTermPlanDetailsPageHeading));
        String headingText = shortTermPlanDetailsPageHeading.getText();
        if (headingText.equals("Short-term payment plan"))
            return true;

        return false;
    }

    public WebElement getShortTermPlanDetailsPageHeading() {
        return shortTermPlanDetailsPageHeading;
    }

    public WebElement getCreditReportingHeading() {
        return creditReportingHeading;
    }

    public WebElement getShortTermsPlanTermsHeading() {
        return shortTermsPlanTermsHeading;
    }

    public WebElement getAgreeAndContinueButton() {
        return agreeAndContinueButton;
    }

    public int getExplicitWaitTimeSeconds() {
        return EXPLICIT_WAIT_TIME_SECONDS;
    }

    public void clickCancelLink() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(cancelLink));
        cancelLink.click();
    }

    public WebElement getCancelModalHeading() {
        return cancelModalHeading;
    }

    public boolean isCancelModalDisplayed() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(cancelModalHeading));

        String headingText = cancelModalHeading.getText();
        if (headingText.equals("What happens when you cancel?"))
            return true;

        return false;
    }

    public WebElement getCancelModalParagraph() {
        return cancelModalParagraph;
    }

    public WebElement getCreditReportingParagraph() {
        return creditReportingParagraph;
    }

    public WebElement getShortTermPlanTermsList() {
        return shortTermPlanTermsList;
    }

    public boolean isAgreeAndContinueButtonPresent() {
        try {
            driver.findElement(By.id("agree-terms-button"));
            return true;
        } catch (org.openqa.selenium.NoSuchElementException e) {
            return false;
        }
    }

    public WebElement getCancelModalLeaveBtn() {
        return cancelModalLeaveBtn;
    }
}
