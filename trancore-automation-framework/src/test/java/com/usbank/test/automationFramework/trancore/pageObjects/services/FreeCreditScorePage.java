package com.usbank.test.automationFramework.trancore.pageObjects.services;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class FreeCreditScorePage<T extends WebDriver> {

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Free credit score')]")
    private WebElement lnkFreeCreditScore;

    @FindBy(how = How.XPATH, using = "//a[@id='extSiteLink']")
    private WebElement btnContinue;

    @FindBy(how = How.XPATH, using = "//input[@name='Cancel']")
    private WebElement btnCancel;

    private T driver;

    private static Logger log = Logger.getLogger(FreeCreditScorePage.class.getName());

    public FreeCreditScorePage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public void clickContinueBtn() throws InterruptedException {
        /*
         * DO NOT DELETE the THREAD.SLEEP statements
         * Thread.Sleep's are needed because the implicit/explicit waits are working on/off to find the Web
         */
        Thread.sleep(5000);
        WebDriverWait wait = new WebDriverWait(driver, 30);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        String currentWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        Iterator<String> i = allWindows.iterator();

        while (i.hasNext()) {
            String childwindow = i.next();
            if (!childwindow.equalsIgnoreCase(currentWindow)) {
                driver.switchTo().window(childwindow);
                driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
                log.info("Get Current URL:" + driver.getCurrentUrl());
                log.info("Get Title  of all windows" + driver.getTitle());
                wait.until(ExpectedConditions.elementToBeClickable(btnContinue)).click();
                Thread.sleep(8000);
                driver.switchTo().window(currentWindow);
            } else {
                driver.switchTo().window(currentWindow);
            }
        }

    }

    public WebElement getBtnContinue() {
        return btnContinue;
    }


    public void clickCancelBtn() throws InterruptedException {
        /*
         * DO NOT DELETE the THREAD.SLEEP statements
         * Thread.Sleep's are needed because the implicit/explicit waits are working on/off to find the Web
         */
        Thread.sleep(5000);
        WebDriverWait wait = new WebDriverWait(driver, 30);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        String currentWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        Iterator<String> i = allWindows.iterator();

        while (i.hasNext()) {
            String childwindow = i.next();
            if (!childwindow.equalsIgnoreCase(currentWindow)) {
                driver.switchTo().window(childwindow);
                driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
                log.info("Get Current URL:" + driver.getCurrentUrl());
                log.info("Get Title  of all windows" + driver.getTitle());
                wait.until(ExpectedConditions.elementToBeClickable(btnCancel)).click();
                driver.switchTo().window(currentWindow);
            } else {
                driver.switchTo().window(currentWindow);
            }
        }


    }

    public WebElement getBtnCancel() {
        return btnCancel;
    }

}
