package com.usbank.test.automationFramework.trancore.stepDefinitions.otp;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class ServiceOTPStepDef {
	@Autowired
	StepDefinitionManager stepDefinitionManager;


	@When("customer click on Service Tab")
	public void customer_click_on_Service_Tab() {
		// Write code here that turns the phrase above into concrete actions
		stepDefinitionManager.getPageObjectManager().getWebNavigationPage().clickServicesLink();
	}

	@When("the customer clicks Report Card Lost or Stolen link in service Tab")
	public void the_customer_clicks_Report_Card_Lost_or_Stolen_link_in_service_Tab() {
		// Write code here that turns the phrase above into concrete actions
		stepDefinitionManager.getPageObjectManager().getServicesLandingPage().clickLostStolenCard();
	}

	@When("the customer clicks on Continue in Report Card Lost or Stolen Page")
	public void the_customer_clicks_on_Continue_in_Report_Card_Lost_or_Stolen_Page() {
		// Write code here that turns the phrase above into concrete actions
		stepDefinitionManager.getPageObjectManager().getReportCardLostOrStolenPage().ClickContinueButton();
	}

	@Then("the customer should be directed to the Recent Review Transaction")
	public void the_customer_should_be_directed_to_the_Recent_Review_Transaction() {
		// Write code here that turns the phrase above into concrete actions
		stepDefinitionManager.getPageObjectManager().getReviewRecentTransactionPage().verifyHeaderName();
	}
	@When("the customer should return to Report Card Lost or Stolen Page")
	public void the_customer_should_return_to_Report_Card_Lost_or_Stolen_Page() {
		// Write code here that turns the phrase above into concrete actions
		stepDefinitionManager.getPageObjectManager().getReportCardLostOrStolenPage().verifyHeaderName();
	}

	@When("the customer should return to Service Tab")
	public void the_customer_should_return_to_Service_Tab() {
		// Write code here that turns the phrase above into concrete actions
		stepDefinitionManager.getPageObjectManager().getServicesLandingPage().verifyHeadername();
	}
	 @Then("the customer should be directed to Add Authorised User page")
	    public void the_customer_should_be_directed_to_Add_Authorised_User_Page() {
	        // Write code here that turns the phrase above into concrete actions
	        stepDefinitionManager.getPageObjectManager().getAddAuthorizedUserPage().verifyHeaderName();
	    }

	 @Given("the customer clicks on Add authorized user at Service Tab")
	    public void the_customer_clicks_on_Add_authorized_user_at_Service_Tab() {
	        // Write code here that turns the phrase above into concrete actions
	        stepDefinitionManager.getPageObjectManager().getServicesLandingPage().clickAddAuthorizedUser();
	    }


    @And("the customer clicks on Cancel at Recent Review Transaction Page")
    public void theCustomerClicksOnCancelAtRecentReviewTransactionPage() {
		stepDefinitionManager.getPageObjectManager().getReviewRecentTransactionPage().ClickCancel();
    }


	@Then("the customer should be directed to the Recent Review Transaction Lost Stolen")
	public void theCustomerShouldBeDirectedToTheRecentReviewTransactionLostStolen() {
		stepDefinitionManager.getPageObjectManager().getReviewRecentTransactionPage().verifyHeaderNameLS();
	}
}
