package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WrapUpFraudPage<T extends WebDriver> {
    protected static final Logger logger = LogManager.getLogger(WrapUpFraudPage.class);

    protected T driver;

    @FindBy(xpath = "//h1[text()='Contact us for help']")
    private WebElement lblContactUs;

    @FindBy(xpath = "//a[text()='Return to transaction details']")
    private WebElement returnTransLink;

    public WebElement getReturnTransLink() {
        return returnTransLink;
    }


    public WebElement getLblContactUs() {
        return lblContactUs;
    }

    public void waitForWrapUpFraudPageToLoad() {

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(lblContactUs));
        wait.until(ExpectedConditions.elementToBeClickable(getReturnTransLink()));
    }

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        ele.click();
    }

    public WrapUpFraudPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public void clickReturnToOriginationLink() {
        waitForWrapUpFraudPageToLoad();
        clickElement(driver, returnTransLink);

    }
}
