package com.usbank.test.automationFramework.trancore.pageObjects.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ServicesPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(ServicesPage.class);

    private T driver;

    @FindBy(xpath = "//h1[text()='Services']")
    private WebElement headingServices;

    @FindBy(linkText = "Enroll in Click to pay with card")
    private WebElement lnkEnrollInClickToPayWithCard;

    @FindBy(linkText = "Setup or manage my digital wallet")
    private WebElement linkSetupOrManageMyDigitalWallet;

    @FindBy(linkText = "Lock or unlock card")
    private WebElement linkLockOrUnlockCard;

    @FindBy(linkText = "Travel notification")
    private WebElement linkTravelNotification;

    @FindBy(linkText = "Report card lost or stolen")
    private WebElement linkReportCardLostOrStolen;

    @FindBy(how = How.LINK_TEXT, using = "Add authorized user")
    private WebElement lnkAddAuthorizedUser;

    @FindBy(how = How.LINK_TEXT, using = "Add joint owner (pdf)")
    private WebElement lnkAddJointOwnerPDF;

    @FindBy(how = How.LINK_TEXT, using = "Request convenience checks")
    private WebElement lnkRequestConenvienceChecks;

    @FindBy(how = How.LINK_TEXT, using = "Spend analysis")
    private WebElement lnkSpendAnalysis;

    @FindBy(how = How.LINK_TEXT, using = "Manage employees")
    private WebElement lnkManageEmployee;

    @FindBy(how = How.LINK_TEXT, using = "Add authorized representative (pdf)")
    private WebElement lnkAddAuthorizedRepPDF;

    @FindBy(how = How.LINK_TEXT, using = "Free credit score")
    private WebElement lnkFreeCreditScore;

    @FindBy(how = How.LINK_TEXT, using = "Download annual account summary")
    private WebElement lnkDownloadAnnualAccountSummary;

    @FindBy(how = How.LINK_TEXT, using = "Balance transfer")
    private WebElement lnkBalanceTransfer;

    public static int EXPLICIT_WAIT_TIME_SECONDS = 30;


    public ServicesPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    public void clickLinkEnrollInClickToPayWithCard() {
        Assert.assertTrue(lnkEnrollInClickToPayWithCard.isDisplayed());
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkEnrollInClickToPayWithCard));
        lnkEnrollInClickToPayWithCard.click();
    }

    public void clickLinkSetupOrManageMyDigitalWallet() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(linkSetupOrManageMyDigitalWallet));
        linkSetupOrManageMyDigitalWallet.click();
    }

    public void clickLinkLockOrUnlockCard() {
        linkLockOrUnlockCard.click();
    }

    public void clickLinkTravelNotification() {
        linkTravelNotification.click();
    }

    public void clickLinkReportCardLostOrStolen() {
        linkReportCardLostOrStolen.click();
    }

    public void waitForPageLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.elementToBeClickable(headingServices));
    }

    public void clickLinkAddAuthorizedUser() {
        lnkAddAuthorizedUser.click();
    }

    public void clickLinkAddJointOwnerPDF() {
        lnkAddJointOwnerPDF.click();
    }

    public void clickLinkRequestConvenienceChecks() {
        boolean lnkConenvienceChecksPresent;
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkRequestConenvienceChecks));

        if (lnkRequestConenvienceChecks.isDisplayed()) {
            lnkConenvienceChecksPresent = true;
            Assert.assertTrue("Convenience check link is present and displayed successfully", lnkConenvienceChecksPresent);
            logger.info("PASSED: Convenience check link is present and displayed successfully ");

            lnkRequestConenvienceChecks.click();

        } else {
            lnkConenvienceChecksPresent = false;
            Assert.assertFalse("Convenience check link is not present ", lnkConenvienceChecksPresent);
            logger.error("Failed: Convenience check link is not present ");
        }

    }

    public void clickLinkSpendAnalysis() throws InterruptedException {
       new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.elementToBeClickable(lnkSpendAnalysis));
        lnkSpendAnalysis.click();
    }
    public void clickLinkManageEmployee() {
        lnkManageEmployee.click();
    }

    public void clickLinkAddAuthorizedRepresentativePDF() {
        lnkAddAuthorizedRepPDF.click();
    }

    public void clickLinkFreeCreditScore() {
        lnkFreeCreditScore.click();
    }

    public void clickLinkBalanceTransfer() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkBalanceTransfer));
        lnkBalanceTransfer.click();
    }

}
