package com.usbank.test.automationFramework.trancore.stepDefinitions.payments;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

public class PaymentErrorScenariosSteps {

    @Autowired
    StepDefinitionManager stepDefinitionManager;
    WebDriver driver;

    @Then("user enters {string} Amount and {string} paymentDueDate")
    public void user_enters_Amount_and_paymentDueDate(String amount, String date) {
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().makePaymentErrorPaymentDate(amount, date);
    }


    @Then("user enters  {string} paymentDueDate")
    public void userEnterspaymentDueDate(String date) {
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().errorMessageOtherAmountField(date);
    }

    @Then("user enters {string} amount")
    public void userEntersAmount(String amount) {
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().errorMessagePaymentDateField(amount);
    }


    @Then("the error message is displayed for otherAmount and paymentfield if the fields are empty")
    public void theErrorMessageIsDisplayedForotherAmountAndPaymentfieldIfTheFieldsAreEmpty() {
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().makePaymentError();
    }


    @Then("user edits {string} Amount and {string} Date of Payment")
    public void userEditsAmountAndDateOfPayment(String amount, String date) {
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().errorEditDateAndAmountOfScheduledPay(amount, date);
    }


    @Then("verify the last statement balance changed as remaning statement balance")
    public void verifyTheLastStatementBalanceChangedAsRemaningStatementBalance() {
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().remaningStaetmentBalanceMakepayment();
    }


    @Then("user is able to edits  {string} Date of Payment")
    public void userIsAbleToEditsDateOfPayment(String date) {
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().errorOnEditpageAmountFieldisEmpty(date);
    }

    @Then("user edits  {string} Date of Payment")
    public void userEditsDateOfPayment(String amount) {
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().errorOnEditpageDateFieldisEmpty(amount);
    }


    @Then("user gets error message")
    public void userGetsErrorMessage() {
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().inputSecurityQuestionerror();
    }

    @Then("the routing number invalid error is displayed.")
    public void theRoutingNumberInvalidErrorIsDisplayed() {
        stepDefinitionManager.getPageObjectManager().getPaymentAccountPage().errorRoutingNumberfield();
    }

    @Then("the accountnumber number invalid error is displayed.")
    public void theAccountnumberNumberInvalidErrorIsDisplayed() {
        stepDefinitionManager.getPageObjectManager().getPaymentAccountPage().errorAccountNumberfield();
    }

    @Then("user is able to see the account number already exist message")
    public void userIsAbleToSeeTheAccountNumberAlreadyExistMessage() {
        stepDefinitionManager.getPageObjectManager().getPaymentAccountPage().errorSetupAccount();
    }


    @And("the user get error message if there are four funding accounts exists in view payment account page")
    public void theUserGetErrorMessageIfThereAreFourFundingAccountsExistsInViewPaymentAccountPage()  {
        stepDefinitionManager.getPageObjectManager().getPaymentAccountPage().clickAddAccounterror();
    }

    @Then("user the error message is displayed for amount field")
    public void userTheErrorMessageIsDisplayedForAmountField() {
        stepDefinitionManager.getPageObjectManager().getAutoPayPage().errorEnterDecimalValueinAutopay();
    }


    @Then("the user provides {string}")
    public void theUserProvides(String paymentAmount) {
        stepDefinitionManager.getPageObjectManager().getAutoPayPage().selectPaymentAmount(paymentAmount);
    }
    @Then("user click next button and user gets error for payment day field")
    public void user_click_next_button_and_user_gets_error_for_payment_day_field() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getAutoPayPage().errorEnterPaymentDayValueinAutopay();
    }



    @Then("user do not accepts terms and coditions and get error message")
    public void userDoNotAcceptsTermsAndCoditionsAndGetErrorMessage() {
        stepDefinitionManager.getPageObjectManager().getAutoPayPage().errorDonotAcceptsTermsandConditionsinAutopay();
   }


    @Then("Amount is greater than current balace error is displayed")
    public void amountIsGreaterThanCurrentBalaceErrorIsDisplayed() {
        stepDefinitionManager.getPageObjectManager().getMakePaymentPage().errorMakePaymentAmountisGreaterThanCurrentBalance();
    }
}


