package com.usbank.test.automationFramework.trancore.stepDefinitions.profile;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

public class ChangePasswordSteps {

    @Autowired
    StepDefinitionManager stepDefinitionManager;

    @Given("I click the Profile link")
    public void i_click_the_Profile_link() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getChangePasswordPage().getProfilePage().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getChangePasswordPage().clickProfileLink();

    }

    @And("I go to change my password")
    public void i_go_to_change_my_password() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getChangePasswordPage().getChangeYourPasswordlink().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getChangePasswordPage().clickChangePasswordlink();
        stepDefinitionManager.getPageObjectManager().getChangePasswordPage().getShieldQuestionAnswer();
    }


    @And("^I enter (.+) as my current password$")
    public void i_enter_my_current_password(String currentPassword) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getChangePasswordPage().getOldPassword().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getChangePasswordPage().enterCurrentPassword(currentPassword);

    }

    @And("^I enter (.+) as my new password$")
    public void i_enter_my_new_password(String newPassword) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getChangePasswordPage().getNewPassword().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getChangePasswordPage().enterNewPassword(newPassword);
    }

    @And("^I enter (.+) as my confirm new password$")
    public void i_enter_tester_as_my_confirm_new_password(String confirmPassword) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getChangePasswordPage().getConfirmNewPassword().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getChangePasswordPage().enterConfirmPassword(confirmPassword);
    }

    @When("I try to submit my password change")
    public void i_try_to_submit_my_password_change() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getChangePasswordPage().getBtnSubmit().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getChangePasswordPage().clickSubmitButton();
    }

    @Then("I can see the success message")
    public void i_can_see_the_success_message() {
        stepDefinitionManager.getPageObjectManager().getChangePasswordPage().checkSuccessMessage();
        stepDefinitionManager.getPageObjectManager().getChangePasswordPage().clickreturnToServicesButton();
    }

    @Then("I can see the error message for blank current password")
    public void iCanSeeTheErrorMessageForBlankCurrentPassword() {
        stepDefinitionManager.getPageObjectManager().getChangePasswordPage().checkErrorMessageForBlankCurrentPassword();
    }

    @Then("I can see the error message for blank new password")
    public void iCanSeeTheErrorMessageForBlankNewPassword() {
        stepDefinitionManager.getPageObjectManager().getChangePasswordPage().checkErrorMessageForBlankNewPassword();

    }

    @Then("I can see the error message for blank confirm new password")
    public void iCanSeeTheErrorMessageForBlankConfirmNewPassword() {
        stepDefinitionManager.getPageObjectManager().getChangePasswordPage().checkErrorMessageForBlankConfirmNewPassword();

    }

    @Then("I can see the error message for wrong password")
    public void iCanSeeTheErrorMessageForWrongPassword() {
        stepDefinitionManager.getPageObjectManager().getChangePasswordPage().checkErrorMessageForWrongCurrentPassword();

    }

    @Then("I can see the error message for New password same as current password")
    public void iCanSeeTheErrorMessageForNewPasswordSameAsCurrentPassword() {
        stepDefinitionManager.getPageObjectManager().getChangePasswordPage().checkErrorMessageNewAndCurrentPasswordSame();

    }

    @Then("I can see the error message for short length new password")
    public void iCanSeeTheErrorMessageForShortLengthNewPassword() {
        stepDefinitionManager.getPageObjectManager().getChangePasswordPage().checkErrorMessageForShortlengthNewPassword();
    }

    @Then("I can see the error message for new password having only characters")
    public void iCanSeeTheErrorMessageForNewPasswordHavingOnlyCharacters() {
        stepDefinitionManager.getPageObjectManager().getChangePasswordPage().checkErrorMessageForOnlyCharactersNewPassword();

    }

    @Then("I can see the error message for new password having only numerics")
    public void iCanSeeTheErrorMessageForNewPasswordHavingOnlyNumerics() {
        stepDefinitionManager.getPageObjectManager().getChangePasswordPage().checkErrorMessageForOnlyNumericNewPassword();

    }

    @Then("I can see the error message for new password having only special characters")
    public void iCanSeeTheErrorMessageForNewPasswordHavingOnlySpecialCharacters() {
        stepDefinitionManager.getPageObjectManager().getChangePasswordPage().checkErrorMessageForOnlySpecialCharactersNewPassword();

    }

    @Then("I can see the error message for new password having space")
    public void iCanSeeTheErrorMessageForNewPasswordHavingSpace() {
        stepDefinitionManager.getPageObjectManager().getChangePasswordPage().checkErrorMessageNewPasswordWithSpace();

    }

    @Then("I can see the error message for new password containing password word")
    public void iCanSeeTheErrorMessageForNewPasswordContainingPasswordWord() {
        stepDefinitionManager.getPageObjectManager().getChangePasswordPage().checkErrorMessageNewPasswordContainsPasswordWord();
    }
}
