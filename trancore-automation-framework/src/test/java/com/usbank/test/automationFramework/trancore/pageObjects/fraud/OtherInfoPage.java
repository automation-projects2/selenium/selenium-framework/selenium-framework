package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OtherInfoPage<T extends WebDriver> {

    protected static final Logger logger = LogManager.getLogger(OtherInfoPage.class);

    protected T driver;


    public OtherInfoPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement btnContinue;

    @FindBy(xpath = "//*[text()='Anything else?']")
    private WebElement lblOtherInfo;

    @FindBy(id = "5052")
    private WebElement txtArea;

    public WebElement getTxtArea() {
        return txtArea;
    }

    public WebElement getLblOtherInfo() {
        return lblOtherInfo;
    }

    public void waitForOtherInfoPageToLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(lblOtherInfo));
    }

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        ele.click();
    }

    public void clickContinue() {
        clickElement(driver, btnContinue);
    }
}
