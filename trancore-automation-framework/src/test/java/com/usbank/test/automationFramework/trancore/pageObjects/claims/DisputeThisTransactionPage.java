package com.usbank.test.automationFramework.trancore.pageObjects.claims;

import com.usbank.test.automationFramework.trancore.pageObjects.waitForPageToLoad.WaitForPageToLoad;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DisputeThisTransactionPage<T extends WebDriver> {
    @FindBy(how = How.XPATH, using = "//h1[@id='servicingAppHeader']")
    private WebElement lblDisputeThisTransactionHeader;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Did you or another authorized person on the accoun')]")
    private WebElement vrbDidYouMakeThisPurchase;

    @FindBy(how = How.ID, using = "radioYes")
    private WebElement rbYes;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'What was the purchase for?')]")
    private WebElement vrbWhatWasPurchase;

    @FindBy(how = How.ID, using = "radioS")
    private WebElement rbService;

    @FindBy(how = How.XPATH, using = "//button[@name='Continue']")
    private WebElement btnContinueOnDisputeThisTransactionPage;

    private T driver;
    //private WaitForPageToLoad waitForPageToLoad;

    WaitForPageToLoad callPageLoadMethod;

    private static final Logger log = LogManager.getLogger(DisputeThisTransactionPage.class.getName());

    public DisputeThisTransactionPage(T inDriver) {
        driver = inDriver;
        callPageLoadMethod = new WaitForPageToLoad(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getLblDisputeThisTransactionHeader() {
        return lblDisputeThisTransactionHeader;
    }

    public void didYouMakeThisPurchaseText() {
        log.info(vrbDidYouMakeThisPurchase.getText());
    }

    public void clickOnYesRdBtn() {
        rbYes.click();
    }

    public void whatWasThePurchaseForText() {
        log.info(vrbWhatWasPurchase.getText());
    }

    public void clickOnServiceRdBtn() {
        rbService.click();
    }

    public void clickOnContinueBtnOnDisputeThisTransactionPage() {
        btnContinueOnDisputeThisTransactionPage.click();
    }

    public void disputeThisTransactionPagePBOMFlow() throws InterruptedException {
        log.info("**************** Dispute This Transaction Page ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblDisputeThisTransactionHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblDisputeThisTransactionHeader, 5);
        didYouMakeThisPurchaseText();
        clickOnYesRdBtn();
        whatWasThePurchaseForText();
        clickOnServiceRdBtn();
        clickOnContinueBtnOnDisputeThisTransactionPage();
    }

    public void waitForPageLoad(){
        WebDriverWait wait = new WebDriverWait(driver,30);
       wait.until(ExpectedConditions.visibilityOf(rbYes));

    }

}
