@BalanceTransfer
Feature: Balance Transfer
  This test case ensures that the Balance Transfer feature automation script is working

  Scenario Outline: Verify that the user can access the Balance Transfer Link
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When I click Services Tab
    And I click link - Balance Transfer
    When the user enters in their Security Question on the Step Up Page
    And the user is able to View and Select a Balance Transfer Offer
    Given the user is now on the Balance Transfer Step One of Three Page and can click on the Balance Transfer Terms and Conditions Link
    And the user has clicked onto the Balance Transfer Terms and Conditions CheckBox
    And the user Selects A Payee <select_payee>
    And the user enters in the Account Number <account_number>
    And the user enters in the Transfer Amount <transfer_amount>
    And the user clicks on the Continue Button
    Then the user clicks on the Submit Button for a Balance Transfer

    Examples:
      | partner | testData | select_payee     | account_number | transfer_amount |

  Scenario Outline: Verify that the user can access the Custom Balance Transfer Link
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When I click Services Tab
    And I click link - Balance Transfer
    When the user enters in their Security Question on the Step Up Page
    And the user is able to View and Select a Balance Transfer Offer
    Given the user is now on the Balance Transfer Step One of Three Page and can click on the Balance Transfer Terms and Conditions Link
    And the user has clicked onto the Balance Transfer Terms and Conditions CheckBox
    And the user may enter in a custom payee name <custom_payee>
    And the user enters in the Account Number <account_number>
    And the user enters in the Payee Address <payee_address>
    And the user enters in the Suite Number <suite>
    And the user enters in the Transfer Amount <transfer_amount>
    And the user enters in the City <city>
    And the user selects a State <state>
    And the user enters in the ZipCode <zipcode>
    And the user clicks on the Continue Button
    Then the user clicks on the Submit Button for a Balance Transfer


    Examples:
      | partner | testData | custom_payee | account_number | payee_address | suite | city    | state     | zipcode | transfer_amount |

  Scenario Outline: Verify that the user can close Modal Pop-ups with ESC key within Balance Transfer

    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When I click Services Tab
    And I click link - Balance Transfer
    When the user enters in their Security Question on the Step Up Page
    And the user is able to View and Select a Balance Transfer Offer
    Given the user is now on the Balance Transfer Step One of Three Page and can click on the Balance Transfer Terms and Conditions Link
    And the user has clicked onto the Balance Transfer Terms and Conditions CheckBox
    And the user Selects A Payee <select_payee>
    And the user enters in the Account Number <account_number>
    And the user enters in the Transfer Amount <transfer_amount>
    And the user clicks on the Add Another Transfer Button and then proceeds to close the Pop-Up
    And the user clicks on the Select A Different Offer Button and then proceeds to close the Pop-Up
    And the user clicks on the Cancel Button and then proceeds to close the Pop-Up
    And the user clicks on the Continue Button
    And the user clicks on the Cancel Button on Step Two Of Three and then proceeds to close the Pop-Up
    Then the user clicks on the Submit Button for a Balance Transfer


    Examples:
      | partner | testData | select_payee | account_number | transfer_amount |
