package com.usbank.test.automationFramework.trancore.stepDefinitions.otp;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class EnterOTPPageStepDef {
    @Autowired
    StepDefinitionManager stepDefinitionManager;

    @When("the customer clicks on Send one-time code button at OTP Page")
    public void the_customer_clicks_on_Continue_at_OTP_Page() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getEnterOTPPage().VerifyContinueButton();
    }

    @Then("the customer should navigate to EnterOTP Page")
    public void the_customer_should_navigate_to_EnterOTP_Page() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getEnterOTPPage().VerifyHeaderName();
    }

    @And("Enter-OTP should display text field to enter OTP")
    public void enter_OTP_should_display_text_field_to_enter_OTP() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getEnterOTPPage().VerifyTextField();
    }

    @And("Enter-OTP page should display Continue button")
    public void enterOTPPageShouldDisplayContinueButton() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getEnterOTPPage().VerifyContinueButton();
    }

    @Then("Enter-OTP should display Send a new code button")
    public void enter_OTP_should_display_Send_a_new_code_button() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getEnterOTPPage().VerifySendanewcodeButton();
    }

    @And("Enter-OTP should display Cancel button")
    public void enterOTPShouldDisplayCancelButton() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getEnterOTPPage().VerifyCancelButton();
    }

    @When("the customer should verify the passcode invalid error message at enter OTP page")
    public void the_customer_should_verify_the_passcode_invalid_error_message_at_enter_OTP_page() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getEnterOTPPage().verifyInvalidOTPMsg();
    }

    @And("the customer should refresh the enter OTP page")
    public void theCustomerShouldRefreshTheEnterOTPPage() {
        stepDefinitionManager.getPageObjectManager().getEnterOTPPage().enterOTPPageRefresh();
    }
}
