package com.usbank.test.automationFramework.trancore.pageObjects.payments;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class MakePaymentPages<T extends WebDriver> {
    private static final Logger logger = LogManager.getLogger(MakePaymentPages.class);

    private T driver;


    @FindBy(how = How.XPATH, using = "//input[@name='Continue']")
    private WebElement continueButtonInDeletePopUp;


    @FindBy(how = How.XPATH, using = "//button[@data-test-id='MakeAPayment']")
    private WebElement makePaymentsPageButton;

    @FindBy(how = How.XPATH, using = "//input[@name='otherAmount']")
    private WebElement otherAmount;

    @FindBy(how = How.XPATH, using = "//input[@name='postingDate']")
    private WebElement paymentDate;

    @FindBy(how = How.XPATH, using = "//button[@title='Next']")
    private WebElement Next;

    @FindBy(how = How.XPATH, using = "//input[@title='Submit']")
    private WebElement Submit;

    @FindBy(how = How.XPATH, using = "//a[@title='Cancel']")
    private WebElement cancelPage1;

    @FindBy(how = How.XPATH, using = "//button[@title='Cancel']")
    private WebElement cancelPage2;

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Back')]")
    private WebElement Back;

    @FindBy(how = How.XPATH, using = "//button[@data-testid='dontLeave']")
    private WebElement stayPopUp;

    @FindBy(how = How.XPATH, using = "//button[@data-testid='leave']")
    private WebElement leavePopUp;

    @FindBy(how = How.XPATH, using = "//a[@class='paymentsLink']")
    private WebElement alertLink;

    @FindBy(how = How.XPATH, using = "//div[@id='alerts_account_tab']")
    private WebElement alertPage;

    @FindBy(how = How.XPATH, using = "//a[@title='View Payment History']")
    private WebElement paymentHistoryLink;

    @FindBy(how = How.XPATH, using = "//h2[@id='subHeader']/span")
    private WebElement paymentHistoryPage;

    @FindBy(how = How.XPATH, using = "//h1[@id='subHeader']/span")
    private WebElement paymentsLandingPage;

    @FindBy(how = How.XPATH, using = "//span[@id='suggestedDueDate']")
    private WebElement paymentDueDate;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Scheduled')]/parent::*/parent::*/tbody/tr/td[5]/div/a[@title='Edit']")
    private WebElement editScheduledPaymentLink;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Scheduled')]/parent::*/parent::*/tbody/tr/td[5]/div/a[@title='Delete']")
    private WebElement deleteScheduledPaymentLink;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Scheduled')]/parent::*/parent::*/tbody/tr/td[1]")
    private WebElement scheduledPaymentfromAccount;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Scheduled')]/parent::*/parent::*/tbody/tr/td[2]")
    private WebElement scheduledPaymentToAccount;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Scheduled')]/parent::*/parent::*/tbody/tr/td[3]/span")
    private WebElement scheduledPaymentAmount;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Scheduled')]/parent::*/parent::*/tbody/tr/td[4]")
    private WebElement ScheduledpaymentDate;

    @FindBy(how = How.XPATH, using = "//a[@title='View Payment Schedule']")
    private WebElement viewPaymentScheduleLink;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'Add or Delete Payment Accounts')]")
    private WebElement addOrDeletePaymentAccountsLink;

    @FindBy(how = How.XPATH, using = "//*[@id='challengeQuestion_lbl']")
    private WebElement securityQuestionText;

    @FindBy(how = How.XPATH, using = "//*[@id='answer']")
    private WebElement securityQuestionAnswerTextBox;

    @FindBy(how = How.XPATH, using = "//*[@data-testid='btnStepUpSubmit']")
    private WebElement securityQuestionSubmit;

    @FindBy(how = How.XPATH, using = "//select[@id='selectedPaymentAccount']")
    private WebElement fromAccount;

    @FindBy(how = How.XPATH, using = "//a[@data-test-id='Cancel']")
    private WebElement cancelButtonMake1of3;

    @FindBy(how = How.XPATH, using = "//div[@id='popupInner']//div[3]//button[@data-testid='leave']")
    private WebElement leaveButtonOnPopUp;

    @FindBy(how = How.XPATH, using = "//div[@id='popupInner']//div[3]//button[@data-testid='dontLeave']")
    private WebElement dontLeaveButtonOnPopUp;

    public MakePaymentPages(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    public void clickOnButtonOrLink(WebElement element) {

        element.click();
    }

    public void waitForElement(WebElement element, int waittime) {

        WebDriverWait wait = new WebDriverWait(driver, waittime);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void deleteAllExistingScheduledPay() {
        List<WebElement> AllScheduledPaymentsDelButton = driver.findElements(By.xpath("//span[contains(text(),'Scheduled')]/parent::*/parent::*/tbody/tr/td[@class='vcp_autopay_scheduleAction']/div/a[@title='Delete']"));
        int size = AllScheduledPaymentsDelButton.size();


        for (int i = 0; i < size; i++) {
            waitForElement(deleteScheduledPaymentLink, 10);
            deleteScheduledPaymentLink.click();
            waitForElement(continueButtonInDeletePopUp, 10);
            continueButtonInDeletePopUp.click();

        }

    }

    public void enterPaymentDetails(String paymentType, String date) {

        switch (paymentType) {
            case "Next Minimum Payment":
                driver.findElement(By.xpath("//input[@id='minimum']")).click();
                break;
            case "Remaining Statement Balance":
                driver.findElement(By.xpath("//input[@id='balance']")).click();
                break;
            case "Current Balance":
                driver.findElement(By.xpath("//input[@id='outstanding']")).click();
                break;
            default:
                otherAmount.sendKeys(paymentType);
                break;
        }

        paymentDate.clear();
        paymentDate.sendKeys(date);

    }

    public void deletePayment() {

        waitForElement(deleteScheduledPaymentLink, 10);
        deleteScheduledPaymentLink.click();
        waitForElement(continueButtonInDeletePopUp, 10);
        continueButtonInDeletePopUp.click();

    }

    public void clickMakePaymentsLink() {
        waitForElement(makePaymentsPageButton, 10);
        makePaymentsPageButton.click();
        System.out.println("Make payment page is opened");
    }

    public void clickAddOrDeletePaymentAccountLink() {
        waitForElement(addOrDeletePaymentAccountsLink, 10);
        addOrDeletePaymentAccountsLink.click();
        System.out.println("Add or Delete payment account page is opened");
    }

    public void inputSecurityQuestion() {
        waitForElement(securityQuestionText, 20);
        String questionText = securityQuestionText.getText();
        String lastWord = questionText.substring(questionText.lastIndexOf(" ") + 1, questionText.length() - 1);
        securityQuestionAnswerTextBox.sendKeys(lastWord);
        securityQuestionSubmit.click();
        System.out.println("Security Question is answered");
    }

    public void clickOnEditScheduledPay() {

        waitForElement(editScheduledPaymentLink, 20);
        editScheduledPaymentLink.click();

    }

    public void clickOnViewPaymentScheduledLink() {
        waitForElement(viewPaymentScheduleLink, 20);
        viewPaymentScheduleLink.click();
    }

    public void editDateAndAmountOfScheduledPay(String amount, String date) {
        waitForElement(otherAmount, 20);
        otherAmount.clear();
        otherAmount.sendKeys(amount);
        paymentDate.clear();
        paymentDate.sendKeys(date);

    }

    public void submitPayment() {
        waitForElement(Submit, 20);
        Submit.click();

    }

    public List<String> collectPaymentValues() {

        waitForElement(Submit, 20);

        WebElement ToAccount = driver.findElement(By.xpath("//span[text()='To this Account']/parent::div/div"));
        String ToAccountNumber = ToAccount.getText();
        ToAccountNumber = ToAccountNumber.substring(ToAccountNumber.lastIndexOf(' ') + 1);

        WebElement fromAccount = driver.findElement(By.xpath("//span[text()='From this Account ']/parent::div/div"));
        String fromAccountNumber = fromAccount.getText();

        WebElement PaymentAmountElement = driver.findElement(By.xpath("//span[text()='Payment Amount']/parent::div/div"));
        String PaymentAmount = PaymentAmountElement.getText();

        WebElement DateElement = driver.findElement(By.xpath("//span[text()='Payment Date']/parent::div/div"));
        String paymentDate = DateElement.getText();

        List<String> SubmittedValues = new ArrayList<>();
        SubmittedValues.add(ToAccountNumber);
        SubmittedValues.add(fromAccountNumber);
        SubmittedValues.add(PaymentAmount);
        SubmittedValues.add(paymentDate);

        return SubmittedValues;
    }

    public void verifySubmittedPayment(List<String> values) throws InterruptedException {

        Thread.sleep(2000);

        String ToAccount = values.get(0);
        String fromAccount = values.get(1);
        String Amount = values.get(2);
        String Date = values.get(3);
        ToAccount = "Credit Card ending in " + ToAccount;
        fromAccount = fromAccount.replace(" account", "");

        Assert.assertEquals(fromAccount, scheduledPaymentfromAccount.getText());
        Assert.assertEquals(ToAccount, scheduledPaymentToAccount.getText());
        Assert.assertEquals(Amount, scheduledPaymentAmount.getText());
        Assert.assertEquals(Date, ScheduledpaymentDate.getText());

    }


    public void verifyEditPayment(String amount, String date) {
        waitForElement(scheduledPaymentAmount, 20);

        String UIAmountValue = scheduledPaymentAmount.getText();
        String UIAmount = UIAmountValue.substring(1);
        String UIDate = ScheduledpaymentDate.getText();
        Assert.assertEquals(UIAmount, amount);
        Assert.assertEquals(UIDate, date);


    }

    public String generateRandomDate() {

        waitForElement(paymentDueDate, 20);

        String DueDate = paymentDueDate.getText();
        String[] DateSplit = DueDate.split("/");
        String DateYear = DateSplit[2];
        String DateMonth = DateSplit[0];
        String DateDay = DateSplit[1];

        DateMonth = DateMonth.replaceAll("\\s", "");

        int DateYearInt = Integer.parseInt(DateYear);
        int DateMonthInt = Integer.parseInt(DateMonth);
        int DateDayInt = Integer.parseInt(DateDay);

        long minDay = LocalDate.now().toEpochDay();

        long maxDay = LocalDate.of(DateYearInt, DateMonthInt, DateDayInt).toEpochDay();

        if (maxDay > minDay) {
            long randomDay = ThreadLocalRandom.current().nextLong(minDay, maxDay);
            LocalDate randomDate = LocalDate.ofEpochDay(randomDay);
            String randomDateStr = randomDate.toString();
            LocalDate dt = LocalDate.parse(randomDateStr);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            return formatter.format(dt);
        } else
            return DueDate;
    }

    public void clickAlertLink() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(alertLink));
        alertLink.click();
    }

    public void verifyAlertsPageLanding() {
        waitForElement(alertPage, 10);
        Assert.assertTrue(alertPage.isDisplayed());
    }

    public void clickPaymantHistoryLink() {
        waitForElement(paymentHistoryLink, 10);
        paymentHistoryLink.click();
    }

    public void verifyPaymnetHistoryPageLanding() {
        waitForElement(paymentHistoryPage, 10);
        Assert.assertTrue(paymentHistoryPage.isDisplayed());
    }

    public void clickCancel1() {
        waitForElement(cancelPage1, 10);
        clickOnButtonOrLink(cancelPage1);
    }

    public void clickCancel2() {
        waitForElement(cancelPage2, 10);
        clickOnButtonOrLink(cancelPage2);
    }

    public void clickBack() {
        waitForElement(Back, 10);
        clickOnButtonOrLink(Back);
    }

    public void clickPopUpNo() {
        waitForElement(stayPopUp, 5);
        clickOnButtonOrLink(stayPopUp);
    }

    public void clickPopUpYes() {
        waitForElement(leavePopUp, 5);
        clickOnButtonOrLink(leavePopUp);
    }

    public void verifyPaymentsLandingPage() {
        waitForElement(paymentsLandingPage, 5);
        Assert.assertTrue(paymentsLandingPage.isDisplayed());
    }

    public boolean checkFundingAccountPresent()
    {
      boolean result3;
        result3 = driver.findElements(By.xpath("//div[@id='challengeQuestion_lbl']")).isEmpty();
        return result3;
    }

    public void selectFromAccount() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(fromAccount));
        Select fromAccount1 = new Select(fromAccount);
        fromAccount1.selectByIndex(0);

    }

    public void clickNextButton() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(Next));
        Next.click();
    }

    public void makePaymentErrorPaymentDate(String amount, String date) {
        selectFromAccount();
        otherAmount.clear();
        otherAmount.sendKeys(amount);
        paymentDate.clear();
        paymentDate.sendKeys(date);
        clickNextButton();
        clickNextButton();
        // WebDriverWait wait = new WebDriverWait(driver, 20);
        //wait.until(ExpectedConditions.elementToBeClickable(Submit));
        //Submit.click();
        System.out.println("Error message is displayed");
    }

    public void errorMessageOtherAmountField(String date) {
        selectFromAccount();
        paymentDate.clear();
        paymentDate.sendKeys(date);
        clickNextButton();
        System.out.println("You must enter a payment amount that is greater than zero dollars message is displayed");
    }

    public void errorMessagePaymentDateField(String amount) {
        selectFromAccount();
        otherAmount.clear();
        otherAmount.sendKeys(amount);
        paymentDate.clear();
        clickNextButton();
        System.out.println("Please enter your Payment Date. message is displayed");
    }

    public void makePaymentError() {

        selectFromAccount();
        paymentDate.clear();
        clickNextButton();
        System.out.println("The error message is displayed for payment date and otneramount filed");
    }

    public void remaningStaetmentBalanceMakepayment() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//label[contains(text(),'Remaining Statement Balance')]"))));
        String Actualst = driver.findElement(By.xpath("//label[contains(text(),'Remaining Statement Balance')]")).getText();
        System.out.println(Actualst);
        String Expectedst = "Remaining";

        if (Actualst.contains(Expectedst)) {
            System.out.println("Last statement balance is changed as Remaining Statement Balance ");
        } else {
            System.out.println("Last statement balance is not changed as Remaining Statement Balance ");
        }
    }


    public void errorEditDateAndAmountOfScheduledPay(String amount, String date) {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(otherAmount));
        otherAmount.clear();
        otherAmount.sendKeys(amount);
        paymentDate.clear();
        paymentDate.sendKeys(date);
        System.out.println("The user is able to see the error message");
    }

    public void errorOnEditpageAmountFieldisEmpty(String date) {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(otherAmount));
        otherAmount.clear();
        paymentDate.clear();
        paymentDate.sendKeys(date);
        clickNextButton();
        System.out.println("You must enter a payment amount that is greater than zero dollars message is displayed");
    }

    public void errorOnEditpageDateFieldisEmpty(String amount) {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(otherAmount));
        otherAmount.clear();
        otherAmount.sendKeys(amount);
        paymentDate.clear();
        clickNextButton();
        System.out.println("Please enter the payment date message is displayed");
    }

    public void inputSecurityQuestionerror() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOf(securityQuestionAnswerTextBox));
        securityQuestionAnswerTextBox.sendKeys("word");
        securityQuestionSubmit.click();
        System.out.println("Answer to your ID Shield Question is invalid message is displyed");
    }

    public void errorMakePaymentAmountisGreaterThanCurrentBalance() {
        System.out.println("You must enter a payment amount that is equal to or less than your current balance error message");
    }

}
