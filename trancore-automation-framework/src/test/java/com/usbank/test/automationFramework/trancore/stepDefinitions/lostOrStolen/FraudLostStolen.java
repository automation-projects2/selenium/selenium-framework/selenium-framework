package com.usbank.test.automationFramework.trancore.stepDefinitions.lostOrStolen;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import java.awt.*;

public class FraudLostStolen {
    @Autowired
    private StepDefinitionManager stepDefinitionManager;

    @And("the user clicks on the Continue Button at Recent Review Transaction Page")
    public void theUserClicksOnTheContinueButtonatRecentReviewTransactionPage() {
    stepDefinitionManager.getPageObjectManager().getReviewRecentTransactionPage().ClickContinue();
        }


    @And("the user clicks on No, I see at least one purchase I didn't make radio button")
    public void theUserClicksOnNoISeeAtLeastOnePurchaseIDidnTMakeRadioButton() {
        stepDefinitionManager.getPageObjectManager().getReviewRecentTransactionPage().selectRadioButtonNo();
    }

    @And("the user navigates to Deactivate this card page and click on Continue Button")
    public void theUserNavigatesToDeactivateThisCardPageAndClickOnContinueButton() {
        stepDefinitionManager.getPageObjectManager().getUnAuthAccActFraudAppChkPage().waitForUnAuthAccActFraudAppChkPageToLoad();
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getUnAuthAccActFraudAppChkPage().getLblDeactivateCard().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getUnAuthAccActFraudAppChkPage().clickDeactivateCardBtn();

    }

    @And("the user navigates to call us now to finish page and clicks on return to Orginate link")
    public void theUserNavigatesToCallUsNowToFinishPageAndClicksOnReturnToOrginateLink() {
        stepDefinitionManager.getPageObjectManager().getCallUsNowToFinish().verifyHeadername();
        stepDefinitionManager.getPageObjectManager().getCallUsNowToFinish().clickReturn();

    }

    @And("the user clicks on the Continue Button and handles the popup at Recent Review Transaction Page")
    public void theUserClicksOnTheContinueButtonAndHandlesThePopupAtRecentReviewTransactionPage() {
        stepDefinitionManager.getPageObjectManager().getReviewRecentTransactionPage().ClickContinue();
        stepDefinitionManager.getPageObjectManager().getReviewRecentTransactionPage().clickPopupContinue();
    }
}
