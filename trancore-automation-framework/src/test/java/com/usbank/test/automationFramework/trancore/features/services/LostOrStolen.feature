@ReportCardLostOrStolen
Feature: Report Card Lost Or Stolen
  This test case ensures that the Lost Or Stolen feature automation script is working

  Scenario Outline: Verify that the user can execute Report Card Lost Or Stolen script
    Given the user has navigated to the <partner> login page
    Given the user has <user_name> username and <password> password
    When the user clicks on the Services tab and is able to click the Report Card Lost Or Stolen Link
    And the user clicks on the Continue Button to proceed into Report Card Lost or Stolen flow
    And the user clicks on the Yes I Recognize All These Purchases Radio Button
    And the user clicks the Continue Button on Review Recent Transactions Page
    And the user enters in the missing date <date> for the Lost Or Stolen on the Quick Question Page
    And the user is on the Quick Question Page and clicks on the Continue Button
    And the user is on the Card Delivery Page and selects Standard Delivery with the Continue Button
    Then the user is on the Review And Submit Your Request page and clicks on Report Card Lost Or Stolen button
    Then the user clicks on Return To Services Button

    Examples:

      | partner | user_name | password | date |
