package com.usbank.test.automationFramework.trancore.pageObjects.profile;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;


public class AddNewPhoneNumberPage {

    private static final Logger logger = LogManager.getLogger(AddNewPhoneNumberPage.class);
    private static final long EXPLICIT_WAIT_TIME_SECONDS = 30;
    private WebDriver driver;

    @FindBy(how = How.TAG_NAME, using = "h1")
    private WebElement headerName;

    @FindBy(how = How.ID, using = "phoneNumber")
    private WebElement txtfieldAddPhoneNumber;

    @FindBy(how = How.LINK_TEXT, using = "Add new phone number")
    private WebElement buttonAddNewPhoneNumber;

    @FindBy(how = How.LINK_TEXT, using = "Cancel")
    private WebElement buttonCancel;

    @FindBy(how = How.ID, using = "textConsentInd")
    private WebElement checkboxStopTextMessages;

    @FindBy(how = How.ID, using = "voiceConsentInd")
    private WebElement checkboxStopAutomatedCalls;

    public AddNewPhoneNumberPage(WebDriver inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public void verifyHeaderName() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(buttonAddNewPhoneNumber));
        assertEquals("add new phone number", headerName.getText().toLowerCase());

    }

    public void clickAddNewPhoneNumber() {
        buttonAddNewPhoneNumber.click();
    }

    public void clickCancelButton() {
        buttonCancel.click();
    }

    public void selectPhoneType(String PhoneType) {
        PhoneType = PhoneType.toLowerCase();
        if (PhoneType.matches("home")) {
            WebElement PhoneNumber = driver.findElement(By.id("homePhoneNumber"));
            PhoneNumber.click();
        } else if (PhoneType.matches("personal")) {
            WebElement PhoneNumber = driver.findElement(By.id("personalPhoneNumber"));
            PhoneNumber.click();
        } else if (PhoneType.matches("work")) {
            WebElement PhoneNumber = driver.findElement(By.id("workPhoneNumber"));
            PhoneNumber.click();
        }
    }

    public void setCheckboxStopTextMessages() {
        checkboxStopTextMessages.click();
    }

    public void setCheckboxStopAutomatedCalls() {
        checkboxStopAutomatedCalls.click();
    }
}



	