package com.usbank.test.automationFramework.trancore.pageObjects.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class OrderANewCardPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(OrderANewCardPage.class);

    private static final long EXPLICIT_WAIT_TIME_SECONDS = 30;

    private T driver;

    @FindBy(how = How.TAG_NAME, using = "h2")
    private WebElement headername;

    @FindBy(how = How.ID, using = "Continue")
    private WebElement linkContinue;


    @FindBy(how = How.LINK_TEXT, using = "Cancel")
    private WebElement linkCancel;

    public OrderANewCardPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }


    public void verifyHeaderName() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(linkContinue));
        assertEquals("order a new card", (headername.getText()).toLowerCase());

    }

    public void ClickContinue() {
        linkContinue.click();
    }

    public void ClickCancel() {
        linkCancel.click();
    }

    public void SelectReasonOrderANewCard(String Reason) {
        List<WebElement> selectReason = driver.findElements(By.className("label__text"));
        int i;
        for (i = 0; i < selectReason.size(); i++) {

            if ((selectReason.get(i).getText()).equals(Reason)) {
                selectReason.get(i).click();
                break;
            }
        }


    }
}