#Author: Sujin James
@authorizedOwner
Feature: Authorized Owner

  @TC001
  Scenario Outline: When Logged with AO account, make sure all the Tabs are been clicked and displayed
    Given the user has navigated to the <partner> login page
    When the user has logged into My Account page with <user_name> username and <password> password
    Then the user to make sure all the features are present

    Examples:
      |partner  |user_name|password |

  @TC002
  Scenario Outline: When Logged in User is AO and selected account type is AE, in Fraud Alerts tab, Update Contact Information tab will be hidden
    Given the user has navigated to the <partner> login page
    When the user has logged into My Account page with <user_name> username and <password> password
    And the user select the Select Account tab
    And the user select AE <account> account
    And the user click on FraudAlert tab
    Then Display status of Update Contact Information button is false

    Examples:
      |partner  |user_name|password |account|

  @TC003
  Scenario Outline: When Logged in User is AO,in Fraud Alerts tab, Update Contact Information tab will be displayed
    Given the user has navigated to the <partner> login page
    When the user has logged into My Account page with <user_name> username and <password> password
    And the user click on FraudAlert tab
    Then Display status of Update Contact Information button is true

    Examples:
      |partner  |user_name|password |

  @TC004
  Scenario Outline: When Logged in User is AO, Lock or Unlock Card is displayed in Services tab
    Given the user has navigated to the <partner> login page
    When the user has logged into My Account page with <user_name> username and <password> password
    And the user click on Services tab
    Then Display Status of Lock or Unlock card link is true

    Examples:
      |partner  |user_name|password |

  @TC005
  Scenario Outline: When Logged in User is AO and selected account type is AE, Lock or Unlock Card is not displayed in Services tab
    Given the user has navigated to the <partner> login page
    When the user has logged into My Account page with <user_name> username and <password> password
    And the user select the Select Account tab
    And the user select AE <account> account
    And the user click on Services tab
    Then Display Status of Lock or Unlock card link is false

    Examples:
      |partner  |user_name|password |account|

  @TC006
  Scenario Outline: When Logged in User is AO, check for the message displayed in Report Lost Or Stolen link
    Given the user has navigated to the <partner> login page
    When the user has logged into My Account page with <user_name> username and <password> password
    And the user click on LostOrStolen link
    Then the user verify the <userType> message of LostOrStolen

    Examples:
      |partner  |user_name|password |userType|

  @TC007
  Scenario Outline: When Logged in User is AO and selected account type is AE, check for the message displayed in Report Lost Or Stolen link
    Given the user has navigated to the <partner> login page
    When the user has logged into My Account page with <user_name> username and <password> password
    And the user select the Select Account tab
    And the user select AE <account> account
    And the user click on LostOrStolen link
    Then the user verify the <userType> message of LostOrStolen

    Examples:
      |partner  |user_name|password |userType|account|

  @TC008
  Scenario Outline: When Logged in User is AO and selected account type is AE, verify if the name been displayed in Card Delivery & Review Page
    Given the user has navigated to the <partner> login page
    When the user has logged into My Account page with <user_name> username and <password> password
    And the user select the Select Account tab
    And the user select AE <account> account
    And the user click on LostOrStolen link
    And the user reach to DeliveryOption Page
    Then the user verify the AE name is displayed in address

    Examples:
      |partner  |user_name|password |userType|account|


  @TC009
  Scenario Outline: When Logged in User is AO, check for the message displayed in Order A New Card link
    Given the user has navigated to the <partner> login page
    When the user has logged into My Account page with <user_name> username and <password> password
    And the user click on OrderANewCard link
    Then the user verify the <userType> message of OrderNewCard

    Examples:
      |partner  |user_name|password |userType|

  @TC010
  Scenario Outline: When Logged in User is AO and selected account type is AE, check for the message displayed in Order A New Card link
    Given the user has navigated to the <partner> login page
    When the user has logged into My Account page with <user_name> username and <password> password
    And the user select the Select Account tab
    And the user select AE <account> account
    And the user click on OrderANewCard link
    Then the user verify the <userType> message of OrderNewCard

    Examples:
      |partner  |user_name|password |userType|account|

  @TC011
  Scenario Outline: When Logged in User is AO and selected account type is AE, AE name displayed along with address in OrderNewCard
    Given the user has navigated to the <partner> login page
    When the user has logged into My Account page with <user_name> username and <password> password
    And the user select the Select Account tab
    And the user select AE <account> account
    And the user click on OrderANewCard link
    And the user reach to Delivery page
    Then the user verify the AE name is displayed in address

    Examples:
      |partner  |user_name|password |account|



  @TC012
  Scenario Outline: Verify  Manage Employee link should be available on Self service page for AO Account
    Given the user has navigated to the <partner> login page
    When the user has logged into My Account page with <user_name> username and <password> password
    And the user click on Services tab
    Then the user verify the Manage Employee link is true

    Examples:
      |partner  |user_name|password |


  @TC013
  Scenario Outline: Verify  Manage Employees link should not be available on Self service page other than AO Account
    Given the user has navigated to the <partner> login page
    When the user has logged into My Account page with <user_name> username and <password> password
    And the user click on Services tab
    Then the user verify the Manage Employee link is false

    Examples:
      |partner  |user_name|password |

  @TC014
  Scenario Outline: Add authorized representative (pdf) link should be displayed if user is logged in with AO account & partner is not Fidelity/ACG
    Given the user has navigated to the <partner> login page
    When the user has logged into My Account page with <user_name> username and <password> password
    And the user click on Services tab
    Then Status of Add Authorized Rep link is true

    Examples:
      |partner  |user_name|password |

  @TC015
  Scenario Outline: Add authorized representative (pdf) link should not be displayed if user is logged in with AO account & partner is Fidelity/ACG
    Given the user has navigated to the <partner> login page
    When the user has logged into My Account page with <user_name> username and <password> password
    And the user click on Services tab
    Then Status of Add Authorized Rep link is false

    Examples:
      |partner  |user_name|password |

  @TC016
  Scenario Outline: Add authorized representative (pdf) link should not be displayed if user is logged in with other Account type
    Given the user has navigated to the <partner> login page
    When the user has logged into My Account page with <user_name> username and <password> password
    And the user click on Services tab
    Then Status of Add Authorized Rep link is false

    Examples:
      |partner  |user_name|password |


  @TC017
  Scenario Outline: When Logged in User is AO, Contact Information link will be displayed
    Given the user has navigated to the <partner> login page
    When the user has logged into My Account page with <user_name> username and <password> password
    And the user click on  Profile tab
    Then Display status of Contact Information button is link is true

    Examples:
      |partner  |user_name|password |

  @TC018
  Scenario Outline: When Logged in User is AO and selected account type is AE, Contact Information link will be hidden
    Given the user has navigated to the <partner> login page
    When the user has logged into My Account page with <user_name> username and <password> password
    And the user select the Select Account tab
    And the user select AE <account> account
    And the user click on  Profile tab
    Then Display status of Contact Information button is link is false

    Examples:
      |partner  |user_name|password |account|

