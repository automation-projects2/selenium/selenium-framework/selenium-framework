package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ContactValidationPage<T extends WebDriver> {

    protected static final Logger logger = LogManager.getLogger(ContactValidationPage.class);

    protected T driver;

    @FindBy(id = "uspsAddress")
    private WebElement rdbUspsAddress;

    @FindBy(id = "servicingAppHeader")
    private WebElement mailingInfoHeader;

    @FindBy(name = "Continue")
    private WebElement btnContinue;

    public ContactValidationPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    public WebElement getRdbUspsAddress() {
        return rdbUspsAddress;
    }

    public void waitForMailingInfoPageToLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 50);
        wait.until(ExpectedConditions.visibilityOf(mailingInfoHeader));
    }


    public WebElement getMailingInfoHeader() {
        return mailingInfoHeader;
    }

    public WebElement getBtnContinue() {
        return btnContinue;
    }

    public void clickContinue() {
        clickElement(driver, btnContinue);
    }

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        ele.click();
    }

    public void clickUspsRadioButton() {
        waitForMailingInfoPageToLoad();
        clickElement(driver, rdbUspsAddress);
    }


}
