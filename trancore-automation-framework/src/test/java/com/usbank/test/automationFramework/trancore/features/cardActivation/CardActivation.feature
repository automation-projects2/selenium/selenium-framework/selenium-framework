@CardActivation

Feature: As primary customer should be able navigate to Card Activation and Activate the card

  Scenario Outline: Customer able to activate the card sucessfully

    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user click the Services Link
    And the user click on Card Activation link at Service Tab
    And the user is navigated to Card Activation Page
    And the user enters <SSN> last 4 SSN at Card Activation Page
    And the user enter <CVV> CVV code at Card Activation Page
    And the user clicks on Submit Button at Card Activation Page
    # Validating to error message for both SSN & CVV Code, This will work for all different messages
    Then the card is activated and user is navigated to Card Activation Page
    And the customer clicks on Logout Button
    And the customer should be directed to Login Page
    Examples:
      | partner | testData | SSN  | CVV |

