@Claims @WIP @Regression

Feature: Claims
  This test case ensures that the Claims feature automation script is working

  Scenario Outline: Verify that the user can make a Claim
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When the user is on the My Account Page and is able to view the Posted transactions and is able to Dispute This Transaction
    Then the user proceeds through the Paid By Other Means <Flow>

    Examples:
      | partner | testData   | Flow |
