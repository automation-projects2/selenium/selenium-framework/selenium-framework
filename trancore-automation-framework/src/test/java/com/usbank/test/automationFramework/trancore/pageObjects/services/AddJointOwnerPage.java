package com.usbank.test.automationFramework.trancore.pageObjects.services;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class AddJointOwnerPage<T extends WebDriver> {

    @FindBy(how = How.LINK_TEXT, using = "Add joint owner (pdf)")
    private WebElement lnkAddJointOwnerPDF;

    private T driver;

    public AddJointOwnerPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public void viewAndDownloadJointOwnerPDF() {
        String parentWindow = driver.getWindowHandle();

        /*
         * this will loop through each open window and close out each one
         */
        for (String subWindow : driver.getWindowHandles()) {
            driver.switchTo().window(subWindow);
            String URL = driver.getCurrentUrl();
            String title = driver.getTitle();
            System.out.println("Current window title: " + title);
            System.out.println("Current window URL: " + URL);
        }

        driver.close();
        driver.switchTo().window(parentWindow);
    }

    public WebElement getLnkAddJointOwnerPDF() {
        return lnkAddJointOwnerPDF;
    }
}
