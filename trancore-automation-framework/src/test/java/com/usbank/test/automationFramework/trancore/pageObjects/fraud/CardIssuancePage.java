package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CardIssuancePage<T extends WebDriver> {

    protected static final Logger logger = LogManager.getLogger(CardIssuancePage.class);

    protected T driver;


    public CardIssuancePage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "radioStandard")
    private WebElement rdbStandard;

    @FindBy(id = "radioExpedited")
    private WebElement rdbExpedited;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement btnContinue;

    @FindBy(xpath = "//*[text()='Card delivery options']")
    private WebElement lblCardDeliveryOptions;

    @FindBy(xpath = " //*[text()='Delivery address:']")
    private WebElement lblDeliveryAddress;

    public WebElement getRdbExpedited() {
        return rdbExpedited;
    }

    public WebElement getLblCardDeliveryOptions() {
        return lblCardDeliveryOptions;
    }

    public WebElement getLblDeliveryAddress() {
        return lblDeliveryAddress;
    }

    public void waitForCardIssuancePageToLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(lblCardDeliveryOptions));

    }

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        ele.click();
    }


    public void clickContinue() {
        clickElement(driver, btnContinue);
    }

}
