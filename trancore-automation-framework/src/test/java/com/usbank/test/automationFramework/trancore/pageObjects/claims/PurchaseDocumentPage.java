package com.usbank.test.automationFramework.trancore.pageObjects.claims;

import com.usbank.test.automationFramework.trancore.pageObjects.waitForPageToLoad.WaitForPageToLoad;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class PurchaseDocumentPage<T extends WebDriver> {
    @FindBy(how = How.XPATH, using = "//h1[@id='servicingAppHeader']")
    private WebElement lblPurchaseDocumentationHeader;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Do you have documentation, such as a receipt or st')]")
    private WebElement vrbDoYouHavePurchaseDocumentation;

    @FindBy(how = How.XPATH, using = "//p[contains(text(),'You can use a copy of your statement or ask the me')]")
    private WebElement vrbYouCanUseACopyOfYourStatement;

    @FindBy(how = How.XPATH, using = "//input[@id='34-Y']")
    private WebElement rbYes;

    @FindBy(how = How.XPATH, using = "//input[@id='34-N']")
    private WebElement rbNo;

    //These web-elements are for flow#2
    @FindBy(how = How.XPATH, using = "//p[contains(text(),\"We'll need some information about your other form \")]")
    private WebElement vrbWeWillNeedSomeInfo;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Is the same merchant listed on the account summary')]")
    private WebElement vrbIsTheSameMerchantListed;

    @FindBy(how = How.XPATH, using = "//input[@id='511-Y']")
    private WebElement rbSelectYesSameMerchant;

    @FindBy(how = How.XPATH, using = "//input[@id='511-N']")
    private WebElement rbSelectNoSameMerchant;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Do you have documentation, such as a receipt or st')]")
    private WebElement vrbDoYouHaveDocumentation;

    @FindBy(how = How.XPATH, using = "//input[@id='514-Y']")
    private WebElement rbSelectYesDoYouHaveDocumentation;

    @FindBy(how = How.XPATH, using = "//input[@id='514-N']")
    private WebElement rbSelectNoDoYouHaveDocumentation;

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Continue')]")
    private WebElement btnContinueOnPurchaseDocumentationPage;

    private T driver;

    WaitForPageToLoad callPageLoadMethod;

    private static final Logger log = LogManager.getLogger(PurchaseDocumentPage.class.getName());

    public PurchaseDocumentPage(T inDriver) {
        driver = inDriver;
        callPageLoadMethod = new WaitForPageToLoad(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement lblPurchaseDocumentationHeader() {
        return lblPurchaseDocumentationHeader;
    }

    public void doYouHavePurchaseDocumentation() {
        log.info(vrbDoYouHavePurchaseDocumentation.getText());
    }

    public void youCanUseACopyOfYourStatement() {
        log.info(vrbYouCanUseACopyOfYourStatement.getText());
    }

    public void clickOnYesRbtn() {
        rbYes.click();
    }

    public void clickOnNoRbtn() {
        rbNo.click();
    }

    //These methods are for alternate Flow#2
    public void weWillNeedSomeInformationText() {
        log.info(vrbWeWillNeedSomeInfo.getText());
    }

    public void isTheSameMerchantListedText() {
        log.info(vrbIsTheSameMerchantListed.getText());
    }

    public void clickOnYesSameMerchantRb() {
        rbSelectYesSameMerchant.click();
    }

    public void clickOnNoSameMerchantRb() {
        rbSelectNoSameMerchant.click();
    }

    public void doYouHaveDocumentationText() {
        log.info(vrbDoYouHaveDocumentation.getText());
    }

    public void clickOnYesDoYouHaveDocumentationRb() {
        rbSelectYesDoYouHaveDocumentation.click();
    }

    public void clickOnNoDoYouHaveDocumentationRb() {
        rbSelectNoDoYouHaveDocumentation.click();
    }

    public void clickOnContinueBtnOnPurchaseDocumentPage() {
        btnContinueOnPurchaseDocumentationPage.click();
    }

    public void purchaseDocumentPagePBOMFlowOne() throws InterruptedException {
        log.info("**************** Purchase Document Page Reasoning One ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblPurchaseDocumentationHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblPurchaseDocumentationHeader, 3);
        doYouHavePurchaseDocumentation();
        youCanUseACopyOfYourStatement();
        clickOnYesRbtn();
        clickOnContinueBtnOnPurchaseDocumentPage();
    }

    public void purchaseDocumentPagePBOMFlowTwo() throws InterruptedException {
        log.info("**************** Purchase Document Page Reasoning Two ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblPurchaseDocumentationHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblPurchaseDocumentationHeader, 3);
        weWillNeedSomeInformationText();
        isTheSameMerchantListedText();
        clickOnYesSameMerchantRb();
        doYouHaveDocumentationText();
        clickOnYesDoYouHaveDocumentationRb();
        clickOnContinueBtnOnPurchaseDocumentPage();
    }

}
