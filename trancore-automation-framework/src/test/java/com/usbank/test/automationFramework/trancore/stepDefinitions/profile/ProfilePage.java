package com.usbank.test.automationFramework.trancore.stepDefinitions.profile;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import com.usbank.test.driver.DriverManager;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Given;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

@RunWith(Cucumber.class)
public class ProfilePage {
	
    @Autowired
    private StepDefinitionManager stepDefinitionManager;
    

    @Then("the user has navigated to the Profile page")
    public void the_user_has_navigated_to_the_Profile_page() throws InterruptedException {
    	 stepDefinitionManager.getPageObjectManager().getWebNavigationPage().clickProfileLink();
    }
    
    
    @Given("the user has navigated to Contact Information page")
    public void the_user_has_navigated_to_Contact_Information_page() {
    	stepDefinitionManager.getPageObjectManager().getWebNavigationPage().clickContactInfo();
    }
    
    @Then("user click on add company phone number link to add phone number")
    public void user_click_on_add_company_phone_number_link_to_add_phone_number() {
    	stepDefinitionManager.getPageObjectManager().getContactInformation().clickAddCompanyPhone();
       
    }

    @Given("the user click cancel modal from add company phone number page")
    public void the_user_click_cancel_modal_from_add_company_phone_number_page() {
    	stepDefinitionManager.getPageObjectManager().getContactInformation().clickCancelonAddComapnyPhone();
       
    }

    @Given("user  click on I will do it later button on no company phone on file modal")
    public void user_click_on_I_will_do_it_later_button_on_no_company_phone_on_file_modal() {
    	stepDefinitionManager.getPageObjectManager().getContactInformation().clickIwillDoitLater();
       
    }

    @Then("the customer should be taken back to the Contact information landing page")
    public void the_customer_should_be_taken_back_to_the_Contact_information_landing_page() {
    	stepDefinitionManager.getPageObjectManager().getContactInformation().validateNavigationtoContactInformationPage();
       
    }
    @When("the customer clicks on the Continue adding number button")
    public void the_customer_clicks_on_the_Continue_adding_number_button() {
    	stepDefinitionManager.getPageObjectManager().getContactInformation().clickContinueAddingPhonenumber();        
    }

    @Then("user must be returned to the Add company phone number page")
    public void user_must_be_returned_to_the_Add_company_phone_number_page() {
    	stepDefinitionManager.getPageObjectManager().getContactInformation().validateNavigationtoAddCompanyPhoneNumberPage();
       
    }

    @Given("user has entered a valid company phone number")
    public void user_has_entered_a_valid_company_phone_number() {
    	stepDefinitionManager.getPageObjectManager().getContactInformation().addCompanyPhoneNumber();
    }
    
    @When("customer clicks the Add company phone number button")
    public void customer_clicks_the_Add_company_phone_number_button() {
    	stepDefinitionManager.getPageObjectManager().getContactInformation().clickAddCompanyPhoneNumber();
    }

    @Then("unique confirmation message Success! Thanks for adding your phone number. appears on the Contact information landing page")
    public void unique_confirmation_message_Success_Thanks_for_adding_your_phone_number_appears_on_the_Contact_information_landing_page() {
    	stepDefinitionManager.getPageObjectManager().getContactInformation().validateSuccessMessage();
    }
    @Given("user click on Editcompany phone number link to edit company phone number")
    public void user_click_on_Editcompany_phone_number_link_to_edit_company_phone_number() {
    	stepDefinitionManager.getPageObjectManager().getContactInformation().clickonEditCompanyPhone();
    }

    @When("user clicks on the Cancel button on the Edit company phone number page after making field entries")
    public void user_clicks_on_the_Cancel_button_on_the_Edit_company_phone_number_page_after_making_field_entries() {
    	stepDefinitionManager.getPageObjectManager().getContactInformation().editandCancelPhone();
    }

    @Then("user clicks No, don't leave on the cancel modal")
    public void user_clicks_No_don_t_leave_on_the_cancel_modal() {
    	stepDefinitionManager.getPageObjectManager().getContactInformation().clickDontleaveonCancelPhonedialog();
       
    }

    @Then("user must be returned to the Edit company phone number page")
    public void user_must_be_returned_to_the_Edit_company_phone_number_page() {
    	stepDefinitionManager.getPageObjectManager().getContactInformation().validateNavigationtoEditCompanyPhoneNumberPage();
       
    }

    @Then("user has entered a valid company phone")
    public void user_has_entered_a_valid_company_phone() {
    	stepDefinitionManager.getPageObjectManager().getContactInformation().EnterValidPhoneNumber();
    }

    @Then("user clicks the Edit company phone number button")
    public void user_clicks_the_Edit_company_phone_number_button() {
    	stepDefinitionManager.getPageObjectManager().getContactInformation().clickOnUpdatePhonenumberButton();
       
    }

    @Then("unique confirmation message Success! Thanks for updating your phone number. appears on the Contact information landing page ")
    public void unique_confirmation_message_Success_Thanks_for_updating_your_phone_number_appears_on_the_Contact_information_landing_page() {
    	stepDefinitionManager.getPageObjectManager().getContactInformation().validateUpdatePhoneNumberSuccessMsg();
    }




}