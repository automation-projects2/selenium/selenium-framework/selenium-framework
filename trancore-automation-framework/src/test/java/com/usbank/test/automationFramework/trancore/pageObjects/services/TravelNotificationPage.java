package com.usbank.test.automationFramework.trancore.pageObjects.services;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import static org.junit.Assert.assertEquals;

public class TravelNotificationPage<T extends WebDriver> {

    private T driver;

    public TravelNotificationPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.LINK_TEXT, using = "SERVICES")
    private WebElement lnkServicesPage;

    @FindBy(how = How.LINK_TEXT, using = "Travel notification")
    private WebElement lnkTravelNotification;

    @FindBy(how = How.XPATH, using = "//input[@id='startDate']")
    private WebElement datePickerStartDate;

    @FindBy(how = How.XPATH, using = "//input[@id='endDate']")
    private WebElement datePickerEndDate;

    @FindBy(how = How.XPATH, using = "//select[@id='destination']")
    private WebElement comboBoxDestination;

    /*@FindBy(how = How.XPATH, using = "//button[@id='cancel_button']")
    private WebElement btnCancelTravelNotification;*/

    @FindBy(how = How.XPATH, using = "//button[@id='save_button']")
    private WebElement btnSubmitTravelNotification;

    @FindBy(how = How.XPATH, using = "//td[contains(text(),'Your Travel Notification has been added.')]")
    private WebElement addTextTravelNotification;

  /*  @FindBy(how = How.XPATH, using = "//td[contains(text(),'A Travel Notification already exists for one or mo')]")
    private WebElement existsTextTravelNotification;*/

    /*@FindBy(how = How.XPATH, using = "//a[contains(text(),'Delete')]")
    private WebElement deleteTravelNotification;*/

    /*@FindBy(how = How.CSS, using = "div.elan-app div.middleWrapper:nth-child(3) div.layoutPageContent2:nth-child(2) div.layoutPageContent3 div.tc_page_shadow div.travelNotification div.singleBlock div.omv-travelNotification div.ng-modal:nth-child(2) div.ng-modal-dialog div.ng-modal-dialog-content p:nth-child(1) > input.tranCoreButton.buttonpad:nth-child(6)")
    private WebElement continueDeleteTravelNotification;*/

    @FindBy(how = How.XPATH, using = "//table[@class='tc_Table']")
    private WebElement tableTravelNotification;

    @FindBy(id = "subHeader")
    private WebElement lblTravelNotification;


    @FindBy(xpath = "//button[@type='submit']")
    private WebElement btnContinue;

    @FindBy(xpath = "//input[@title='CONTINUE']")
    private WebElement popupContinue;

    @FindBy(xpath = "//input[@title='BACK']")
    private WebElement popupBack;

    @FindBy(className = "errortext")
    private WebElement textMessage;
    public void clickOnServicesButton() {
        lnkServicesPage.click();
    }

    public WebElement getHeadingTravelNotification() {
        return lblTravelNotification;
    }

    public void clickOnLinkTravelNotification() {
        lnkTravelNotification.click();
    }

    public void addTravelNotification() throws Exception {

        //To print the headings, no of rows,columns In table
        List<WebElement> heading_table = tableTravelNotification.findElements(By.tagName("th"));
        List<WebElement> rows_table = tableTravelNotification.findElements(By.tagName("tr"));
        int rows_count = rows_table.size();
        int column_count = heading_table.size();
        System.out.println("-------------------------------------------------- ");
        System.out.println("Number of  Rows " + rows_count);
        System.out.println("Number of  Columns " + column_count);
        System.out.println("Headings in the table");

        for (int colhead = 0; colhead < column_count; colhead++) {
            String headtext = heading_table.get(colhead).getText();
            System.out.println("Column Heading " + colhead + " is " + headtext);
        }
        System.out.println("-------------------------------------------------- ");
        //Print contents of the table until the last row of table.
        List<Date> enddate_list = new ArrayList<Date>();
        String[] enddates = new String[rows_count - 1];
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        int index = 0;

        for (int row = 1; row < rows_count; row++) {

            //To locate columns(cells) of that specific row.
            List<WebElement> columns_row = rows_table.get(row).findElements(By.tagName("td"));
            enddates[index] = columns_row.get(1).getText();
            index++;

            //To calculate no of columns (cells). In that specific row.
            int columns_count = columns_row.size();
            System.out.println("Number of cells In Row " + row + " are " + columns_count);

            //Loop will execute till the last cell of that specific row.
            for (int column = 0; column < columns_count; column++) {
                // To retrieve text from that specific cell.
                String celltext = columns_row.get(column).getText();
                System.out.println("Cell Value of row number " + row + " and column number " + column + " Is " + celltext);
            }
            System.out.println("-------------------------------------------------- ");
        }

        /*Convert string to date and then supply to Start Date and End date fields*/
        System.out.println("No of Travel Notifications currently available is " + enddates.length);
        System.out.println("-------------------------------------------------- ");
        Date startDate = new Date();
        Date endDate = new Date();
        if (enddates.length != 0) {
            for (int i = 0; i < enddates.length; i++) {
                //Date date = new Date();
                endDate = formatter.parse(enddates[i]);
                enddate_list.add(endDate);
            }
            int maxi = 0;
            Date greatDate = enddate_list.get(0);
            while (enddate_list.size() - 1 > maxi) {
                if (enddate_list.get(maxi).getTime() < enddate_list.get(maxi + 1).getTime()) {
                    greatDate = enddate_list.get(maxi + 1);
                }
                maxi++;
            }
            Calendar startCal = Calendar.getInstance();
            startCal.setTime(greatDate);
            startCal.add(Calendar.DATE, 2);
            startDate = startCal.getTime();
            startCal.add(Calendar.DATE, 4);
            endDate = startCal.getTime();
            System.out.println("Start Date:" + formatter.format(startDate) + " ---Greater than the most recent date from the list of existing travel Notifications---");
            System.out.println("End Date:" + formatter.format(endDate));
            datePickerStartDate.sendKeys(formatter.format(startDate));
            datePickerEndDate.sendKeys(formatter.format(endDate));
            comboBoxDestination.sendKeys("Taiwan");
            btnSubmitTravelNotification.click();

        } else {
            Calendar Cal = Calendar.getInstance();
            Cal.add(Calendar.DATE, 2);
            startDate = Cal.getTime();
            Cal.add(Calendar.DATE, 4);
            endDate = Cal.getTime();
            datePickerStartDate.sendKeys(formatter.format(startDate));
            datePickerEndDate.sendKeys(formatter.format(endDate));
            comboBoxDestination.sendKeys("Taiwan");
            btnSubmitTravelNotification.click();

        }
        System.out.println("-------------------------------------------------- ");
        String travelNotificationTxt = "Your Travel Notification has been added.";
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(addTextTravelNotification));
        String bodyText = addTextTravelNotification.getText();

        if (bodyText.equals(travelNotificationTxt)) {
            System.out.println(bodyText);
        }
        System.out.println("-------------------------------------------------- ");
    }

    public void waitForPageLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(lblTravelNotification));
    }

    public void clickPopupBackButton() {
        popupBack.click();
    }

    public void clickPopupContinueButton() {
        popupContinue.click();
    }

    public void deleteAllNotification() {
        List<WebElement> linkDelete = driver.findElements(By.linkText("Delete"));
        System.out.println(linkDelete.size());
        for (int i = 0; i < linkDelete.size(); i++) {
            linkDelete.get(i).click();
            //WebElement popupContinueBtn = driver.findElement(By.xpath("//input[@value='CONTINUE']"));
            //popupContinueBtn.click();
            popupContinue.click();
            waitForPageLoad();
            assertEquals("Thank you.  Your travel notification has been deleted successfully.", textMessage.getText());

        }
    }
}



