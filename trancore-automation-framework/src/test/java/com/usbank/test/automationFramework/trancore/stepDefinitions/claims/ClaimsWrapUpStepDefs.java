package com.usbank.test.automationFramework.trancore.stepDefinitions.claims;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

public class ClaimsWrapUpStepDefs {

    @Autowired
    private StepDefinitionManager stepDefinitionManager;

    @And("I confirm that I made the purchase")
    public void i_confirm_that_I_made_the_purchase(){
        stepDefinitionManager.getPageObjectManager().getDisputePurchaseTypePage().clickRdbYes();

    }

    @And("I select Merchandise when asked for Dispute type and Continue")
    public void i_select_Merchandise_when_asked_for_Dispute_type_and_continue() {
        stepDefinitionManager.getPageObjectManager().getDisputePurchaseTypePage().selectRadioBtnMerchandise();
        stepDefinitionManager.getPageObjectManager().getDisputePurchaseTypePage().clickContinueButton();

    }

    @When("^I select dispute reason (.+)$")
    public void i_select_dispute_reason(String disputeReason) {

        stepDefinitionManager.getPageObjectManager().getDisputeReasonSelectionPage().selectDisputeReason(disputeReason);

    }

    @Then("I must see Wrap up - No Reason page")
    public void i_must_see_Wrap_up_No_Reason_page() {

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        stepDefinitionManager.getPageObjectManager().getWrapUpUnsupportedReason().waitForPageLoad();
        Assert.assertEquals(stepDefinitionManager.getPageObjectManager().getWrapUpUnsupportedReason().getComingSoonText(),"Coming soon");
    }
}
