@phonePrompt

Feature: User should get phone prompt once user enters username and password, when Phone prompt FF is ON


  Scenario Outline: User should get phone prompt once user enters username and password, when Phone prompt FF is ON
    Given the user has navigated to the <partner> login page
    And the user has logged into with <user_name> username and <password> password
    And the user should be directed to Please update your contact information Page
    And the user specifies <cell_phone> for Do you have a cell phone statement
    When the user clicks on Remind me next link at Contact Information Page
    Then the customer clicks on Logout Button
    And the customer should be directed to Login Page

    Examples:

     | partner | user_name  | password  | cell_phone | Reason  | Contact |

  Scenario Outline: User should get phone prompt once user enters username and password, when Phone prompt FF is OFF
    Given the user has navigated to the <partner> login page
    And the user has logged into with <user_name> username and <password> password
    Then the customer clicks on Logout Button
    And the customer should be directed to Login Page

    Examples:

      | partner | user_name  | password  | cell_phone | Reason  | Contact |



