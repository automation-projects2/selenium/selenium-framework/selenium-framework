package com.usbank.test.automationFramework.trancore.pageObjects.messageGranularity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ChargeOffPage {
    protected static final Logger logger = LogManager.getLogger(ChargeOffPage.class);

    protected WebDriver driver;

    public ChargeOffPage(WebDriver inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@id='chargeOffPageHeader']/span")
    private WebElement chargeOffPageHeader;

    @FindBy(how = How.ID, using="chargeOffPageDefinitionBody")
    private WebElement chargeOffPageDefinitionBody;

    @FindBy(how = How.ID, using="chargeOffPageMeaningHeader")
    private WebElement chargeOffPageMeaningHeader;

    @FindBy(how = How.ID, using="chargeOffPageMeaningBody")
    private WebElement chargeOffPageMeaningBody;


    public WebElement getChargeOffPageHeader() {return chargeOffPageHeader;}

    public WebElement getChargeOffPageDefinitionBody() {
        return chargeOffPageDefinitionBody;
    }

    public WebElement getChargeOffPageMeaningHeader() {
        return chargeOffPageMeaningHeader;
    }

    public WebElement getChargeOffPageMeaningBody() {
        return chargeOffPageMeaningBody;
    }

    public void waitForPageLoad(){
        WebDriverWait wait = new WebDriverWait(driver,15);
        wait.until(ExpectedConditions.visibilityOf(chargeOffPageHeader));
    }
}
