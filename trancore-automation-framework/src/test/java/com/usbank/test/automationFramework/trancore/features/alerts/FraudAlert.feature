# new feature
# Tags: optional
@Alert
Feature: Alerts functionality
  @FraudAlert
  Scenario Outline:Verify Signupnow link is working under alert tab.
    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    And I click the Alerts link
    Then click Fraud alert link present on Alertpage
    And click on update contact information
    Then verify the page title and click on cancel button

    Examples:
      | partner | user_name | password |

