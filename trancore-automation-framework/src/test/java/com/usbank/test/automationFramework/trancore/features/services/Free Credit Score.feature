@FreeCreditScore @CardmemberServicesSuite @WIP @Regression

Feature: Free Credit Score
  This test case ensures that Free Credit Score automation script is working

  Scenario Outline: Verify user can check his Free Credit Score
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When I click Services Tab
    And I click Link - Free Credit Score
    Then the user clicks on Continue Button within the Free Credit Score Pop-up window to leave Trancore Card Member Services page

    Examples:
      | partner       | testData |

  Scenario Outline: Verify user can navigate back from Free Credit Score pop-up
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When I click Services Tab
    And I click Link - Free Credit Score
    Then the user clicks on Cancel button within the Free Credit Score Pop-up window to return to Trancore Card Member Services page

    Examples:
      | partner | testData |
