package com.usbank.test.automationFramework.trancore.pageObjects.profile;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.logging.Logger;

public class ChangePasswordPage<T extends WebDriver> {


    @FindBy(how = How.LINK_TEXT, using = "Profile")
    private WebElement lnkProfilePage;

    @FindBy(how = How.XPATH, using = "//a[@title='Change Your Password']")
    private WebElement lnkChangeYourPassword;

    @FindBy(how = How.ID, using = "challengeQuestion_lbl")
    private WebElement lblQuestion;

    @FindBy(how = How.ID, using = "answer")
    private WebElement txtAnswer;

    @FindBy(how = How.XPATH, using = "//button[@type='submit']")
    private WebElement btnSubmit;

    @FindBy(how = How.XPATH, using = "//input[@name='oldPassword']")
    private WebElement txtOldPassword;

    @FindBy(how = How.XPATH, using = "//input[@name='newPassword']")
    private WebElement txtNewPassword;

    @FindBy(how = How.XPATH, using = "//input[@name='confirmPassword']")
    private WebElement txtConfirmPassword;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Thank you.  Your request has been processed.')]")
    private WebElement vrbSuccessMessage;

    @FindBy(how = How.XPATH, using = "//button[@name='Return to Profile']")
    private WebElement btnReturnToServices;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Profile')]")
    private WebElement profilePage;

    @FindBy(how = How.XPATH, using = "//td[contains(text(),'Please enter the current Password.')]")
    private WebElement VrbOldPasswordBlank;

    @FindBy(how = How.XPATH, using = "//td[contains(text(),'Please enter your new Password.')]")
    private WebElement VrbNewPasswordBlank;

    @FindBy(how = How.XPATH, using = "//td[contains(text(),'The Passwords you entered do not match. Please re-enter the passwords.')]")
    private WebElement VrbConfirmNewPasswordBlank;

    @FindBy(how = How.XPATH, using = "//td[contains(text(),'The New Password entered')]")
    private WebElement VrbSamePassword;

    @FindBy(how = How.XPATH, using = "//td[contains(text(),'Passwords must be')]")
    private WebElement VrbNotMeetingPasswordStandards;

    @FindBy(how = How.XPATH, using = "//td[contains(text(),'We are unable')]")
    private WebElement VrbWrongOldPassword;

    @FindBy(how = How.XPATH, using = "//td[contains(text(),'Passwords can contain only')]")
    private WebElement VrbPasswordWithSpace;

    @FindBy(how = How.XPATH, using = "//td[contains(text(),'Passwords cannot be')]")
    private WebElement VrbSameNewAndOldPassword;


    private T driver;

    private static Logger log = Logger.getLogger(ChangePasswordPage.class.getName());

    public ChangePasswordPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public void clickProfileLink() {
        lnkProfilePage.click();
    }

    public WebElement getProfilePage() {
        return lnkProfilePage;
    }

    public void clickChangePasswordlink() {
        lnkChangeYourPassword.click();
    }

    public WebElement getChangeYourPasswordlink() {
        return lnkChangeYourPassword;
    }

    public String getShieldQuestionAnswer() {

        String questionText = lblQuestion.getText();
        String lastWord = questionText.substring(questionText.lastIndexOf(" ") + 1, questionText.length() - 1);
        txtAnswer.sendKeys(lastWord);
        btnSubmit.click();
        return lastWord;

    }

    public void enterCurrentPassword(String currentPassword) {
        txtOldPassword.sendKeys(currentPassword);
    }

    public WebElement getOldPassword() {
        return txtOldPassword;
    }

    public void enterNewPassword(String newPassword) {
        txtNewPassword.sendKeys(newPassword);
    }

    public WebElement getNewPassword() {
        return txtNewPassword;
    }

    public void enterConfirmPassword(String confirmPassword) {
        txtConfirmPassword.sendKeys(confirmPassword);
    }

    public WebElement getConfirmNewPassword() {
        return txtConfirmPassword;
    }

    public void clickSubmitButton() {
        btnSubmit.click();
    }

    public WebElement getBtnSubmit() {
        return btnSubmit;
    }

    public void checkSuccessMessage() {
        WebDriverWait wait = new WebDriverWait(driver, 30);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Thank you.  Your request has been processed.')]")));
        String message = vrbSuccessMessage.getText();
        Assert.assertEquals("Your assert condition is failed", "Thank you.  Your request has been processed.", message);
        log.info("PASSED: Password is changed successfully");
    }

    public void clickreturnToServicesButton() {
        btnReturnToServices.click();
        String txtProfile = profilePage.getText();

        if (txtProfile.equalsIgnoreCase("Profile")) {
            log.info("Successfully returned to Profile page");
        } else {
            log.info("Returned to Profile page is not successful");
        }
    }

    public void checkErrorMessageForBlankCurrentPassword() {
        String blankCurrentPassword = VrbOldPasswordBlank.getText();
        Assert.assertEquals("Your assert condition is failed", "Please enter the current Password.", blankCurrentPassword);
        log.info("PASSED: Please enter the current Password.");
    }

    public void checkErrorMessageForBlankNewPassword() {
        String blankNewPassword = VrbNewPasswordBlank.getText();
        Assert.assertEquals("Your assert condition is failed", "Please enter your new Password.", blankNewPassword);
        log.info("PASSED: Please enter your new Password.");
    }

    public void checkErrorMessageForBlankConfirmNewPassword() {
        String blankConfirmNewPassword = VrbConfirmNewPasswordBlank.getText();
        Assert.assertEquals("Your assert condition is failed", "The Passwords you entered do not match. Please re-enter the passwords.", blankConfirmNewPassword);
        log.info("PASSED: The Passwords you entered do not match. Please re-enter the passwords.");
    }

    public void checkErrorMessageForWrongCurrentPassword() {
        String wrongCurrentPassword = VrbWrongOldPassword.getText();
        Assert.assertEquals("Your assert condition is failed", "We are unable to verify the information you've entered.  Please review your entry or call Technical Support at 1-877-334-0460 for assistance.", wrongCurrentPassword);
        log.info("PASSED: We are unable to verify the information you've entered.  Please review your entry or call Technical Support at 1-877-334-0460 for assistance.");
    }

    public void checkErrorMessageForShortlengthNewPassword() {
        String ShortlengthNewPassword = VrbNotMeetingPasswordStandards.getText();
        Assert.assertEquals("Your assert condition is failed", "Passwords are case sensitive. Passwords must be 8 - 24 characters in length, include at least one alpha character, and at least one numeric or special character.  You cannot include: spaces, three of the same characters in a row, your Personal ID or the word \"password\".  You cannot reuse a previously used password.", ShortlengthNewPassword);
        log.info("PASSED: Passwords are case sensitive. Passwords must be 8 - 24 characters in length, include at least one alpha character, and at least one numeric or special character.  You cannot include: spaces, three of the same characters in a row, your Personal ID or the word \"password\".  You cannot reuse a previously used password.");
    }

    public void checkErrorMessageForOnlyCharactersNewPassword() {
        String CharactersNewPassword = VrbNotMeetingPasswordStandards.getText();
        Assert.assertEquals("Your assert condition is failed", "Passwords are case sensitive. Passwords must be 8 - 24 characters in length, include at least one alpha character, and at least one numeric or special character.  You cannot include: spaces, three of the same characters in a row, your Personal ID or the word \"password\".  You cannot reuse a previously used password.", CharactersNewPassword);
        log.info("PASSED: Passwords are case sensitive. Passwords must be 8 - 24 characters in length, include at least one alpha character, and at least one numeric or special character.  You cannot include: spaces, three of the same characters in a row, your Personal ID or the word \"password\".  You cannot reuse a previously used password.");
    }

    public void checkErrorMessageForOnlySpecialCharactersNewPassword() {
        String SpecialCharactersNewPassword = VrbNotMeetingPasswordStandards.getText();
        Assert.assertEquals("Your assert condition is failed", "Passwords are case sensitive. Passwords must be 8 - 24 characters in length, include at least one alpha character, and at least one numeric or special character.  You cannot include: spaces, three of the same characters in a row, your Personal ID or the word \"password\".  You cannot reuse a previously used password.", SpecialCharactersNewPassword);
        log.info("PASSED: Passwords are case sensitive. Passwords must be 8 - 24 characters in length, include at least one alpha character, and at least one numeric or special character.  You cannot include: spaces, three of the same characters in a row, your Personal ID or the word \"password\".  You cannot reuse a previously used password.");
    }

    public void checkErrorMessageForOnlyNumericNewPassword() {
        String NumericNewPassword = VrbNotMeetingPasswordStandards.getText();
        Assert.assertEquals("Your assert condition is failed", "Passwords are case sensitive. Passwords must be 8 - 24 characters in length, include at least one alpha character, and at least one numeric or special character.  You cannot include: spaces, three of the same characters in a row, your Personal ID or the word \"password\".  You cannot reuse a previously used password.", NumericNewPassword);
        log.info("PASSED: Passwords are case sensitive. Passwords must be 8 - 24 characters in length, include at least one alpha character, and at least one numeric or special character.  You cannot include: spaces, three of the same characters in a row, your Personal ID or the word \"password\".  You cannot reuse a previously used password.");
    }

    public void checkErrorMessageNewPasswordWithSpace() {
        String NewPasswordWithSpace = VrbPasswordWithSpace.getText();
        Assert.assertEquals("Your assert condition is failed", "Passwords can contain only the following special characters !@#$%^&*()-~=+ ;:,./?[]{}|", NewPasswordWithSpace);
        log.info("PASSED: Passwords can contain only the following special characters !@#$%^&*()-~=+ ;:,./?[]{}|");
    }

    public void checkErrorMessageNewPasswordContainsPasswordWord() {
        String NewPasswordContainsPasswordWord = VrbNotMeetingPasswordStandards.getText();
        Assert.assertEquals("Your assert condition is failed", "Passwords are case sensitive. Passwords must be 8 - 24 characters in length, include at least one alpha character, and at least one numeric or special character.  You cannot include: spaces, three of the same characters in a row, your Personal ID or the word \"password\".  You cannot reuse a previously used password.", NewPasswordContainsPasswordWord);
        log.info("PASSED: Passwords are case sensitive. Passwords must be 8 - 24 characters in length, include at least one alpha character, and at least one numeric or special character.  You cannot include: spaces, three of the same characters in a row, your Personal ID or the word \"password\".  You cannot reuse a previously used password.");
    }

    public void checkErrorMessageNewAndCurrentPasswordSame() {
        String NewAndCurrentPasswordSame = VrbSameNewAndOldPassword.getText();
        Assert.assertEquals("Your assert condition is failed", "Passwords cannot be reused. The New Password entered has been recently used. Please re-enter the New Password.", NewAndCurrentPasswordSame);
        log.info("PASSED: Passwords cannot be reused. The New Password entered has been recently used. Please re-enter the New Password.");
    }


}
