package com.usbank.test.automationFramework.trancore.pageObjects.claims;

import com.usbank.test.automationFramework.trancore.pageObjects.waitForPageToLoad.WaitForPageToLoad;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PurchaseDetailsPage<T extends WebDriver> {
    @FindBy(how = How.XPATH, using = "//h1[@id='servicingAppHeader']")
    private WebElement lblPurchaseDetailsHeader;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Please explain what you expected to receive from t')]")
    private WebElement vrbExplainWhatYouExpectedText;

    @FindBy(how = How.XPATH, using = "//p[contains(text(),'Knowing the details of your purchase, such as bran')]")
    private WebElement vrbKnowingTheDetails;

    @FindBy(how = How.XPATH, using = "//textarea[@id='28']")
    private WebElement txtContentArea;
    @FindBy(how = How.XPATH, using = "//textarea[@id='27']")
    private WebElement txtArea;

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Continue')]")
    private WebElement btnContinueOnPurchaseDetailsPage;

    //foe deposit fee page
    @FindBy(how = How.XPATH, using = "//input[@id='38-Y']")
    private WebElement rdbtnYes;
    @FindBy(how = How.XPATH, using = "//input[@id='38-N']")
    private WebElement rdbtnNo;


    private T driver;

    WaitForPageToLoad callPageLoadMethod;

    private static final Logger log = LogManager.getLogger(ContactingMerchantPage.class.getName());

    public PurchaseDetailsPage(T inDriver) {
        driver = inDriver;
        callPageLoadMethod = new WaitForPageToLoad(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getLblPurchaseDetailsHeader() {
        return lblPurchaseDetailsHeader;
    }

    public void pleaseExplainWhatYouExpectedText() {
        log.info(vrbExplainWhatYouExpectedText.getText());
        log.info(vrbKnowingTheDetails.getText());
    }

    public void textAreaInput() {
        txtContentArea.sendKeys("example");
    }
    public void purchaseDetailArea() {
        txtArea.sendKeys("example");
    }

    public void clickOnContinueBtnOnPurchaseDetailsPage() {
        btnContinueOnPurchaseDetailsPage.click();
    }

    public void purchaseDetailsPagePBOMFlow() throws InterruptedException {
        log.info("**************** Purchase Details Page Reasoning One ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblPurchaseDetailsHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblPurchaseDetailsHeader, 5);
        pleaseExplainWhatYouExpectedText();
        textAreaInput();
        clickOnContinueBtnOnPurchaseDetailsPage();
    }

    public void purchaseDetailsPagePBOMFlowOne() throws InterruptedException {
        log.info("**************** Purchase Details Page Reasoning One ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblPurchaseDetailsHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblPurchaseDetailsHeader, 5);
        pleaseExplainWhatYouExpectedText();
        purchaseDetailArea();
        clickOnContinueBtnOnPurchaseDetailsPage();
    }


    public void depositOrFee() throws InterruptedException{
        log.info("******** Deposit of Fee page *******");
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(rdbtnYes));
        rdbtnYes.click();
        clickOnContinueBtnOnPurchaseDetailsPage();
    }
}

