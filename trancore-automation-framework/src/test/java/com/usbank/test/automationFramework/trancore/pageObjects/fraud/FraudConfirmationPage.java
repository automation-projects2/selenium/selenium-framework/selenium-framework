package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FraudConfirmationPage<T extends WebDriver> {
    protected static final Logger logger = LogManager.getLogger(ReviewPage.class);

    protected T driver;

    public FraudConfirmationPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "servicingAppHeader")
    private WebElement headingFraudConfirmation;

    @FindBy(xpath = "//*[@id=\"returnToOrigination\"]/a")
    private WebElement transactionDetailsLink;

    public void waitForConfirmationPageToLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(headingFraudConfirmation));
    }

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        ele.click();
    }

    public WebElement getHeader() {
        return headingFraudConfirmation;
    }

    public WebElement getTransactionDetailsLink() {
        return transactionDetailsLink;
    }

}

