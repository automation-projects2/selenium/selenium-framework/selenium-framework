package com.usbank.test.automationFramework.trancore.pageObjects.otp;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

public class EnterOTPPage<T extends WebDriver> {
    private static final Logger logger = LogManager.getLogger(OTPScreenPage.class);

    private static final long EXPLICIT_WAIT_TIME_SECONDS = 30;

    private T driver;

    @FindBy(how = How.TAG_NAME, using = "h1")
    private WebElement OtpHeader;

    @FindBy(how = How.CLASS_NAME, using = "errortext")
    private WebElement ErrorMsg;

    @FindBy(how = How.ID, using = "returnToServices")
    private WebElement buttonCancel;

    @FindBy(how = How.ID, using = "sendANewCode")
    private WebElement buttonSendANewCode;

    @FindBy(how = How.ID, using = "otpAndContinue")
    private WebElement buttonContinue;

    @FindBy(how = How.ID, using = "otp-value")
    private WebElement textfieldEnterOTP;


    public EnterOTPPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    public void ClickCancel() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(buttonContinue));
        buttonCancel.click();

    }


    public void ClickSendANewCode() {
        buttonSendANewCode.click();
    }

    public void ClickContinue() {
        buttonContinue.click();
    }

    public void SetEnterOTP(String setValue) {
        textfieldEnterOTP.clear();
        textfieldEnterOTP.sendKeys(setValue);
    }

    public void VerifyHeaderName() {

        String headername = OtpHeader.getText();
        assertEquals("one-time passcode", headername.toLowerCase());
    }

    public void VerifyTextField() {
        if (textfieldEnterOTP.isDisplayed()) {
            logger.info("Enter OTP text field is enabled or displayed");
        } else {
            logger.error("text field is not enabled or displayed");
        }
    }

    public void VerifySendanewcodeButton() {
        assertEquals("Send a new code", buttonSendANewCode.getText());

    }

    public void verifyInvalidOTPMsg() {

        assertEquals("Please enter the 6-digit code we sent. The code is numbers only, no other characters are allowed.", ErrorMsg.getText());
    }

    public void VerifyCancelButton() {
        assertEquals("Cancel", buttonCancel.getText());
    }

    public void VerifyContinueButton() {
        assertEquals("Continue", buttonContinue.getText());

    }

    public void verifyMismatchOTPMsg() {

        assertEquals("It looks like the code you entered doesn't match the one we sent. Can you try again?", ErrorMsg.getText());

    }

    public void enterOTPPageRefresh() {
        driver.navigate().refresh();
    }
}
        
    