package com.usbank.test.automationFramework.trancore.pageObjects.enrollinClicktoPay;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PayWithCardPage<T extends WebDriver> {

    private T driver;

    @FindBy(how = How.XPATH, using = "//*[@id=\"subHeader\"]/span')]")
    WebElement clicktopaywithcard;

    @FindBy(how = How.XPATH, using = "//button[@title='ENROLL']")
    WebElement enrollButton;

    @FindBy(how = How.NAME, using = "ENROLL")
    WebElement btnEnroll;

    public PayWithCardPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public void clickOnEnrollBtn() {
        Assert.assertTrue(btnEnroll.isDisplayed());
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@name='ENROLL']"))).click();
    }

    public WebElement getBtnEnroll() {
        return btnEnroll;
    }
}
