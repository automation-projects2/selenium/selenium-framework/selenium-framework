package com.usbank.test.automationFramework.trancore.pageObjects.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class AddAuthorizedUserPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(AddAuthorizedUserPage.class);

    private static final long EXPLICIT_WAIT_TIME_SECONDS = 30;

    private T driver;

    @FindBy(how = How.TAG_NAME, using = "h2")
    private WebElement headername;

    @FindBy(how = How.ID, using = "Continue")
    private WebElement linkContinue;


    @FindBy(how = How.LINK_TEXT, using = "Cancel")
    private WebElement linkCancel;

    @FindBy(how = How.ID, using = "challengeQuestion_lbl")
    private WebElement lblQuestion;

    @FindBy(how = How.ID, using = "answer")
    private WebElement txtAnswer;

    @FindBy(how = How.XPATH, using = "//button[@class='tranCoreButton buttonpad omv_button--full']")
    private WebElement btnSubmitSecurityQuestion;

    @FindBy(how = How.ID, using = "firstName")
    private WebElement txtFirstName;

    @FindBy(how = How.ID, using = "middleName")
    private WebElement txtMiddleName;

    @FindBy(how = How.ID, using = "lastName")
    private WebElement txtLastName;

    @FindBy(how = How.ID, using = "nameSuffix")
    private WebElement lstSuffix;

    @FindBy(how = How.ID, using = "ssn")
    private WebElement txtSSN;

    @FindBy(how = How.ID, using = "dateOfBirth")
    private WebElement txtDateOfBirth;

    @FindBy(how = How.XPATH, using = "//p[contains(text(),'You agree to be solely responsible for all transac')]")
    private WebElement vrbDisclosure;

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Show More')]")
    private WebElement lnkShowMore;

    @FindBy(how = How.NAME, using = "continue")
    private WebElement btnContinueToReview;

    @FindBy(how = How.XPATH, using = "//div[@class='add-au-content-body add-au-omv-margin']")
    private WebElement vrbAuthorizedUserInfo;

    @FindBy(how = How.XPATH, using = "//button[@name='continue']")
    private WebElement btnSubmitAuthorizedUserRequest;

    public AddAuthorizedUserPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }


    public void verifyHeaderName() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(txtFirstName));
        assertEquals("new authorized user information", (headername.getText()).toLowerCase());
    }

    public void ClickContinue() {
        linkContinue.click();
    }

    public void ClickCancel() {
        linkCancel.click();
    }

    public void SelectReasonOrderANewCard(String Reason) {
        List<WebElement> selectReason = driver.findElements(By.className("label__text"));
        int i;
        for (i = 0; i < selectReason.size(); i++) {

            if ((selectReason.get(i).getText()).equals(Reason)) {
                selectReason.get(i).click();
                break;
            }
        }
    }

    public WebElement getSecurityQuestion() {
        return lblQuestion;
    }

    public void getShieldQuestionAnswer() {
        String questionText = lblQuestion.getText();
        String lastWord = questionText.substring(questionText.lastIndexOf(" ") + 1, questionText.length() - 1);
        txtAnswer.sendKeys(lastWord);
        btnSubmitSecurityQuestion.click();
    }

    public void enterFirstName(String firstName) {
        txtFirstName.click();
        txtFirstName.sendKeys(firstName);
    }

    public WebElement getTxtFirstName() {
        return txtFirstName;
    }

    public void enterMiddleName(String middleName) {
        txtMiddleName.click();
        txtMiddleName.sendKeys(middleName);
    }

    public WebElement getTxtMiddleName() {
        return txtMiddleName;
    }

    public void enterLastName(String lastName) {
        txtLastName.click();
        txtLastName.sendKeys(lastName);
    }

    public WebElement getTxtLastName() {
        return txtLastName;
    }

    public void selectSuffix(String suffix) {
        lstSuffix.click();

        List<WebElement> listItems = driver.findElements(By.id("nameSuffix"));

        for (WebElement item : listItems) {

            System.out.println(item.getText());
            System.out.println("*************This Is The End Of The List*************");
            Select drpdwnSuffix = new Select(driver.findElement(By.id("nameSuffix")));
            drpdwnSuffix.selectByVisibleText(suffix);

        }

    }

    public WebElement getLstSuffix() {
        return lstSuffix;
    }

    public void enterSSN(String ssn) {
        txtSSN.sendKeys(ssn);
    }

    public WebElement getTxtSSN() {
        return txtSSN;
    }

    public void enterDateOfBirth(String dateOfBirth) {
        txtDateOfBirth.sendKeys(dateOfBirth);
    }

    public WebElement getTxtDateOfBirth() {
        return txtDateOfBirth;
    }

    public void readDisclosure() {
        lnkShowMore.click();
        System.out.println(vrbDisclosure.getText());
    }

    public WebElement getLnkShowMore() {
        return lnkShowMore;
    }

    public void clickOnContinueTOReviewButton() {
        btnContinueToReview.click();
    }

    public WebElement getBtnContinueToReview() {
        return btnContinueToReview;
    }

    public WebElement getAuthorizedUserInfo() {

        return vrbAuthorizedUserInfo;
    }

    public void readAndSubmitAuthorizedUserInfo() {
        System.out.println(vrbAuthorizedUserInfo.getText());
        btnSubmitAuthorizedUserRequest.click();
    }
}