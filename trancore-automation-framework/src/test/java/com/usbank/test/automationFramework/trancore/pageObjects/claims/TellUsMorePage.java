package com.usbank.test.automationFramework.trancore.pageObjects.claims;

import com.usbank.test.automationFramework.trancore.pageObjects.waitForPageToLoad.WaitForPageToLoad;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TellUsMorePage<T extends WebDriver> {
    @FindBy(how = How.XPATH, using = "//h1[@id='servicingAppHeader']")
    private WebElement lblTellUsMorePageHeader;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),\"What is the exact amount you're disputing?\")]")
    private WebElement vrbExactAmountDisputing;

    @FindBy(how = How.XPATH, using = "//span[contains[text(),\"How much did you expect to be charged?\")]")
    private WebElement vrbHowMuchAmountDisputing;

    @FindBy(how = How.XPATH, using = "//input[@id='disputeAmount']")
    private WebElement txtDisputeAmount;

    @FindBy(how = How.XPATH, using = "//input[@id='disputeAmount']")
    private WebElement txtTransactionAmount;

    @FindBy(how = How.XPATH, using = "//input[@id='disputeAmount']")
    private WebElement txtChargeAmount;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Did you contact the merchant?')]")
    private WebElement vrbDidYouContactMerchant;

    @FindBy(how = How.XPATH, using = "//input[@id='Y']")
    private WebElement rbYes;

    @FindBy(how = How.XPATH, using = "//input[@id='N']")
    private WebElement rbNo;

    @FindBy(how = How.XPATH, using = "//input[@id='U']")
    private WebElement rbICantContactTheMerchant;

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Continue')]")
    private WebElement btnContinue;

    private T driver;
    @FindBy(how = How.XPATH, using = "//input[@id='noneApply']")
    private WebElement chkBoxNoneApply;


    WaitForPageToLoad callPageLoadMethod;

    private static final Logger log = LogManager.getLogger(TellUsMorePage.class.getName());

    public TellUsMorePage(T inDriver) {
        driver = inDriver;
        callPageLoadMethod = new WaitForPageToLoad(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getLblTellUsMorePageHeader() {
        return lblTellUsMorePageHeader;
    }

    public void whatIsTheExactAmountYouAreDisputing() {
        log.info(vrbExactAmountDisputing.getText());
    }

    public void howMuchAmountAreYouDisputing() {
        log.info(vrbHowMuchAmountDisputing.getText());
    }

    public void disputeTransactionAmount() {
        String amountDisputing = "0.99";
        txtTransactionAmount.sendKeys(amountDisputing);
    }
    public void chargeAmount() {
        String amountDisputing = "1.99";
        txtChargeAmount.sendKeys(amountDisputing);
    }

    public void didYouContactTheMerchant() {
        log.info(vrbDidYouContactMerchant.getText());
    }

    public void clickOnYes() {
        rbYes.click();
    }

    public void clickOnNo() {
        rbNo.click();
    }

    public void clickOnICantContactTheMerchant() {
        rbICantContactTheMerchant.click();
    }

    public void clickOnContinueBtnOnTellUsMorePage() {
        btnContinue.click();
    }

    public void tellUsMorePagePBOMFlowOne() throws InterruptedException {
        log.info("**************** Tell Us More Page Reasoning One ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblTellUsMorePageHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblTellUsMorePageHeader, 5);
       // whatIsTheExactAmountYouAreDisputing();
        disputeTransactionAmount();
        didYouContactTheMerchant();
        clickOnYes();
        clickOnContinueBtnOnTellUsMorePage();
    }

    public void tellUsMorePagePBOMFlowTwo() throws InterruptedException {
        log.info("**************** Tell Us More Page Reasoning Two ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblTellUsMorePageHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblTellUsMorePageHeader, 5);
        whatIsTheExactAmountYouAreDisputing();
        disputeTransactionAmount();
        didYouContactTheMerchant();
        clickOnNo();
        clickOnContinueBtnOnTellUsMorePage();
    }

    public void tellUsMorePagePBOMFlowThree() throws InterruptedException {
        log.info("**************** Tell Us More Page Reasoning Three ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblTellUsMorePageHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblTellUsMorePageHeader, 5);
        whatIsTheExactAmountYouAreDisputing();
        disputeTransactionAmount();
        didYouContactTheMerchant();
        clickOnICantContactTheMerchant();
        clickOnICantContactTheMerchant();
        clickOnContinueBtnOnTellUsMorePage();
    }

    public void tellUsMorePagePBOMFlowFour() throws InterruptedException {
        log.info("**************** Tell Us More Page Reasoning Four ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblTellUsMorePageHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblTellUsMorePageHeader, 5);
        didYouContactTheMerchant();
        clickOnNo();
        clickOnContinueBtnOnTellUsMorePage();
    }

    public void tellUsMorePagePBOMFlowFive() throws InterruptedException {
        log.info("**************** Tell Us More Page Reasoning Five ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblTellUsMorePageHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblTellUsMorePageHeader, 5);
        howMuchAmountAreYouDisputing();
        chargeAmount();
        didYouContactTheMerchant();
        clickOnYes();
        clickOnContinueBtnOnTellUsMorePage();
    }

    public void chooseNoneApply() throws InterruptedException{
        log.info (("************Merchant creidt found page******"));
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(chkBoxNoneApply));
        chkBoxNoneApply.click();
        clickOnContinueBtnOnTellUsMorePage();
    }
}
