package com.usbank.test.automationFramework.trancore.stepDefinitions.googlePay;

import com.usbank.test.automationFramework.trancore.pageObjects.samsungPay.SamsungPayPushProvision;
import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.junit.Cucumber;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

@RunWith(Cucumber.class)

public class GooglePay {
    @Autowired
    StepDefinitionManager stepDefinitionManager;
    public static boolean gpayElementPresent = true;
    public static boolean gpayMultipleContactType = true;


    private static final Logger logger = LogManager.getLogger(GooglePay.class);
    @Then("display google Pay link")
    public void displayGooglePayLink() {
        gpayElementPresent=  stepDefinitionManager.getPageObjectManager().getGooglePayPage().verifyGooglePayLink();
    }

    @And("Google Pay Image is displayed")
    public void googlePayImageIsDisplayed() {
        if(gpayElementPresent)
        stepDefinitionManager.getPageObjectManager().getGooglePayPage().verifyGooglePayImage();
    }

    @Then("do not display google Pay link and image")
    public void doNotDisplayGooglePayLinkAndImage() {
        stepDefinitionManager.getPageObjectManager().getGooglePayPage().verifyGooglePayLinkNotDisplayed();

    }

    @Then("display google Pay link and image for tenured user")
    public void displayGooglePayLinkAndImageForTenuredUser() {
        gpayElementPresent=  stepDefinitionManager.getPageObjectManager().getGooglePayPage().verifyGooglePayLink();
    }

    @And("the user is Business user")
    public void theUserIsBusinessUser() {

    }

    @And("the user is a tenured")
    public void theUserIsATenured() {

    }

    @Then("display google Pay link and image")
    public void displayGooglePayLinkAndImage() {
        gpayElementPresent=  stepDefinitionManager.getPageObjectManager().getGooglePayPage().verifyGooglePayLink();
        if(gpayElementPresent)
            stepDefinitionManager.getPageObjectManager().getGooglePayPage().verifyGooglePayImage();
    }

    @And("the user is a cosigner")
    public void theUserIsACosigner() {
    }

    @And("the user is eligible")
    public void theUserIsEligible() {
    }
    

//    @And("then select the account (.+)")
//    public void thenSelectTheAccountAccount(String accountHolder, String accountType) {
//        stepDefinitionManager.getPageObjectManager().getGooglePayPage().openSelectedAccount(accountHolder, accountType);
//    }

    @And("if the account is eligible")
    public void ifTheAccountIsEligible() {
    }

    @And("the user clicks on google pay link")
    public void theUserClicksOnGooglePayLink() {
        if(gpayElementPresent)
        stepDefinitionManager.getPageObjectManager().getGooglePayPage().clickOnGooglePayLink();

    }

    @Then("display push provision initiation screen to the user")
    public void displayPushProvisionInitiationScreenToTheUser() {
        if(gpayElementPresent)
        stepDefinitionManager.getPageObjectManager().getGooglePayPage().verifyPushProvisionInitiationScreen();
    }

    @When("the user is on Push Provision Initiation page of google pay")
    public void theUserIsOnPushProvisionInitiationPageOfGooglePay() throws IOException {
        stepDefinitionManager.getPageObjectManager().getWebNavigationPage().clickServicesLinkadded();
        stepDefinitionManager.getPageObjectManager().getCaptureScreenShots().takeScreenshot(" servicePage");
        gpayElementPresent = stepDefinitionManager.getPageObjectManager().getGooglePayPage().verifyGooglePayLink();
        if (gpayElementPresent) {
            stepDefinitionManager.getPageObjectManager().getGooglePayPage().verifyGooglePayImage();
            stepDefinitionManager.getPageObjectManager().getGooglePayPage().clickOnGooglePayLink();
            stepDefinitionManager.getPageObjectManager().getGooglePayPage().verifyPushProvisionInitiationScreen();
        }

    }

    @When("the user is on Secondary Authentication Screen of google pay")
    public void theUserIsOnSecondaryAuthenticationScreenOfGooglePay() throws IOException {
        stepDefinitionManager.getPageObjectManager().getWebNavigationPage().clickServicesLinkadded();
        stepDefinitionManager.getPageObjectManager().getCaptureScreenShots().takeScreenshot(" servicePage");
        gpayElementPresent = stepDefinitionManager.getPageObjectManager().getGooglePayPage().verifyGooglePayLink();
        if (gpayElementPresent) {
            stepDefinitionManager.getPageObjectManager().getGooglePayPage().verifyGooglePayImage();
            stepDefinitionManager.getPageObjectManager().getGooglePayPage().clickOnGooglePayLink();
            stepDefinitionManager.getPageObjectManager().getGooglePayPage().verifyPushProvisionInitiationScreen();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnContinueButton();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySecondaryAuthenticationScreen();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyContactInformationText();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyContactMethods();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyWarningInformationText();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySendAoneTimeCode();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyCancelButton();
        }
    }

    @When("the user is on OTP verification screen of the google pay")
    public void theUserIsOnOTPVerificationScreenOfTheGooglePay() throws IOException {
        this.theUserIsOnSecondaryAuthenticationScreenOfGooglePay();
        if (gpayElementPresent) {
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnSendAOneTimeCode(stepDefinitionManager.sessionCounter);
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySecondaryAuthenticationScreen();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyEnterOtpText();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyEnterSixDigitCodeHeading();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyInputFieldToEnterOtp();
            gpayMultipleContactType = stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySendANewCodeButton();
            if (gpayMultipleContactType) {
                stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyUseAnotherMethodButton();
            }
        }
    }

    @When("the user is on Secondary Authentication Screen of google pay for first attempt")
    public void theUserIsOnSecondaryAuthenticationScreenOfGooglePayForFirstAttempt() throws IOException {
        this.theUserIsOnOTPVerificationScreenOfTheGooglePay();
    }

    @And("verify send a new code button is hidden")
    public void verifySendANewCodeButtonIsHidden() {
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySendANewCodeButton();
    }

    @Then("the user is on Secondary Authentication Screen of google pay for second attempt")
    public void theUserIsOnSecondaryAuthenticationScreenOfGooglePayForSecondAttempt() throws IOException {
        this.theUserIsOnSecondaryAuthenticationScreenOfGooglePay();
        if (gpayElementPresent) {
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnSendAOneTimeCode(stepDefinitionManager.sessionCounter);
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySecondaryAuthenticationScreen();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyEnterOtpText();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyEnterSixDigitCodeHeading();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyInputFieldToEnterOtp();
        }
    }

    @When("the user is on OTP verification screen of google pay after second OTP is send")
    public void theUserIsOnOTPVerificationScreenOfGooglePayAfterSecondOTPIsSend() throws IOException {
        this.theUserIsOnOTPVerificationScreenOfTheGooglePay();
        if (gpayElementPresent) {
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnCancelLink();
            this.theUserIsOnSecondaryAuthenticationScreenOfGooglePayForSecondAttempt();
        }
    }

    @When("the user is on the services page and exceeds two OTP (.+)count")
    public void theUserIsOnTheServicesPageAndExceedsTwoOTPOtpCount(String otp) throws IOException {
        this.theUserIsOnOTPVerificationScreenOfTheGooglePay();
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnCancelLink();
        this.theUserIsOnSecondaryAuthenticationScreenOfGooglePayForSecondAttempt();
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnCancelLink();

    }

    @Then("display OTP Failure Wrap Up screen of google pay")
    public void displayOTPFailureWrapUpScreenOfGooglePay() {
        stepDefinitionManager.getPageObjectManager().getGooglePayPage().verifyOTPFailureWrapUpScreen();
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyOTPFailureWrapUpScreenMessage();
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyReturnToSerivcesLink();
    }
    @When("the user is on secondary authentication screen of google pay and enter the first code (.+) incorrectly for three times")
    public void theUserIsOnSecondaryAuthenticationScreenOfGooglePayAndEnterTheFirstCodeOtpIncorrectlyForThreeTimes(String otp) throws IOException {
        this.theUserIsOnOTPVerificationScreenOfTheGooglePay();
        if (gpayElementPresent) {
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().enterTheOTP(otp);
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnContinueButton();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().enterTheOTP(otp);
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnContinueButton();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().enterTheOTP(otp);
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnContinueButton();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyErrorMessageInSecondaryAuthentication();
        }
    }

    @And("the OTP code is sent to the user")
    public void theOTPCodeIsSentToTheUser() {
        if (gpayElementPresent)
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnSendAOneTimeCode(stepDefinitionManager.sessionCounter);
    }

    @Then("verify use a another method button is hidden for multiple contact user")
    public void verifyUseAAnotherMethodButtonIsHiddenForMultipleContactUser() {
        if (gpayElementPresent) {
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyUseAnotherMethodButton();
        }
    }

    @And("the user has added the card manually")
    public void theUserHasAddedTheCardManually() {
        
    }

    @Then("display google Pay link with card added status")
    public void displayGooglePayLinkWithCardAddedStatus() {
        stepDefinitionManager.getPageObjectManager().getGooglePayPage().verifyAddedStatus();
    }

    @Then("display info screen")
    public void displayInfoScreen() {
        stepDefinitionManager.getPageObjectManager().getGooglePayPage().verifyInfoScreen();
    }

    @And("verify the account type \\\"([^\\\"]*)\\\" and then select the account (.+)$")
    public void verifyTheAccountTypeAccounttypeAndThenSelectTheAccountAccountholder(String accountType, String accountHolder) {

        stepDefinitionManager.getPageObjectManager().getGooglePayPage().openSelectedAccount(accountType, accountHolder);
    }
}

