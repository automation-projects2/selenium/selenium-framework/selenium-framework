package com.usbank.test.automationFramework.trancore.stepDefinitions.manageEmployees;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

public class ManageEmployeesSteps {
    @Autowired
    StepDefinitionManager stepDefinitionManager;

    @And("the user clicks on the Add Employee button")
    public void theUserClicksOnTheAddEmployeeButton() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().getBtnAddEmployee().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().clickOnAddEmployeeButton();
    }

    @And("the user has landed on the Add Employee Information page")
    public void theUserHasLandedOnTheAddEmployeeInformationPage() {
        //Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().getLblAddEmployee().isDisplayed());
        //stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().addEmployeesInformation();
    }

    @And("^the user enters in the First Name (.+), Middle Name (.+) , Last Name (.+)$")
    public void theUserEntersInTheFirstNameMiddleNameLastName(String firstName, String middleName, String lastName) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().getTxtFirstName().isDisplayed());
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().getTxtMiddleName().isDisplayed());
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().getTxtLastName().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().enterFirstMiddleLastNames(firstName, middleName, lastName);
    }

    @And("^the user selects a suffix (.+)$")
    public void theUserSelectsASuffix(String suffix) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().getLstSuffix().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().selectASuffix(suffix);
    }

    @And("^the user enters in the SSN (.+)$")
    public void theUserEntersInTheSSNSSN(String sSN) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().getTxtSSN().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().enterSSN(sSN);
    }

    @And("^the user enters in the DateOfBirth (.+)$")
    public void theUserEntersInTheDateOfBirth(String dateOfBirth) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().getTxtDateOfBirth().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().enterDateOfBirth(dateOfBirth);
    }

    @And("^the user enters in the Work Phone Number (.+)$")
    public void theUserEntersInTheWorkPhoneNumber(String phoneNUmber) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().getTxtPhoneNumber().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().enterPhoneNumber(phoneNUmber);
    }

    @And("^the user enters in a Spending Limit (.+)$")
    public void theUserEntersInASpendingLimit(String spendingLimit) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().getTxtSpendingLimit().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().enterSpendingLimit(spendingLimit);
    }

    @And("the user clicks on the Continue button on the Add Employee Page")
    public void theUserClicksOnTheContinueButtonOnTheAddEmployeePage() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().getBtnContinue().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().clickContinueBtnOnAddEmployeePage();
    }

    @Then("the user verifies and Submits on the Review Employee Information page")
    public void theUserHasLandedAndVerifiesTheReviewEmployeeInformationPage() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().getVrbReviewEmployeeInformation().isDisplayed());
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().getBtnSubmit().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().reviewAndSubmitEmployeeInformation();
    }

    @Then("^the Manage Employees page/dashboard opens$")
    public void the_Manage_Employees_pageordashboard_opens() throws IOException {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().getEmployeeHeading().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().verifyManageEmployeeDashBoadDisplayed();
    }

    @Then("the Edit Spending Limit link displayed for AO with {int}")
    public void the_Edit_Spending_Limit_link_displayed_for_AO_with(Integer status) throws IOException {
        //Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().getLinkEditSpendingLimitForAO().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().verifyEditSpendingLimitLink(status);
        stepDefinitionManager.getPageObjectManager().getCaptureScreenShots().takeScreenshot(" employeePage");
    }

    @Then("the Edit Spending Limit link not displayed for AO with {int}")
    public void the_Edit_Spending_Limit_link_not_displayed_for_AO_with(Integer status) throws IOException {
        //Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().getLinkEditSpendingLimitForAO().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getManageEmployeesPage().verifyEditSpendingLimitLink(status);
        stepDefinitionManager.getPageObjectManager().getCaptureScreenShots().takeScreenshot(" employeePageWithMessage");
    }
}
