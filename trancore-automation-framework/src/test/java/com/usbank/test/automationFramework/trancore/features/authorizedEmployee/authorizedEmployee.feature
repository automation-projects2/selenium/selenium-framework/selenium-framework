
@authorizedEmployee
Feature: Authorized Employee

  @TC001
  Scenario Outline: When a user logged in with selected account AE, Credit Limit Increase is not displayed in Services tab
    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    When the user click the Services Link
    Then Credit Limit Increase link not present


    Examples:
      |partner  |user_name|password |

  @TC002
  Scenario Outline: When a user logged in with selected account other than AE, Credit Limit Increase is displayed in Services tab
    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    When the user click the Services Link
    Then Credit Limit Increase link present


    Examples:
      |partner  |user_name|password |
