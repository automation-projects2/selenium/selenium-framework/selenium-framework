package com.usbank.test.automationFramework.trancore.pageObjects.payments;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;


public class PaymentAccountAddViewDeletePage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(PaymentAccountAddViewDeletePage.class);

    private T driver;

    @FindBy(how = How.XPATH, using = "//div/button[text()='Add an Account']")
    private WebElement addAccount;

    @FindBy(how = How.XPATH, using = "//div/input[@id='checkingAccount_inpt']")
    private WebElement checkingOption;

    @FindBy(how = How.XPATH, using = "//div/input[@id='savingsAccount_inpt']")
    private WebElement savingsOption;

    @FindBy(how = How.XPATH, using = "//div/input[@name='routeNumber']")
    private WebElement routingNumber;

    @FindBy(how = How.XPATH, using = "//div/input[@name='routeNumberConfirm']")
    private WebElement routingNumberConfirm;

    @FindBy(how = How.XPATH, using = "//div/input[@name='number']")
    private WebElement accountNumber;

    @FindBy(how = How.XPATH, using = "//div/input[@name='numberConfirm']")
    private WebElement accountNumberConfirm;

    @FindBy(how = How.XPATH, using = "//div/button[@id='next']")
    private WebElement next;

    @FindBy(how = How.XPATH, using = "//h2[@id='subHeader']/span[text()='Set Up Payment Account - Step 4 of 4']")
    private WebElement pageHeader;

    @FindBy(how = How.XPATH, using = "//button[@id='submit']")
    private WebElement submit;

    @FindBy(how = How.XPATH, using = "//div[text()='The following account has been set up for payment to your credit card:']")
    private WebElement setUpAccountSuccessMessage;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'Payments')]")
    private WebElement paymentsButtonInAddFundingAccountPage4of4;


    //Delete payment account
    @FindBy(how = How.XPATH, using = "//*[@id=\"subHeader\"]/span")
    private WebElement viewPaymentAccountsLandingPage;

    @FindBy(how = How.XPATH, using = "//*[@data-testid='deleteAccountBack']")
    private WebElement deletePaymentAccountPopUpBackButon;

//    @FindBy(how = How.XPATH, using = "//*[@data-testid='deleteAccountContinue']")
//    private WebElement deletePaymentAccountPopUpContinueButon;

    @FindBy(how = How.XPATH, using = "//button[@title='Continue']")
    private WebElement deletePaymentAccountPopUpContinueButon;

    @FindBy(how = How.XPATH, using = "//a[@title='Delete']")
    private WebElement deleteFundingAccount;

    public WebElement getSetUpAccountSuccessMessage() {
        return setUpAccountSuccessMessage;
    }

    public PaymentAccountAddViewDeletePage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }


    public void clickAddAccount() {

        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(addAccount));
        addAccount.click();
    }

    public void selectAccountType(String accountType) {
        if (accountType.equals("savings"))
            savingsOption.click();

    }

    public void enterRoutingNumber(String routingNumberValue) {
        routingNumber.sendKeys(routingNumberValue);
    }

    public void confirmRoutingNumber(String routingNumberValue) {
        routingNumberConfirm.sendKeys(routingNumberValue);
    }

    public void enterAccountNumber(String accountNumberValue) {
        accountNumber.sendKeys(accountNumberValue);
    }

    public void confirmAccountNumber(String accountNumberValue) {
        accountNumberConfirm.sendKeys(accountNumberValue);
    }

    public void clickNext() {
        next.click();
    }

    public void clickSubmit() {
        submit.click();
    }

    public void clickPaymentsButtonInAddFundingAccountPage4of4() {
        paymentsButtonInAddFundingAccountPage4of4.click();
    }

    public void deleteFundingAcc() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(deleteFundingAccount));
        deleteFundingAccount.click();
    }


    public void verifyThePaymentAccountAddedIsPresentInTheViewPaymentAccounts(String accountType, String routingNum, String accountNum) {

        // formatting user input values to match table values
        String rouNum = "Ending in " + routingNum.substring(routingNum.length() - 4);
        String accNum = "Ending in " + accountNum.substring(accountNum.length() - 4);
        boolean MatchingRecordFound = false;

        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("layoutContentBody")));
        // Grab the table
        WebElement table = driver.findElement(By.id("layoutContentBody"));
        //Get number of rows in table
        int numOfRow = table.findElements(By.tagName("tr")).size();
        //Get number of columns In table.
        int numOfCol = driver.findElements(By.xpath("//*[@id='layoutContentBody']/div/table/tbody/tr[1]/td")).size();
        //divided Xpath In three parts to pass Row_count and Col_count values.
        String first_part = "//*[@id='layoutContentBody']/div/table/tbody/tr[";
        String second_part = "]/td[";
        String third_part = "]";
        //Loop through the rows and compare the values with the user input values
        for (int i = 1; i <= (numOfRow - 1); i++) {
            //Prepared final xpath of specific cell as per values of i and j.
            int j = 1; // refers to column value
            String final_xpath = first_part + i + second_part + j + third_part;
            //Will retrieve value from located cell and print It.
            String test_name = driver.findElement(By.xpath(final_xpath)).getText();
            if (test_name.equalsIgnoreCase(accountType) && (j <= numOfCol)) {
                j++;
                String final_xpath1 = first_part + i + second_part + j + third_part;
                String test_name1 = driver.findElement(By.xpath(final_xpath1)).getText();
                if (test_name1.equalsIgnoreCase(rouNum) && (j <= numOfCol)) {
                    j++;
                    String final_xpath2 = first_part + i + second_part + j + third_part;
                    String test_name2 = driver.findElement(By.xpath(final_xpath2)).getText();
                    if (test_name2.equalsIgnoreCase(accNum) && (j <= numOfCol)) {
                        MatchingRecordFound = true;
                    }

                }
            }
        }

        if (MatchingRecordFound)
            System.out.println(" Matching Payment Account record is found  " + MatchingRecordFound);


    }


    // Delete Payment Account Code

    public void viewPaymentAccountsLandingPage() {

        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOf(viewPaymentAccountsLandingPage));
        boolean a = viewPaymentAccountsLandingPage.isDisplayed();
        System.out.println("landed in View Payment Accounts Page" + a);
    }

    public void navigatingToTheMatchingPaymentAccountRecord(String accountType, String routingNum, String accountNum) throws InterruptedException {
        // formatting user input values to match table values
        String rouNum = "Ending in " + routingNum.substring(routingNum.length() - 4);
        String accNum = "Ending in " + accountNum.substring(accountNum.length() - 4);
        boolean noMatchingRecordFound = false;
        Thread.sleep(2000);
        // Grab the table
        WebElement table = driver.findElement(By.id("layoutContentBody"));
        //Get number of rows in table
        int numOfRow = table.findElements(By.tagName("tr")).size();
        //Get number of columns In table.
        int numOfCol = driver.findElements(By.xpath("//*[@id='layoutContentBody']/div/table/tbody/tr[1]/td")).size();
        //divided Xpath In three parts to pass Row_count and Col_count values.
        String first_part = "//*[@id='layoutContentBody']/div/table/tbody/tr[";
        String second_part = "]/td[";
        String third_part = "]";
        //Loop through the rows and compare the values with the user input values
        for (int i = 1; i <= (numOfRow - 1); i++) {
            //Prepared final xpath of specific cell as per values of i and j.
            int j = 1; // refers to column value
            String final_xpath = first_part + i + second_part + j + third_part;
            //Will retrieve value from located cell and print It.
            String test_name = driver.findElement(By.xpath(final_xpath)).getText();
            if (test_name.equalsIgnoreCase(accountType) && (j <= numOfCol)) {
                j++;
                String final_xpath1 = first_part + i + second_part + j + third_part;
                String test_name1 = driver.findElement(By.xpath(final_xpath1)).getText();
                if (test_name1.equalsIgnoreCase(rouNum) && (j <= numOfCol)) {
                    j++;
                    String final_xpath2 = first_part + i + second_part + j + third_part;
                    String test_name2 = driver.findElement(By.xpath(final_xpath2)).getText();
                    if (test_name2.equalsIgnoreCase(accNum) && (j <= numOfCol)) {
                        System.out.println("Matching payment account found and clicked on delete");
                        driver.findElement(By.xpath("(//a[@href='Delete'])[" + i + "]")).click();
                    } else {
                        noMatchingRecordFound = true;
                    }

                } else {
                    noMatchingRecordFound = true;
                }
            } else {
                noMatchingRecordFound = true;
            }
        }

        if (noMatchingRecordFound)
            System.out.println("Matching Payment Account record is NOT found " + noMatchingRecordFound);
    }

    public void clickContinueInTheDeletePaymentAccountDialog() {

        System.out.println("Entered delete payment account popup");
        WebDriverWait wait1 = new WebDriverWait(driver, 20);
        wait1.until(ExpectedConditions.elementToBeClickable(deletePaymentAccountPopUpContinueButon));
        deletePaymentAccountPopUpContinueButon.click();
        System.out.println("Deleted");

    }

    public void clickBackInTheDeletePaymentAccountDialog() {

        //System.out.println("Entered delete payment account popup");
        //WebDriverWait wait1=new WebDriverWait(driver, 20);
        //wait1.until(ExpectedConditions.elementToBeClickable(deletePaymentAccountPopUpBackButonButon));
        // deletePaymentAccountPopUpBackButonButon.click();
    }

    public void errorRoutingNumberfield() {
        System.out.println("Invalid routing number message is displayed");
    }

    public void errorAccountNumberfield() {
        System.out.println("Invalid Account number message is displayed");
    }

    public void errorSetupAccount() {
        System.out.println("The Payment relationship is already exist message is displayed");
    }

    public void clickAddAccounterror() {


        List<WebElement> noofaccounts = driver.findElements(By.xpath("//tbody//tr"));
        System.out.println(noofaccounts.size());
        if (noofaccounts.size() < 4) {
            WebDriverWait wait = new WebDriverWait(driver, 20);
            wait.until(ExpectedConditions.visibilityOf(addAccount));
            addAccount.click();
            System.out.println("user able to add an account and setup account page is displayed ");
        } else {
            try {
                Assert.assertFalse("addaccountbutton is not visible ", addAccount.isDisplayed());
            } catch (NoSuchElementException e) {
                System.out.println("Add account button is not present and already 4 funding accounts are present ");
            }

        }

    }

}








