package com.usbank.test.automationFramework.trancore.stepDefinitions.otpByPass;
//// Developed by Sanjay K (Line of Sight Team ) on 11/20/2020

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class otpByPass {

    @Autowired
    StepDefinitionManager stepDefinitionManager;

    @When("the user tries to navigate Recent Review Page with url")
    public void theUserTriesToNavigateRecentReviewPageWithUrlForPartner() {
        stepDefinitionManager.getPageObjectManager().getInternalErrorPage().navigateToRecentReviewTransactionPage();
    }


    @And("the user navigates to Internal Error Page")
    public void theUserNavigatesToInternalErrorPage() {
        stepDefinitionManager.getPageObjectManager().getInternalErrorPage().verifyHeaderName();
    }
    
    @When("the user tries to navigate Contact Information with url")
    public void theUserTriesToNavigateContactInformationPageWithUrlForPartner(){
              stepDefinitionManager.getPageObjectManager().getInternalErrorPage().navigateToUpdateContactInformationPage();
    }

    @When("the user tries to navigate Update Mailing Address Page with url")
    public void theUserTriesToNavigateUpdateMailingAddressPageWithUrl() {
        stepDefinitionManager.getPageObjectManager().getInternalErrorPage().navigateToUpdateMailingAddressPage();
    }

    @When("the user tries to navigate Add Email Address Page with url")
    public void theUserTriesToNavigateAddEmailAddressPageWithUrl() {
        stepDefinitionManager.getPageObjectManager().getInternalErrorPage().navigateToAddEmailAddressPage();
    }

    @When("the user tries to navigate Add Authorized Users Page with url")
    public void theUserTriesToNavigateAddAuthorizedUsersPageWithUrl() {
    	stepDefinitionManager.getPageObjectManager().getInternalErrorPage().navigateToAddAuthorizedAU();
    }
    
    @When("the user tries to navigate Card Replacement Delivery Options Page with url")
    public void theUserTriesToNavigateCardReplacementDeliveryOptionsPageWithUrl() {
    	stepDefinitionManager.getPageObjectManager().getInternalErrorPage().navigateToCardReplacementDeliveryOptionsPage();
    }
    
    @When("the user tries to navigate Card Replacement Select Users Page with url")
    public void theUserTriesToNavigateCardReplacementSelectUsersPageWithUrl() {
    	stepDefinitionManager.getPageObjectManager().getInternalErrorPage().navigateToCardReplacementSelectUsersPage();
    }
}
