package com.usbank.test.automationFramework.trancore.pageObjects.payPal;

import com.usbank.test.helper.CaptureScreenShots;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.function.Function;


public class ContactMethodPage<T extends WebDriver> {
    private static final Logger logger = LogManager.getLogger(ContactMethodPage.class);
    private T driver;
    public CaptureScreenShots captureScreenshots;
    public static int EXPLICIT_WAIT_TIME_SECONDS = 30;

    @FindBy(how = How.ID, using = "subHeader")
    WebElement pageHeading;

    @FindBy(how = How.XPATH, using = "//input[@name='OTP']")
    WebElement otpField;

    @FindBy(how = How.XPATH, using = "//button[@type='submit']")
    WebElement sendOneTimeCodeButton;

    @FindBy(how = How.XPATH, using = "//button[@title='Continue']")
    WebElement continueButton;

    // @FindBy(xpath = "//button[@type='submit']")
    //  WebElement continueButton;

    @FindBy(how = How.XPATH, using = "//table[@class='errortext']//following::td")
    WebElement errorMessage;

    @FindBy(how = How.XPATH, using = "//input[@name='OTP']")
    WebElement otpValue;

    @FindBy(how = How.XPATH, using = "//h1[@id ='subHeader']//span|//h1[@id ='subHeader']")
    WebElement passcodeFailHeading;

//    @FindBy(how=How.XPATH, using="//h1[text()='Log in with PayPal']")
//    WebElement payPalLogin;

    @FindBy(how = How.XPATH, using = "//button[text()='Send a new code']")
    WebElement sendANewCode;

//    @FindBy(how = How.XPATH, using = "//button[text()='Log In']")
//    WebElement payPalLogin;

    @FindBy(how = How.XPATH, using = "//button[@name='btnLogin']")
    WebElement payPalLogin;

    @FindBy(how = How.XPATH, using = "//input[@name='login_email']")
    WebElement payPalUserName;

    @FindBy(how = How.XPATH, using = "//input[@name='login_password']")
    WebElement payPalPassword;

    @FindBy(how = How.XPATH, using = "//a[@data-name='makePreferredPayment']")
    WebElement btnPreferred;

    @FindBy(how = How.XPATH, using = "//button[@name='choiceSelectSubmit']")
    WebElement btnpayPalchoiceSelectSubmit;

    @FindBy(how = How.XPATH, using = "//a[@data-name='choiceSelectSuccessDone']")
    WebElement btnPayPalChoiceSelectSuccessDone;

    @FindBy(how = How.XPATH, using = "//a[@title='Services' or@title='SERVICES']")
    private WebElement linkServicesPage;

    @FindBy(how = How.XPATH, using = "//a[text()='Log Out' or text()='LOG OUT']")
    private WebElement linkLogOut;

    @FindBy(how = How.XPATH, using = "//input[@name ='contactToSendOTP']")
    WebElement rdocontactMethod;


    String parentWindow;

    public ContactMethodPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public void verifyContactMethodPageHeading() throws IOException {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(sendOneTimeCodeButton));
        WebDriverWait wait = new WebDriverWait(driver, 1000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@type='submit']")));
        String headingText = pageHeading.getText();
//        captureScreenshots.takeScreenshot(" Contact Method page");
        if (headingText.equalsIgnoreCase("Which email or phone should we use to send a one-time code?")) {
//            selectContactMethod(contact);
            clickElement(driver, rdocontactMethod);
        }
        logger.info("PASSED: Contact Method page heading displayed successfully : " + headingText);

        ClickOnSendAOneTimeCodeButton();

    }

    public void selectContactMethod(String contact) {

//        List<WebElement> contactMethods = driver.findElements(By.xpath("//div[@class='paypal-radio-inputs']//label"));
//        List<WebElement> selectContact = driver.findElements(By.xpath("//input[@name='contactToSendOTP']"));
//        int count = contactMethods.size();
//        for (int i = 0; i < count; i++) {
//            if (contactMethods.get(i).getText().contains(contact)) {
//                selectContact.get(i).click();
//
//            }
//        }


    }

    public void ClickOnSendAOneTimeCodeButton() {
        sendOneTimeCodeButton.click();
        logger.info("PASSED: Send a One Time Code button is clicked successfully");

    }

    public void verifyEnterOTPpage() throws IOException {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(continueButton));
//        WebDriverWait wait = new WebDriverWait(driver, 1000);
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@title='Continue']")));
        String headingtext = pageHeading.getText();

//        captureScreenshots.takeScreenshot(" Contact Method page");
        if (headingtext.equalsIgnoreCase("Please enter the code we just sent.")) {
            logger.info("PASSED: " + headingtext + " page displayed successfully");
        } else
            logger.error("FAILED: '" + headingtext + "' page not displayed successfully");
    }

    public Boolean enterOTPAndVerifyMessage(String otp) throws IOException {
        String error = null;
        Boolean payPalFlag = false;
        Integer i = 0;


        do {
            parentWindow = driver.getWindowHandle();
            otpField.sendKeys(otp);
            // driver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
            //continueButton.click();
            int windowCount = driver.getWindowHandles().size();
            clickElement(driver, continueButton);
            // driver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
            //   WebDriverWait wait = new WebDriverWait(driver, 1000);

            if (otp.equals("123456")) {
                payPalFlag = true;
                waitForWindow(2000, windowCount + 1);
                logger.info("PASSED: User navigated to PayPal Window successfully");

            }
            if (!otp.equals("123456") && i < 2) {
                new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(errorMessage));
                error = errorMessage.getText();
                logger.info("PASSED: Error message:'" + error + "'");
            }
            i++;

        } while (error != null && i < 3);
        if (!otp.equals("123456")) {
            WebDriverWait wait = new WebDriverWait(driver, 1000);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@title='Return To Services']")));
            String heading = passcodeFailHeading.getText();
            logger.info("PASSED: '" + heading + "'");

            if (i == 3 && heading.equalsIgnoreCase("That code doesn’t match.")) {
                logger.info("PASSED: Passcode fail first error page:" + heading);
                sendANewCode.click();
                verifyContactMethodPageHeading();
                verifyEnterOTPpage();
                enterOTPAndVerifyMessage(otp);

            } else {
                if (heading.equalsIgnoreCase("We couldn’t match the codes we sent.")) {
                    logger.error("PASSED: Passcode fail second error page:" + heading);

                }

            }
        }
        return payPalFlag;

    }

    public void payPalFlowCompletion(String userName, String password) {
        WebDriverWait wait = new WebDriverWait(driver, 1000);
        ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
        System.out.println(newTab.size());

        logger.info("PASSED: Redirecting to PayPal Window");
        driver.switchTo().window(newTab.get(1));

        payPalUserName.clear();
        payPalUserName.sendKeys(userName);
        payPalPassword.sendKeys(password);
        payPalLogin.click();
        // Set your preferred way to pay
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnPreferred));
        btnPreferred.click();

        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(sendOneTimeCodeButton));
        sendOneTimeCodeButton.click();

//
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnPayPalChoiceSelectSuccessDone));
        btnPayPalChoiceSelectSuccessDone.click();

        ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
        driver.close();
        driver.switchTo().window(tabs2.get(driver.getWindowHandles().size() - 1));
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(linkServicesPage));
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(linkLogOut));

        logger.info("PASSED: PayPal E-2-E flow Completed");
    }

    //}
    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        ele.click();
    }

    public void waitForWindow(int max_sec_toWait, int noOfExpectedWindow) {

        WebDriverWait wait = new WebDriverWait(driver, 1000);
        wait.ignoring(NoSuchWindowException.class);
        Function<WebDriver, Boolean> function = new Function<WebDriver, Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                Set<String> handel = driver.getWindowHandles();
                if (handel.size() == noOfExpectedWindow)
                    return true;
                else
                    return false;
            }
        };
        wait.until(function);
    }

}