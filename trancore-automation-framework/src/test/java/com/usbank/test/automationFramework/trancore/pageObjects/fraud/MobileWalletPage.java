package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MobileWalletPage<T extends WebDriver> {
    protected static final Logger logger = LogManager.getLogger(MobileWalletPage.class);

    protected T driver;


    public MobileWalletPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement btnContinue;

    @FindBy(id = "5044-Y")
    private WebElement rdbYesFirstQuestion;

    @FindBy(id = "5045-Y")
    private WebElement rdbYesSecondQuestion;

    @FindBy(id = "5045-N")
    private WebElement rdbNoSecondQuestion;

    @FindBy(xpath = "//*[text()='This was a mobile wallet transaction.']")
    private WebElement lblMobileWallet;

    public WebElement getLblMobileWallet() {
        return lblMobileWallet;
    }

    public void waitForMobileWalletToLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(lblMobileWallet));
    }

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        ele.click();
    }

    public void clickFirstQuestionYesRadioButton() {
        waitForMobileWalletToLoad();
        clickElement(driver, rdbYesFirstQuestion);
    }

    /*public void clickFirstQuestionNoRadioButton()
    {
        waitForMobileWalletToLoad();
        clickElement(driver,rdbNoFirstQuestion);
    }
    public void clickSecondQuestionYesRadioButton()
    {
        waitForMobileWalletToLoad();
        clickElement(driver,rdbYesSecondQuestion);
    }*/
    public void clickSecondQuestionNoRadioButton() {
        waitForMobileWalletToLoad();
        clickElement(driver, rdbNoSecondQuestion);
    }

    public void clickContinue() {
        clickElement(driver, btnContinue);
    }
}

