package com.usbank.test.automationFramework.trancore.pageObjects.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

public class CardDeliveryPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(CardDeliveryPage.class);

    private static final long EXPLICIT_WAIT_TIME_SECONDS = 30;

    private T driver;

    @FindBy(how = How.ID, using = "subHeader")
    private WebElement headername;

    @FindBy(how = How.NAME, using = "cardDeliveryRadio")
    private WebElement radiobutton;

    @FindBy(how = How.ID, using = "cardDeliveryContinue ")
    private WebElement linkContinue;


    @FindBy(how = How.LINK_TEXT, using = "Cancel")
    private WebElement linkCancel;

    @FindBy(how = How.NAME, using = "cancel")
    private WebElement cancelButton;
    @FindBy(how = How.XPATH, using = "//button[text()='Yes, leave']")
    private WebElement yesILeaveButton;
    @FindBy(how = How.XPATH, using = "//input[@value='Standard delivery']")
    private WebElement standardDeliveryOption;
    @FindBy(how = How.NAME, using = "continue")
    private WebElement continueButton;

    public CardDeliveryPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }


    public void verifyHeaderName() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(radiobutton));
        if (radiobutton.isDisplayed()) {
            System.out.println(headername.getText());
            assertEquals("card delivery", (headername.getText()).toLowerCase());
        }


    }

    public void ClickContinue() {
        linkContinue.click();
    }

    public void ClickCancel() {
        linkCancel.click();
    }

    public void cancelButtonClick() {
        cancelButton.click();
    }

    public void ClickYesILeaveButton() {
        yesILeaveButton.click();
    }

    public void ClickStandardDeliveryOption() {
        standardDeliveryOption.click();
    }

    public void continueButtonClick() {
        continueButton.click();
    }


}

