@FraudLostStolen

Feature: Customer should card lost stole and choose to check for Fraud check

  Scenario Outline: Customer should card lost stole and choose to check for Fraud check to deactivate the card

    Given the user has navigated to the <partner> login page
    And the user has logged into with <user_name> username and <password> password
    And customer click on Service Tab
    And the customer clicks Report Card Lost or Stolen link in service Tab
    And the customer clicks on Continue in Report Card Lost or Stolen Page
    And the customer should be directed to the OTP Screen
    And the customer should select the contact <Contact> at Send OTP Page
    When the customer clicks on Send one-time passcode at Send OTP Page
    And the customer enters <passcode>passcode at Enter OTP Page
    And the customer clicks on Continue at Enter OTP Page
    Then the customer should be directed to the Recent Review Transaction Lost Stolen
    And the user clicks on the Continue Button at Recent Review Transaction Page
    And the user verifies the radio button error text at Recent Review Transaction Page
    And the user clicks on No, I see at least one purchase I didn't make radio button
    And the user clicks on the Continue Button and handles the popup at Recent Review Transaction Page
    And the user navigates through Fraud App Check page
    And the user navigates through Check Profile Info page by selecting No radio button
    And the customer clicks on Logout Button
    And the customer should be directed to Login Page

    Examples:
      | partner | user_name     | password  | passcode | Date  | Contact |