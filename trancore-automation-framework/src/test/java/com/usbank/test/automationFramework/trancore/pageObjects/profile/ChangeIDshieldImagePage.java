package com.usbank.test.automationFramework.trancore.pageObjects.profile;

import com.usbank.test.automationFramework.trancore.pageObjects.StepUpIdshieldPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ChangeIDshieldImagePage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(StepUpIdshieldPage.class);

    private T driver;

    @FindBy(xpath = "//span[text()='Change']")
    private WebElement btnChange;

    @FindBy(id = "radio0")
    private WebElement rdbFirstImage;

    @FindBy(id = "imagePhrase")
    private WebElement imagePhrase;

    @FindBy(id = "next")
    private WebElement btnNext;

    @FindBy(xpath = "//button[text()='Submit']")
    private WebElement btnSubmit;

    @FindBy(xpath = "//a[@title='Alerts']")
    private WebElement linkAlerts;

    public ChangeIDshieldImagePage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    public void clickButtonChange() {

        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.visibilityOf(btnChange));
        btnChange.click();
    }

    public void setImagePhrase(String input) {
        imagePhrase.sendKeys(input);
    }

    public void clickButtonNext() {
        btnNext.click();
    }

    public void clickFirstRadio() throws InterruptedException {

        Thread.sleep(3000);
        int counter = 0;
        while (!rdbFirstImage.isDisplayed() && counter < 50) {
            btnChange.click();
            counter++;
        }
        Thread.sleep(3000);
        rdbFirstImage.click();
    }

    public void clickButtonSubmit() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(btnSubmit));
        btnSubmit.click();
    }

    public void clickLinkAlerts() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(linkAlerts));
        linkAlerts.click();
    }
}
