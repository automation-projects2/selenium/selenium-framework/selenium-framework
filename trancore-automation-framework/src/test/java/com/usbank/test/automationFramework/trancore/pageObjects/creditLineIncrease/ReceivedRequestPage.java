package com.usbank.test.automationFramework.trancore.pageObjects.creditLineIncrease;

import com.usbank.test.automationFramework.trancore.pageObjects.services.AddAuthorizedUserPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.assertEquals;

public class ReceivedRequestPage<T extends WebDriver> {
    private static final Logger logger = LogManager.getLogger(AddAuthorizedUserPage.class);

    private static final long EXPLICIT_WAIT_TIME_SECONDS = 30;

    private T driver;

    @FindBy(how = How.TAG_NAME, using = "h1")
    private WebElement headerName;

    @FindBy(how = How.ID, using = "Cancel")
    private WebElement buttonCancel;


    public ReceivedRequestPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    public void verifyHeaderReviewYourRequest() {
        assertEquals("review your request.", (headerName.getText()).toLowerCase());

    }

    public void clickFinishAndReturntoServices() {
        Assert.assertTrue("Finish and Return to Services is not diaplyed",buttonCancel.isDisplayed());
        buttonCancel.click();
    }
}
