package com.usbank.test.automationFramework.trancore.stepDefinitions.claims;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

public class ClaimsSteps {
    @Autowired
    private StepDefinitionManager stepDefinitionManager;

    @When("the user is on the My Account Page and is able to view the Posted transactions and is able to Dispute This Transaction")
    public void theUserIsOnTheMyAccountPageAndIsAbleToViewThePostedTransactionsAndIsAbleToDisputeThisTransaction() {
        stepDefinitionManager.getPageObjectManager().getMyAccountPage().clickOnPostedBtn();
        stepDefinitionManager.getPageObjectManager().getMyAccountPage().viewAndClickDisputeTransactionLink();
    }


    @SuppressWarnings("DuplicateBranchesInSwitch")
    @Then("^the user proceeds through the Paid By Other Means (\\d+)")
    public void theUserProceedsThroughThePaidByOtherMeansFlow(int flow) throws InterruptedException {
        switch (flow) {
            case 1:
                //This is when user says YES on the TellUSMorePage
                stepDefinitionManager.getPageObjectManager().getDisputeThisTransactionPage().disputeThisTransactionPagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getDisputeThisTransactionPage().getLblDisputeThisTransactionHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getReasonForDisputePage().reasonForDisputePagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getReasonForDisputePage().getLblReasonForDisputeHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getTellUsMorePage().tellUsMorePagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getTellUsMorePage().getLblTellUsMorePageHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getContactingMerchantPage().contactingMerchantPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getContactingMerchantPage().getLblContactingMerchantHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPurchaseDetailsPage().purchaseDetailsPagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPurchaseDetailsPage().getLblPurchaseDetailsHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPaymentTypePage().paymentTypePagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPaymentTypePage().getLblPaymentTypeHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPurchaseDocumentPage().purchaseDocumentPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPurchaseDocumentPage().lblPurchaseDocumentationHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getDisputeCaseContactInfoPage().disputeCaseContactInfoPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getDisputeCaseContactInfoPage().getLblDisputeCaseContactInfoHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getReviewAndSubmitPage().reviewAndSubmitPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getReviewAndSubmitPage().getLblReviewAndSubmitHeader().isDisplayed());
                break;

            case 2:
                //This is when user says YES but, the DisputeCaseContactInfo page will change at the end
                stepDefinitionManager.getPageObjectManager().getDisputeThisTransactionPage().disputeThisTransactionPagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getDisputeThisTransactionPage().getLblDisputeThisTransactionHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getReasonForDisputePage().reasonForDisputePagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getReasonForDisputePage().getLblReasonForDisputeHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getTellUsMorePage().tellUsMorePagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getTellUsMorePage().getLblTellUsMorePageHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getContactingMerchantPage().contactingMerchantPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getContactingMerchantPage().getLblContactingMerchantHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPurchaseDetailsPage().purchaseDetailsPagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPurchaseDetailsPage().getLblPurchaseDetailsHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPaymentTypePage().paymentTypePagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPaymentTypePage().getLblPaymentTypeHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPurchaseDocumentPage().purchaseDocumentPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPurchaseDocumentPage().lblPurchaseDocumentationHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getDisputeCaseContactInfoPage().disputeCaseContactInfoPagePBOMFlowTwo();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getDisputeCaseContactInfoPage().getLblDisputeCaseContactInfoHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getReviewAndSubmitPage().reviewAndSubmitPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getReviewAndSubmitPage().getLblReviewAndSubmitHeader().isDisplayed());
                break;

            case 3:
                //this when user says NO
                stepDefinitionManager.getPageObjectManager().getDisputeThisTransactionPage().disputeThisTransactionPagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getDisputeThisTransactionPage().getLblDisputeThisTransactionHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getReasonForDisputePage().reasonForDisputePagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getReasonForDisputePage().getLblReasonForDisputeHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getTellUsMorePage().tellUsMorePagePBOMFlowTwo();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getTellUsMorePage().getLblTellUsMorePageHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getContactingMerchantPage().contactingMerchantPagePBOMFlowTwo();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getContactingMerchantPage().getLblContactingMerchantHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPurchaseDetailsPage().purchaseDetailsPagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPurchaseDetailsPage().getLblPurchaseDetailsHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPaymentTypePage().paymentTypePagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPaymentTypePage().getLblPaymentTypeHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPurchaseDocumentPage().purchaseDocumentPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPurchaseDocumentPage().lblPurchaseDocumentationHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getDisputeCaseContactInfoPage().disputeCaseContactInfoPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getDisputeCaseContactInfoPage().getLblDisputeCaseContactInfoHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getReviewAndSubmitPage().reviewAndSubmitPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getReviewAndSubmitPage().getLblReviewAndSubmitHeader().isDisplayed());
                break;

            case 4:
                //This is when user says I CANT CONTACT THE MERCHANT
                stepDefinitionManager.getPageObjectManager().getDisputeThisTransactionPage().disputeThisTransactionPagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getDisputeThisTransactionPage().getLblDisputeThisTransactionHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getReasonForDisputePage().reasonForDisputePagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getReasonForDisputePage().getLblReasonForDisputeHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getTellUsMorePage().tellUsMorePagePBOMFlowThree();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getTellUsMorePage().getLblTellUsMorePageHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getContactingMerchantPage().contactingMerchantPagePBOMFlowThree();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getContactingMerchantPage().getLblContactingMerchantHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPurchaseDetailsPage().purchaseDetailsPagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPurchaseDetailsPage().getLblPurchaseDetailsHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPaymentTypePage().paymentTypePagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPaymentTypePage().getLblPaymentTypeHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPurchaseDocumentPage().purchaseDocumentPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPurchaseDocumentPage().lblPurchaseDocumentationHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getDisputeCaseContactInfoPage().disputeCaseContactInfoPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getDisputeCaseContactInfoPage().getLblDisputeCaseContactInfoHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getReviewAndSubmitPage().reviewAndSubmitPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getReviewAndSubmitPage().getLblReviewAndSubmitHeader().isDisplayed());
                break;

            case 5:
                //this is for payment type ATM/DebitCard or the selection of Other CreditCard, Prepaid Card, Gift Card
                // and when the user clicks YES on the OtherCardUsedPage the flow changes
                // once on the VerifyOtherFormOfPaymentPage user can select three different reasons
                stepDefinitionManager.getPageObjectManager().getDisputeThisTransactionPage().disputeThisTransactionPagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getDisputeThisTransactionPage().getLblDisputeThisTransactionHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getReasonForDisputePage().reasonForDisputePagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getReasonForDisputePage().getLblReasonForDisputeHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getTellUsMorePage().tellUsMorePagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getTellUsMorePage().getLblTellUsMorePageHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getContactingMerchantPage().contactingMerchantPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getContactingMerchantPage().getLblContactingMerchantHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPurchaseDetailsPage().purchaseDetailsPagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPurchaseDetailsPage().getLblPurchaseDetailsHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPaymentTypePage().paymentTypePagePBOMFlowTwo();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPaymentTypePage().getLblPaymentTypeHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getOtherCardUsedPage().otherCardUsedPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getOtherCardUsedPage().getLblOtherCardUsedHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getVerifyOtherFormOfPaymentPage().verifyOtherFormOfPaymentPBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getVerifyOtherFormOfPaymentPage().lblVerifyOtherFormOfPaymentHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPurchaseDocumentPage().purchaseDocumentPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPurchaseDocumentPage().lblPurchaseDocumentationHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getDisputeCaseContactInfoPage().disputeCaseContactInfoPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getDisputeCaseContactInfoPage().getLblDisputeCaseContactInfoHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getReviewAndSubmitPage().reviewAndSubmitPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getReviewAndSubmitPage().getLblReviewAndSubmitHeader().isDisplayed());
                break;

            case 6:
                //this is for payment type ATM/DebitCard or the selection of Other CreditCard, Prepaid Card, Gift Card
                // and when the user clicks YES on the OtherCardUsedPage the flow changes
                // once on the VerifyOtherFormOfPaymentPage user can select three different reasons
                stepDefinitionManager.getPageObjectManager().getDisputeThisTransactionPage().disputeThisTransactionPagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getDisputeThisTransactionPage().getLblDisputeThisTransactionHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getReasonForDisputePage().reasonForDisputePagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getReasonForDisputePage().getLblReasonForDisputeHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getTellUsMorePage().tellUsMorePagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getTellUsMorePage().getLblTellUsMorePageHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getContactingMerchantPage().contactingMerchantPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getContactingMerchantPage().getLblContactingMerchantHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPurchaseDetailsPage().purchaseDetailsPagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPurchaseDetailsPage().getLblPurchaseDetailsHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPaymentTypePage().paymentTypePagePBOMFlowTwo();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPaymentTypePage().getLblPaymentTypeHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getOtherCardUsedPage().otherCardUsedPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getOtherCardUsedPage().getLblOtherCardUsedHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getVerifyOtherFormOfPaymentPage().verifyOtherFormOfPaymentPBOMFlowTwo();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getVerifyOtherFormOfPaymentPage().lblVerifyOtherFormOfPaymentHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPurchaseDocumentPage().purchaseDocumentPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPurchaseDocumentPage().lblPurchaseDocumentationHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getDisputeCaseContactInfoPage().disputeCaseContactInfoPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getDisputeCaseContactInfoPage().getLblDisputeCaseContactInfoHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getReviewAndSubmitPage().reviewAndSubmitPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getReviewAndSubmitPage().getLblReviewAndSubmitHeader().isDisplayed());
                break;

            case 7:
                //this is for payment type ATM/DebitCard or the selection of Other CreditCard, Prepaid Card, Gift Card
                // and when the user clicks YES on the OtherCardUsedPage the flow changes
                // once on the VerifyOtherFormOfPaymentPage user can select three different reasons
                stepDefinitionManager.getPageObjectManager().getDisputeThisTransactionPage().disputeThisTransactionPagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getDisputeThisTransactionPage().getLblDisputeThisTransactionHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getReasonForDisputePage().reasonForDisputePagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getReasonForDisputePage().getLblReasonForDisputeHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getTellUsMorePage().tellUsMorePagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getTellUsMorePage().getLblTellUsMorePageHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getContactingMerchantPage().contactingMerchantPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getContactingMerchantPage().getLblContactingMerchantHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPurchaseDetailsPage().purchaseDetailsPagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPurchaseDetailsPage().getLblPurchaseDetailsHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPaymentTypePage().paymentTypePagePBOMFlowTwo();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPaymentTypePage().getLblPaymentTypeHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getOtherCardUsedPage().otherCardUsedPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getOtherCardUsedPage().getLblOtherCardUsedHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getVerifyOtherFormOfPaymentPage().verifyOtherFormOfPaymentPBOMFlowThree();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getVerifyOtherFormOfPaymentPage().lblVerifyOtherFormOfPaymentHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPurchaseDocumentPage().purchaseDocumentPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPurchaseDocumentPage().lblPurchaseDocumentationHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getDisputeCaseContactInfoPage().disputeCaseContactInfoPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getDisputeCaseContactInfoPage().getLblDisputeCaseContactInfoHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getReviewAndSubmitPage().reviewAndSubmitPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getReviewAndSubmitPage().getLblReviewAndSubmitHeader().isDisplayed());
                break;

            case 8:
                //this is for payment type ATM/DebitCard or the selection of  Other CreditCard, Prepaid Card, Gift Card
                // and when the user clicks No on the OtherCardUsedPage the flow changes
                stepDefinitionManager.getPageObjectManager().getDisputeThisTransactionPage().disputeThisTransactionPagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getDisputeThisTransactionPage().getLblDisputeThisTransactionHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getReasonForDisputePage().reasonForDisputePagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getReasonForDisputePage().getLblReasonForDisputeHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getTellUsMorePage().tellUsMorePagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getTellUsMorePage().getLblTellUsMorePageHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getContactingMerchantPage().contactingMerchantPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getContactingMerchantPage().getLblContactingMerchantHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPurchaseDetailsPage().purchaseDetailsPagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPurchaseDetailsPage().getLblPurchaseDetailsHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPaymentTypePage().paymentTypePagePBOMFlowTwo();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPaymentTypePage().getLblPaymentTypeHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getOtherCardUsedPage().otherCardUsedPagePBOMFlowTwo();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getOtherCardUsedPage().getLblOtherCardUsedHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPurchaseDocumentPage().purchaseDocumentPagePBOMFlowTwo();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPurchaseDocumentPage().lblPurchaseDocumentationHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getDisputeCaseContactInfoPage().disputeCaseContactInfoPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getDisputeCaseContactInfoPage().getLblDisputeCaseContactInfoHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getReviewAndSubmitPage().reviewAndSubmitPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getReviewAndSubmitPage().getLblReviewAndSubmitHeader().isDisplayed());
                break;

            case 9:
                //this is for payment type Other
                stepDefinitionManager.getPageObjectManager().getDisputeThisTransactionPage().disputeThisTransactionPagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getDisputeThisTransactionPage().getLblDisputeThisTransactionHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getReasonForDisputePage().reasonForDisputePagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getReasonForDisputePage().getLblReasonForDisputeHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getTellUsMorePage().tellUsMorePagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getTellUsMorePage().getLblTellUsMorePageHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getContactingMerchantPage().contactingMerchantPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getContactingMerchantPage().getLblContactingMerchantHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPurchaseDetailsPage().purchaseDetailsPagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPurchaseDetailsPage().getLblPurchaseDetailsHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPaymentTypePage().paymentTypePagePBOMFlowThree();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPaymentTypePage().getLblPaymentTypeHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getOtherFormOfPaymentPage().otherFormOfPaymentPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getOtherFormOfPaymentPage().lblOtherFormOfPaymentHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPurchaseDocumentPage().purchaseDocumentPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPurchaseDocumentPage().lblPurchaseDocumentationHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getDisputeCaseContactInfoPage().disputeCaseContactInfoPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getDisputeCaseContactInfoPage().getLblDisputeCaseContactInfoHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getReviewAndSubmitPage().reviewAndSubmitPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getReviewAndSubmitPage().getLblReviewAndSubmitHeader().isDisplayed());
                break;

            default:
                stepDefinitionManager.getPageObjectManager().getDisputeThisTransactionPage().disputeThisTransactionPagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getDisputeThisTransactionPage().getLblDisputeThisTransactionHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getReasonForDisputePage().reasonForDisputePagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getReasonForDisputePage().getLblReasonForDisputeHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getTellUsMorePage().tellUsMorePagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getTellUsMorePage().getLblTellUsMorePageHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getContactingMerchantPage().contactingMerchantPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getContactingMerchantPage().getLblContactingMerchantHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPurchaseDetailsPage().purchaseDetailsPagePBOMFlow();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPurchaseDetailsPage().getLblPurchaseDetailsHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPaymentTypePage().paymentTypePagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPaymentTypePage().getLblPaymentTypeHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getPurchaseDocumentPage().purchaseDocumentPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getPurchaseDocumentPage().lblPurchaseDocumentationHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getDisputeCaseContactInfoPage().disputeCaseContactInfoPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getDisputeCaseContactInfoPage().getLblDisputeCaseContactInfoHeader().isDisplayed());
                stepDefinitionManager.getPageObjectManager().getReviewAndSubmitPage().reviewAndSubmitPagePBOMFlowOne();
                Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getReviewAndSubmitPage().getLblReviewAndSubmitHeader().isDisplayed());
                break;
        }
    }
}
