package com.usbank.test.automationFramework.trancore.pageObjects.payPal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AgreeAndContinuePage<T extends WebDriver> {
    private static final Logger logger = LogManager.getLogger(AgreeAndContinuePage.class);
    private T driver;
    public static int EXPLICIT_WAIT_TIME_SECONDS = 30;

    @FindBy(how = How.ID, using = "subHeader")
    WebElement agreeAndContinuePageheading;

    @FindBy(how = How.XPATH, using = "//button[text() ='Agree and continue']")
    WebElement agreeAndContinueButton;

    public AgreeAndContinuePage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public void verifyAgreeAndContinuePageDisplayed() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(agreeAndContinueButton));
        String headingText = agreeAndContinuePageheading.getText();
        Assert.assertTrue("MisMatch in header of Agree and Continue page", headingText.equalsIgnoreCase("Checkout is easy when you add your card to PayPal."));
        logger.info("PASSED: Agree and Continue page heading :" + headingText + "  displayed successfully ");
    }


    public void verifyAgreeAndContinueButton() {
        boolean button = agreeAndContinueButton.isDisplayed();
        if (button == true) {
            logger.info("PASSED: Agree and Continue Button is displayed");
        }

    }

    public void agreeAndContinueButtonClick() {
        agreeAndContinueButton.click();
        logger.info("PASSED: Agree and Continue button is clicked successfully");
    }

}



