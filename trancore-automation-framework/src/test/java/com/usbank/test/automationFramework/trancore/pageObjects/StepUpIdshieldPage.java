package com.usbank.test.automationFramework.trancore.pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StepUpIdshieldPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(StepUpIdshieldPage.class);

    private T driver;

    StepUpIdshieldPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "challengeQuestion_lbl")
    private WebElement securityQuestion;

    @FindBy(id = "answer")
    private WebElement answer;

    @FindBy(xpath = "//button[@value='Submit']")
    private WebElement submitButton;

    public WebElement getSecurityQuestion() {
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.visibilityOf(securityQuestion));
        return securityQuestion;
    }

    public WebElement getAnswer() {
        return answer;
    }

    public WebElement getSubmitButton() {
        return submitButton;
    }
}
