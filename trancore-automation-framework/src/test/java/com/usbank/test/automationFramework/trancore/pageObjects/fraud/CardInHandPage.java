package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CardInHandPage<T extends WebDriver> {

    protected static final Logger logger = LogManager.getLogger(CardInHandPage.class);

    protected T driver;


    public CardInHandPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//input[@type='checkbox']")
    private WebElement chkDonotRemember;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement btnContinue;

    @FindBy(xpath = "//*[text()= 'Last time you had your card']")
    private WebElement lblCardInHand;

    @FindBy(xpath = "//input[@id='5033']")
    private WebElement dateField;

    @FindBy(id = "5034")
    private WebElement txtArea;

    public WebElement getTxtArea() {
        return txtArea;
    }

    public WebElement getDateField() {
        return dateField;
    }

    public WebElement getLblCardInHand() {
        return lblCardInHand;
    }

    public void waitForCardInHandPageToLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 50);
        wait.until(ExpectedConditions.visibilityOf(lblCardInHand));
    }

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        ele.click();
    }

    public void clickDoNotRememberCheckbox() {
        waitForCardInHandPageToLoad();
        clickElement(driver, chkDonotRemember);
    }

    public void clickContinue() {
        clickElement(driver, btnContinue);
    }
}
