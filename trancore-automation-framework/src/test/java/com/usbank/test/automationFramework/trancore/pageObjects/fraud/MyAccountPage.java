package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class MyAccountPage<T extends WebDriver> {

    protected static final Logger logger = LogManager.getLogger(MyAccountPage.class);

    protected T driver;


    protected static final String DATE_PATTERN = "dd/MM/yy";

    protected static final String HTML_ID_POSTED_TABLE = "transactionDetailTable_completed";
    protected static final String HTML_ID_PENDING_TABLE = "transactionDetailTable_pending";
    protected static final String HTML_ID_RECURRING_TABLE = "transactionDetailTable_recurring";

    protected static final int INDEX_CELL_TXN_DATE = 0;
    protected static final int INDEX_CELL_PND_DESCRIPTION = 1;
    protected static final int INDEX_CELL_DESCRIPTION = 2;
    protected static final int INDEX_CELL_AMOUNT = 3;

    protected static final String TAB_NAME_POSTED = "posted";
    protected static final String TAB_NAME_PENDING = "pending";
    protected static final String TAB_NAME_RECURRING = "recurring";

    public MyAccountPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    @FindAll({
            @FindBy(how = How.LINK_TEXT, using = "MY ACCOUNT"),
            @FindBy(how = How.LINK_TEXT, using = "My Account")
    })
    private WebElement linkMyAccountPage;

    @FindBy(xpath = "//a[@href='/onlineCard/accountSummary.do']")
    private WebElement linkSelectAccountPage;

    @FindBy(id = "navOnlineStatementAnchor")
    private WebElement btnOnlineStatements;

    @FindBy(id = "navMakePaymentAnchor")
    private WebElement btnMakePayment;

    @FindBy(id = "tc_PostedTab")
    private WebElement tabPosted;

    @FindBy(id = "tc_PendingTab")
    private WebElement tabPending;

    @FindBy(id = "tc_RecurringTab")
    private WebElement tabRecurring;
    @FindAll({
            @FindBy(xpath = "//a[(@title = 'Account Activity')]"),
            @FindBy(xpath = "//a[(@title = 'MY ACCOUNT')]"),
            @FindBy(xpath = "//a[(@title = 'My Account')]"),
            @FindBy(xpath = "//a[(@title = 'Account')]"),
            @FindBy(xpath = "//a[(@title ='MY CREDIT CARD')]")
    })
    private WebElement tabMyAccount;

    @FindAll({
            @FindBy(linkText = "SERVICES"),
            @FindBy(linkText = "Services")
    })
    //@FindBy(linkText = "SERVICES")
    private WebElement linkServices;

    public static int EXPLICIT_WAIT_TIME_SECONDS = 30;

    @FindBy(xpath = "//a[@title='Alerts']")
    private WebElement linkAlerts;

    @FindBy(xpath = "//a[@title='Profile']")
    private WebElement linkProfile;

    @FindBy(xpath ="//a[@data-testid='paymentSolutionsLink']")
    private WebElement linkPaymentSolutions;

        @FindBy(xpath = "//*[@id='mgInformation-children-notification']/div/span[1]")
        private WebElement messageGranularityHeader;

        @FindBy(xpath = "//*[@id='mgInformation-children-notification']/div/span[2]")
        private WebElement messageGranularityBody;

        @FindBy(id = "mgInformation--close-btn")
        private WebElement messageGranularityBtn;

        @FindBy(xpath = "//*[@id='mgError-children-notification']/div/span")
        private WebElement messageGranularityErrorHeader;

        @FindBy(xpath = "//*[@id='mgError-children-notification']/div/p/span")
        private WebElement messageGranularityErrorBody;

        @FindBy(xpath = "//*[@id='mgError-children-notification']/div/div/a")
        private WebElement messageGranularityPaymentLink;

        @FindBy(xpath = "//*[@id='mgError-children-notification']/div/div/a[2]")
        private WebElement messageGranularityPaymentOptionsLink;

        @FindBy(xpath = "//*[@id='mgError-children-notification']/div/div/a[2]")
        private WebElement messageGranularityChargeOffLink;

        @FindBy(xpath = "//*[@id='mgError-children-notification']/div/p/a")
        private WebElement messageGranularityInLineChargeOffLink;

        @FindBy(xpath = "//a[contains(text(),'800-944-2706')]")
        private WebElement phoneNumberLink;


        @FindBy(xpath = "//a[contains(text(),'Learn about charge-offs')]")
        private WebElement chargeOffLink;

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        ele.click();
    }

    public void clickMyAccountLink() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(tabMyAccount));
        tabMyAccount.click();
    }

    public void clickSelectAccountTab() {
        linkSelectAccountPage.click();
    }

    public void clickbtnOnlineStatements() {
        clickElement(driver, btnOnlineStatements);
        //btnOnlineStatements.click();
    }

    public void clickbtnMakePayment() {
        clickElement(driver, btnMakePayment);
        //btnMakePayment.click();
    }

    public void clicktabPosted() {
        clickElement(driver, tabPosted);
        //tabPosted.click();
    }

    public void clicktabPending() {
        clickElement(driver, tabPending);
    }

    public void clicktabRecurring() {
        clickElement(driver, tabRecurring);

    }

    public void clickLinkServices(){
        String bodyTest = driver.findElement(By.tagName("body")).getText();

        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(linkServices));
        clickElement(driver, linkServices);

        if (bodyTest.contains("SERVICES")) {
            driver.findElement(By.linkText("SERVICES")).click();

        } else if (bodyTest.contains("Services")) {
            driver.findElement(By.linkText("Services")).click();

        }
    }

    public String getTitle() {
        return driver.getTitle();
    }

    protected Integer expandTransactionDetails(String tabName, String transactionDate, String description) {
        Integer index = getMatchingRowIndex(tabName, transactionDate, description);
        logger.info("Matching Row Index: " + index);
        // found match
        if (index != null && index.intValue() > -1) {
            WebElement postedTable = driver.findElement(By.id(HTML_ID_POSTED_TABLE)).findElement(By.tagName("tbody"));
            List<WebElement> postedTable_rows = postedTable.findElements(By.tagName("tr"));

            // get hidden moreDescDetails_row to get click links
            List<WebElement> cells = postedTable_rows.get(index).findElements(By.tagName("td"));
            WebElement descCell = cells.get(2);
            WebElement lnkPlus = descCell.findElement(By.tagName("a"));
            lnkPlus.click();
        }

        return index;
    }

    public Integer clickDisputeTransactionForTxnDateAndDescription(String tabName, String transactionDate,
                                                                   String description) {

        Integer index = expandTransactionDetails(tabName, transactionDate, description);

        logger.info("Transaction Index: "+index);

        if (index != null && index.intValue() > -1) {
            WebElement postedTable = driver.findElement(By.id(HTML_ID_POSTED_TABLE)).findElement(By.tagName("tbody"));
            List<WebElement> postedTable_rows = postedTable.findElements(By.tagName("tr"));

            // get hidden moreDescDetails_row to get click links
            WebElement detailsRow = postedTable_rows.get(index + 1);
            WebElement lnkDisputeTransaction = detailsRow.findElement(By.tagName("a"));
            lnkDisputeTransaction.click();
        }
        return index;
    }


    //Pending transaction
    public Integer clickPendingDisputeTransactionForTxnDateAndDescription(String pendingTabName, String pendingTransactionDate,
                                                                   String pendingDescription) {

        Integer indexp = expandPendingTransactionDetails(pendingTabName, pendingTransactionDate, pendingDescription);

        logger.info("Transaction Index: "+indexp);

        if (indexp != null && indexp.intValue() > -1) {
            WebElement pendingTable = driver.findElement(By.id(HTML_ID_PENDING_TABLE)).findElement(By.tagName("tbody"));
            List<WebElement> pendingTable_rows = pendingTable.findElements(By.tagName("tr"));

            // get hidden moreDescDetails_row to get click links
            WebElement detailsRowp = pendingTable_rows.get(indexp + 1);
            WebElement lnkPendingDisputeTransaction = detailsRowp.findElement(By.tagName("a"));
            lnkPendingDisputeTransaction.click();
        }
        return indexp;
    }
    protected Integer expandPendingTransactionDetails(String TabName, String TransactionDate,
                                                      String Description) {

        Integer indexp = getMatchingRowIndex(TabName, TransactionDate, Description);
        logger.info("Matching Row Index: " + indexp);
        // found match
        if ( indexp != null && indexp.intValue() > -1) {
                       WebElement pendingTable = driver.findElement(By.id(HTML_ID_PENDING_TABLE)).findElement(By.tagName("tbody"));
            List<WebElement> pendingTable_rows = pendingTable.findElements(By.tagName("tr"));

            // get hidden moreDescDetails_row to get click links
            List<WebElement> cellsp = pendingTable_rows.get(indexp).findElements(By.tagName("td"));
            WebElement descCellp = cellsp.get(INDEX_CELL_PND_DESCRIPTION);
            WebElement lnkPlusp = descCellp.findElement(By.tagName("a"));
            lnkPlusp.click();
        }

        return indexp;
    }

    public void switchTabs(String tabName) {
        switch (tabName.toLowerCase()) {
            case TAB_NAME_POSTED:
                break;
            case TAB_NAME_PENDING:
                break;
            case TAB_NAME_RECURRING:
                break;
            default:
                throwError("Tab does not exist:" + tabName);

        }
    }

    /**
     * @param transactionDate
     * @param description
     * @return
     */
    public Integer getMatchingRowIndex(String tabName, String transactionDate, String description) {
                return getMatchingRowIndex(tabName, transactionDate, description, null);
    }


    /**
     * TransactionDate and Description are required, Amount is not required
     *
     * @param transactionDate
     * @param description
     * @param amount          TBD!
     * @return
     */
    public Integer getMatchingRowIndex(String tabName, String transactionDate, String description, Double amount) {

        if (StringUtils.isEmpty(transactionDate))
            throwError("Transaction Date is null or not valid:" + transactionDate);

        if (StringUtils.isEmpty(description))
            throwError("Description is null:" + description);

        // Get table
        switch (tabName.toLowerCase()) {

            case TAB_NAME_POSTED: {
                WebElement postedTable = driver.findElement(By.id(HTML_ID_POSTED_TABLE)).findElement(By.tagName("tbody"));
                List<WebElement> postedTable_rows = postedTable.findElements(By.tagName("tr"));

                if (CollectionUtils.isEmpty(postedTable_rows)) {
                    return null;
                } else {
                    for (int index = 0; index < postedTable_rows.size(); index++) {
                        WebElement row = postedTable_rows.get(index);
                        // Find first row that matches txn date and description
                        List<WebElement> cells = row.findElements(By.tagName("td"));
                        if (cells.size() < 0)
                            return null;

                        String txnDate = null;


                        txnDate = cells.get(INDEX_CELL_TXN_DATE).getText();

                        logger.info("txnDate  -  " + txnDate + ":" + transactionDate);
                        if (txnDate.equalsIgnoreCase(transactionDate)) {
                            WebElement descCell = cells.get(INDEX_CELL_DESCRIPTION);
                            String desc = descCell.getText().substring(1);
                            logger.info("description  -  " + desc + ":" + description);
                            if (desc.trim().equalsIgnoreCase(description.trim())) {
                                logger.info("found matching transaction!");
                                return index;
                            }
                        }

                    }

                }
                break;
            }
            case TAB_NAME_PENDING: {
                WebElement pendingTable = driver.findElement(By.id(HTML_ID_PENDING_TABLE)).findElement(By.tagName("tbody"));
                List<WebElement> pendingTable_rows = pendingTable.findElements(By.tagName("tr"));

                if (CollectionUtils.isEmpty(pendingTable_rows)) {
                    return null;
                } else {

                    for (int index = 0; index < pendingTable_rows.size(); index++) {
                        WebElement row = pendingTable_rows.get(index);
                        // Find first row that matches txn date and description
                        List<WebElement> cells = row.findElements(By.tagName("td"));
                        if (cells.size() < 0) {

                            return null;
                        }
                        String txnDate = null;

                        txnDate = cells.get(INDEX_CELL_TXN_DATE).getText();

                        logger.info("txnDate  -  " + txnDate + ":" + transactionDate);


                        if (txnDate.equalsIgnoreCase(transactionDate)) {

                            WebElement descCell = cells.get(INDEX_CELL_PND_DESCRIPTION);
                            String desc = descCell.getText().substring(1);
                            logger.info("description  -  " + desc + ":" + description);
                            if (desc.trim().equalsIgnoreCase(description.trim())){
                                logger.info("found matching transaction!");
                                return index;
                            }

                        }
                                            }

                }
                break;
            }
            case TAB_NAME_RECURRING:
                break;

            default: {
                throwError("Not Yet Implemented:" + tabName.toLowerCase());
            }
        }

        // no match
              return null;
    }

    public boolean validDate(String date) {
        DateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
        dateFormat.setLenient(false);
        try {
            Date d = dateFormat.parse(date);
            return true;
        } catch (ParseException ex) {
            // string contains invalid date
            return false;
        }
    }

    public void throwError(String message) {
        throw new RuntimeException(message);
    }


    public void waitForMyAccountPageToLoad(){
        WebDriverWait wait = new WebDriverWait(driver,30);
        addEmailInTheAddEmailPrompt();
        wait.until(ExpectedConditions.visibilityOf(tabMyAccount));

    }

    public void clickLinkAlerts(){
        linkAlerts.click();
    }

    public void clickLinkProfile(){
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(linkProfile));
        linkProfile.click();
    }

    public void clickLinkPaymentSolutions() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(linkPaymentSolutions));
        linkPaymentSolutions.click();
    }

    // added by Madhava for handling Email Prompt to input email

    @FindBy(name = "primaryEmail")
    private WebElement emailAddressInAddYourEmailPopUp;

    @FindBy(xpath = "//*[@data-test-id='submitContinue']")
    private WebElement submitInAddYourEmailPopUp;

    public void addEmailInTheAddEmailPrompt(){
        if(driver.getTitle().equalsIgnoreCase("EmailPrompt")){
            emailAddressInAddYourEmailPopUp.sendKeys("test@usbank.com");
            submitInAddYourEmailPopUp.click();
        }
    }
    /*
     * Do NOT delete below method it is part of the PBOM Claims flow
     */

    public void clickOnPostedBtn() {
        tabPosted.click();
    }

    /*
     * Do NOT delete this method it iterates through each row of the Pending Transactions
     * and clicks on the Dispute Transaction Link it is part of the PBOM Claims Flow
     */

        public boolean viewAndClickDisputeTransactionLink() {
        boolean linkPresent;
        String test_name = null;
        WebDriverWait wait = new WebDriverWait(driver, 1);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@id='transactionDetailTable_completed']//tbody")));
        // Grab the table
        WebElement table = driver.findElement(By.xpath("//table[@id='transactionDetailTable_completed']//tbody"));
        //Get number of rows in table
        int numOfRow = table.findElements(By.tagName("tr")).size();
        for (int i = 1; i <= (numOfRow - 1); i = i + 2) {
            //Below will expand each row
            driver.findElement(By.xpath("//*[@id='transactionDetailTable_completed']/tbody/tr[" + i + "]/td[3]/a")).click();
            System.out.println(i + ":   " + driver.findElement(By.xpath("//*[@id='transactionDetailTable_completed']/tbody/tr[" + i + "]/td[3]/a")).getText());
            WebDriverWait wait1 = new WebDriverWait(driver, 1);
            By lnkDisputeTransaction = By.xpath("//*[@id='transactionDetailTable_completed']/tbody/tr[" + (i + 1) + "]/td/div/div[2]/a");

            linkPresent = isElementPresent(lnkDisputeTransaction);
            if (linkPresent) {
                test_name = driver.findElement(By.xpath("//*[@id='transactionDetailTable_completed']/tbody/tr[" + (i + 1) + "]/td/div/div[2]/a")).getText();
                System.out.println("hyperlink string: " + test_name);

                //This is for checking Hyperlink in each row like "Have Questions" or ""Dispute a Transaction"
                if (test_name.equalsIgnoreCase("Dispute this transaction")) {
                    //driver.findElement(By.xpath("//*[@id='transactionDetailTable_completed']/tbody/tr["+i+"]/td/div/div[2]")).click();
                    System.out.println("Dispute can be done " + driver.findElement(By.xpath("//*[@id='transactionDetailTable_completed']/tbody/tr[" + (i + 1) + "]/td/div/div[2]/a")).toString());
                    WebElement totalDisputeLinks = driver.findElement(By.xpath("//*[@id='transactionDetailTable_completed']/tbody/tr[" + (i + 1) + "]/td/div/div[2]/a"));
                    totalDisputeLinks.click();
                        return true;
                }
            }
        }
            return false;
    }

    /*
     * Do NOT delete below method it is part of the PBOM Claims flow
     */

    public boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }


    @FindBy(how = How.ID, using = "navOnlineStatementAnchor")
    private WebElement linkOnlineStatements;

    @FindBy(how = How.ID, using = "navMakePaymentAnchor")
    private WebElement linkMakeAPayment;

    @FindBy(how = How.ID, using = "navDownloadTransactionDataAnchor")
    private WebElement linkDownloadTransactions;


    @FindBy(how = How.XPATH, using = "//a[@data-testid='paymentSolutionsLink']")
    private WebElement linkLearnPaymentSolutions;


    public void clickOnlineStatementsLink() {
        linkOnlineStatements.click();
    }


    public void clickMakeAPaymentLink() {
        linkMakeAPayment.click();
    }

    public void clickDownloadTransactionsLink() {
        linkDownloadTransactions.click();
    }

    //for ShortTermPlanTest.java
    public void clickLearnAboutOurPaymentOptionsLink(){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOf(linkLearnPaymentSolutions));

        linkLearnPaymentSolutions.click();
    }

    public WebElement getBtnOnlineStatements() {
        return btnOnlineStatements;
    }


        public WebElement getMessageGranularityHeader() {
            return messageGranularityHeader;
        }

        public WebElement getMessageGranularityBody() {
            return messageGranularityBody;
        }

        public WebElement getMessageGranularityBtn() {
            return messageGranularityBtn;
        }

        public WebElement getMessageGranularityErrorHeader() {return messageGranularityErrorHeader;}

        public WebElement getMessageGranularityErrorBody() { return messageGranularityErrorBody;}

        public WebElement getMessageGranularityPaymentLink() { return messageGranularityPaymentLink;}

        public WebElement getMessageGranularityPaymentOptionsLink() {

            Boolean isPresent = driver.findElements(By.xpath("//*[@id='mgError-children-notification']/div/div/a[2]")).size()>0;
            if(!isPresent)
                return null;

        return messageGranularityPaymentOptionsLink;

        }
        public WebElement getMessageGranularityChargeOffLink(){ return messageGranularityChargeOffLink;}

        public WebElement getMessageGranularityInLineChargeOffLink() { return messageGranularityInLineChargeOffLink;}

        public WebElement getPhoneNumberLink() {return phoneNumberLink;}

        public WebElement getChargeOffLink() {
            Boolean isPresent = driver.findElements(By.xpath("//a[contains(text(),'Learn about charge-offs')]")).size()>0;
            if(!isPresent)
                return null;
        return chargeOffLink;
        }
}
