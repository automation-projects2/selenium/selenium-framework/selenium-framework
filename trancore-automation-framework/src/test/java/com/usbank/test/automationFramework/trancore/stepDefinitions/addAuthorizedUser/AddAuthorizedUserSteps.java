package com.usbank.test.automationFramework.trancore.stepDefinitions.addAuthorizedUser;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

public class AddAuthorizedUserSteps {

    @Autowired
    private StepDefinitionManager stepDefinitionManager;

    @When("the user enters in their Security Question on the Step Up Page for Add Authorized User Flow")
    public void theUserEntersInTheirSecurityQuestionOnTheStepUpPageForAddAuthorizedUserFlow() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getAddAuthorizedUserPage().getSecurityQuestion().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getAddAuthorizedUserPage().getShieldQuestionAnswer();
    }

    @And("^the user enters in the first name (.+), middle name (.+) , last name (.+)$")
    public void theUserEntersInTheFirstNameFirst_NameMiddleNameMiddle_NameLastNameLast_Name(String firstName, String middleName, String lastName) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getAddAuthorizedUserPage().getTxtFirstName().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getAddAuthorizedUserPage().enterFirstName(firstName);
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getAddAuthorizedUserPage().getTxtMiddleName().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getAddAuthorizedUserPage().enterMiddleName(middleName);
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getAddAuthorizedUserPage().getTxtLastName().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getAddAuthorizedUserPage().enterLastName(lastName);
    }


    @And("^the user has clicked onto the option to select a Suffix (.+)$")
    public void theUserHasClickedOntoTheOptionToSelectASuffixSuffix(String suffix) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getAddAuthorizedUserPage().getLstSuffix().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getAddAuthorizedUserPage().selectSuffix(suffix);
    }

    @And("^the user enters in the Social Security Number (.+)$")
    public void theUserEntersInTheSocialSecurityNumberSocial_Security_Number(String ssn) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getAddAuthorizedUserPage().getTxtSSN().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getAddAuthorizedUserPage().enterSSN(ssn);
    }

    @And("^the user enters in the Date Of Birth (.+)$")
    public void theUserEntersInTheDateOfBirthDate_Of_Birth(String dateOfBirth) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getAddAuthorizedUserPage().getTxtDateOfBirth().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getAddAuthorizedUserPage().enterDateOfBirth(dateOfBirth);
    }

    @And("the user is able to read the Disclosure by clicking on the Show More Link")
    public void theUserIsAbleToReadTheDisclosureByClickingOnTheShowMoreLink() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getAddAuthorizedUserPage().getLnkShowMore().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getAddAuthorizedUserPage().readDisclosure();
    }

    @And("the user clicks on the Continue To Review button")
    public void theUserClicksOnTheContinueToReviewButton() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getAddAuthorizedUserPage().getBtnContinueToReview().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getAddAuthorizedUserPage().clickOnContinueTOReviewButton();
    }

    @Then("the user clicks on the Submit Authorized User Request Button and capture user info")
    public void theUserClicksOnTheSubmitAuthorizedUserRequestButton() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getAddAuthorizedUserPage().getAuthorizedUserInfo().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getAddAuthorizedUserPage().readAndSubmitAuthorizedUserInfo();
    }

}
