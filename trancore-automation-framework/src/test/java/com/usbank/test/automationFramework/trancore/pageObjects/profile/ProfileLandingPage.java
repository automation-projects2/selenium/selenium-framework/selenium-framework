package com.usbank.test.automationFramework.trancore.pageObjects.profile;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProfileLandingPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(ProfileLandingPage.class);
    private static final long EXPLICIT_WAIT_TIME_SECONDS = 30;
    private T driver;

    @FindBy(how = How.LINK_TEXT, using = "Profile")
    private WebElement linkProfilePage;

    @FindBy(xpath = "//a[@title='View or Change ID Shield Image/Sound and Phrase']")
    private WebElement linkChangeIdShieldImage;

    @FindBy(how = How.TAG_NAME, using = "h2")
    private WebElement headername;


    @FindBy(how = How.XPATH, using = "//*[@title='Contact Information']")
    private WebElement linkContactInfoPage;


    public ProfileLandingPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    public void clickRewardsLink() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        linkProfilePage.click();
    }

    public void clickLinkChangeIdShieldImage() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(linkChangeIdShieldImage));
        linkChangeIdShieldImage.click();
    }


    public void clickContactInformationPage() {
        linkContactInfoPage.click();
    }


    public void verifyHeaderName() {
        // TODO Auto-generated method stub
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(linkContactInfoPage));
        String headername1 = headername.getText();
        Assert.assertEquals("profile", headername1.toLowerCase());
        logger.info(headername1, "is displayed successfully, PASSED");


    }
}
