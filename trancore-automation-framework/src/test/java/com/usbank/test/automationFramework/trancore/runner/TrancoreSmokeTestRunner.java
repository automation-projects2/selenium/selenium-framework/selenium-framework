package com.usbank.test.automationFramework.trancore.runner;

import com.usbank.test.helper.RunnerBase;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/java/com/usbank/test/automationFramework/trancore/features/payments"},
        glue = {"com.usbank.test.automationFramework.trancore.stepDefinitions"},
        tags = {"@FundingAccountScreen"},
        plugin = {"pretty", "html:target/cucumber-reports", "junit:target/cucumber.xml"}, monochrome = true)
public class TrancoreSmokeTestRunner extends RunnerBase {
}
