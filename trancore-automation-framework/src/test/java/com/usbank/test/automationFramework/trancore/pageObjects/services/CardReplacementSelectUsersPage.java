package com.usbank.test.automationFramework.trancore.pageObjects.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.assertEquals;

public class CardReplacementSelectUsersPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(CardReplacementSelectUsersPage.class);

    private static final long EXPLICIT_WAIT_TIME_SECONDS = 30;

    private T driver;

    @FindBy(how = How.TAG_NAME, using = "h2")
    private WebElement headername;

    @FindBy(how = How.ID, using = "selectall")
    private WebElement checkboxSelectAll;

    @FindBy(how = How.ID, using = "Continue")
    private WebElement linkContinue;

    @FindBy(how = How.NAME, using = "Continue")
    private WebElement continueButton;
    @FindBy(how = How.LINK_TEXT, using = "Cancel")
    private WebElement linkCancel;

    public CardReplacementSelectUsersPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }


    public void verifyHeaderName() {
        assertEquals("Whose cards would you like to replace?", (headername.getText()));
    }

    public void ClickContinue() {
        linkContinue.click();
    }

    public void continueButtonClick() {
        continueButton.click();
    }

    public void ClickCancel() {
        linkCancel.click();
    }

    public void selectUser(String user) {
        driver.findElement(By.xpath("//input[@value='" + user + "']")).click();
        ;
    }


}

