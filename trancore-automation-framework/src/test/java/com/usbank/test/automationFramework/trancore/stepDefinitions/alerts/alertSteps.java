package com.usbank.test.automationFramework.trancore.stepDefinitions.alerts;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class alertSteps {
    @Autowired
    StepDefinitionManager stepDefinitionManager;

    @When("I click the Alerts link")
    public void i_click_the_Alerts_link() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().clickAlertsLink();
    }

    @Then("click on signupnow link")
    public void click_on_signupnow_link() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().clickSignupNowLink();
    }

    @And("close the signup page")
    public void closeTheSignupPage()
    {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().closeSignupPage();
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().clickAlertsLink();
    }

    @Then("click on AlertHistory link")
    public void clickOnAlertHistoryLink() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().clickAlertHistoryLink();
    }

    @And("close the AlertHistory page")
    public void closeTheAlertHistoryPage() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().closeAlertHistoryPage();
    }

    @Then("click on SeeAgreement link")
    public void clickOnSeeAgreementLink() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().clickSeeAgreementLink();

    }

    @And("close the SeeAgreement page")
    public void closeTheSeeAgreementPage() {
       stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().closeSeeAgreementLink();
    }

    @And("click balancealert on Alertpage")
    public void clickBalancealertferOnAlertpage() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().clickBalanceTranfer();
    }

    @Then("user should get message as Thank you.Your alert has been set up successfully.")
    public void userShouldGetMessageAsThankYouYourAlertHasBeenSetUpSuccessfully() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().verifyAccountSetUpAlertSuccessMessage();
    }
    @Then("click security alert link present on Alertpage")
    public void click_security_alert_link_present_on_Alertpage() {
       stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().clickSecurityAlertLink();
    }

    @Then("user should get message as Thank you. Your alert has been set up successfully.")
    public void user_should_get_message_as_Thank_you_Your_alert_has_been_set_up_successfully() {
       // stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().verifySecurityAlertSuccessMessage();
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().verifyAccountSetUpAlertSuccessMessage();

    }

    @And("click StatementandpaymentAlert on Alertpage")
    public void clickStatementandpaymentAlertOnAlertpage() {
       stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().clickstatementAndPaymentLink();
    }

    /*@And("provide {string} emailId")
    public void provideEmailId(String emailId) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().setonlineStatmentAvailableAlerts(emailId);
    }*/

    @Then("click on {string} under Statement and payment alert and click on SetUp link and provide {string} emailId")
    public void clickOnUnderStatementAndPaymentAlertAndClickOnSetUpLinkAndProvideEmailId(String subtype, String emailId) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().setUpAlertForStatementAndPaymentSubTypes(subtype,emailId);
    }

    @Then("click on {string} under Statement and payment alert and click on Edit link and provide {string} emailId")
    public void clickOnUnderStatementAndPaymentAlertAndClickOnEditLinkAndProvideEmailId(String subtype, String emailId) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().editAlertForStatementAndPaymentSubTypes(subtype,emailId);
    }

    @Then("user should get message as Thank you.Your alert has been updated successfully.")
    public void userShouldGetMessageAsThankYouYourAlertHasBeenUpdatedSuccessfully() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().verifyAccountEditAlertSuccessMessage();
    }


    @And("click transcationalert on Alertpage")
    public void clickTranscationalertOnAlertpage() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().clickTranscationAlerts();
    }

    @Then("click on {string} under transcationa alert and provide {string} emailid1")
    public void clickOnUnderTranscationaAlertAndProvideEmailid(String alertSubType1,String emailid1) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().setUpAlertForTranscationSubTypes(alertSubType1,emailid1);
    }


    @Then("select alert subtype {string} and provide creditamount {string} ,emailid{string}")
    public void selectAlertSubtypeAndProvideCreditamountEmailid(String alertSubType, String Amount, String emailID) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().setUpAlertForBalanceTranferSubTypes(alertSubType, Amount, emailID);

    }


    @Then("click on {string}  and click edit link and provide {string} emailid")
    public void clickOnAndClickEditLinkAndProvideEmailid(String alertSubType1, String Editemailid1) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().editAlertForTranscationSubTypes(alertSubType1, Editemailid1);
    }

    @Then("select alert subtype {string} and click edit link and provide creditamount {string}")
    public void selectAlertSubtypeAndClickEditLinkAndProvideCreditamount(String alertSubType, String editAmount) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().editAlertForBalanceTranferSubTypes(alertSubType, editAmount);
    }


    @And("select email as contact adddress and add new {string} to edit the security alert")
    public void selectEmailAsContactAdddressAndAddNewToEditTheSecurityAlert(String email) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().editSecurityAlerts(email);

    }
    @Then("click on {string} under Statement and payment alert and click on delete link and continue")
    public void clickOnUnderStatementAndPaymentAlertAndClickOnDeleteLinkAndContinue(String subtype) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().deleteAlertForStatementAndPaymentSubTypes(subtype);
    }

    @Then("user should get message as Thank you.Your alert has been deleted successfully for {string}.")
    public void userShouldGetMessageAsThankYouYourAlertHasBeenDeletedSuccessfullyFor(String subtype) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().verifyAccountdeleteAlertSuccessMessage(subtype);
    }

    @And("verify the title of page and click on manage alert link from alert history page")
    public void verifyTheTitleOfPageAndClickOnManageAlertLinkFromAlertHistoryPage() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().clickManageAlertLink();
    }

 /*   @Then("user should land on alert home page.")
    public void userShouldLandOnAlertHomePage() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().verifyAlertHomePageTitle();
    }*/

   /* @Then("click on {string} under Statement and payment alert and click on SetUp link and provide {string}status, {string} emailId")
    public void clickOnUnderStatementAndPaymentAlertAndClickOnSetUpLinkAndProvideStatusEmailId(String alertSubtype, String status, String emailid) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().setUpAlertForStatementAndPaymentSubTypes(alertSubtype,status,emailid);
    }*/

    @And("select {string} card status, email as contact adddress and add new {string} to set the security alert")
    public void selectCardStatusEmailAsContactAdddressAndAddNewToSetTheSecurityAlert(String status, String email) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().setSecurityAlerts(status,email);
    }

    /*@Then("click on {string} under Statement and payment alert and click on Edit link")
    public void clickOnUnderStatementAndPaymentAlertAndClickOnEditLink(String alertSubType) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().selectingAlertSubTypeUnderStatementAndPayment(alertSubType);
    }*/

    @Then("click on {string} under Statement and payment alert, click on Edit link and change the card status")
    public void clickOnUnderStatementAndPaymentAlertClickOnEditLinkAndChangeTheCardStatus(String alertSubType) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().editAlertByChangingStatusOfSubTypeAlertsUnderStatementAndPayment(alertSubType);
    }

    @And("change the card status")
    public void changeTheCardStatus() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().editSecurityAlertByChangingStatus();

    }

    @Then("user should land on alert home page and verify the page title of homepage")
    public void userShouldLandOnAlertHomePageAndVerifyThePageTitleOfHomepage() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().verifyAlertHomePageTitle();
    }

    @Then("select alert subtype {string} and click set up link and provide {string} and {string}")
    public void selectAlertSubtypeAndClickSetUpLinkAndProvideAnd(String alertSubType, String Amount, String emailID) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().setUpAlertForBalanceTranferSubTypes(alertSubType, Amount, emailID);
    }

    @Then("select alert subtype {string} and click edit link and change card status")
    public void selectAlertSubtypeAndClickEditLinkAndChangeCardStatus(String alertSubtype) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().editAlertByChangingStatusOfSubtypesForBalanceTranferSubTypes(alertSubtype);
    }

    @Then("select alert subtype {string} and click Delete link")
    public void selectAlertSubtypeAndClickDeleteLink(String alertSubtypes) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().deleteAlertForBalanceTranferSubTypes(alertSubtypes);
    }

    @Then("click on {string}  and click set up link and provide {string}  to set up alert")
    public void clickOnAndClickSetUpLinkAndProvideToSetUpAlert(String alertSubType1,String emailid1) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().setUpAlertForTranscationSubTypes(alertSubType1,emailid1);
    }

    @Then("click on alert subtype {string}  and click edit link and change card status")
    public void clickOnAlertSubtypeAndClickEditLinkAndChangeCardStatus(String alertSubtype1) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().editAlertByChangingStatusOfSubtypesForTranscationSubTypes(alertSubtype1);
    }

    @Then("click on {string}  and click Delete link")
    public void clickOnAndClickDeleteLink(String alertSubType1) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().deleteAlertForTranscationSubTypes(alertSubType1);
    }

    @Then("click Fraud alert link present on Alertpage")
    public void clickFraudAlertLinkPresentOnAlertpage() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().clickFraudertsLink();
    }

    @Then("verify the page title and click on cancel button")
    public void verifyThePageTitleAndClickOnCancelButton() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().verifyPageTitleFraudAlertPage();
    }

    @And("click on update contact information")
    public void clickOnUpdateContactInformation() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().clickUpdateContactInfo();
    }
@Then("select alert subtype {string} and click set up link and {string}")
    public void selectAlertSubtypeAndClickSetUpLinkAnd(String alertSubType,String emailId ) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().setUpAlertEmptyAmountfieldForBalanceTranferSubTypes(alertSubType,emailId);
    }

 @Then("user should get message as Please enter a valid dollar amount for {string} alert")
    public void userShouldGetMessageAsPleaseEnterAValidDollarAmountForAlert(String alertsubtype) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().verifyAmountFieldEmptyAlertErroressage(alertsubtype);
    }

    @Then("user should get message as Please enter a valid dollar amount  and Email Id for {string} alert")
    public void userShouldGetMessageAsPleaseEnterAValidDollarAmountAndEmailIdForAlert(String alertsubtype) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().verifyFieldEmptyAlertErroressage(alertsubtype);
    }

    @Then("user should get message as the amount must be a numeric value for {string}")
    public void userShouldGetMessageAsTheAmountMustBeANumericValueFor(String alertsubtype) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().verifyIncorrectFormatAmountFieldEmptyAlertErroressage(alertsubtype);
    }


    @Then("select alert subtype {string} and click set up link and click save")
    public void selectAlertSubtypeAndClickSetUpLinkAndClickSave(String alertsubtype) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().setUpAlertEmptyAllfieldForBalanceTranferSubTypes(alertsubtype);
    }


    @Then("click on debitposted alert  and click set up link and click save button")
    public void clickOnDebitpostedAlertAndClickSetUpLinkAndClickSaveButton() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().DebitPostedAlertErrorForAllEmptyFields();
    }

    @Then("click on debitposted alert and click set up link and provide {string}  to set up alert")
    public void clickOnDebitpostedAlertAndClickSetUpLinkAndProvideToSetUpAlert(String emailid1) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().DebitPostedAlertErrorForRadioButton(emailid1);
    }

    @Then("user should get error message for radio button")
    public void userShouldGetErrorMessageForRadioButton() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().verifyErrorMessageForRadioButtonDebitPostedAlert();
    }

    @Then("click on debitposted alert and click set up link and provide {string}")
    public void clickOnDebitpostedAlertAndClickSetUpLinkAndProvide(String emailid1) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().DebitPostedAlertErrorForAmountField(emailid1);
    }

    @Then("user should get error message for Amount Field")
    public void userShouldGetErrorMessageForAmountField() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().verifyErrorMessageForAmountFieldDebitPostedAlert();
    }

    @Then("user should get error message for respective fields")
    public void userShouldGetErrorMessageForRespectiveFields() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().verifyErrorMessageForEmailIDFieldDebitPostedAlert();
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().verifyErrorMessageForRadioButtonDebitPostedAlert();
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().verifyErrorMessageForAmountFieldDebitPostedAlert();

    }

    @Then("click on Travelnotification alert  and click set up link and provide {string}")
    public void clickOnTravelnotificationAlertAndClickSetUpLinkAndProvide(String emailid1) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().getErrorTranscationForTravelNotificationAlert(emailid1);
    }

    @Then("user should get message as Please enter a valid dollar amount for the Authorization Amount field.")
    public void userShouldGetMessageAsPleaseEnterAValidDollarAmountForTheAuthorizationAmountField() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().verifyErrorMessageForTravelNotificationAlert();
    }

    @Then("click on Payment due under Statement and payment alert and click on SetUp link and provide {string} emailId")
    public void clickOnPaymentDueUnderStatementAndPaymentAlertAndClickOnSetUpLinkAndProvideEmailId(String emailId) {
      stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().getErrorPaymentDueAlerts(emailId) ;
    }

    @Then("user should get message as Please select the number of days prior to the due date that you would like the alert to be sent")
    public void userShouldGetMessageAsPleaseSelectTheNumberOfDaysPriorToTheDueDateThatYouWouldLikeTheAlertToBeSent() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().verifyErrorMessageForPaymentDueAlert();
    }

    @Then("click on debitposted alert and click set up link and provide {string} and {string}")
    public void clickOnDebitpostedAlertAndClickSetUpLinkAndProvideAnd(String emailid1, String amount) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().debitPostedAlertErrorForIncorrectAmount(emailid1,amount);
    }

    @Then("user should get message as the debit amount must be a numeric value.")
    public void userShouldGetMessageAsTheDebitAmountMustBeANumericValue() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().verifyErrorMessageForIncorrectformatAmountFieldDebitPostedAlert();
    }

    @Then("click on Payment due under Statement and payment alert and click on SetUp link and click save")
    public void clickOnPaymentDueUnderStatementAndPaymentAlertAndClickOnSetUpLinkAndClickSave() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().getErrorForAllFiledPaymentDueAlerts();
    }

    @Then("user should get message as for both amount and email field")
    public void userShouldGetMessageAsForBothAmountAndEmailField() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().verifyErrorMessageForPaymentDueAlert();
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().verifyErrorMessageForEmailIDFieldDebitPostedAlert();
    }

    @Then("user should get message as Invalid selection.You can manage only one alert at a time")
    public void userShouldGetMessageAsInvalidSelectionYouCanManageOnlyOneAlertAtATime() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().invalidSelectionAlertPopUp();
    }

    @Then("select alert subtype {string} and click set up link and click on {string}")
    public void selectAlertSubtypeAndClickSetUpLinkAndClickOn(String alertSubType, String otherAlertSubType) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().commonBalanceTranferSubTypes(alertSubType,otherAlertSubType);
    }

    @Then("click on alert subtype {string} and click set up link and click on {string}")
    public void clickOnAlertSubtypeAndClickSetUpLinkAndClickOn(String alertSubType1, String otherAlertSubType) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().commanErrorForTranscationSubTypes(alertSubType1,otherAlertSubType);
    }

    @Then("click on {string}  and click on SetUp link and click on {string}")
    public void clickOnAndClickOnSetUpLinkAndClickOn(String alertSubType1, String otherAlertSubType) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().commanErrorForStatementAndPaymentSubTypes(alertSubType1, otherAlertSubType);
    }
    @Then("select alert subtype {string},click set up link and input {string} and save")
    public void selectAlertSubtypeClickSetUpLinkAndInputAndSave(String alertsubtype, String creditamount) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().setUpAlertAlertForBalanceTranferSubTypesWithoutEmailValue(alertsubtype,creditamount);
    }

    @Then("click on {string}  and click set up link and save")
    public void clickOnAndClickSetUpLinkAndSave(String alertsubtype) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().setUpAlertForTranscationSubTypesWithEmailNonSelected(alertsubtype);
    }

    @Then("click on {string} under Statement and payment alert and click on SetUp link and save")
    public void clickOnUnderStatementAndPaymentAlertAndClickOnSetUpLinkAndSave(String alertsubtype) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().setUpAlertForStatementAndPaymentSubTypesWithNoEmailValue(alertsubtype);
    }

    @And("click on save button")
    public void clickOnSaveButton() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().setSecurityAlertswithNoEmailValue();
    }

    @Then("click on {string} and click edit link and save")
    public void clickOnAndClickEditLinkAndSave(String alertsubtype) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().editAlertForTranscationSubTypesWithoutChangingEmailvalue(alertsubtype);
    }

    @Then("select alert subtype {string} and save")
    public void selectAlertSubtypeAndSave(String alertsubtype) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().WithoutEditingExistingValuesForForBalanceTranferSubTypes(alertsubtype);
    }

    @Then("click on {string} under Statement and payment alert and click on Edit link and save")
    public void clickOnUnderStatementAndPaymentAlertAndClickOnEditLinkAndSave(String alertsubtype) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().WithoutEditingExistingValuesForStatementAndPaymentSubTypes(alertsubtype);
    }

    @And("select email as contact adddress and click on save")
    public void selectEmailAsContactAdddressAndClickOnSave() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().WithoutEditingExistingValuesOfSecurityAlerts();
    }

    @Then("user should get the error message on the screen for not selecting email as An Address is required. Please enter a valid address.")
    public void userShouldGetTheErrorMessageOnTheScreenForNotSelectingEmailAsAnAddressIsRequiredPleaseEnterAValidAddress() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().getErrorMessageWhenEmailNonSelected();
    }

    @Then("user should get the error message on the screen for inputting invalid format of email as The address entered in Address is invalid. Please review and re-enter the address.")
    public void userShouldGetTheErrorMessageOnTheScreenForInputtingInvalidFormatOfEmailAsTheAddressEnteredInAddressIsInvalidPleaseReviewAndReEnterTheAddress() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().getErrorMessageForIncorrectFormateOfEmail();
    }

    @Then("user should get error message as on screen for not changing the pre-exting value for email as No updates were made to this Alert.  Please make your updates below and select Save to save your changes.")
    public void userShouldGetErrorMessageAsOnScreenForNotChangingThePreExtingValueForEmailAsNoUpdatesWereMadeToThisAlertPleaseMakeYourUpdatesBelowAndSelectSaveToSaveYourChanges() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().getErrorMessagewhileinputtingExistingEmail();
    }

    @Then("click on {string},click on Edit link and click on {string}")
    public void clickOnClickOnEditLinkAndClickOn(String alertsubtype, String otheralertsubtype) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().selectingOtherSubAlertForForBalanceTranferSubTypes(alertsubtype,otheralertsubtype);

    }

    @Then("user should get error message as You can manage only one alert at a time. Please use the Cancel or Save buttons.")
    public void userShouldGetErrorMessageAsYouCanManageOnlyOneAlertAtATimePleaseUseTheCancelOrSaveButtons() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().invalidSelectionAlertPopUp();
    }

    @Then("click on {string},click Edit link and click on {string} and save")
    public void clickOnClickEditLinkAndClickOnAndSave(String alertsubtype, String otheralertsubtype) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().invalidSelectionAlertPopUpForTranscationSubTypes(alertsubtype,otheralertsubtype);
    }

    @Then("click on {string} under Statement and payment alert, click Edit link and click on {string} and save")
    public void clickOnUnderStatementAndPaymentAlertClickEditLinkAndClickOnAndSave(String alertsubtype, String otheralertsubtype) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().invalidSelectionAlertPopUpForStatementAndPaymentSubTypes(alertsubtype,otheralertsubtype);
    }

    @Then("select alert subtype {string} and click set up link and provide {string} and {string} omv")
    public void selectAlertSubtypeAndClickSetUpLinkAndProvideAndOmv(String alertSubType,String Amount,String emailId) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().setUpAlertForBalanceTranferSubTypesomv(alertSubType, Amount, emailId);
    }

    @Then("click on {string}  and click set up link and provide {string}  to set up alert for omv")
    public void clickOnAndClickSetUpLinkAndProvideToSetUpAlertForOmv(String alertSubType1,String emailid1) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().setUpAlertForTranscationSubTypesomv(alertSubType1, emailid1);
    }

    @Then("select alert subtype {string} and click edit link and provide creditamount {string} for OMV")
    public void selectAlertSubtypeAndClickEditLinkAndProvideCreditamountForOMV(String alertSubType,String EditAmount) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().editAlertForBalanceTranferSubTypesomv(alertSubType, EditAmount);
    }

    @Then("select alert subtype {string} and click edit link and change card status for OMV")
    public void selectAlertSubtypeAndClickEditLinkAndChangeCardStatusForOMV(String alertSubType) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().editAlertByChangingStatusOfSubtypesForBalanceTranferSubTypesomv(alertSubType);
    }

    @Then("click on {string}  and click edit link and provide {string} emailid for OMV")
    public void clickOnAndClickEditLinkAndProvideEmailidForOMV(String alertSubType1,String Editemailid1) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().editAlertForTranscationSubTypesomv(alertSubType1, Editemailid1);
    }

    @Then("click on alert subtype {string}  and click edit link and change card status for OMV")
    public void clickOnAlertSubtypeAndClickEditLinkAndChangeCardStatusForOMV(String alertSubType1) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().editAlertByChangingStatusOfSubtypesForTranscationSubTypesomv(alertSubType1);
    }

    @Then("select alert subtype {string} and click Delete link for OMV")
    public void selectAlertSubtypeAndClickDeleteLinkForOMV(String alertSubType) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().deleteAlertForBalanceTranferSubTypesomv(alertSubType);
    }

    @Then("click on {string} and click Delete link for OMV")
    public void clickOnAndClickDeleteLinkForOMV(String alertSubType1) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().deleteAlertForTranscationSubTypesomv(alertSubType1);
    }

    @Then("click on debitposted alert and click set up link and provide {string}  to set up alert for OMV")
    public void clickOnDebitpostedAlertAndClickSetUpLinkAndProvideToSetUpAlertForOMV(String emailid1) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().setDebitPostedAlertRadiobuttonErroromv(emailid1);
    }

    @And("click transcationalert on Alertpage for Omv")
    public void clickTranscationalertOnAlertpageForOmv() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().clickTranscationAlertsOmv();
    }

    @Then("click on debitposted alert and click set up link and provide {string} for Omv")
    public void clickOnDebitpostedAlertAndClickSetUpLinkAndProvideForOmv(String emailid1) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().DebitPostedAlertErrorForAmountFieldomv(emailid1);
    }

    @Then("click on debitposted alert and click set up link and provide {string} and {string} for Omv")
    public void clickOnDebitpostedAlertAndClickSetUpLinkAndProvideAndForOmv(String emailid1,String amount) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().DebitPostedAlertErrorForIncorrectAmountFieldomv(emailid1, amount);
    }

    @Then("click on Travelnotification alert and click set up link and provide {string} for Omv")
    public void clickOnTravelnotificationAlertAndClickSetUpLinkAndProvideForOmv(String emailid1) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().getErrorTranscationForTravelNotificationAlertomv(emailid1);
    }

    @Then("click on Payment due under Statement and payment alert and click on SetUp link and provide {string} emailId for Omv")
    public void clickOnPaymentDueUnderStatementAndPaymentAlertAndClickOnSetUpLinkAndProvideEmailIdForOmv(String emailId) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().getErrorPaymentDueAlertsomv(emailId);
    }
    @Then("click on setup link for {string} and pass the {string}  emailId")
    public void clickOnSetupLinkForAndPassTheEmailId(String alertsubtype, String emailID) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().setUpOMVAlertForStatementAndPaymentSubTypes(alertsubtype, emailID);
    }

    @Then("click on setup link  and  add new {string} to set the security alert")
    public void clickOnSetupLinkAndAddNewToSetTheSecurityAlert(String emailID) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().setupomvSecurityAlerts(emailID);
    }

    @Then("click on Edit link for {string} under Statement and payment alert and change the card status")
    public void clickOnEditLinkForUnderStatementAndPaymentAlertAndChangeTheCardStatus(String alertsubtype) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().editOMVAlertByChangingStatusOfSubTypeAlertsUnderStatementAndPayment(alertsubtype);
    }

    @Then("click on Edit link for {string} under Statement and payment alert and provide {string}")
    public void clickOnEditLinkForUnderStatementAndPaymentAlertAndProvide(String alertsubtype, String emailID)  {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().editOMVAlertForStatementAndPaymentSubTypes(alertsubtype, emailID);
    }

    @And("select email as contact adddress and add new {string} to edit the security alert for omv")
    public void selectEmailAsContactAdddressAndAddNewToEditTheSecurityAlertForOmv(String emailID) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().editOMVSecurityAlerts(emailID);
    }

    @And("change the card status for security alert")
    public void changeTheCardStatusForSecurityAlert() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().editOMVSecurityAlertByChangingStatus();
    }

    @And("click on security alert delete link and continue")
    public void clickOnSecurityAlertDeleteLinkAndContinue() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().deleteSecurityAlert();
    }

    @And("close the SeeAgreement page for SeeAgreement link")
    public void closeTheSeeAgreementPageForSeeAgreementLink() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().closeOMVSeeAgreementLinkAlertPage();
    }

    @Then("click on {string} under Statement and payment alert and click on SetUp link and save for omv")
    public void clickOnUnderStatementAndPaymentAlertAndClickOnSetUpLinkAndSaveForOmv(String alertsubtype) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().setUpOMVAlertForStatementAndPaymentSubTypesWithNoEmailValue(alertsubtype);
    }

    @And("click on save button for omv")
    public void clickOnSaveButtonForOmv() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().setOMVSecurityAlertswithNoEmailValue();
    }

    @Then("click on Edit link for {string} under Statement and payment alert and save")
    public void clickOnEditLinkForUnderStatementAndPaymentAlertAndSave(String alertsubtype) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().WithoutEditingExistingValuesForOMVStatementAndPaymentSubTypes(alertsubtype);
    }

    @And("select email as contact adddress and click on save for omv")
    public void selectEmailAsContactAdddressAndClickOnSaveForOmv() {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().WithoutEditingExistingValuesForOMVSecurityAlerts();
    }


    @Then("click on set up link for {string}and input {string} and save")
    public void clickOnSetUpLinkForAndInputAndSave(String alertsubtype, String creditamount) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().setUpOMVAlertForBalanceTranferSubTypesWithNoEmailValue(alertsubtype, creditamount);
    }

    @Then("click on set up link for Transcation {string} and save")
    public void clickOnSetUpLinkForTranscationAndSave(String alertsubtype) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().setUpOMVAlertForTranscationSubTypesWithNoEmailValue(alertsubtype);
    }

    @Then("select alert subtype {string} and save for omv")
    public void selectAlertSubtypeAndSaveForOmv(String alertsubtype) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().editAlertForBalanceTranferSubTypesomv(alertsubtype);
    }

    @Then("click on edit link for {string} under transaction alert and save")
    public void clickOnEditLinkForUnderTransactionAlertAndSave(String alertsubtype) {
        stepDefinitionManager.getPageObjectManager().getLandingPageForAlerts().WithoutEditingExistingValuesForOMVSForTranscationSubTypes(alertsubtype);
    }



}
