package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UnAuthAccActFraudAppChkPage<T extends WebDriver> {
    private static final Logger logger = LogManager.getLogger(UnAuthAccActFraudAppChkPage.class);

    private T driver;

    @FindBy(xpath = "//*[text()='Deactivate this card.']")
    private WebElement lblDeactivateCard;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement btnDeactivateCard;

    @FindBy(xpath = "//button[@type='button']")
    private WebElement btnCancel;

    @FindBy(xpath = "//*text()='Yes, leave']")
    private WebElement yesLeaveBtn;

    @FindBy(id = "claimsCancel")
    private WebElement cancelModal;

    @FindBy(id = "cancelModalHeading")
    private WebElement cancelModalHeading;

    public WebElement getLblDeactivateCard() {
        return lblDeactivateCard;
    }

    public WebElement getCancelModal() {
        return cancelModal;
    }

    public UnAuthAccActFraudAppChkPage(T inDriver) {

        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    public void waitForUnAuthAccActFraudAppChkPageToLoad() {

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(lblDeactivateCard));
    }

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        ele.click();
    }

    public void clickDeactivateCardBtn() {
        waitForUnAuthAccActFraudAppChkPageToLoad();
        clickElement(driver, btnDeactivateCard);
    }

    public void clickCancelBtn() {
        waitForUnAuthAccActFraudAppChkPageToLoad();
        clickElement(driver, btnCancel);


    }
}
