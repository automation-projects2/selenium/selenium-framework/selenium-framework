package com.usbank.test.automationFramework.trancore.pageObjects.enrollment;

import org.openqa.selenium.WebElement;

public class SecurityQA {

    private WebElement checkbox;
    private WebElement question;
    private WebElement answer;

    public SecurityQA(WebElement checkbox, WebElement question, WebElement answer) {
        this.checkbox = checkbox;
        this.question = question;
        this.answer = answer;
    }

    public WebElement getCheckbox() {
        return checkbox;
    }

    public WebElement getQuestion() {
        return question;
    }

    public WebElement getAnswer() {
        return answer;
    }
}