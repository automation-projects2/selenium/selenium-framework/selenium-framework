package com.usbank.test.automationFramework.trancore.pageObjects.selectAccount;

import com.usbank.test.automationFramework.trancore.pageObjects.WebNavigationPage;
import com.usbank.test.automationFramework.trancore.pageObjects.creditLineIncrease.IncomePromptPage;
import com.usbank.test.automationFramework.trancore.pageObjects.services.AddAuthorizedUserPage;
import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class SelectAccountPage<T extends WebDriver> {
    private static final Logger logger = LogManager.getLogger(AddAuthorizedUserPage.class);

    private static final long EXPLICIT_WAIT_TIME_SECONDS = 30;

    private T driver;
    @FindBy(how = How.XPATH, using = "//a[contains(@href, '/onlineCard/accountSummary.do')]")
    public WebElement tabAccountSummary;

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Privacy Policy")
    private WebElement linkPrivacyPolicy;

    @FindBy(how = How.ID, using = "primaryIncome_inpt")
    private WebElement textFieldPrimaryIncome;

    //@FindBy(how = How.XPATH, using = "//a[contains(@href, '/onlineCard/accountSummary.do')]")
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Select Account")
    private WebElement selectAccountTab;

    @FindBy(how = How.ID, using = "coOwner_inpt")
    private WebElement textFieldCoIncome;


    @FindBy(how = How.ID, using = "primaryIncomeSource")
    private WebElement dropdownPriIncomeSource;

    @FindBy(how = How.ID, using = "coOwnerIncomeSource")
    private WebElement dropdownCoIncomeSource;


    @FindBy(how = How.NAME, using = "Submit")
    private WebElement buttonSubmit;

    StepDefinitionManager stepDefinitionManager;
    WebNavigationPage webNavigationPage;
    IncomePromptPage incomePromptPage;
    boolean incomeprompt;


    public SelectAccountPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    // This function is to handle income error text
    public void verifySingleUserErrorTextInComePrompt() {
        List<WebElement> errorText = driver.findElements(By.xpath("//tr//td[@class='errortext'][2]"));
        if (errorText.size() == 2) {
            assertEquals("Please enter your income.", errorText.get(0).getText());
            assertEquals("Please select a primary source of income.", errorText.get(1).getText());


        } else if ((textFieldPrimaryIncome.getText() == "") && (errorText.size() == 1)) {
            assertEquals("Please enter your income.", errorText.get(0).getText());
        } else if ((dropdownPriIncomeSource.getText() == "Select") && (errorText.size() == 1)) {
            assertEquals("Please select a primary source of income.", errorText.get(0).getText());
        }
    }

    public void selectAccountCheckForIncomePrompt(String ComIncome, String ComIncSource) {
        int flag = 0;
        int i = 0;
        List<WebElement> linkRemove = driver.findElements(By.className("Link"));
        for (i = 0; i < linkRemove.size(); i++) {
            WebElement linkAccount = driver.findElement(By.xpath("//a[contains(@href, 'accountSummary.do?phase=transactionDetails&index=" + i + "')]"));
            linkAccount.click();
            boolean tablink = tabAccountSummary.isDisplayed();
            boolean selecttablink = selectAccountTab.isDisplayed();
            try {
                if (textFieldPrimaryIncome.isDisplayed()) {
                    incomeprompt = true;
                } else {
                    incomeprompt = false;
                }
            } catch (Exception e) {

                incomeprompt = false;
            }


            //WebElement headerName1 = driver.findElement(By.id("subHeader"));
            if (tablink == false && selecttablink == false || incomeprompt == true) {
                System.out.println("This is working");
                buttonSubmit.click();
                verifySingleUserErrorTextInComePrompt();
                textFieldPrimaryIncome.sendKeys(ComIncome);
                dropdownPriIncomeSource.sendKeys(ComIncSource);
                buttonSubmit.click();
                flag = 1;
                break;


            } else if (!(selectAccountTab.isSelected())) {
                selectAccountTab.click();
                flag = 0;
            }
        }
        if (flag == 0) {
            tabAccountSummary.click();
            logger.info("Income Prompt is completed for the account # at row " + i + 1);
        } else {
            logger.info("Income prompt was not prompted for any accounts");
        }
    }


}









