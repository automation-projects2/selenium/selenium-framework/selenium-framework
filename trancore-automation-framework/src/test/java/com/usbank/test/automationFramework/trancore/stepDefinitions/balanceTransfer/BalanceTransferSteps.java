package com.usbank.test.automationFramework.trancore.stepDefinitions.balanceTransfer;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;


public class BalanceTransferSteps {

    @Autowired
    private StepDefinitionManager stepDefinitionManager;

    @When("the user enters in their Security Question on the Step Up Page")
    public void the_user_enters_in_their_security_question_on_the_step_up_page() {
        stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().getShieldQuestionAnswer();
    }


    @And("the user is able to View and Select a Balance Transfer Offer")
    public void theUserIsAbleToViewAndSelectABalanceTransferOffer() {
        stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().clickOnViewDetailsLinkAndSelectThisOffer();
    }

    @Given("the user is now on the Balance Transfer Step One of Three Page and can click on the Balance Transfer Terms and Conditions Link")
    public void theUserIsNowOnTheBalanceTransferStepOneOfThreePageAndCanClickOnTheBalanceTransferTermsAndCondtionsLink() {
        //Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().getLinkTermsAndConditions().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().verifyAndClickTermsAndConditionLink();
        stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().switchBetweenWindows();
    }

    @And("the user has clicked onto the Balance Transfer Terms and Conditions CheckBox")
    public void theUserHasClickedOntoTheBalanceTransferTermsAndConditionsCheckBox() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().getCkbCheckBox().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().clickOnCheckBox();
    }

    @And("^the user Selects A Payee (.+)$")
    public void theUserSelectsAPayee(String selectPayee) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().getLstSelectAPayee().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().selectAPayee(selectPayee);
    }

    @And("^the user may enter in a custom payee name (.+)$")
    public void theUserMayEnterInACustomPayeeName(String customePayeeName) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().getTxtCustomPayee().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().enterCustomPayee(customePayeeName);
    }


    @And("^the user enters in the Account Number (.+)$")
    public void theUserEntersInTheAccountNumber(String accountNumber) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().getTxtAccountNumber().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().enterAccountNumber(accountNumber);
    }


    @And("^the user enters in the Payee Address (.+)$")
    public void theUserEntersInThePayeeAddress(String payeeAddress) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().getTxtPayeeAddress().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().enterPayeeAddress(payeeAddress);
    }

    @And("^the user enters in the Suite Number (.+)$")
    public void theUserEntersInTheSuiteNumber(String suiteNumber) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().getTxtSuite().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().enterSuiteNumber(suiteNumber);
    }

    @And("^the user enters in the City (.+)$")
    public void theUserEntersInTheCity(String city) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().getTxtCity().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().enterCity(city);
    }

    @And("^the user enters in the Transfer Amount (.+)$")
    public void theUserEntersInTheTransfer_amount(String amount) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().getTxtTransferAmount().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().enterTransferAmount(amount);
    }

    @And("^the user selects a State (.+)$")
    public void theUserSelectsAState(String state) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().getLstSelectState().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().selectAState(state);
    }


    @And("^the user enters in the ZipCode (.+)$")
    public void theUserEntersInTheZipcode(String zipcode) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().getTxtZipCode().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().enterZipCode(zipcode);
    }

    @And("the user clicks on the Continue Button")
    public void theUserClicksOnTheContinueButton() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().getBtnContinue().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().clickOnContinueButton();
    }

    @Then("the user clicks on the Submit Button for a Balance Transfer")
    public void theUserClicksOnTheSubmitButtonForABalanceTransfer() {
        //Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().getBtnFinalSubmission().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().clickOnFinalSubmission();
    }


    @And("the user clicks on the Add Another Transfer Button and then proceeds to close the Pop-Up")
    public void theUserClicksOnTheAddAnotherTransferButtonAndThenProceedsToCloseThePopUp() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().getBtnAddAnotherTransfer().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().clickOnAddAnotherTransferButton();
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().getBtnRemoveBalanceTransfer().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().clickAndCloseRemoveBalanceTransfer();
    }

    @And("the user clicks on the Select A Different Offer Button and then proceeds to close the Pop-Up")
    public void theUserClicksOnTheSelectADifferentOfferButtonAndThenProceedsToCloseThePopUp() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().getBtnSelectADifferentOffer().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().clickAndCloseSelectADifferentOffer();
    }


    @And("the user clicks on the Cancel Button and then proceeds to close the Pop-Up")
    public void theUserClicksOnTheCancelButtonAndThenProceedsToCloseThePopUp() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().getBtnCancel().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().clickAndCloseCancel();
    }

    @And("the user clicks on the Cancel Button on Step Two Of Three and then proceeds to close the Pop-Up")
    public void theUserClicksOnTheCancelButtonOnStepTwoOfThreeAndThenProceedsToCloseThePopUp() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().getBtnSecondCancel().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().clickAndCloseCancelButton();
    }

    @Then("the user clicks on the Request Another Balance Transfer button")
    public void theUserClicksOnTheRequestAnotherBalanceTransferButton() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().getBtnRequestAnotherBalanceTransfer().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getBalanceTransferPage().clickOnRequestAnotherBalanceTransfer();
    }

}
