package com.usbank.test.automationFramework.trancore.pageObjects.combinedAccounts;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static java.util.concurrent.TimeUnit.SECONDS;

public class CombinedAccountsPage<T extends WebDriver> {
    private static final Logger logger = LogManager.getLogger(CombinedAccountsPage.class);

    private T driver;
    public static int EXPLICIT_WAIT_TIME_SECONDS = 30;

    public CombinedAccountsPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "accountNo")
    private WebElement txtAccountNumber;

    @FindBy(id = "sigPanelCode-tel-input")
    private WebElement txtCVV2;

    @FindBy(id = "ssn-tel-input")
    private WebElement txtSSN;

    @FindBy(name = "zipCd")
    private WebElement txtZipCode;

    @FindBy(how = How.ID_OR_NAME, using = "Cancel")
    private WebElement btnCancel;

    @FindBy(how = How.LINK_TEXT, using = "Select Account")
    private WebElement selectAccountTab;

    @FindBy(how = How.XPATH, using = "//a[text()='SELECT ACCOUNT']")
    private WebElement clickSelectAccountTab;

    @FindBy(how = How.LINK_TEXT, using = "Profile")
    private WebElement linkProfilePage;

    @FindBy(how = How.LINK_TEXT, using = "Add an Account")
    private WebElement linkAddAAccountPage;

    @FindBy(how = How.LINK_TEXT, using = "Remove")
    private WebElement linkRemoveAccountPage;

    @FindBy(how = How.ID_OR_NAME, using = "Continue")
    private WebElement btnContinue;

    @FindBy(how = How.ID_OR_NAME, using = "Submit")
    private WebElement btnSubmit;

    @FindBy(how = How.ID_OR_NAME, using = "showPin")
    private WebElement lnkShowPin;
    @FindBy(how = How.ID_OR_NAME, using = "pin")
    private WebElement txtShowPin;

    @FindBy(xpath = "//td[@class='errorText']")
    private WebElement errorMessageList;

    @FindBy(how = How.XPATH, using = "//img[@title='Help']")
    private WebElement btnImage;

    @FindAll({
            @FindBy(how = How.LINK_TEXT, using = "Account Activity"),
            @FindBy(how = How.LINK_TEXT, using = "MY ACCOUNT")
    })
    private List<WebElement> lnkAccountActivity;


    public void showPin() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkShowPin));
        lnkShowPin.click();
        clickpinImage();
    }

    public void clickpinImage() {
        new WebDriverWait(driver, 100).until(ExpectedConditions.visibilityOf(txtShowPin));
        List<WebElement> listImg = driver.findElements(By.xpath("//img[@title='Help']"));
        listImg.get(1).click();
    }

    public void clickImage() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnImage));
        btnImage.click();
    }

    public void clickAllPinImages() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnImage));
        List<WebElement> helpEls = driver.findElements(By.xpath("//img[@title='Help']"));
        for (WebElement helpEl : helpEls) {
            helpEl.click();
        }
    }

    public void clickProfileLinkadded() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(linkProfilePage));
        linkProfilePage.click();
    }

    public void cantSeeSelectAccountTab() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(linkProfilePage));
        try {
            Assert.assertTrue(selectAccountTab.isDisplayed());
        } catch (NoSuchElementException nsee) {
            Assert.assertTrue(true);
        }

    }

    public void clickSelectAccount() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(clickSelectAccountTab));
        clickSelectAccountTab.click();
    }

    public void clickCancel() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnCancel));
        btnCancel.click();
    }

    public void clickAddAAccount() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(linkAddAAccountPage));
        linkAddAAccountPage.click();
    }

    public void clickRemoveAccount() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(linkRemoveAccountPage));
        linkRemoveAccountPage.click();
    }

    public void clickContinue() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnContinue));
        btnContinue.click();
    }

    public void clickSubmit() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnSubmit));
        btnSubmit.click();
    }

    public void fillAccountForm(String accountNumber, String cvv, String ssn, String zip) {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(txtAccountNumber));

        txtAccountNumber.sendKeys(accountNumber);
        txtCVV2.sendKeys(cvv);
        txtSSN.sendKeys(ssn);
        txtZipCode.sendKeys(zip);
    }

    public void populateAddAccount(String field, String value) {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(txtAccountNumber));
        String accountNumber = field.equals("accountnumber") ? value : "4037670127194305";
        String CVV = field.equals("cvv") ? value : "1234";
        String SSN = field.equals("ssn") ? value : "4142";
        String zip = field.equals("zip") ? value : "33324";
        if (field.equals("invalid")) {
            accountNumber = "1234567890";
            CVV = "000";
            SSN = "1234";
            zip = "12345";
        }
        txtAccountNumber.sendKeys(accountNumber);
        txtCVV2.sendKeys(CVV);
        txtSSN.sendKeys(SSN);
        if (field.equals("pin")) {
            lnkShowPin.click();
            new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(txtShowPin));
            txtShowPin.sendKeys(value);
        } else {
            txtZipCode.sendKeys(zip);
        }
    }

    public void populateEntireAddAccount(List<String> value) {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnSubmit));
        txtAccountNumber.sendKeys(value.get(0));
        txtCVV2.sendKeys(value.get(1));
        txtZipCode.sendKeys(value.get(2));
        txtSSN.sendKeys(value.get(3));
    }

    public void getErrorCode(String errorCode) {
        boolean isErrorCodeDisplayed = false;
        driver.manage().timeouts().implicitlyWait(3, SECONDS);
        List<WebElement> errorCodeEls = driver.findElements(By.xpath("//div[@class='environmentInfo']"));

        for (WebElement errorCodeEl : errorCodeEls) {
            logger.info(errorCodeEl.getText());
            String errorCodeText = errorCodeEl.getText();
            if (errorCodeText.equals(errorCode)) {
                isErrorCodeDisplayed = true;
                break;
            }
        }
        Assert.assertTrue(isErrorCodeDisplayed);
    }

    public void getErrorMessage(String message) {
        boolean isErrorDisplayed = false;
        driver.manage().timeouts().implicitlyWait(3, SECONDS);
        List<WebElement> errorTexts = driver.findElements(By.xpath("//td[@class='errortext']"));

        for (WebElement errorMessage : errorTexts) {
            logger.info(errorMessage.getText());
            String getValue = errorMessage.getText();
            if (getValue.equals(message)) {
                isErrorDisplayed = true;
                break;
            }
        }
        Assert.assertTrue(isErrorDisplayed);

    }

    public void openAccountDetails(String accountNumber) {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnImage));
        boolean isAccountClicked = false;
        String lastFourDigits = accountNumber.substring(accountNumber.length() - 4);
        //List <WebElement> openAccount = driver.findElements(By.xpath("//a[text()="+lastFourDigits+"]"));
        List<WebElement> openAccount = driver.findElements(By.xpath("//a[text()=" + lastFourDigits + "]"));
        for (WebElement getAccount : openAccount) {
            getAccount.click();
            isAccountClicked = true;
        }
        Assert.assertTrue(isAccountClicked);

    }

    public void openAccountTabs() {
        driver.manage().timeouts().implicitlyWait(3, SECONDS);
        if (!lnkAccountActivity.isEmpty()) {
            Assert.assertTrue("Account Tab Link present", true);
        } else {

            Assert.assertTrue("Account Tab Link not present", false);
        }
    }

    public void getMultipleErrorMessage(List<String> message) {
        boolean isErrorDisplayed = false;
        Integer errorsDisplayed = 0;

        driver.manage().timeouts().implicitlyWait(3, SECONDS);
        List<WebElement> errorTexts = driver.findElements(By.xpath("//td[@class='errortext']"));
        for (String getMessage : message) {
            for (WebElement errorMessage : errorTexts) {
                String getValue = errorMessage.getText();
                if (getValue.equals(getMessage)) {
                    errorsDisplayed = errorsDisplayed + 1;
                    break;
                }
            }
        }
        if (message.size() == errorsDisplayed) {
            isErrorDisplayed = true;
        }
        Assert.assertTrue(isErrorDisplayed);

    }
}
