package com.usbank.test.automationFramework.trancore.pageObjects.claims;

import com.usbank.test.automationFramework.trancore.pageObjects.waitForPageToLoad.WaitForPageToLoad;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class OtherCardUsedPage<T extends WebDriver> {
    @FindBy(how = How.XPATH, using = "//h1[@id='servicingAppHeader']")
    private WebElement lblOtherCardUsedHeader;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Is the other payment card a Visa?')]")
    private WebElement vrbIsTheOtherPaymentCardAVisa;

    @FindBy(how = How.XPATH, using = "//input[@id='31-Y']")
    private WebElement rbYes;

    @FindBy(how = How.XPATH, using = "//input[@id='31-N']")
    private WebElement rbNo;

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Continue')]")
    private WebElement btnContinueOnOtherCardUsedPage;

    private T driver;

    WaitForPageToLoad callPageLoadMethod;

    private static final Logger log = LogManager.getLogger(OtherCardUsedPage.class.getName());

    public OtherCardUsedPage(T inDriver) {
        driver = inDriver;
        callPageLoadMethod = new WaitForPageToLoad(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getLblOtherCardUsedHeader() {
        return lblOtherCardUsedHeader;
    }

    public void isTheOtherPaymentCardAVisaText() {
        log.info(vrbIsTheOtherPaymentCardAVisa.getText());
    }

    //Clicking on YES takes the user to a separate path
    public void clickOnYes() {
        rbYes.click();
    }

    //Clicking on NO takes the user to a separate path
    public void clickOnNo() {
        rbNo.click();
    }

    public void clickOnTheContinueBtnOnOtherCardUsedPage() {
        btnContinueOnOtherCardUsedPage.click();
    }

    public void otherCardUsedPagePBOMFlowOne() throws InterruptedException {
        log.info("**************** Other Card Used Page Reasoning One ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblOtherCardUsedHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblOtherCardUsedHeader, 3);
        isTheOtherPaymentCardAVisaText();
        clickOnYes();
        clickOnTheContinueBtnOnOtherCardUsedPage();
    }

    public void otherCardUsedPagePBOMFlowTwo() throws InterruptedException {
        log.info("**************** Other Card Used Page Reasoning Two ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblOtherCardUsedHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblOtherCardUsedHeader, 3);
        isTheOtherPaymentCardAVisaText();
        clickOnNo();
        clickOnTheContinueBtnOnOtherCardUsedPage();
    }

}