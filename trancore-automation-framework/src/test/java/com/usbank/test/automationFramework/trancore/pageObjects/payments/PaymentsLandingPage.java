package com.usbank.test.automationFramework.trancore.pageObjects.payments;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PaymentsLandingPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(PaymentsLandingPage.class);

    private T driver;

    @FindBy(xpath = "//html/head/title")
    private WebElement pageTitle;

    @FindBy(xpath = "//*[@id='subHeader']/span")
    private WebElement headingPaymentLanding;

    @FindBy(how = How.LINK_TEXT, using = "PAYMENTS")
    private WebElement linkPaymentsPage;

    @FindBy(xpath = "//a[text()='Learn about our Payment Options']")
    private WebElement learnAbtLink;

    @FindBy (xpath = "//a[@href='/onlineCard/transactionDetails.do']")
    private WebElement myAccountTab;

    public PaymentsLandingPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    public WebElement getMyAccountTab() { return myAccountTab;}

    public void clickPaymentsLink() {
        linkPaymentsPage.click();
    }


    public void clickLearnABoutLink() {

        learnAbtLink.click();
    }

    public WebElement getHeadingPaymentLanding() {
        try {
            waitForPageLoad();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return headingPaymentLanding;
    }

    public void waitForPageLoad() throws InterruptedException {

        int max = 2;
        int counter = 0;
        boolean pageLoaded = false;

        while (pageLoaded == false && counter < max) {

            if (pageTitle.getText().equalsIgnoreCase("Credit Card Account Access:  Manage Payments")) {
                pageLoaded = true;
            } else {

                Thread.sleep(5000);
                counter++;

            }

        }
    }
}
