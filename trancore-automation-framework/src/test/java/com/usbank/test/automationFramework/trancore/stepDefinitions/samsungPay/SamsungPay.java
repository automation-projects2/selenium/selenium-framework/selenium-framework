package com.usbank.test.automationFramework.trancore.stepDefinitions.samsungPay;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import com.usbank.test.automationFramework.trancore.stepDefinitions.googlePay.GooglePay;
import cucumber.api.java.en.*;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

@RunWith(Cucumber.class)

public class SamsungPay {
    @Autowired
    StepDefinitionManager stepDefinitionManager;
    GooglePay googlePay;
    boolean elementPresent = true, samsungPayLinkImagePresent, verifyCancelButtonPresent, multipleContactType = true ;
//   Integer sessionCounter = stepDefinitionManager.sessionCounter;
   boolean gpayElementPresent= googlePay.gpayElementPresent;
   boolean gpayMultiContactType = googlePay.gpayMultipleContactType;



    @Then("display Add to Samsung Pay link")
    public void display_Add_to_Samsung_Pay_link() throws IOException {
        elementPresent = stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyAddToSamsungPayLink();
    }

    @Then("do not display Add to Samsung Pay link")
    public void do_not_display_Add_to_Samsung_Pay_link() {
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyAddToSamsungPayLinkNotDisplayed();
    }

    @And("Samsung Pay Image is displayed")
    public void samsungPayImageIsDisplayed() {
        if (elementPresent) {
            samsungPayLinkImagePresent = stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyAddToSamsungPayImage();
        }
    }

    @And("user click on Add to Samsung Pay link")
    public void userClickOnAddToSamsungPayLink() {
        if (elementPresent) {
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnAddToSamsungPayLink();
        }
    }

    @And("display push provision initiation screen")
    public void displayPushProvisionInitiationScreen() {
        if (elementPresent) {
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyPushProvisionInitiationScreen();
        }
    }

    @And("verify continue button is displayed")
    public void verifyContinueButtonIsDisplayed() {
        if (elementPresent && gpayElementPresent) {
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyContinueButton();
        }
    }

    @Then("display Add to Samsung Pay link for the tenured user")
    public void displayAddToSamsungPayLinkForTheTenuredUser() {
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyAddToSamsungPayLink();
    }

    @And("the user have only one contact method")
    public void theUserHaveOnlyOneContactMethod() {

    }

    @And("the user have multiple contact methods")
    public void theUserHaveMultipleContactMethods() {

    }

    @And("the user clicks on the Continue button")
    public void theUserClicksOnTheContinueButton() {
        if (elementPresent && gpayElementPresent) {
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnContinueButton();
        }
    }

    @And("Verify samsung pay agree text")
    public void verifySamsungPayAgreeText() {
        if (elementPresent) {
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySamsungPayAgreeTextInitiationScreen();
        }
    }

    @And("verify cancel button is displayed")
    public void verifyCancelButtonIsDisplayed() {
        if (elementPresent && gpayElementPresent) {
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyCancelButton();
        }
    }

    @And("verify add your card to samsung wallet text")
    public void verifyAddYourCardToSamsungWalletText() {
        if (elementPresent) {
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySamsungPayWalletText();
        }
    }

    @When("the user is on Push Provision Initiation page")
    public void theUserIsOnPushProvisionInitiationPage() {
        stepDefinitionManager.getPageObjectManager().getWebNavigationPage().clickServicesLinkadded();
        elementPresent = stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyAddToSamsungPayLink();
        if (elementPresent) {
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnAddToSamsungPayLink();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyPushProvisionInitiationScreen();
            verifyCancelButtonPresent = stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyCancelButton();
        }
    }

    @And("the user click on the Cancel button")
    public void theUserClickOnTheCancelButton() {
       // if (verifyCancelButtonPresent) {  //commenting this as this condition is not working in all the cases as we are reusing this code for multiple pages
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnCancelButton();
       // }
    }

    @And("the user click on the Cancel Link")
    public void theUserClickOnTheCancelLink() {
        if (elementPresent && gpayElementPresent)
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnCancelLink();
    }

    @Then("display the Services page")
    public void displayTheServicesPage() {
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyBackToSerivcesPage();
    }

    @When("the user is on Secondary Authentication Screen")
    public void theUserIsOnSecondaryAuthenticationScreen() {
        stepDefinitionManager.getPageObjectManager().getWebNavigationPage().clickServicesLinkadded();
        elementPresent = stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyAddToSamsungPayLink();
        if (elementPresent) {
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnAddToSamsungPayLink();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyPushProvisionInitiationScreen();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyCancelButton();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnContinueButton();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySecondaryAuthenticationScreen();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyContactInformationText();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyContactMethods();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyWarningInformationText();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySendAoneTimeCode();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyCancelButton();
        }
    }

    @And("verify contact information text is displayed")
    public void verifyContactInformationTextIsDisplayed() {
        if (elementPresent && gpayElementPresent)
            multipleContactType = stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyContactInformationText();
    }

    @Then("display Secondary Authentication Screen")
    public void displaySecondaryAuthenticationScreen() {
        if (elementPresent && gpayElementPresent)
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySecondaryAuthenticationScreen();
    }

    @And("Verify warning information is displayed")
    public void verifyWarningInformationIsDisplayed() {
        if (elementPresent && gpayElementPresent)
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyWarningInformationText();
    }

    @And("verify  send a one time code button is displayed")
    public void verifySendAOneTimeCodeButtonIsDisplayed() {
        if (elementPresent && gpayElementPresent)
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySendAoneTimeCode();
    }

    @And("verify the contact methods")
    public void verifyTheContactMethods() {
        if (elementPresent&& gpayElementPresent)
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyContactMethods();
    }

    @And("the user clicks on the Send a one time code button")
    public void theUserClicksOnTheSendAOneTimeCodeButton() {
        if (elementPresent && gpayElementPresent)
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnSendAOneTimeCode(stepDefinitionManager.sessionCounter);
    }

    @Then("display OTP verification screen")
    public void displayOTPVerificationScreen() {
        if (elementPresent && gpayElementPresent)
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySecondaryAuthenticationScreen();
    }

    @And("verify enter OTP text is displayed")
    public void verifyEnterOTPTextIsDisplayed() {
        if (elementPresent && gpayElementPresent)
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyEnterOtpText();
    }

    @And("verify enter six digit code heading is displayed")
    public void verifyEnterSixDigitCodeHeadingIsDisplayed() {
        if (elementPresent && gpayElementPresent)
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyEnterSixDigitCodeHeading();
    }

    @And("verify input field to enter the OTP  is displayed")
    public void verifyInputFieldToEnterTheOTPIsDisplayed() {
        if (elementPresent && gpayElementPresent)
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyInputFieldToEnterOtp();
    }

    @And("verify send a new code button is displayed")
    public void verifySendANewCodeButtonIsDisplayed() {
        if (elementPresent && gpayElementPresent)
            multipleContactType = stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySendANewCodeButton();
    }

    @And("verify use a another method button is displayed for multiple contact user")
    public void verifyUseAAnotherMethodButtonIsDisplayedForMultipleContactUser() {
        if (elementPresent && multipleContactType && gpayElementPresent && gpayMultiContactType)
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyUseAnotherMethodButton();
    }

    @When("the user is on Secondary Authentication Screen first attempt")
    public void theUserIsOnSecondaryAuthenticationScreenFirstAttempt() {
        this.theUserIsOnSecondaryAuthenticationScreen();
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnSendAOneTimeCode(stepDefinitionManager.sessionCounter);
    }

    @When("the user is on Secondary Authentication Screen second attempt")
    public void theUserIsOnSecondaryAuthenticationScreenSecondAttempt() {
        this.theUserIsOnSecondaryAuthenticationScreenFirstAttempt();
    }

    @Then("verify use a another method button  is hidden")
    public void verifyUseAAnotherMethodButtonIsHidden() {
        if (elementPresent) {
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyUseAnotherMethodButton();
        }
    }
/*
    @When("the user is on OTP verification screen")
    public void theUserIsOnOTPVerificationScreen() {
        stepDefinitionManager.getPageObjectManager().getWebNavigationPage().clickServicesLinkadded();
        elementPresent = stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyAddToSamsungPayLink();
        if (elementPresent) {
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnAddToSamsungPayLink();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyPushProvisionInitiationScreen();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyCancelButton();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnContinueButton();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySecondaryAuthenticationScreen();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyContactInformationText();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyContactMethods();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyWarningInformationText();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySendAoneTimeCode();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyCancelButton();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnSendAOneTimeCode(stepDefinitionManager.sessionCounter);
        }
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySecondaryAuthenticationScreen();
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyEnterOtpText();
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyEnterSixDigitCodeHeading();
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyInputFieldToEnterOtp();
        multipleContactType = stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySendANewCodeButton();
        if (multipleContactType)
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyUseAnotherMethodButton();

    }
*/

    @When("the user is on OTP verification screen")
    public void theUserIsOnOTPVerificationScreen() {
        this.theUserIsOnSecondaryAuthenticationScreen();
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnSendAOneTimeCode(stepDefinitionManager.sessionCounter);
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySecondaryAuthenticationScreen();
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyEnterOtpText();
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyEnterSixDigitCodeHeading();
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyInputFieldToEnterOtp();
        multipleContactType = stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySendANewCodeButton();
        if (multipleContactType) {
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyUseAnotherMethodButton();
        }
    }

//    @Then("verify the error messages displayed in the (.+) verification screen")
//    public void verifyTheErrorMessagesDisplayedInTheOTPVerificationScreen(String otp) {
//        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyErrorMessages(otp);
//    }

//    @And("the user enters the (.+) incorrectly")
//    public void theUserEntersTheOTPIncorrectly(String otp) {
//        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().enterTheOTP(otp);
//    }

    @When("the user is on OTP verification screen second attempt")
    public void theUserIsOnOTPVerificationScreenSecondAttempt() {
        if (elementPresent){
            this.theUserIsOnSecondaryAuthenticationScreen();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnSendAOneTimeCode(stepDefinitionManager.sessionCounter);
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySecondaryAuthenticationScreen();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyEnterOtpText();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyEnterSixDigitCodeHeading();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyInputFieldToEnterOtp();
        }

    }

    @And("the user enters the (.+) incorrectly for the first time")
    public void theUserEntersTheOTPIncorrectlyForTheFirstTime(String otp) {
        if (elementPresent)
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().enterTheOTP(otp);
    }

    @And("the user enters the (.+) incorrectly for the second attempt")
    public void theUserEntersTheSecondOTPIncorrectlyForTheSecondAttempt(String secondOtp) {
        if (elementPresent)
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().enterTheOTP(secondOtp);
    }

    @Then("verify the error message displayed in the (.+) verification screen for the first attempt")
    public void verifyTheErrorMessageDisplayedInTheOtpVerificationScreenForTheFirstAttempt(String otp) {
        if (elementPresent)
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyErrorMessages(otp);
    }

    @Then("verify the error messages displayed in the (.+) verification screen for the second attempt")
    public void verifyTheErrorMessagesDisplayedInTheSecond_otpVerificationScreenForTheSecondAttempt(String secondOtp) {
        if (elementPresent)
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyErrorMessages(secondOtp);
    }

    @Then("user attempted three Invalid (.+) attempts")
    public void userAttemptedThreeInvalidOtpcodeAttempts(String otpcode) {
        if (elementPresent && gpayElementPresent) {

            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().enterTheOTP(otpcode);
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnContinueButton();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().enterTheOTP(otpcode);
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnContinueButton();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().enterTheOTP(otpcode);
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnContinueButton();
        }
//        if (stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyInputFieldToEnterOtp()) {
//            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnContinueButton();
//        }
    }

    @And("verify appropriate error message displayed")
    public void verifyAppropriateErrorMessageDisplayed() {
        if (elementPresent && gpayElementPresent)
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyErrorMessageInSecondaryAuthentication();
    }

    @And("the user click on Send a new code button")
    public void theUserClickOnSendANewCodeButton() {
        if (elementPresent && gpayElementPresent)
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnSendANewCode();
    }

    @Then("code will be sent to the user and will stay on the same page")
    public void codeWillBeSentToTheUserAndWillStayOnTheSamePage() {
        if (elementPresent && gpayElementPresent)
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySecondaryAuthenticationScreen();
    }

    @And("verify confirmation message is displayed")
    public void verifyConfirmationMessageIsDisplayed() {
        if (elementPresent && gpayElementPresent)
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyConfirmationMessage();
    }

    @And("the user enters the (.+) incorrectly")
    public void theUserEntersTheOtpIncorrectly(String otp) {
        if (elementPresent && gpayElementPresent)
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().enterTheOTP(otp);

    }

    @Then("verify the error message displayed in the (.+) verification screen")
    public void verifyTheErrorMessageDisplayedInTheOtpVerificationScreen(String otp) {
        if (elementPresent && gpayElementPresent)
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyErrorMessages(otp);
    }


    @And("the user clicks on Use Another Method Button")
    public void theUserClicksOnUseAnotherMethodButton() {
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnAnotherMethodButton();
    }

    @Then("display Secondary Authentication Screen for selecting the contact")
    public void displaySecondaryAuthenticationScreenForSelectingTheContact() {
        if(multipleContactType && gpayMultiContactType){
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySecondaryAuthenticationScreen();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyContactMethods();
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySendAoneTimeCode();

        }
    }

    @When("the user is on OTP verification screen and enter the first code (.+) incorrectly for three times")
    public void theUserIsOnOTPVerificationScreenAndEnterTheFirstCodeOtpIncorrectlyForThreeTimes(String otp) {
        this.theUserIsOnOTPVerificationScreen();
        this.userAttemptedThreeInvalidOtpcodeAttempts(otp);
    }

    @Then("display appropriate screen according to user type")
    public void displayAppropriateScreenAccordingToUserType() {
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySecondaryAuthenticationScreen();
        if(multipleContactType){

            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyContactMethods();
        }
        else{
            stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyEnterOtpText();
        }
    }

    @When("the user is on OTP verification screen after second OTP is send")
    public void theUserIsOnOTPVerificationScreenAfterSecondOTPIsSend() {
        this.theUserIsOnSecondaryAuthenticationScreenFirstAttempt();
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnCancelLink();
        this.theUserIsOnSecondaryAuthenticationScreenFirstAttempt();

    }

    @Then("display OTP code Failure screen")
    public void displayOTPCodeFailureScreen() {
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySecondaryAuthenticationScreen();
        //verify message
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyOTPCodeFailureScreenMessage();
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyReturnToSerivcesLink();
    }

    @When("the user is on the Services page and exceeds two OTP (.+) count")
    public void theUserIsOnTheServicesPageAndExceedsTwoOTPCount(String otp) {
        this.theUserIsOnSecondaryAuthenticationScreenFirstAttempt();
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnCancelLink();
        this.theUserIsOnSecondaryAuthenticationScreenFirstAttempt();
//        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnCancelLink();
        userAttemptedThreeInvalidOtpcodeAttempts(otp);
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyOTPCodeFailureScreenMessage();
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyReturnToSerivcesLink();
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().ClickReturnToSerivcesLink();
    }

    @Then("display OTP Failure Wrap Up screen")
    public void displayOTPFailureWrapUpScreen() {
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyOTPFailureWrapUpScreen();
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyOTPFailureWrapUpScreenMessage();
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyReturnToSerivcesLink();
    }

    @And("the user has the eligible account")
    public void theUserHasTheEligibleAccount() {
    }

    @And("the user has an  ineligible account")
    public void theUserHasAnIneligibleAccount() {
    }

    @Then("display samsung Pay link and image")
    public void displaySamsungPayLinkAndImage() {
        elementPresent = stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyAddToSamsungPayLink();
        if (elementPresent) {
            samsungPayLinkImagePresent = stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyAddToSamsungPayImage();
        }
    }

    @Then("display samsung Pay link with card added status")
    public void displaySamsungPayLinkWithCardAddedStatus() {
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyAddedStatus();

        
    }

    @And("the user clicks on samsung pay link")
    public void theUserClicksOnSamsungPayLink() {
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnAddToSamsungPayLink();
    }

    @Then("display info screen of samsung pay")
    public void displayInfoScreenOfSamsungPay() {
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyInfoScreen();
    }

    @And("the user has already added the card")
    public void theUserHasAlreadyAddedTheCard() {
        stepDefinitionManager.getPageObjectManager().getWebNavigationPage().clickServicesLinkadded();
    }

    @And("the user is on the Info screen add card to device")
    public void theUserIsOnTheInfoScreenAddCardToDevice() {
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().clickOnAddToSamsungPayLink();
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyInfoScreen();

    }

    @And("the user has return to services page")
    public void theUserHasReturnToServicesPage() {
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyBackToSerivcesPage();
    }

    @When("the user has previously added the card to samsung pay")
    public void theUserHasPreviouslyAddedTheCardToSamsungPay() {
        stepDefinitionManager.getPageObjectManager().getWebNavigationPage().clickServicesLinkadded();
    }

    @And("if the account is  a CB account")
    public void ifTheAccountIsACBAccount() {
        stepDefinitionManager.getPageObjectManager().getWebNavigationPage().ServicesPageConfirmation();

    }
    @Then("display Samsung pay Set Up not complete page")
    public void displaySamsungPaySetUpNotCompletePage() {
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyPushProvisionInitiationScreen();
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifyContinueButton();
        stepDefinitionManager.getPageObjectManager().getsamsungPayPagePage().verifySetUpNotCompletePage();
    }


}

