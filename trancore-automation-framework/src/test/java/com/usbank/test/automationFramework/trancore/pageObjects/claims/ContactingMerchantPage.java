package com.usbank.test.automationFramework.trancore.pageObjects.claims;

import com.usbank.test.automationFramework.trancore.pageObjects.waitForPageToLoad.WaitForPageToLoad;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class ContactingMerchantPage<T extends WebDriver> {

    /* NOTE:
     *   The Contacting Merchant Page has three separate flows based on the outcome on the previous page
     *   which is the Tell Us More page -> when answering YES, NO, & I CANT CONTACT THE MERCHANT
     * */

    @FindBy(how = How.XPATH, using = "//h1[@id='servicingAppHeader']")
    private WebElement lblContactingMerchantHeader;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'When did you last contact the merchant?')]")
    private WebElement vrbWhenDidYouContactMerchant;

    @FindBy(how = How.XPATH, using = "//*[@id=\"7\"]")
    private WebElement txtDate;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'What was the title or name of the person you spoke')]")
    private WebElement vrbPersonYouSpokeWith;

    @FindBy(how = How.XPATH, using = "//textarea[@id='10']")
    private WebElement txtContentAreaOne;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),\"What was the merchant's response when contacted?\")]")
    private WebElement vrbResponseWhenContacted;

    @FindBy(how = How.XPATH, using = "//textarea[@id='11']")
    private WebElement txtContentAreaTwo;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'How did you contact the merchant?')]")
    private WebElement vrbHowDidYouContactmerchant;

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Continue')]")
    private WebElement btnContinueOnContactingMerchantPage;

    //This is where the page changes based on the outcome two so, I have included them at the bottom of these web-elements
    @FindBy(how = How.XPATH, using = "//span[contains(text(),\"Can you provide some details on why you didn't con\")]")
    private WebElement vrbCanYouProvideSomeDetails;

    @FindBy(how = How.XPATH, using = "//textarea[@id='12']")
    private WebElement txtContentAreaOnReasoningPageTwo;

    //This is where the page changes based on the outcome three so, I have included them at the bottom of these web-elements
    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Can you provide details on why you were unable to ')]")
    private WebElement vrbCanYouProvideDetails;

    @FindBy(how = How.XPATH, using = "//textarea[@id='13']")
    private WebElement txtContentAreaThree;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'When was your last attempt to contact the merchant')]")
    private WebElement vrbWhenWasYourLastAttempt;

    @FindBy(how = How.XPATH, using = "//input[@id='14']")
    private WebElement txtDateOnFlowThree;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'How did you attempt to contact the merchant?')]")
    private WebElement vrbHowDidYouAttempt;

    private T driver;

    WaitForPageToLoad callPageLoadMethod;

    private static final Logger log = LogManager.getLogger(ContactingMerchantPage.class.getName());

    public ContactingMerchantPage(T inDriver) {
        driver = inDriver;
        callPageLoadMethod = new WaitForPageToLoad(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getLblContactingMerchantHeader() {
        return lblContactingMerchantHeader;
    }

    public void whendDidYouLastContactTheMerchantText() {
        log.info(vrbWhenDidYouContactMerchant.getText());
    }

    public void selectDateForContactingMerchant() {
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        cal.add(Calendar.DATE, -5);
        System.out.println( dateFormat.format(cal.getTime()));

        String newDate = dateFormat.format(cal.getTime());
        txtDate.sendKeys(newDate);
        //txtDate.sendKeys("04202021");
    }

    public void whatWasTheTitelOrNameOfThePersonYouSpokeWithText() {
        log.info(vrbPersonYouSpokeWith.getText());
    }

    public void textAreaOneInput() {
        txtContentAreaOne.sendKeys("example");
    }

    public void whatWasTheMerchantsResponseWhenContactedText() {
        log.info(vrbResponseWhenContacted.getText());
    }

    public void textAreaTwoInput() {
        txtContentAreaTwo.sendKeys("example");
    }

    public void selectHowDidYouContactTheMerchant() {
        String reasonForContactingMerchant = "Email";

        List<WebElement> listOfReasons = driver.findElements(By.name("radio8"));
        List<WebElement> listOfText = driver.findElements(By.className("svc-radioButton"));

        int listSize = listOfReasons.size();

        for (int i = 0; i < listSize; i++) {

            String reasonText = listOfText.get(i).getText();

            log.info("Reasoning " + i + ": " + reasonText);
            if (reasonText.equalsIgnoreCase(reasonForContactingMerchant)) {
                listOfReasons.get(i).click();
            }
        }
    }

    public void clickOnContinueBtnOnContactingMerchantPage() {
        btnContinueOnContactingMerchantPage.click();
    }

    //These methods are Contacting Merchant Page Two flow
    public void canYouProvideSomeDetailsText() {
        log.info(vrbCanYouProvideSomeDetails.getText());
    }

    public void textAreaInputForFlowTwo() {
        txtContentAreaOnReasoningPageTwo.sendKeys("example");
    }

    //These methods are Contacting Merchant Page Third flow
    public void canYouProvideDetailsOnWhyYouWereUnableToContactText() {
        log.info(vrbCanYouProvideDetails.getText());
    }

    public void textAreaInputForFlowThree() {
        txtContentAreaThree.sendKeys("example");
    }

    public void whenWasYourLastAttemptText() {
        log.info(vrbWhenWasYourLastAttempt.getText());
    }

    public void selectDateOnFlowThree() {
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        cal.add(Calendar.DATE, -15);
        System.out.println( dateFormat.format(cal.getTime()));

        String newDate = dateFormat.format(cal.getTime());
        txtDateOnFlowThree.sendKeys(newDate);
        //txtDateOnFlowThree.sendKeys("11202020");
    }

    public void howDidYouLastAttemptToContactText() {
        log.info(vrbHowDidYouAttempt.getText());
    }

    public void selectHowDidYouContactTheMerchantOnFlowThree() {
        String reasonForContactingMerchant = "Email";

        List<WebElement> listOfReasons = driver.findElements(By.name("radio16"));
        List<WebElement> listOfText = driver.findElements(By.className("svc-radioButton"));

        int listSize = listOfReasons.size();

        for (int i = 0; i < listSize; i++) {

            String reasonText = listOfText.get(i).getText();

            log.info("Reasoning " + i + ": " + reasonText);
            if (reasonText.equalsIgnoreCase(reasonForContactingMerchant)) {
                listOfReasons.get(i).click();
            }
        }
    }

    public void contactingMerchantPagePBOMFlowOne() throws InterruptedException {
        log.info("**************** Contacting Merchant Page Reasoning One ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblContactingMerchantHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblContactingMerchantHeader, 10);
        //callPageLoadMethod.waitForPageToLoadExampleThree();
        whendDidYouLastContactTheMerchantText();
        selectDateForContactingMerchant();
        whatWasTheTitelOrNameOfThePersonYouSpokeWithText();
        textAreaOneInput();
        whatWasTheMerchantsResponseWhenContactedText();
        textAreaTwoInput();
        selectHowDidYouContactTheMerchant();
        clickOnContinueBtnOnContactingMerchantPage();
    }

    public void contactingMerchantPagePBOMFlowTwo() throws InterruptedException {
        log.info("**************** Contacting Merchant Page Reasoning Two ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblContactingMerchantHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblContactingMerchantHeader, 5);
        //callPageLoadMethod.waitForPageToLoadExampleThree();
        canYouProvideSomeDetailsText();
        textAreaInputForFlowTwo();
        clickOnContinueBtnOnContactingMerchantPage();
    }

    /*TODO

     *  and i have to figure out the new page that is created
     *  when i click on Other in the list
     * */

    public void contactingMerchantPagePBOMFlowThree() throws InterruptedException {
        log.info("**************** Contacting Merchant Page Reasoning Three ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblContactingMerchantHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblContactingMerchantHeader, 5);
        canYouProvideDetailsOnWhyYouWereUnableToContactText();
        textAreaInputForFlowThree();
        whenWasYourLastAttemptText();
        selectDateOnFlowThree();
        howDidYouLastAttemptToContactText();
        selectHowDidYouContactTheMerchantOnFlowThree();
        clickOnContinueBtnOnContactingMerchantPage();
    }


}
