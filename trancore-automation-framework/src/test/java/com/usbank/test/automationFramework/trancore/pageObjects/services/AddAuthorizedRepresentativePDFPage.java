package com.usbank.test.automationFramework.trancore.pageObjects.services;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.logging.Logger;

public class AddAuthorizedRepresentativePDFPage<T extends WebDriver> {

    private T driver;

    private static Logger log = Logger.getLogger(AddAuthorizedRepresentativePDFPage.class.getName());

    public AddAuthorizedRepresentativePDFPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public void viewAddAuthorizedRepPDF() {
        String parentWindow = driver.getWindowHandle();

        for (String subWindow : driver.getWindowHandles()) {
            driver.switchTo().window(subWindow);
            log.info("Get Current URL:" + driver.getCurrentUrl());
            log.info("Get Title  of all windows" + driver.getTitle());
        }
        driver.close();
        driver.switchTo().window(parentWindow);
    }

}
