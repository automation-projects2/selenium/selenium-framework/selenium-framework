package com.usbank.test.automationFramework.trancore.smoke;

import com.usbank.test.automationFramework.trancore.regression.RegressionBase;
import com.usbank.test.helper.SeleniumExtentionsBrowser;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SmokeTests extends RegressionBase {

    private String parentHandle;

    @Test
    public void testOnLoginTab() {
        pageObjectManager.getWebNavigationPage().clickLoginTab();
        Assert.assertTrue("Personal ID Login not displayed!", pageObjectManager.getLoginPage().getTxtPersonalID().isDisplayed());
        pageObjectManager.getLoginPage().clickForgotPersonalIdLink();
        assertTextOnScreen("Retrieve Personal ID");
        pageObjectManager.getLoginPage().getBtnCancel().click();
        Assert.assertTrue("Personal ID Login not displayed!", pageObjectManager.getLoginPage().getTxtPersonalID().isDisplayed());
    }


    @Test
    public void testEnrollTab() {
        pageObjectManager.getWebNavigationPage().clickEnrollTab();
        pageObjectManager.getDriver().findElement(By.xpath("//span[@id='enrollFormCancel']")).click();

    }

    @Test
    public void testPrivacyPolicy() {
        parentHandle = SeleniumExtentionsBrowser.openWindow(pageObjectManager.getDriver(), pageObjectManager.getWebNavigationPage().getLinkPrivacyPolicy());
        // Do things in child window
        WebElement btnCloseWindow = pageObjectManager.getDriver().findElement(By.xpath("//input[@name='Close Window']"));
        btnCloseWindow.click();
        // Return to parent window
        pageObjectManager.getDriver().switchTo().window(parentHandle);
    }


    @Test
    public void testSecurityStandards() {
        parentHandle = SeleniumExtentionsBrowser.openWindow(pageObjectManager.getDriver(), pageObjectManager.getWebNavigationPage().getLinkSecurityStandards());
        pageObjectManager.getDriver().findElement(By.xpath("//input[@name='Close Window']")).click();

        // Return to parent window
        pageObjectManager.getDriver().switchTo().window(parentHandle);

    }

    @Test
    public void testContactUs() {
        pageObjectManager.getWebNavigationPage().clickContactUsTab();
        assertTextOnScreen("Contact Us");

    }
}
