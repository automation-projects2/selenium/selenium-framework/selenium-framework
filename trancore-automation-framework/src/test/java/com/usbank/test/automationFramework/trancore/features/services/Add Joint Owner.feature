#Author: Ben Pemberton (bmpembe)
@CardmemberServicesSuite @AddJointOwner @WIP @Regression
Feature: Add Joint Owner

  @TC001
  Scenario Outline: Add Joint Owner_pdf
    Given the user has navigated to the <partner> login page and logs in using <testData> profile
    When I click Services Tab
    And I click link - Joint Owner PDF
    Then the user is able to view the Joint Owner PDF

    Examples:
      | partner       | testData |
