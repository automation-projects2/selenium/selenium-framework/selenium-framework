package com.usbank.test.automationFramework.trancore.pageObjects.myAccount;
//Created by Sanjay Kalluri(Team LOS) 11/02/2020

import com.usbank.test.automationFramework.trancore.pageObjects.services.AddAuthorizedUserPage;
import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

public class AccountSummary<T extends WebDriver> {
    private static final Logger logger = LogManager.getLogger(AddAuthorizedUserPage.class);

    private static final long EXPLICIT_WAIT_TIME_SECONDS = 30;

    private T driver;
    @FindBy(how = How.XPATH, using = "/onlineCard/accountSummary.do")
    private WebElement tabAccountSummary;

    @FindBy(how = How.ID, using = "subHeader")
    private WebElement headerName;

    @FindBy(how = How.ID, using = "tc_PostedTab")
    private WebElement tabPosted;

    StepDefinitionManager stepDefinitionManager;

    public AccountSummary(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public void verifyHeadername() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(tabPosted));
        assertEquals("account summary", (headerName.getText().toLowerCase()));
    }


}


