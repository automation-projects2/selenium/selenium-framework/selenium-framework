package com.usbank.test.automationFramework.trancore.pageObjects.claims;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DisputeReasonSelectionPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(DisputeReasonSelectionPage.class);

    private T driver;

    @FindBy(xpath = "//h1[text()='Reason for dispute']")
    private WebElement lblDisputeReasonSelectionHeader;

    @FindBy(xpath = "//button[@data-testid='svcContinueButton']")
    private WebElement btnContinue;

    public DisputeReasonSelectionPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    public void waitForPageLoad(){
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(lblDisputeReasonSelectionHeader));

    }

    public void selectDisputeReason(String disputeReason){

        String xpathForDisputeReason = "//label[text()='"+disputeReason+"']";

        System.out.println("xpathForDisputeReason: "+xpathForDisputeReason);

        WebElement disputeReasonRadio = driver.findElement(By.xpath(xpathForDisputeReason));
        disputeReasonRadio.click();

        btnContinue.click();

    }
}
