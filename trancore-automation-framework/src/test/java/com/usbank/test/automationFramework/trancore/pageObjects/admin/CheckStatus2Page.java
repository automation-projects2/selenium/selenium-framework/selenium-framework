package com.usbank.test.automationFramework.trancore.pageObjects.admin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class CheckStatus2Page<T extends WebDriver> {

    private T driver;
    //todo: make global
    public static int EXPLICIT_WAIT_TIME_SECONDS = 30;

    private static final Logger logger = LogManager.getLogger(CheckStatus2Page.class);

    public CheckStatus2Page(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Test transaction download (OFX)")
    private WebElement linkTestTransactionDownloads;

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Test the AIM web service and CMOD (report Archive)")
    private WebElement linkTestAIM;

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Test payments (web service)")
    private WebElement linkTestPayments;

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Test secure messages (I200)")
    private WebElement linkTestSecureMessages;

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Enhanced Authentication Server (EAS)")
    private WebElement linkTestEAS;

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "IDXML")
    private WebElement linkTestIDXML;

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Asset Management Application Server (AMA)")
    private WebElement linkTestAMA;

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Test Teamsite Partner Config Primary DB")
    private WebElement linkTestTeamsitePrimary;

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Test Teamsite Partner Config Failover DB")
    private WebElement linkTestTeamsiteSecondary;

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Display Partner feature flags")
    private WebElement linkTestFeatureFlags;

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Test SSO Authentication service call")
    private WebElement linkTestSSO;

    private List<String> testFailureErrors = new ArrayList<>();


    public void testCheckStatus(String url) {
        driver.get(url);
        checkElement(linkTestTransactionDownloads);
        checkElement(linkTestAIM);
        checkElement(linkTestPayments);
        checkElement(linkTestSecureMessages);
        checkElement(linkTestEAS);
        checkElement(linkTestIDXML);
        checkElement(linkTestAMA);
        checkElement(linkTestTeamsitePrimary);
        checkElement(linkTestTeamsiteSecondary);
        checkElement(linkTestFeatureFlags);
        checkElement(linkTestSSO);

        if (!testFailureErrors.isEmpty()) {
            testFailureErrors.stream().forEach(System.out::println);
            Assert.assertTrue("Test Failures: See Log!", testFailureErrors.isEmpty());
        }
    }

    private boolean checkElement(WebElement link) {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(link));
        String linkText = link.getText();
        link.click();
        String text = "SUCCESS";
        String bodyText = driver.findElement(By.tagName("body")).getText();
        Boolean isSuccess = bodyText.contains(text);
        if (!isSuccess) {
            testFailureErrors.add(bodyText);
        }
        driver.navigate().back();
        return isSuccess;
    }

}
