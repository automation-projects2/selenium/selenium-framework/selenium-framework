package com.usbank.test.automationFramework.trancore.pageObjects.services;

import com.usbank.test.helper.Utility;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DigitalWalletPage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(DigitalWalletPage.class);

    private T driver;
    public static int EXPLICIT_WAIT_TIME_SECONDS = 30;

    @FindBy(xpath = "//span[text()='Digital Wallet']")
    private WebElement headingDigitalWallet;

    //Mobile Payments Website
    @FindBy(xpath = "//*[@id=\"layoutContentBody\"]/div/div/div[2]/p[6]/a")
    private WebElement aLinkMobilePaymentsWebsite;

    public DigitalWalletPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    public void waitForPageLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(headingDigitalWallet));
    }

    public WebElement getHeadingDigitalWallet() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(headingDigitalWallet));
        return headingDigitalWallet;
    }

    public Boolean checkMobilePaymentWebsite() {
        String url = aLinkMobilePaymentsWebsite.getAttribute("href");
        if (Utility.verifyLink(url).equalsIgnoreCase("OK")) {
            return true;
        } else {
            return false;
        }
    }
}
