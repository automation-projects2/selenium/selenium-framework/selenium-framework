package com.usbank.test.automationFramework.trancore.pageObjects.claims;

import com.usbank.test.automationFramework.trancore.pageObjects.waitForPageToLoad.WaitForPageToLoad;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class DisputeCaseContactInfoPage<T extends WebDriver> {
    @FindBy(how = How.XPATH, using = "//h1[@id='servicingAppHeader']")
    private WebElement lblDisputeCaseContactInfoHeader;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Where would you like us to send the letters or con')]")
    private WebElement vrbWhereWouldYouLikeUsToSendTheLetters;

    @FindBy(how = How.XPATH, using = "//input[@id='radioFileAddress']")
    private WebElement rbMailingAddressOnFile;

    //This captures the mailing info on file
    @FindBy(how = How.CSS, using = "div.elan-app div.middleWrapper:nth-child(3) div.layoutPageContent2:nth-child(2) div.layoutPageContent3 div.tc_page_shadow div.claimInitiationRedirectToApp div.singleBlock div.svc-layout-container div.claims-init-feat div.claims-content-body:nth-child(2) div.claims-dispute-contact-info-body.svc-body-layout:nth-child(4) form:nth-child(2) div.svc-padding-element > div.claims-address-field.svc-show-content:nth-child(3)")
    private WebElement vrbMailingInfo;

    @FindBy(how = How.XPATH, using = "//input[@id='phoneNumber']")
    private WebElement txtPhoneNumber;

    //This is the alternate selection that the user can select
    @FindBy(how = How.XPATH, using = "//input[@id='radioAltAddress']")
    private WebElement rbAlternateMailingAddress;

    @FindBy(how = How.XPATH, using = "//input[@id='mailingAddressLine1']")
    private WebElement txtMailingAddress;

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Continue')]")
    private WebElement btnContinueOnDisputeCaseContactInfoPage;

    @FindBy(how = How.XPATH, using = "//input[@id='mailingApartmentUnitNum']")
    private WebElement txtSuite;

    @FindBy(how = How.XPATH, using = "//input[@id='mailingCity']")
    private WebElement txtCity;

    @FindBy(how = How.XPATH, using = "//select[@id='mailingState']")
    private WebElement lstState;

    @FindBy(how = How.XPATH, using = "//input[@id='mailingZipFive']")
    private WebElement txtZipCode;

    @FindBy(how = How.XPATH, using = "//input[@id='altPhoneNumber']")
    private WebElement txtPhoneNUmber;

    @FindBy(how = How.XPATH, using = "//input[@id='validUntil']")
    private WebElement txtFutureDate;

    private T driver;

    WaitForPageToLoad callPageLoadMethod;

    private static final Logger log = LogManager.getLogger(DisputeCaseContactInfoPage.class.getName());

    public DisputeCaseContactInfoPage(T inDriver) {
        driver = inDriver;
        callPageLoadMethod = new WaitForPageToLoad(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getLblDisputeCaseContactInfoHeader() {
        return lblDisputeCaseContactInfoHeader;
    }

    //These methods are Dispute Case Contact Info Page alternate flow#1
    public void whereWouldYouLikeUsToSendTheLetters() {
        log.info(vrbWhereWouldYouLikeUsToSendTheLetters.getText());
    }

    public void clickOnMailingAddressOnFile() {
        rbMailingAddressOnFile.click();
    }

    public void mailingInformation() {
        log.info(vrbMailingInfo.getText());
    }

    public void enterInThePhoneNumber() {

            new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(txtPhoneNUmber));
            txtPhoneNumber.sendKeys("829-688-3636");
    }

    //These methods are Dispute Case Contact Info Page alternate flow#2
    public void selectMailUpdatesToAlternateAddress() {
        rbAlternateMailingAddress.click();
    }

    public void listOfAdresseeNames() {
        List<WebElement> ListOfNames = driver.findElements(By.name("addresseeName"));

        for (int i = 0; i < ListOfNames.size(); i++) {
            log.info(ListOfNames.get(i).getText());
        }

        WebElement names = driver.findElement(By.name("addresseeName"));
        Select selectAnObject = new Select(names);
        selectAnObject.selectByIndex(1);
    }

    public void enterInAddress() {
        txtMailingAddress.sendKeys("example");
    }

    public void enterInSuiteNumber() {
        txtSuite.sendKeys("1234");
    }

    public void enterInCity() {
        txtCity.sendKeys("asdf");
    }

    public void selectAState() {
        List<WebElement> listOfStates = driver.findElements(By.xpath("//select[@id='mailingState']"));
        for (WebElement listOfState : listOfStates) {
           // log.info(listOfState.getText());
            Select drpdwnState = new Select(driver.findElement(By.xpath("//select[@id='mailingState']")));
            drpdwnState.selectByVisibleText("Minnesota");
        }
    }

    public void enterInZipCode() {
        txtZipCode.sendKeys("55523");
    }

    public void enterInPhoneNumberAlternateFlow2() {
        txtPhoneNUmber.sendKeys("5054017709");
    }

    /* TODO:
     *   Need to code for future date currently it is hardcoded
     *  */
    public void enterInTheFutureDate() {
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        cal.add(Calendar.DATE, 180);
        System.out.println( dateFormat.format(cal.getTime()));

        String newDate = dateFormat.format(cal.getTime());
        txtFutureDate.sendKeys(newDate);
       // txtFutureDate.sendKeys("07302021");
    }

    public void clickOnTheContinueBtnOnDisputeCaseContactInfoPage() {
        btnContinueOnDisputeCaseContactInfoPage.click();
    }

    public void disputeCaseContactInfoPagePBOMFlowOne() throws InterruptedException {
        log.info("**************** Dispute Case Contact Info Page Reasoning One ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblDisputeCaseContactInfoHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblDisputeCaseContactInfoHeader, 5);
        whereWouldYouLikeUsToSendTheLetters();
        clickOnMailingAddressOnFile();
        mailingInformation();
        enterInThePhoneNumber();
        clickOnTheContinueBtnOnDisputeCaseContactInfoPage();
    }

    public void disputeCaseContactInfoPagePBOMFlowTwo() throws InterruptedException {
        log.info("**************** Dispute Case Contact Info Page Reasoning Two ****************");
       callPageLoadMethod.waitExplicitlyForPageToLoad(lblDisputeCaseContactInfoHeader);
       callPageLoadMethod.waitImplicitlyForPageToLoad(lblDisputeCaseContactInfoHeader, 5);
        selectMailUpdatesToAlternateAddress();
        listOfAdresseeNames();
        enterInAddress();
        enterInSuiteNumber();
        enterInCity();
        selectAState();
        enterInZipCode();
        enterInPhoneNumberAlternateFlow2();
        enterInTheFutureDate();
        clickOnTheContinueBtnOnDisputeCaseContactInfoPage();
    }


    public void waitForPageLoad() {

        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOf(rbMailingAddressOnFile));
    }

}
