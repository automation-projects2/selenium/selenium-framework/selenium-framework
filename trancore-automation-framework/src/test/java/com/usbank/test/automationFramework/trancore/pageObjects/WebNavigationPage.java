package com.usbank.test.automationFramework.trancore.pageObjects;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class WebNavigationPage<T extends WebDriver> {

    //    private static final Logger logger = LogManager.getLogger(WebNavigationPage.class);
    private T driver;
    //todo: make global
    public static int EXPLICIT_WAIT_TIME_SECONDS = 30;
    @Autowired
    StepDefinitionManager stepDefinitionManager;

    public WebNavigationPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }


    @FindAll({
            @FindBy(how = How.LINK_TEXT, using = "MY ACCOUNT"),
            @FindBy(how = How.LINK_TEXT, using = "My Account"),
            @FindBy(how = How.LINK_TEXT, using = "Account Activity"),
    })
    private WebElement linkMyAccountPage;

    @FindBy(how = How.LINK_TEXT, using = "Contact Information")
    private WebElement linkContactInformation;

    @FindAll({
            @FindBy(how = How.LINK_TEXT, using = "PAYMENTS"),
            @FindBy(how = How.LINK_TEXT, using = "Payments")
    })
    private WebElement linkPaymentsPage;

    @FindBy(how = How.LINK_TEXT, using = "Profile")
    private WebElement linkProfilepage;

    @FindBy(how = How.XPATH, using = "//a[contains(@href, '/onlineCard/alert.do')]")
    private WebElement linkAlertsPage;

    @FindBy(how = How.XPATH, using = "//a[contains(@href, '/onlineCard/customerService.do')]")
    private WebElement linkServicesPage;

    @FindBy(how = How.XPATH, using = "//*[@title='Profile']")
    private WebElement linkProfilePage;

    @FindBy(how = How.ID, using = "tc_HeaderLogout")
    private WebElement linkLogout;

    @FindBy(how = How.XPATH, using = "//a[contains(@href, '/onlineCard/accountSummary.do')]")
    private WebElement selectAccountTab;

    @FindAll({@FindBy(how = How.LINK_TEXT, using = "REWARDS"), @FindBy(how = How.LINK_TEXT, using = "Rewards")})
    private WebElement linkRewardsPage;

    /* added- as ABC partner link text is "Service"*/
    @FindBy(how = How.XPATH, using = "//a[@title='Services' or @title='SERVICES']")
    private WebElement linkServicesPagenew;

    @FindBy(how = How.ID, using = "nextButton")
    private WebElement btnContinue;

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Privacy Policy")
    private WebElement linkPrivacyPolicy;

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Security Standards")
    private WebElement linkSecurityStandards;


    /**
     * Landing Page
     */
    @FindAll({
            @FindBy(how = How.LINK_TEXT, using = "Enroll"),
            @FindBy(how = How.LINK_TEXT, using = "ENROLL")
    })
    private List<WebElement> linksEnrollPage;

    @FindAll({
            @FindBy(how = How.LINK_TEXT, using = "Login"),
            @FindBy(how = How.LINK_TEXT, using = "LOGIN")
    })
    private List<WebElement> linksLoginPage;
    @FindAll({
            @FindBy(how = How.LINK_TEXT, using = "Contact Us"),
            @FindBy(how = How.LINK_TEXT, using = "CONTACT US")
    })
    private List<WebElement> linksContactPage;

    @FindBy(how = How.ID, using = "subHeader")
    WebElement pageHeading;

    public void clickMyAccountLink() {
        linkMyAccountPage.click();
        waitForPrivacyPolicy();
    }

    public void clickPaymentsLink() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(linkPaymentsPage));
        linkPaymentsPage.click();
        waitForPrivacyPolicy();
    }

    public void clickAlertsLink() {
        linkAlertsPage.click();
        waitForPrivacyPolicy();
    }

    public void clickServicesLink() {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        //Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getServicesPage().getServicesLink().isDisplayed());
        Assert.assertTrue("SERVICES tap is not displayed",linkServicesPagenew.isDisplayed());
        linkServicesPagenew.click();
    }

    public void clickRewardsLink() {
        linkRewardsPage.click();
        waitForPrivacyPolicy();
    }

    public void clickProfileLink() {
        linkProfilePage.click();
        waitForPrivacyPolicy();
    }

    public void clickLogoutButton() {
        linkLogout.click();
    }

    /* added the method to access the Services tab*/
    public void clickServicesLinkadded() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(linkServicesPagenew));
        linkServicesPagenew.click();
    }

    public void ServicesPageConfirmation() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(pageHeading));
        String headingText = pageHeading.getText();

//        logger.info(headingText);
        Assert.assertTrue("Invalid", headingText.equalsIgnoreCase("Services"));
    }


    public void clickContactInfo() {
        linkContactInformation.click();
        waitForPrivacyPolicy();

    }

    public void clickLoginTab() {
        waitForPrivacyPolicy();
        if (!linksLoginPage.isEmpty()) {
            linksLoginPage.get(0).click();
        } else {
            Assert.assertTrue("Login Link not present", false);
        }
    }

    public void clickEnrollTab() {
        waitForPrivacyPolicy();
        if (!linksEnrollPage.isEmpty()) {
            linksEnrollPage.get(0).click();
        } else {
            Assert.assertTrue("Enroll Link not present", false);
        }
    }

    public void clickContactUsTab() {
        waitForPrivacyPolicy();
        if (!linksContactPage.isEmpty()) {
            linksContactPage.get(0).click();
        } else {
            Assert.assertTrue("Contact Us Link not present", false);
        }
    }


    public void clickSelectAccountTab() {
        selectAccountTab.click();
    }

    public void waitForPrivacyPolicy() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(linkPrivacyPolicy));
    }

    public WebElement getLinkMyAccountPage() {
        return linkMyAccountPage;
    }

    public WebElement getLinkContactInformation() {
        return linkContactInformation;
    }

    public WebElement getLinkPaymentsPage() {
        return linkPaymentsPage;
    }

    public WebElement getLinkProfilepage() {
        return linkProfilepage;
    }

    public WebElement getLinkAlertsPage() {
        return linkAlertsPage;
    }

    public WebElement getLinkServicesPage() {
        return linkServicesPage;
    }

    public WebElement getLinkProfilePage() {
        return linkProfilePage;
    }

    public WebElement getLinkLogout() {
        return linkLogout;
    }

    public WebElement getSelectAccountTab() {
        return selectAccountTab;
    }

    public WebElement getLinkRewardsPage() {
        return linkRewardsPage;
    }

    public WebElement getLinkServicesPagenew() {
        return linkServicesPagenew;
    }

    public WebElement getBtnContinue() {
        return btnContinue;
    }

    public WebElement getLinkPrivacyPolicy() {
        return linkPrivacyPolicy;
    }

    public List<WebElement> getLinksEnrollPage() {
        return linksEnrollPage;
    }

    public List<WebElement> getLinksLoginPage() {
        return linksLoginPage;
    }

    public List<WebElement> getLinksContactPage() {
        return linksContactPage;
    }

    public WebElement getLinkSecurityStandards() {
        return linkSecurityStandards;
    }
}
