package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FraudAppChkPage<T extends WebDriver> {

    protected static final Logger logger = LogManager.getLogger(FraudAppChkPage.class);

    protected T driver;


    public FraudAppChkPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "radioYes")
    private WebElement rdbYes;

    @FindBy(id = "radioNo")
    private WebElement rdbNo;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement btnContinue;

    @FindBy(xpath = "//*[text()='This might be fraud.']")
    private WebElement lblMightBeFraud;

    public WebElement getRdbYes() {
        return rdbYes;
    }

    public WebElement getRdbNo() {
        return rdbNo;
    }

    public void waitForFraudAppChkPageToLoad() {

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(lblMightBeFraud));
        wait.until(ExpectedConditions.elementToBeClickable(getRdbYes()));
    }

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        ele.click();
    }

    public void navigateThroughFraudAppChkPage() {
        waitForFraudAppChkPageToLoad();
        clickElement(driver, rdbYes);
        clickElement(driver, btnContinue);
    }

    public void selectNoRadioButton() {
        waitForFraudAppChkPageToLoad();
        clickElement(driver, rdbNo);
        clickElement(driver, btnContinue);
    }

}
