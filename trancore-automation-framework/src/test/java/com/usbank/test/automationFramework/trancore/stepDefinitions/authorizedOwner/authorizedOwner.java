package com.usbank.test.automationFramework.trancore.stepDefinitions.authorizedOwner;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;

public class authorizedOwner {

    @Autowired
    StepDefinitionManager stepDefinitionManager;
    String getName;
    @When("^the user to make sure all the features are present$")
    public void user_to_check_all_the_features_are_present() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getAuthorizedOwner().clickAllLinks();
    }

    @When("^the user select the Select Account tab$")
    public void user_select_the_Select_Account_tab() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getAuthorizedOwner().clickSelectAccount();
    }
    @When("^the user click on  Profile tab$")
    public void user_click_on_Profile_tab() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getAuthorizedOwner().clickProfile();
    }
    @When("^Display status of Contact Information button is link is (.+)$")
    public void the_user_check_contact_info_link(String value) throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getAuthorizedOwner().checkContactInfoLink(value);
    }
    @Then("^the user click on FraudAlert tab$")
    public void click_on_fraud_alerts() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getAuthorizedOwner().clickFraudAlerts();
    }
    @Then("^Display status of Update Contact Information button is (.+)$")
    public void contact_information_button_not_visible(String value) throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getAuthorizedOwner().checkForContactInformationButton(value);
    }
    @Then("^the user select AE (.+) account$")
    public void select_ae_account(String account) throws InterruptedException {
        getName = stepDefinitionManager.getPageObjectManager().getAuthorizedOwner().openSelectedAccount(account);
    }
    @When("^the user click on Services tab$")
    public void click_on_services() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getAuthorizedOwner().clickServices();
    }
    @When("^the user click on LostOrStolen link$")
    public void click_on_lostOrStolen() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getAuthorizedOwner().clickLostOrStolen();
    }
    @When("^the user click on OrderANewCard link$")
    public void click_on_Order_A_NewCard() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getAuthorizedOwner().clickOrderANewCard();
    }

    @Then("^Display Status of Lock or Unlock card link is (.+)$")
    public void display_status_of_lock_or_unlock_link(String value) throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getAuthorizedOwner().checkForLockOrUnlock(value);
    }

    @Then("^the user verify the (.+) message of LostOrStolen$")
    public void user_verify_message(String userType) throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getAuthorizedOwner().checkLostOrStolenMessage(userType, getName);
    }
    @Then("^the user verify the (.+) message of OrderNewCard$")
    public void user_verify_message_new_card(String userType) throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getAuthorizedOwner().checkOrderNewCardMessage(userType, getName);
    }
    @Then("^the user verify the AE name is displayed in address$")
    public void I_verify_name_in_address() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getAuthorizedOwner().checkFlow(getName);
    }
    @When("^the user reach to Delivery page$")
    public void user_reach_to_Delivery_page() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getAuthorizedOwner().navigateToDeliveryPage(getName);
    }

    @Then("^the user verify the Manage Employee link is (.+)$")
    public void user_verify_the_Manage_Employee_link(String value) throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getAuthorizedOwner().checkForManageEmployee(value);
    }
    @When("^the user reach to DeliveryOption Page$")
    public void user_reach_DeliveryOption_page() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getAuthorizedOwner().navigateToDeliveryOptionPage();
    }
    @When("^Status of Add Authorized Rep link is (.+)$")
    public void Status_of_Add_Authorized_Rep(String value) throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getAuthorizedOwner().chckForAddAuthoirzedRep(value);
    }

}
