# new feature
# Tags: optional
@AlertNegativeScenarios
Feature: Alert Negative scenarios

  @SetupBalanceAlertErrorMessageForEmptyAmountField
    #Preconditions: The Amount field is empty for Balance Alert
  Scenario Outline:Verify the SetUp functionality of Balance alert when Amount field is empty
    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    And I click the Alerts link
    And click balancealert on Alertpage
    Then select alert subtype "<alertsubtype>" and click set up link and "<emailid>"
    Then user should get message as Please enter a valid dollar amount for "<alertsubtype>" alert

    Examples:
      |partner  |user_name|password   |alertsubtype     |emailid              |

  @SetupBalanceAlertErrorMessgaeForEmptyAllFields
    #Precondtions:Both amount and email field are empty for Balance Alert
  Scenario Outline:Verify the SetUp functionality of Balance alert when all the fields are empty
    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    And I click the Alerts link
    And click balancealert on Alertpage
    Then select alert subtype "<alertsubtype>" and click set up link and click save
    Then user should get message as Please enter a valid dollar amount  and Email Id for "<alertsubtype>" alert

    Examples:
      |partner  |user_name|password   |alertsubtype     |

  @SetupBalanceAlertErrorMessageForInformatAmountField
    #Preconditions: The user inputs special character and negative amount in balance field
  Scenario Outline:Verify the SetUp functionality of Balance alert when the Amount is incorrect format
    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    And I click the Alerts link
    And click balancealert on Alertpage
    Then select alert subtype "<alertsubtype>" and click set up link and provide "<creditamount>" and "<emailid>"
    Then user should get message as the amount must be a numeric value for "<alertsubtype>"

    Examples:
      |partner  |user_name   |password   |alertsubtype     |creditamount|emailid              |

  @SetupDebitpostedAlertErrorMessgaeForEmptyAllFields
    #Preconditions:All field are empty for debit posted sub alert under Transational alert
  Scenario Outline:Verify the SetUp functionality of DebitPosted alert when all fileds are empty
    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    And I click the Alerts link
    And click transcationalert on Alertpage
    Then click on debitposted alert  and click set up link and click save button
    Then user should get error message for respective fields

    Examples:
      |partner  |user_name|password   |

  @SetupDebitpostedAlertErrorMessgaeForRadibuttonNotSelected
    #preconditions:The Debit radio button is not selected for less than, Greater than and equals to for debit posted sub alert
  Scenario Outline:Verify the SetUp functionality of DebitPosted alert when  Alert me when Debit radio button is not selected
    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    And I click the Alerts link
    And click transcationalert on Alertpage
    Then click on debitposted alert and click set up link and provide "<emailid1>"  to set up alert
    Then user should get error message for radio button

    Examples:
      |partner  |user_name|password    |emailid1            |

  @SetupDebitpostedAlertErrorMessgaeForAmountField
    #preconditions:The amount is empty for debit posted sub alert under Transational alert
  Scenario Outline:Verify the SetUp functionality of DebitPosted alert when the Amount field is empty
    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    And I click the Alerts link
    And click transcationalert on Alertpage
    Then click on debitposted alert and click set up link and provide "<emailid1>"
    Then user should get error message for Amount Field

    Examples:
      |partner  |user_name|password    |emailid1            |

  @SetupDebitpostedAlertErrorMessgaeForInformatAmountField
    #preconditions:Provide informat value in the balance field for debit posted sub alert under Transational alert
  Scenario Outline:Verify the SetUp functionality of DebitPosted alert when the user provides incorrect format amount for balance field
    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    And I click the Alerts link
    And click transcationalert on Alertpage
    Then click on debitposted alert and click set up link and provide "<emailid1>" and "<Amount>"
    Then user should get message as the debit amount must be a numeric value.

    Examples:
      |partner  |user_name|password    |emailid1            |Amount|

  @SetupErrorTraveNotificationTranscationAlert
    #Preconditions:Alert me Authorization Field is empty for travel notification sub alert
  Scenario Outline:Verify the SetUp functionality of Transcation  alert when Alert me when Authorization is greater than is empty
    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    And I click the Alerts link
    And click transcationalert on Alertpage
    Then click on Travelnotification alert  and click set up link and provide "<emailid1>"
    Then user should get message as Please enter a valid dollar amount for the Authorization Amount field.

    Examples:
      |partner  |user_name|password    |emailid1              |

  @SetUpErrorPaymentDueStatementandPaymentAlert
    #Preconditions:When Number of Days Prior to Due Date to be Notified drop down is not selected for payment due sub alert
  Scenario Outline:Verify the setup functionality of Payment Due alert when the drop down is not selected
    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    And I click the Alerts link
    And click StatementandpaymentAlert on Alertpage
    Then click on Payment due under Statement and payment alert and click on SetUp link and provide "<emailId>" emailId
    Then user should get message as Please select the number of days prior to the due date that you would like the alert to be sent

    Examples:
      |partner  |user_name  |password   |emailId            |

  @SetUpErrorForAllFieldsPaymentDueStatementandPaymentAlert
    #Preconditions:When Number of Days Prior to Due Date to be Notified drop down is not selected for payment due sub alert
  Scenario Outline:Verify the setup functionality of Payment Due alert when all field are empty
    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    And I click the Alerts link
    And click StatementandpaymentAlert on Alertpage
    Then click on Payment due under Statement and payment alert and click on SetUp link and click save
    Then user should get message as for both amount and email field

    Examples:
      |partner  |user_name  |password   |

  @CommanErrorForBalanceAlert
    #Preconditions:When user tries to open other sub alert without saving or closing the previous alert the user should get the alert pop up
  Scenario Outline:Verify the setup functionality of alert When user tries to open other sub alert without saving or closing the previous alert the user should get the alert pop up
    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    And I click the Alerts link
    And click balancealert on Alertpage
    Then select alert subtype "<alertsubtype>" and click set up link and click on "<otheralertsubType>"
    Then user should get message as Invalid selection.You can manage only one alert at a time

    Examples:
      |partner  |user_name  |password   |alertsubtype|otheralertsubType|

  @CommanErrorForTranscationalAlert
    #Preconditions:When user tries to open other sub alert without saving or closing the previous alert the user should get the alert pop up
  Scenario Outline:Verify the setup functionality of Transcational alert When user tries to open other sub alert without saving or closing the previous alert the user should get the alert pop up
    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    And I click the Alerts link
    And click transcationalert on Alertpage
    Then click on alert subtype "<alertsubtype1>" and click set up link and click on "<otheralertsubType>"
    Then user should get message as Invalid selection.You can manage only one alert at a time

    Examples:
      |partner  |user_name|password  |    alertsubtype1           |otheralertsubType         |

  @CommanErrorForStatementAndPaymentAlert
    #Preconditions:When user tries to open other sub alert without saving or closing the previous alert the user should get the alert pop up
  Scenario Outline:Verify the setup functionality of Statement and Payment alert When user tries to open other sub alert without saving or closing the previous alert the user should get the alert pop up
    Given the user has navigated to the <partner> login page
    And the user has logged into My Account page with <user_name> username and <password> password
    And I click the Alerts link
    And click StatementandpaymentAlert on Alertpage
    Then click on "<alertsubtype>"  and click on SetUp link and click on "<otheralertsubType>"
    Then user should get message as Invalid selection.You can manage only one alert at a time

    Examples:
      |partner  |user_name|password   |alertsubtype                           |otheralertsubType                       |
