package com.usbank.test.automationFramework.trancore.pageObjects.claims;

import com.usbank.test.automationFramework.trancore.pageObjects.waitForPageToLoad.WaitForPageToLoad;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class OtherFormOfPaymentPage<T extends WebDriver> {
    @FindBy(how = How.XPATH, using = "//h1[@id='servicingAppHeader']")
    private WebElement lblOtherFormOfPaymentHeader;

    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Describe other form of payment.')]")
    private WebElement vrbDescribeOtherFormOfPayment;

    @FindBy(how = How.XPATH, using = "//textarea[@id='30']")
    private WebElement txtArea;

    @FindBy(how = How.XPATH, using = "//button[contains(text(),'Continue')]")
    private WebElement btnContinueOnOtherFormOfPaymentPage;

    private T driver;

    WaitForPageToLoad callPageLoadMethod;

    private static final Logger log = LogManager.getLogger(OtherFormOfPaymentPage.class.getName());

    public OtherFormOfPaymentPage(T inDriver) {
        driver = inDriver;
        callPageLoadMethod = new WaitForPageToLoad(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement lblOtherFormOfPaymentHeader() {
        return lblOtherFormOfPaymentHeader;
    }

    public void describeOtherFormOfPaymentText() {
        log.info(vrbDescribeOtherFormOfPayment.getText());
    }

    public void textAreaInput() {
        txtArea.sendKeys("asdf");
    }

    public void clickOnContinueBtnOnOtherFormOfPaymentPage() {
        btnContinueOnOtherFormOfPaymentPage.click();
    }

    public void otherFormOfPaymentPagePBOMFlowOne() throws InterruptedException {
        log.info("**************** Other Form Of Payment Page Reasoning One ****************");
        callPageLoadMethod.waitExplicitlyForPageToLoad(lblOtherFormOfPaymentHeader);
        callPageLoadMethod.waitImplicitlyForPageToLoad(lblOtherFormOfPaymentHeader, 3);
        describeOtherFormOfPaymentText();
        textAreaInput();
        clickOnContinueBtnOnOtherFormOfPaymentPage();
    }
}
