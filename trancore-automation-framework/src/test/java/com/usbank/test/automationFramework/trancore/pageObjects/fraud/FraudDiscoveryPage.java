package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class FraudDiscoveryPage<T extends WebDriver> {

    protected static final Logger logger = LogManager.getLogger(FraudDiscoveryPage.class);

    protected T driver;


    public FraudDiscoveryPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "5081-Have")
    private WebElement rdbHaveCard;

    @FindBy(id = "5081-Lost")
    private WebElement rdLostCard;

    @FindBy(id = "5002-M")
    private WebElement rdbReceivedAlert;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement btnContinue;

    @FindBy(xpath = "//input[@id='5001']")
    private WebElement txtDate;

    public WebElement getRdbHaveCard() {
        return rdbHaveCard;
    }

    public WebElement rdLostCard() {
        return rdLostCard;
    }

    public void waitForFraudDiscoveryPageToLoad() {

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(getRdbHaveCard()));
    }

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        ele.click();
    }

    public void selectFirstRadioButtonInFirstQuestionSet() {
        waitForFraudDiscoveryPageToLoad();
        clickElement(driver, rdLostCard);
    }

    public void selectThirdRadiobuttonInFirstQuestionSet() {
        waitForFraudDiscoveryPageToLoad();
        clickElement(driver, rdbHaveCard);
    }

    public void selectSecondRadiobuttonInSecondQuestionSet() {
        clickElement(driver, rdbReceivedAlert);
    }

    public void clickContinue() {
        clickElement(driver, btnContinue);
    }

    public void selectAnyPreviousDate() {
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        cal.add(Calendar.DATE, -4);
        System.out.println("Previous date was: " + dateFormat.format(cal.getTime()));
        String newDate = dateFormat.format(cal.getTime());
        txtDate.sendKeys(newDate);
    }

}
