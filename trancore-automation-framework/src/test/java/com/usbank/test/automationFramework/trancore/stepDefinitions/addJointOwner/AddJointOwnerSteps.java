package com.usbank.test.automationFramework.trancore.stepDefinitions.addJointOwner;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

public class AddJointOwnerSteps {

    @Autowired
    private StepDefinitionManager stepDefinitionManager;

    @Then("the user is able to view the Joint Owner PDF")
    public void theUserIsAbleToViewAndDownloadTheJointOwnerPDF() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getAddJointOwnerPage().getLnkAddJointOwnerPDF().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getAddJointOwnerPage().viewAndDownloadJointOwnerPDF();
    }
}
