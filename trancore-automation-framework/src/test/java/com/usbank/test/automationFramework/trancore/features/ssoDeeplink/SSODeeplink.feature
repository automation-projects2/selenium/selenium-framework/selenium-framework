# new feature
# Tags: optional
@SSODeeplinks
Feature: SSo Deeplinks

  @TC001
  Scenario Outline: Verify the functionality of Lock and unlock sso deeplink

    Given the user navigated to the <partner> login page using SSO <hashid> and <destination>
    Then verify the page title for lock and unlock deeplink
    And the user can either click on Lock Card or Unlock Card Button depending on user account
    Then the user clicks on Return To Services Link

    Examples:
      | partner | hashid                                                           | destination |