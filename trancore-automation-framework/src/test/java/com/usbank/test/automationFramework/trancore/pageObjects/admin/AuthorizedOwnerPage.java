package com.usbank.test.automationFramework.trancore.pageObjects.admin;

import com.usbank.test.automationFramework.trancore.pageObjects.combinedAccounts.CombinedAccountsPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.SECONDS;

public class AuthorizedOwnerPage<T extends WebDriver> {
    private static final Logger logger = LogManager.getLogger(CombinedAccountsPage.class);

    private T driver;
    public static int EXPLICIT_WAIT_TIME_SECONDS = 30;

    public AuthorizedOwnerPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
        driver.manage().timeouts().implicitlyWait(2, SECONDS);
    }

    @FindAll({
            @FindBy(how = How.LINK_TEXT, using = "Payments"),
            @FindBy(how = How.LINK_TEXT, using = "PAYMENTS")
    })
    private List<WebElement> lnkPayment;



    @FindAll({
            @FindBy(how = How.LINK_TEXT, using = "Alerts"),
            @FindBy(how = How.LINK_TEXT, using = "ALERTS")
    })
    private List<WebElement> lnkAlerts;
    @FindAll({
            @FindBy(how = How.LINK_TEXT, using = "Fraud Alerts"),
            @FindBy(how = How.LINK_TEXT, using = "FRAUD ALERTS")
    })
    private List<WebElement> lnkFraudAlerts;

    @FindAll({
            @FindBy(how = How.LINK_TEXT, using = "Rewards"),
            @FindBy(how = How.LINK_TEXT, using = "REWARDS")
    })
    private List<WebElement> lnkReward;

    @FindAll({
            @FindBy(how = How.LINK_TEXT, using = "Select Account"),
            @FindBy(how = How.LINK_TEXT, using = "SELECT ACCOUNT")
    })
    private WebElement selectAccountTab;

    @FindAll({
            @FindBy(how = How.LINK_TEXT, using = "Link to Rewards"),
            @FindBy(how = How.LINK_TEXT, using = "LINK TO REWARDS"),
    })
    private WebElement btnLinkToRewards;

    @FindAll({
            @FindBy(how = How.LINK_TEXT, using = "Manage Rewards"),
            @FindBy(how = How.LINK_TEXT, using = "MANAGE REWARDS"),
    })
    private WebElement btnRwd;



    @FindBy(how = How.NAME, using = "Update Contact Information")
    private WebElement btnContactInformation;

    @FindBy(how = How.LINK_TEXT, using = "Contact Information")
    private WebElement lnkContactInformation;

    @FindBy(how = How.LINK_TEXT, using = "Lock or unlock card")
    private WebElement lnkLockOrUnlock;

    @FindBy(how = How.LINK_TEXT, using = "Manage employees")
    private WebElement lnkME;

    @FindBy(how = How.LINK_TEXT, using = "Report card lost or stolen")
    private WebElement lnkLostOrStolen;

    @FindBy(how = How.LINK_TEXT, using = "Order a new card")
    private WebElement lnkOrderANewCard;

    @FindBy(how = How.LINK_TEXT, using = "Add authorized representative (pdf)")
    private WebElement lnkAuthRep;

    @FindBy(how=How.XPATH,using="//img[@title='Help']")
    private WebElement btnImage;
    @FindBy(how=How.XPATH,using="//span['Manage Alerts']")
    private WebElement spnAlert;

    @FindBy(how=How.CLASS_NAME,using="vcp_welcomeUser_message")
    private WebElement btnWelcomeMessage;

    @FindBy(how=How.CLASS_NAME,using="cardLostStolenList")
    private WebElement pageLostOrStolen;

    @FindBy(how=How.ID, using = "checkbox3")
    private WebElement lblDamaged;
    @FindBy(how=How.ID, using = "sendOTP")
    private WebElement btnSendOTP;
    @FindBy(how=How.ID, using = "otp-value")
    private WebElement txtOTP;
    @FindBy(how=How.ID, using = "otpAndContinue")
    private WebElement btnOtpAndContinue;
    @FindBy (how=How.ID, using ="E1")
    private WebElement radioEmail;

    @FindAll({
            @FindBy(how = How.NAME, using = "continue"),
            @FindBy(how = How.NAME, using = "Continue"),
    })
    private WebElement btnContinue;

    @FindBy(how=How.ID, using = "lostStolenContinue")
    private WebElement btnLostStolenContinue;

    @FindBy(how = How.LINK_TEXT, using = "Profile")
    private WebElement linkProfilePage;

    @FindBy(how=How.ID, using = "0")
    private WebElement radioStdDelivery;

    @FindBy(how=How.ID, using="radioButton0")
    private WebElement radioAcknowledge;

    @FindBy(how=How.ID, using="missingDate")
    private WebElement txtMissingDate;

    @FindBy(how=How.CLASS_NAME, using="layoutContentPageSubHeader")
    private WebElement classHeader;

    @FindAll({
            @FindBy(how = How.LINK_TEXT, using = "Services"),
            @FindBy(how = How.LINK_TEXT, using = "SERVICES")
    })
    private List<WebElement> lnkServices;
    public void clickServices(){
        driver.manage().timeouts().implicitlyWait(4,SECONDS);
        if (!lnkServices.isEmpty()) {
            lnkServices.get(0).click();
            Assert.assertTrue("Services Link present", true);
        } else {
            Assert.assertTrue("Services Link not present", false);
        }
    }

    public void clickProfile()
    {
        driver.manage().timeouts().implicitlyWait(4,SECONDS);
        linkProfilePage.click();
    }

    public void chckForAddAuthoirzedRep(String status){
        try {
            new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkAuthRep));
            Assert.assertTrue(lnkAuthRep.isDisplayed());
            Assert.assertTrue(true);
        } catch (Exception nsee) {
            if (status.equals("false")) {
                Assert.assertTrue(true);
            }else{
                Assert.assertTrue(false);
            }
        }
    }
    public void navigateToDeliveryOptionPage() throws InterruptedException{
        btnContinue.click();
        try{
            new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnSendOTP));
            btnSendOTP.click();
            new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(txtOTP));
            txtOTP.sendKeys("111111");
            btnOtpAndContinue.click();
            synchronized (driver) {
                driver.wait(3000);
            }
        }catch(Exception error){

        }
        try{ //Adding try catch hear to handle scenario that Acknowledge radio button wont be available if we dont have have recent transactions
            radioAcknowledge.click();
            btnContinue.click();
            synchronized (driver) {
                driver.wait(3000);
            }
        }catch (Exception error){
            btnContinue.click();
            synchronized (driver) {
                driver.wait(3000);
            }
        }

        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(txtMissingDate));
        txtMissingDate.sendKeys("12/12/2019");
        btnContinue.click();
        synchronized (driver) {
            driver.wait(3000);
        }
    }

    public void checkFlow(String getName)
    {
        boolean messageStatus;
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(radioStdDelivery));
        List<WebElement> listDivData = driver.findElements(By.xpath("//div[@class='tieringElement']"));
        messageStatus = messageStatus(listDivData, getName);
        Assert.assertTrue("AO Message for Order New Card", messageStatus);

        radioStdDelivery.click();
        btnContinue.click();
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnContinue));
        listDivData = driver.findElements(By.xpath("//div[@class='tieringElement']"));
        messageStatus = messageStatus(listDivData, getName);
        Assert.assertTrue("AO Message for Order New Card", messageStatus);

    }
    public void navigateToDeliveryPage(String getName){
        boolean messageStatus;

        new WebDriverWait(driver, 5);
        try{
            lblDamaged.click();
            btnContinue.click();
            try{
                new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnSendOTP));
                try {
                    if (radioEmail.isDisplayed()) {
                        radioEmail.click();
                    }
                }catch(Exception error){

                }
                btnSendOTP.click();
                new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(txtOTP));
                txtOTP.sendKeys("111111");
                btnOtpAndContinue.click();
            }catch(Exception error){
                //Bypassed oneTimePasscode flow
            }
            Assert.assertTrue("Navigation To Delivery Page Successful", true);
        }catch(NoSuchElementException e){
            Assert.assertTrue("Navigation To Delivery Page UnSuccessful", false);
        }
    }
    public boolean  messageStatus(List<WebElement> openAccount, String message){
        boolean messageStatus=false;
        for (WebElement getMsg : openAccount) {
            String[] msg = getMsg.getText().split("\n");
            if ((msg[0].contains(message))) {
                messageStatus = true;
                break;
            }
        }
        return messageStatus;
    }
    public void checkOrderNewCardMessage(String userType, String getName){
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnWelcomeMessage));
        String getAccountNumber = btnWelcomeMessage.getText();
        String lastFourDigits = getAccountNumber.substring(getAccountNumber.length() - 4);
        List<WebElement> openAccount = driver.findElements(By.xpath("//div[@class='tieringElement']/div"));
        switch (userType) {
            case "AO": {
                String OrderNewCard = "Before we get started, please tell us what's the issue with the card ending in " + lastFourDigits;
                boolean messageStatus = messageStatus(openAccount, OrderNewCard);
                Assert.assertTrue("AO Message for Order New Card", messageStatus);
            }
            break;
            case "AE": {
                String OrderNewCard = "Before we get started, please tell us what's the issue with "+ getName+"'s card ending in " + lastFourDigits + " in case there are any unrecognized charges.";
                boolean messageStatus = messageStatus(openAccount, OrderNewCard);
                Assert.assertTrue("AE Message for Order New Card", messageStatus);
            }
            break;
        }
    }
    public void checkLostOrStolenMessage(String  userType, String getName){
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(pageLostOrStolen));
        String getAccountNumber = btnWelcomeMessage.getText();
        String lastFourDigits = getAccountNumber.substring(getAccountNumber.length() - 4);
        List<WebElement> openAccount = driver.findElements(By.className("cardLostStolenList"));

        switch (userType){
            case "AO": {
                String LostOrStolenMessage = "We'll review your recent purchases on card ending in " + lastFourDigits + " in case there are any unrecognized charges.";
                boolean messageStatus = messageStatus(openAccount,LostOrStolenMessage);
                Assert.assertTrue("AO Message", messageStatus);
            }
            break;
            case "AE": {
                String message1 ="We'll review the recent purchases for "+ getName +" on card ending in " + lastFourDigits + " in case there are any unrecognized charges.";
                boolean messageStatus = messageStatus(openAccount,message1);
                Assert.assertTrue("AO Message", messageStatus);
            }
            break;
        }
    }

    public void clickLostOrStolen()throws InterruptedException{
        clickServices();
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 500);");
        synchronized (driver) {
            driver.wait(3000);
        }
        try{
            lnkLostOrStolen.click();
            synchronized (driver) {
                driver.wait(3000);
            }
            Assert.assertTrue("Lost Or Stolen link present", true);
        }catch(NoSuchElementException e){
            Assert.assertTrue("Lost Or Stolen link Unavailable", true);
        }
    }
    public void clickOrderANewCard(){
        clickServices();
        try{
            new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkOrderANewCard));
            lnkOrderANewCard.click();
            Assert.assertTrue("Order A New Card link present", true);
        }catch(NoSuchElementException e){
            Assert.assertTrue("Order A New Card link Unavailable", true);
        }
    }


    public void clickRewards() {
        try {
            if (!lnkReward.isEmpty()) {
                lnkReward.get(0).click();
                Assert.assertTrue("Rewards Link present", true);
                new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnLinkToRewards));
            } else {
                Assert.assertTrue("Rewards Link not present", true);
            }
        }catch(Exception error){
            try {
                new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnRwd));
                Assert.assertTrue("Rewards Link present", true);
            }catch(Exception e) {
                Assert.assertTrue("Rewards Link Not present", true);
            }
        }
    }
    public void clickPayment(){
        //   driver.manage().timeouts().implicitlyWait(2,SECONDS);
        if (!lnkPayment.isEmpty()) {
            lnkPayment.get(0).click();
            Assert.assertTrue("Payment Link present", true);
        } else {
            Assert.assertTrue("Payment Link not present", false);
        }
    }
    public void clickAlerts(){
        if (!lnkAlerts.isEmpty()) {
            lnkAlerts.get(0).click();
            Assert.assertTrue("Alerts Link present", true);
        } else {
            Assert.assertTrue("Alerts Link not present", false);
        }
    }
    public void clickFraudAlerts(){
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        clickAlerts();
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOfAllElements(lnkFraudAlerts));
        if (!lnkFraudAlerts.isEmpty()) {
            lnkFraudAlerts.get(0).click();
            Assert.assertTrue(" Fraud Alerts Link present", true);
        } else {
            Assert.assertTrue("Fraud Alerts Link not present", false);
        }
    }
    public void checkForContactInformationButton(String chkValue){
        try {
            Assert.assertTrue(btnContactInformation.isDisplayed());
            Assert.assertTrue(true);
        } catch (NoSuchElementException error) {
            if (chkValue.equals("false")) {
                Assert.assertTrue(true);
            }else{
                Assert.assertTrue(false);
            }
        }
    }
    public void checkContactInfoLink(String chkValue){
        try {
            Assert.assertTrue(lnkContactInformation.isDisplayed());
            Assert.assertTrue(true);
        } catch (Exception error) {
            if (chkValue.equals("false")) {
                Assert.assertTrue(true);
            }else{
                Assert.assertTrue(false);
            }
        }
    }

    public void checkForLockOrUnlock(String chkValue){
        try {
            new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkLockOrUnlock));
            Assert.assertTrue(lnkLockOrUnlock.isDisplayed());
            Assert.assertTrue(true);
        } catch (Exception nsee) {
            if (chkValue.equals("false")) {
                Assert.assertTrue(true);
            }else{
                Assert.assertTrue(false);
            }
        }
    }
    public void checkForManageEmployee(String chkValue){
        try {
            new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(lnkME));
            Assert.assertTrue(lnkME.isDisplayed());
            Assert.assertTrue(true);
        } catch (NoSuchElementException nsee) {
            if (chkValue.equals("false")) {
                Assert.assertTrue(true);
            }else{
                Assert.assertTrue(false);
            }
        }
    }

    public String openSelectedAccount(String accountNumber){
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(btnImage));
        boolean isAccountClicked=false;
        String lastFourDigits = accountNumber.substring(accountNumber.length() - 4);
        List <WebElement> openAccount = driver.findElements(By.xpath("//td[@class='vcp_accountSummary_col']"));
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOfAllElements(openAccount));
        Integer getLength=0;
        String selectedAccountHolderName = "";
        WebElement lnkName = null;
        for(WebElement getAccount : openAccount){
            String getData = getAccount.getText();
            getLength = getLength +1;
            if (getData.equals(lastFourDigits)) {
                WebElement lnkData = openAccount.get(getLength-2);
                String[] getName = lnkData.getText().split("\n");
                selectedAccountHolderName = getName[0];
                driver.findElement(By.linkText(getName[0])).click();
                isAccountClicked = true;
                break;
            }
        }
        //Assert.assertTrue(isAccountClicked);

        return selectedAccountHolderName;
    }

    public void clickAllLinks() {
        clickPayment();
        clickAlerts();
        clickServices();
        clickRewards();

    }
    public void clickSelectAccount() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(selectAccountTab));
        selectAccountTab.click();
    }
}
