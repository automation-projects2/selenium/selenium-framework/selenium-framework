package com.usbank.test.automationFramework.trancore.pageObjects.creditLineIncrease;

import com.usbank.test.automationFramework.trancore.pageObjects.services.AddAuthorizedUserPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

public class ReviewYourRequestPage<T extends WebDriver> {
    private static final Logger logger = LogManager.getLogger(AddAuthorizedUserPage.class);

    private static final long EXPLICIT_WAIT_TIME_SECONDS = 30;

    private T driver;

    @FindBy(how = How.TAG_NAME, using = "h2")
    private WebElement headerName;

    @FindBy(how = How.TAG_NAME, using = "h1")
    private WebElement subHeader;

    @FindBy(how = How.LINK_TEXT, using = "Edit income")
    private WebElement linkEditIncome;

    @FindBy(how = How.LINK_TEXT, using = "Edit housing")
    private WebElement linkEditHousing;

    @FindBy(how = How.ID, using = "submitRequest")
    private WebElement buttonSubmitRequest;

    @FindBy(how = How.ID, using = "cancelRequest")
    private WebElement buttonCancel;

    @FindBy(how = How.ID, using = "revenueAmount")
    private WebElement txtPrimaryIncome;

    @FindBy(how = How.ID, using = "sourceAmount")
    private WebElement txtPrimaryIncomeSource;

    @FindBy(how = How.ID, using = "paymentAmount")
    private WebElement txtMonthlyPayment;

    @FindBy(how = How.ID, using = "clmOwn")
    private WebElement txtHouseType;

    @FindBy(how = How.ID, using = "jointAmount")
    private WebElement txtJointIncome;

    @FindBy(how = How.ID, using = "ownerAmount")
    private WebElement txtJointIncomeSource;

    public ReviewYourRequestPage(T driver) {

        PageFactory.initElements(driver, this);
    }

    public void verifyHeaderReviewYourRequest() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id(buttonSubmitRequest.getAttribute("id"))));
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(buttonSubmitRequest));
        assertEquals("Credit limit increase\n" + "Step 3 of 3", (headerName.getText()));
        assertEquals("review your request.", subHeader.getText().toLowerCase());
    }

    public void clickEditIncome() {
        linkEditIncome.click();
    }

    public void clickEditHousing() {
        linkEditHousing.click();
    }

    public void clickSubmitMyRequest() {
        Assert.assertTrue("Submit request button is not displayed",buttonSubmitRequest.isDisplayed());
        buttonSubmitRequest.click();
    }

    public void clickCancelMyRequest() {
        buttonCancel.click();
    }

    public void validatePrimaryIncomeDetails(String primaryIncomeAmount, String primaryIncomeSource, String monthlyInstalmentAmount, String houseType) {
        assertEquals("Incorrect Primary Income Amount", primaryIncomeAmount, txtPrimaryIncome.getText().replace("$", "").replace(".00", "").replaceAll(",", ""));
        assertEquals("Incorrect Primary Income source", primaryIncomeSource, txtPrimaryIncomeSource.getText());
        assertEquals("Incorrect Monthly Installment amount", monthlyInstalmentAmount, txtMonthlyPayment.getText().replace("$", "").replace(".00", "").replaceAll(",", ""));
        if (houseType.toLowerCase().equals("yes"))
            assertEquals("Incorrect housing type", "Own", txtHouseType.getText());
        else
            assertEquals("Incorrect housing type", "Don't own", txtHouseType.getText());
    }

    public void validateJointIncomeDetails(String primaryIncomeAmount, String primaryIncomeSource, String jointIncomeAmount, String jointIncomeSource, String monthlyInstalmentAmount, String houseType) {
        assertEquals("Incorrect Primary Income Amount", primaryIncomeAmount, txtPrimaryIncome.getText().replace("$", "").replace(".00", "").replaceAll(",", ""));
        assertEquals("Incorrect Primary Income source", primaryIncomeSource, txtPrimaryIncomeSource.getText());
        assertEquals("Incorrect joint Income Amount", jointIncomeAmount, txtJointIncome.getText().replace("$", "").replace(".00", "").replaceAll(",", ""));
        assertEquals("Incorrect joint Income source", jointIncomeSource, txtJointIncomeSource.getText());
        assertEquals("Incorrect Monthly Installment amount", monthlyInstalmentAmount, txtMonthlyPayment.getText().replace("$", "").replace(".00", "").replaceAll(",", ""));
        if (houseType.toLowerCase().equals("yes"))
            assertEquals("Incorrect housing type", "Own", txtHouseType.getText());
        else
            assertEquals("Incorrect housing type", "Don't own", txtHouseType.getText());
    }
}