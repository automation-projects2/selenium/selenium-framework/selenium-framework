package com.usbank.test.automationFramework.trancore.pageObjects.phonePrompt;
// Developed by Sanjay K (Line of Sight Team ) on 11/20/2020

import com.usbank.test.automationFramework.trancore.pageObjects.otp.OTPScreenPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class PhonePromptPage<T extends WebDriver> {
    private static final Logger logger = LogManager.getLogger(OTPScreenPage.class);

    private static final long EXPLICIT_WAIT_TIME_SECONDS = 30;

    private T driver;

    @FindBy(how = How.TAG_NAME, using = "h1")
    private WebElement headerName;

    @FindBy(how = How.TAG_NAME, using = "h2")
    private WebElement subHeaderName;

    @FindBy(how = How.TAG_NAME, using = "p")
    private WebElement bodyText;

    @FindBy(how = How.TAG_NAME, using = "strong")
    private WebElement textCellPhone;

    @FindBy(how = How.ID, using = "contactDetailsPrompt")
    private WebElement remindMeNextTimeLink;

    @FindBy(how = How.ID, using = "submitAndContinue")
    private WebElement submitAndContinueLink;

    @FindBy(how = How.ID, using = "Cellphonenumber")
    private WebElement textCellPhoneNumber;

    public PhonePromptPage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public void VerifyHeaderName() {
        new WebDriverWait(driver, EXPLICIT_WAIT_TIME_SECONDS).until(ExpectedConditions.visibilityOf(remindMeNextTimeLink));
        assertEquals("please update your contact information.", headerName.getText().toLowerCase());
        assertEquals("Add your cell phone number.", subHeaderName.getText());
        assertEquals("Provide your cell phone number to take advantage of improved security features like one-time passcodes and suspicious transactions alerts via text. We will not use your cell phone number for automated marketing purposes.", bodyText.getText());
        assertEquals("Do you have a cell phone?", textCellPhone.getText());
    }

    public void selectRadioButtonDoYouHaveACellPhone(String value) {
        List<WebElement> selectRadioButton = driver.findElements(By.className("label__container"));
        if (selectRadioButton.get(0).getText().equalsIgnoreCase(value)) {
            selectRadioButton.get(0).click();
        } else if (selectRadioButton.get(1).getText().equalsIgnoreCase(value)) {
            selectRadioButton.get(1).click();
        }

    }

    public void clickRemindMeNextTime() {
        remindMeNextTimeLink.click();
    }

    public void enterPhoneNumberAndSubmit(String cellPhoneNumber) {
        textCellPhoneNumber.sendKeys(cellPhoneNumber);
        submitAndContinueLink.click();
    }

}
        
    