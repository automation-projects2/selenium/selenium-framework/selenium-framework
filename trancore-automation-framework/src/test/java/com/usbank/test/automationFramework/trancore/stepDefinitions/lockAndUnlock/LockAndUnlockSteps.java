package com.usbank.test.automationFramework.trancore.stepDefinitions.lockAndUnlock;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

public class LockAndUnlockSteps {

    @Autowired
    private StepDefinitionManager stepDefinitionManager;

    @And("the user can either click on Lock Card or Unlock Card Button depending on user account")
    public void theUserCanEitherClickOnLockCardOrUnlockCardButtonDependingOnUserAccount() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getLockAndUnlockPage().getBtnLockOrUnlockCard().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getLockAndUnlockPage().lockOrUnlockCard();
    }

    @Then("the user clicks on the View FAQ Link")
    public void theUserClicksOnTheViewFAQLink() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getLockAndUnlockPage().getLnkViewFAQ().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getLockAndUnlockPage().clickOnViewFAQLink();
    }

    @Then("the user clicks on Report It Lost Or Stolen Link")
    public void theUserClicksOnReportItLostOrStolenLink() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getLockAndUnlockPage().getLnkReportItLostOrStolen().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getLockAndUnlockPage().clickOnReportItLostOrStolen();
    }

    @Then("the user clicks on Return To Services Link")
    public void theUserClicksOnReturnToServicesLink() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getLockAndUnlockPage().getLnkReturnToServices().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getLockAndUnlockPage().clickOnReturnToServices();
    }

    @Then("the user heads to the MyAccount Page to Unlock Card from the Banner")
    public void theUserHeadsToTheMyAccountPageToUnlockCardFromTheBanner() {
        stepDefinitionManager.getPageObjectManager().getLockAndUnlockPage().unlockCardFromBanner();
    }
    @Then("verify the page title for lock and unlock deeplink")
    public void verifyThePageTitleForLockAndUnlockDeeplink() {
        stepDefinitionManager.getPageObjectManager().getssoDeeplinkLoginPage().verifyTitleForLockAndUnlock();
    }

}