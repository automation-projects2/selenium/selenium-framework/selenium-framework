package com.usbank.test.automationFramework.trancore.pageObjects.fraud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PossessCardsPage<T extends WebDriver> {

    protected static final Logger logger = LogManager.getLogger(PossessCardsPage.class);

    protected T driver;


    public PossessCardsPage(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "5024-Y")
    private WebElement rdbYes;

    @FindBy(id = "5024-N")
    private WebElement rdbNo;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement btnContinue;

    @FindBy(xpath = "//*[text()='Where’s your card now?']")
    private WebElement lblPossessCards;

    public WebElement getRdbYes() {
        return rdbYes;
    }

    public WebElement getLblPossessCards() {
        return lblPossessCards;
    }

    public void waitForPossessCardsPageToLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(lblPossessCards));
        wait.until(ExpectedConditions.elementToBeClickable(getRdbYes()));
    }

    public void clickElement(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px solid red'", ele);
        ele.click();
    }

    public void clickYesRadioButton() {
        waitForPossessCardsPageToLoad();
        clickElement(driver, rdbYes);
    }

    public void clickNoRadioButton() {
        waitForPossessCardsPageToLoad();
        clickElement(driver, rdbNo);
    }

    public void clickContinue() {
        waitForPossessCardsPageToLoad();
        clickElement(driver, btnContinue);
    }

}
