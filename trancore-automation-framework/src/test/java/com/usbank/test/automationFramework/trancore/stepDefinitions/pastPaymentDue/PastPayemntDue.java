package com.usbank.test.automationFramework.trancore.stepDefinitions.pastPaymentDue;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import com.usbank.test.driver.DriverManager;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Given;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

@RunWith(Cucumber.class)
public class PastPayemntDue {
	
    @Autowired
    private StepDefinitionManager stepDefinitionManager;

    @Given("business user is on the My Account page")
    public void business_user_is_on_the_My_Account_page() {
    	stepDefinitionManager.getPageObjectManager().getPastPaymentDue().clickMyAccountTab();
    }

    @When("user clicks on Select Account link from past due banner to land on the select account page")
    public void user_clicks_on_Select_Account_link_from_past_due_banner_to_land_on_the_select_account_page() {
    	stepDefinitionManager.getPageObjectManager().getPastPaymentDue().clickSelectAccountLink();
    }

    @When("select Make a payment link for that account needs a payment")
    public void select_Make_a_payment_link_for_that_account_needs_a_payment() {
    	stepDefinitionManager.getPageObjectManager().getPastPaymentDue().clickonMakeAPayementLink();
    }

    @Then("user navigate to the Payments page for the account that needs a payment")
    public void user_navigate_to_the_Payments_page_for_the_account_that_needs_a_payment() {
    	stepDefinitionManager.getPageObjectManager().getPastPaymentDue().validateMessage();
    }  
}