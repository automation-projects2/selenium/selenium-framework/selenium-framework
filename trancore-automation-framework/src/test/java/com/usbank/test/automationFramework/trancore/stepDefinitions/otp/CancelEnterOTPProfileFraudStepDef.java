package com.usbank.test.automationFramework.trancore.stepDefinitions.otp;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class CancelEnterOTPProfileFraudStepDef {

    @Autowired
    StepDefinitionManager stepDefinitionManager;

    @When("the customer clicks on Send one-time passcode at Send OTP Page")
    public void the_customer_clicks_on_Send_one_time_passcode_at_Send_OTP_Page() throws InterruptedException {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getOTPScreenPage().ClickSendOTP();

    }

    @Given("the customer should select the contact (.+) at Send OTP Page")
    public void the_customer_should_select_the_contact_at_Send_OTP_Page(String Contact) throws InterruptedException {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getOTPScreenPage().selectContactForOTP(Contact);
    }


    @When("the customer clicks on Cancel at Enter OTP Page")
    public void the_customer_clicks_on_Cancel_at_Enter_OTP_Page() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getEnterOTPPage().ClickCancel();
    }

    @When("the customer clicks on Send A New Code button at Enter OTP Page")
    public void the_customer_clicks_on_Send_A_New_Code_button_at_Enter_OTP_Page() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getEnterOTPPage().ClickSendANewCode();
    }

    @When("the customer should verify the passcode mismatch error message at enter OTP page")
    public void the_customer_should_verify_the_passcode_mismatch_error_message_at_enter_OTP_page() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getEnterOTPPage().verifyMismatchOTPMsg();
    }

    @When("the customer should verify error message to send a new code at Send OTP page")
    public void the_customer_should_verify_error_message_to_send_a_new_code_at_Send_OTP_page() {
        // Write code here that turns the phrase above into concrete actions
        stepDefinitionManager.getPageObjectManager().getOTPScreenPage().verifyErrorMsg();
    }

}
