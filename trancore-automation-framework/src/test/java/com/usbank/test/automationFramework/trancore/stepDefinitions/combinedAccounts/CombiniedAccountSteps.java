package com.usbank.test.automationFramework.trancore.stepDefinitions.combinedAccounts;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class CombiniedAccountSteps {
    @Autowired
    StepDefinitionManager stepDefinitionManager;

    @When("^the user click the Profile link$")
    public void the_user_click_the_Profile_link() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getCombinedAccountsPage().clickProfileLinkadded();
    }

    @When("^the user click the Select Account tab$")
    public void the_user_click_the_select_account_tab() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getCombinedAccountsPage().clickSelectAccount();
    }
    @When("^the user can add an account$")
    public void the_user_can_add_an_account() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getCombinedAccountsPage().clickAddAAccount();
    }
    @When("^the user can remove an account$")
    public void the_user_can_remove_account() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getCombinedAccountsPage().clickRemoveAccount();
    }
    @And("^the user click on Continue button$")
    public void the_user_click_on_Continue_button() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getCombinedAccountsPage().clickContinue();
    }
    @When("^the user can cancel adding an account$")
    public void the_user_can_cancel_adding_an_account() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getCombinedAccountsPage().clickCancel();
    }
    @When("^the user can view the security code help text$")
    public void the_user_can_view_the_security_code_help_text() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getCombinedAccountsPage().clickImage();
    }
    @Then("^the user can view the contextual help texts$")
    public void the_user_can_view_contextual_help_texts() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getCombinedAccountsPage().clickAllPinImages();
    }
    @Then("^the user can't see the Select Account tab$")
    public void the_user_cant_see_the_select_account_link() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getCombinedAccountsPage().cantSeeSelectAccountTab();
    }
    @When("^the user can view the PIN help text$")
    public void the_user_can_view_the_PIN_help_text() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getCombinedAccountsPage().showPin();
    }
    @When("^the user leave all fields blank$")
    public void the_user_leave_all_fields_blank() throws InterruptedException {

    }
    @When("^the user try to submit adding an account$")
    public void the_user_try_to_submit_adding_an_account() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getCombinedAccountsPage().clickSubmit();
    }

    @When("^the user enter all valid data$")
    public void the_user_enter_all_valid_data() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getCombinedAccountsPage().populateAddAccount("", "");
    }
    @When("^the user enter (.+) as the (.+)$")
    public void the_user_enter_value_as_field(String value, String field) throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getCombinedAccountsPage().populateAddAccount(field, value);
    }
    @And("^the user enter account details form with (.+),(.+),(.+),(.+)$")
    public void the_user_enter_account_details_form(String accountNumber, String cvv, String ssn, String zip) throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getCombinedAccountsPage().fillAccountForm(accountNumber, cvv, ssn, zip);
    }
    @When("^the user can see the (.+) message$")
    public void the_user_can_see_the_message(String message) throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getCombinedAccountsPage().getErrorMessage(message);
    }
    @When("^the user can see the errors$")
    public void the_user_can_see_the_errors(List<String> message) throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getCombinedAccountsPage().getMultipleErrorMessage(message);
    }

    @And("^the user can see the error code (.+)$")
    public void the_user_can_see_the_error_code(String errorCode) throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getCombinedAccountsPage().getErrorCode(errorCode);
    }
    @When("^the user add the same detail$")
    public void the_user_try_same_detail(List<String> data) throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getCombinedAccountsPage().populateEntireAddAccount(data);
    }
    @When("^the user can select the (.+) account$")
    public void the_user_can_select_account(String account) throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getCombinedAccountsPage().openAccountDetails(account);
    }
    @When("^the user can see Account Activity tab$")
    public void the_user_can_see_Account_Activity_tab() throws InterruptedException {
        stepDefinitionManager.getPageObjectManager().getCombinedAccountsPage().openAccountTabs();
    }




}
