package com.usbank.test.automationFramework.trancore.pageObjects.messages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SecureMessagePage<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(SecureMessagePage.class);

    private T driver;

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Messages")
    private WebElement messagesLink;

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Send a Secure Message')]")
    private WebElement sendSecureMessageButton;

    @FindBy(how = How.NAME, using = "messageText")
    private WebElement SecureMessageBody;

    @FindBy(how = How.NAME, using = "subject")
    private WebElement SecureMessageSubject;

    @FindBy(how = How.NAME, using = "Send Message")
    private WebElement SendMessageButton;

    @FindBy(how = How.CLASS_NAME, using = "tieringElement")
    private WebElement SecureMessageSentSuccessful;

    @FindBy(how = How.XPATH, using = "//*[@id='subHeader']/child::span")
    private WebElement readMessagePageOpened;

    @FindBy(how = How.NAME, using = "Delete Message")
    private WebElement deleteMessageFromReadOutPage;

    @FindBy(how = How.NAME, using = "Continue")
    private WebElement deleteMessageFromReadOutPageContinueButton;

    @FindBy(how = How.CLASS_NAME, using = "indent shortBorderLine messageDelete")
    private WebElement selectCheckBox;

    @FindBy(how = How.NAME, using = "Delete Checked Messages")
    private WebElement deletedCheckedMessagesButton;

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Reply')]")
    private WebElement replyButton;

    @FindBy(how = How.NAME, using = "Save Message")
    private WebElement saveMessageButton;


    public SecureMessagePage(T inDriver) {
        driver = inDriver;
        PageFactory.initElements(driver, this);
    }

    public void clickMessagesLink() {

        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(messagesLink));
        messagesLink.click();
    }

    public void clickSendSecureMessage() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(sendSecureMessageButton));
        sendSecureMessageButton.click();
    }

    public void inputSecureMessageDetails(String messageSubject, String messageBody) {
        SecureMessageSubject.sendKeys(messageSubject);
        SecureMessageBody.sendKeys(messageBody);
    }

    public void sendMessage() {
        SendMessageButton.click();
    }

    public void secureMessageSentSuccessful() {
        Assert.assertTrue(SecureMessageSentSuccessful.isDisplayed());
        String str = SecureMessageSentSuccessful.getText();
        String messageNumber = str.substring(39, 45);
        System.out.println("Message ID: " + messageNumber);

    }

    public void readMessage1(String messageSubject) {

        WebElement table = driver.findElement(By.id("MessageThreadForm"));
        int numOfRow = table.findElements(By.tagName("tr")).size();
        String first_part = "//*[@id='MessageThreadForm']/table/tbody/tr[";
        String second_part = "]/td[";
        String third_part = "]";
        for (int i = (numOfRow - 1); i >= 1; i--) {
            String final_xpath = first_part + i + second_part + 3 + third_part;
            String messageID = driver.findElement(By.xpath(final_xpath)).getText();
            if (messageID.equalsIgnoreCase(messageSubject)) {
                String messageNum = driver.findElement(By.xpath(first_part + i + second_part + 2 + third_part)).getText();
                driver.findElement(By.id(messageNum)).click();
                break;
            }
        }
    }

    public void deleteMessageFromReadOutPage() {

        deleteMessageFromReadOutPage.click();
        deleteMessageFromReadOutPageContinueButton.click();

    }

    public void deleteCheckedMessagesButton() {

        //selectCheckBox.click();
        deletedCheckedMessagesButton.click();
        deleteMessageFromReadOutPageContinueButton.click();
    }

    public void readMessage(String messageSubject) {

        WebElement table = driver.findElement(By.id("MessageThreadForm"));
        int numOfRow = table.findElements(By.tagName("tr")).size();
        String first_part = "//*[@id='MessageThreadForm']/table/tbody/tr[";
        String second_part = "]/td[";
        String third_part = "]";
        for (int i = (numOfRow - 1); i >= 1; i--) {
            String final_xpath = first_part + i + second_part + 3 + third_part;
            String messageID = driver.findElement(By.xpath(final_xpath)).getText();
            if (messageID.equalsIgnoreCase(messageSubject)) {
                String messageNum = driver.findElement(By.xpath(first_part + i + second_part + 2 + third_part)).getText();
                driver.findElement(By.partialLinkText(messageNum)).click();
                break;

            }
        }
        Assert.assertTrue(readMessagePageOpened.isDisplayed());
    }


    public void clickReplyButton() {

        replyButton.click();
        System.out.println(readMessagePageOpened.getText());
        Assert.assertTrue(readMessagePageOpened.isDisplayed());

    }

    public void sendSecureMessagePageOpened() {
        System.out.println(readMessagePageOpened.getText());
        Assert.assertTrue(readMessagePageOpened.isDisplayed());
    }

    public void inputMessage(String messageBody) {
        SecureMessageBody.sendKeys(messageBody);
    }

    public void saveMessage() {
        saveMessageButton.click();
    }

    public void verifySavedMessage(String messageSubject) {
        WebElement table = driver.findElement(By.id("MessageThreadForm"));
        int numOfRow = table.findElements(By.tagName("tr")).size();
        String first_part = "//*[@id='MessageThreadForm']/table/tbody/tr[";
        String second_part = "]/td[";
        String third_part = "]";
        for (int i = (numOfRow - 1); i >= 1; i--) {
            String final_xpath = first_part + i + second_part + 3 + third_part;
            String messageID = driver.findElement(By.xpath(final_xpath)).getText();
            if (messageID.equalsIgnoreCase(messageSubject)) {
                String messageSaved = driver.findElement(By.xpath(first_part + i + second_part + 4 + third_part)).getText();
                if (messageSaved.equalsIgnoreCase("Saved"))
                    System.out.println("Message Saved");
                break;
            }
        }
    }

}
