package com.usbank.test.automationFramework.trancore.pageObjects.payments;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Iterator;
import java.util.Set;

public class AutoPaySetupEditDeletePages<T extends WebDriver> {

    private static final Logger logger = LogManager.getLogger(AutoPaySetupEditDeletePages.class);

    private T driver;

    @FindBy(how = How.XPATH, using = "//*[@data-test-id='setUpAutoPay']")
    private WebElement setUpAutoPayButton;

    @FindBy(how = How.XPATH, using = "//*[@id='balance']")
    private WebElement paymentAmountBalance;

    @FindBy(how = How.XPATH, using = "//*[@id='minimum']")
    private WebElement paymentAmountMinDue;

    @FindBy(how = How.XPATH, using = "//*[@name='otherAmount']")
    private WebElement paymentAmountotherAmount;

    @FindBy(how = How.CSS, using = "#currentPaymentDay_inpt")
    private WebElement paymentDay;

    @FindBy(how = How.XPATH, using = "//*[@data-test-id='setUpAutoPayCancel']")
    private WebElement cancelButtonInSetUpAutoPayPage1;

    @FindBy(how = How.XPATH, using = "//*[@data-test-id='setUpAutoPayNext']")
    private WebElement nextButtonInSetUpAutoPayPage1;

    @FindBy(how = How.XPATH, using = "//*[@class='omv-link']")
    private WebElement autoPayTermsAndConditions;

    @FindBy(how = How.XPATH, using = "//*[@name='Accept']")
    private WebElement autoPayTermsAndConditionsAcceptButton;

    @FindBy(how = How.XPATH, using = "//*[@data-test-id='autoPayPreviewSubmit']")
    private WebElement submitButtonInSetUpAutoPayPage2;

    @FindBy(how = How.XPATH, using = "//*[@data-test-id='autoPayPreviewCancel']")
    private WebElement cancelButtonInSetUpAutoPayPage2;

    @FindBy(how = How.XPATH, using = "//*[@data-test-id='autoPayPreviewBack']")
    private WebElement backButtonInSetUpAutoPayPage2;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'AutoPay Terms And Conditions')]")
    private WebElement autoPayTermsAndConditionsHeader;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'Set Up AutoPay - Step 3 of 3')]")
    private WebElement autoPaySetUpPage3of3;

    @FindBy(how = How.XPATH, using = "//*[@id='autoPayOptionsForm']/div[4]/a")
    private WebElement editScheduledPaymentsInAutoPaySetUpPage30f3;

    @FindBy(how = How.XPATH, using = "//*[@data-test-id='viewPaymentSchedule']")
    private WebElement viewScheduledPaymentsInPayments;

    @FindBy(how = How.XPATH, using = "//*[@id=\"payment_schedule_tblCaption\"]/span")
    private WebElement autoPaySetUpInViewPaymentSchedule;

    @FindBy(how = How.XPATH, using = "//*[@id='layoutContentBody']/div/div[4]/div[2]/button")
    private WebElement viewAutoPayButton;
    //*[contains(text(),'View AutoPay')] View AutoPay


    @FindBy(how = How.XPATH, using = "//*[@data-testid='autoPaymentScheduleDelete']")
    private WebElement deleteLinkInAutoPaySetup;

    @FindBy(how = How.XPATH, using = "//*[@id=\"deleteAutoPayDialog\"]/div[3]/input[2]")
    private WebElement deleteAutoPayContinueButton;

    @FindBy(how = How.XPATH, using = "//*[@id='layoutContentErrorMessages'/div/table/tbody/tr[1]/td/table/tbody/tr/td[2]")
    private WebElement deleteAutoPayConfirmationMessage;

    @FindBy(how = How.XPATH, using = "//*[@data-testid='autoPaymentScheduleEdit']")
    private WebElement editAutoPaySetupLink;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'Edit AutoPay - Step 3 of 3')]")
    private WebElement editAutoPayPage3of3Page;

    @FindBy(how = How.XPATH, using = "//*[@id='autoPayOptionsForm']/div[2]/span")
    private WebElement editSuccessfulMessage;

    @FindBy(how = How.XPATH, using = "//*[@id='autoPayOptionsForm']/div[2]/span")
    private WebElement setupAutoPaySuccessfulMessage;

    public AutoPaySetupEditDeletePages(T inDriver) {
        driver = inDriver;

        PageFactory.initElements(driver, this);
    }

    public void clickSetUpAutoPayButton() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(setUpAutoPayButton));
        setUpAutoPayButton.click();
        System.out.println("Setup AutoPay page opened");
    }

    public void selectPaymentAmount(String paymentAmount) {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(paymentAmountBalance));
        if (paymentAmount.equals("Balance")) {
            paymentAmountBalance.click();
        } else if (paymentAmount.equals("Minimum Due")) {
            paymentAmountMinDue.click();
        } else {
            System.out.println("entered Else Condition");

            paymentAmountotherAmount.clear();
            paymentAmountotherAmount.sendKeys(paymentAmount);
        }
        System.out.println("Payment Amount Selected");
    }

    public void selectPaymentDay() {

        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(paymentDay));
        Select day = new Select(paymentDay);
        day.selectByIndex(2);
    }

    public void clickNextInSetUpAutoPayPage1() {
        nextButtonInSetUpAutoPayPage1.click();
        System.out.println("Setup AutoPay 2 of 3 page entered");
    }

    public void acceptAutoPayTermsAndConditions() {

        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(autoPayTermsAndConditions));
        autoPayTermsAndConditions.click();

//  ****** getting window ID's of parent and child windows ******
        Set<String> allWindowHandles = driver.getWindowHandles();
        System.out.println("Number of Windows" + allWindowHandles.size());
        Iterator<String> it = allWindowHandles.iterator();
        String parentWindow = it.next();
        System.out.println("Window ID" + parentWindow);
        String popUp = it.next();
        System.out.println("Window ID" + popUp);

        // **** Switching to Child Window *******
        driver.switchTo().window(popUp);

        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 1000);");
        WebDriverWait wait4 = new WebDriverWait(driver, 20);
        wait4.until(ExpectedConditions.elementToBeClickable(autoPayTermsAndConditionsAcceptButton));
        autoPayTermsAndConditionsAcceptButton.click();

        // *** Switching back to parent window******
        driver.switchTo().window(parentWindow);

    }

    public void submitButtonInSetUpAutoPayPage2of3() {
        WebDriverWait wait2 = new WebDriverWait(driver, 20);
        wait2.until(ExpectedConditions.elementToBeClickable(submitButtonInSetUpAutoPayPage2));
        submitButtonInSetUpAutoPayPage2.click();
    }

    public void theUserNavigatesToSetupAutoPay3Of3Page() {

        WebDriverWait wait2 = new WebDriverWait(driver, 20);
        wait2.until(ExpectedConditions.visibilityOf(autoPaySetUpPage3of3));
        System.out.println(autoPaySetUpPage3of3.isDisplayed());

    }

    public void verifySetupAutoPaySuccessfulMessage() {

        String result = setupAutoPaySuccessfulMessage.getText();
        System.out.println(result);
        if (result.equalsIgnoreCase("Thank you.  Your AutoPay request is complete.  You may want to print this page for your records.")) {
            System.out.println("Edit AutoPay is Successful");
        }
    }

    public void clickOnEditScheduledPaymentsInAutoPaySetUpPage30f3() {

        WebDriverWait wait2 = new WebDriverWait(driver, 20);
        wait2.until(ExpectedConditions.elementToBeClickable(editScheduledPaymentsInAutoPaySetUpPage30f3));
        editScheduledPaymentsInAutoPaySetUpPage30f3.click();
    }

    public void clickViewScheduledPaymentsInPayments() {

        WebDriverWait wait2 = new WebDriverWait(driver, 20);
        wait2.until(ExpectedConditions.elementToBeClickable(viewScheduledPaymentsInPayments));
        viewScheduledPaymentsInPayments.click();
    }

    public void verifyAutoPaySetUpInViewPaymentSchedule() {

        WebDriverWait wait2 = new WebDriverWait(driver, 20);
        wait2.until(ExpectedConditions.visibilityOf(autoPaySetUpInViewPaymentSchedule));
        String text = autoPaySetUpInViewPaymentSchedule.getText();
        System.out.println("TEXT" + text);
        if (text.equals("AutoPay Setup ")) {

            System.out.println("AutoPay Setup is displaying");
        } else {
            System.out.println(" AutoPay Setup is NOT displaying");
        }
//        String text2 = "AutoPay Setup";
//         assertEquals(text,text2);
//
//            System.out.println("AutoPay Setup is displaying");

    }

    public void clickViewAutoPayButton() {

        System.out.println("Entered View AutoPay function");
        WebDriverWait wait2 = new WebDriverWait(driver, 20);
        wait2.until(ExpectedConditions.elementToBeClickable(viewAutoPayButton));
        boolean b = viewAutoPayButton.getText().equalsIgnoreCase("View AutoPay");
        if (b) {
            System.out.println("Clicking View AutoPay button");
            viewAutoPayButton.click();
        } else
            System.out.println("View AutoPay button is not available");
//       try {

//           Assert.assertTrue(b);
//           viewAutoPayButton.click();

        //       }catch (AssertionError err){
//         //err.printStackTrace();
//       }
    }

    public void deleteInAutoPaySetup() {
        WebDriverWait wait2 = new WebDriverWait(driver, 20);
        wait2.until(ExpectedConditions.elementToBeClickable(deleteLinkInAutoPaySetup));
        deleteLinkInAutoPaySetup.click();
    }

    public void deletePopUpInAutoPaySetup() throws InterruptedException {
        WebDriverWait wait4 = new WebDriverWait(driver, 20);
        wait4.until(ExpectedConditions.elementToBeClickable(deleteAutoPayContinueButton));
        deleteAutoPayContinueButton.click();

        Thread.sleep(2000);
        driver.switchTo().defaultContent();

    }

    public void deleteAutoPayConfirmationMessageInPaymentSchedule() throws InterruptedException {

        Thread.sleep(3000);
        System.out.println("Delete confirmation displayed");

    }

    public void clickEditAutoPaySetupLink() {
        WebDriverWait wait4 = new WebDriverWait(driver, 20);
        wait4.until(ExpectedConditions.elementToBeClickable(editAutoPaySetupLink));
        editAutoPaySetupLink.click();
    }

    public void theUserNavigatesToEditAutoPay3Of3Page() {

        WebDriverWait wait2 = new WebDriverWait(driver, 20);
        wait2.until(ExpectedConditions.visibilityOf(editAutoPayPage3of3Page));
        System.out.println(editAutoPayPage3of3Page.isDisplayed());

    }

    public void verifyEditAutoPaySuccessfulMessage() {

        String result = editSuccessfulMessage.getText();
        System.out.println(result);
        if (result.equalsIgnoreCase("Thank you.  Your AutoPay change request is complete.  You may want to print this page for your records.")) {
            System.out.println("Edit AutoPay is Successful");
        }
    }

    public void errorEnterDecimalValueinAutopay() {
        nextButtonInSetUpAutoPayPage1.click();
        System.out.println("The Payment you entered is invalid error message is dispalyed");
    }

    public void errorEnterPaymentDayValueinAutopay() {
        nextButtonInSetUpAutoPayPage1.click();
        System.out.println("Please select the payment day error message is dispalyed");
    }

    public void errorDonotAcceptsTermsandConditionsinAutopay() {
        nextButtonInSetUpAutoPayPage1.click();
        submitButtonInSetUpAutoPayPage2of3();
        System.out.println("Please view terms and conditions error message is dispalyed");
    }
}
