package com.usbank.test.automationFramework.trancore.stepDefinitions.profile;

import com.usbank.test.automationFramework.trancore.stepDefinitions.StepDefinitionManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

public class ChangePersonalIDSteps {

    @Autowired
    StepDefinitionManager stepDefinitionManager;

    @And("I go to change your personal ID")
    public void i_go_to_change_your_personal_ID() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getChangePersonalIDPage().getChangeYourPersonalIDlink().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getChangePersonalIDPage().clickChangeYourPersonalIDlink();
        stepDefinitionManager.getPageObjectManager().getChangePersonalIDPage().getShieldQuestionAnswer();

    }

    @And("^I enter (.+) as my new personal id$")
    public void i_enter_qwerty_as_my_new_personal_id(String newPersonalId) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getChangePersonalIDPage().getNewPersonalID().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getChangePersonalIDPage().enterPersonalID(newPersonalId);
    }

    @And("^I enter (.+) as my confirm new personal id$")
    public void i_enter_qwerty_as_my_confirm_new_personal_id(String ConfNewPersonalId) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getChangePersonalIDPage().getConfirmNewPersonalID().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getChangePersonalIDPage().enterConfirmNewPersonalID(ConfNewPersonalId);

    }

    @And("^I enter (.+) as my password$")
    public void i_enter_qwerty_as_my_password(String password) {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getChangePersonalIDPage().getPassword().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getChangePersonalIDPage().enterPassword(password);

    }

    @When("I try to submit my personal id change")
    public void i_try_to_submit_my_personal_id_change() {
        Assert.assertTrue(stepDefinitionManager.getPageObjectManager().getChangePersonalIDPage().getBtnSubmit().isDisplayed());
        stepDefinitionManager.getPageObjectManager().getChangePersonalIDPage().clickSubmitButton();

    }

    @Then("I can see the personal ID changed success message")
    public void i_can_see_the_personal_ID_changed_success_message() {
        stepDefinitionManager.getPageObjectManager().getChangePersonalIDPage().checkSuccessMessage();
        stepDefinitionManager.getPageObjectManager().getChangePersonalIDPage().clickReturnToProfileLink();

    }

    @Then("I can see the error message for blank new personal id")
    public void i_can_see_the_error_message_for_blank_new_personal_id() {
        stepDefinitionManager.getPageObjectManager().getChangePersonalIDPage().checkErrorMessageForBlankNewPersonalId();
    }


    @Then("I can see the error message for blank confirm new personal id")
    public void i_can_see_the_error_message_for_blank_confirm_new_personal_id() {
        stepDefinitionManager.getPageObjectManager().getChangePersonalIDPage().checkErrorMessageForBlankConfirmNewPersonalId();
    }

    @Then("I can see the error message for blank password")
    public void i_can_see_the_error_message_for_blank_new_password() {
        stepDefinitionManager.getPageObjectManager().getChangePersonalIDPage().checkErrorMessageForBlankPassword();
    }

    @Then("I can see the error message for wrongly typed password")
    public void i_can_see_the_error_message_for_wrongly_typed_password() {
        stepDefinitionManager.getPageObjectManager().getChangePersonalIDPage().checkErrorMessageForWrongPassword();
    }

    @Then("I can see the error message for short length personal id")
    public void i_can_see_the_error_message_for_short_length_personal_id() {
        stepDefinitionManager.getPageObjectManager().getChangePersonalIDPage().checkErrorMessageForShortPersonalId();
    }

    @Then("I can see the error message for reused personal id")
    public void i_can_see_the_error_message_for_reused_personal_id() {
        stepDefinitionManager.getPageObjectManager().getChangePersonalIDPage().checkErrorMessageForReusedPersonalId();
    }

    @Then("I can see the error message for personal id having not allowed special character")
    public void i_can_see_the_error_message_for_personal_id_having_not_allowed_special_character() {
        stepDefinitionManager.getPageObjectManager().getChangePersonalIDPage().checkErrorMessageForNotAllowedSpecialCharacter();
    }

    @Then("I can see the error message for personal id and confirm new personal id not matching")
    public void I_can_see_the_error_message_for_personal_id_and_confirm_new_personal_id_not_matching() {
        stepDefinitionManager.getPageObjectManager().getChangePersonalIDPage().checkErrorMessageForBlankConfirmNewPersonalId();

    }
}

