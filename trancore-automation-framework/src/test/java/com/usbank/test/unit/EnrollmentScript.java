package com.usbank.test.unit;

import com.usbank.test.automationFramework.trancore.pageObjects.PageObjectManager;
import com.usbank.test.automationFramework.trancore.pageObjects.enrollment.SecurityQA;
import com.usbank.test.component.SpringConfiguration;
import com.usbank.test.component.TestDataManager;
import com.usbank.test.enums.BrowserType;
import com.usbank.test.objects.EnrollmentAccount;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfiguration.class)
public class EnrollmentScript {
    private static final Logger logger = LogManager.getLogger(EnrollmentScript.class);

    @Autowired
    private PageObjectManager pageObjectManager;

    @Autowired
    private TestDataManager testDataManager;

    @Autowired
    private AutomationConfig automationConfig;

    private Properties enrollmentConfigProps;

    private Map performanceTestSecQAS;

    private final static String URL_KEY = "URL";
    private final static String SECURITY_QUESTIONANSWERS_SOURCE_KEY = "SecurityQASource";
    private final static String INPUTFILENAME="AccountInput.csv";
    private final static String OUTPUTFILENAME="AccountOutput.csv";
    private final static String USERNAME_PREFIX="UsernamePrefix";
    private final static String PASSWORD="Password";



    @Before
    public void intialize() throws IOException {
        pageObjectManager.initPageObjectDriver(RunMode.valueOf(automationConfig.getRunMode()), BrowserType.valueOf(automationConfig.getBrowserType()));

        File currentDirFile = new File(".");
        String helper = currentDirFile.getAbsolutePath();

        InputStream enrollmentConfigStream = new FileInputStream("src/test/resources/enrollmentConfig.properties");
        enrollmentConfigProps = new Properties();
        enrollmentConfigProps.load(enrollmentConfigStream);
        performanceTestSecQAS = new HashMap();

        if(enrollmentConfigProps.getProperty(SECURITY_QUESTIONANSWERS_SOURCE_KEY).equalsIgnoreCase("performancetesting")){
            String fileName="src/test/resources/performanceTestingSecQA.txt";

            List<String> lines = FileUtils.readLines(new File(fileName), "UTF-8");
            for (String line: lines) {
                String[] qa = line.split("=");
                performanceTestSecQAS.put(qa[0], qa[1]);
            }
        }

    }



@Test
    public void enrollAccounts() throws IOException {


        //URL where the accounts will be enrolled
        final String baseURL = enrollmentConfigProps.getProperty(URL_KEY);

         //Prefix to add to username. Format for Username is usernamePrefix + Last four digits of Account Number
         final String usernamePrefix = enrollmentConfigProps.getProperty(USERNAME_PREFIX);

         //Trancore Account Password
         final String password = enrollmentConfigProps.getProperty(PASSWORD);

         final String securityQAsource = enrollmentConfigProps.get(SECURITY_QUESTIONANSWERS_SOURCE_KEY).toString();

         logger.info("\nEnrollment Script Configuration Settings\nEnrollment Page URL: "+baseURL+"\n"+"Answer for Security Question: "+ ((securityQAsource.equalsIgnoreCase("performancetesting")?" Answer from performanceTestingSecQA.txt file ": "Last word of Security question"))+"\n"+"Username Prefix: "+usernamePrefix+"\n"+"Password for account: "+password+"\n");

        //Parse Input data into a list of Enrollment Accounts
        List<EnrollmentAccount> accountList = parseInputAccountFile(INPUTFILENAME, ',', usernamePrefix, password);

        try {
            //Loop
            for (EnrollmentAccount account : accountList) {

                System.out.println("Enrolling account: " + account.getAccountNumber());

                pageObjectManager.getEnrollmentPage().navigateToEnrollment(baseURL);
                pageObjectManager.getEnrollmentPage().populateEnrollForm(account);
                pageObjectManager.getEnrollmentPage().submitEnrollmentForm();

                int counter = 1;
                int timeout = 3;
                String currentUrl;

                while (counter < timeout) {

                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    currentUrl = pageObjectManager.getDriver().getCurrentUrl();
                    if (currentUrl.contains("esignAgreement")) {
                        System.out.println("Current URL: " + pageObjectManager.getDriver().getCurrentUrl());
                        System.out.println("Account enrolled: " + account.getPersonalId());

                        pageObjectManager.getESignAgreementPage().isEsignAgreementLinkExists();
                        pageObjectManager.getESignAgreementPage().clickOnViewEsignConsentAgreement();

                        //Enroll ID Shield
                        pageObjectManager.getIdShieldPage().navigateToEnrollIDShield();
                        pageObjectManager.getIdShieldPage().waitForIDShieldPageToLoad();
                        pageObjectManager.getIdShieldPage().clickElement(pageObjectManager.getDriver(), pageObjectManager.getIdShieldPage().getMoreQuestionsLink());
                        answerSecurityQuestions();
                        pageObjectManager.getIdShieldPage().getAlwaysAskMe().click();
                        pageObjectManager.getDriver().findElement(By.id("next")).click();

                        pageObjectManager.getIdShieldPage().selectIdShieldImageAndPhrase(password);
                        pageObjectManager.getDriver().findElement(By.id("next")).click();

                        pageObjectManager.getIdShieldPage().waitforFinalizeImagePageToLoad();
                        pageObjectManager.getDriver().findElement(By.name("Submit")).click();

                        pageObjectManager.getIdShieldPage().waitforGoToTransactionDetailsBtnPageToLoad();
                        pageObjectManager.getDriver().findElement(By.name("Go To Transaction Details")).click();

                        break;
                    } else {
                        try {
                            if (pageObjectManager.getEnrollmentPage().getErrorCode().isDisplayed()) {
                                System.out.println("Error code displayed: " + pageObjectManager.getEnrollmentPage().getErrorCode().getText());

                                account.setPersonalId("Not enrolled");

                                account.setErrorCode(pageObjectManager.getEnrollmentPage().getErrorCode().getText());

                                break;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            System.out.println("Page still loading or Error code not available. Retrying. Please wait. We will time out on this account after couple of attempts.");
                        }
                    }

                    counter = counter + 1;

                    if (counter == timeout - 1) {
                        account.setErrorCode("Something went wrong when enrolling this account!");

                    }
                }


            }
        }catch(Exception e){
            e.printStackTrace();
        }


        //Write output to file
        writeToOutputFile(OUTPUTFILENAME, accountList);


    }

    private void writeToOutputFile(String fileName, List<EnrollmentAccount> accountList) {

        try {

            Writer writer = Files.newBufferedWriter(Paths.get(fileName));
            CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader("Account Number", "SSN", "cvv", "ZipCode", "Username", "Error Code"));

            for (EnrollmentAccount account : accountList) {

                ArrayList<String> accountData = new ArrayList<String>();
                accountData.add(account.getAccountNumber());
                accountData.add(account.getSsn());
                accountData.add(account.getSecurityCode());
                accountData.add(account.getZipCode());
                accountData.add(account.getPersonalId());
                accountData.add(account.getErrorCode());
                csvPrinter.printRecord(accountData);
            }

            csvPrinter.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private List<EnrollmentAccount> parseInputAccountFile(String filename, char delimiter, String usernamePrefix, String password) {


        CSVFormat format = CSVFormat.RFC4180.withHeader().withDelimiter(delimiter);

        CSVParser parser = null;

        List<EnrollmentAccount> accountList = new ArrayList<EnrollmentAccount>();
        try {

            File file = new File(filename);
            parser = new CSVParser(new FileReader(file), format);

            for (CSVRecord record : parser) {
                EnrollmentAccount account = new EnrollmentAccount();
                account.setAccountNumber(record.get("Account Number"));
                account.setSsn(record.get("SSN"));
                account.setSecurityCode(record.get("Signature Code (CVV/CSC-3)"));
                account.setZipCode(record.get("ZipCode"));
                String username = usernamePrefix + account.getAccountNumber().substring(account.getAccountNumber().length() - 4);
                account.setPersonalId(username);
                account.setPassword(password);
                accountList.add(account);
            }
            //close the parser
            parser.close();

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        return accountList;
    }

    public void answerSecurityQuestions() throws IOException {


        List<WebElement> checkBoxElementList = pageObjectManager.getDriver().findElements(By.cssSelector("[id^=checkbox]"));
        List<WebElement> questionElementList = pageObjectManager.getDriver().findElements(By.cssSelector("[id^=questionText]"));
        List<WebElement> answerElementList = pageObjectManager.getDriver().findElements(By.cssSelector("[id^=answer0x]"));

        List<SecurityQA> securityQAList = new ArrayList<SecurityQA>();

        //Set to store answers that have been entered so far
        HashSet<String> answerSet = new HashSet<String>();

        //logger.info("checkboxelementlist size: "+checkBoxElementList.size()  + "; questionElementList Size: "+questionElementList.size()+"; answerElementSize: "+answerElementList.size());

        //Count to keep track of how many questions have been answered so far.
        int answeredCount=0;

        //Looping through all questions on the page
        for(int i=0; i<checkBoxElementList.size(); i++){

            //Create SecurityQA object with checkbox, question, and answer elements
            SecurityQA securityQA = new SecurityQA(checkBoxElementList.get(i), questionElementList.get(i), answerElementList.get(i));
            securityQAList.add(securityQA);
            String question = securityQA.getQuestion().getText();

            //We are ignoring few questions because, we cannot use last word for those questions.
            if(!getIgnoredQuestions().contains(question)){

                String answer;

                if(enrollmentConfigProps.get(SECURITY_QUESTIONANSWERS_SOURCE_KEY).equals("performancetesting")){
                    answer = performanceTestSecQAS.get(question).toString();
                    logger.info("Answer: "+answer);
                }else {
                    answer = question.substring(question.lastIndexOf(" ") + 1, question.length() - 1);
                }
                //logger.info("Question: "+question + "; Answer: "+answer);

                //If answer has already been used for a different question, ignore this question
                if(!answerSet.contains(answer)){
                    securityQA.getCheckbox().click();
                    securityQA.getAnswer().sendKeys(answer);
                    answerSet.add(answer);
                    answeredCount = answeredCount+1;

                    //If we answered enough questions already, skip the rest
                    if(answeredCount==3) {
                        //logger.info("Answered 3 questions");
                        break;
                    }
                }
            }

        }
    }

    /**
     * Returns Set of ignored questions, as last word cannot be used as answer for these questions.
     *
     * @return Set of ignored questions
     */
    private HashSet<String> getIgnoredQuestions() {

        HashSet<String> ignoredQuestions;

        String[] ignoredQuestionStrings = {"What are the last five digits of your favorite credit card?",
                "What are the last five digits of your favorite frequent flyer card?",
                "What are the last five digits of your student id?", "What is the month and day of your anniversary?",
                "What is the month and year of your birth?",
                "What is your phone number?",
                "What is your work email address?",
                "What time of day were you born?",
                "What was the street number of the house in which you grew up?",
                "What date did you get married?",
                "What year was your eldest child born?",
                "In what year the eldest sibling born?"
        };
        ignoredQuestions = new HashSet<>(Arrays.asList(ignoredQuestionStrings));

        return ignoredQuestions;
    }


}
