package com.usbank.test.enums;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Enum for Date Formats.
 */

public enum DateFormat {
    MM_DD_YYYY("MM/dd/yyyy"),
    EEE_MMM_D_YYYY("EEEE, MMMM d, yyyy"),
    EEE_MMM_D("EEEE, MMM. d"),
    EEE_MMM_DD("EEEE, MMM. dd"),
    YYYYMMDD_T_HHMMSS_MMM("yyyy-MM-ddTHH:mm:ss:mmm"),
    YYYYMMDD_HHMMSS_Z("yyyy-MM-dd HH:mm:ss 'Z'"),
    YYYYMMDD_T_HHMMSS_SSS_Z("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"),
    YYYYMMDD_T_HHMMSSSSSZ("yyyy-MM-dd'T'HH:mm:ss.SSSZ"),
    YYYYMMDD_T_HHMMSS("yyyy-MM-dd'T'HH:mm:ss"),
    YYYYMMDD_T_HHMM("yyyy-MM-dd'T'HH:mm"),
    YYYYMMDD_T_HHMMSSX("yyyy-MM-dd'T'HH:mm:ssX"),
    YYYYMMDD_T_HHMMSSZ("yyyy-MM-dd'T'HH:mm:ss'Z'"),
    YYYYMMDD_T_HHMMSSSSSSSS("yyyy-MM-dd'T'HH:mm:ss.SSSSSS"),
    YYYY_MM_DD("yyyy-MM-dd"),
    MM_DD_YYYY_HH_MM_SS_A("MM/dd/yyyy hh:mm:ss a"),
    M_D_YYYY_H_MM_SS_A("M/d/yyyy h:mm:ss a"),
    M_D_YYYY("M/d/yyyy"),
    M_D_YY("M/d/yy"),
    DD_MMM_YYYY("dd MMM yyyy"),
    DAY_MONTH_DATE("EEEE, MMMM d"),
    MONTH_DAY_YEAR_FORMAT_ABBREVIATED("MMM. d, yyyy"),
    MONTH_DAY_YEAR_FORMAT("MMMM d, yyyy"),
    MMM_DOT_D_H_MM_A("MMM. d, h:mm a"),
    MMM_D_H_MM_A("MMM d, h:mm a"),
    MMMM_D_H_MM_A("MMMM d, h:mm a"),
    H_MM_A("h:mm a"),
    MMM_DD_YYYY("MMM dd, yyyy"),
    YYYYMM("yyyyMM"),
    MMDDYYYY("MMddYYYY"),
    MM_DD_YY("MM/dd/yy"),
    YYYY_MM_DD_HH_MM_SSSSSSSS("yyyy-MM-dd-HH.mm.ss.SSSSSS"),
    MMM_YYYY("MMM, YYYY");

    private final String value;

    DateFormat(String s) {
        value = s;
    }

    DateFormat() {
        value = "";
    }

    /**
     * Parse the date string and returns the date object
     */
    public Date parse(final String date) throws ParseException {
        return new SimpleDateFormat(this.value, Locale.ENGLISH).parse(date);
    }

    /**
     * Formats the Date object to String
     */
    public String format(final Date date) {
        return new SimpleDateFormat(this.value, Locale.ENGLISH).format(date);
    }
}
