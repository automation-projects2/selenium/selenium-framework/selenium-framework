package com.usbank.test.enums;

public enum OperatingSystem {
    WIN, LINUX, MAC
}
