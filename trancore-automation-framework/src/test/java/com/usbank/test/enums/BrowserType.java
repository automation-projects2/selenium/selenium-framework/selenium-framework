package com.usbank.test.enums;

import java.util.Arrays;

public enum BrowserType {
    FIREFOX,
    CHROME,
    HEADLESS,
    IE,
    EDGE,
    OPERA,
    SAFARI,
    ANDROID_BROWSER,
    IOS_BROWSER,
    ANDROID_NATIVE_APPLICATION,
    IOS_NATIVE_APPLICATION;

    public static BrowserType fromString(String name) {
        return Arrays.stream(BrowserType.values())
                .filter(x -> x.toString().equalsIgnoreCase(name))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Cannot find BrowserType enum for :" + name));
    }

}
