package com.usbank.test.enums;

public enum OperatingSystemArchitecture {
    DEFAULT, X32, X64
}
