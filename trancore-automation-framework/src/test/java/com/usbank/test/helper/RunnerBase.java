package com.usbank.test.helper;


import org.junit.AfterClass;

import java.io.IOException;

/**
 * Class to add some cucumber reporting features
 * Integration with TestNG
 */

public abstract class RunnerBase {


    public static String reportFolder = System.getProperty("user.dir") + "\\cucumber-reports\\";

    /**
     * Initializes the test items
     *
     * @throws IOException
     */

    public void setUp() throws IOException {

    }


    /**
     * Generates the Cucumber Reports report
     *
     * @throws Exception
     */
    public static void generateCucumberReport() {

        //todo: implement me
    }

    /**
     * Executes any items for after the entire test suite has run
     *
     * @throws Exception
     */
    public void cleanUpReports() {
        //todo: implement me
    }

    /**
     * Junit After suite cleanup items
     */
    @AfterClass
    public static void endSuite() {
        //todo: implement me
    }
}
