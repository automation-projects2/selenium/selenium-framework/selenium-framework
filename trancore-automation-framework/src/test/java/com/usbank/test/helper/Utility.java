package com.usbank.test.helper;

import java.net.HttpURLConnection;
import java.net.URL;

public class Utility {

    // The below function verifyLink(String urlLink) verifies any broken links and return the server status Message.
    public static String verifyLink(String urlLink) {
        String linkStatus = null;
        //Sometimes we may face exception "java.net.MalformedURLException". Keep the code in try catch block to continue the broken link analysis
        try {
            //Use URL Class - Create object of the URL Class and pass the urlLink as parameter
            URL link = new URL(urlLink);
            // Create a connection using URL object (i.e., link)
            HttpURLConnection httpConn = (HttpURLConnection) link.openConnection();
            //Set the timeout for 2 seconds
            httpConn.setConnectTimeout(2000);
            //connect using connect method
            httpConn.connect();
            //use getResponseCode() to get the response code.
            //200 – Valid Link OK
            if (httpConn.getResponseCode() == 200) {
                System.out.println(urlLink + " - " + httpConn.getResponseMessage());
            }
            //404 – Link not found
            if (httpConn.getResponseCode() == 404) {
                System.out.println(urlLink + " - " + httpConn.getResponseMessage());
            }
            //400 – Bad request
            if (httpConn.getResponseCode() == 400) {
                System.out.println(urlLink + " - " + httpConn.getResponseMessage());
            }
            //401 – Unauthorized
            if (httpConn.getResponseCode() == 401) {
                System.out.println(urlLink + " - " + httpConn.getResponseMessage());
            }
            //500 – Internal Error
            if (httpConn.getResponseCode() == 500) {
                System.out.println(urlLink + " - " + httpConn.getResponseMessage());
            }

            linkStatus = httpConn.getResponseMessage();
        }
        //getResponseCode method returns = IOException - if an error occurred connecting to the server.
        catch (Exception e) {
            //e.printStackTrace();
        }

        return linkStatus;
    }
}
