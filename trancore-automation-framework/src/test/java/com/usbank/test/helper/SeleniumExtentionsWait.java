package com.usbank.test.helper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.*;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class SeleniumExtentionsWait extends SeleniumExtentionsCSSSelector {

    private static final Logger logger = LogManager.getLogger(SeleniumExtentionsWait.class);

    private WebDriver driver;
    private final Integer timeUnit = 30;

    /* NOTE:
    * Implicit Wait works at the WebDriver level
    * Explicit wait works at the WebElement level
     */

    /*
     * This method will wait for the page to load until it has the visibility of the web-element
     */
    public void waitExplicitlyForAnElementToLoad(WebElement someWebElement) {
        if (!someWebElement.isDisplayed()) {
            driver.manage().timeouts().pageLoadTimeout(timeUnit, TimeUnit.SECONDS);
            WebDriverWait wait = new WebDriverWait(driver, timeUnit);
            wait.until(ExpectedConditions.visibilityOf(someWebElement));
        }
    }

    public void explicitWaitForAWebElement(WebElement someWebElement){
        new WebDriverWait(driver, timeUnit).until(ExpectedConditions.visibilityOf(someWebElement));
    }

    public void waitForPageToLoad(){
        driver.manage().timeouts().implicitlyWait(timeUnit, TimeUnit.SECONDS);
    }

    /*
     * This method will wait until the page has loaded, if the web-element is being displayed
     * otherwise it will wait for the page to load N'Number of seconds
     */
    public void waitImplicitlyForPageToLoad(WebElement someWebElement, Integer threadSleepTime) throws InterruptedException {
        int max = 3;
        int counter = 0;
        boolean pageLoaded = false;

        while (!pageLoaded && counter < max) {
            if (someWebElement.isDisplayed()) {
                pageLoaded = true;
            } else {
                Thread.sleep(threadSleepTime);
                counter++;
            }
        }
    }

    /*
     * This method will check to see if the loaded/loading page request is complete
     * and place the driver to sleep & upon fail exception error handling is thrown
     */
    @SuppressWarnings("Convert2Lambda")
    public void waitForTimeoutPageLoadRequestToComplete(Integer threadSleepTime) {
        ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
            }
        };
        try {
            Thread.sleep(threadSleepTime);
            WebDriverWait wait = new WebDriverWait(driver, timeUnit);
            wait.until(expectation);
        } catch (Throwable error) {
            Assert.fail("Timeout waiting for Page Load Request to complete.");
        }
    }

    /*
     * This method will check to see if the loaded/loading page request is complete
     * and will wait until the condition has been met
     */
    @SuppressWarnings("Convert2Lambda")
    public void waitForPageRequestToComplete(WebDriver driver) {
        ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
            }
        };
        WebDriverWait wait = new WebDriverWait(driver, timeUnit);
        wait.until(pageLoadCondition);
    }

    /*
     * This method waits for the visibility of the web element text on a loaded page
     */
    public boolean waitForVisibilityOfText(WebElement someWebElement, Integer explicitWaitTimeSeconds, String someText) {
        new WebDriverWait(driver, explicitWaitTimeSeconds).until(ExpectedConditions.visibilityOf(someWebElement));
        String webElementText = someWebElement.getText();
        if (webElementText.equals(someText)) {
            return true;
        }
        return false;
    }


    public static WebElement waitForLoadByName(WebDriver driver, String name) {
        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(10))
                .pollingEvery(Duration.ofSeconds(1))
                .ignoring(NoSuchElementException.class);

        WebElement foo = wait.until(d -> driver.findElement(By.name(name)));
        return foo;
    }

    public static WebElement waitForLoadById(WebDriver driver, String id) {
        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(10))
                .pollingEvery(Duration.ofSeconds(1))
                .ignoring(NoSuchElementException.class);

        WebElement foo = wait.until(d -> driver.findElement(By.id(id)));
        return foo;
    }


    public static WebElement waitForLoadByXpath(WebDriver driver, String xpath) {
        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(10))
                .pollingEvery(Duration.ofSeconds(1))
                .ignoring(NoSuchElementException.class);

        WebElement foo = wait.until(d -> driver.findElement(By.xpath(xpath)));
        return foo;
    }

    public static WebElement waitForLoadByLinkText(WebDriver driver, String linkText) {
        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(10))
                .pollingEvery(Duration.ofSeconds(1))
                .ignoring(NoSuchElementException.class);

        WebElement foo = wait.until(d -> driver.findElement(By.linkText(linkText)));
        return foo;
    }


    public static java.util.List<WebElement> waitForTablePresent(WebDriver driver, String tableSelector, String rowSelector) {
        java.util.List<WebElement> rows = null;
        for (int i = 0; i < 200; i++) {
            WebElement table = getElementBySelector(driver, tableSelector, 30);
            rows = table.findElements(By.cssSelector(rowSelector));
            if (rows.size() == 0) {
                sleep(500);
                continue;
            }
            break;
        }
        return rows;
    }

    public static WebElement waitForWebElementPresent(WebDriver driver, String cssSelector) {

        WebElement returnElement = null;

        for (int i = 0; i < 200; i++) {
            try {
                returnElement = driver.findElement(By.cssSelector(cssSelector));
                break;
            } catch (Exception ex) {
                sleep(500);
            }
        }
        return returnElement;
    }

    public static java.util.List<WebElement> waitForWebElementsPresent(WebDriver driver, String cssSelector) {

        java.util.List<WebElement> returnElement = null;

        for (int i = 0; i < 200; i++) {
            try {
                returnElement = getElementsBySelector(driver, cssSelector);
                break;
            } catch (Exception ex) {
                sleep(500);
            }
        }
        return returnElement;
    }
}
