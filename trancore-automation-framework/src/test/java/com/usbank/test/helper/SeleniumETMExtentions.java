package com.usbank.test.helper;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

@SuppressWarnings({"unused", "JavaDoc"})
public class SeleniumETMExtentions {

    public WebDriver oDriver = null;
    private WebElement globalElement = null;

    private static int iTimeOut = 240;
    private static String originalWindow;
    private static String g_sDriverLocation = Paths.get(".").toAbsolutePath().normalize().toString();
    public static final String c_sObjectNotEnabled = "Object not enabled!";
    public static final String c_sObjectNotFound = "Object not found in application under test!";
    public static final String c_sIndexIndicator = "=index";
    private static final String c_sTableCellXPath = ".//td | .//th";
    public static final String c_sCollectionIndicator = "|collection|";
    private Point lastPosition;
    private Dimension lastSize;

    // Constructors
    public SeleniumETMExtentions(int timeOut) {
        iTimeOut = timeOut;
    }

    public SeleniumETMExtentions() {
    }

    /**
     * Creates a local machine instance of a Selenium WebDriver in the desired browser
     *
     * @param sBrowser String - Browser to open. IE, Firefox, Chrome, Safari, Edge, etc
     * @throws Exception
     */
    public void createLocalDriver(String sBrowser) throws Exception {
        createLocalDriver(sBrowser, new HashMap<String, Object>());
    }

    /**
     * Creates a local machine instance of a Selenium WebDriver in the desired browser
     *
     * @param sBrowser String - Browser to open. IE, Firefox, Chrome, Safari, Edge, etc
     * @throws Exception
     */
    public void createLocalDriver(String sBrowser, HashMap<String, Object> oPreferences) throws Exception {
        int maxTries = 3;
        int count = 0;
        try {
            switch (sBrowser.toUpperCase().trim()) {
                case "FF":
                case "FIREFOX": {
                    FirefoxOptions oOptions = new FirefoxOptions();
                    for (String sKey : oPreferences.keySet()) {
                        String sType = oPreferences.get(sKey).getClass().getName();
                        if (sType.contains("Boolean")) {
                            oOptions.addPreference(sKey, (Boolean) oPreferences.get(sKey));
                        } else if (sType.contains("String")) {
                            oOptions.addPreference(sKey, (String) oPreferences.get(sKey));
                        } else {
                            oOptions.addPreference(sKey, (int) oPreferences.get(sKey));
                        }
                    }
                    // oOptions.setLegacy(true);
                    System.setProperty("webdriver.gecko.driver", g_sDriverLocation + "/geckodriver.exe");
                    while (true) {
                        try {
                            oDriver = new FirefoxDriver(oOptions);
                            break;
                        } catch (Exception e) {
                            if (++count == maxTries) throw e;
                        }
                    }
                    break;
                }
                case "IE":
                case "INTERNETEXPLORER": {
                    InternetExplorerOptions oOptions = new InternetExplorerOptions();
                    System.setProperty("webdriver.ie.driver", g_sDriverLocation + "/IEDriverServer.exe");
                    for (String sKey : oPreferences.keySet()) {
                        String sType = oPreferences.get(sKey).getClass().getName();
                        if (sType.contains("Boolean")) {
                            oOptions.setCapability(sKey, (Boolean) oPreferences.get(sKey));
                        } else if (sType.contains("String")) {
                            oOptions.setCapability(sKey, (String) oPreferences.get(sKey));
                        } else {
                            oOptions.setCapability(sKey, (int) oPreferences.get(sKey));
                        }
                    }
                    while (true) {
                        try {
                            oDriver = new InternetExplorerDriver(oOptions);
                            break;
                        } catch (Exception e) {
                            if (++count == maxTries) throw e;
                        }
                    }
                    break;
                }
                case "CHROME": {
                    System.setProperty("webdriver.chrome.driver", g_sDriverLocation + "/chromedriver.exe");
                    ChromeOptions oOptions = new ChromeOptions();

                    // Enable flash, some sites need it
                    oOptions.addArguments("--disable-features=EnableEphemeralFlashPermission");

                    // MUST send a List<String> of needed arguments in the oPreferences HashMap
                    if (oPreferences.containsKey("addArguments")) {
                        @SuppressWarnings("unchecked")
                        List<String> arguments = (List<String>) oPreferences.get("addArguments");
                        oOptions.addArguments(arguments);
                        oPreferences.remove("addArguments");
                    }

                    // Uncomment the following line if you want to load the user's
                    // profile instead of a clean Chrome session
                    // oOptions.addArguments("--user-data-dir=C:\\Users\\" +
                    // System.getProperty("user.name").toString() +
                    // "\\AppData\\Local\\Google\\Chrome\\User Data");

                    oOptions.setExperimentalOption("prefs", oPreferences);

                    while (true) {
                        try {
                            oDriver = new ChromeDriver(oOptions);
                            break;
                        } catch (Exception e) {
                            if (++count == maxTries) throw e;
                        }
                    }
                    break;
                }
                case "SAFARI": {
                    // System.setProperty("webdriver.safari.driver",
                    // g_sDriverLocation + "\\geckodriver.exe");
                    while (true) {
                        try {
                            oDriver = new SafariDriver();
                            break;
                        } catch (Exception e) {
                            if (++count == maxTries) throw e;
                        }
                    }
                    break;
                }
                case "EDGE": {
                    System.setProperty("webdriver.edge.driver", g_sDriverLocation + "/MicrosoftWebDriver.exe");
                    while (true) {
                        try {
                            oDriver = new EdgeDriver();
                            break;
                        } catch (Exception e) {
                            if (++count == maxTries) throw e;
                        }
                    }
                    break;
                }

                default:
                    throw new Exception("Invalid browser option " + sBrowser);
            }
        } catch (Exception oException) {
            throw new Exception(oException.getMessage());
        }
        oDriver.manage().timeouts().pageLoadTimeout(iTimeOut, TimeUnit.SECONDS);
        originalWindow = oDriver.getWindowHandle();
    }

    /**
     * Creates a remote instance, on a US Bank machine, of a Selenium WebDriver in the desired browser.
     *
     * @param sRemoteServer String - The path to the remote machine, http://<MachineName>:4444/wd/hub
     * @param sBrowser      String - Browser to open. IE, Firefox, Chrome, Safari, Edge, etc
     * @throws Exception
     */
    public void createRemoteDriver(String sRemoteServer, String sBrowser) throws Exception {
        Capabilities browserOptions;

        switch (sBrowser.toUpperCase().trim()) {
            case "FF":
            case "FIREFOX":
                browserOptions = new FirefoxOptions();
                break;
            case "CHROME":
                browserOptions = new ChromeOptions();
                break;
            case "IE":
            case "INTERNET EXPLORER":
                InternetExplorerOptions ieOptions = new InternetExplorerOptions();
                ieOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                browserOptions = ieOptions;
                break;
            case "SAFARI":
                browserOptions = new SafariOptions();
                break;
            case "EDGE":
                browserOptions = new EdgeOptions();
                break;
            default:
                browserOptions = new ChromeOptions();
                break;
        }
        oDriver = new RemoteWebDriver(new java.net.URL(sRemoteServer), browserOptions);
        oDriver.manage().timeouts().pageLoadTimeout(iTimeOut, TimeUnit.SECONDS);
        originalWindow = oDriver.getWindowHandle();
    }

    /**
     * Creates a cloud instance of a Selenium WebDriver
     *
     * @param sRemoteServer String - The path to the remote machine, http://hub.browserstack.com/wd/hub/ for example
     * @param sCapabilities String - Cloud tool capabilities in key=value pairs separated by ; Example - os=OS
     *                      X;os_version=Yosemite;browser=Safari;browser_version=8.0;browserstack.debug=true;resolution=1024x768;project=test
     * @throws Exception
     */
    public void createCloudDriver(String sRemoteServer, String sCapabilities) throws Exception {
        DesiredCapabilities oCapability = new DesiredCapabilities();

        System.getProperties().put("http.proxyHost", "web-proxymain.us.bank-dns.com");
        System.getProperties().put("http.proxyPort", "3128");

        String[] aCapabilities = sCapabilities.split(";");
        String[] aCapability;
        for (int i = 0; i < aCapabilities.length; i++) {
            if (aCapabilities[i].indexOf("=") > 0) {
                aCapability = aCapabilities[i].split("=");
                oCapability.setCapability(aCapability[0], aCapability[1]);
            } else {
                throw new Exception("Invalid or empty capability passed.  Ensure all are using syntax <Capability>=<Value>.");
            }
        }
        try {
            oDriver = new RemoteWebDriver(new java.net.URL(sRemoteServer), oCapability);
        } catch (Exception oException) {
            if (oException.getMessage().toLowerCase().contains("proxy")) {
                throw new Exception("Proxy error connecting to remote browser.  Please retry.");
            } else {
                throw new Exception(oException.getMessage());
            }
        }
        originalWindow = oDriver.getWindowHandle();
    }

    /**
     * Sets the default timeout period for the instance of the Selenium WebDriver.
     *
     * @param iTime Integer - Time in seconds for the time out period
     */
    public void setDriverTimeout(int iTime) {
        iTimeOut = iTime;
    }

    /**
     * Sets the folder location where the various browser driver executables are stored.
     *
     * @param sDriverLocation String - Folder path, send \ as \\
     */
    public void setDriverLocation(String sDriverLocation) {
        g_sDriverLocation = sDriverLocation;
    }

    /**
     * Stores the current window's location and size
     */
    public void storeWindow() {
        lastPosition = oDriver.manage().window().getPosition();
        lastSize = oDriver.manage().window().getSize();
    }

    /**
     * Restores the current window's location to the last saved location else maxmimizes
     */
    public void restoreWindow() {
        if (lastPosition == null) {
            try {
                DriverMaximize();
            } catch (Exception e) {
            }
            storeWindow();
        } else {
            oDriver.manage().window().setPosition(lastPosition);
            oDriver.manage().window().setSize(lastSize);
        }

    }


    /**
     * Switches the current focus back to the main document of the page
     */
    public void focusMainFrame() {
        oDriver.switchTo().defaultContent();
    }

    /**
     * Switches the focus from the current document or Frame to a child one
     *
     * @param localLocator By - Locator description of the child Frame from the current focus
     * @throws Exception
     */
    public void focusChildFrame(By localLocator) throws Exception {
        WebDriverWait oWait = new WebDriverWait(oDriver, iTimeOut);
        oWait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(localLocator));
    }

    /**
     * This method searches for text within a web page and returns a boolean indicating whether or not it was found
     *
     * @param sSearchText String - The text to search for within the web page
     * @param iTimeout    Integer - Time in seconds to search for the text before declaring it not present
     * @return Boolean - True if the text was found, False if it was not
     */
    public Boolean doesPageTextExist(String sSearchText, int iTimeout) {
        Calendar EndTime = Calendar.getInstance();
        EndTime.add(Calendar.SECOND, iTimeout);
        sSearchText = sSearchText.replaceAll("\\s+", " ").trim(); // Normalize
        // white
        // space and
        // trim

        do {
            WebElement oPage = oDriver.findElement(By.tagName("body"));
            String sCurrentText = oPage.getText().replaceAll("\\\r\\\n", " ");
            sCurrentText = sCurrentText.replaceAll("\\\r", " ");
            sCurrentText = sCurrentText.replaceAll("\\\n", " ");
            sCurrentText = sCurrentText.replaceAll("<br>", " ");
            sCurrentText = sCurrentText.replaceAll("\\s+", " ").trim(); // Normalize
            // white
            // space
            // and
            // trim
            if (sCurrentText.contains(sSearchText)) {
                return true;
            }
        } while (Calendar.getInstance().before(EndTime));
        return false;
    }

    /**
     * This method returns the text contained within the currently displayed web page
     *
     * @return String - Text from page
     */
    public String getPageText() {
        WebElement oPage = oDriver.findElement(By.tagName("body"));
        return oPage.getText();
    }


    /**
     * Searches for and closes a particular browser window
     *
     * @param searchValue String - The title, window handle, or url search string or regexp
     * @param bRegExp     Boolean - True indicates searchValue is a regular expression, false a string
     * @param searchType  String - Valid options are TITLE, WHND, or URL
     * @throws Exception
     */
    public void closeBrowser(String searchValue, Boolean bRegExp, String searchType) throws Exception {
        if (switchBrowser(searchValue, bRegExp, searchType)) {
            oDriver.close();
        }
        focusBrowser();
    }

    /**
     * Navigates the current active window to an address
     *
     * @param sURL URL to navigate to
     * @throws Exception
     */
    public void navigateBrowser(String sURL) throws Exception {
        DriverNavigate(sURL);
    }

    /**
     * Performs a "Back" operation in the currently active Browser window
     */
    public void backBrowser() {
        oDriver.navigate().back();
    }

    /**
     * Performs a "Forward" operation in the currently active Browser window
     */
    public void forwardBrowser() {
        oDriver.navigate().forward();
    }

    /**
     * Performs a "Refresh" operation in the currently active Browser window
     */
    public void refreshBrowser() {
        oDriver.navigate().refresh();
    }

    /**
     * Searches for and switches to a particular browser window
     *
     * @param searchValue String - The title, window handle, or url search string or regexp
     * @param bRegExp     Boolean - True indicates searchValue is a regular expression, false a string
     * @param searchType  String - Valid options are TITLE, WHND, or URL
     * @return Boolean - True if the browser window was found, False if it was not.
     * @throws Exception
     */
    public Boolean switchBrowser(String searchValue, Boolean bRegExp, String searchType) throws Exception {
        String actualValue = "";
        Boolean bEmpty = false;
        Calendar End = Calendar.getInstance();
        End.add(Calendar.SECOND, iTimeOut);

        Object[] handles = oDriver.getWindowHandles().toArray();
        for (int iHandle = 0; iHandle < handles.length; iHandle++) {
            oDriver.switchTo().window((String) handles[iHandle]);

            switch (searchType.toUpperCase().trim()) {
                case "TITLE": {
                    actualValue = oDriver.getTitle();
                    if (actualValue.equals("")) {
                        bEmpty = true;
                    }
                    break;
                }
                case "URL": {
                    searchValue = searchValue.replaceAll("/$", "");
                    actualValue = oDriver.getCurrentUrl().replaceAll("/$", "");
                    break;
                }
                case "WHND": {
                    actualValue = oDriver.getWindowHandle();
                    break;
                }
                case "INDEX": {
                    try {
                        int iIndex = Integer.parseInt(searchValue);
                        while (handles.length < iIndex + 1 && Calendar.getInstance().before(End)) {
                            Thread.sleep(1000);
                            handles = oDriver.getWindowHandles().toArray();
                        }
                        oDriver.switchTo().window((String) handles[iIndex]);
                    } catch (Exception oException) {
                        return false;
                    }
                    return true;
                }
                default:
                    throw new Exception("Unknown search type " + searchType + "!  Valid options are Title, URL, WHND, or INDEX.");
            }

            if (bRegExp) {
                if (actualValue.matches(searchValue)) {
                    return true;
                }
            } else {
                if (actualValue.equals(searchValue)) {
                    return true;
                }
            }
        }

        // If the title is empty and the window was not found, try URL instead
        if (bEmpty) {
            return switchBrowser(searchValue, bRegExp, "URL");
        }

        return false;
    }

    /**
     * Resets the focus back to the original browser window or tab
     *
     * @throws Exception
     */
    public void focusBrowser() {
        try {
            oDriver.switchTo().window(originalWindow);
        } catch (Exception e) // If the original window was closed, we can't
        // switch to it. Need to switch to an open
        // window and make it the new "original"
        {
            int iIndex = -1;
            try {
                do {
                    iIndex++;
                } while (!switchBrowser(String.valueOf(iIndex), false, "INDEX"));
            } catch (Exception e1) {
            }
            originalWindow = oDriver.getWindowHandle();
        }
    }

    /**
     * Closes all windows except for the original browser window
     *
     * @throws Exception
     */
    public void closePopup() throws Exception {
        Object[] handles = oDriver.getWindowHandles().toArray();
        for (int iHandle = 0; iHandle <= handles.length; iHandle++) {
            if (handles[iHandle].toString().equals(originalWindow) == false) {
                closeBrowser((String) handles[iHandle], false, "WHND");
            }
        }
    }

    /**
     * Returns a boolean based on whether or not an object is present after the given time out period.
     *
     * @param dTime        Integer - Time out period in seconds
     * @param localLocator By - Locator to desired object
     * @return Boolean - True if element was found, False if not
     * @throws Exception
     */
    public Boolean elementExistWait(int dTime, By localLocator) throws Exception {

        localLocator = xPathIndexCheck(localLocator);

        WebDriverWait oWait = new WebDriverWait(oDriver, dTime);
        try {
            globalElement = oWait.until(ExpectedConditions.presenceOfElementLocated(localLocator));
            return true;
        } catch (Exception e) {
        }

        globalElement = null;
        return false;
    }

    /**
     * Waits for an element to be clickable
     *
     * @param iTime        int - Time to wait
     * @param localLocator By - Locator for the object
     * @return Boolean - if the object was clickable within the timeout
     * @throws Exception
     * @author pbarndt - 3/27/2018
     */
    public Boolean elementClickableWait(int iTime, By localLocator) throws Exception {
        localLocator = xPathIndexCheck(localLocator);

        WebDriverWait oWait = new WebDriverWait(oDriver, iTime);
        try {
            globalElement = oWait.until(ExpectedConditions.elementToBeClickable(localLocator));
            return true;
        } catch (Exception e) {
        }

        globalElement = null;
        return false;
    }

    /**
     * Checks for an xpath description with index
     *
     * @param localLocator By - locator
     * @return By - xpath locator with index applied
     * @throws Exception
     * @author pbarndt - 3/27/2018
     */
    private By xPathIndexCheck(By localLocator) throws Exception {
        // Check for index in XPath
        String locatorValue = localLocator.toString();
        if (locatorValue.contains("By.xpath: ")) {
            if (locatorValue.contains(c_sIndexIndicator)) {
                return By.xpath(getXPathByIndex(locatorValue.replace("By.xpath: ", "")));
            }
        }
        return localLocator;
    }

    /**
     * Clicks an element in a web page.
     *
     * @param localLocator By - Locator expression for the desired object
     * @throws Exception
     */
    public void elementClick(By localLocator) throws Exception {
        if (elementClickableWait(iTimeOut, localLocator)) {
            Thread.sleep(500);
            try {
                globalElement.click();
            } catch (Exception e) {
                throw new Exception(e.getMessage().split("\\n")[0]);
            }
        } else {
            throw new Exception("The element was not found or clickable within the global timeout period " + iTimeOut + ".");
        }
    }

    /**
     * Fires a native event on an object within the browser
     *
     * @param localLocator By - Locator expression of the element in which to fire the event
     * @param sEvent       String - Native event name
     * @throws Exception
     */
    public void fireEvent(By localLocator, String sEvent) throws Exception {
        WebElement oElement = GetElement(localLocator);
        JavascriptExecutor JavaScript = (JavascriptExecutor) oDriver;
        if (sEvent.toLowerCase().startsWith("on")) {
            sEvent = sEvent.substring(2);
        }
        JavaScript.executeScript(
                "var fireOnThis = arguments[0];" + "fireOnThis.scrollIntoView(false);" + "var evt = '" + sEvent + "';  " + "if( document.createEvent ) {"
                        + "    var evObj = document.createEvent('MouseEvents');" + "    evObj.initEvent(evt, true, false);"
                        + "   fireOnThis.dispatchEvent(evObj);" + "} else if (document.createEventObject) {" + "   fireOnThis.fireEvent('on' + evt);" + "}",
                oElement);
        // Wait half a second before continuing to give the application time to
        // react to the event
        Thread.sleep(500);
    }

    /**
     * Sets text to a text box contained in a web page.
     *
     * @param localLocator By - Locator expression of the desired text box
     * @param sValue       String - Text value to set
     * @throws Exception
     */
    public void setTextboxClearValue(By localLocator, String sValue) throws Exception {
        if (elementClickableWait(iTimeOut, localLocator)) {
            globalElement.clear();
            globalElement.sendKeys(sValue);
        }
    }

    /**
     * Sets text to a text box contained in a web page.
     *
     * @param localLocator By - Locator expression of the desired text box
     * @param sValue       String - Text value to set
     * @throws Exception
     */
    public void setTextboxValue(By localLocator, String sValue) throws Exception {
        if (elementExistWait(iTimeOut, localLocator)) {
            globalElement.sendKeys(sValue);
        }
    }

    /**
     * Sets text to a date field contained in a web page.
     *
     * @param localLocator By - Locator expression of the desired text box
     * @param sValue       String - Text value to set
     * @throws Exception
     */
    public void setDateValue(By localLocator, String sValue) throws Exception {
        if (elementClickableWait(iTimeOut, localLocator)) {
            switch (oDriver.getClass().getSimpleName()) {
                case "FirefoxDriver":
                    globalElement.click();
                    KeyboardAction(sValue);
                    return;
                case "ChromeDriver":
                    globalElement.sendKeys(sValue);
                    return;
                default:
                    globalElement.clear();
                    globalElement.sendKeys(sValue);
                    return;
            }
        }
    }

    /**
     * Selects an option from a list based on it's displayed text
     *
     * @param localLocator String - Locator expression describing the desired list
     * @param sValue       String - Value to set the list to
     * @throws Exception
     */
    public void selectDropdownOption(By localLocator, String sValue) throws Exception {
        WebElement oElement = GetElement(localLocator);
        if (oElement.isEnabled()) {
            Select oSelection = new Select(oElement);
            if (oSelection.isMultiple()) {
                oSelection.deselectAll();
            }
            oSelection.selectByVisibleText(sValue);
        } else {
            throw new Exception(c_sObjectNotEnabled);
        }
    }

    /**
     * Adds a selection in a list that already has one or more items selected
     *
     * @param localLocator String - Locator expression describing the desired list
     * @param sOption      String - Option from the list to add to the selected items
     * @throws Exception
     */
    public void addDropdownOption(By localLocator, String sOption) throws Exception {
        WebElement oElement = GetElement(localLocator);
        if (oElement.isEnabled()) {
            Select oSelection = new Select(oElement);
            oSelection.selectByVisibleText(sOption);
        } else {
            throw new Exception(c_sObjectNotEnabled);
        }
    }

    /**
     * Returns all of the options available in a list separated by a semi-colon ;
     *
     * @param localLocator By - Locator expression describing the desired list
     * @return String - Semi-colon delimited list of options
     * @throws Exception
     */
    public String returnDropdownOptions(By localLocator) throws Exception {
        String sAllItems = "";
        WebElement oElement = GetElement(localLocator);
        Select oSelection = new Select(oElement);
        List<WebElement> oListOptions = oSelection.getOptions();
        if (oListOptions.size() > 0) {
            for (int iOption = 0; iOption < oListOptions.size(); iOption++) {
                sAllItems = sAllItems + oListOptions.get(iOption).getText() + ";";
            }
            sAllItems = sAllItems.substring(0, sAllItems.length() - 1);
        }
        return sAllItems;
    }

    /**
     * Determines if a list supports multiselect and returns a boolean
     *
     * @param localLocator By - Locator expression describing the desired list
     * @return Boolean - If the list supports multiselect, returns True
     * @throws Exception
     */
    public Boolean returnDropdownMultiSelect(By localLocator) throws Exception {
        WebElement oElement = GetElement(localLocator);
        Select oSelection = new Select(oElement);
        if (oSelection.isMultiple()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns the first selected item in a list
     *
     * @param localLocator By - Locator expression describing the desired list
     * @return String - First list item that is selected
     * @throws Exception
     */
    public String returnDropdownSelectedOption(By localLocator) throws Exception {
        WebElement oElement = GetElement(localLocator);
        Select oList = new Select(oElement);
        oElement = oList.getFirstSelectedOption();
        return oElement.getText();
    }

    /**
     * Selects an option from a radio group
     *
     * @param sXPath  String - XPath expression describing the desired radio
     * @param sOption String - Option to select, can also be an index with # preceding the index sent as a String
     * @throws Exception
     */
    public void selectRadioOption(String sXPath, String sOption) throws Exception {
        List<WebElement> oElements = GetElementCollection(sXPath);
        int iOption = 0;
        WebElement oElement;

        Thread.sleep(500);
        if (sOption.startsWith("#")) {
            sOption = sOption.replaceFirst("#", "");
            iOption = Integer.parseInt(sOption);
        } else {
            int iFirstOption = 0;
            for (int iElement = 0; iElement < oElements.size(); iElement++) {
                if (oElements.get(iElement).getAttribute("value").toUpperCase().trim().equals(sOption.toUpperCase().trim())) {
                    if (oElements.get(iElement).isDisplayed()) {
                        break;
                    } else {
                        iFirstOption = iOption;
                    }
                }
                iOption++;
            }
            if (iFirstOption > 0) {
                iOption = iFirstOption;
            }
            if (iOption > oElements.size() - 1) {
                throw new Exception("Option " + sOption + " was not found in radio group!");
            }
        }

        oElement = oElements.get(iOption);

        if (!oElement.isSelected()) {

            // In IE, there were problems with the element not being clickable.
            // This is here to get to a clickable element and then bring it
            // focus
            if (!oElement.isDisplayed() && oDriver.getClass().getPackage().getName() == "OpenQA.Selenium.IE") {
                int iTry = 0;
                sXPath = "(" + sXPath + ")[" + (iOption + 1) + "]";
                while (!oElement.isDisplayed() && iTry < 5) {
                    sXPath = sXPath + "/..";
                    oElement = GetElement(By.xpath(sXPath));
                    iTry++;
                }
                oElement.sendKeys(Keys.SPACE);
            }
            oElement.click();
        }

    }

    /**
     * Returns the currently selected item in a radio group
     *
     * @param sXPath String - Locator expression describing the desired radio
     * @return String - The currently selected item
     * @throws Exception
     */
    public String returnRadioSelectedOption(String sXPath) throws Exception {
        List<WebElement> oElements = GetElementCollection(sXPath);
        String sAttribute;
        for (int iElement = 0; iElement < oElements.size(); iElement++) {
            try {
                sAttribute = oElements.get(iElement).getAttribute("checked");
                if (sAttribute == null) {
                    sAttribute = "";
                }
            } catch (Exception e) {
                sAttribute = "";
            }
            if (sAttribute.equals("true") || sAttribute.equals("checked")) {
                return oElements.get(iElement).getAttribute("value");
            }
        }
        return "No option selected.";
    }

    /**
     * Returns all of the available options in a radio group delimited by a semicolon
     *
     * @param sXPath String - Locator expression describing the desired radio
     * @return String - Semicolon delimited string containing all of the available options
     * @throws Exception
     */
    public String returnRadioOptions(String sXPath) throws Exception {
        List<WebElement> oElements = GetElementCollection(sXPath);
        String sOptions = "";
        for (int iElement = 0; iElement < oElements.size(); iElement++) {
            sOptions = sOptions + oElements.get(iElement).getAttribute("value") + ';';
        }
        return sOptions.substring(0, sOptions.length() - 1);
    }

    /**
     * Returns the text contained within an element
     *
     * @param localLocator By - Locator expression describing the desired element
     * @return String - The text contained within the element
     * @throws Exception
     */
    public String returnElementText(By localLocator) throws Exception {
        WebElement oElement = GetElement(localLocator);
        return oElement.getText();
    }

    /**
     * Captures a screenshot and saves it to a file
     *
     * @param sSavePath String - Full path and name of file in which to save the screenshot
     * @throws IOException
     */
    public void captureDriverImage(String sSavePath) throws IOException {
        File oScreenShot = ((TakesScreenshot) oDriver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(oScreenShot, new File(sSavePath));
    }

    /**
     * Captures a screenshot for ReportPortal
     *
     * @return byte []- Screeenshot as bytes
     */

    public byte[] getScreenshot() throws IOException {
        return (((TakesScreenshot) oDriver).getScreenshotAs(OutputType.BYTES));
    }

    /**
     * Switches to and clears a modal alert
     *
     * @param sAction String - Valid options are "ACCEPT" or "DISMISS"
     */
    public void clearDriverAlert(String sAction) {
        switch (sAction.toUpperCase()) {
            case "ACCEPT":
                oDriver.switchTo().alert().accept();
                break;
            case "DISMISS":
                oDriver.switchTo().alert().dismiss();
                break;
            default:
                oDriver.switchTo().alert().dismiss();
                break;
        }

        // The window maybe closed. In that case, switch back to base
        try {
            oDriver.switchTo().defaultContent();
        } catch (Exception e) {
            focusBrowser();
        }

    }

    /**
     * This method returns the text of the currently displayed alert
     *
     * @param sAction String - Valid options are "ACCEPT" or "DISMISS"
     * @return String - Text of the displayed alert
     */
    public String AlertTextReturn(String sAction) {
        Alert oAlert = oDriver.switchTo().alert();
        String sAlertText = oAlert.getText();
        clearDriverAlert(sAction);
        return sAlertText;
    }

    /**
     * Performs a mouse action on a passed element
     *
     * @param localLocator By - Locator expression describing the desired element
     * @param sAction      String - Valid actions are: CLICK, MOUSEOVER, HOVER, DOUBLECLICK, and RIGHTCLICK
     * @throws Exception
     */
    public void MouseAction(By localLocator, String sAction) throws Exception {
        WebElement oElement = GetElement(localLocator);
        Actions oMouse = new Actions(oDriver);

        Calendar dteEndTime = Calendar.getInstance();
        dteEndTime.add(Calendar.SECOND, iTimeOut);

        // Check every second for the object to be displayed
        while (!oElement.isDisplayed() && Calendar.getInstance().before(dteEndTime)) {
            Thread.sleep(1000);
        }

        switch (sAction.toUpperCase().trim()) {
            case "CLICK": {
                oMouse.click(oElement).perform();
                break;
            }
            case "MOUSEOVER":
            case "HOVER": {
                oMouse.moveToElement(oElement).perform();
                break;
            }
            case "DOUBLECLICK": {
                oMouse.doubleClick(oElement).perform();
                break;
            }
            case "RIGHTCLICK": {
                oMouse.contextClick(oElement).perform();
                break;
            }
            default:
                throw new Exception("Unknown mouse operation " + sAction + ".");
        }
    }

    /**
     * Performs a Drag and Drop mouse action on a passed element
     *
     * @param localLocator By - XPath expression describing the desired element
     * @param iXOffset     String - 'X' distance to drag object
     * @param iYoffset     String - 'Y' distance to drag object
     * @throws Exception
     */
    public void DragAndDrop(By localLocator, int iXOffset, int iYoffset) throws Exception {
        DragAndDrop(localLocator, iXOffset, iYoffset, false);
    }

    /**
     * Performs a Drag and Drop mouse action on a passed element
     *
     * @param localLocator By - XPath expression describing the desired element
     * @param iXOffset     String - 'X' distance to drag object
     * @param iYoffset     String - 'Y' distance to drag object
     * @param useCenter    Boolean - Whether or not to force mouse to center of object being dragged
     * @throws Exception
     */
    public void DragAndDrop(By localLocator, int iXOffset, int iYoffset, Boolean useCenter) throws Exception {
        WebElement oElement = GetElement(localLocator);
        Actions oMouse = new Actions(oDriver);

        Calendar dteEndTime = Calendar.getInstance();
        dteEndTime.add(Calendar.SECOND, iTimeOut);

        // Check every second for the object to be displayed
        while (!oElement.isDisplayed() && Calendar.getInstance().before(dteEndTime)) {
            Thread.sleep(1000);
        }

        Thread.sleep(1000);

        if (useCenter) {
            oMouse.clickAndHold(oElement).moveByOffset(iXOffset, iYoffset).release().perform();
        } else {
            // Drags and drops from 0,0, doesn't always work
            oMouse.dragAndDropBy(oElement, iXOffset, iYoffset);
        }
    }

    /**
     * Performs a Drag and Drop mouse action from one passed in element to another
     *
     * @param sourceLocator String - XPath expression describing the desired source element
     * @param targetLocator String - XPath expression describing the desired target element
     * @throws Exception
     */
    public void DragAndDrop(By sourceLocator, By targetLocator) throws Exception {
        DragAndDrop(sourceLocator, targetLocator, false);
    }

    /**
     * Performs a Drag and Drop mouse action from one passed in element to another
     *
     * @param sourceLocator String - XPath expression describing the desired source element
     * @param targetLocator String - XPath expression describing the desired target element
     * @param useCenter     Boolean - Whether or not to force mouse to center of object being dragged and to
     * @throws Exception
     */
    public void DragAndDrop(By sourceLocator, By targetLocator, Boolean useCenter) throws Exception {
        WebElement oSource = GetElement(sourceLocator);
        WebElement oTarget = GetElement(targetLocator);
        Actions oMouse = new Actions(oDriver);

        Calendar dteEndTime = Calendar.getInstance();
        dteEndTime.add(Calendar.SECOND, iTimeOut);

        // Check every second for the object to be displayed
        while (!(oSource.isDisplayed() && oTarget.isDisplayed()) && Calendar.getInstance().before(dteEndTime)) {
            Thread.sleep(1000);
        }

        if (useCenter) {
            oMouse.clickAndHold(oSource).moveToElement(oTarget).release().perform();
        } else {
            // Drags and drops from 0,0, doesn't always work
            oMouse.dragAndDrop(oSource, oTarget);
        }
        Thread.sleep(1000);
    }

    /**
     * Sends a keyboard key to the application
     *
     * @param sKeys String - The key to send to the application. Valid options are: CTRL, CTRLDN, CTRLUP, ALT, ALTDN, ALTUP, SHIFTDN, SHIFTUP, ESC, ENTER,
     *              F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12, PAUSE, TAB, UP, DOWN, LEFT, RIGHT, PGDN, PGUP, DELETE, and BACKSPACE Adding + at
     *              the start holds down the SHIFT key, adding ^ at the start holds down the CONTROL key, and adding % at the start holds down the ALT key
     */
    public void KeyboardAction(String sKeys) throws Exception {
        Actions oKeyboard = new Actions(oDriver);
        Robot robot;
        switch (sKeys) {
            case "CTRL":
                oKeyboard.sendKeys(Keys.CONTROL).perform();
                break;
            case "CTRLDN":
                // Changed to Robot class due to actions not working with compound click - Kathy 3/25/2019
                // oKeyboard.keyDown(Keys.CONTROL).perform();
                robot = new Robot();
                robot.keyPress(KeyEvent.VK_CONTROL);
                break;
            case "CTRLUP":
                // Changed to Robot class due to actions not working with compound click - Kathy 3/25/2019
                // oKeyboard.keyUp(Keys.CONTROL).perform();
                robot = new Robot();
                robot.keyRelease(KeyEvent.VK_CONTROL);
                break;
            case "ALT":
                oKeyboard.sendKeys(Keys.ALT).perform();
                break;
            case "ALTDN":
                // Changed to Robot class due to actions not working with compound click - Kathy 3/25/2019
                // oKeyboard.keyDown(Keys.ALT).perform();
                robot = new Robot();
                robot.keyPress(KeyEvent.VK_ALT);
                break;
            case "ALTUP":
                // Changed to Robot class due to actions not working with compound click - Kathy 3/25/2019
                // oKeyboard.keyUp(Keys.ALT).perform();
                robot = new Robot();
                robot.keyRelease(KeyEvent.VK_ALT);
                break;
            case "SHIFTDN":
                // Changed to Robot class due to actions not working with compound click - Kathy 3/25/2019
                // oKeyboard.keyDown(Keys.SHIFT).perform();
                robot = new Robot();
                robot.keyPress(KeyEvent.VK_SHIFT);
                break;
            case "SHIFTUP":
                // Changed to Robot class due to actions not working with compound click - Kathy 3/25/2019
                // oKeyboard.keyUp(Keys.SHIFT).perform();
                robot = new Robot();
                robot.keyRelease(KeyEvent.VK_SHIFT);
                break;
            case "ESC":
                oKeyboard.sendKeys(Keys.ESCAPE).perform();
                break;
            case "ENTER":
                oKeyboard.sendKeys(Keys.ENTER).perform();
                break;
            case "F1":
                oKeyboard.sendKeys(Keys.F1).perform();
                break;
            case "F2":
                oKeyboard.sendKeys(Keys.F2).perform();
                break;
            case "F3":
                oKeyboard.sendKeys(Keys.F3).perform();
                break;
            case "F4":
                oKeyboard.sendKeys(Keys.F4).perform();
                break;
            case "F5":
                oKeyboard.sendKeys(Keys.F5).perform();
                break;
            case "F6":
                oKeyboard.sendKeys(Keys.F6).perform();
                break;
            case "F7":
                oKeyboard.sendKeys(Keys.F7).perform();
                break;
            case "F8":
                oKeyboard.sendKeys(Keys.F8).perform();
                break;
            case "F9":
                oKeyboard.sendKeys(Keys.F9).perform();
                break;
            case "F10":
                oKeyboard.sendKeys(Keys.F10).perform();
                break;
            case "F11":
                oKeyboard.sendKeys(Keys.F11).perform();
                break;
            case "F12":
                oKeyboard.sendKeys(Keys.F12).perform();
                break;
            case "PAUSE":
                oKeyboard.sendKeys(Keys.PAUSE).perform();
                break;
            case "TAB":
                oKeyboard.sendKeys(Keys.TAB).perform();
                break;
            case "UP":
                oKeyboard.sendKeys(Keys.ARROW_UP).perform();
                break;
            case "DOWN":
                oKeyboard.sendKeys(Keys.ARROW_DOWN).perform();
                break;
            case "LEFT":
                oKeyboard.sendKeys(Keys.ARROW_LEFT).perform();
                break;
            case "RIGHT":
                oKeyboard.sendKeys(Keys.ARROW_RIGHT).perform();
                break;
            case "PGDN":
                oKeyboard.sendKeys(Keys.PAGE_DOWN).perform();
                break;
            case "PGUP":
                oKeyboard.sendKeys(Keys.PAGE_UP).perform();
                break;
            case "DELETE":
                oKeyboard.sendKeys(Keys.DELETE).perform();
                break;
            case "BACKSPACE":
                oKeyboard.sendKeys(Keys.BACK_SPACE).perform();
                break;
            case "HOME":
                oKeyboard.sendKeys(Keys.HOME).perform();
                break;
            case "END":
                oKeyboard.sendKeys(Keys.END).perform();
                break;
            default:
                if (sKeys.startsWith("+")) // Shift
                {
                    sKeys = sKeys.substring(1).toLowerCase();
                    if (sKeys.startsWith("^")) // Control
                    {
                        sKeys = sKeys.substring(1);
                        if (sKeys.startsWith("%")) // Alt
                        {
                            sKeys = sKeys.substring(1);
                            oKeyboard.keyDown(Keys.SHIFT).keyDown(Keys.CONTROL).keyDown(Keys.ALT).sendKeys(sKeys).keyUp(Keys.SHIFT).keyUp(Keys.CONTROL)
                                    .keyUp(Keys.ALT).perform();
                        } else {
                            oKeyboard.keyDown(Keys.SHIFT).keyDown(Keys.CONTROL).sendKeys(sKeys).keyUp(Keys.SHIFT).keyUp(Keys.CONTROL).perform();
                        }
                    } else {
                        oKeyboard.keyDown(Keys.SHIFT).sendKeys(sKeys).keyUp(Keys.SHIFT).perform();
                    }
                } else if (sKeys.startsWith("^")) // Control
                {
                    sKeys = sKeys.substring(1).toLowerCase();
                    if (sKeys.startsWith("%")) // Alt
                    {
                        sKeys = sKeys.substring(1);
                        oKeyboard.keyDown(Keys.CONTROL).keyDown(Keys.ALT).sendKeys(sKeys).keyUp(Keys.CONTROL).keyUp(Keys.ALT).perform();
                    } else {
                        oKeyboard.keyDown(Keys.CONTROL).sendKeys(sKeys).keyUp(Keys.CONTROL).perform();
                    }
                } else if (sKeys.startsWith("%")) // Alt
                {
                    sKeys = sKeys.substring(1).toLowerCase();
                    oKeyboard.keyDown(Keys.ALT).sendKeys(sKeys).keyUp(Keys.ALT).perform();
                } else {
                    oKeyboard.sendKeys(sKeys).perform();
                }
                break;
        }
    }

    /**
     * Returns the value of the maxlength attribute for an element
     *
     * @param localLocator By - Locator expression describing the desired element
     * @return String - maxlength attribute value
     * @throws Exception
     */
    public String ElementMaxLengthReturn(By localLocator) throws Exception {
        WebElement oElement = GetElement(localLocator);
        return oElement.getAttribute("maxlength");
    }

    /**
     * Returns the value of the "value" attribute of the element
     *
     * @param localLocator By - Locator expression describing the desired element
     * @return String - value attribute value
     * @throws Exception
     */
    public String ElementValueReturn(By localLocator) throws Exception {
        WebElement oElement = GetElement(localLocator);
        return oElement.getAttribute("value");
    }

    /**
     * Returns the value of the desired attribute of the element
     *
     * @param localLocator By - Locator expression describing the desired element
     * @param sAttribute   String - The name of the attribute to retrieve the value from
     * @return String - attribute value
     * @throws Exception
     */
    public String ElementAttributeReturn(By localLocator, String sAttribute) throws Exception {
        WebElement oElement = GetElement(localLocator);
        try {
            return oElement.getAttribute(sAttribute);
        } catch (Exception e) {
            throw new Exception("Invalid attribute provided for object!");
        }
    }

    /**
     * Returns the name of the tag for the desired element
     *
     * @param localLocator By - Locator expression describing the desired element
     * @return String - Tag name
     * @throws Exception
     */
    public String ElementTagNameReturn(By localLocator) throws Exception {
        WebElement oElement = GetElement(localLocator);
        return oElement.getTagName();
    }

    /**
     * This method retrieves font styles for objects
     *
     * @param localLocator By - Locator expression describing the desired element
     * @param sStyle       String - Which style's property to retrieve
     * @return String - The requested style property's value
     * @throws Exception
     */
    public String ElementStyleReturn(By localLocator, String sStyle) throws Exception {
        WebElement oElement = GetElement(localLocator);
        try {
            return oElement.getCssValue(sStyle);
        } catch (Exception e) {
            throw new Exception("Invalid style attribute provided for object!");
        }
    }

    /**
     * Returns the X location of the desired element
     *
     * @param localLocator By - Locator expression describing the desired element
     * @return Integer - X location of the element
     * @throws Exception
     */
    public int ElementXLocationReturn(By localLocator) throws Exception {
        WebElement oElement = GetElement(localLocator);
        Point pntObject = oElement.getLocation();
        return pntObject.x;
    }

    /**
     * Returns the absolute X location of the desired element
     *
     * @param localLocator By - Locator expression describing the desired element
     * @return Integer - Absolute X location of the element
     * @throws Exception
     */
    public int ElementAbsXLocationReturn(By localLocator) throws Exception {
        WebElement oElement = GetElement(localLocator);
        Point pntBrowser = oDriver.manage().window().getPosition();
        Point pntObject = oElement.getLocation();
        return Math.abs(pntBrowser.x) + Math.abs(pntObject.x);
    }

    /**
     * Returns the Y location of the desired element
     *
     * @param localLocator By - Locator expression describing the desired element
     * @return Integer - Y location of the element
     * @throws Exception
     */
    public int ElementYLocationReturn(By localLocator) throws Exception {
        WebElement oElement = GetElement(localLocator);
        Point pntObject = oElement.getLocation();
        return pntObject.y;
    }

    /**
     * Returns the absolute Y location of the desired element
     *
     * @param localLocator By - Locator expression describing the desired element
     * @return Integer - Absolute Y location of the element
     * @throws Exception
     */
    public int ElementAbsYLocationReturn(By localLocator) throws Exception {
        WebElement oElement = GetElement(localLocator);
        Point pntBrowser = oDriver.manage().window().getPosition();
        Point pntObject = oElement.getLocation();
        return Math.abs(pntBrowser.y) + Math.abs(pntObject.y);
    }

    /**
     * Returns the height of the desired element
     *
     * @param localLocator By - Locator expression describing the desired element
     * @return Integer - Height of the element
     * @throws Exception
     */
    public int ElementHeightReturn(By localLocator) throws Exception {
        WebElement oElement = GetElement(localLocator);
        Dimension szeObject = oElement.getSize();
        return szeObject.height;
    }

    /**
     * Returns the width of the desired element
     *
     * @param localLocator By - Locator expression describing the desired element
     * @return Integer - Width of the element
     * @throws Exception
     */
    public int ElementWidthReturn(By localLocator) throws Exception {
        WebElement oElement = GetElement(localLocator);
        Dimension szeObject = oElement.getSize();
        return szeObject.width;
    }

    /**
     * Returns a boolean based on whether or not the element is enabled
     *
     * @param localLocator By - Locator expression describing the desired element
     * @return Boolean - True if the element is enabled
     * @throws Exception
     */
    public Boolean ElementEnabledStatus(By localLocator) throws Exception {
        WebElement oElement = GetElement(localLocator);
        if (oElement.isEnabled()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns a boolean based on whether or not the element is displayed
     *
     * @param localLocator By - Locator expression describing the desired element
     * @return Boolean - True if the element is displayed
     * @throws Exception
     */
    public Boolean ElementDisplayedStatus(By localLocator) throws Exception {
        WebElement oElement = GetElement(localLocator);
        if (oElement.isDisplayed()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns the title of the opened browser
     *
     * @return String - Browser's title
     */
    public String BrowserTitleReturn() {
        return oDriver.getTitle();
    }

    /**
     * Returns the current URL of the page displayed in the browser
     *
     * @return String - URL of current page
     */
    public String BrowserURLReturn() {
        return oDriver.getCurrentUrl();
    }

    /**
     * Returns the window handle of the open browser
     *
     * @return String - Window handle of the browser
     */
    public String BrowserHWNDReturn() {
        return oDriver.getWindowHandle();
    }

    /**
     * Returns the checked status of a checkbox
     *
     * @param localLocator By - Locator expression describing the desired checkbox
     * @return Boolean - True if the checkbox is checked
     * @throws Exception
     */
    public Boolean CheckboxChecked(By localLocator) throws Exception {
        WebElement oElement = GetElement(localLocator);
        return oElement.isSelected();
    }

    /**
     * Sets the checked status of a checkbox
     *
     * @param localLocator By - Locator expression describing the desired checkbox
     * @param sState       String - ON or OFF
     * @throws Exception
     */
    public void CheckboxSet(By localLocator, String sState) throws Exception {
        WebElement oElement = GetElement(localLocator);
        WebElement oClickElement = oElement;
        int iTry = 0;
        Boolean bStatus = oElement.isSelected();

        // Sometimes the actual checkbox is not visible, move up one level until
        // a visible element is found or 5 tries are reached
        String sXPath = GetElementXPath(oElement);
        while (!oClickElement.isDisplayed() && iTry < 5) {
            sXPath = sXPath + "/..";
            oClickElement = GetElement(By.xpath(sXPath));
            iTry++;
        }

        // Sometimes the state of the checkbox does not change on the first try,
        // try up to 5 times
        iTry = 0;
        if (sState.toUpperCase().equals("ON") && !bStatus) {
            while (!bStatus && iTry < 5) {
                oClickElement.click();
                iTry++;
                oElement = GetElement(localLocator);
                oClickElement = GetElement(By.xpath(sXPath));
                bStatus = oElement.isSelected();
            }
        } else if (sState.toUpperCase().equals("OFF") && bStatus) {
            while (bStatus && iTry < 5) {
                oClickElement.click();
                iTry++;
                oElement = GetElement(localLocator);
                oClickElement = GetElement(By.xpath(sXPath));
                bStatus = oElement.isSelected();
            }
        }
        if (iTry >= 5) {
            throw new Exception("Unable to change the state of the checkbox after 5 tries!");
        }
    }

    /**
     * Returns the number of rows contained within a table
     *
     * @param localLocator By - Locator expression describing the table
     * @return Integer - The number of rows in the table
     * @throws Exception
     */
    public int TableRowCountReturn(By localLocator) throws Exception {
        WebElement oTable = GetElement(localLocator);
        try {
            List<WebElement> oRows = oTable.findElements(By.tagName("tr"));
            return oRows.size();
        } catch (NoSuchElementException e) {
            throw new Exception("Rows were not found in table.");
        }
    }

    /**
     * Returns the number of columns contained within a table
     *
     * @param localLocator By - Locator expression describing the table
     * @param iRowNumber   Integer - The number which row to use to determine the number of columns
     * @return Integer - The number of columns in the row of the table
     * @throws Exception
     */
    public int TableColumnCountReturn(By localLocator, int iRowNumber) throws Exception {
        WebElement oTable = GetElement(localLocator);
        try {
            List<WebElement> oRows = oTable.findElements(By.tagName("tr"));
            oTable = oRows.get(iRowNumber - 1);
            List<WebElement> oColumns = oTable.findElements(By.xpath(c_sTableCellXPath));
            return oColumns.size();
        } catch (NoSuchElementException e) {
            throw new Exception("Columns were not found in row " + String.valueOf(iRowNumber) + ".");
        }
    }

    /**
     * Returns the column headings of a table
     *
     * @param localLocator By - Locator expression describing the table
     * @return String - Semi-colon delimited list of the column headings
     * @throws Exception
     */
    public String TableColumnHeadingsReturn(By localLocator) throws Exception {
        WebElement oTable = GetElement(localLocator);
        List<WebElement> oCells;
        String sColumnNames = "";
        Boolean bHeader = false;
        try {
            bHeader = oTable.findElement(By.xpath(".//thead")).isDisplayed();
        } catch (Exception e) {
        }

        WebElement oHeaderRow;

        if (bHeader) {
            oHeaderRow = oTable.findElement(By.xpath(".//thead/tr"));
        } else {
            oHeaderRow = oTable.findElement(By.xpath("(.//tbody/tr)[1]"));
        }
        oCells = oHeaderRow.findElements(By.tagName("th"));
        if (oCells.size() == 0) // If there are no <th> children, then try <td>
        {
            oCells = oHeaderRow.findElements(By.tagName("td"));
        }
        if (oCells.size() == 0) {
            throw new Exception("No column headings were found!");
        }
        for (int iEle = 0; iEle < oCells.size(); iEle++) {
            sColumnNames = sColumnNames + oCells.get(iEle).getAttribute("innerText").trim() + ";";
        }
        return sColumnNames.substring(0, sColumnNames.length() - 1);
    }

    /**
     * Retrieves the text from the specified cell in a table
     *
     * @param localLocator  By - Locator expression describing the table
     * @param iRowNumber    Integer - Row number
     * @param iColumnNumber Integer - Column number
     * @return String - Cell contents
     * @throws Exception
     */
    public String TableCellTextReturn(By localLocator, int iRowNumber, int iColumnNumber) throws Exception {
        WebElement oTable = GetElement(localLocator);
        List<WebElement> oRows;
        List<WebElement> oCells;
        WebElement oCell;
        Boolean bHeader = false;
        try {
            bHeader = oTable.findElement(By.xpath(".//thead")).isDisplayed();
        } catch (Exception e) {
        }

        try {
            if (iRowNumber == 1 && bHeader) {
                oCells = oTable.findElement(By.xpath(".//thead/tr")).findElements(By.xpath(c_sTableCellXPath));
            } else if (iRowNumber == 1 && !bHeader) {
                oRows = oTable.findElements(By.xpath(".//tbody/tr"));
                oCells = oRows.get(iRowNumber - 1).findElements(By.xpath(c_sTableCellXPath));
            } else {
                oRows = oTable.findElements(By.xpath(".//tbody/tr"));
                if (!bHeader) {
                    iRowNumber = iRowNumber - 1;
                } else {
                    iRowNumber = iRowNumber - 2;
                }
                if (iRowNumber == oRows.size()) {
                    oRows = oTable.findElements(By.xpath(".//tfoot/tr"));
                    iRowNumber = 0;
                }
                oCells = oRows.get(iRowNumber).findElements(By.xpath(c_sTableCellXPath));
            }
            if (oCells.size() == 0) // This means this row did not contain a
            // column element
            {
                return null;
            }
            if (iColumnNumber - 1 > oCells.size()) {
                return null;
            }
            oCell = oCells.get(iColumnNumber - 1);
        } catch (NoSuchElementException e) {
            return null;
        }

        return oCell.getAttribute("innerText");
    }

    /**
     * Searches for text contained within a table and returns the row number where it was found
     *
     * @param localLocator  By - Locator expression describing the table
     * @param sCellContents String - The text to search for
     * @param sSearchColumn String - The column in which to search for the text, can also pass a number as a string
     * @param iStartRow     Integer - The row in which to start the search, pass 0 for all
     * @return Integer - The row number where the text was found
     * @throws Exception
     */
    public int TableRowContainsTextReturn(By localLocator, String sCellContents, String sSearchColumn, int iStartRow) throws Exception {
        WebElement oTable = GetElement(localLocator);
        List<WebElement> oRows;
        List<WebElement> oCells;
        int iColumn = 0;
        int iRow;
        Boolean bFound = false;
        Boolean bSearchAll = false;
        Boolean bHeader = false;
        try {
            bHeader = oTable.findElement(By.xpath(".//thead")).isDisplayed();
        } catch (Exception e) {
        }

        if (iStartRow == 0) {
            iRow = iStartRow;
        } else {
            iRow = iStartRow - 1;
        }

        // Get the header row cells
        if (bHeader) {
            oCells = oTable.findElement(By.xpath(".//thead/tr")).findElements(By.xpath(c_sTableCellXPath));
        } else {
            oCells = oTable.findElement(By.xpath("(.//tbody/tr)[1]")).findElements(By.xpath(c_sTableCellXPath));
        }

        // This entire try block is to determine which column we want
        try {
            iColumn = Integer.parseInt(sSearchColumn);
            if (iColumn == 0) {
                bSearchAll = true;
            } else {
                iColumn = iColumn - 1;
            }
        } catch (Exception e) {
            for (int iCell = 0; iCell < oCells.size(); iCell++) {
                if (sSearchColumn.equals(oCells.get(iCell).getText())) {
                    bFound = true;
                    break;
                }
                iColumn = iColumn + 1;
            }
            if (bFound == false) {
                throw new Exception("Column heading " + sSearchColumn + " was not found in table.");
            }
        }
        if (iColumn > (oCells.size() - 1)) {
            throw new Exception("The desired column number is higher than the number of columns availble in the table.");
        }

        bFound = false;
        if (iRow == 0) {
            if (bSearchAll) {
                for (int iCell = 0; iCell < oCells.size(); iCell++) {
                    if (oCells.get(iCell).getText().contains(sCellContents)) {
                        return iRow + 1;
                    }
                }
            } else {
                if (oCells.get(iColumn).getText().contains(sCellContents)) {
                    return iRow + 1;
                }
            }
        } else {
            iRow = iRow - 1;
        }

        try {
            oRows = oTable.findElements(By.xpath(".//tbody/tr"));
        } catch (NoSuchElementException e) {
            throw new Exception("Rows in body of table not found.");
        }

        if (iRow > (oRows.size() - 1)) {
            throw new Exception("The starting row provided is higher than the number of rows in the table.");
        }

        for (int i = iRow; i < oRows.size(); i++) {
            try {
                if (oRows.get(i).getText().equals("")) // If the row has no text
                // content, do not try
                // and get the children.
                // Locks the driver up.
                // - Paul Arndt 4/5/2016
                {
                    iRow++;
                    continue;
                } else {
                    oCells = oRows.get(i).findElements(By.xpath(c_sTableCellXPath));
                }
            } catch (Exception e) {
                iRow++;
                continue; // Changed to just continue instead of throw an
                // exception as some applications have empty rows
                // for display purposes - Paul Arndt 4/5/2016
                // throw new Exception("No cell contents found in row " +
                // Convert.ToString(iRow + 2) + ".");
            }
            if (bSearchAll) {
                for (int iCell = 0; iCell < oCells.size(); iCell++) {
                    if (oCells.get(iCell).getText().contains(sCellContents)) {
                        if (bHeader) {
                            return iRow + 2;
                        } else {
                            return iRow + 1;
                        }
                    }
                }
            } else {
                if (oCells.get(iColumn).getText().contains(sCellContents)) {
                    if (bHeader) {
                        return iRow + 2;
                    } else {
                        return iRow + 1;
                    }
                }
            }
            iRow++;
        }

        try {
            oCells = oTable.findElement(By.xpath(".//tfoot/tr")).findElements(By.xpath(c_sTableCellXPath));
            if (bSearchAll) {
                for (int iCell = 0; iCell < oCells.size(); iCell++) {
                    if (oCells.get(iCell).getText().contains(sCellContents)) {
                        if (bHeader) {
                            return iRow + 2;
                        } else {
                            return iRow + 1;
                        }
                    }
                }
            } else {
                if (oCells.get(iColumn).getText().contains(sCellContents)) {
                    if (bHeader) {
                        return iRow + 2;
                    } else {
                        return iRow + 1;
                    }
                }
            }
        } catch (Exception e) {
        }

        throw new Exception("Cell contents " + sCellContents + " were not found in search!");
    }

    /**
     * Closes the Selenium driver window(s)
     */
    public void DriverQuit() {
        if (oDriver == null) return;
        try {
            Set<String> handles = oDriver.getWindowHandles();
            for (Iterator<String> iHandles = handles.iterator(); iHandles.hasNext(); ) {
                oDriver.switchTo().window(iHandles.next()).close();
            }
            oDriver.quit();
            Thread.sleep(2000);
            oDriver = null;
            Runtime.getRuntime().exec("taskkill /F /FI \"IMAGENAME eq WerFault*\""); // IE Crash Error
        } catch (Exception e) {
        }
    }

    /**
     * Navigates the driver window to a URL
     *
     * @param sURL String - The desired URL in which to navigate
     * @throws Exception
     */
    public void DriverNavigate(String sURL) throws Exception {
        try {
            oDriver.navigate().to(sURL);
        } catch (Exception e) {
            throw new Exception("Selenium web driver navigation failed!");
        }
    }

    /**
     * Maximizes the driver window
     *
     * @throws Exception
     */
    public void DriverMaximize() throws Exception {
        WebDriver.Window oWindow = oDriver.manage().window();
        try {
            oWindow.maximize();
        } catch (Exception e) {
            throw new Exception(
                    "An error was encountered while attempting to maximize the browser.  Verify that a modal dialog or alert was not displayed.");
        }
        storeWindow();
    }

    /**
     * Minimizes the driver window
     *
     * @throws Exception
     */
    public void DriverMinimize() throws Exception {
        oDriver.manage().window().setPosition(new Point(0, -3000));
    }

    /**
     * Resizes the driver window
     *
     * @param width  int - Width
     * @param height int - Height
     * @throws Exception
     */
    public void DriverResize(int width, int height) throws Exception {
        WebDriver.Window window = oDriver.manage().window();
        window.setSize(new Dimension(width, height));
    }

    /**
     * Retrieves an element by XPath
     *
     * @param localLocator By - Locator expression describing the element to retrieve
     * @return WebElement
     * @throws Exception
     */
    private WebElement GetElement(By localLocator) throws Exception {
        if (elementExistWait(iTimeOut, localLocator)) {
            return globalElement;
        } else {
            throw new Exception(c_sObjectNotFound);
        }
    }

    /**
     * Retrieves an element by XPath when the XPath returns a collection
     *
     * @param sXPath String - Locator expression describing the element to retrieve
     * @return WebElement
     * @throws Exception
     */
    public String getXPathByIndex(String sXPath) throws Exception {

        // Split apart the index from the rest of the XPath
        String[] aXPath = sXPath.split(c_sIndexIndicator);
        // Index from input is zero based, xPath representation is 1 based
        Integer index = (Integer.parseInt(aXPath[0])) + 1;
        return "(" + aXPath[1] + ")[" + index + "]";
    }

    /**
     * Gets a collection of WebElements by XPath
     *
     * @param sXPath  String - XPath expression describing the element collection to retrieve
     * @param bFilter Boolean - True if you want to filter out elements that are not displayed, enabled, or have height
     * @return List<WebElement>
     * @throws Exception
     */
    private List<WebElement> GetElementCollection(String sXPath, Boolean bFilter) throws Exception {

        if (sXPath.contains(c_sIndexIndicator)) {
            String[] aXPath = sXPath.split(c_sIndexIndicator);
            sXPath = aXPath[1];
        }
        List<WebElement> oElements;
        try {
            oElements = oDriver.findElements(By.xpath(sXPath));
            if (oElements.size() == 0) {
                throw new Exception(c_sObjectNotFound);
            }
        } catch (Exception e) {
            throw new Exception(c_sObjectNotFound);
        }

        if (bFilter) {
            Predicate<WebElement> displayed = (d) -> !d.isDisplayed();
            Predicate<WebElement> enabled = (e) -> !e.isEnabled();
            Predicate<WebElement> height = (h) -> h.getSize().getHeight() < 1;
            oElements.removeIf(height.or(enabled.or(displayed)));
        }

        if (oElements.size() == 0) {
            throw new Exception(c_sObjectNotEnabled);
        }
        return oElements;
    }

    /**
     * Gets a collection of WebElements by XPath
     *
     * @param sXPath String - Locator expression describing the element collection to retrieve
     * @return List<WebElement>
     * @throws Exception
     */
    private List<WebElement> GetElementCollection(String sXPath) throws Exception {
        return GetElementCollection(sXPath, true);
    }

    /**
     * Returns the number of elements matching the locator
     *
     * @param locator By - Locator
     * @return int - Number of elements
     */
    public int ElementCount(By locator) {
        List<WebElement> list = oDriver.findElements(locator);
        return list.size();
    }


    /**
     * Returns an XPath description for an object
     *
     * @param oElement WebElement - The WebElemnt to retrieve the XPath description for
     * @return String - XPath expression
     */
    private String GetElementXPath(WebElement oElement) {
        String sXPath = (String) ((JavascriptExecutor) oDriver).executeScript("var node = arguments[0];" + "return getXPath(node);"
                + "function getXPath(node) {" + "if (node.id !== ''){ " + "return '//' + node.tagName.toLowerCase() + \"[@id='\" + node.id + \"']\";"
                + "}" + "if (node.hasAttribute(\"name\")){ "
                + "return '//' + node.tagName.toLowerCase() + \"[@name='\" + node.getAttribute(\"name\") + \"']\";" + "}" + "if (node === document.body){"
                + "return \"//\" + node.tagName.toLowerCase();" + "}" + "var nodeCount = 0;" + "var childNodes = node.parentNode.childNodes;"
                + "for (var i=0; i<childNodes.length; i++) {" + "var currentNode = childNodes[i];" + "if (currentNode === node) {"
                + "return getXPath(node.parentNode) + '/' + node.tagName.toLowerCase() + '[' + (nodeCount + 1) + ']';" + "}"
                + "if (currentNode.nodeType === 1 && currentNode.tagName.toLowerCase() === node.tagName.toLowerCase()) {" + "nodeCount++;" + "}" + "}"
                + "}", oElement);
        if (sXPath.contains("svg")) {
            String[] aSVG = sXPath.split("svg");
            sXPath = aSVG[0];
            String[] aChildren = aSVG[1].split("/");
            aChildren[0] = "svg" + aChildren[0];
            for (String sTag : aChildren) {
                String sTagName = sTag.substring(0, sTag.indexOf("["));
                sTag = sTag.substring(sTag.indexOf("["));
                sXPath += "*[name()='" + sTagName + "']" + sTag + "/";
            }
            sXPath = sXPath.substring(0, sXPath.lastIndexOf("/"));
        }
        return sXPath;
    }

    /**
     * Executes JavaScript code in the window with the indicated object
     *
     * @param sScript      String - JavaScript code to execute in the window
     * @param localLocator String - By object for the element being passed to the function
     * @param sValue       String - Value to be used in JavaScript code
     * @return Object - Value returned by ScriptExecute. Can be many types
     * @throws Exception
     */
    public Object ScriptExecute(String sScript, By localLocator, String sValue) throws Exception {
        WebElement oElement = GetElement(localLocator);
        JavascriptExecutor oJse = (JavascriptExecutor) oDriver;

        return oJse.executeScript(sScript, oElement, sValue);
    }

    /**
     * Executes java script code directly in the driver/page
     *
     * @param sScript String - JavaScript code to execute
     * @return Object - The result of the JavaScript
     * @throws Exception Exception - Any exception encountered
     */
    public Object ScriptExecute(String sScript) throws Exception {
        JavascriptExecutor oJse = (JavascriptExecutor) oDriver;
        return oJse.executeScript(sScript);
    }

    /**
     * Highlights an object in the application
     *
     * @param localLocator Locator used to identify the object
     * @throws Exception
     */
    public void Highlight(By localLocator) throws Exception {
        WebElement oElement = GetElement(localLocator);
        JavascriptExecutor javascript = (JavascriptExecutor) oDriver;
        for (int i = 0; i < 3; i++) {
            javascript.executeScript("arguments[0].setAttribute('style', arguments[1]);", oElement, "border: 5px solid orange;");
            Thread.sleep(100);
            javascript.executeScript("arguments[0].setAttribute('style', arguments[1]);", oElement, "border: 5px solid pink;");
            Thread.sleep(100);
            javascript.executeScript("arguments[0].setAttribute('style', arguments[1]);", oElement, "border: 5px solid yellow;");
            Thread.sleep(100);
            javascript.executeScript("arguments[0].setAttribute('style', arguments[1]);", oElement, "");
            Thread.sleep(100);
        }
    }

    /**
     * This method returns the text of the currently displayed alert without clicking a button.
     *
     * @return String - Text of the displayed alert
     */
    public String GetAlertText() {
        return oDriver.switchTo().alert().getText();
    }

    /**
     * This method will add the passed in cookie to the browser session. It must be added after the browser is launched.
     *
     * @param name  - The name of the cookie
     * @param value - The address of the cookie
     */
    public void AddCookie(String name, String value) {
        Cookie ck = new Cookie(name, value);
        oDriver.manage().addCookie(ck);
    }
}
