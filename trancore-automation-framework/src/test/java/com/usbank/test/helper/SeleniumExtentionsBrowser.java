package com.usbank.test.helper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Calendar;
import java.util.Set;

public class SeleniumExtentionsBrowser extends SeleniumExtentions {

    private static final Logger logger = LogManager.getLogger(SeleniumExtentionsBrowser.class);


    /**
     * Switches the current focus back to the main document of the page
     */
    public static void focusMainFrame(WebDriver driver) {
        driver.switchTo().defaultContent();
    }

    /**
     * Switches the focus from the current document or Frame to a child one
     *
     * @param localLocator By - Locator description of the child Frame from the current focus
     * @throws Exception
     */
    public static void focusChildFrame(WebDriver driver, By localLocator) throws Exception {
        WebDriverWait oWait = new WebDriverWait(driver, 10);
        oWait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(localLocator));
    }

    /**
     * Searches for and closes a particular browser window
     *
     * @param searchValue String - The title, window handle, or url search string or regexp
     * @param bRegExp     Boolean - True indicates searchValue is a regular expression, false a string
     * @param searchType  String - Valid options are TITLE, WHND, or URL
     * @throws Exception
     */
    public static void closeBrowser(WebDriver driver, String searchValue, Boolean bRegExp, String searchType) throws Exception {
        if (switchBrowser(driver, searchValue, bRegExp, searchType)) {
            driver.close();
        }
    }

    public static <T extends WebDriver> String openWindow(T driver, By by) {
        String parentHandle = driver.getWindowHandle(); // Save parent window
        WebElement clickableElement = driver.findElement(by);
        clickableElement.click(); // Open child window
        WebDriverWait wait = new WebDriverWait(driver, 5);
        boolean isChildWindowOpen = wait.until(ExpectedConditions.numberOfWindowsToBe(2));
        if (isChildWindowOpen) {
            Set<String> handles = driver.getWindowHandles();
            // Switch to child window
            for (String handle : handles) {
                driver.switchTo().window(handle);
                if (!parentHandle.equals(handle)) {
                    break;
                }
            }
            driver.manage().window().maximize();
        }
        return parentHandle; // Returns parent window if need to switch back
    }

    public static <T extends WebDriver> String openWindow(T driver, WebElement clickableElement) {
        String parentHandle = driver.getWindowHandle(); // Save parent window
        clickableElement.click(); // Open child window
        WebDriverWait wait = new WebDriverWait(driver, 5);
        boolean isChildWindowOpen = wait.until(ExpectedConditions.numberOfWindowsToBe(2));
        if (isChildWindowOpen) {
            Set<String> handles = driver.getWindowHandles();
            // Switch to child window
            for (String handle : handles) {
                driver.switchTo().window(handle);
                if (!parentHandle.equals(handle)) {
                    break;
                }
            }
            driver.manage().window().maximize();
        }
        return parentHandle; // Returns parent window if need to switch back
    }


    /**
     * Performs a "Back" operation in the currently active Browser window
     */
    public static void backBrowser(WebDriver driver) {
        driver.navigate().back();
    }

    /**
     * Performs a "Forward" operation in the currently active Browser window
     */
    public static void forwardBrowser(WebDriver driver) {
        driver.navigate().forward();
    }

    /**
     * Performs a "Refresh" operation in the currently active Browser window
     */
    public static void refreshBrowser(WebDriver driver) {
        driver.navigate().refresh();
    }

    /**
     * Searches for and switches to a particular browser window
     *
     * @param searchValue String - The title, window handle, or url search string or regexp
     * @param bRegExp     Boolean - True indicates searchValue is a regular expression, false a string
     * @param searchType  String - Valid options are TITLE, WHND, or URL
     * @return Boolean - True if the browser window was found, False if it was not.
     * @throws Exception
     */
    public static Boolean switchBrowser(WebDriver driver, String searchValue, Boolean bRegExp, String searchType) throws Exception {
        String actualValue = "";
        Boolean bEmpty = false;
        Calendar End = Calendar.getInstance();
        End.add(Calendar.SECOND, 10);

        Object[] handles = driver.getWindowHandles().toArray();
        for (int iHandle = 0; iHandle < handles.length; iHandle++) {
            driver.switchTo().window((String) handles[iHandle]);

            switch (searchType.toUpperCase().trim()) {
                case "TITLE": {
                    actualValue = driver.getTitle();
                    if (actualValue.equals("")) {
                        bEmpty = true;
                    }
                    break;
                }
                case "URL": {
                    searchValue = searchValue.replaceAll("/$", "");
                    actualValue = driver.getCurrentUrl().replaceAll("/$", "");
                    break;
                }
                case "WHND": {
                    actualValue = driver.getWindowHandle();
                    break;
                }
                case "INDEX": {
                    try {
                        int iIndex = Integer.parseInt(searchValue);
                        while (handles.length < iIndex + 1 && Calendar.getInstance().before(End)) {
                            Thread.sleep(1000);
                            handles = driver.getWindowHandles().toArray();
                        }
                        driver.switchTo().window((String) handles[iIndex]);
                    } catch (Exception oException) {
                        return false;
                    }
                    return true;
                }
                default:
                    throw new Exception("Unknown search type " + searchType + "!  Valid options are Title, URL, WHND, or INDEX.");
            }

            if (bRegExp) {
                if (actualValue.matches(searchValue)) {
                    return true;
                }
            } else {
                if (actualValue.equals(searchValue)) {
                    return true;
                }
            }
        }

        // If the title is empty and the window was not found, try URL instead
        if (bEmpty) {
            return switchBrowser(driver, searchValue, bRegExp, "URL");
        }

        return false;
    }

    /**
     * Resets the focus back to the original browser window or tab
     *
     * @throws Exception
     */
    public static void focusBrowser(WebDriver driver, String originalWindow) {
        try {
            driver.switchTo().window(originalWindow);
        } catch (Exception e) // If the original window was closed, we can't
        // switch to it. Need to switch to an open
        // window and make it the new "original"
        {
            int iIndex = -1;
            try {
                do {
                    iIndex++;
                } while (!switchBrowser(driver, String.valueOf(iIndex), false, "INDEX"));
            } catch (Exception e1) {
            }
            originalWindow = driver.getWindowHandle();
        }
    }

    /**
     * Closes all windows except for the original browser window
     *
     * @throws Exception
     */
    public static void closePopup(WebDriver driver) throws Exception {
        //TODO: Implement me
    }
}
