package com.usbank.test.helper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumExtentionsCSSSelector extends SeleniumExtentions {

    private static final Logger logger = LogManager.getLogger(SeleniumExtentionsCSSSelector.class);


    public static WebElement getElementBySelector(WebDriver driver, String cssSelector, long waitSeconds) {
        return waitForElement(driver, By.cssSelector(cssSelector), waitSeconds);
    }

    public static WebElement getElementBySelector(WebDriver driver, String cssSelector) {
        return waitForElement(driver, By.cssSelector(cssSelector), 10);
    }

    public static WebElement waitForElement(WebDriver driver, By locator, long waitSeconds) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, waitSeconds);
            return wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        } catch (RuntimeException ex) {
            seleniumExceptionHandler(ex);
            throw (ex);
        }
    }

    public static java.util.List<WebElement> getElementsBySelector(WebDriver driver, String cssSelctor, long waitSeconds) {

        return waitForManyElements(driver, By.cssSelector(cssSelctor), waitSeconds);
    }

    public static java.util.List<WebElement> getElementsBySelector(WebDriver driver, By cssSelctor, long waitSeconds) {

        return waitForManyElements(driver, cssSelctor, waitSeconds);
    }


    public static java.util.List<WebElement> getElementsBySelector(WebDriver driver, String cssSelctor) {

        return waitForManyElements(driver, By.cssSelector(cssSelctor), 10);
    }

    public static java.util.List<WebElement> waitForManyElements(WebDriver driver, By locator, long waitSeconds) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, waitSeconds);
            return wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
        } catch (RuntimeException ex) {
            seleniumExceptionHandler(ex);
            throw (ex);
        }
    }

    public static WebElement waitForElementDisplayed(WebDriver driver, By locator, long waitSeconds) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, waitSeconds);
            return wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        } catch (RuntimeException ex) {
            seleniumExceptionHandler(ex);
            throw (ex);
        }
    }

    public static void waitForElementNotDisplayed(WebDriver driver, By locator, long waitSeconds) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, waitSeconds);
            wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
        } catch (RuntimeException ex) {
            seleniumExceptionHandler(ex);
            throw (ex);
        }
    }


    public static void waitForElementNotDisplayed(WebDriver driver, String cssSelector, long waitSeconds) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, waitSeconds);
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(cssSelector)));
        } catch (RuntimeException ex) {
            seleniumExceptionHandler(ex);
            throw (ex);
        }
    }

    public static void waitForElementNotDisplayed(WebDriver driver, String cssSelector) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(cssSelector)));
        } catch (RuntimeException ex) {
            seleniumExceptionHandler(ex);
            throw (ex);
        }
    }

    public static void waitForElementClickable(WebDriver driver, String cssSelector, long waitSeconds) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, waitSeconds);
            wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(cssSelector)));
        } catch (RuntimeException ex) {
            seleniumExceptionHandler(ex);
            throw (ex);
        }
    }

    public static void waitForElementClickable(WebDriver driver, String cssSelector) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(cssSelector)));
        } catch (RuntimeException ex) {
            seleniumExceptionHandler(ex);
            throw (ex);
        }
    }

    public static boolean getChk(WebDriver driver, String cssSelector) {
        if ("true" == getElementBySelector(driver, cssSelector, 10).getAttribute("checked"))
            return true;
        return false;
    }

    public static boolean getChk(WebElement webElement) {
        if ("true" == webElement.getAttribute("checked"))
            return true;
        return false;
    }

    public static void setCmbText(WebDriver driver, String cssSelector, String text) {
        Select selectObject = new Select(getElementBySelector(driver, cssSelector));
        selectObject.selectByVisibleText(text);
    }

    public static void setChk(WebDriver driver, String cssSelector, boolean value) {
        String currentState = getElementBySelector(driver, cssSelector, 10).getAttribute("checked");
        if (value) {
            if (currentState != "true")
                getElementBySelector(driver, cssSelector, 10).click();
        } else {
            if (currentState != null)
                getElementBySelector(driver, cssSelector, 10).click();
            ;
        }
    }

    public static void setChk(WebElement webElement, boolean value) {
        String currentState = webElement.getAttribute("checked");
        if (value) {
            if (currentState != "true")
                webElement.click();
        } else {
            if (currentState != null)
                webElement.click();
            ;
        }
    }

    public static void setValue(WebElement webElement, String value) {
        webElement.clear();
        webElement.sendKeys(value);
    }

    public static void setValue(WebDriver driver, String cssSelector, String value) {
        WebElement element = waitForElement(driver, By.cssSelector(cssSelector), 10);
        element.clear();
        element.sendKeys(value);
    }

    public static String getValue(WebDriver driver, String cssSelector) {
        String value = getElementBySelector(driver, cssSelector).getAttribute("value");
        return value;
    }

    public static String getText(WebDriver driver, String cssSelector) {
        String value = getElementBySelector(driver, cssSelector).getText();
        return value;
    }
}
