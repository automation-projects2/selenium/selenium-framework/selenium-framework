package com.usbank.test.helper;

import com.usbank.test.automationFramework.trancore.regression.ShortTermPlanTests;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CaptureScreenShots {

	private  WebDriver driver;

	private static final Logger logger = LogManager.getLogger(CaptureScreenShots.class);
	
	public CaptureScreenShots(WebDriver driver){
		this.driver=driver;
	}

    /**
     *
     * @param filename Class and Method Names Concatenated
     * @throws IOException
     */
	public void takeScreenshot(String filename) throws IOException{
		File file=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(file,new File(System.getProperty("user.dir") + "\\Screenshots\\"+"\\"+getCurrentDate()+"\\"+filename+"_"+getCurrentDateTime()+".jpg"));
	}

	private String getCurrentDateTime(){

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd HH_mm_ss", Locale.US);
		String formattedDate = sdf.format(new Date());
		return formattedDate;

	}

	private String getCurrentDate(){

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd", Locale.US);
		String formattedDate = sdf.format(new Date());
		return formattedDate;

	}
}
