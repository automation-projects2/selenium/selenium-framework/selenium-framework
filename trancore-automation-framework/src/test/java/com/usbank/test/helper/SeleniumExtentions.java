package com.usbank.test.helper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Calendar;

public class SeleniumExtentions {

    private static final Logger logger = LogManager.getLogger(SeleniumExtentions.class);


    public static void seleniumExceptionHandler(Exception ex) {
        String exceptionName = ex.getMessage();
        logger.info(String.format("WebDriver Exception Handler Caught Exception: [%s]", exceptionName));
        screenshot();

    }

    public static void sleep(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public static void screenshot() {
        //TODO: Implement me!

    }


    /**
     * This method searches for text within a web page and returns a boolean indicating whether or not it was found
     *
     * @param sSearchText String - The text to search for within the web page
     * @param iTimeout    Integer - Time in seconds to search for the text before declaring it not present
     * @return Boolean - True if the text was found, False if it was not
     */
    public static Boolean doesPageTextExist(WebDriver driver, String sSearchText, int iTimeout) {
        Calendar EndTime = Calendar.getInstance();
        EndTime.add(Calendar.SECOND, iTimeout);
        sSearchText = sSearchText.replaceAll("\\s+", " ").trim(); // Normalize
        // white
        // space and
        // trim

        do {
            WebElement oPage = driver.findElement(By.tagName("body"));
            String sCurrentText = oPage.getText().replaceAll("\\\r\\\n", " ");
            sCurrentText = sCurrentText.replaceAll("\\\r", " ");
            sCurrentText = sCurrentText.replaceAll("\\\n", " ");
            sCurrentText = sCurrentText.replaceAll("<br>", " ");
            sCurrentText = sCurrentText.replaceAll("\\s+", " ").trim(); // Normalize
            // white
            // space
            // and
            // trim
            if (sCurrentText.contains(sSearchText)) {
                return true;
            }
        } while (Calendar.getInstance().before(EndTime));
        return false;
    }


}
