package com.usbank.test.component;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = "classpath:config.properties", ignoreResourceNotFound = true)
@ComponentScan("com.usbank")
public class SpringConfiguration {

}
