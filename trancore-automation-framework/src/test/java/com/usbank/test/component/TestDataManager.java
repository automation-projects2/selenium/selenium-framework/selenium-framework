package com.usbank.test.component;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.usbank.test.objects.EnvironmentData;
import com.usbank.test.objects.trancore.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class TestDataManager {

    private static final Logger logger = LogManager.getLogger(TestDataManager.class);

    private ObjectMapper mapper;
    private AutomationConfig config;
    private Map<String, Map<String, User>> testDataProfiles = new HashMap<>();
    private List<EnvironmentData> environments = new ArrayList<>();


    @Autowired
    public TestDataManager(AutomationConfig config) {
        this.config = config;
    }

    @PostConstruct
    public void init() {
        logger.info("Initializing Test Data Manager!");
        mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        //Load Trancore User Data
        try {
            TypeReference<HashMap<String, User>> typeReference = new TypeReference<HashMap<String, User>>() {
            };

            File folder = new File(config.getDataLocation() + "/testData/");
            File[] files = folder.listFiles();
            for (File f : files) {
                if (f.getName().contains(TestEnvironment.DEV.toString())) {
                    testDataProfiles.put(TestEnvironment.DEV.toString(), mapper.readValue(f, typeReference));
                }
                if (f.getName().contains(TestEnvironment.IT.toString())) {
                    testDataProfiles.put(TestEnvironment.IT.toString(), mapper.readValue(f, typeReference));
                }
                if (f.getName().contains(TestEnvironment.UAT.toString())) {
                    testDataProfiles.put(TestEnvironment.UAT.toString(), mapper.readValue(f, typeReference));
                }
            }


        } catch (IOException e) {
            logger.error("ERROR loading Data File: " + config.getDataLocation(), e);
            throw new RuntimeException("Error Loading Test Data!");
        }

        //Load Trancore Environment Data
        try {
            File data = new File(config.getDataLocation() + "environmentData.json");
            environments = mapper.readValue(data, mapper.getTypeFactory().constructCollectionType(List.class, EnvironmentData.class));
        } catch (IOException e) {
            logger.error("Error loading Environment File: " + config.getDataLocation(), e);
            throw new RuntimeException("Failed to Load Environment Data!");
        }

        logger.info("Loaded: " + testDataProfiles.size() + " user records from " + config.getDataLocation());
        logger.info("Loaded: " + environments.size() + " environment records from " + config.getDataLocation());
    }

    public List<EnvironmentData> getEnvironmentsByEnvironment(String environment) {
        return environments.stream().filter(e -> environment.equals(e.getEnvironment()))
                .collect(Collectors.toList());
    }

    public List<EnvironmentData> getEnvironmentsBySystem(String system) {
        return environments.stream().filter(e -> system.equals(e.getSystem()))
                .collect(Collectors.toList());
    }

    public Optional<EnvironmentData> getEnvironmentsByEnvironmentSystem(String environment, String system) {
        return environments.stream().filter(e -> environment.equals(e.getEnvironment()))
                .filter(e -> system.equals(e.getSystem())).findFirst();
    }

    public User getUserData(String environment, String profileName) {
        Map<String, User> dataProfiles = testDataProfiles.get(environment);
        return dataProfiles.get(profileName);
    }

    public Map<String, Map<String, User>> getTestDataProfiles() {
        return testDataProfiles;
    }

    public List<EnvironmentData> getEnvironments() {
        return environments;
    }


    public void logUserData() {
        logger.info("----BEGIN USER DATA----");
        logger.info(testDataProfiles.toString());
        logger.info("----END USER DATA----");
    }

    public void logUserCount() {
        logger.info("Test Data User Count: " + testDataProfiles.size());
    }

    public void printUserJSON() {
        try {
            logger.info(mapper.writeValueAsString(testDataProfiles));
        } catch (JsonProcessingException e) {
            logger.error("ERROR printing USER JSON!", e);
        }
    }


}
